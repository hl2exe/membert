# coding=utf-8
# Copyright 2019 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""
Tokenization classes implementation.
The file is forked from:
https://github.com/google-research/bert/blob/master/tokenization.py.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import collections
import os

import numpy as np
import tensorflow as tf
from transformers import DistilBertTokenizer, RobertaTokenizer, BertTokenizer

from utility.embedding_utils import build_embeddings_matrix, load_embedding_model
from utility.log_utils import get_logger
from utility.python_utils import flatten_nested_array
from spacy.lang.en import English
from collections import Counter
from tqdm import tqdm

logger = get_logger(__name__)


class Tokenizer(object):

    def __init__(self, build_embedding_matrix=False, embedding_dimension=None,
                 embedding_model_type=None):
        if build_embedding_matrix:
            assert embedding_model_type is not None
            assert embedding_dimension is not None and type(embedding_dimension) == int

        self.build_embedding_matrix = build_embedding_matrix
        self.embedding_dimension = embedding_dimension
        self.embedding_model_type = embedding_model_type
        self.embedding_model = None
        self.embedding_matrix = None
        self.vocab = None

    def build_vocab(self, data, **kwargs):
        raise NotImplementedError()

    def initalize_with_vocab(self, vocab):
        raise NotImplementedError()

    def tokenize(self, text):
        raise NotImplementedError()

    def convert_tokens_to_ids(self, tokens):
        raise NotImplementedError()

    def convert_ids_to_tokens(self, ids):
        raise NotImplementedError()

    def get_info(self):
        return {
            'build_embedding_matrix': self.build_embedding_matrix,
            'embedding_dimension': self.embedding_dimension,
            'embedding_model_type': self.embedding_model_type,
            'embedding_matrix': self.embedding_matrix,
            'embedding_model': self.embedding_model,
            'vocab_size': len(self.vocab) + 1,
            'vocab': self.vocab
        }

    def save_info(self, filepath, prefix=None):
        filepath = os.path.join(filepath, 'tokenizer_info_{}.npy'.format(prefix))
        np.save(filepath, self.get_info())

    def show_info(self, info=None):
        info = info if info is not None else self.get_info()
        info = {key: value for key, value in info.items() if key != 'vocab'}

        logger.info('Tokenizer info: {}'.format(info))

    @staticmethod
    def load_info(filepath, prefix=None):
        filepath = os.path.join(filepath, 'tokenizer_info_{}.npy'.format(prefix))
        return np.load(filepath, allow_pickle=True).item()


class KerasTokenizer(Tokenizer):

    def __init__(self, tokenizer_args=None, **kwargs):
        super(KerasTokenizer, self).__init__(**kwargs)

        tokenizer_args = {} if tokenizer_args is None else tokenizer_args
        assert isinstance(tokenizer_args, dict) or isinstance(tokenizer_args, collections.OrderedDict)

        self.tokenizer_args = tokenizer_args

    def build_vocab(self, data, **kwargs):
        all_data = [item.get_data() for item in data if item is not None]
        all_data = [item for seq in all_data for item in seq]

        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.tokenizer.fit_on_texts(all_data)
        self.vocab = self.tokenizer.word_index

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                            embedding_model=self.embedding_model,
                                                            embedding_dimension=self.embedding_dimension,
                                                            word_to_idx=self.vocab)

    def initalize_with_vocab(self, vocab):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.tokenizer.word_index = vocab

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=vocab,
                                                                        merge_vocabularies=self.merge_vocabularies)

            self.tokenizer.word_index = self.vocab

    def get_info(self):
        info = super(KerasTokenizer, self).get_info()
        info['vocab_size'] = len(self.vocab) + 1
        info['tokenizer'] = self.tokenizer

        return info

    def tokenize(self, text):
        return text

    def convert_tokens_to_ids(self, tokens):
        if type(tokens) == str:
            return self.tokenizer.texts_to_sequences([tokens])[0]
        else:
            return self.tokenizer.texts_to_sequences(tokens)

    def convert_ids_to_tokens(self, ids):
        return self.tokenizer.sequences_to_texts(ids)


class SpacyTokenizer(Tokenizer):

    def __init__(self, tokenizer_args=None, **kwargs):
        super(SpacyTokenizer, self).__init__(**kwargs)

        tokenizer_args = {} if tokenizer_args is None else tokenizer_args
        assert isinstance(tokenizer_args, dict) or isinstance(tokenizer_args, collections.OrderedDict)

        self.tokenizer = English()
        self.tokenizer_args = tokenizer_args
        self.null_token = '--NULL--'
        self.unk_token = '--UNK--'

    def build_vocab(self, data, **kwargs):
        all_data = [item.get_data() for item in data if item is not None]
        all_data = [item for seq in all_data for item in seq]

        words = []
        for text in tqdm(all_data):
            for word in self.tokenize(text):
                words.append(word.strip('\n'))

        self.vocab = Counter(words)
        self.vocab = [self.null_token] + [self.unk_token] + sorted(self.vocab, key=self.vocab.get, reverse=True)
        self.vocab = dict([(x, y + 1) for (y, x) in enumerate(self.vocab)])
        self.rev_vocab = {value: key for key, value in self.vocab.items()}

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                            embedding_model=self.embedding_model,
                                                            embedding_dimension=self.embedding_dimension,
                                                            word_to_idx=self.vocab)

    def initalize_with_vocab(self, vocab):
        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix, self.vocab = build_embeddings_matrix(vocab_size=len(vocab) + 1,
                                                                        embedding_model=self.embedding_model,
                                                                        embedding_dimension=self.embedding_dimension,
                                                                        word_to_idx=vocab)

            self.rev_vocab = {value: key for key, value in self.vocab.items()}

    def get_info(self):
        info = super(SpacyTokenizer, self).get_info()
        info['vocab_size'] = len(self.vocab) + 1
        info['tokenizer'] = self

        return info

    def tokenize(self, text):
        return [token.text for token in self.tokenizer(text)]

    def convert_tokens_to_ids(self, tokens):
        token_ids = []
        for token in tokens:
            token_ids.append(self.vocab[token] if token in self.vocab else self.vocab[self.unk_token])

        return token_ids

    def convert_ids_to_tokens(self, ids):
        is_single_example = False
        if type(ids[0]) not in [list, np.ndarray]:
            ids = [ids]
            is_single_example = True

        tokens = []
        for ids_seq in ids:
            tokens.append(' '.join([self.rev_vocab[idx] for idx in ids_seq if idx != 0]))

        return tokens if not is_single_example else tokens[0]


class RationaleKerasTokenizer(KerasTokenizer):

    def build_vocab(self, data, **kwargs):
        data = flatten_nested_array(data)
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(**self.tokenizer_args)
        self.tokenizer.fit_on_texts(data)
        self.vocab = self.tokenizer.word_index

        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

            self.embedding_matrix = build_embeddings_matrix(vocab_size=len(self.vocab) + 1,
                                                            embedding_model=self.embedding_model,
                                                            embedding_dimension=self.embedding_dimension,
                                                            word_to_idx=self.vocab)


class DistilBertTokenizerWrapper(Tokenizer):

    def __init__(self, preloaded_name, **kwargs):
        super(DistilBertTokenizerWrapper, self).__init__(**kwargs)
        self.tokenizer = DistilBertTokenizer.from_pretrained(preloaded_name)
        self.vocab = self.tokenizer.get_vocab()
        self.vocab_size = len(self.vocab)
        self.preloaded_model_name = preloaded_name

    def build_vocab(self, data, **kwargs):
        pass

    def tokenize(self, text):
        return self.tokenizer(text)

    def get_info(self):
        info = super(DistilBertTokenizerWrapper, self).get_info()
        info['tokenizer'] = self

        return info

    @classmethod
    def from_pretrained(cls, preloaded_name):
        tokenizer = DistilBertTokenizerWrapper(preloaded_name)
        return tokenizer

    def initialize_with_info(self, info):
        self.tokenizer = DistilBertTokenizer.from_pretrained(self.preloaded_model_name)
        self.tokenizer.word_index = info['vocab']

    @property
    def cls_token(self):
        return self.tokenizer.cls_token

    @property
    def cls_token_id(self):
        return self.tokenizer.cls_token_id

    @property
    def sep_token(self):
        return self.tokenizer.sep_token

    @property
    def sep_token_id(self):
        return self.tokenizer.sep_token_id

    @property
    def pad_token(self):
        return self.tokenizer.pad_token

    @property
    def pad_token_id(self):
        return self.tokenizer.pad_token_id

    def convert_ids_to_tokens(self, ids):
        is_single_example = False
        if type(ids[0]) not in [list, np.ndarray]:
            ids = [ids]
            is_single_example = True

        tokens = []
        for ids_seq in ids:
            tokens_seq = [self.tokenizer._convert_id_to_token(idx) for idx in ids_seq if idx != 0]
            tokens.append(self.tokenizer.convert_tokens_to_string(tokens_seq))

        return tokens if not is_single_example else tokens[0]


class DistilRoBertaTokenizerWrapper(Tokenizer):

    def __init__(self, preloaded_name, **kwargs):
        super(DistilRoBertaTokenizerWrapper, self).__init__(**kwargs)
        self.tokenizer = RobertaTokenizer.from_pretrained(preloaded_name)
        self.vocab = self.tokenizer.get_vocab()
        self.vocab_size = len(self.vocab)
        self.preloaded_model_name = preloaded_name

    def build_vocab(self, data, **kwargs):
        pass

    def get_info(self):
        info = super(DistilRoBertaTokenizerWrapper, self).get_info()
        info['tokenizer'] = self

        return info

    def tokenize(self, text):
        return self.tokenizer(text)

    @classmethod
    def from_pretrained(cls, preloaded_name):
        tokenizer = DistilBertTokenizerWrapper(preloaded_name)
        return tokenizer

    def initialize_with_info(self, info):
        self.tokenizer = DistilBertTokenizer.from_pretrained(self.preloaded_model_name)
        self.tokenizer.word_index = info['vocab']

    @property
    def cls_token(self):
        return self.tokenizer.cls_token

    @property
    def cls_token_id(self):
        return self.tokenizer.cls_token_id

    @property
    def sep_token(self):
        return self.tokenizer.sep_token

    @property
    def sep_token_id(self):
        return self.tokenizer.sep_token_id

    @property
    def pad_token(self):
        return self.tokenizer.pad_token

    @property
    def pad_token_id(self):
        return self.tokenizer.pad_token_id


class BertTokenizerWrapper(Tokenizer):

    def __init__(self, preloaded_name, **kwargs):
        super(BertTokenizerWrapper, self).__init__(**kwargs)
        self.tokenizer = BertTokenizer.from_pretrained(preloaded_name)
        self.vocab = self.tokenizer.get_vocab()
        self.vocab_size = len(self.vocab)
        self.preloaded_model_name = preloaded_name

    def build_vocab(self, data, **kwargs):
        pass

    def tokenize(self, text):
        return self.tokenizer(text)

    def get_info(self):
        info = super(BertTokenizerWrapper, self).get_info()
        info['tokenizer'] = self

        return info

    @classmethod
    def from_pretrained(cls, preloaded_name):
        tokenizer = DistilBertTokenizerWrapper(preloaded_name)
        return tokenizer

    def initialize_with_info(self, info):
        self.tokenizer = DistilBertTokenizer.from_pretrained(self.preloaded_model_name)
        self.tokenizer.word_index = info['vocab']

    @property
    def cls_token(self):
        return self.tokenizer.cls_token

    @property
    def cls_token_id(self):
        return self.tokenizer.cls_token_id

    @property
    def sep_token(self):
        return self.tokenizer.sep_token

    @property
    def sep_token_id(self):
        return self.tokenizer.sep_token_id

    @property
    def pad_token(self):
        return self.tokenizer.pad_token

    @property
    def pad_token_id(self):
        return self.tokenizer.pad_token_id


class TokenizerFactory(object):
    supported_tokenizers = {
        'keras_tokenizer': KerasTokenizer,
        'spacy_tokenizer': SpacyTokenizer,

        'rationale_keras_tokenizer': RationaleKerasTokenizer,

        'distilbert_tokenizer': DistilBertTokenizerWrapper,
        'bert_tokenizer': BertTokenizerWrapper,
        'roberta_tokenizer': DistilRoBertaTokenizerWrapper
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if TokenizerFactory.supported_tokenizers[key]:
            return TokenizerFactory.supported_tokenizers[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
