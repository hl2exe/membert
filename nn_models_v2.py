"""

@Author: Federico Ruggeri

@Date: 18/09/19

TODO: add Hierarchical-Attention-Network (HAN)

"""

import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

import numpy as np

from experimental_custom_tf_v2 import M_Basic_Memn2n, M_Baseline_LSTM, M_Baseline_CNN, M_Gated_Memn2n, \
    M_Discriminative_Memn2n, M_Windowed_HAN, M_Res_Memn2n, M_Baseline_Sum, M_DistilBERT, M_MemoryDistilBERT, \
    M_RationaleCNN, M_DistilRoBERTa, M_BERT, M_MemoryDistilRoBERTa, M_MemoryBERT, M_SquadMANN, M_BiDAF, \
    M_SquadDistilBert, M_SquadDistilRoBerta, M_SquadBERT, M_Mapper_Memn2n, M_Pairwise_Baseline_Sum
from utility.cross_validation_utils import build_metrics, compute_iteration_validation_error
from utility.log_utils import get_logger
from utility.python_utils import merge
from utility.printing_utils import prettify_statistics
from utility.tensorflow_utils_v2 import add_gradient_noise
from tqdm import tqdm
import os
from collections import OrderedDict
from sklearn.utils.class_weight import compute_class_weight
from dataset_samplers import SamplerFactory
import pickle
from transformers import DistilBertConfig, RobertaConfig, BertConfig

logger = get_logger(__name__)


# TODO: add checkpoint validation set evaluation during fit()
class Network(object):

    def __init__(self, embedding_dimension, name='network', additional_data=None,
                 is_pretrained=False, is_multilabel=False):
        self.embedding_dimension = embedding_dimension
        self.model = None
        self.optimizer = None
        self.name = name
        self.additional_data = additional_data
        self.is_pretrained = is_pretrained
        self.is_multilabel = is_multilabel

    # Saving/Weights

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        self.model.set_weights(weights)

    def save(self, filepath, overwrite=True):
        if filepath.endswith('.h5'):
            base_path = filepath.split('.h5')[0]
        else:
            base_path = filepath

        # Model weights
        weights_path = base_path + '.h5'
        self.model.save_weights(weights_path)

        # Model state
        network_state = self.get_state()
        if network_state is not None:
            state_path = base_path + '.pickle'
            with open(state_path, 'wb') as f:
                pickle.dump(network_state, f)

    def load(self, filepath, is_external=False, **kwargs):
        if filepath.endswith('.h5'):
            base_path = filepath.split('.h5')[0]
        else:
            base_path = filepath

        # Model weights
        weights_path = base_path + '.h5'
        self.model.load_weights(filepath=weights_path, **kwargs)

        # Model state
        if not is_external:
            state_path = base_path + '.pickle'
            if os.path.isfile(state_path):
                with open(state_path, 'rb') as f:
                    network_state = pickle.load(f)

                self.set_state(network_state=network_state)

    def get_state(self):
        return None

    def set_state(self, network_state):
        pass

    def _update_internal_state(self, model_additional_info, batch_idx, epoch):
        pass

    # Tensorboard

    def build_tensorboard_info(self):
        pass

    # Loss weights

    def compute_output_weights(self, y_train, num_classes, mode='multi-class'):

        self.num_classes = num_classes

        # Multi-class
        if mode == 'multi-class':
            if len(y_train.shape) > 1:
                if y_train.shape[1] > 1:
                    y_train = np.argmax(y_train, axis=1)
                else:
                    y_train = y_train.ravel()

            classes = np.unique(y_train)
            class_weights = compute_class_weight(class_weight='balanced', classes=classes, y=y_train)
            self.class_weights = {cls: weight for cls, weight in zip(classes, class_weights)}

        # Multi-label
        else:
            assert type(y_train) in [dict, OrderedDict]

            class_weights = {}
            for label_name, classes_amount in num_classes.items():
                label_key = 'label_id_{}'.format(label_name)
                key_values = np.argmax(y_train[label_key], axis=-1)
                key_classes = list(range(classes_amount))
                current_weights = compute_class_weight(class_weight='balanced',
                                                       classes=key_classes, y=key_values)
                class_weights.setdefault(label_key, {cls: weight for cls, weight in zip(key_classes, current_weights)})

            self.class_weights = class_weights

    # Training/Inference

    def predict(self, x, steps, callbacks=None, suffix='test'):

        callbacks = callbacks or []

        total_preds = {}

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self)
                callback.on_prediction_begin(logs={'suffix': suffix})

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'suffix': suffix})

            batch = next(x)
            if type(batch) in [tuple, list]:
                batch = batch[0]
            preds, model_additional_info = self.batch_predict(x=batch)
            preds = self._parse_predictions(preds)

            if type(preds) in [dict, OrderedDict]:
                preds = {key: value for key, value in preds.items()}
                for key, value in preds.items():
                    total_preds.setdefault(key, []).extend(value)
            else:
                total_preds.setdefault('predictions', []).extend(preds)

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model_additional_info': model_additional_info,
                                                                            'suffix': suffix})

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs={'suffix': suffix})

        if type(total_preds) in [dict, OrderedDict]:
            total_preds = {key: np.array(value) for key, value in total_preds.items()}
        else:
            total_preds = np.array(total_preds)

        if len(total_preds.keys()) == 1 and 'predictions' in total_preds:
            total_preds = total_preds['predictions']

        return total_preds

    def distributed_predict(self, x, steps, strategy, callbacks=None, suffix='test'):

        callbacks = callbacks or []

        if self.is_multilabel:
            total_preds = {}
        else:
            total_preds = []

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self)
                callback.on_prediction_begin(logs={'suffix': suffix})

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'suffix': suffix})

            batch = next(x)
            if type(batch) in [tuple, list]:
                batch = batch[:1]
            preds, model_additional_info = self.distributed_batch_predict(inputs=list(batch), strategy=strategy)
            if type(preds) in [dict, OrderedDict]:
                preds = {key: value for key, value in preds.items()}
                for key, value in preds.items():
                    total_preds.setdefault(key, []).extend(value)
            else:
                total_preds.extend(preds)

            # model_additional_info = {key: tf.stack(value).numpy() if type(value) is tuple else value
            #                          for key, value in model_additional_info.items()}

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model_additional_info': model_additional_info,
                                                                            'suffix': suffix})

            if self.is_multilabel:
                total_preds = {key: np.concatenate(value) for key, value in total_preds.items()}
            else:
                total_preds = np.array(total_preds)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs={'suffix': suffix})

        return np.array(total_preds)

    def evaluate(self, data, steps):
        total_loss = {}

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            batch_additional_info = self._get_additional_info()
            batch_info = self.batch_evaluate(*next(data), batch_additional_info)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / steps for key, item in total_loss.items()}
        return total_loss

    def evaluate_and_predict(self, data, steps, callbacks=None, suffix='val'):
        total_loss = {}

        callbacks = callbacks or []

        total_preds = {}

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self)
                callback.on_prediction_begin(logs={'suffix': suffix})

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'suffix': suffix})

            batch = next(data)
            batch_additional_info = self._get_additional_info()
            batch_info, preds, model_additional_info = self.batch_evaluate_and_predict(*batch, batch_additional_info)
            preds = self._parse_predictions(preds)
            batch_info = {key: item for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

            if type(preds) in [dict, OrderedDict]:
                preds = {key: value for key, value in preds.items()}
                for key, value in preds.items():
                    total_preds.setdefault(key, []).extend(value)
            else:
                total_preds.setdefault('predictions', []).extend(preds)

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model_additional_info': model_additional_info,
                                                                            'suffix': suffix})

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs={'suffix': suffix})

        total_loss = {key: item / steps for key, item in total_loss.items()}

        if type(total_preds) in [dict, OrderedDict]:
            total_preds = {key: np.array(value) for key, value in total_preds.items()}
        else:
            total_preds = np.array(total_preds)

        if len(total_preds.keys()) == 1 and 'predictions' in total_preds:
            total_preds = total_preds['predictions']

        return total_loss, total_preds

    def distributed_evaluate(self, data, steps, strategy):
        total_loss = {}

        for batch_idx in tqdm(range(steps), leave=True, position=0):

            batch_additional_info = self._get_additional_info()
            batch_info = self.distributed_batch_evaluate(inputs=list(next(data)) + [batch_additional_info],
                                                         strategy=strategy)
            batch_info = {key: item for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / steps for key, item in total_loss.items()}
        return total_loss

    def average_evaluate(self, validation_data, val_y, eval_steps, callbacks=None, repetitions=1,
                         metrics=None, parsed_metrics=None, additional_metrics_info=None,
                         metrics_nicknames=None, error_metrics_flags=None):

        avg_val_info = {}
        avg_val_metrics = {}

        for rep in range(repetitions):
            val_info, val_preds = self.evaluate_and_predict(data=iter(validation_data()), steps=eval_steps,
                                                            callbacks=callbacks, suffix='val')
            for key, value in val_info.items():
                avg_val_info.setdefault(key, []).append(value)

            if metrics is not None:
                all_val_metrics = compute_iteration_validation_error(parsed_metrics,
                                                                     true_values=val_y,
                                                                     predicted_values=val_preds,
                                                                     error_metrics_additional_info=additional_metrics_info,
                                                                     error_metrics_nicknames=metrics_nicknames,
                                                                     error_metrics_flags=error_metrics_flags,
                                                                     prefix='val')
                for key, value in all_val_metrics.items():
                    avg_val_metrics.setdefault(key, []).append(value)

        avg_val_info = {key: np.mean(value, axis=0) for key, value in avg_val_info.items()}
        if metrics is not None:
            avg_val_metrics = {key: np.mean(value, axis=0) for key, value in avg_val_metrics.items()}

        return avg_val_info, avg_val_metrics

    def average_predict(self, data, steps, true_values, parsed_metrics, error_metrics_additional_info,
                        error_metrics_nicknames, metric_function, error_metrics_flags=None, callbacks=None, suffix=None,
                        repetitions=1):

        avg_predictions = {} if self.is_multilabel else []
        avg_metrics = {}

        for rep in range(repetitions):
            rep_suffix = suffix + '_inference-rep_{}'.format(rep) if suffix is not None else suffix
            predictions = self.predict(x=iter(data()), steps=steps, callbacks=callbacks, suffix=rep_suffix)
            metrics = metric_function(parsed_metrics=parsed_metrics,
                                      true_values=true_values,
                                      predicted_values=predictions,
                                      error_metrics_additional_info=error_metrics_additional_info,
                                      error_metrics_nicknames=error_metrics_nicknames,
                                      error_metrics_flags=error_metrics_flags)

            for key, value in metrics.items():
                avg_metrics.setdefault(key, []).append(value)

            if self.is_multilabel:
                for key, value in predictions.items():
                    avg_predictions.setdefault(key, []).append(value)
            else:
                avg_predictions.append(predictions)

        avg_metrics = {key: np.mean(value, axis=0) for key, value in avg_metrics.items()}

        return avg_predictions, avg_metrics

    def fit(self, train_data=None, fixed_train_data=None,
            epochs=1, verbose=1,
            callbacks=None, validation_data=None,
            step_checkpoint=None,
            metrics=None, additional_metrics_info=None, metrics_nicknames=None,
            error_metrics_flags=None,
            train_num_batches=None, eval_num_batches=None,
            np_val_y=None,
            np_train_y=None,
            val_inference_repetitions=1):

        # self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            res = callback.on_train_begin(logs={'epochs': epochs,
                                                'steps_per_epoch': train_num_batches})
            if res is not None and type(res) == dict and 'epochs' in res:
                epochs = res['epochs']

        if verbose:
            logger.info('Start Training!')

            if train_num_batches is not None:
                logger.info('Total batches: {}'.format(train_num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(train_num_batches * step_checkpoint)
                logger.info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > train_num_batches:
                    step_checkpoint = int(train_num_batches * 0.1)
                    logger.info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        train_data = iter(train_data())

        # Training
        for epoch in range(epochs):

            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            for callback in callbacks:
                callback.on_epoch_begin(epoch=epoch, logs={'epochs': epochs})

            train_loss = {}
            batch_idx = 0

            # Run epoch
            pbar = tqdm(total=train_num_batches)
            while batch_idx < train_num_batches:

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                batch_additional_info = self._get_additional_info()
                batch = next(train_data)
                batch_info, model_additional_info = self.batch_fit(*batch, batch_additional_info)
                batch_info = {key: item.numpy() for key, item in batch_info.items()}

                for key, value in batch_info.items():
                    if np.isnan(value):
                        raise RuntimeError('Loss encountered nan values!')

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                # Update any internal network state
                self._update_internal_state(model_additional_info, batch_idx, epoch)

                for key, item in batch_info.items():
                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

                batch_idx += 1
                pbar.update(1)

            pbar.close()

            train_loss = {key: item / train_num_batches for key, item in train_loss.items()}
            train_loss_str = {key: float('{:.2f}'.format(value)) for key, value in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                val_info, all_val_metrics = self.average_evaluate(validation_data=validation_data,
                                                                  eval_steps=eval_num_batches,
                                                                  val_y=np_val_y,
                                                                  callbacks=callbacks,
                                                                  metrics=metrics,
                                                                  parsed_metrics=parsed_metrics,
                                                                  additional_metrics_info=additional_metrics_info,
                                                                  metrics_nicknames=metrics_nicknames,
                                                                  error_metrics_flags=error_metrics_flags,
                                                                  repetitions=val_inference_repetitions)

                val_info_str = {key: float('{:.2f}'.format(value)) for key, value in val_info.items()}

                if metrics is not None:
                    val_metrics_str_result = {key: float('{:.2f}'.format(value)) for key, value in
                                              all_val_metrics.items()}

                    merged_statistics = merge(train_loss_str, val_info_str)
                    merged_statistics = merge(merged_statistics, val_metrics_str_result)
                    merged_statistics = merge(merged_statistics, {'epoch': epoch + 1})

                    logger.info('\n{}'.format(prettify_statistics(merged_statistics)))

                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        merged_statistics = merge(train_loss_str, val_info_str)
                        merged_statistics = merge(merged_statistics, {'epoch': epoch + 1})
                        logger.info(prettify_statistics(merged_statistics))
            else:
                merged_statistics = merge(train_loss_str, {'epoch': epoch + 1})
                logger.info(prettify_statistics(merged_statistics))

            for callback in callbacks:
                callback_args = merge(train_loss, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs={'name': self.name})

    def _get_input_iterator(self, input_fn, strategy):
        """Returns distributed dataset iterator."""
        # When training with TPU pods, datasets needs to be cloned across
        # workers. Since Dataset instance cannot be cloned in eager mode, we instead
        # pass callable that returns a dataset.
        if not callable(input_fn):
            raise ValueError('`input_fn` should be a closure that returns a dataset.')
        iterator = iter(
            strategy.experimental_distribute_datasets_from_function(input_fn))
        return iterator

    # TODO: update
    def distributed_fit(self, train_data=None,
                        epochs=1, verbose=1, strategy=None,
                        callbacks=None, validation_data=None,
                        step_checkpoint=None,
                        metrics=None, additional_metrics_info=None, metrics_nicknames=None,
                        train_num_batches=None, eval_num_batches=None,
                        np_val_y=None):

        # self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            callback.on_train_begin(logs=None)

        if verbose:
            logger.info('Start Training!')

            if train_num_batches is not None:
                logger.info('Total batches: {}'.format(train_num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(train_num_batches * step_checkpoint)
                logger.info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > train_num_batches:
                    step_checkpoint = int(train_num_batches * 0.1)
                    logger.info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        train_data = self._get_input_iterator(train_data, strategy)

        # Training
        for epoch in range(epochs):

            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            for callback in callbacks:
                callback.on_epoch_begin(epoch=epoch, logs={'epochs': epochs})

            train_loss = {}
            batch_idx = 0

            # Run epoch
            pbar = tqdm(total=train_num_batches)
            while batch_idx < train_num_batches:

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                batch_additional_info = self._get_additional_info()
                batch_info = self.distributed_batch_fit(inputs=list(next(train_data)) + [batch_additional_info],
                                                        strategy=strategy)
                batch_info = {key: item.numpy() for key, item in batch_info.items()}

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                for key, item in batch_info.items():

                    # Stop if something went wrong at optimization level...
                    if np.isnan(item):
                        raise RuntimeError('NaNs encountered! Aborting...')

                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

                batch_idx += 1
                pbar.update(1)

            pbar.close()

            train_loss = {key: item / train_num_batches for key, item in train_loss.items()}
            train_loss_str = {key: float('{:.3f}'.format(value)) for key, value in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                # val_info = self.distributed_evaluate(data=self._get_input_iterator(validation_data, strategy),
                #                                      strategy=strategy,
                #                                      steps=eval_num_batches)
                val_info = self.evaluate(data=iter(validation_data()), steps=eval_num_batches)
                val_info_str = {key: float('{:.3f}'.format(value)) for key, value in val_info.items()}

                if metrics is not None:
                    # val_predictions = self.distributed_predict(self._get_input_iterator(validation_data, strategy),
                    #                                            steps=eval_num_batches,
                    #                                            callbacks=callbacks,
                    #                                            strategy=strategy)
                    val_predictions = self.predict(iter(validation_data()),
                                                   steps=eval_num_batches,
                                                   callbacks=callbacks)
                    val_predictions = val_predictions.reshape(np_val_y.shape).astype(np_val_y.dtype)

                    all_val_metrics = compute_iteration_validation_error(parsed_metrics,
                                                                         true_values=np_val_y,
                                                                         predicted_values=val_predictions,
                                                                         error_metrics_additional_info=additional_metrics_info,
                                                                         error_metrics_nicknames=metrics_nicknames,
                                                                         prefix='val')
                    val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in all_val_metrics.items()])]
                    logger.info('Epoch: {0} -- Train Loss: {1}'
                                ' -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                               train_loss_str,
                                                                               val_info_str,
                                                                               ' -- '.join(
                                                                                   val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                            train_loss_str,
                                                                                            val_info_str))
            else:
                logger.info('Epoch: {0} -- Train Loss: {1}'.format(epoch + 1,
                                                                   train_loss_str))

            for callback in callbacks:
                callback_args = train_loss
                if validation_data is not None:
                    callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback_args['strategy'] = strategy
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def _get_additional_info(self):
        return None

    def _parse_predictions(self, raw_predictions):
        return raw_predictions

    @tf.function
    def batch_fit(self, x, y, additional_info=None):
        loss, loss_info, model_additional_info, grads = self.train_op(x, y, additional_info=additional_info)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))
        train_loss_info = {'train_{}'.format(key): item for key, item in loss_info.items()}
        train_loss_info['train_loss'] = loss
        return train_loss_info, model_additional_info

    @tf.function
    def distributed_batch_fit(self, inputs, strategy):
        train_loss_info = strategy.run(self.batch_fit, args=inputs)
        train_loss_info = {key: strategy.reduce(tf.distribute.ReduceOp.MEAN, item, axis=None)
                           for key, item in train_loss_info.items()}
        return train_loss_info

    @tf.function
    def batch_predict(self, x):
        additional_info = self._get_additional_info()
        predictions, model_additional_info = self.model(x,
                                                        state='prediction',
                                                        training=False,
                                                        additional_info=additional_info)
        return predictions, model_additional_info

    @tf.function
    def distributed_batch_predict(self, inputs, strategy):
        predictions = strategy.run(self.batch_predict, args=inputs)
        return predictions

    @tf.function
    def distributed_batch_evaluate(self, inputs, strategy):
        val_loss_info = strategy.run(self.batch_evaluate, args=inputs)
        val_loss_info = {key: strategy.reduce(tf.distribute.ReduceOp.MEAN, item, axis=None)
                         for key, item in val_loss_info.items()}
        return val_loss_info

    @tf.function
    def batch_evaluate(self, x, y, additional_info=None):
        loss, loss_info = self.loss_op(x, y, training=False, state='evaluation', additional_info=additional_info)
        val_loss_info = {'val_{}'.format(key): item for key, item in loss_info.items()}
        val_loss_info['val_loss'] = loss
        return val_loss_info

    @tf.function
    def batch_evaluate_and_predict(self, x, y, additional_info=None):
        loss, loss_info, predictions, model_additional_info = self.loss_op(x, y, training=False,
                                                                           state='evaluation',
                                                                           additional_info=additional_info,
                                                                           return_predictions=True)
        val_loss_info = {'val_{}'.format(key): item for key, item in loss_info.items()}
        val_loss_info['val_loss'] = loss
        # predictions = self._parse_predictions(predictions)
        return val_loss_info, predictions, model_additional_info

    # Model definition

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y, training=True, additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        return loss, loss_info, model_additional_info, grads

    def build_model(self, text_info):
        raise NotImplementedError()

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        raise NotImplementedError()


class ClassificationNetwork(Network):

    def __init__(self, weight_predictions=True, **kwargs):
        super(ClassificationNetwork, self).__init__(**kwargs)
        self.weight_predictions = weight_predictions

    def _classification_ce(self, targets, logits, reduce=True):
        if self.is_multilabel:
            cross_entropy = None
            for key, key_targets in targets.items():
                key_logits = logits[key]
                key_class_weights = self.class_weights[key]
                key_targets = tf.cast(key_targets, key_logits.dtype)

                # [batch_size,]
                key_cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=key_targets,
                                                                            logits=key_logits,
                                                                            axis=-1)

                if self.weight_predictions:
                    key_weights = tf.ones(shape=key_targets.shape[0], dtype=key_logits.dtype)
                    key_target_classes = tf.argmax(key_targets, axis=-1)
                    for cls, weight in key_class_weights.items():
                        to_fill = tf.cast(tf.fill(key_weights.shape, value=weight), key_logits.dtype)
                        key_weights = tf.where(key_target_classes == cls, to_fill, key_weights)

                    key_cross_entropy *= key_weights

                if cross_entropy is None:
                    cross_entropy = key_cross_entropy
                else:
                    cross_entropy += key_cross_entropy

            cross_entropy = tf.reduce_mean(cross_entropy)
        else:
            targets = tf.cast(targets, logits.dtype)

            # [batch_size]
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                    logits=logits,
                                                                    axis=-1)

            # build weights for unbalanced classification
            if self.weight_predictions:
                weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
                target_classes = tf.argmax(targets, axis=-1)
                for cls, weight in self.class_weights.items():
                    to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                    weights = tf.where(target_classes == cls, to_fill, weights)

                cross_entropy *= weights

            if reduce:
                cross_entropy = tf.reduce_mean(cross_entropy)

        return cross_entropy

    def _parse_predictions(self, raw_predictions):
        predictions = {}
        for key, value in raw_predictions.items():
            predictions[key] = np.argmax(value, axis=-1)

        return predictions


# Memory Networks #


class Basic_Memn2n(ClassificationNetwork):
    """
    Basic version of the end-to-end memory network.
    The model architecture is defined as follows:
        1. Memory Lookup: stack of Memory Lookup layers
        2. Memory Reasoning: stack of Dense layers
        3. Memory reasoning is done only at the end of the sequence of memory lookups.
    """

    def __init__(self, hops, optimizer, optimizer_args, embedding_info,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info, dropout_rate=.4,
                 positional_encoding=False, max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True,
                 l2_regularization=None, answer_weights=(256, 64),
                 accumulate_attention=False, sampling=False,
                 max_memory_size=None, update_rate=30, sampling_mode='uniform',
                 priority_update_mode='loss_gain', alpha=0.7, epsilon=0.01,
                 samplewise_sampling=False,
                 filter_positive_samples=True, **kwargs):

        super(Basic_Memn2n, self).__init__(**kwargs)

        self.hops = hops
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.embedding_info = embedding_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.partial_supervision_info = partial_supervision_info
        self.positional_encoding = positional_encoding
        self.accumulate_attention = accumulate_attention

        self.sampling = sampling
        self.max_memory_size = max_memory_size
        self.update_rate = update_rate
        self.samplewise_sampling = samplewise_sampling
        self.sampling_mode = sampling_mode

        if sampling:
            self.sampler = SamplerFactory.factory('memn2n_sampler',
                                                  max_memory_size=self.max_memory_size,
                                                  sampling_mode=self.sampling_mode,
                                                  update_rate=self.update_rate,
                                                  priority_update_mode=priority_update_mode,
                                                  alpha=alpha,
                                                  epsilon=epsilon,
                                                  samplewise_sampling=samplewise_sampling,
                                                  filter_positive_samples=filter_positive_samples)
        else:
            self.sampler = None

    def get_state(self):
        if self.sampler is not None:
            return self.sampler.kb_priority
        else:
            return None

    def set_state(self, network_state):
        if self.sampler is not None:
            self.sampler.kb_priority = network_state

    def _get_additional_info(self):
        if self.sampler is not None:
            return {
                'kb_priority': tf.convert_to_tensor(self.sampler.kb_priority),
                'acc_scores': tf.convert_to_tensor(self.sampler.acc_scores),
                'acc_counts': tf.convert_to_tensor(self.sampler.acc_counts),
            }

        return {}

    def _update_internal_state(self, model_additional_info, batch_idx, epoch):
        if self.sampler is not None:
            self.sampler.set_priority(model_additional_info)

    def build_model(self, text_info):

        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.kb = text_info['kb'] if 'kb' in text_info else None
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.accumulate_attention = True

        if self.sampler is not None and self.sampling_mode == 'ground_truth':
            self.sampler.kb_priority = text_info['kb_frequencies']

        self.model = M_Basic_Memn2n(query_size=self.max_seq_length,
                                    position_encoding=self.positional_encoding,
                                    sentence_size=self.max_kb_seq_length,
                                    accumulate_attention=self.accumulate_attention,
                                    vocab_size=self.vocab_size,
                                    embedding_dimension=self.embedding_dimension,
                                    hops=self.hops,
                                    embedding_info=self.embedding_info,
                                    answer_weights=self.answer_weights,
                                    dropout_rate=self.dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    memory_lookup_info=self.memory_lookup_info,
                                    extraction_info=self.extraction_info,
                                    reasoning_info=self.reasoning_info,
                                    partial_supervision_info=self.partial_supervision_info,
                                    num_labels=self.num_labels,
                                    num_classes_per_label=self.num_classes_per_label,
                                    kb=self.kb,
                                    sampler=self.sampler)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets,
                                                logits=logits['MANN'],
                                                reduce=False)

        model_additional_info['batch_wise_CE'] = cross_entropy

        red_cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = red_cross_entropy
        loss_info = {
            'CE': red_cross_entropy,
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            # Convex combination of both losses
            total_supervision = supervision_loss * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['SS'] = total_supervision

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

            # Update sampler
            if self.sampler is not None:
                query_only_CE = self._classification_ce(targets=y,
                                                        logits=model_additional_info['query_only_answer'],
                                                        reduce=False)

                sampler_state = self.sampler.update_internal_state(batch_wise_CE=model_additional_info['batch_wise_CE'],
                                                                   sampled_indices=model_additional_info[
                                                                       'sampled_indices'],
                                                                   memory_attention=
                                                                   model_additional_info['memory_attention'][0],
                                                                   additional_info=additional_info,
                                                                   query_only_CE=query_only_CE,
                                                                   labels=y)
                model_additional_info['sampler_state'] = sampler_state

        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, model_additional_info, grads


class Mapper_Memn2n(Basic_Memn2n):
    """
    Basic version of the end-to-end memory network.
    The model architecture is defined as follows:
        1. Memory Lookup: stack of Memory Lookup layers
        2. Memory Reasoning: stack of Dense layers
        3. Memory reasoning is done only at the end of the sequence of memory lookups.
    """

    def __init__(self, mapper_weights, mapper_coefficient, **kwargs):
        super(Mapper_Memn2n, self).__init__(**kwargs)

        self.mapper_weights = mapper_weights
        self.mapper_coefficient = mapper_coefficient

        assert 0. <= self.mapper_coefficient < 1.0

        # We require this for unbiased mapper learning
        assert self.sampling_mode == 'uniform'
        assert self.sampling

    def get_state(self):
        state = {
            'mapper_coefficient': self.mapper_coefficient
        }

        if self.sampler is not None:
            state['kb_priority'] = self.sampler.kb_priority

        return state

    def set_state(self, network_state):
        self.mapper_coefficient = network_state['mapper_coefficient']

        if self.sampler is not None:
            self.sampler.kb_priority = network_state['kb_priority']

    def _get_additional_info(self):
        additional_info = super(Mapper_Memn2n, self)._get_additional_info()
        additional_info['mapper_coefficient'] = self.mapper_coefficient
        return additional_info

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.kb = text_info['kb'] if 'kb' in text_info else None
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.accumulate_attention = True

        if self.sampler is not None and self.sampling_mode == 'ground_truth':
            self.sampler.kb_priority = text_info['kb_frequencies']

        self.model = M_Mapper_Memn2n(query_size=self.max_seq_length,
                                     position_encoding=self.positional_encoding,
                                     sentence_size=self.max_kb_seq_length,
                                     accumulate_attention=self.accumulate_attention,
                                     vocab_size=self.vocab_size,
                                     embedding_dimension=self.embedding_dimension,
                                     hops=self.hops,
                                     embedding_info=self.embedding_info,
                                     answer_weights=self.answer_weights,
                                     dropout_rate=self.dropout_rate,
                                     l2_regularization=self.l2_regularization,
                                     embedding_matrix=text_info['embedding_matrix'],
                                     memory_lookup_info=self.memory_lookup_info,
                                     extraction_info=self.extraction_info,
                                     reasoning_info=self.reasoning_info,
                                     partial_supervision_info=self.partial_supervision_info,
                                     num_labels=self.num_labels,
                                     num_classes_per_label=self.num_classes_per_label,
                                     kb=self.kb,
                                     sampler=self.sampler,
                                     mapper_weights=self.mapper_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets,
                                                logits=logits['MANN'],
                                                reduce=False)

        model_additional_info['batch_wise_CE'] = cross_entropy

        red_cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = red_cross_entropy
        loss_info = {
            'CE': red_cross_entropy,
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            # Convex combination of both losses
            total_supervision = supervision_loss * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['SS'] = total_supervision

        # Mapper Loss
        mse_loss = tf.keras.losses.MeanSquaredError(reduction=tf.keras.losses.Reduction.NONE)
        upd_memory = model_additional_info['upd_memory']  # mapper targets
        target_mask = tf.cast(model_additional_info['target_mask'], tf.float32)
        supervision_flag = tf.minimum(tf.reduce_sum(target_mask, axis=-1), 1.)
        mapper_vector = model_additional_info['mapper_memory_search']

        # We move the mapper accumulator vector to each memory cell weighted by their importance
        # Positive examples importance: SS mask, weighted by attention to focus on similar targets
        # Negative examples importance: Loss gain
        mapper_memory_loss = mse_loss(tf.stop_gradient(upd_memory), mapper_vector[:, None, :])
        mapper_SS_loss = tf.reduce_sum(mapper_memory_loss * target_mask * model_additional_info['probabilities'],
                                       axis=-1)

        # Compute gain
        query_only_CE = self._classification_ce(targets=targets,
                                                logits=model_additional_info['query_only_answer'],
                                                reduce=False)
        gain = query_only_CE - cross_entropy
        gain = tf.clip_by_value(gain, -3.0, 3.0)
        gain = tf.stop_gradient(tf.exp(gain))

        mapper_WS_loss = gain * tf.reduce_sum(mapper_memory_loss * model_additional_info['probabilities'], axis=-1)

        mapper_loss = mapper_SS_loss * supervision_flag + mapper_WS_loss * (1 - supervision_flag)
        mapper_loss = tf.reduce_mean(mapper_loss)

        total_loss += mapper_loss
        loss_info['MSE'] = mapper_loss

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, model_additional_info, grads


@DeprecationWarning
class Gated_Memn2n(Basic_Memn2n):

    def __init__(self, gating_info, **kwargs):
        super(Gated_Memn2n, self).__init__(**kwargs)
        self.gating_info = gating_info

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['max_supervision_padding']
            self.accumulate_attention = True
        else:
            self.padding_amount = None

        self.model = M_Gated_Memn2n(query_size=self.max_seq_length,
                                    position_encoding=self.positional_encoding,
                                    sentence_size=self.max_kb_seq_length,
                                    accumulate_attention=self.accumulate_attention,
                                    vocab_size=self.vocab_size,
                                    embedding_dimension=self.embedding_dimension,
                                    hops=self.hops,
                                    embedding_info=self.embedding_info,
                                    answer_weights=self.answer_weights,
                                    dropout_rate=self.dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    gating_info=self.gating_info,
                                    memory_lookup_info=self.memory_lookup_info,
                                    extraction_info=self.extraction_info,
                                    reasoning_info=self.reasoning_info,
                                    partial_supervision_info=self.partial_supervision_info,
                                    num_labels=self.num_labels,
                                    num_classes_per_label=self.num_classes_per_label,
                                    padding_amount=self.padding_amount)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


@DeprecationWarning
class Res_Memn2n(Basic_Memn2n):

    def __init__(self, gating_info, **kwargs):
        super(Res_Memn2n, self).__init__(**kwargs)
        self.gating_info = gating_info

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['max_supervision_padding']
            self.accumulate_attention = True
        else:
            self.padding_amount = None

        self.model = M_Res_Memn2n(query_size=self.max_seq_length,
                                  position_encoding=self.positional_encoding,
                                  sentence_size=self.max_kb_seq_length,
                                  accumulate_attention=self.accumulate_attention,
                                  vocab_size=self.vocab_size,
                                  embedding_dimension=self.embedding_dimension,
                                  hops=self.hops,
                                  embedding_info=self.embedding_info,
                                  answer_weights=self.answer_weights,
                                  dropout_rate=self.dropout_rate,
                                  l2_regularization=self.l2_regularization,
                                  embedding_matrix=text_info['embedding_matrix'],
                                  gating_info=self.gating_info,
                                  memory_lookup_info=self.memory_lookup_info,
                                  extraction_info=self.extraction_info,
                                  reasoning_info=self.reasoning_info,
                                  partial_supervision_info=self.partial_supervision_info,
                                  num_labels=self.num_labels,
                                  num_classes_per_label=self.num_classes_per_label,
                                  padding_amount=self.padding_amount)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


@DeprecationWarning
class Discriminative_Memn2n(Basic_Memn2n):

    def __init__(self, discriminator_weights=(), confidence_coefficient=1e-3, **kwargs):
        super(Discriminative_Memn2n, self).__init__(hops=1, **kwargs)
        self.discriminator_weights = discriminator_weights
        self.confidence_coefficient = confidence_coefficient

    def build_model(self, text_info):

        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['max_supervision_padding']
            self.accumulate_attention = True
        else:
            self.padding_amount = None

        self.model = M_Discriminative_Memn2n(query_size=self.max_seq_length,
                                             position_encoding=self.positional_encoding,
                                             sentence_size=self.max_kb_seq_length,
                                             accumulate_attention=self.accumulate_attention,
                                             vocab_size=self.vocab_size,
                                             embedding_dimension=self.embedding_dimension,
                                             hops=self.hops,
                                             embedding_info=self.embedding_info,
                                             answer_weights=self.answer_weights,
                                             dropout_rate=self.dropout_rate,
                                             l2_regularization=self.l2_regularization,
                                             embedding_matrix=text_info['embedding_matrix'],
                                             memory_lookup_info=self.memory_lookup_info,
                                             extraction_info=self.extraction_info,
                                             reasoning_info=self.reasoning_info,
                                             partial_supervision_info=self.partial_supervision_info,
                                             output_size=text_info['num_labels'],
                                             padding_amount=self.padding_amount,
                                             discriminator_weights=self.discriminator_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None):
        # query_answer -> [batch_size,]
        # mem_answer -> [batch_size,]
        # discriminator_answer -> [batch_size,]
        (query_answer, mem_answer, discriminator_answer), model_additional_info = self.model(x, training=training)

        query_probs = tf.clip_by_value(tf.nn.sigmoid(query_answer), 1e-10, 1.0)
        mem_probs = tf.clip_by_value(tf.nn.sigmoid(mem_answer), 1e-10, 1.0)

        # Query answer cross-entropy
        query_ce = self._classification_ce(targets=targets, logits=query_answer)
        self.query_ce = tf.reduce_mean(query_ce)
        total_loss = self.query_ce

        # Memory answer cross-entropy
        memory_ce = self._classification_ce(targets=targets, logits=mem_answer)
        memory_weights = self.model.dist.prob(tf.stop_gradient(query_probs))

        self.memory_ce = tf.reduce_mean(memory_ce * memory_weights)
        total_loss += self.memory_ce

        # Discriminator loss
        query_weights = tf.math.divide(query_probs, query_probs + mem_probs)
        query_weights = tf.clip_by_value(query_weights, 1e-10, 1.0)
        discriminator_loss_pos = tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_answer,
                                                                         labels=query_weights,
                                                                         name='discriminator_ce')

        mem_weights = tf.math.divide(mem_probs, query_probs + mem_probs)
        mem_weights = tf.clip_by_value(mem_weights, 1e-10, 1.0)
        discriminator_loss_neg = tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_answer,
                                                                         labels=mem_weights,
                                                                         name='discriminator_ce')

        discriminator_loss = discriminator_loss_pos * targets + discriminator_loss_neg * (1 - targets)

        self.discriminator_loss = tf.reduce_mean(discriminator_loss)
        total_loss += self.discriminator_loss

        # Confidence penalization
        confidence_entropy = tf.nn.sigmoid_cross_entropy_with_logits(logits=query_answer,
                                                                     labels=query_probs)
        self.confidence_loss = -tf.reduce_mean(confidence_entropy) * self.confidence_coefficient
        total_loss += self.confidence_loss

        loss_info = {
            'query_cross_entropy': self.query_ce,
            'memory_cross_entropy': self.memory_ce,
            'discriminator_loss': self.discriminator_loss,
            'confidence_loss': self.confidence_loss
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Masked Supervision by Attention

            # # [batch_size, #hops]
            masked_supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            #
            # # [batch_size, mem_size, #hops]
            # # [batch_size, #hops]
            attentions = tf.stack(model_additional_info['memory_attention'], axis=2)
            attentions = tf.stop_gradient(attentions)
            attentions = tf.reduce_max(attentions, axis=1)
            attentions = tf.round(attentions)
            #
            masked_supervision_loss = tf.reduce_mean(masked_supervision_loss * attentions)

            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            # Convex combination of both losses
            total_supervision = masked_supervision_loss * additional_info['supervision_alpha'] \
                                + (1 - additional_info['supervision_alpha']) * supervision_loss
            total_supervision = total_supervision * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['supervision_loss'] = total_supervision

        return total_loss, loss_info

    def _parse_predictions(self, predictions):
        query_preds, mem_preds, discriminator_preds = predictions
        depth = query_preds.shape[-1]

        query_preds = tf.math.argmax(query_preds, axis=1)
        query_preds = tf.reshape(query_preds, [-1, ])
        mem_preds = tf.math.argmax(mem_preds, axis=1)
        mem_preds = tf.reshape(mem_preds, [-1, ])
        discriminator_preds = tf.math.argmax(discriminator_preds, axis=1)
        discriminator_preds = tf.cast(discriminator_preds, tf.float32)
        discriminator_preds = tf.reshape(discriminator_preds, [-1, ])

        final_predictions = query_preds * discriminator_preds + mem_preds * (1 - discriminator_preds)
        final_predictions = tf.one_hot(final_predictions, depth=depth)

        return final_predictions


@DeprecationWarning
class Windowed_HAN(Network):

    def __init__(self, optimizer, optimizer_args, encoding_size, attention_projection, windowing_info,
                 dropout_rate=.4, positional_encoding=False, max_grad_norm=40.0,
                 clip_gradient=True, add_gradient_noise=True, weight_predictions=True,
                 l2_regularization=None, answer_weights=(256, 64),
                 accumulate_attention=False, **kwargs):

        super(Windowed_HAN, self).__init__(**kwargs)

        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.encoding_size = encoding_size
        self.windowing_info = windowing_info
        self.attention_projection = attention_projection
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.positional_encoding = positional_encoding
        self.accumulate_attention = accumulate_attention

        self.additional_data['categories'] = list(map(lambda x: x.upper(), self.additional_data['categories']))
        self.categories = self.additional_data['categories'] + ['not-unfair']
        self.weight_predictions = weight_predictions

    def build_model(self, text_info):

        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Windowed_HAN(query_size=self.max_seq_length,
                                    position_encoding=self.positional_encoding,
                                    sentence_size=self.max_kb_seq_length,
                                    accumulate_attention=self.accumulate_attention,
                                    vocab_size=self.vocab_size,
                                    embedding_dimension=self.embedding_dimension,
                                    encoding_size=self.encoding_size,
                                    answer_weights=self.answer_weights,
                                    dropout_rate=self.dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    attention_projection=self.attention_projection,
                                    windowing_info=self.windowing_info,
                                    output_size=text_info['weight_predictions'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, additional_info=None):
        logits, _ = self.model(x, training=training)
        targets = tf.cast(targets, dtype=logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        # Cross entropy
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=targets,
                                                                logits=logits)

        # build weights for unbalanced classification
        if self.weight_predictions:
            weights = tf.ones(shape=targets.shape[0], dtype=logits.dtype)
            target_classes = tf.argmax(targets, axis=1)
            for cls, weight in self.class_weights.items():
                to_fill = tf.cast(tf.fill(weights.shape, value=weight), logits.dtype)
                weights = tf.where(target_classes == cls, to_fill, weights)

            cross_entropy *= weights
        cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        return total_loss, loss_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True, additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_predictions(self, predictions):
        depth = predictions.shape[-1]
        predictions = tf.math.argmax(predictions, axis=1)
        predictions = tf.one_hot(predictions, depth=depth)
        return predictions


class DistilBERT(ClassificationNetwork):

    def __init__(self, optimizer_args, preloaded_name,
                 config_args, sentence_embedding_mode='first_token',
                 is_bert_trainable=True,
                 **kwargs):
        super(DistilBERT, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.sentence_embedding_mode = sentence_embedding_mode
        self.preloaded_name = preloaded_name
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = DistilBertConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_DistilBERT(bert_config=self.bert_config,
                                  preloaded_model_name=self.preloaded_name,
                                  num_labels=self.num_labels,
                                  num_classes_per_label=self.num_classes_per_label,
                                  sentence_embedding_mode=self.sentence_embedding_mode,
                                  is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info


class MemoryDistilBert(DistilBERT):

    def __init__(self, memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, sampling=False, max_memory_size=None,
                 sampling_mode='uniform', update_rate=1,
                 priority_update_mode='loss_gain', alpha=0.7, epsilon=0.01,
                 filter_positive_samples=True, samplewise_sampling=False,
                 **kwargs):
        super(MemoryDistilBert, self).__init__(**kwargs)

        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention

        self.sampling = sampling
        self.max_memory_size = max_memory_size
        self.update_rate = update_rate
        self.samplewise_sampling = samplewise_sampling
        self.sampling_mode = sampling_mode

        if sampling:
            self.sampler = SamplerFactory.factory('mem_bert_sampler',
                                                  max_memory_size=max_memory_size,
                                                  sampling_mode=sampling_mode,
                                                  priority_update_mode=priority_update_mode,
                                                  alpha=alpha,
                                                  update_rate=update_rate,
                                                  epsilon=epsilon,
                                                  samplewise_sampling=samplewise_sampling,
                                                  filter_positive_samples=filter_positive_samples)
        else:
            self.sampler = None

    def get_state(self):
        if self.sampler is not None:
            return self.sampler.kb_priority
        else:
            return None

    def set_state(self, network_state):
        self.sampler.kb_priority = network_state

    def _get_additional_info(self):
        if self.sampler is not None:
            return {
                'kb_priority': tf.convert_to_tensor(self.sampler.kb_priority),
                'acc_scores': tf.convert_to_tensor(self.sampler.acc_scores),
                'acc_counts': tf.convert_to_tensor(self.sampler.acc_counts),
            }

        return {}

    def _update_internal_state(self, model_additional_info, batch_idx, epoch):
        if self.sampler is not None:
            self.sampler.set_priority(model_additional_info)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.kb = text_info['kb'] if 'kb' in text_info else None
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.accumulate_attention = True

        if self.sampler is not None and self.sampling_mode == 'ground_truth':
            self.sampler.kb_priority = text_info['kb_frequencies']

        self.model = M_MemoryDistilBERT(preloaded_model_name=self.preloaded_name,
                                        bert_config=self.bert_config,
                                        sentence_size=self.max_kb_seq_length,
                                        query_size=self.max_seq_length,
                                        num_labels=self.num_labels,
                                        num_classes_per_label=self.num_classes_per_label,
                                        memory_lookup_info=self.memory_lookup_info,
                                        reasoning_info=self.reasoning_info,
                                        extraction_info=self.extraction_info,
                                        partial_supervision_info=self.partial_supervision_info,
                                        accumulate_attention=self.accumulate_attention,
                                        kb=self.kb,
                                        sampler=self.sampler,
                                        sentence_embedding_mode=self.sentence_embedding_mode,
                                        is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets,
                                                logits=logits['MANN'],
                                                reduce=False)

        model_additional_info['batch_wise_CE'] = cross_entropy

        red_cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = red_cross_entropy
        loss_info = {
            'CE': red_cross_entropy,
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            total_supervision = supervision_loss * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['SS'] = total_supervision

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

            # Update sampler
            if self.sampler is not None:
                query_only_CE = self._classification_ce(targets=y,
                                                        logits=model_additional_info['query_only_answer'],
                                                        reduce=False)

                sampler_state = self.sampler.update_internal_state(batch_wise_CE=model_additional_info['batch_wise_CE'],
                                                                   sampled_indices=model_additional_info[
                                                                       'sampled_indices'],
                                                                   memory_attention=
                                                                   model_additional_info['memory_attention'][0],
                                                                   additional_info=additional_info,
                                                                   query_only_CE=query_only_CE,
                                                                   labels=y)
                model_additional_info['sampler_state'] = sampler_state

        grads = tape.gradient(loss, self.model.trainable_variables)
        return loss, loss_info, model_additional_info, grads


class DistilRoBERTa(ClassificationNetwork):

    def __init__(self, optimizer_args, preloaded_name,
                 config_args, is_bert_trainable=True, **kwargs):
        super(DistilRoBERTa, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.preloaded_name = preloaded_name
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = RobertaConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_DistilRoBERTa(bert_config=self.bert_config,
                                     preloaded_model_name=self.preloaded_name,
                                     num_labels=self.num_labels,
                                     num_classes_per_label=self.num_classes_per_label,
                                     is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info


class MemoryDistilRoBerta(DistilRoBERTa):

    def __init__(self, memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, sampling=False, max_memory_size=None,
                 sampling_mode='uniform', update_rate=1,
                 priority_update_mode='loss_gain', alpha=0.7, epsilon=0.01,
                 filter_positive_samples=True, samplewise_sampling=False, **kwargs):
        super(MemoryDistilRoBerta, self).__init__(**kwargs)

        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention

        self.sampling = sampling
        self.max_memory_size = max_memory_size
        self.update_rate = update_rate
        self.samplewise_sampling = samplewise_sampling
        self.sampling_mode = sampling_mode

        if sampling:
            self.sampler = SamplerFactory.factory('mem_bert_sampler',
                                                  max_memory_size=max_memory_size,
                                                  sampling_mode=sampling_mode,
                                                  priority_update_mode=priority_update_mode,
                                                  alpha=alpha,
                                                  update_rate=update_rate,
                                                  epsilon=epsilon,
                                                  samplewise_sampling=samplewise_sampling,
                                                  filter_positive_samples=filter_positive_samples)
        else:
            self.sampler = None

    def get_state(self):
        if self.sampler is not None:
            return self.sampler.kb_priority
        else:
            return None

    def set_state(self, network_state):
        if self.sampler is not None:
            self.sampler.kb_priority = network_state

    def _get_additional_info(self):
        if self.sampler is not None:
            return {
                'kb_priority': tf.convert_to_tensor(self.sampler.kb_priority),
                'acc_scores': tf.convert_to_tensor(self.sampler.acc_scores),
                'acc_counts': tf.convert_to_tensor(self.sampler.acc_counts),
            }

        return {}

    def _update_internal_state(self, model_additional_info, batch_idx, epoch):
        if self.sampler is not None:
            self.sampler.set_priority(model_additional_info)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.kb = text_info['kb'] if 'kb' in text_info else None
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.accumulate_attention = True

        if self.sampler is not None and self.sampling_mode == 'ground_truth':
            self.sampler.kb_priority = text_info['kb_frequencies']

        self.model = M_MemoryDistilRoBERTa(preloaded_model_name=self.preloaded_name,
                                           bert_config=self.bert_config,
                                           sentence_size=self.max_kb_seq_length,
                                           query_size=self.max_seq_length,
                                           num_labels=self.num_labels,
                                           num_classes_per_label=self.num_classes_per_label,
                                           memory_lookup_info=self.memory_lookup_info,
                                           reasoning_info=self.reasoning_info,
                                           extraction_info=self.extraction_info,
                                           partial_supervision_info=self.partial_supervision_info,
                                           accumulate_attention=self.accumulate_attention,
                                           kb=self.kb,
                                           sampler=self.sampler,
                                           is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets,
                                                logits=logits['MANN'],
                                                reduce=False)

        model_additional_info['batch_wise_CE'] = cross_entropy

        red_cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = red_cross_entropy
        loss_info = {
            'CE': red_cross_entropy,
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            total_supervision = supervision_loss * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['SS'] = total_supervision

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

            # Update sampler
            if self.sampler is not None:
                query_only_CE = self._classification_ce(targets=y,
                                                        logits=model_additional_info['query_only_answer'],
                                                        reduce=False)

                sampler_state = self.sampler.update_internal_state(batch_wise_CE=model_additional_info['batch_wise_CE'],
                                                                   sampled_indices=model_additional_info[
                                                                       'sampled_indices'],
                                                                   memory_attention=
                                                                   model_additional_info['memory_attention'][0],
                                                                   additional_info=additional_info,
                                                                   query_only_CE=query_only_CE,
                                                                   labels=y)
                model_additional_info['sampler_state'] = sampler_state

        grads = tape.gradient(loss, self.model.trainable_variables)
        return loss, loss_info, model_additional_info, grads


class BERT(ClassificationNetwork):

    def __init__(self, optimizer_args, preloaded_name,
                 config_args, is_bert_trainable=True, **kwargs):
        super(BERT, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.preloaded_name = preloaded_name
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = BertConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_BERT(bert_config=self.bert_config,
                            preloaded_model_name=self.preloaded_name,
                            num_labels=self.num_labels,
                            num_classes_per_label=self.num_classes_per_label,
                            is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info


class MemoryBERT(BERT):

    def __init__(self, memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, sampling=False, max_memory_size=None,
                 sampling_mode='uniform', update_rate=1,
                 priority_update_mode='loss_gain', alpha=0.7, epsilon=0.01,
                 filter_positive_samples=True, samplewise_sampling=False, **kwargs):
        super(MemoryBERT, self).__init__(**kwargs)

        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention

        self.sampling = sampling
        self.max_memory_size = max_memory_size
        self.update_rate = update_rate
        self.samplewise_sampling = samplewise_sampling
        self.sampling_mode = sampling_mode

        if sampling:
            self.sampler = SamplerFactory.factory('mem_bert_sampler',
                                                  max_memory_size=max_memory_size,
                                                  sampling_mode=sampling_mode,
                                                  priority_update_mode=priority_update_mode,
                                                  alpha=alpha,
                                                  update_rate=update_rate,
                                                  epsilon=epsilon,
                                                  samplewise_sampling=samplewise_sampling,
                                                  filter_positive_samples=filter_positive_samples)
        else:
            self.sampler = None

    def get_state(self):
        if self.sampler is not None:
            return self.sampler.kb_priority
        else:
            return None

    def set_state(self, network_state):
        if self.sampler is not None:
            self.sampler.kb_priority = network_state

    def _get_additional_info(self):
        if self.sampler is not None:
            return {
                'kb_priority': tf.convert_to_tensor(self.sampler.kb_priority),
                'acc_scores': tf.convert_to_tensor(self.sampler.acc_scores),
                'acc_counts': tf.convert_to_tensor(self.sampler.acc_counts),
            }

        return {}

    def _update_internal_state(self, model_additional_info, batch_idx, epoch):
        if self.sampler is not None:
            self.sampler.set_priority(model_additional_info)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_kb_seq_length = text_info['max_kb_seq_length']
        self.max_kb_length = text_info['max_kb_length']
        self.kb = text_info['kb'] if 'kb' in text_info else None
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        if self.partial_supervision_info['flag']:
            self.accumulate_attention = True

        if self.sampler is not None and self.sampling_mode == 'ground_truth':
            self.sampler.kb_priority = text_info['kb_frequencies']

        self.model = M_MemoryBERT(preloaded_model_name=self.preloaded_name,
                                  bert_config=self.bert_config,
                                  sentence_size=self.max_kb_seq_length,
                                  query_size=self.max_seq_length,
                                  num_labels=self.num_labels,
                                  num_classes_per_label=self.num_classes_per_label,
                                  memory_lookup_info=self.memory_lookup_info,
                                  reasoning_info=self.reasoning_info,
                                  extraction_info=self.extraction_info,
                                  partial_supervision_info=self.partial_supervision_info,
                                  accumulate_attention=self.accumulate_attention,
                                  kb=self.kb,
                                  sampler=self.sampler,
                                  is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets,
                                                logits=logits['MANN'],
                                                reduce=False)

        model_additional_info['batch_wise_CE'] = cross_entropy

        red_cross_entropy = tf.reduce_mean(cross_entropy)

        total_loss = red_cross_entropy
        loss_info = {
            'CE': red_cross_entropy,
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            # Original Supervision

            # [batch_size, #hops]
            supervision_loss = tf.stack(model_additional_info['supervision_losses'])
            supervision_loss = tf.reduce_mean(supervision_loss)

            total_supervision = supervision_loss * self.partial_supervision_info['coefficient']
            total_loss += total_supervision
            loss_info['SS'] = total_supervision

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

            # Update sampler
            if self.sampler is not None:
                query_only_CE = self._classification_ce(targets=y,
                                                        logits=model_additional_info['query_only_answer'],
                                                        reduce=False)

                sampler_state = self.sampler.update_internal_state(batch_wise_CE=model_additional_info['batch_wise_CE'],
                                                                   sampled_indices=model_additional_info[
                                                                       'sampled_indices'],
                                                                   memory_attention=
                                                                   model_additional_info['memory_attention'][0],
                                                                   additional_info=additional_info,
                                                                   query_only_CE=query_only_CE,
                                                                   labels=y)
                model_additional_info['sampler_state'] = sampler_state

        grads = tape.gradient(loss, self.model.trainable_variables)
        return loss, loss_info, model_additional_info, grads


class RationaleCNN(Network):

    def __init__(self, optimizer, optimizer_args, ngram_filters=[3, 4, 5], n_filters=32,
                 sent_dropout=0.5, doc_dropout=0.5, l2_regularization=None, weight_predictions=True,
                 max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True, **kwargs):
        super(RationaleCNN, self).__init__(**kwargs)

        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.ngram_filters = ngram_filters
        self.n_filters = n_filters
        self.sent_dropout = sent_dropout
        self.doc_dropout = doc_dropout
        self.l2_regularization = l2_regularization
        self.weight_predictions = weight_predictions
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise

        self.sentence_coefficient = 1.
        self.document_coefficient = 1.

    def build_model(self, text_info):

        self.doc_size = text_info['max_doc_length']
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_RationaleCNN(doc_size=self.doc_size,
                                    sentence_size=self.sentence_size,
                                    ngram_filters=self.ngram_filters,
                                    n_filters=self.n_filters,
                                    sent_dropout=self.sent_dropout,
                                    doc_dropout=self.doc_dropout,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    embedding_dimension=self.embedding_dimension,
                                    vocab_size=self.vocab_size)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _get_additional_info(self):
        return {
            'sentence_coefficient': tf.convert_to_tensor(self.sentence_coefficient),
            'document_coefficient': tf.convert_to_tensor(self.document_coefficient)
        }

    def loss_op(self, x, targets, training=False, state='training', additional_info=None):
        doc_logits, model_additional_info = self.model(x, training=training, state=state)
        sent_logits = model_additional_info['sent_logits']

        doc_targets = targets
        sent_targets = x['sentence_targets']
        sent_targets = tf.reshape(sent_targets, sent_logits.shape)

        sentence_coefficient = additional_info['sentence_coefficient']
        document_coefficient = additional_info['document_coefficient']

        # Sentence level
        sent_ce = tf.nn.softmax_cross_entropy_with_logits(labels=sent_targets, logits=sent_logits)
        sent_ce = tf.reduce_mean(sent_ce)
        sent_ce *= sentence_coefficient

        total_loss = sent_ce
        loss_info = {
            'sentence_ce': sent_ce
        }

        # Doc level
        doc_ce = tf.nn.softmax_cross_entropy_with_logits(labels=doc_targets, logits=doc_logits)

        # build weights for unbalanced classification
        if self.weight_predictions:
            weights = tf.ones(shape=targets.shape[0], dtype=doc_logits.dtype)
            target_classes = tf.argmax(targets, axis=1)
            for cls, weight in self.class_weights.items():
                to_fill = tf.cast(tf.fill(weights.shape, value=weight), doc_logits.dtype)
                weights = tf.where(target_classes == cls, to_fill, weights)

            doc_ce *= weights

            doc_ce = tf.reduce_mean(doc_ce)
            doc_ce *= document_coefficient

            total_loss += doc_ce
            loss_info['doc_ce'] = doc_ce

            # L2 regularization
            if self.model.losses:
                additional_losses = tf.reduce_sum(self.model.losses)
                total_loss += additional_losses
                loss_info['l2_regularization'] = additional_losses

            return total_loss, loss_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True, state='training', additional_info=additional_info)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_predictions(self, predictions):
        depth = predictions.shape[-1]
        predictions = tf.math.argmax(predictions, axis=1)
        predictions = tf.one_hot(predictions, depth=depth)
        return predictions


class SquadMANN(Network):

    def __init__(self, hops, optimizer, optimizer_args,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info, dropout_rate=.4,
                 positional_encoding=False, max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True,
                 l2_regularization=None, answer_weights=(256, 64),
                 accumulate_attention=False, **kwargs):

        super(SquadMANN, self).__init__(**kwargs)

        self.hops = hops
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.partial_supervision_info = partial_supervision_info
        self.positional_encoding = positional_encoding
        self.accumulate_attention = accumulate_attention

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_context_length = text_info['max_context_length']
        self.vocab_size = text_info['vocab_size']
        self.tokenizer = text_info['tokenizer']

        self.model = M_SquadMANN(max_seq_length=self.max_seq_length,
                                 position_encoding=self.positional_encoding,
                                 max_context_length=self.max_context_length,
                                 accumulate_attention=self.accumulate_attention,
                                 vocab_size=self.vocab_size,
                                 embedding_dimension=self.embedding_dimension,
                                 hops=self.hops,
                                 answer_weights=self.answer_weights,
                                 dropout_rate=self.dropout_rate,
                                 l2_regularization=self.l2_regularization,
                                 embedding_matrix=text_info['embedding_matrix'],
                                 memory_lookup_info=self.memory_lookup_info,
                                 extraction_info=self.extraction_info,
                                 reasoning_info=self.reasoning_info,
                                 partial_supervision_info=self.partial_supervision_info)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        scce = tf.keras.losses.SparseCategoricalCrossentropy()

        # Classification CE for answer start
        pred_start = logits['answer_start_probs']

        gold_start = targets['answer_starts']
        gold_start = tf.strings.split(gold_start, sep='<DIV>')
        gold_start = gold_start[:, :1].to_tensor(shape=[gold_start.shape[0], 1])
        gold_start = tf.reshape(tf.strings.to_number(gold_start), [pred_start.shape[0], -1])
        # gold_start = tf.squeeze(tf.strings.to_number(gold_start))
        # gold_start = tf.one_hot(gold_start, self.max_context_length)

        # [batch_size]
        start_CE = scce(gold_start, pred_start)
        # start_CE = tf.reduce_mean(start_CE)

        # Classification CE for answer end
        pred_end = logits['answer_end_probs']

        gold_end = targets['answer_ends']
        gold_end = tf.strings.split(gold_end, sep='<DIV>')
        gold_end = gold_end[:, :1].to_tensor(shape=[gold_end.shape[0], 1])
        gold_end = tf.reshape(tf.strings.to_number(gold_end), [pred_end.shape[0], -1])
        # gold_end = tf.squeeze(tf.strings.to_number(gold_end))
        # gold_end = tf.one_hot(gold_end, self.max_context_length)

        # [batch_size]
        end_CE = scce(gold_end, pred_end)
        # end_CE = tf.reduce_mean(end_CE)

        total_loss = start_CE + end_CE
        loss_info = {
            'start_CE': start_CE,
            'end_CE': end_CE
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def train_op(self, x, y, additional_info):
        with tf.GradientTape() as tape:
            loss, loss_info, model_additional_info = self.loss_op(x, y,
                                                                  training=True,
                                                                  state='training',
                                                                  additional_info=additional_info)

        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, model_additional_info, grads

    def _parse_predictions(self, raw_predictions):
        answer_start_probs = raw_predictions['answer_start_probs'].numpy()
        start_indexes = np.argmax(answer_start_probs, axis=-1)

        end_mask = np.arange(self.max_context_length) >= start_indexes.reshape(-1, 1)
        end_mask = end_mask.astype(answer_start_probs.dtype)
        answer_end_probs = raw_predictions['answer_end_probs'].numpy() * end_mask
        end_indexes = np.argmax(answer_end_probs, axis=-1)

        # Get text
        memory = raw_predictions['memory'].numpy()
        predicted_ids = [mem[start_idx:end_idx + 1] for mem, start_idx, end_idx in zip(memory,
                                                                                       start_indexes,
                                                                                       end_indexes)]
        predicted_text = self.tokenizer.convert_ids_to_tokens(predicted_ids)

        return {
            'answer_starts': start_indexes,
            'answer_ends': end_indexes,
            'answer_texts': predicted_text
        }


class BiDAF(Network):

    def __init__(self, optimizer, optimizer_args, token_encoder_units, output_encoder_units,
                 dropout_rate=0.1, **kwargs):
        super(BiDAF, self).__init__(**kwargs)
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.token_encoder_units = token_encoder_units
        self.output_encoder_units = output_encoder_units
        self.dropout_rate = dropout_rate

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_context_length = text_info['max_context_length']
        self.vocab_size = text_info['vocab_size']
        self.tokenizer = text_info['tokenizer']

        self.model = M_BiDAF(max_seq_length=self.max_seq_length,
                             vocab_size=self.vocab_size,
                             max_context_length=self.max_context_length,
                             token_encoder_units=self.token_encoder_units,
                             embedding_dimension=self.embedding_dimension,
                             output_encoder_units=self.output_encoder_units,
                             dropout_rate=self.dropout_rate,
                             embedding_matrix=text_info['embedding_matrix'])

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        scce = tf.keras.losses.SparseCategoricalCrossentropy()

        # Classification CE for answer start
        pred_start = logits['answer_start_probs']

        gold_start = targets['answer_starts']
        gold_start = tf.strings.split(gold_start, sep='<DIV>')
        gold_start = gold_start[:, :1].to_tensor(shape=[gold_start.shape[0], 1])
        gold_start = tf.reshape(tf.strings.to_number(gold_start), [pred_start.shape[0], -1])
        # gold_start = tf.squeeze(tf.strings.to_number(gold_start))
        # gold_start = tf.one_hot(gold_start, self.max_context_length)

        # [batch_size]
        start_CE = scce(gold_start, pred_start)
        # start_CE = tf.reduce_mean(start_CE)

        # Classification CE for answer end
        pred_end = logits['answer_end_probs']

        gold_end = targets['answer_ends']
        gold_end = tf.strings.split(gold_end, sep='<DIV>')
        gold_end = gold_end[:, :1].to_tensor(shape=[gold_end.shape[0], 1])
        gold_end = tf.reshape(tf.strings.to_number(gold_end), [pred_end.shape[0], -1])
        # gold_end = tf.squeeze(tf.strings.to_number(gold_end))
        # gold_end = tf.one_hot(gold_end, self.max_context_length)

        # [batch_size]
        end_CE = scce(gold_end, pred_end)
        # end_CE = tf.reduce_mean(end_CE)

        total_loss = start_CE + end_CE
        loss_info = {
            'start_CE': start_CE,
            'end_CE': end_CE
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def _parse_predictions(self, raw_predictions):
        answer_start_probs = raw_predictions['answer_start_probs'].numpy()
        start_indexes = np.argmax(answer_start_probs, axis=-1)

        end_mask = np.arange(self.max_context_length) >= start_indexes.reshape(-1, 1)
        end_mask = end_mask.astype(answer_start_probs.dtype)
        answer_end_probs = raw_predictions['answer_end_probs'].numpy() * end_mask
        end_indexes = np.argmax(answer_end_probs, axis=-1)

        # Get text
        memory = raw_predictions['context'].numpy()
        predicted_ids = [mem[start_idx:end_idx + 1] for mem, start_idx, end_idx in zip(memory,
                                                                                       start_indexes,
                                                                                       end_indexes)]
        predicted_text = self.tokenizer.convert_ids_to_tokens(predicted_ids)

        return {
            'answer_starts': start_indexes,
            'answer_ends': end_indexes,
            'answer_texts': predicted_text
        }


class SquadDistilBert(Network):

    def __init__(self, optimizer_args, preloaded_name, config_args,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(SquadDistilBert, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.preloaded_name = preloaded_name
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = DistilBertConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_context_length = text_info['max_context_length']
        self.tokenizer = text_info['tokenizer']

        self.model = M_SquadDistilBert(bert_config=self.bert_config,
                                       preloaded_model_name=self.preloaded_name,
                                       max_seq_length=self.max_seq_length,
                                       max_context_length=self.max_context_length,
                                       accumulate_attention=self.accumulate_attention,
                                       memory_lookup_info=self.memory_lookup_info,
                                       extraction_info=self.extraction_info,
                                       reasoning_info=self.reasoning_info,
                                       partial_supervision_info=self.partial_supervision_info,
                                       is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        scce = tf.keras.losses.SparseCategoricalCrossentropy()

        # Classification CE for answer start
        gold_start = targets['answer_starts']
        gold_start = tf.strings.split(gold_start, sep='<DIV>')
        gold_start = gold_start[:, :1].to_tensor(shape=[gold_start.shape[0], 1])
        gold_start = tf.squeeze(tf.strings.to_number(gold_start))
        # gold_start = tf.one_hot(gold_start, self.max_context_length)

        pred_start = logits['answer_start_probs']

        # [batch_size]
        start_CE = scce(gold_start, pred_start)
        # start_CE = tf.reduce_mean(start_CE)

        # Classification CE for answer end

        gold_end = targets['answer_ends']
        gold_end = tf.strings.split(gold_end, sep='<DIV>')
        gold_end = gold_end[:, :1].to_tensor(shape=[gold_end.shape[0], 1])
        gold_end = tf.squeeze(tf.strings.to_number(gold_end))
        # gold_end = tf.one_hot(gold_end, self.max_context_length)

        pred_end = logits['answer_end_probs']

        # [batch_size]
        end_CE = scce(gold_end, pred_end)
        # end_CE = tf.reduce_mean(end_CE)

        total_loss = start_CE + end_CE
        loss_info = {
            'start_CE': start_CE,
            'end_CE': end_CE
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def _parse_predictions(self, raw_predictions):
        answer_start_probs = raw_predictions['answer_start_probs'].numpy()
        start_indexes = np.argmax(answer_start_probs, axis=-1)

        end_mask = np.arange(self.max_context_length) >= start_indexes.reshape(-1, 1)
        end_mask = end_mask.astype(answer_start_probs.dtype)
        answer_end_probs = raw_predictions['answer_end_probs'].numpy() * end_mask
        end_indexes = np.argmax(answer_end_probs, axis=-1)

        # Get text
        memory = raw_predictions['memory'].numpy()
        predicted_ids = [mem[start_idx:end_idx + 1] for mem, start_idx, end_idx in zip(memory,
                                                                                       start_indexes,
                                                                                       end_indexes)]
        predicted_tokens = [self.tokenizer.tokenizer.convert_ids_to_tokens(ids) for ids in predicted_ids]
        # We make sure we do not eliminate CLS token for unanswerable questions
        predicted_text = [self.tokenizer.tokenizer.convert_tokens_to_string(toks)
                          if toks != [self.tokenizer.sep_token] else self.tokenizer.sep_token
                          for toks in predicted_tokens]

        return {
            'answer_starts': start_indexes,
            'answer_ends': end_indexes,
            'answer_texts': predicted_text
        }


class SquadDistilRoBerta(Network):

    def __init__(self, optimizer_args, preloaded_name, config_args,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(SquadDistilRoBerta, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.preloaded_name = preloaded_name
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = RobertaConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_context_length = text_info['max_context_length']
        self.tokenizer = text_info['tokenizer']

        self.model = M_SquadDistilRoBerta(bert_config=self.bert_config,
                                          preloaded_model_name=self.preloaded_name,
                                          max_seq_length=self.max_seq_length,
                                          max_context_length=self.max_context_length,
                                          accumulate_attention=self.accumulate_attention,
                                          memory_lookup_info=self.memory_lookup_info,
                                          extraction_info=self.extraction_info,
                                          reasoning_info=self.reasoning_info,
                                          partial_supervision_info=self.partial_supervision_info,
                                          is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        scce = tf.keras.losses.SparseCategoricalCrossentropy()

        # Classification CE for answer start
        gold_start = targets['answer_starts']
        gold_start = tf.strings.split(gold_start, sep='<DIV>')
        gold_start = gold_start[:, :1].to_tensor(shape=[gold_start.shape[0], 1])
        gold_start = tf.squeeze(tf.strings.to_number(gold_start))
        # gold_start = tf.one_hot(gold_start, self.max_context_length)

        pred_start = logits['answer_start_probs']

        # [batch_size]
        start_CE = scce(gold_start, pred_start)
        # start_CE = tf.reduce_mean(start_CE)

        # Classification CE for answer end

        gold_end = targets['answer_ends']
        gold_end = tf.strings.split(gold_end, sep='<DIV>')
        gold_end = gold_end[:, :1].to_tensor(shape=[gold_end.shape[0], 1])
        gold_end = tf.squeeze(tf.strings.to_number(gold_end))
        # gold_end = tf.one_hot(gold_end, self.max_context_length)

        pred_end = logits['answer_end_probs']

        # [batch_size]
        end_CE = scce(gold_end, pred_end)
        # end_CE = tf.reduce_mean(end_CE)

        total_loss = start_CE + end_CE
        loss_info = {
            'start_CE': start_CE,
            'end_CE': end_CE
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def _parse_predictions(self, raw_predictions):
        answer_start_probs = raw_predictions['answer_start_probs'].numpy()
        start_indexes = np.argmax(answer_start_probs, axis=-1)

        end_mask = np.arange(self.max_context_length) >= start_indexes.reshape(-1, 1)
        end_mask = end_mask.astype(answer_start_probs.dtype)
        answer_end_probs = raw_predictions['answer_end_probs'].numpy() * end_mask
        end_indexes = np.argmax(answer_end_probs, axis=-1)

        # Get text
        memory = raw_predictions['memory'].numpy()
        predicted_ids = [mem[start_idx:end_idx + 1] for mem, start_idx, end_idx in zip(memory,
                                                                                       start_indexes,
                                                                                       end_indexes)]
        predicted_tokens = [self.tokenizer.tokenizer.convert_ids_to_tokens(ids) for ids in predicted_ids]
        # We make sure we do not eliminate CLS token for unanswerable questions
        predicted_text = [self.tokenizer.tokenizer.convert_tokens_to_string(toks)
                          if toks != [self.tokenizer.sep_token] else self.tokenizer.sep_token
                          for toks in predicted_tokens]

        return {
            'answer_starts': start_indexes,
            'answer_ends': end_indexes,
            'answer_texts': predicted_text
        }


class SquadBERT(Network):

    def __init__(self, optimizer_args, preloaded_name, config_args,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(SquadBERT, self).__init__(embedding_dimension=768, **kwargs)
        self.optimizer_args = optimizer_args
        self.preloaded_name = preloaded_name
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.is_bert_trainable = is_bert_trainable

        self.bert_config = BertConfig.from_pretrained(pretrained_model_name_or_path=preloaded_name)
        # Over-writing config values if required
        for key, value in config_args.items():
            setattr(self.bert_config, key, value)

    def build_model(self, text_info):
        self.max_seq_length = text_info['max_seq_length']
        self.max_context_length = text_info['max_context_length']
        self.tokenizer = text_info['tokenizer']

        self.model = M_SquadBERT(bert_config=self.bert_config,
                                 preloaded_model_name=self.preloaded_name,
                                 max_seq_length=self.max_seq_length,
                                 max_context_length=self.max_context_length,
                                 accumulate_attention=self.accumulate_attention,
                                 memory_lookup_info=self.memory_lookup_info,
                                 extraction_info=self.extraction_info,
                                 reasoning_info=self.reasoning_info,
                                 partial_supervision_info=self.partial_supervision_info,
                                 is_bert_trainable=self.is_bert_trainable)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x,
                                                   training=training,
                                                   state=state,
                                                   additional_info=additional_info)

        scce = tf.keras.losses.SparseCategoricalCrossentropy()

        # Classification CE for answer start
        gold_start = targets['answer_starts']
        gold_start = tf.strings.split(gold_start, sep='<DIV>')
        gold_start = gold_start[:, :1].to_tensor(shape=[gold_start.shape[0], 1])
        gold_start = tf.squeeze(tf.strings.to_number(gold_start))
        # gold_start = tf.one_hot(gold_start, self.max_context_length)

        pred_start = logits['answer_start_probs']

        # [batch_size]
        start_CE = scce(gold_start, pred_start)
        # start_CE = tf.reduce_mean(start_CE)

        # Classification CE for answer end

        gold_end = targets['answer_ends']
        gold_end = tf.strings.split(gold_end, sep='<DIV>')
        gold_end = gold_end[:, :1].to_tensor(shape=[gold_end.shape[0], 1])
        gold_end = tf.squeeze(tf.strings.to_number(gold_end))
        # gold_end = tf.one_hot(gold_end, self.max_context_length)

        pred_end = logits['answer_end_probs']

        # [batch_size]
        end_CE = scce(gold_end, pred_end)
        # end_CE = tf.reduce_mean(end_CE)

        total_loss = start_CE + end_CE
        loss_info = {
            'start_CE': start_CE,
            'end_CE': end_CE
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def _parse_predictions(self, raw_predictions):
        answer_start_probs = raw_predictions['answer_start_probs'].numpy()
        start_indexes = np.argmax(answer_start_probs, axis=-1)

        end_mask = np.arange(self.max_context_length) >= start_indexes.reshape(-1, 1)
        end_mask = end_mask.astype(answer_start_probs.dtype)
        answer_end_probs = raw_predictions['answer_end_probs'].numpy() * end_mask
        end_indexes = np.argmax(answer_end_probs, axis=-1)

        # Get text
        memory = raw_predictions['memory'].numpy()
        predicted_ids = [mem[start_idx:end_idx + 1] for mem, start_idx, end_idx in zip(memory,
                                                                                       start_indexes,
                                                                                       end_indexes)]
        predicted_tokens = [self.tokenizer.tokenizer.convert_ids_to_tokens(ids) for ids in predicted_ids]
        # We make sure we do not eliminate CLS token for unanswerable questions
        predicted_text = [self.tokenizer.tokenizer.convert_tokens_to_string(toks)
                          if toks != [self.tokenizer.sep_token] else self.tokenizer.sep_token
                          for toks in predicted_tokens]

        return {
            'answer_starts': start_indexes,
            'answer_ends': end_indexes,
            'answer_texts': predicted_text
        }


# Baselines #

class Baseline_LSTM(ClassificationNetwork):

    def __init__(self, lstm_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_LSTM, self).__init__(**kwargs)
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_Baseline_LSTM(sentence_size=self.sentence_size,
                                     vocab_size=self.vocab_size,
                                     lstm_weights=self.lstm_weights,
                                     answer_weights=self.answer_weights,
                                     embedding_dimension=self.embedding_dimension,
                                     l2_regularization=self.l2_regularization,
                                     dropout_rate=self.dropout_rate,
                                     embedding_matrix=text_info['embedding_matrix'],
                                     num_labels=self.num_labels,
                                     num_classes_per_label=self.num_classes_per_label)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info


class Baseline_CNN(ClassificationNetwork):

    def __init__(self, cnn_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_CNN, self).__init__(**kwargs)
        self.cnn_weights = cnn_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_Baseline_CNN(sentence_size=self.sentence_size,
                                    vocab_size=self.vocab_size,
                                    cnn_weights=self.cnn_weights,
                                    answer_weights=self.answer_weights,
                                    embedding_dimension=self.embedding_dimension,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    num_labels=self.num_labels,
                                    num_classes_per_label=self.num_classes_per_label)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info


class Baseline_Sum(ClassificationNetwork):

    def __init__(self, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_Sum, self).__init__(**kwargs)
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def loss_op(self, x, targets, training=False, state='training', additional_info=None, return_predictions=False):
        logits, model_additional_info = self.model(x, training=training)

        # Cross entropy
        cross_entropy = self._classification_ce(targets=targets, logits=logits['Q_Only'])
        total_loss = cross_entropy
        loss_info = {
            'CE': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['L2'] = additional_losses

        if return_predictions:
            return total_loss, loss_info, logits, model_additional_info

        return total_loss, loss_info, model_additional_info

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_Baseline_Sum(sentence_size=self.sentence_size,
                                    vocab_size=self.vocab_size,
                                    answer_weights=self.answer_weights,
                                    embedding_dimension=self.embedding_dimension,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    num_labels=self.num_labels,
                                    num_classes_per_label=self.num_classes_per_label)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


# TODO: add example ID in PairwiseTextFeatures, then average predictions with the same example ID
# TODO: merge labels as well to compute correct metrics!
class Pairwise_Baseline_Sum(Baseline_Sum):

    def build_model(self, text_info):
        self.sentence_size = text_info['max_seq_length']
        self.vocab_size = text_info['vocab_size']
        self.num_labels = text_info['num_labels']
        self.num_classes_per_label = text_info['num_classes_per_label']

        self.model = M_Pairwise_Baseline_Sum(sentence_size=self.sentence_size,
                                             vocab_size=self.vocab_size,
                                             answer_weights=self.answer_weights,
                                             embedding_dimension=self.embedding_dimension,
                                             l2_regularization=self.l2_regularization,
                                             dropout_rate=self.dropout_rate,
                                             embedding_matrix=text_info['embedding_matrix'],
                                             num_labels=self.num_labels,
                                             num_classes_per_label=self.num_classes_per_label)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_predictions(self, raw_predictions):
        example_ids = raw_predictions['example_id']
        predictions = raw_predictions['Q_Only']

        # Trick to pass both outputs
        predictions = np.hstack((example_ids[:, None], predictions))

        return predictions


class ModelFactory(object):
    supported_models = {
        # ToS
        'experimental_basic_memn2n_v2': Basic_Memn2n,
        'experimental_baseline_lstm_v2': Baseline_LSTM,
        'experimental_baseline_cnn_v2': Baseline_CNN,
        'experimental_gated_memn2n_v2': Gated_Memn2n,
        'experimental_discriminative_memn2n_v2': Discriminative_Memn2n,
        'experimental_windowed_HAN_v2': Windowed_HAN,
        'experimental_res_memn2n_v2': Res_Memn2n,
        'experimental_baseline_sum_v2': Baseline_Sum,
        'experimental_mapper_memn2n_v2': Mapper_Memn2n,

        'experimental_pairwise_baseline_sum_v2': Pairwise_Baseline_Sum,

        'experimental_multi_basic_memn2n_v2': Basic_Memn2n,

        'rationale_cnn': RationaleCNN,

        'distilbert-base-uncased': DistilBERT,
        'memory-distilbert-base-uncased': MemoryDistilBert,

        'distilroberta-base': DistilRoBERTa,
        'memory-distilroberta-base': MemoryDistilRoBerta,

        'bert-base-uncased': BERT,
        'memory-bert-base-uncased': MemoryBERT,

        'multi-distilbert-base-uncased': DistilBERT,
        'multi-memory-distilbert-base-uncased': MemoryDistilBert,

        'experimental_multi_baseline_lstm_v2': Baseline_LSTM,
        'experimental_multi_baseline_cnn_v2': Baseline_CNN,

        # IBM2015
        'ibm2015_experimental_basic_memn2n_v2': Basic_Memn2n,
        'ibm2015_experimental_baseline_lstm_v2': Baseline_LSTM,
        'ibm2015_experimental_baseline_cnn_v2': Baseline_CNN,
        'ibm2015_experimental_mapper_memn2n_v2': Mapper_Memn2n,

        'ibm2015_distilbert-base-uncased': DistilBERT,
        'ibm2015_memory-distilbert-base-uncased': MemoryDistilBert,

        'ibm2015_distilroberta-base': DistilRoBERTa,
        "ibm2015_memory-distilroberta-base": MemoryDistilRoBerta,

        'ibm2015_bert-base-uncased': BERT,
        "ibm2015_memory-bert-base-uncased": MemoryBERT,

        # SQUAD
        'squad_mann': SquadMANN,

        'squad_distilbert-base-uncased': SquadDistilBert,
        'squad_distilroberta-base': SquadDistilRoBerta,
        'squad_bert-base-uncased': SquadBERT,

        'bidaf': BiDAF,

        # SQUAD 2.0
        'squad2_mann': SquadMANN,

        'squad2_distilbert-base-uncased': SquadDistilBert,
        'squad2_distilroberta-base': SquadDistilRoBerta,
        'squad2_bert-base-uncased': SquadBERT,

        'bidaf2': BiDAF,

    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ModelFactory.supported_models[key]:
            return ModelFactory.supported_models[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
