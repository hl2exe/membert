import os

import numpy as np
import tensorflow as tf

import const_define as cd
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import cross_validation_complete_forward
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.python_utils import merge
from utility.printing_utils import prettify_statistics

logger = get_logger(__name__)

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.run_functions_eagerly(True)

    # Inputs

    model_type = "experimental_basic_memn2n_v2"
    test_name = "ToS-30_A_WS_LG-F-5"
    save_scores = False

    model_replace_args = {
        'max_memory_size': {
            'value': 8,
            "flags": ["model_class"]
        }
    }

    model_path = os.path.join(cd.CV_DIR, model_type, test_name)

    if not os.path.isdir(model_path):
        raise RuntimeError('Could not find test folder. Got: {}'.format(model_path))

    cv_test_config = load_json(os.path.join(model_path, cd.JSON_CV_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = cv_test_config['gpu_start_index']
            gpu_end_index = cv_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

    for key, value in model_replace_args.items():
        model_config[key] = value

    training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Distributed info
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    callbacks = []

    model_routine_args = {key: value['value'] for key, value in model_config.items()
                          if 'routine' in value['flags']}

    scores = cross_validation_complete_forward(validation_percentage=cv_test_config['validation_percentage'],
                                               data_handle=data_handle,
                                               network_args=model_config,
                                               data_loader_info=data_loader_info,
                                               model_type=cv_test_config['model_type'],
                                               error_metrics=cv_test_config['error_metrics'],
                                               error_metrics_additional_info=cv_test_config[
                                                   'error_metrics_additional_info'],
                                               error_metrics_nicknames=cv_test_config['error_metrics_nicknames'],
                                               training_config=training_config,
                                               cv=cv,
                                               test_path=model_path,
                                               callbacks=callbacks,
                                               save_predictions=True,
                                               repetitions=cv_test_config['repetitions'],
                                               seeds=cv_test_config['seeds'],
                                               split_key=cv_test_config['split_key'],
                                               distributed_info=distributed_info,
                                               **model_routine_args)

    # Validation
    if cv_test_config['repetitions'] > 1:
        validation_scores = {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if key.startswith('avg')}
        logger.info('Average validation scores: \n{}'.format(prettify_statistics(validation_scores)))
    else:
        validation_scores = {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if
             not key.startswith('avg')}
        logger.info('Average validation scores: \n{}'.format(prettify_statistics(validation_scores)))

    # Test
    if cv_test_config['repetitions'] > 1:
        test_scores = {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if key.startswith('avg')}
        logger.info('Average test scores: \n{}'.format(prettify_statistics(test_scores)))
    else:
        test_scores = {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if not key.startswith('avg')}
        logger.info('Average test scores: \n{}'.format(prettify_statistics(test_scores)))

    if save_scores:
        # Validation
        save_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])

        # Test
        save_json(os.path.join(model_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])

        # Predictions
        save_json(os.path.join(model_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])
