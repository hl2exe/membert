"""

@Author: Federico Ruggeri

@Date: 22/07/2019

[Repeated] Cross-validation routine.

** BEFORE RUNNING **

1. Check the following configuration files:
    distributed_config.json, callbacks.json, data_loader.json, model_config.json, training_config.json

"""

import os
from datetime import datetime


import numpy as np
import tensorflow as tf

import const_define as cd
from custom_callbacks_v2 import EarlyStopping, TrainingLogger, SamplerWeightsRetriever, MapperController
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import cross_validation
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.python_utils import merge
from utility.printing_utils import prettify_statistics
from copy import deepcopy
from nn_models_v2 import ModelFactory

logger = get_logger(__name__)

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.run_functions_eagerly(True)

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = cv_test_config['gpu_start_index']
            gpu_end_index = cv_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    if cv_test_config['pre_loaded']:
        network_class = ModelFactory.supported_models[cv_test_config['model_type']]
        if cv_test_config['load_externally']:
            local_model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
                cv_test_config['model_type']]

            if 'preloaded_name' in local_model_config:
                preloaded_name = local_model_config['preloaded_name']['value']
            else:
                preloaded_name = cv_test_config['model_type']

            ext_model_config = network_class.from_pretrained_config(preloaded_name)
            model_config = merge(local_model_config, ext_model_config)
        else:
            model_config = load_json(os.path.join(cd.CV_DIR,
                                                  cv_test_config['pre_loaded_model'],
                                                  cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))
    else:
        model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
            cv_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Distributed info
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    if cv_test_config['pre_loaded_model'] is None:
        current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
        save_base_path = os.path.join(cd.CV_DIR, cv_test_config['model_type'], current_date)

        if cv_test_config['save_model']:
            os.makedirs(save_base_path)
    else:
        save_base_path = os.path.join(cd.CV_DIR, cv_test_config['model_type'], cv_test_config['pre_loaded_model'])
        if not os.path.isdir(save_base_path):
            msg = "Can't find given pre-trained model. Got: {}".format(save_base_path)
            logger.exception(msg)
            raise RuntimeError(msg)

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    training_logger = TrainingLogger(filepath=save_base_path, suffix=None, save_model=cv_test_config['save_model'])
    sampler_retriever = SamplerWeightsRetriever(save_path=save_base_path,
                                                save_suffix=None,
                                                **callbacks_data['samplerweightsretriever'])
    mapper_controller = MapperController(**callbacks_data['mappercontroller'])
    callbacks = [
        early_stopper,
        mapper_controller
    ]

    if cv_test_config['save_model']:
        callbacks.append(training_logger)
        # callbacks.append(sampler_retriever)

    model_routine_args = {key: value['value'] for key, value in model_config.items()
                          if 'routine' in value['flags']}

    if cv_test_config['seeds'] is None:
        cv_test_config['seeds'] = np.random.randint(low=1, high=100000, size=cv_test_config['repetitions'])

    scores = cross_validation(validation_percentage=cv_test_config['validation_percentage'],
                              data_handle=data_handle,
                              network_args=deepcopy(model_config),
                              data_loader_info=data_loader_info,
                              model_type=cv_test_config['model_type'],
                              error_metrics=cv_test_config['error_metrics'],
                              error_metrics_additional_info=cv_test_config['error_metrics_additional_info'],
                              error_metrics_nicknames=cv_test_config['error_metrics_nicknames'],
                              training_config=training_config,
                              repetitions=cv_test_config['repetitions'],
                              cv=cv,
                              save_model=cv_test_config['save_model'],
                              test_path=save_base_path,
                              callbacks=callbacks,
                              save_predictions=True,
                              use_tensorboard=cv_test_config['use_tensorboard'],
                              split_key=cv_test_config['split_key'],
                              distributed_info=distributed_info,
                              preloaded=cv_test_config['pre_loaded'],
                              preloaded_model=cv_test_config["pre_loaded_model"],
                              load_externally=cv_test_config['load_externally'],
                              seeds=cv_test_config['seeds'],
                              **model_routine_args
                              )
    # Validation
    if cv_test_config['repetitions'] > 1:
        validation_scores = {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if key.startswith('avg')}
        logger.info('Average validation scores: \n{}'.format(prettify_statistics(validation_scores)))
    else:
        validation_scores = {key: np.mean(item, axis=0) for key, item in scores['validation_info'].items() if
             not key.startswith('avg')}
        logger.info('Average validation scores: \n{}'.format(prettify_statistics(validation_scores)))

    # Test
    if cv_test_config['repetitions'] > 1:
        test_scores = {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if key.startswith('avg')}
        logger.info('Average test scores: \n{}'.format(prettify_statistics(test_scores)))
    else:
        test_scores = {key: np.mean(item, axis=0) for key, item in scores['test_info'].items() if not key.startswith('avg')}
        logger.info('Average test scores: \n{}'.format(prettify_statistics(test_scores)))

    if cv_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(save_base_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME), model_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAINING_CONFIG_NAME), training_config)
        save_json(os.path.join(save_base_path, cd.JSON_CV_TEST_CONFIG_NAME), data=cv_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_CALLBACKS_NAME), data=callbacks_data)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_CONFIG_NAME), data=distributed_info)

    # Rename folder
    if cv_test_config['rename_folder'] is not None and cv_test_config['pre_loaded_model'] is None:
        renamed_base_path = os.path.join(cd.CV_DIR, cv_test_config['model_type'], cv_test_config['rename_folder'])
        os.rename(save_base_path, renamed_base_path)
