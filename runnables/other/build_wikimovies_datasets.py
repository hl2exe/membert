"""

Builds WikiMovies dataset using one particular KB.
Memory for each QA pair is built using word matching with frequency threshold (F < 1000, no stopwords)

"""

from sklearn.feature_extraction.text import CountVectorizer
from keras.preprocessing.text import Tokenizer
import os
import const_define as cd
from tqdm import tqdm

# Step 0: Settings

# [wiki, wiki_ie, wiki_entities_kb]
kb_tag = "wiki_entities_kb"

# [train, dev, test]
data_split = 'train'

# Step 1: Load Data

kb_path = os.path.join(cd.WIKIMOVIES_DIR, '{}.txt'.format(kb_tag))

with open(kb_path, 'r') as f:
    kb_data = f.readlines()

kb_data = list(map(lambda item: item[2:].strip(), kb_data))
kb_data = list(filter(lambda item: item != '', kb_data))

print('Total KB: ', len(kb_data))

query_path = os.path.join(cd.WIKIMOVIES_DIR, 'wiki-entities_qa_{}.txt'.format(data_split))

with open(query_path, 'r') as f:
    query_data = f.readlines()

query_data = list(filter(lambda item: item.strip() != '', query_data))
query_data = list(map(lambda item: item[2:], query_data))
query_data = list(map(lambda item: item.split('\t')[0].strip(), query_data))
print('Total QA: ', len(query_data))

# Step 2: Build vocabulary and filter frequency

count_vectorizer = CountVectorizer(stop_words="english", max_df=1000)

vectorizer_data = kb_data + query_data
count_vectorizer.fit(vectorizer_data)

vocab = list(count_vectorizer.vocabulary_.keys())

print(len(vocab))

word2idx = {word: idx + 1 for idx, word in enumerate(vocab)}
idx2word = {value: key for key, value in word2idx.items()}

# Step 3: Build per QA pair memory via word matching

tokenizer = Tokenizer()
tokenizer.word_index = word2idx
tokenizer.index_word = idx2word
tokenizer.num_words = len(vocab)

conv_query_data = tokenizer.texts_to_sequences(query_data)
conv_query_data = list(filter(lambda item: len(item) > 0, conv_query_data))
conv_kb_data = tokenizer.texts_to_sequences(kb_data)
conv_kb_data = list(filter(lambda item: len(item) > 0, conv_kb_data))

memory = {}

for pair_idx, qa_pair in enumerate(tqdm(conv_query_data)):
    for kb_item in conv_kb_data:
        match = set(qa_pair).intersection(set(kb_item))
        if len(match) == len(set(qa_pair)):
            memory.setdefault(pair_idx, []).append(kb_item)
    if pair_idx in memory and len(memory[pair_idx]) >= 100:
        print(len(memory[pair_idx]))
    if pair_idx not in memory:
        print(tokenizer.sequences_to_texts([qa_pair]))
        print(qa_pair)
