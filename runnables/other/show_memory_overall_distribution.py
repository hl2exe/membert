"""

For a given category, shows memory usage for unfair examples, distiguishing for each explanation ID

"""

import const_define as cd
import os
import numpy as np
import pandas as pd
from utility.json_utils import load_json
import ast
from utility.python_utils import flatten_nested_array
import matplotlib.pyplot as plt

plt.style.use('seaborn-paper')

SMALL_SIZE = 3
MEDIUM_SIZE = 10
BIGGER_SIZE = 30

plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# Step -1: functions

def get_predictions(attention_values, attention_mode):
    if attention_mode == 'softmax':
        return np.argmax(attention_values, axis=1).ravel().tolist()
    elif attention_mode == 'sigmoid':
        return np.where(attention_values >= 0.5)[-1].tolist()
    else:
        raise RuntimeError('Invalid attention mode! Got: {}'.format(attention_mode))


# Step 0: config
# Consider the best rep for each fold

model_type = "memory-distilbert-base-uncased"
test_name = "LTD"
set_suffix = 'val'

image_folder_name = '{}_Images'.format(test_name)
image_path = os.path.join(cd.CV_DIR, model_type, image_folder_name)

if not os.path.isdir(image_path):
    os.makedirs(image_path)

fig_save_path = os.path.join(image_path, 'memory_distribution_fold_{0}_split_{1}.jpg')

candidate_folders = [folder for folder in os.listdir(os.path.join(cd.CV_DIR, model_type)) if folder.startswith(test_name) and folder != image_folder_name]
print("Found {} candidate folders!".format(candidate_folders))

for fold_idx in range(10):
    print("Considering folder : ", fold_idx)

    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(18.5, 10.5)

    for folder in candidate_folders:

        model_path = os.path.join(cd.CV_DIR, model_type, folder)

        loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
        category = loader_info['configs'][loader_info['type']]["category"]

        attention_weights_names = [name for name in os.listdir(model_path) if
                                   'top1_{}_attention_weights'.format(set_suffix) in name.lower()]

        attention_mode = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))['extraction_info']['value'][
            'mode']

        explanations = pd.read_csv(os.path.join(cd.KB_DIR, '{}_explanations.csv'.format(category.upper())))
        explanations_tags = explanations['Id'].values

        # Load attention weights
        fold_attention_name = [name for name in attention_weights_names if 'fold_{}'.format(fold_idx) in name][0]

        # [samples, memory_size]
        attention_weights = load_json(os.path.join(model_path, fold_attention_name))
        attention_weights = np.squeeze(attention_weights, axis=1)

        mean_activation = np.mean(attention_weights, axis=0)
        std_activation = np.std(attention_weights, axis=0)

        # Plot time!
        ax.plot(mean_activation, marker='o', linestyle='solid', markersize=MEDIUM_SIZE, linewidth=SMALL_SIZE)
        ax.fill_between(np.arange(mean_activation.shape[0]),
                        mean_activation - std_activation,
                        mean_activation + std_activation,
                        alpha=0.3)
        ax.set_title('Category {0} -- Fold {1}'.format(test_name.split('-')[0], fold_idx))

        ax.set_xticks(ticks=np.arange(mean_activation.shape[0]))
        ax.set_yticks(ticks=np.arange(6) / 5)
        ax.set_xticklabels(labels=explanations_tags, rotation=-90)

    plt.legend(candidate_folders, bbox_to_anchor=(1.0, 1.05))
    plt.savefig(fig_save_path.format(fold_idx, set_suffix), bbox_inches='tight')
    plt.close(fig)
