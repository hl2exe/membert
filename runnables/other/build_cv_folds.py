"""

@Author: Federico Ruggeri

@Date: 26/03/2019

Builds ToS Task 1 cross-validation folds

"""

import os

import numpy as np

import const_define as cd
from data_loader import Task1Loader
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json

if __name__ == '__main__':
    # Step 1: load dataset
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))

    task1_loader = Task1Loader()

    data_handle = task1_loader.load(**data_loader_config['task1'])

    dataset_list = np.unique(data_handle.data['document'].values)

    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key='test')
    cv.build_folds(X=dataset_list, y=dataset_list)

    folds_save_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds', 'ToS_100_splits_10_kfold.json')
    cv.save_folds(folds_save_path, tolist=True)

    list_save_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds', 'ToS_100_splits_10_kfold.txt')

    with open(list_save_path, 'w') as f:
        f.writelines(os.linesep.join(dataset_list))
