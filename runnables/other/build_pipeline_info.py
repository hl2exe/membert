"""

Builds pipeline info for given data

"""

from utility.json_utils import load_json
from data_loader import Task1KBLoader
import const_define as cd
import os
from utility.cross_validation_utils import PrebuiltCV
from data_processor import TextKBSupervisionProcessor
from data_converter import KBSupervisionConverter
from tokenization import KerasTokenizer
from utility.log_utils import get_logger
import numpy as np

logger = get_logger(__name__)

split_key = 'document_ID'
prebuilt_folds = 'tos_100_splits_10_kfold'
validation_percentage = 0.2

data_loader_info = {'category': 'TER',
                    'dataset': 'tos_100'}
processor_args = {
    'filter_names': None,
    'partial_supervision_info': {'flag': True}
}
tokenizer_args = {
    'embedding_dimension': 128,
    'tokenizer_args': {'oov_token': 1}
}
converter_args = {
    'feature_class': 'text_kb_supervision_features',
    'partial_supervision_info': {'flag': True}
}

# Data loader
data_loader = Task1KBLoader()
data_handle = data_loader.load(**data_loader_info)

# CV
cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key='test')
folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(prebuilt_folds))
cv.load_folds(load_path=folds_path)

# Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

# Build processor

processor_args['loader_info'] = data_handle.get_additional_info()
processor = TextKBSupervisionProcessor(**processor_args)

# Build tokenizer

tokenizer = KerasTokenizer(**tokenizer_args)

# Build converter
converter = KBSupervisionConverter(**converter_args)

for fold_idx, (train_indexes, test_indexes) in enumerate(cv.split(None)):
    logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

    train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                      key_values=test_indexes,
                                                      validation_percentage=validation_percentage)

    # Processor

    train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
    val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
    test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

    # Tokenizer
    save_prefix = 'fold_{}'.format(fold_idx)

    train_texts = train_data.get_data()
    tokenizer.build_vocab(data=[train_data, val_data, test_data], filepath=".", prefix=save_prefix)
    tokenizer.save_info(filepath=".", prefix=save_prefix)
    tokenizer_info = tokenizer.get_info()

    converter.convert_data(examples=train_data,
                           label_list=processor.get_labels(),
                           output_file="train_fold_{}".format(fold_idx),
                           tokenizer=tokenizer,
                           is_training=True)
    converter.save_conversion_args(filepath=".", prefix=save_prefix)
    converter_info = converter.get_conversion_args()

    # Debug
    tokenizer.show_info(tokenizer_info)
    logger.info('Converter info: \n{}'.format(converter_info))
