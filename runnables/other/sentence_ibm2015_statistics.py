import pandas as pd
import const_define as cd
import os
import ast
from nltk import word_tokenize, sent_tokenize
from collections import Counter
import numpy as np
import re


def show_statistics(df):
    check_df = df.query('C_Label == 1 & NA_Label == 1')
    assert check_df.empty

    print('Normal Statistics - > ', df['C_Label'].value_counts())


def show_unlinked_claims(df):
    claims_df = df.query("C_Label == 1")
    linked_claims = [row_idx for row_idx, row in claims_df.iterrows() if ast.literal_eval(row['Evidence indices'])]

    print("Linked claims -> ", len(linked_claims))
    print("Unlinked claims -> ", claims_df.shape[0] - len(linked_claims))


def show_sentence_statistics(df):
    lengths = [len(word_tokenize(re.sub('[^A-Za-z0-9]+', ' ', text).strip())) for text in df.Sentence.values]
    counts = Counter(lengths)
    counts = sorted(counts.items(), key=lambda item: item[0])
    print('Lengths -> \n ', counts)

    good_lengths = np.where(np.array(lengths) >= 3)[0]
    good_df = df.iloc[good_lengths]

    print('Good Statistics - > ', good_df['C_Label'].value_counts())
    good_df.to_csv(os.path.join(cd.IBM2015_DIR, 'reduced_corrected_sentence_dataset.csv'))


def verify_sentences(df):

    nltk_sentences = [sent_tokenize(text) for text in df.Sentence.values]
    nltk_sentences = [item for seq in nltk_sentences for item in seq]

    print("Original #sentences -> ", df.shape[0])
    print("Nltk #sentences -> ", len(nltk_sentences))

    lengths = [len(word_tokenize(text)) for text in nltk_sentences]
    print('Nltk Lengths -> \n ', Counter(lengths))


df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'corrected_sentence_dataset.csv'))

show_statistics(df)
show_unlinked_claims(df)
show_sentence_statistics(df)
# verify_sentences(df)
