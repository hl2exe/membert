"""

Testing Memory enhanced DistilBERT

"""

import pandas as pd
from ktrain import text
import os
import const_define as cd
from old_stuff.custom_tf_v2 import M_MemoryDistilBERT
import numpy as np
from utility.ktrain_utils import KBTransformerSequence
import tensorflow as tf
from ktrain.text.learner import TransformerTextClassLearner

tf.config.experimental_run_functions_eagerly(True)

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))
df = df.iloc[:1000]

train_df, test_df = df.iloc[:800], df.iloc[800:]

category = 'A'
with open(os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category))) as f:
    kb = f.readlines()

print('Train shape: ', train_df.shape)
print('Test shape: ', test_df.shape)

model_name = 'distilbert-base-uncased'

preprocessor = text.Transformer(model_name, maxlen=300, classes=[0, 1])

train_data = preprocessor.preprocess_train(train_df.text.values,
                                           train_df[category].values)

test_data = preprocessor.preprocess_test(test_df.text.values,
                                         test_df[category].values)

kb_data = preprocessor.preprocess_test(kb)

total_train_data = KBTransformerSequence.from_transformer_sequences(train_data, kb_data)
total_test_data = KBTransformerSequence.from_transformer_sequences(test_data, kb_data)

# Create original pre-trained
model = preprocessor.get_classifier()

# Create and compile custom model
my_model = M_MemoryDistilBERT(config=model.config)
loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
my_model.compile(loss=loss_fn,
                 optimizer=tf.keras.optimizers.Adam(learning_rate=3e-5, epsilon=1e-08),
                 metrics=['accuracy'])

# Inputs:
# clause_input_ids, clause_attention_mask, clause_token_type_ids
# memory_input_ids, memory_attention_mask, memory_token_type_ids

clause_input_ids = np.array([7, 6, 4, 0, 0]).reshape(1, 5)
clause_attention_mask = np.array([1, 1, 1, 0, 0]).reshape(1, 5)
clause_token_type_ids = np.zeros_like(clause_input_ids)

memory_input_ids = np.array([[5, 4, 3, 0, 0], [8, 20, 3, 13, 0]]).reshape(1, -1, 5)
memory_attention_mask = np.array([[1, 1, 1, 0, 0], [1, 1, 1, 1, 0]]).reshape(1, -1, 5)
memory_token_type_ids = np.zeros_like(memory_input_ids)

ret = my_model({
    'clause_input_ids': clause_input_ids,
    'clause_attention_mask': clause_attention_mask,
    'clause_token_type_ids': clause_token_type_ids,
    'memory_input_ids': memory_input_ids,
    'memory_attention_mask': memory_attention_mask,
    'memory_token_type_ids': memory_token_type_ids
})

# Set weights
for original, custom in zip(model._layers, my_model._layers):
    try:
        custom.set_weights(original.get_weights())
    except Exception as e:
        continue

learner = TransformerTextClassLearner(my_model, train_data=total_train_data, val_data=total_test_data,
                                      batch_size=1)

learner.fit(lr=5e-6, n_cycles=1, verbose=1)

predictions = my_model.predict(total_test_data.to_tfdataset(shuffle=False, repeat=False))
predictions = tf.keras.activations.softmax(tf.convert_to_tensor(predictions)).numpy()

print(predictions.shape)
