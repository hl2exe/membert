"""

Creates a sub-version of tos_100 dataset


"""

import os
import pandas as pd
import const_define as cd
import numpy as np

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))
doc_subset = 70

categories = ['A', 'CH', 'CR', 'LTD', 'TER']
documents = list(set(df.document.values))
random_subset = []
while len(random_subset) < doc_subset:
    random_doc = np.random.choice(a=documents, size=1, replace=False)[0]

    # Check if has all labels
    can_be_added = True
    for cat in categories:
        if len(df[df.document == random_doc][cat].value_counts()) != 2:
            print("[{}] Skipping document -> ".format(cat), random_doc)
            can_be_added = False
            break

    if can_be_added:
        random_subset.append(random_doc)

assert len(random_subset) == doc_subset
sub_df = df[df.document.isin(random_subset)]
print('Shape -> ', sub_df.shape)

sub_df.to_csv(os.path.join(cd.TOS_100_DIR, 'dataset_{}.csv'.format(doc_subset)), index=False)