"""

@Author: Federico Ruggeri

@Date: 26/03/2019

Builds IBM2015 calibration folds (5 folds)

"""

import os

import numpy as np

import const_define as cd
from data_loader import IBM2015Loader
from data_loader import Task1Loader
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json

if __name__ == '__main__':
    # Step 1: load dataset
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))['configs']

    task1_loader = Task1Loader()
    sub_suffix = "70"

    data_handle = task1_loader.load(dataset='tos_100', category='A', sub_suffix=sub_suffix)

    dataset_list = np.unique(data_handle.data['document'].values)

    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key='test')
    cv.build_folds(X=dataset_list, y=dataset_list)

    train_data = data_handle.data

    cv.build_all_sets_folds(X=dataset_list, y=dataset_list, validation_n_splits=5)

    base_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds')

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    folds_save_path = os.path.join(base_path, 'tos_{0}_splits_10_kfold.json'.format(sub_suffix))
    cv.save_folds(folds_save_path, tolist=True)

    list_save_path = os.path.join(base_path, 'tos_{0}_splits_10_kfold.txt'.format(sub_suffix))

    with open(list_save_path, 'w') as f:
        f.writelines(os.linesep.join(dataset_list))
