import os
import pandas as pd
import const_define as cd
import numpy as np
from tqdm import tqdm
import json
from pycorenlp import StanfordCoreNLP
from ast import literal_eval
from utility.json_utils import save_json
import difflib
import re
from copy import deepcopy
from nltk import sent_tokenize


def build_motions_dict(motions):
    results = {}
    for _, row in motions.iterrows():
        results.setdefault(row['Topic'], {}).setdefault('Topic id', row['Topic id'])
        results.setdefault(row['Topic'], {}).setdefault('Data-set', row['Data-set'])

    return results


def check_alone_claims(claims_df, evidences_df):
    connected_claims = evidences_df.Claim.values
    all_claims = claims_df['Claim original text'].values

    connected_claims = set(connected_claims)
    all_claims = set(all_claims)

    print("Connected claims -> ", len(connected_claims))
    print("All claims -> ", len(all_claims))

    difference = all_claims.difference(connected_claims)
    return len(difference)


def parse_article(article_id, corenlp, use_corenlp=True):
    if use_corenlp:
        return parse_article_corenlp(article_id, corenlp)
    else:
        return parse_article_simple(article_id)


def parse_article_simple(article_id):
    filepath = os.path.join(cd.IBM2015_DIR, 'articles', 'clean_{}.txt'.format(article_id))

    with open(filepath, 'r') as f:
        sentences = f.readlines()

    text = os.linesep.join(sentences)
    text = text.replace('%', '%25')
    text = text.replace("\\+", "%2B")
    text = text.strip()

    return text


def parse_article_corenlp(article_id, corenlp):
    filepath = os.path.join(cd.IBM2015_DIR, 'articles', 'clean_{}.txt'.format(article_id))

    with open(filepath, 'r') as f:
        sentences = f.readlines()

    text = os.linesep.join(sentences)
    text = text.replace('%', '%25')
    text = text.replace("\\+", "%2B")
    text = text.strip()
    sentences = text.split(os.linesep)

    parsed_sentences = []
    for sentence_idx, sentence in enumerate(sentences):
        annotation = corenlp.annotate(sentence.strip(),
                                      properties={'timeout': 100000000,
                                                  'max_char_length': 10000000,
                                                  'annotators': 'tokenize,ssplit'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print("Failed annotation! Document: {0} -- Sentence Idx: {1}".format(filepath, sentence_idx))
                print("Annotation failure: {0}".format(e))
                parsed_sentences.append(sentence)

            # Sentences
            for idx in range(len(data['sentences'])):
                tokens = data['sentences'][idx]['tokens']
                tokens = [item['word'] for item in tokens]
                parsed_sentence = ' '.join(tokens)
                parsed_sentence = parsed_sentence.lower()
                parsed_sentence = parsed_sentence.strip()
                parsed_sentences.append(parsed_sentence)

    return os.linesep.join(parsed_sentences)


def parse_text(text, corenlp, use_corenlp=True):
    if use_corenlp:
        return parse_text_corenlp(text, corenlp)
    else:
        return parse_text_simple(text)


def parse_text_corenlp(text, corenlp):
    text = text.replace('%', '%25')
    text = text.replace("\\+", "%2B")
    text = text.strip()
    text = text.encode('utf-8').decode('utf-8', 'ignore')

    parsed_sentences = []
    annotation = corenlp.annotate(text,
                                  properties={'timeout': 100000000,
                                              'max_char_length': 100000000000,
                                              'annotators': 'tokenize,ssplit'})
    if annotation:
        try:
            data = json.loads(annotation)
        except Exception as e:
            print("Failed annotation! -> ", text)
            print("Annotation failure: {0}".format(e))

        # Sentences
        for idx in range(len(data['sentences'])):
            tokens = data['sentences'][idx]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            parsed_sentence = parsed_sentence.strip()
            parsed_sentences.append(parsed_sentence)

    return [sent for sent in parsed_sentences if len(sent.strip()) >= 5]


def parse_text_list_corenlp(text_list, corenlp):
    result = []

    for text in text_list:
        parsed_sentences = parse_text_corenlp(text, corenlp)
        result += parsed_sentences

    return result


def parse_text_simple(text):
    text = text.replace('%', '%25')
    text = text.replace("\\+", "%2B")
    text = text.strip()

    return text


def check_difference(a, b, show=False):
    count = 0

    for i, s in enumerate(difflib.ndiff(a, b)):
        if s[0] == ' ':
            continue
        elif s[0] == '-':
            if show:
                print("Delete {0} from position {1}".format(s[-1], i))
            count += 1
        elif s[0] == '+':
            if show:
                print("Add {0} from position {1}".format(s[-1], i))
            count += 1

    return count


def map_arguments_in_article_text(claims_df, evidences_df, article_row, claim_corrections,
                                  unmapped_claims=None):
    # Filter by topic
    article_topic = article_row['Topic']

    topic_claims_df = claims_df[claims_df.Topic == article_topic]
    topic_evidences_df = evidences_df[evidences_df.Topic == article_topic]

    topic_claims = topic_claims_df['Parsed Claim'].values

    evidence_listing = {}
    for evidence_idx, topic_evidence in enumerate(set(topic_evidences_df['Parsed Evidence'].values)):
        evidence_listing.setdefault(topic_evidence, evidence_idx)

    claim_evidence_link = {}
    for _, ev_row in topic_evidences_df.iterrows():
        ev_row_claim = parse_text_simple(ev_row['Claim'])
        ev_row_evidence = parse_text_simple(ev_row['Evidence'])
        claim_evidence_link.setdefault(ev_row_claim, []).append(evidence_listing[ev_row_evidence])

    for claim in topic_claims:
        assert claim in claim_corrections

    # Mapping
    retrieved_info = {}

    if unmapped_claims is None:
        unmapped_claims = set(topic_claims)

    def _check_within_text(candidate, text):

        simple_text = text.replace('%', '%25')
        simple_text = simple_text.replace("\\+", "%2B")
        simple_text = re.sub('[^A-Za-z0-9]+', '', simple_text)
        simple_text = simple_text.strip()

        simple_candidate = candidate.replace('%', '%25')
        simple_candidate = simple_candidate.replace("\\+", "%2B")
        simple_candidate = re.sub('[^A-Za-z0-9]+', '', simple_candidate)
        simple_candidate = simple_candidate.strip()

        to_try = [
            simple_candidate,
            simple_candidate.lower(),
            candidate,
            candidate.lower()
        ]

        check_in_text = any([item.strip().lower() in text.strip().lower() for item in to_try])
        check_in_simple_text = any([item.strip().lower() in simple_text.strip().lower() for item in to_try])

        return check_in_text or check_in_simple_text

    article_text = re.sub(r'\n+', '\n', article_row['Article Text'])
    article_sentences = parse_text_list_corenlp(article_text.split(os.linesep), nlp)
    article_sentences = [item for item in article_sentences if len(item.strip())]
    for sentence in article_sentences:

        retrieved_info.setdefault('Topic', []).append(article_row['Topic'])
        retrieved_info.setdefault('Title', []).append(article_row['Title'])
        retrieved_info.setdefault('article Id', []).append(article_row['article Id'])
        retrieved_info.setdefault('Data-set', []).append(article_row['Data-set'])
        retrieved_info.setdefault('Sentence', []).append(sentence)

        C_Label = 0
        NA_Label = 0

        arg_mapping_flag = False
        found_claims = []
        found_evidences = []

        for topic_claim in topic_claims:

            # Mapping a claim (original text)
            if _check_within_text(topic_claim, sentence):
                C_Label = 1
                arg_mapping_flag = True
                if topic_claim in unmapped_claims:
                    unmapped_claims.remove(topic_claim)
                if topic_claim not in found_claims:
                    found_claims.append(topic_claim)
                    if topic_claim in claim_evidence_link:
                        found_evidences.extend(claim_evidence_link[topic_claim])

            # Mapping a claim (corrected text)
            if _check_within_text(claim_corrections[topic_claim], sentence):
                C_Label = 1
                arg_mapping_flag = True
                if topic_claim in unmapped_claims:
                    unmapped_claims.remove(topic_claim)
                if claim_corrections[topic_claim] not in found_claims:
                    found_claims.append(claim_corrections[topic_claim])
                    if topic_claim in claim_evidence_link:
                        found_evidences.extend(claim_evidence_link[topic_claim])

        # if len(found_claims) > 1:
        #     print("Warning -> got multiple claims in sentence! Total -> ", len(found_claims))

        # Mapping a non-argumentative sentence
        if not arg_mapping_flag:
            NA_Label = 1

        retrieved_info.setdefault('C_Label', []).append(C_Label)
        retrieved_info.setdefault('NA_Label', []).append(NA_Label)
        retrieved_info.setdefault('Mapped Claims', []).append(found_claims)
        retrieved_info.setdefault('Evidence indices', []).append(list(set(found_evidences)))

    return retrieved_info, unmapped_claims, evidence_listing


checkpoint_path = os.path.join(cd.IBM2015_DIR, 'articles_checkpoint.csv')

use_corenlp = False

# CoreNLP
port = 9000
nlp = StanfordCoreNLP('http://localhost:{}'.format(port))

if not os.path.isfile(checkpoint_path):

    # Load motions
    motions_path = os.path.join(cd.IBM2015_DIR, 'motions.txt')
    motions = pd.read_csv(motions_path, sep='\t')
    motions_dict = build_motions_dict(motions)

    # Load articles
    articles_path = os.path.join(cd.IBM2015_DIR, 'articles.txt')
    articles = pd.read_csv(articles_path, sep='\t')

    # Add motions data to articles
    articles['Topic id'] = np.nan
    articles['Data-set'] = np.nan

    for key, value in motions_dict.items():
        articles.loc[articles.Topic == key, "Topic id"] = value['Topic id']
        articles.loc[articles.Topic == key, "Data-set"] = value['Data-set']

    # Correctness check
    assert not articles['Topic id'].isnull().values.any()
    assert not articles['Data-set'].isnull().values.any()

    # At this point we have (articles dataFrame)
    # Topic Title   article Id  Topic id    Data-set

    print("Parsing articles... Total -> ", len(articles))
    article_texts = {
        row['article Id']: parse_article(row['article Id'], nlp, use_corenlp) for _, row in tqdm(articles.iterrows())
    }

    articles['Article Text'] = np.nan

    for key, value in article_texts.items():
        articles.loc[articles['article Id'] == key, "Article Text"] = value

    # Correctness check
    assert not articles['Article Text'].isnull().values.any()

    # Checkpoint
    articles.to_csv(checkpoint_path, index=False)
else:
    articles = pd.read_csv(checkpoint_path)

# At this point we have (articles dataFrame)
# Topic Title   article Id  Topic id    Data-set    Article Text

# Load claims and evidences

# Topic     Claim original text     Claim corrected text
claims_path = os.path.join(cd.IBM2015_DIR, 'claims.txt')
claims_df = pd.read_csv(claims_path, sep='\t')
claims_dict = {
    row_idx: {'Corrected': row['Claim corrected version'], 'Topic': row['Topic'], 'Claim': row['Claim original text']}
    for row_idx, row in claims_df.iterrows()}

# Topic     Claim   Evidence    Tag
evidences_path = os.path.join(cd.IBM2015_DIR, 'evidence.txt')
evidences_df = pd.read_csv(evidences_path, sep='\t', names=['Topic', 'Claim', 'Evidence', 'Tag'])

# The claim associated to each evidence is the 'Claim original text' column/key
evidences_dict = {row_idx: {'Topic': row['Topic'], 'Claim': row['Claim'], 'Evidence': row['Evidence']}
                  for row_idx, row in evidences_df.iterrows()}

# Check if there are claims not connected to an evidence
check_alone = check_alone_claims(claims_df, evidences_df)
print("Alone claims -> ", check_alone)

# Parse claims and evidences while maintaining claim -> evidence association
checkpoint_path_template = os.path.join(cd.IBM2015_DIR, 'checkpoint_parse_{}.npy')

if not os.path.isfile(checkpoint_path_template.format('claims')):

    all_claims = claims_df['Claim original text'].values
    claims_corrections = {row['Claim original text']: row['Claim corrected version'] for _, row in claims_df.iterrows()}
    evidences = evidences_df.Evidence.values

    print("Parsing arguments...")
    parsed_all_claims = [parse_text(text, nlp, use_corenlp) for text in tqdm(all_claims)]
    parsed_claim_corrections = {parse_text(key, nlp, use_corenlp): parse_text(value, nlp, use_corenlp) for key, value in
                                claims_corrections.items()}
    parsed_evidences = [parse_text(text, nlp, use_corenlp) for text in tqdm(evidences)]

    # Checkpoint

    np.save(checkpoint_path_template.format("claims"), parsed_all_claims)
    np.save(checkpoint_path_template.format("evidences"), parsed_evidences)
    np.save(checkpoint_path_template.format("claim_corrections"), parsed_claim_corrections)

else:
    parsed_all_claims = np.load(checkpoint_path_template.format("claims"))
    parsed_claim_corrections = np.load(checkpoint_path_template.format("claim_corrections"), allow_pickle=True).item()
    parsed_evidences = np.load(checkpoint_path_template.format("evidences"))

claims_df['Parsed Claim'] = parsed_all_claims
evidences_df['Parsed Evidence'] = parsed_evidences

for claim in claims_df['Parsed Claim'].values:
    assert claim in parsed_claim_corrections

# Map arguments in article texts
articles['C_Label'] = np.nan
articles['E_Label'] = np.nan
articles['NA_Label'] = np.nan

C_Label = []
E_Label = []
NA_Label = []

total_unmapped_claims = {}
evidence_listing = {}

topics = set(articles.Topic.values)

print("Mapping arguments in articles text")

sentence_info = {}

for topic in tqdm(topics):

    topic_articles = articles[articles.Topic == topic]

    unmapped_claims = None

    for row_idx, row in topic_articles.iterrows():

        retrieved_info, \
        unmapped_claims, \
        topic_evidence_listing = map_arguments_in_article_text(claims_df=claims_df,
                                                               evidences_df=evidences_df,
                                                               article_row=row,
                                                               unmapped_claims=unmapped_claims,
                                                               claim_corrections=parsed_claim_corrections)

        for key, value in retrieved_info.items():
            sentence_info.setdefault(key, []).extend(value)

        evidence_listing[topic] = topic_evidence_listing

    if len(unmapped_claims):
        print("[Topic -> {0}] Got {1} unmapped claims".format(topic, len(unmapped_claims)))
        total_unmapped_claims[topic] = unmapped_claims

# Save errors
error_save_path = os.path.join(cd.IBM2015_DIR, 'unmapped_{}.json')
save_json(error_save_path.format('claims'), total_unmapped_claims)

print("Total unmapped claims -> ", np.sum([len(value) for value in total_unmapped_claims.values()]))

save_json(os.path.join(cd.IBM2015_DIR, 'evidence_topic_memory.json'), evidence_listing)

sentence_df = pd.DataFrame.from_dict(sentence_info)
print("Before dropna -> ", sentence_df.shape)
sentence_df = sentence_df.dropna()
print("After dropna -> ", sentence_df.shape)
sentence_df.to_csv(os.path.join(cd.IBM2015_DIR, 'corrected_sentence_dataset.csv'), index=False)
