"""

Simple test interface for quick and no error-prone setup

"""

from utility.json_utils import load_json, save_json
import os
import const_define as cd
from utility.log_utils import get_logger
import argparse
import json
import time

logger = get_logger(__name__)

if __name__ == '__main__':

    # Immediately lock access to configuration files
    while os.path.isfile('quick_test_setup.lock'):
        logger.info("Another process is modifying access files, waiting...")
        time.sleep(1.0)
    with open('quick_test_setup.lock', 'w') as f:
        pass

    # Any model name
    model_type = 'bidaf'

    # squad -> ""
    # ibm2015 -> "ibm2015"
    # tos -> ""
    model_dataset_type = ""

    # squad
    # ibm2015_1|2|3|4
    # tos_30|50|70|100
    dataset_name = "squad"

    # If using KB -> task1_kb_simple - If not using KB -> task1
    # ibm2015
    # multi_task1_kb
    # multi_task1
    task_type = 'squad'

    test_type = "train_and_test"

    # If you want to save results and do a proper test -> True
    is_valid_test = True

    # Rename folder
    rename_folder = "squad_test"

    # GPU
    gpu_start_index = 0
    gpu_end_index = 1

    # Seeds
    seeds = [
        15371,
        # 15372,
        # 15373
    ]

    # category = A|CH|CR|LTD|TER <- tos
    # nothing                    <- ibm2015
    # any combination of ["A", "CH", "CR", "LTD", "TER"] <- multi_task1_kb, multi_task1
    loader_args = {
        # "category": "A"
    }

    # earlystopping -> patience
    callback_args = {
        "earlystopping": {
            "patience": 15,
            "monitor": "val_EM",
            "mode": "max"
        }
    }

    batch_size = 32
    step_checkpoint = None

    quick_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_QUICK_SETUP_CONFIG_NAME))

    # Parser
    parser = argparse.ArgumentParser(description="Argument Parser")
    parser.add_argument("-mt", "--model_type", help="Model architecture name."
                                                    " Check const_define.py -> MODEL_CONFIG variable for more info.",
                        type=str, default=model_type)
    parser.add_argument("-tt", "--task_type", help="task1_kb_simple, task1, multi_task1, multi_task1_kb, ibm2015",
                        type=str, default=task_type)
    parser.add_argument("-tety", "--test_type", help="cv_test, train_and_test",
                    type=str, default=test_type)
    parser.add_argument("-mdt", "--model_dataset_type", help="Leave blank for tos_100 and ibm2015 otherwise",
                        type=str, default=model_dataset_type)
    parser.add_argument("-dn", "--dataset_name", help="tos_30, tos_50, tos_70, tos_100,"
                                                      " ibm2015_1, ibm2015_2, ibm2015_3, ibm2015_4",
                        type=str, default=dataset_name)
    parser.add_argument("-la", "--loader_args", help="tos_100 requires category and nothing for ibm2015",
                        type=json.loads, default=loader_args)
    parser.add_argument("-ca", "--callback_args", help="bert models usually have 10 patience, others have 50 patience",
                        type=json.loads, default=callback_args)
    parser.add_argument("-ivt", "--is_valid_test", help="If True, benchmark test configuration is loaded "
                                                        "and test results will be saved.",
                        type=bool, default=is_valid_test)
    parser.add_argument("-rf", "--rename_folder", help="How to rename the test folder when the test ends",
                        type=str, default=rename_folder)
    parser.add_argument("-gsi", "--gpu_start_index", help="Start index of visible gpu",
                        type=int, default=gpu_start_index)
    parser.add_argument("-gei", "--gpu_end_index", help="End index of visible gpu",
                        type=int, default=gpu_end_index)
    parser.add_argument("-bs", "--batch_size", help="Number of samples per batch",
                        type=int, default=batch_size)
    parser.add_argument("-s", "--seeds", help="Seeds for each test repetition",
                        type=json.loads, default=seeds)
    args = parser.parse_args()

    model_type = args.model_type
    model_dataset_type = args.model_dataset_type
    dataset_name = args.dataset_name
    task_type = args.task_type
    test_type = args.test_type
    is_valid_test = args.is_valid_test
    batch_size = args.batch_size
    loader_args = args.loader_args
    callback_args = args.callback_args
    rename_folder = args.rename_folder
    gpu_start_index = args.gpu_start_index
    gpu_end_index = args.gpu_end_index
    seeds = args.seeds

    if not is_valid_test:
        seeds = seeds[:1]

    logger.info("Debugging argparse...")
    for key, value in args.__dict__.items():
        logger.info("Arg: {0} -- Value: {1}".format(key, value))
    logger.info("End of debugging... {}".format(os.linesep))

    logger.info('Quick setup start!')

    # Data loader

    data_loader_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_settings['type'] = task_type
    if dataset_name in quick_settings['input_specific_settings']:

        logger.info('Uploading data loader settings...')

        for key, value in quick_settings['input_specific_settings'][dataset_name].items():
            data_loader_settings['configs'][task_type][key] = value

        for key, value in loader_args.items():
            if key in data_loader_settings['configs'][task_type]:
                data_loader_settings['configs'][task_type][key] = value
            else:
                logger.info("Ignoring key {}... (not in original configuration)".format(key))

    else:
        logger.warning('Could not find dataset input specific settings...Is it ok?')

    logger.info('Data loader settings uploaded! Do not forget to check loader specific args!!')

    save_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME), data_loader_settings)

    # Callbacks
    logger.info('Uploading callbacks settings...')

    callbacks_settings = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    for key, key_config in callback_args.items():
        if key in callbacks_settings:
            for config_key, config_value in key_config.items():
                callbacks_settings[key][config_key] = config_value

    save_json((os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME)), callbacks_settings)

    logger.info('Callbacks settings uploaded!')

    # Specific test settings

    if model_dataset_type:
        actual_model_type = model_dataset_type + '_' + model_type
    else:
        actual_model_type = model_type
    test_settings = load_json(os.path.join(cd.CONFIGS_DIR, '{}_config.json'.format(test_type)))
    test_settings['model_type'] = actual_model_type

    if dataset_name in quick_settings['data_specific_settings']:

        logger.info('Uploading {} settings...'.format(test_type))

        for key, value in quick_settings['data_specific_settings'][dataset_name].items():
            test_settings[key] = value
    else:
        logger.warning('Could not find dataset test specific settings...Aborting..')
        exit(-1)

    if is_valid_test:
        if test_type in quick_settings['valid_test_settings'] and dataset_name in quick_settings['valid_test_settings'][
            test_type]:
            for key, value in quick_settings['valid_test_settings'][test_type][dataset_name].items():
                test_settings[key] = value

        test_settings['save_model'] = True
        test_settings['compute_test_info'] = True
    else:
        if test_type in quick_settings['default_test_settings'] and dataset_name in \
                quick_settings['default_test_settings'][test_type]:
            for key, value in quick_settings['default_test_settings'][test_type][dataset_name].items():
                test_settings[key] = value

        test_settings['save_model'] = False

    if model_type in quick_settings['model_specific_settings']:
        for key, value in quick_settings['model_specific_settings'][model_type].items():
            test_settings[key] = value
    else:
        logger.warning('Could not find model specific settings! Is this ok?')

    test_settings['rename_folder'] = rename_folder
    test_settings['gpu_start_index'] = gpu_start_index
    test_settings['gpu_end_index'] = gpu_end_index
    test_settings['seeds'] = seeds

    save_json(os.path.join(cd.CONFIGS_DIR, '{}_config.json'.format(test_type)), test_settings)

    # Training settings

    training_path = os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME)
    training_settings = load_json(training_path)

    logger.info('Uploading {} training settings...'.format(test_type))

    training_settings['batch_size'] = batch_size

    for key, value in quick_settings['data_specific_settings'][dataset_name].items():
        if key in training_settings:
            training_settings[key] = value

    if model_type in quick_settings['model_specific_settings']:
        for key, value in quick_settings['model_specific_settings'][model_type].items():
            if key in training_settings:
                training_settings[key] = value

    save_json(training_path, training_settings)

    logger.info(
        'Quick setup upload done! Check loader specific settings and your model config before running the test!')

    # Release access
    os.remove('quick_test_setup.lock')