"""

Plots the timeline of sampling memory priority weights

"""

import ast
import os
from collections import Counter
from sklearn.metrics import mean_squared_error, mean_absolute_error

import matplotlib.pyplot as plt

plt.style.use(["science", "no-latex"])

SMALL_SIZE = 3
MEDIUM_SIZE = 10
BIGGER_SIZE = 20

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

import numpy as np

import const_define as cd
from data_loader import DataLoaderFactory
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json
from utility.python_utils import merge


def compute_ground_truth_distribution(df, target_column, total_kb_size):
    targets = df[target_column].values
    targets = [ast.literal_eval(item) for item in targets if type(item) != float]
    targets = [item for item in targets if item is not None and item]

    flatten_targets = [item for seq in targets for item in seq]
    target_count = Counter(flatten_targets)
    for kb_index in range(total_kb_size):
        if kb_index not in target_count:
            target_count[kb_index] = 0

    target_count = list(target_count.items())
    target_count = sorted(target_count, key=lambda pair: pair[0])
    target_count = [item[1] for item in target_count]

    target_dist = target_count / np.sum(target_count)
    sort_indexes = np.argsort(target_dist)[::-1]

    return target_dist[sort_indexes], sort_indexes


def get_chunked_weights(filename, chunk_rate):
    data = np.load(filename)
    total_epochs = data.shape[0]

    indexes = []
    current_rate = 0.0
    while current_rate <= 1.0:
        current_index = np.ceil(total_epochs * current_rate).astype(int)
        current_index = min(current_index, total_epochs - 1)

        if current_index not in indexes:
            indexes.append(current_index)

        current_rate += chunk_rate

    print("Total chunks -> ", len(indexes))

    return [data[index] for index in indexes]


def get_all_weights(filename):
    return np.load(filename)


def plot_weights(avg_weights, std_weights, target_dist, sort_indexes):

    fig, axes = plt.subplots(avg_weights.shape[0], 1)
    fig.text(0.5, 0.03, 'Memory Slots', ha='center', va='center')
    fig.text(0.03, 0.5, 'Priority', ha='center', va='center', rotation='vertical')

    for el_idx, ax in enumerate(axes):
        ax_avg_data = avg_weights[el_idx, :]
        ax_avg_data = ax_avg_data[sort_indexes]
        ax_std_data = std_weights[el_idx, :]
        ax_std_data = ax_std_data[sort_indexes]
        ax.plot(ax_avg_data,
                # marker='x',
                linestyle='--',
                markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)
        ax.plot(target_dist,
                # marker='^',
                markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)

        ax.fill_between(np.arange(avg_weights.shape[1]),
                        np.maximum(ax_avg_data - ax_std_data, 0.),
                        ax_avg_data + ax_std_data,
                        alpha=0.20)

        if el_idx == avg_weights.shape[0] - 1:
            fig.legend(['Model', 'Train Truth'], loc='upper right')
            # ax.legend(['Model', 'Train Truth'], loc='best')


model_type = 'experimental_basic_memn2n_v2'
test_name = "ToS-30_LTD_WS_Prio5_AttOnly_Filter"
target_column = "LTD_targets"
fold_to_show = 2
chunk_rate = 0.10  # we plot the distribution at each epoch that corresponds to this rate w.r.t. all epochs

is_local = True

if is_local:
    model_path = os.path.join(cd.CV_DIR, model_type, test_name)
else:
    model_path = os.path.join('/mnt/hdd/MemBERT/cv_test/', model_type, test_name)

model_config = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

# Sampler weight retriever frequency
saving_frequency = load_json(os.path.join(model_path, cd.JSON_CALLBACKS_NAME))['samplerweightsretriever']['frequency']

# Loading data
data_loader_config = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
data_loader_type = data_loader_config['type']
data_loader_info = data_loader_config['configs'][data_loader_type]
loader_additional_info = {key: value['value'] for key, value in model_config.items()
                          if 'data_loader' in value['flags']}
data_loader_info = merge(data_loader_info, loader_additional_info)

data_loader = DataLoaderFactory.factory(data_loader_type)
data_handle = data_loader.load(**data_loader_info)

# Get total kb size
data_targets = data_handle.data[target_column].values
data_targets = [ast.literal_eval(item) for item in data_targets if type(item) != float]
data_targets = [item for seq in data_targets for item in seq]
total_kb_size = len(set(data_targets))

# CV
cv_test_config = load_json(os.path.join(model_path, cd.JSON_CV_TEST_CONFIG_NAME))
cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
cv.load_folds(load_path=folds_path)

for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
    if fold_to_show == fold_idx or fold_to_show == -1:

        train_df, val_df, test_df = data_handle.get_split(key=cv_test_config['split_key'],
                                                          key_values=test_indexes,
                                                          val_indexes=val_indexes,
                                                          validation_percentage=cv_test_config['validation_percentage'])

        target_dist, sort_indexes = compute_ground_truth_distribution(df=train_df,
                                                                      target_column=target_column,
                                                                      total_kb_size=total_kb_size)

        filenames = [item for item in os.listdir(model_path) if
                     'sampler_weights' in item and 'fold_{}'.format(fold_to_show if fold_to_show >= 0 else fold_idx) in item]

        print("Found {} files".format(len(filenames)))

        total_chunked_weights = []

        for filename in filenames:
            chunked_weights = get_chunked_weights(os.path.join(model_path, filename), chunk_rate)
            # chunked_weights = get_all_weights(os.path.join(model_path, filename))
            total_chunked_weights.append(chunked_weights)

        total_chunked_weights = np.array(total_chunked_weights)
        avg_chunked_weights = np.mean(total_chunked_weights, axis=0)
        std_chunked_weights = np.std(total_chunked_weights, axis=0)

        # Plot
        plot_weights(avg_chunked_weights, std_chunked_weights, target_dist, sort_indexes)
        plt.show()
        # plt.savefig("{}.jpg".format(test_name), dpi=1200, bbox_inches='tight')

        # Errors
        mse_errors = [mean_squared_error(y_pred=pred_weights, y_true=target_dist)
                      for pred_weights in avg_chunked_weights]
        plt.plot(mse_errors)
        plt.show()