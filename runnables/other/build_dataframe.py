"""

@Author: Federico Ruggeri

@Date: 22/07/2019

Builds a single pandas.DataFrame containing all the dataset (ToS)

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from old_stuff import loader

import const_define as cd

# Settings
base_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'ToS_100')
data_suffix = 'sentences'
labels_suffix = 'labels'

df_dict = {}

# Load document order
doc_list_path = os.path.join(cd.PREBUILT_FOLDS_DIR, 'tos_100_splits_10_kfold.txt')
with open(doc_list_path, 'r') as f:
    doc_list = f.readlines()

doc_ids = [(doc.split('.txt')[0], idx) for idx, doc in enumerate(doc_list)]
doc_ids = OrderedDict(doc_ids)

# Load unfair categories
categories = ['A', 'CH', 'CR', 'J', 'LAW', 'LTD', 'PINC', 'TER', 'USE']

for filename in os.listdir(os.path.join(base_path, data_suffix)):
    data_sub_path = os.path.join(base_path, data_suffix, filename)
    labels_sub_path = os.path.join(base_path, labels_suffix, filename)

    doc_name = filename.split('.txt')[0]

    doc_sentences = loader.load_file_data(data_sub_path)
    df_dict.setdefault('text', []).extend(doc_sentences)

    df_dict.setdefault('document', []).extend([doc_name] * len(doc_sentences))
    df_dict.setdefault('document_ID', []).extend([doc_ids[doc_name]] * len(doc_sentences))

    # load unfair categories
    category_labels = []
    for category in categories:
        cat_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'ToS_100', 'labels_{}'.format(category))
        cat_data = loader.load_data_labels(os.path.join(cat_path, filename))
        df_dict.setdefault(category, []).extend(cat_data)
        category_labels.append(cat_data)

    # verify labels logical OR (takes into account Nov'19 update)
    doc_labels = loader.load_data_labels(labels_sub_path)

    category_labels = np.array(category_labels)
    or_labels = np.logical_or.reduce(category_labels)
    or_labels = or_labels.astype(np.int32)

    integrity_condition = np.equal(doc_labels, or_labels)
    if not integrity_condition.all():
        df_dict.setdefault('label', []).extend(or_labels.tolist())
        fault_indexes = np.argwhere(integrity_condition == False).ravel()
        print('[{0}] Logical OR was found different! Indexes: {1}'.format(filename, fault_indexes))
    else:
        df_dict.setdefault('label', []).extend(doc_labels)

df = pd.DataFrame.from_dict(df_dict)
df_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'ToS_100', 'dataset.csv')
df.to_csv(df_path)
