import os
import pandas as pd
import const_define as cd
import numpy as np
from pycorenlp import StanfordCoreNLP
from tqdm import tqdm
import json


def parse_corenlp(sentences, corenlp):
    parsed_sentences = []
    for sentence in tqdm(sentences):

        sentence = sentence.replace('%', '%25')
        sentence = sentence.replace("\\+", "%2B")
        sentence = sentence.strip()

        annotation = corenlp.annotate(sentence, properties={'timeout': 100000000, 'annotators': 'tokenize,ssplit'})
        if annotation:
            try:
                data = json.loads(annotation)
            except Exception as e:
                print('Failed: ', annotation)

            # Sentences
            tokens = data['sentences'][0]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            parsed_sentences.append(parsed_sentence)

    return parsed_sentences


data_path = os.path.join(cd.IBM2015_DIR, 'selected_subdataset.csv')
data = pd.read_csv(data_path)

# Exclude Evidence
memory = data.query("E_Label == 1")
claim_dataset = data.query("C_Label == 1")
valid_topics = set(data.Topic.values)

print("Memory -> ", memory.shape)
print("Claim data -> ", claim_dataset.shape)

memory_save_path = os.path.join(cd.IBM2015_DIR, 'evidence_memory.csv')
claim_save_path = os.path.join(cd.IBM2015_DIR, 'claim_data.csv')

memory.to_csv(memory_save_path, index=False)
claim_dataset.to_csv(claim_save_path, index=False)

# Build targets for SS
orig_evidence_df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'evidence.txt'), sep='\t',
                               names=["Topic", "Claim", "Evidence", "Tag"])
orig_evidence_df = orig_evidence_df[orig_evidence_df.Topic.isin(valid_topics)]
orig_evidences = orig_evidence_df.Evidence.values
orig_claims = orig_evidence_df.Claim.values

# Parse original data
port = 9000
nlp = StanfordCoreNLP('http://localhost:{}'.format(port))

orig_evidences = parse_corenlp(orig_evidences, nlp)
orig_claims = parse_corenlp(orig_claims, nlp)

data_claims = claim_dataset.query('C_Label == 1').Sentence.values
data_evidences = memory.Sentence.values

targets = []

for orig_claim, orig_evidence in zip(orig_claims, orig_evidences):
    match_evidence_index = np.where(data_evidences == orig_evidence)
