import argparse
import os
from collections import OrderedDict

import numpy as np
import pandas as pd

import const_define as cd
from utility.data_utils import get_data_config_id
from utility.json_utils import load_json
from utility.log_utils import get_logger
from utility.plot_utils import show_target_coverage, show_voting_coverage, MemoryMetrics
from utility.python_utils import merge, flatten
from itertools import combinations
import ast
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.metrics import precision_recall_curve, roc_auc_score

plt.style.use(["science", "no-latex"])

SMALL_SIZE = 3
MEDIUM_SIZE = 12
BIGGER_SIZE = 20

plt.rc('font', size=BIGGER_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

logger = get_logger(__name__)


def compute_coverage_info(attention_weights, predictions, attention_mode, category, K_repetition_ids, R=3,
                          target_column=None, threshold=0.5):
    target_column = '{}_targets'.format(category) if target_column is None else target_column

    total_usage, total_hits, total_correct, total_correct_and_hit, \
    total_correct_and_no_hit, total_samples, \
    avg_memory_percentage = 0, 0, 0, 0, 0, 0, 0
    total_mrr = []
    total_top_R_hits = np.zeros(R)

    for attention_weight in attention_weights:
        sub_path = os.path.join(model_path, attention_weight)
        loaded_weights = load_json(sub_path)
        fold_name = attention_weight.split('fold')[1].split('_')[1]

        fold_test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold_name)))
        repetition_id = K_repetition_ids[int(fold_name)]

        iter_preds = predictions[fold_name][repetition_id]

        if type(iter_preds) in [dict, OrderedDict]:
            iter_preds = iter_preds['label_id_{}'.format(category)]

        fold_stats = show_target_coverage(
            attention=loaded_weights,
            test_df=fold_test_df,
            fold_name=fold_name,
            predictions=iter_preds,
            category=category,
            attention_mode=attention_mode,
            verbose=0,
            target_column=target_column,
            R=R,
            threshold=threshold)

        total_usage += fold_stats.usage
        total_hits += fold_stats.hits
        total_correct += fold_stats.correct
        total_correct_and_hit += fold_stats.correct_and_hit
        total_correct_and_no_hit += fold_stats.correct_and_no_hit
        total_samples += fold_stats.samples
        total_top_R_hits += fold_stats.top_R_hits
        avg_memory_percentage += fold_stats.avg_memory_percentage
        total_mrr.append(fold_stats.mrr)

    memory_usage = total_usage / total_samples
    coverage = total_hits / total_samples
    recall = total_correct / total_samples
    supervision_precision = total_correct_and_hit / total_samples
    non_memory_accuracy = (total_correct - total_correct_and_hit - total_correct_and_no_hit) / total_samples
    mrr = np.mean(total_mrr)

    try:
        coverage_precision = total_hits / total_usage
        memory_precision = total_correct_and_hit / total_usage
        avg_memory_percentage = avg_memory_percentage / total_usage
        top_R_coverage_precision = total_top_R_hits / total_usage
    except ZeroDivisionError:
        coverage_precision = None
        memory_precision = None
        avg_memory_percentage = None
        top_R_coverage_precision = None

    metrics = MemoryMetrics(memory_usage,
                            coverage,
                            coverage_precision,
                            recall,
                            memory_precision,
                            supervision_precision,
                            non_memory_accuracy,
                            top_R_coverage_precision,
                            avg_memory_percentage,
                            mrr)
    return metrics


def get_per_fold_top_K_indexes(metric_data, K):
    if type(metric_data) is not np.ndarray:
        metric_data = np.array(metric_data)

    metric_data = metric_data.transpose()

    if K == 1:
        return np.argmax(metric_data, axis=1)[:, np.newaxis]

    best_indexes = np.argsort(metric_data, axis=1)[:, ::-1]
    best_indexes = best_indexes[:, :K]

    return best_indexes


def get_config_path(network_args, data_loader_info, data_name):
    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name, should_exist=True)
    config_path = os.path.join(model_base_path, str(config_id))

    return config_path


def show_coverage_for_category(category, target_column=None, threshold=0.5):
    attention_weights = [name for name in os.listdir(model_path)
                         if 'attention_weights' in name.lower() and set_suffix in name]

    predictions = load_json(os.path.join(model_path, cd.JSON_PREDICTIONS_NAME))

    loaded_val_results = load_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME))
    metric_val_results = loaded_val_results[retrieval_metric]

    repetition_ids = get_per_fold_top_K_indexes(metric_val_results, top_K)

    attention_mode = \
        load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))['extraction_info']['value'][
            'mode']

    # Special cases
    if 'discriminative' in model_type.lower():
        attention_mode = 'sigmoid'

    total_metrics = []

    # Check inference repetitions
    if any(['inference-rep' in item for item in attention_weights]):
        inference_reps = 1
        for item in attention_weights:
            item_tokens = item.split('_')
            item_inference_rep = None
            for token_idx, token in enumerate(item_tokens):
                if token == 'inference-rep':
                    item_inference_rep = int(item_tokens[token_idx + 1])
                    break
            if item_inference_rep > inference_reps:
                inference_reps = item_inference_rep

        for inference_rep in range(inference_reps + 1):
            for K in range(top_K):
                K_attention_weights = [name for name in attention_weights
                                       if 'top{}'.format(K + 1) in name
                                       and 'inference-rep_{}'.format(inference_rep) in name]
                K_metrics = compute_coverage_info(
                    attention_weights=K_attention_weights,
                    predictions=predictions,
                    attention_mode=attention_mode,
                    K_repetition_ids=repetition_ids[:, K],
                    category=category,
                    target_column=target_column,
                    R=ranking,
                    threshold=threshold)

                total_metrics.append(K_metrics._asdict())
    else:
        for K in range(top_K):
            K_attention_weights = [name for name in attention_weights
                                   if 'top{}'.format(K + 1) in name]
            K_metrics = compute_coverage_info(
                attention_weights=K_attention_weights,
                predictions=predictions,
                attention_mode=attention_mode,
                K_repetition_ids=repetition_ids[:, K],
                category=category,
                target_column=target_column,
                R=ranking,
                threshold=threshold)

            total_metrics.append(K_metrics._asdict())

    return total_metrics


def plot_metric_data(data, metric_name, total_data, markers, baseline_statistics, show_std=False):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(18.5, 10.5)

    legend_values = []
    total_samples = None

    # baseline_colours = ["black", "dimgray", "silver"]

    # for idx, (mem_slot, baseline_metrics) in enumerate(baseline_statistics):
    #     ax.axhline(y=baseline_metrics._asdict()[metric_name[len('avg_'):]], markersize=MEDIUM_SIZE,
    #                linewidth=SMALL_SIZE, color=baseline_colours[idx])
    #
    #     legend_values.append("Baseline @{}".format(idx + 1))

    for marker, (test_name, test_metric_data) in zip(markers, data.items()):
        test_metric_data = [seq for seq in test_metric_data if seq is not None]
        if 'top_R_coverage_precision' in metric_name:
            test_metric_data = np.array(test_metric_data)[:, 2]
        else:
            test_metric_data = np.array(test_metric_data)
        ax.plot(test_metric_data, marker=marker, linestyle='--', markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)

        if show_std:
            # test_min_metric_data = np.array(total_data['min_{}'.format(metric_name[len('avg_'):])][test_name])
            # test_max_metric_data = np.array(total_data['max_{}'.format(metric_name[len('avg_'):])][test_name])
            test_std_metric_data = np.array(total_data['std_{}'.format(metric_name[len('avg_'):])][test_name])

            if 'top_R_coverage_precision' in metric_name:
                test_std_metric_data = test_std_metric_data[:, 2]

            ax.fill_between(np.arange(len(test_std_metric_data)),
                            np.maximum(test_metric_data - test_std_metric_data, 0.),
                            test_metric_data + test_std_metric_data,
                            alpha=0.15)

        legend_values.append(name_substitutions[test_name] if test_name in name_substitutions else test_name)
        total_samples = len(test_metric_data)

    # ax.legend(legend_values)

    ax.set_xticks(np.arange(total_samples))
    ax.set_xticklabels(['{:.2f}'.format(item) for item in thresholds])

    ax.set_xlabel("Threshold")

    if 'top_R_coverage_precision' in metric_name:
        metric_name = 'P@3'

    ax.set_ylabel(metric_name)


def plot_matched_data(metrics_pair, total_data, markers, baseline_statistics):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(18.5, 10.5)

    legend_values = []

    baseline_colours = ["black", "dimgray", "silver"]

    # X Data
    x_metric = 'avg_' + metrics_pair[0]
    x_metric_data = total_data[x_metric]

    # Y Data
    y_metric = 'avg_' + metrics_pair[1]
    y_metric_data = total_data[y_metric]

    for marker, (x_test_name, x_test_metric_data), (y_test_name, y_test_metric_data) \
            in zip(markers, x_metric_data.items(), y_metric_data.items()):
        ax.plot(x_test_metric_data, y_test_metric_data, marker=marker, linestyle='--', markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)
        legend_values.append(x_test_name)

    for idx, (mem_slot, baseline_metrics) in enumerate(baseline_statistics):
        x_baseline_metric_data = baseline_metrics._asdict()[x_metric[len('avg_'):]]
        y_baseline_metric_data = baseline_metrics._asdict()[y_metric[len('avg_'):]]

        ax.plot(x_baseline_metric_data, y_baseline_metric_data, markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE, color=baseline_colours[idx])

        legend_values.append("Baseline @{}".format(idx + 1))

    ax.legend(legend_values)

    ax.set_xlabel(x_metric)
    ax.set_ylabel(y_metric)


def plot_roc_curve(per_model_attention, per_model_targets, random_repetitions=3):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(18.5, 10.5)

    legend_values = []

    # Random baseline
    random_shape = None
    random_true_values = None
    auc_roc_values = {}

    for key, pred_values in per_model_attention.items():
        true_values = per_model_targets[key]
        precision, recall, thresholds = precision_recall_curve(y_true=true_values,
                                                               probas_pred=pred_values,
                                                               pos_label=1)
        ax.plot(recall, precision, markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)

        legend_values.append(key)

        auc_roc_values[key] = roc_auc_score(y_true=true_values,
                                            y_score=pred_values)

        if random_shape is None:
            random_shape = len(pred_values)
            random_true_values = true_values

    random_preds = []
    for rep in range(random_repetitions):
        rep_preds = np.random.random(random_shape)
        random_preds.append(rep_preds)
    random_preds = np.mean(random_preds, axis=0)

    random_key = 'Random@{}'.format(random_repetitions)

    precision, recall, thresholds = precision_recall_curve(y_true=random_true_values,
                                                           probas_pred=random_preds,
                                                           pos_label=1)
    ax.plot(recall, precision, markersize=MEDIUM_SIZE,
            linewidth=SMALL_SIZE, color="silver")

    auc_roc_values[random_key] = roc_auc_score(y_true=random_true_values,
                                               y_score=random_preds)

    legend_values.append(random_key)

    ax.legend(legend_values)
    ax.set_xlabel('Recall')
    ax.set_ylabel('Precision')

    print("Area under curve values: \n")
    auc_roc_values = sorted(auc_roc_values.items(), key=lambda pair: pair[1], reverse=True)
    for key, value in auc_roc_values:
        print("{0}: {1}".format(key, value))


def plot_ranking_coverage_precision(top_R_coverage_precision, markers):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(18.5, 10.5)

    bar_width = 0.15
    ranking_checkpoint_rate = 0.05
    bar_x_position = None
    all_positions = []

    legend_values = []

    for marker, (test_name, test_metric_data) in zip(markers, top_R_coverage_precision.items()):
        test_metric_data = [seq for seq in test_metric_data if seq is not None]
        test_metric_data = np.array(test_metric_data)[min(5, len(test_metric_data) - 1), :]  # pick threshold 0.0

        # Filter values
        checkpoint_indexes = np.arange(0, test_metric_data.shape[0], int(np.ceil(test_metric_data.shape[0] * ranking_checkpoint_rate)))
        test_metric_data = test_metric_data[checkpoint_indexes]
        if bar_x_position is None:
            bar_x_position = checkpoint_indexes
        else:
            bar_x_position = bar_x_position + bar_width

        all_positions.append(bar_x_position)

        ax.plot(test_metric_data, marker=marker, linestyle='--', markersize=MEDIUM_SIZE,
                linewidth=SMALL_SIZE)

        legend_values.append(name_substitutions[test_name] if test_name in name_substitutions else test_name)

    ax.legend(legend_values)

    ax.set_xticks(checkpoint_indexes)
    ax.set_xticklabels(checkpoint_indexes + 1)

    ax.set_xlabel("K")
    ax.set_ylabel('P@K')


def compute_baseline_statistics(model_path, memory_slots, folds, category, target_column, heads=2):
    pred_combs = []

    comb_lengths = [1]

    memory_slots = np.arange(memory_slots)

    for comb_length in comb_lengths:
        combs = combinations(memory_slots, comb_length)
        pred_combs.extend(combs)

    comb_results = {}

    def get_hits(target, predicted):
        target = set(target)
        predicted = set(predicted)
        intersection = predicted.intersection(target)
        hits = len(intersection)
        missed = target.difference(intersection)
        others = predicted.difference(intersection)
        return hits, missed, others

    for pred_id, pred_comb in enumerate(tqdm(pred_combs)):

        R = 3
        total_usage, total_hits, total_correct, total_correct_and_hit, \
        total_correct_and_no_hit, total_samples, \
        avg_memory_percentage = 0, 0, 0, 0, 0, 0, 0
        total_top_R_hits = np.zeros(R)

        for fold in range(folds):
            fold_usage = 0
            fold_hits = 0
            fold_top_R_hits = [0] * R
            fold_correct = 0
            fold_correct_and_hit = 0
            fold_correct_and_no_hit = 0
            fold_avg_memory_percentage = 0
            fold_real_samples = 0

            fold_test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold)))
            unfair_data = fold_test_df[fold_test_df[category] == 1]

            for row_id, row in unfair_data.iterrows():
                target_values = row[target_column]
                if not ast.literal_eval(target_values):
                    continue
                fold_real_samples += 1
                target_values = [int(item) for item in target_values[1:-1].split(',')]

                predicted_label = 1
                true_label = 1

                hits, missed, others = get_hits(target=target_values, predicted=pred_comb)

                if len(pred_comb) > 0:
                    fold_usage += 1

                    top_R_predictions = pred_comb[:R]
                    fold_top_R_hits = [
                        count + 1 if len(set(top_R_predictions[:idx + 1]).intersection(set(target_values)))
                        else count
                        for idx, count in enumerate(fold_top_R_hits)]

                    fold_avg_memory_percentage += len(set(pred_comb)) / len(memory_slots)

                    if hits > 0:
                        fold_hits += 1
                        fold_correct_and_hit += int(predicted_label == true_label)
                    else:
                        fold_correct_and_no_hit += int(predicted_label == true_label)

                fold_correct += int(predicted_label == true_label)

            fold_top_R_hits = np.array(fold_top_R_hits)

            total_usage += fold_usage
            total_hits += fold_hits
            total_correct += fold_correct
            total_correct_and_hit += fold_correct_and_hit
            total_correct_and_no_hit += fold_correct_and_no_hit
            total_samples += fold_real_samples
            total_top_R_hits += fold_top_R_hits
            avg_memory_percentage += fold_avg_memory_percentage

        memory_usage = total_usage / total_samples
        coverage = total_hits / total_samples
        recall = total_correct / total_samples
        supervision_precision = total_correct_and_hit / total_samples
        non_memory_accuracy = (total_correct - total_correct_and_hit - total_correct_and_no_hit) / total_samples

        try:
            coverage_precision = total_hits / total_usage
            memory_precision = total_correct_and_hit / total_usage
            avg_memory_percentage = avg_memory_percentage / total_usage
            top_R_coverage_precision = total_top_R_hits / total_usage
        except ZeroDivisionError:
            coverage_precision = None
            memory_precision = None
            avg_memory_percentage = None
            top_R_coverage_precision = None

        metrics = MemoryMetrics(memory_usage,
                                coverage,
                                coverage_precision,
                                recall,
                                memory_precision,
                                supervision_precision,
                                non_memory_accuracy,
                                top_R_coverage_precision[-1],
                                avg_memory_percentage,
                                0)

        comb_results[pred_id] = metrics

    sorted_results = sorted(comb_results.items(), key=lambda item: item[1].coverage, reverse=True)[:heads]

    return sorted_results


model_type = 'ibm2015_memory-distilbert-base-uncased'
test_names = [
    # "topic_1_WS-MLP-512",
    'topic_1_WS_Unif10',
    "topic_1_WS_Prio10_AttOnly_Filter",
    'topic_1_WS_Prio10_LossGain_Filter',
    # "topic_1_SS-MLP-512",
    "topic_1_SS_Unif10",
    "topic_1_SS_Prio10_AttOnly_Filter",
    "topic_1_SS_Prio10_LossGain_Filter",
]

markers = [
    'X',
    '^',
    'o',
    'd',
    'v',
    '*',
    'P',
    'v'
]

name_substitutions = {
    'topic_1_WS_Prio10_AttOnly_Filter': 'MemBERT (WS) (P-10-Att-F)',
    'topic_1_WS_Prio10_LossGain_Filter': 'MemBERT (WS) (P-10-LG-F)',
    'topic_1_WS_Unif10': 'MemBERT (WS) (U-10)',
    'topic_1_SS_Unif10': 'MemBERT (SS) (U-10)',
    'topic_1_SS_Prio10_AttOnly_Filter': 'MemBERT (SS) (P-10-Att-F)',
    'topic_1_SS_Prio10_LossGain_Filter': 'MemBERT (SS) (P-10-LG-F)'
}

retrieval_metric = 'F1'
set_suffix = 'test'
top_K = 1
ranking = 20
show_std = False
label_column = 'C_Label'
memory_slots = 130
baseline_heads = 2
folds = 4
target_column = 'evidence_targets'
thresholds = np.arange(0.0, 0.6, 0.05)
metrics_to_show = [
    "top_R_coverage_precision",
    # "memory_precision",
    # "coverage_precision",
    # "coverage",
    # "memory_usage",
    # "non_memory_precision",
    # 'supervision_precision'
]
# matched_metrics = [
#     ("coverage", "coverage_precision")
# ]

# Parser
parser = argparse.ArgumentParser(description="Argument Parser")
parser.add_argument("--model_type", help="Model architecture name."
                                         " Check const_define.py -> MODEL_CONFIG variable for more info.",
                    type=str, default=model_type)
parser.add_argument("--test_names", help="Test unique IDs (folder names).", nargs='+', default=test_names)
parser.add_argument("--retrieval_metric", help="Metric criterium for best runs retrieval for each fold.",
                    type=str, default=retrieval_metric)
parser.add_argument("--set_suffix", help="Which dataset split to consider: train, val, test",
                    type=str, default=set_suffix)
parser.add_argument("--top_K", help="How many top runs for which attention scores should be collected.",
                    type=int, default=top_K)
parser.add_argument("--ranking", help="How many top memory hits to consider when computing coverage precision",
                    type=int, default=ranking)
args = parser.parse_args()

model_type = args.model_type
test_names = args.test_names
retrieval_metric = args.retrieval_metric
set_suffix = args.set_suffix
top_K = args.top_K
ranking = args.ranking

logger.info("Debugging argparse...")
for key, value in args.__dict__.items():
    logger.info("Arg: {0} -- Value: {1}".format(key, value))
logger.info("End of debugging... {}".format(os.linesep))

total_metric_result = {}

# Baseline statistics
baseline_statistics = compute_baseline_statistics(model_path=os.path.join(cd.CV_DIR, model_type, test_names[0]),
                                                  memory_slots=memory_slots,
                                                  folds=folds,
                                                  heads=baseline_heads,
                                                  category=label_column,
                                                  target_column=target_column)

per_model_attention = {}
per_model_targets = {}

# Model statistics
for test_name in tqdm(test_names):
    # print("Considering -> ", test_name)

    model_path = os.path.join(cd.CV_DIR, model_type, test_name)
    converter_type = cd.MODEL_CONFIG[model_type]['converter']

    loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
    loader_info = loader_info['configs'][loader_info['type']]

    # Get targets
    fold_target_indexes = {}

    for fold in range(folds):
        test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold)))
        test_df = test_df[test_df[label_column] == 1]
        fold_targets = test_df[target_column].values
        fold_targets = [[item_idx, ast.literal_eval(item)] for item_idx, item in zip(test_df.index.values, fold_targets)
                        if type(item) != float]
        fold_target_indexes[fold] = [item[0] for item in fold_targets]
        fold_targets = [item[1] for item in fold_targets]
        fold_target_values = [[1 if idx in seq else 0 for idx in range(memory_slots)]
                              for seq in fold_targets]
        fold_target_values = np.array(fold_target_values).ravel()
        per_model_targets.setdefault(test_name, []).extend(fold_target_values.tolist())

    # Get attention values
    total_attention_weights = [name for name in os.listdir(model_path)
                               if 'attention_weights' in name.lower() and set_suffix in name
                               and 'top1' in name]

    if any(['inference-rep' in item for item in total_attention_weights]):
        inference_reps = 1
        for item in total_attention_weights:
            item_tokens = item.split('_')
            item_inference_rep = None
            for token_idx, token in enumerate(item_tokens):
                if token == 'inference-rep':
                    item_inference_rep = int(item_tokens[token_idx + 1])
                    break
            if item_inference_rep > inference_reps:
                inference_reps = item_inference_rep

        avg_attention_weights = []
        for fold in range(folds):

            # Get positive indexes
            positive_indexes = fold_target_indexes[fold]

            rep_attention_weights = []
            for inference_rep in range(inference_reps + 1):
                attention_weights = [name for name in total_attention_weights
                                     if 'top1' in name
                                     and 'inference-rep_{}'.format(inference_rep) in name
                                     and 'fold_{}'.format(fold) in name]
                assert len(attention_weights) == 1
                attention_weights = load_json(os.path.join(model_path, attention_weights[0]))[positive_indexes]
                rep_attention_weights.append(attention_weights.ravel())

            avg_attention_weights = np.mean(rep_attention_weights, axis=0)
            per_model_attention.setdefault(test_name, []).extend(avg_attention_weights.tolist())
    else:
        for fold in range(folds):
            # Get positive indexes
            positive_indexes = fold_target_indexes[fold]

            attention_weights = [name for name in total_attention_weights
                                 if 'top1' in name
                                 and 'fold_{}'.format(fold) in name]
            assert len(attention_weights) == 1
            attention_weights = load_json(os.path.join(model_path, attention_weights[0]))[positive_indexes]
            per_model_attention.setdefault(test_name, []).extend(attention_weights.ravel().tolist())

    for threshold in thresholds:

        current_metric_result = None

        if 'category' not in loader_info:
            if 'categories' in loader_info:
                categories = loader_info['categories']
                for cat in categories:
                    logger.info("\n\n**** CATEGORY: {} ****\n\n".format(cat))
                    current_metric_result = show_coverage_for_category(cat, threshold=threshold)
            else:
                current_metric_result = show_coverage_for_category(label_column, target_column, threshold=threshold)
        else:
            category = loader_info['category']
            current_metric_result = show_coverage_for_category(category, threshold=threshold)

        assert current_metric_result is not None
        metric_block_keys = []

        current_metric_summary_result = {}

        for metric_block in current_metric_result:
            for key, value in metric_block.items():
                current_metric_summary_result.setdefault(key, []).append(value)
                metric_block_keys.append(key)

        # Compute Mean
        for key, value in current_metric_summary_result.items():
            total_metric_result.setdefault('avg_{}'.format(key), {}).setdefault(test_name, []).append(np.mean(value, axis=0) if value[0] is not None else None)

        # Compute Max/Min
        for key, value in current_metric_summary_result.items():
            total_metric_result.setdefault('max_{}'.format(key), {}).setdefault(test_name, []).append(
                np.max(value, axis=0) if value[0] is not None else None)
            total_metric_result.setdefault('min_{}'.format(key), {}).setdefault(test_name, []).append(
                np.min(value, axis=0) if value[0] is not None else None)

        # Compute Std
        for key, value in current_metric_summary_result.items():
            total_metric_result.setdefault('std_{}'.format(key), {}).setdefault(test_name, []).append(np.std(value, axis=0) if value[0] is not None else None)

# Plot
for metric, metric_data in total_metric_result.items():
    if metric.startswith('avg') and metric[len('avg_'):] in metrics_to_show:
        plot_metric_data(metric_data, metric, total_metric_result, show_std=show_std, markers=markers,
                         baseline_statistics=baseline_statistics)

        plt.savefig("{}.eps".format(metric), format='eps', dpi=600, bbox_inches='tight')

# Matched data
# for pair in matched_metrics:
#     plot_matched_data(metrics_pair=pair, total_data=total_metric_result,
#                       markers=markers, baseline_statistics=baseline_statistics)


# ROC curves
# plot_roc_curve(per_model_attention, per_model_targets)

# Top R coverage precision
plot_ranking_coverage_precision(total_metric_result['avg_top_R_coverage_precision'], markers=markers)
plt.savefig("avg_top_K_ranking.eps", format='eps', dpi=600, bbox_inches='tight')

plt.show()
