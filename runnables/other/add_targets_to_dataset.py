"""

Adds built targets to dataset.csv as a new column

** BEFORE RUNNING **

1. Change the 'category' variable as to match the unfairness category of your interest.
2. Make sure you have completed all the initial setup as reported in the repo README file.

"""

import os
import const_define as cd
import pandas as pd
from utility.json_utils import load_json
import numpy as np

category = 'CR'

targets = load_json(os.path.join(cd.KB_DIR, '{}_targets.json'.format(category)))

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))

original_text = df['text']

df['text'] = df['text'].apply(lambda sentence: sentence.strip())
df['{}_targets'.format(category)] = ''

for doc, clause, target_set in targets:
    print('Considering tuple: ', doc, ' ', clause, ' ', target_set)
    doc = doc.split('.xml')[0]
    clause = clause.strip()
    query_df = df.loc[(df.document == doc) & (df.text == clause)]

    if query_df.shape[0] != 1:
        print('Unexpected behaviour! Got multiple matches!')
        print('Checking if duplicates...')
        docs = np.unique(query_df.document.values)

        # Duplicate case
        if docs.shape[0] == 1:
            retrieved_indexes = query_df.index.values
            for idx in retrieved_indexes:
                df['{}_targets'.format(category)][idx] = target_set
        else:
            print('Unexpected behaviour confirmed! Got: {}'.format(query_df))
            break
    else:
        retrieved_index = query_df.index.values[0]
        df['{}_targets'.format(category)][retrieved_index] = target_set

    print('*' * 50)

print(df['{}_targets'.format(category)])

df['text'] = original_text

df.to_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'), index=False)