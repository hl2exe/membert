"""

Test reformer

"""

from reformer.reformers import TFReformerLM

model = TFReformerLM(
    num_tokens=1,
    emb=300,
    depth=6,
    max_seq_len=1000,
    heads=4,
    lsh_dropout=0.1,
    causal=True,
    bucket_size=64,
    n_hashes=4,
    ff_chunks=600,
    weight_tie=True,
    attn_chunks=8,
    use_full_attn=False
)

model.build(input_shape=(1, 1000))
model.summary()