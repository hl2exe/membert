import os
import const_define as cd
import ast
import pandas as pd
from tqdm import tqdm


def load_file_data(file_path):
    """
    Reads a file line by line.

    :param file_path: path to the file
    :return: list of sentences (string)
    """

    sentences = []

    with open(file_path, 'r') as f:
        for line in f:
            sentences.append(line)

    return sentences


if __name__ == '__main__':

    categories = ['A', 'CH', 'CR', 'LTD', 'TER']
    all_categories = ['A', 'CH', 'CR', 'J', 'LAW', 'LTD', 'PINC', 'TER', 'USE']
    suffix = '30'
    save_path = os.path.join(cd.TOS_100_DIR, 'pairwise_dataset_{0}_{1}.csv')
    no_suffix_save_path = os.path.join(cd.TOS_100_DIR, 'pairwise_dataset_{0}.csv')

    for category in tqdm(categories):
        # Read ToS
        df_path = os.path.join(cd.TOS_100_DIR, 'dataset_{}.csv'.format(suffix) if suffix is not None else 'dataset.csv')
        df = pd.read_csv(df_path)

        # Read KB
        kb_name = '{}_KB.txt'.format(category)
        kb_csv_name = '{}_explanations.csv'.format(category)

        kb_path = os.path.join(cd.KB_DIR, kb_name)
        kb_data = load_file_data(kb_path)

        # Build pairwise dataset
        augmented_df_dict = {}
        for row_idx, row in df.iterrows():
            # Get target info
            category_targets = row['{}_targets'.format(category)]
            if type(category_targets) == str:
                # Store a row for each target
                category_targets = ast.literal_eval(category_targets)
                target_explanations = [kb_data[target] for target in category_targets]
            else:
                target_explanations = []

            # Augment inputs for each explanation
            for explanation in kb_data:
                for cat in all_categories:
                    cat_label = row[cat]

                    # (clause, explanation) has negative label if explanation is non-target
                    if cat == category and explanation not in target_explanations:
                        cat_label = 0

                    augmented_df_dict.setdefault(cat, []).append(cat_label)

                augmented_df_dict.setdefault('document', []).append(row['document'])
                augmented_df_dict.setdefault('document_ID', []).append(row['document_ID'])
                augmented_df_dict.setdefault('label', []).append(row['label'])
                augmented_df_dict.setdefault('explanation', []).append(explanation)
                augmented_df_dict.setdefault('text', []).append(row['text'])
                augmented_df_dict.setdefault('example_ID', []).append(row_idx)

        augmented_df = pd.DataFrame.from_dict(augmented_df_dict)
        augmented_df.to_csv(save_path.format(category, suffix)
                            if suffix is not None else no_suffix_save_path.format(category),
                            index=False)