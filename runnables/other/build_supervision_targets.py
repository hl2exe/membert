"""

Automatically finds and builds supervision targets from:

    1. Explanations KB
    2. Annotated samples (xlsx format)

** BEFORE RUNNING **

1. Change the 'category' variable as to match the unfairness category of your interest.
2. Make sure the 'port' variable matches the CoreNLP server port.
3. Make sure you have completed all the initial setup as reported in the repo README file.

"""

import os
import simplejson as sj
import const_define as cd
import pandas as pd
from pycorenlp import StanfordCoreNLP
from utility.log_utils import get_logger
from utility.json_utils import save_json

logger = get_logger(__name__)


# Step 0: Settings

category = 'LTD'
port = 9000

# Step 1: Load explanation KB

# Load KB
kb_path = os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category))
with open(kb_path, 'r') as f:
    kb = f.readlines()

# Load category data
explanations = pd.read_csv(os.path.join(cd.KB_DIR, '{}_explanations.csv'.format(category)))
samples = pd.read_csv(os.path.join(cd.KB_DIR, '{}_samples.csv'.format(category)))

# Build KB_mappings
explanations_ids = explanations['Id'].values
explanations_ids = {key: idx for idx, key in enumerate(explanations_ids)}

# Step 2: Get annotated samples
parser = StanfordCoreNLP('http://localhost:{}'.format(port))

# Iterate over (clause, targets) pairs
annotated_sentences = []
for clause, targets, doc in zip(samples['Clause'].values, samples['Targets'].values, samples['Document'].values):
    logger.info('Considering: {}'.format(clause))

    # Parse and convert targets via explanations_ids
    target_set = targets.split(',')
    target_set = [item.strip() for item in target_set]
    target_set = [explanations_ids[item] for item in target_set]

    # Parse clause
    annotation = parser.annotate(clause, properties={'timeout': 10000000})
    if annotation:
        try:
            data = sj.loads(annotation)
        except Exception as e:
            logger.error('Failed to annotate clause: {}'.format(clause))

        # For each parsed sentence build (clause_sentence, targets)
        sentences_found = len(data['sentences'])
        logger.info('Sentences retrieved: {}'.format(sentences_found))
        for sent_idx in range(sentences_found):
            tokens = data['sentences'][sent_idx]['tokens']
            tokens = [item['word'] for item in tokens]
            parsed_sentence = ' '.join(tokens)
            parsed_sentence = parsed_sentence.lower()
            annotated_sentences.append((doc, parsed_sentence, target_set))

print(annotated_sentences)
save_json(os.path.join(cd.KB_DIR, '{}_targets.json'.format(category)), annotated_sentences)
