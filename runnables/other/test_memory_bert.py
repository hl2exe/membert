"""

Testing KB enhanced DistilledBERT with hugging's face ktrain model (wrapper for TF2)

"""

import os
from datetime import datetime

import numpy as np

import const_define as cd
from old_stuff import reporters
from data_loader import DataLoaderFactory
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger
from old_stuff.test_utils_v2 import ktrain_custom_cross_validation

logger = get_logger(__name__)

if __name__ == '__main__':

    # Settings

    model_name = 'distilbert-base-uncased'

    # Configs

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    cv_test_config['model_type'] = model_name

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    save_base_path = os.path.join(cd.CV_DIR, 'kb-' + model_name, current_date)

    if cv_test_config['save_model'] and not os.path.isdir(save_base_path):
        os.makedirs(save_base_path)

    # Data

    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    scores = ktrain_custom_cross_validation(validation_percentage=cv_test_config['validation_percentage'],
                                            data_handle=data_handle,
                                            cv=cv,
                                            training_config=training_config,
                                            model_name=model_name,
                                            error_metrics=cv_test_config['error_metrics'],
                                            error_metrics_additional_info=cv_test_config[
                                                'error_metrics_additional_info'],
                                            compute_test_info=True,
                                            save_predictions=True,
                                            save_model=cv_test_config['save_model'],
                                            save_path=save_base_path,
                                            split_key=cv_test_config['split_key'])

    # Validation
    logger.info('Average validation scores: {}'.format(
        {key: np.mean(item) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    if cv_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])

    # Test
    logger.info('Average test scores: {}'.format(
        {key: np.mean(item) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    if cv_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])

    # Predictions
    if cv_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])

    # Test config
    if cv_test_config['save_model']:
        reporters.save_training_info(folder=save_base_path, training_info=training_config)
        save_json(os.path.join(save_base_path, cd.JSON_CV_TEST_CONFIG_NAME), data=cv_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
