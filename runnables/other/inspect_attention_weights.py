"""

"""

from utility.json_utils import load_json
import numpy as np
import matplotlib.pyplot as plt
import os
import const_define as cd
import argparse
from utility.log_utils import get_logger

logger = get_logger(__name__)


def plot_max_histogram(data):
    max_values = np.max(data, axis=-1)
    fig, ax = plt.subplots(1, 1)
    ax.hist(max_values)
    ax.set_xlabel("Memory")
    ax.set_ylabel("#highest score")


def plot_average_distribution(data):
    average = np.mean(data, axis=0)
    std = np.std(data, axis=0)
    fig, ax = plt.subplots(1, 1)
    ax.plot(average)
    ax.fill_between(np.arange(average.shape[0]),
                    average - std,
                    average + std,
                    alpha=0.3)
    ax.set_xlabel("Memory")
    ax.set_ylabel("Average +/- Std score")


def plot_boundary_distribution(data):
    average = np.mean(data, axis=0)
    max = np.max(data, axis=0)
    min = np.min(data, axis=0)
    fig, ax = plt.subplots(1, 1)
    ax.plot(average)
    ax.fill_between(np.arange(average.shape[0]),
                    min,
                    max,
                    alpha=0.3)
    ax.set_xlabel("Memory")
    ax.set_ylabel("Min & Max areas")


def plot_average_memory_attention(data):
    avg = np.mean(data, axis=-1)
    fig, ax = plt.subplots(1, 1)
    ax.plot(avg)
    ax.set_xlabel("Samples")
    ax.set_ylabel("Average mem attention")


model_type = 'ibm2015_experimental_basic_memn2n_v2'
test_name = "topic_1_WS_Prio10_AttOnly"
set_suffix = 'val'
visualize = True

# Parser
parser = argparse.ArgumentParser(description="Argument Parser")
parser.add_argument("--model_type", help="Model architecture name."
                                         " Check const_define.py -> MODEL_CONFIG variable for more info.",
                    type=str, default=model_type)
parser.add_argument("--test_name", help="Test unique ID (date).", type=str, default=test_name)
parser.add_argument("--set_suffix", help="val or test",
                    type=str, default=set_suffix)
parser.add_argument("--visualize", help="If true, it shows generated plots. Otherwise, it saves them to file",
                    type=bool, default=visualize)
args = parser.parse_args()

model_type = args.model_type
test_name = args.test_name
set_suffix = args.set_suffix
visualize = args.visualize

logger.info("Debugging argparse...")
for key, value in args.__dict__.items():
    logger.info("Arg: {0} -- Value: {1}".format(key, value))
logger.info("End of debugging... {}".format(os.linesep))

model_path = os.path.join(cd.CV_DIR, model_type, test_name)

filenames = [filename for filename in os.listdir(model_path)
             if 'attention_weights' in filename and set_suffix in filename.split('_')
             and 'top1' in filename]
data_list = [np.squeeze(load_json(os.path.join(model_path, filename))) for filename in filenames]

for data, filename in zip(data_list, filenames):
    print("Filename -> ", filename)
    save_path = os.path.join(model_type, filename.replace('.json', '.pdf'))

    # # Max histogram
    # plot_max_histogram(data)
    #
    # # Avg distribution
    # plot_average_distribution(data)
    #
    # # Min/Max distribution
    plot_boundary_distribution(data)

    # Avg memory score over data
    plot_average_memory_attention(data)

    if not visualize:
        plt.savefig(save_path)

if visualize:
    plt.show()