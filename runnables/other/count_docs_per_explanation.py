
"""

For each explanation count the number of docs in which it appears

"""

import pandas as pd
import os
import const_define as cd
import numpy as np
from tqdm import tqdm

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))

grouped_df = df.groupby(by='document_ID')

category = 'A'
explanations_count = {}

for category in tqdm(['A', 'CH', 'CR', 'LTD', 'TER']):
    print()

    with open(os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category)), 'r') as f:
        kb = f.readlines()

    for exp_id in range(len(kb)):
        explanations_count[exp_id] = 0

    total_docs = 0
    for doc_id, doc_df in grouped_df:

        doc_df = doc_df[doc_df[category] == 1]
        total_docs += 1

        if doc_df.shape[0] == 0:
            continue

        targets = doc_df['{}_targets'.format(category)].values
        targets = list(map(lambda t: [int(item) for item in t[1:-1].split(',')], targets))
        targets = set(np.concatenate(targets))

        for target in targets:
            explanations_count[target] += 1

    print('*' * 50)
    print('Category: ', category)
    print(explanations_count)
    print('*' * 50)

print('Total docs: ', total_docs)