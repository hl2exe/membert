"""

@Author: Federico Ruggeri

@Date: 26/03/2019

Builds IBM2015 calibration folds (5 folds)

"""

import os

import numpy as np

import const_define as cd
from data_loader import IBM2015Loader
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json

if __name__ == '__main__':
    # Step 1: load dataset
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))['configs']

    loader = IBM2015Loader()

    topic_subset = 4
    data_handle = loader.load(topic_subset=topic_subset)

    train_data = data_handle.data

    y_values = train_data['C_Label'].values
    x_values = np.arange(y_values.shape[0])

    cv = PrebuiltCV(n_splits=4, cv_type='stratifiedkfold',
                    shuffle=True, random_state=None, held_out_key='test')
    cv.build_all_sets_folds(X=y_values, y=y_values, validation_n_splits=4)

    base_path = os.path.join(cd.PROJECT_DIR, 'prebuilt_folds')

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    folds_save_path = os.path.join(base_path, 'IBM2015_splits_4_kfold_subset_{}.json'.format(topic_subset))
    cv.save_folds(folds_save_path, tolist=True)

    list_save_path = os.path.join(base_path, 'IBM2015_splits_4_kfold_subset_{}.txt'.format(topic_subset))

    with open(list_save_path, 'w') as f:
        f.writelines(os.linesep.join(x_values.astype(str).tolist()))
