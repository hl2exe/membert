
import pandas as pd
from utility.json_utils import load_json
import os
import const_define as cd
import numpy as np
import ast

topic_subset = 4

df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'selected_subdataset_{}.csv'.format(topic_subset)))
memory = load_json(os.path.join(cd.IBM2015_DIR, 'evidence_topic_memory.json'))

topics = set(df.Topic.values)
selected_memory = {key: value for key, value in memory.items() if key in topics}

total_memory = []
offset = 0

for topic in topics:
    topic_memory = selected_memory[topic]
    ordered_topic_memory = sorted(topic_memory.items(), key=lambda item: item[1])
    ordered_topic_indexes = np.array([item[1] for item in ordered_topic_memory]) + offset
    ordered_topic_memory = [item[0] + os.linesep for item in ordered_topic_memory]
    total_memory.extend(ordered_topic_memory)

    # Re-map topic specific indexes
    topic_remap = {value_idx: value for value_idx, value in enumerate(ordered_topic_indexes)}
    topic_df = df[df.Topic == topic]
    topic_targets = topic_df['evidence_targets'].values
    topic_targets = [ast.literal_eval(item) for item in topic_targets]
    topic_targets = [[topic_remap[idx] for idx in item] for item in topic_targets]
    df.loc[topic_df.index.values, 'evidence_targets'] = topic_targets

    # Update offset
    offset += ordered_topic_indexes.shape[0]


# Save updated dataset
df.to_csv(os.path.join(cd.IBM2015_DIR, 'selected_subdataset_{}.csv'.format(topic_subset)),
          index=False)

# Save unified memory
save_path = os.path.join(cd.IBM2015_DIR, 'ibm2015_memory_{}.txt'.format(topic_subset))
with open(save_path, 'w') as f:
    f.writelines(total_memory)