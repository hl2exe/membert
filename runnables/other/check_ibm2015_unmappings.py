from utility.json_utils import load_json
import pandas as pd
import const_define as cd
import os
import numpy as np
from copy import deepcopy
from utility.json_utils import save_json

sentence_df = pd.read_csv(os.path.join(cd.IBM2015_DIR, 'corrected_sentence_dataset.csv'))

unmapped_claims = load_json(os.path.join(cd.IBM2015_DIR, 'unmapped_claims.json'))
true_unmapped_claims = deepcopy(unmapped_claims)

unmapped_evidences = load_json(os.path.join(cd.IBM2015_DIR, 'unmapped_evidences.json'))
true_unmapped_evidences = deepcopy(unmapped_evidences)

checkpoint_path_template = os.path.join(cd.IBM2015_DIR, 'checkpoint_parse_{}.npy')
parsed_claim_corrections = np.load(checkpoint_path_template.format("claim_corrections"), allow_pickle=True).item()


def check_unmapped_args(unmapped, true_unmapped, sentence_df, label, corrections=None):
    total_mapped = 0
    total_unmapped = 0
    total_already_tagged = 0
    total_not_tagged = 0

    for topic, arg_set in unmapped.items():
        print("---- Topic: {} ----".format(topic))

        found = 0
        already_tagged = 0
        not_tagged = 0

        topic_sentences = sentence_df[sentence_df.Topic == topic]['Sentence'].values
        arg_labels = sentence_df[sentence_df.Topic == topic][label].values
        for sentence, arg_label in zip(topic_sentences, arg_labels):
            for arg in arg_set:
                if arg in sentence or (corrections is not None and corrections[arg] in sentence):
                    found += 1

                    # Remove
                    try:
                        true_unmapped[topic].remove(arg)
                    except KeyError as e:
                        continue

                    # Check if already annotated as claim
                    if arg_label == 1:
                        already_tagged += 1
                    else:
                        not_tagged += 1

                    break

        print("Total found {0}/{1} -- Already tagged {2} -- Not tagged {3}".format(found, len(arg_set), already_tagged,
                                                                                   not_tagged))
        total_mapped += found
        total_unmapped += len(arg_set)
        total_already_tagged += already_tagged
        total_not_tagged += not_tagged

    print("Result -> {0}/{1} -- Already tagged {2} -- Not tagged {3}".format(total_mapped, total_unmapped,
                                                                             total_already_tagged, total_not_tagged))

    return true_unmapped


# Claims
true_unmapped_claims = check_unmapped_args(unmapped=unmapped_claims,
                                           true_unmapped=true_unmapped_claims,
                                           sentence_df=sentence_df,
                                           label='C_Label',
                                           corrections=parsed_claim_corrections)
true_unmapped_claims = {key: value for key, value in true_unmapped_claims.items() if len(value)}

print("True unmapped claims -> ", sum([len(item) for item in true_unmapped_claims.values()]))

error_save_path = os.path.join(cd.IBM2015_DIR, 'unmapped_{}.json')
save_json(error_save_path.format('claims'), true_unmapped_claims)

# Evidences
true_unmapped_evidences = check_unmapped_args(unmapped=unmapped_evidences,
                                              true_unmapped=true_unmapped_evidences,
                                              sentence_df=sentence_df,
                                              label='E_Label',
                                              corrections=None)
true_unmapped_evidences = {key: value for key, value in true_unmapped_evidences.items() if len(value)}

print("True unmapped evidences -> ", sum([len(item) for item in true_unmapped_evidences.values()]))

error_save_path = os.path.join(cd.IBM2015_DIR, 'unmapped_{}.json')
save_json(error_save_path.format('evidences'), true_unmapped_evidences)
