"""

Loads a pre-trained model and:

1. Gives predictions for given data
2. Associates used memory to each example in given data

"""

import os
import argparse
from utility.json_utils import load_json
import const_define as cd
from data_processor import TextKBProcessor
from tokenization import TokenizerFactory
from data_converter import KBConverter, DataConverterFactory
from utility.log_utils import get_logger
import pandas as pd
import numpy as np
from utility.pipeline_utils import get_dataset_fn
from nn_models_v2 import ModelFactory
from utility.python_utils import merge
from tensorflow.python.keras import backend as K
from custom_callbacks_v2 import AttentionRetriever
from sample_wrappers import FeatureFactory

# Step 1: Parse settings

parser = argparse.ArgumentParser()

parser.add_argument('-kb', '-kb_path', help="path where unfair rules are stored (default='.')", type=str, default=".")
parser.add_argument('-mt', '-model_type', help="name of the NN architecture (default='experimental_basic_memn2n_v2')",
                    type=str,
                    default="experimental_basic_memn2n_v2")
parser.add_argument('test_name', help="unique ID of pre-trained model to employ", type=str)
parser.add_argument('-mf', '-model_folder', help="folder where the pre-trained model and related info are stored (default='.')",
                    type=str, default=".")
parser.add_argument('-sp', '-save_prefix', help="test specific prefix concerning the training evaluation criteria"
                                                ": e.g. models trained via cross-validation will have a fold_*number* prefix (default=None)",
                    type=str, default=None)
parser.add_argument('-df', '-data_folder', help="path where input data for inference is stored (default='.')",
                    type=str, default=".")
parser.add_argument('data_name', help="name of the textual file storing input data (without file format suffix)",
                    type=str)
parser.add_argument('-rp', '-repetition_prefix', help="test specific prefix concerning the training evaluation criteria"
                                                      ":e.g. models obtained from a repeated evaluation criteria will"
                                                      " have a repetition_*number* prefix (default=None)",
                    type=str, default=None)
parser.add_argument('-lp', '-log_path', help="path where to create a log file storing script debug info (default='.')",
                    type=str,
                    default=".")
parser.add_argument('-pip', "-pipeline_info_path", help="path where pre-trained model pipeline info"
                                                        " (tokenization, data conversion, etc...) is stored. (default='.')",
                    type=str, default=".")

args = parser.parse_args()

kb_path = cd.KB_DIR
model_type = "experimental_gated_memn2n_v2"
test_name = "01-05-2020-12-09-01"
model_folder = cd.CV_DIR
model_path = os.path.join(model_folder, model_type, test_name)
save_prefix = "fold_0"
data_folder = cd.KB_DIR
data_name = "LTD_KB"
repetition_prefix = 0
log_path = None
pipeline_info_path = os.path.join(cd.TESTS_DATA_DIR, 'tos_100_task1kb', model_type, '12')

logger = get_logger(__name__, log_path=log_path)

# Step 2: Load data

logger.info('[Step 1] Loading data...')

data_loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))['configs']['task1_kb']
kb_category = data_loader_info['category']

with open(os.path.join(data_folder, '{}.txt'.format(data_name)), 'r') as f:
    input_data = f.readlines()

logger.info('Loaded dataset size: {}'.format(len(input_data)))

with open(os.path.join(kb_path, '{}_KB.txt'.format(kb_category.upper())), 'r') as f:
    kb_data = f.readlines()

logger.info('Loaded {0} knowledge base: {1} sentences'.format(kb_category.upper(), len(kb_data)))

loader_info = {
    'inference_labels': [0, 1],
    'data_name': data_name,
    'label': kb_category,
    'kb': {kb_category: kb_data}
}

# Step 3: Load pre-trained pipeline

logger.info('[Step 2] Retrieving model input pipeline...')

network_args = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))

test_prefix = None

# Build processor
processor_type = cd.MODEL_CONFIG[model_type]['processor']
processor = TextKBProcessor(loader_info=loader_info, retrieve_label=False)

# Build tokenizer
tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

# Build converter
converter_type = cd.MODEL_CONFIG[model_type]['converter']
converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
converter_args['feature_class'] = 'text_kb_features'
del converter_args['partial_supervision_info']
converter = KBConverter(**converter_args)

tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=pipeline_info_path,
                                                                                 prefix='{0}_{1}'.format(
                                                                                     test_prefix,
                                                                                     save_prefix)
                                                                                 if test_prefix is not None
                                                                                 else save_prefix)
converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
    filepath=pipeline_info_path,
    prefix='{0}_{1}'.format(test_prefix, save_prefix) if test_prefix is not None else save_prefix)

tokenizer.show_info(tokenizer_info)
logger.info('Converter info: \n{}'.format(converter_info))

# Step 4: Convert data

logger.info('[Step 3] Converting input data...')

training_config = load_json(os.path.join(model_path, cd.JSON_TRAINING_CONFIG_NAME))
distributed_info = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_CONFIG_NAME))

test_name = '{}_tf_data'.format(data_name)
if save_prefix:
    test_name += '_{}'.format(save_prefix)
test_filepath = os.path.join(data_folder, test_name)

test_df = pd.DataFrame.from_dict({'text': input_data})

if not os.path.isfile(test_filepath):
    logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

    # Processor
    test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

    # Tokenizer
    tokenizer.initalize_with_vocab(tokenizer_info['vocab'])

    # Conversion
    for key, value in converter_info.items():
        setattr(converter, key, value)
    setattr(converter, 'feature_class', FeatureFactory.supported_features['text_kb_features'])
    converter.convert_data(examples=test_data,
                           label_list=processor.get_labels(),
                           output_file=test_filepath,
                           has_labels=False,
                           tokenizer=tokenizer)

    # Create Datasets
test_data = get_dataset_fn(filepath=test_filepath,
                           batch_size=training_config['batch_size'],
                           name_to_features=converter.feature_class.get_mappings(converter_info, has_labels=False),
                           selector=converter.feature_class.get_dataset_selector(),
                           is_training=False,
                           prefetch_amount=distributed_info['prefetch_amount'])

# Step 5: Load pre-loaded model

logger.info('[Step 4] Loading pre-trained model')

additional_info = {
    'kb': kb_data
}

callbacks = [AttentionRetriever(model_path, save_suffix=save_prefix)]

network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                          if 'model_class' in value['flags']}
network_retrieved_args['additional_data'] = additional_info
network_retrieved_args['name'] = cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix']
if repetition_prefix is not None:
    network_retrieved_args['name'] += '_repetition_{}'.format(repetition_prefix)
network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

# Useful stuff
test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

logger.info('Total test steps: {}'.format(test_steps))

# Custom callbacks only
for callback in callbacks:
    if hasattr(callback, 'on_build_model_begin'):
        callback.on_build_model_begin(logs={'network': network})

text_info = merge(tokenizer_info, converter_info)
network.build_model(text_info=text_info)

# Custom callbacks only
for callback in callbacks:
    if hasattr(callback, 'on_build_model_end'):
        callback.on_build_model_end(logs={'network': network})

# Setup model by feeding an input
network.predict(x=iter(test_data()), steps=1)

# load pre-trained weights
pretrained_weight_filename = network_retrieved_args['name']
if repetition_prefix:
    pretrained_weight_filename += '_repetition_{}'.format(repetition_prefix)
if save_prefix:
    pretrained_weight_filename += '_{}'.format(save_prefix)
current_weight_filename = os.path.join(model_path,
                                       '{}.h5'.format(pretrained_weight_filename))
network.load(os.path.join(model_path, current_weight_filename))

# Step 6: Inference

logger.info('[Step 5] Inference on given input data...')

test_predictions = network.predict(x=iter(test_data()),
                                   steps=test_steps,
                                   callbacks=callbacks)
test_predictions = np.argmax(test_predictions, axis=1)

# Flush
K.clear_session()

# Step 7: Organize final results

logger.info('[Step 6] Saving final results...')

attention_weights = load_json(
    os.path.join(model_path, '{0}_{1}_attention_weights.json'.format(network.name, save_prefix)))
attention_weights_rounded = np.round(attention_weights)

test_predictions_uf = ['unfair' if item else 'not-unfair' for item in test_predictions]
attention_weights_uf = [str(item.ravel()) for item in attention_weights]
attention_weights_rounded_uf = [str(item.ravel()) for item in attention_weights_rounded]

df_dict = {
    'text': input_data,
    'predictions': test_predictions_uf,
    'memory_weights_raw': attention_weights_uf,
    'memory_weights_rounded_raw': attention_weights_rounded_uf
}
df = pd.DataFrame.from_dict(df_dict)
df.to_csv(os.path.join(data_folder, '{}_inference.csv'.format(data_name)), index=False)
