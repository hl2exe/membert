import argparse
import os
from collections import OrderedDict

import numpy as np
import pandas as pd

import const_define as cd
from utility.data_utils import get_data_config_id
from utility.json_utils import load_json
from utility.log_utils import get_logger
from utility.plot_utils import show_target_coverage, show_voting_coverage, MemoryMetrics
from utility.python_utils import merge, flatten

logger = get_logger(__name__)


def compute_coverage_info(attention_weights, predictions, attention_mode, category, K_repetition_ids, R=3,
                          target_column=None, threshold=0.5):
    target_column = '{}_targets'.format(category) if target_column is None else target_column

    total_usage, total_hits, total_correct, total_correct_and_hit, \
    total_correct_and_no_hit, total_samples, \
    avg_memory_percentage = 0, 0, 0, 0, 0, 0, 0
    total_mrr = []
    total_top_R_hits = np.zeros(R)

    for attention_weight in attention_weights:
        sub_path = os.path.join(model_path, attention_weight)
        loaded_weights = load_json(sub_path)
        fold_name = attention_weight.split('fold')[1].split('_')[1]

        fold_test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold_name)))
        repetition_id = K_repetition_ids[int(fold_name)]

        iter_preds = predictions[fold_name][repetition_id]

        if type(iter_preds) in [dict, OrderedDict]:
            iter_preds = iter_preds['label_id_{}'.format(category)]

        fold_stats = show_target_coverage(
            attention=loaded_weights,
            test_df=fold_test_df,
            fold_name=fold_name,
            predictions=iter_preds,
            category=category,
            attention_mode=attention_mode,
            verbose=0,
            target_column=target_column,
            R=R,
            threshold=threshold)

        total_usage += fold_stats.usage
        total_hits += fold_stats.hits
        total_correct += fold_stats.correct
        total_correct_and_hit += fold_stats.correct_and_hit
        total_correct_and_no_hit += fold_stats.correct_and_no_hit
        total_samples += fold_stats.samples
        total_top_R_hits += fold_stats.top_R_hits
        avg_memory_percentage += fold_stats.avg_memory_percentage
        total_mrr.append(fold_stats.mrr)

    memory_usage = total_usage / total_samples
    coverage = total_hits / total_samples
    recall = total_correct / total_samples
    supervision_precision = total_correct_and_hit / total_samples
    non_memory_accuracy = (total_correct - total_correct_and_hit - total_correct_and_no_hit) / total_samples
    mrr = np.mean(total_mrr)

    try:
        coverage_precision = total_hits / total_usage
        memory_precision = total_correct_and_hit / total_usage
        avg_memory_percentage = avg_memory_percentage / total_usage
        top_R_coverage_precision = total_top_R_hits / total_usage
    except ZeroDivisionError:
        coverage_precision = None
        memory_precision = None
        avg_memory_percentage = None
        top_R_coverage_precision = None

    metrics = MemoryMetrics(memory_usage,
                            coverage,
                            coverage_precision,
                            recall,
                            memory_precision,
                            supervision_precision,
                            non_memory_accuracy,
                            top_R_coverage_precision,
                            avg_memory_percentage,
                            mrr)
    return metrics


def get_per_fold_top_K_indexes(metric_data, K):
    if type(metric_data) is not np.ndarray:
        metric_data = np.array(metric_data)

    metric_data = metric_data.transpose()

    if K == 1:
        return np.argmax(metric_data, axis=1)

    best_indexes = np.argsort(metric_data, axis=1)[:, ::-1]
    best_indexes = best_indexes[:, :K]

    return best_indexes


def get_config_path(network_args, data_loader_info, data_name):
    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name, should_exist=True)
    config_path = os.path.join(model_base_path, str(config_id))

    return config_path


def show_coverage_for_category(category, target_column=None, threshold=0.5):
    attention_weights = [name for name in os.listdir(model_path)
                         if 'attention_weights' in name.lower() and set_suffix in name]

    predictions = load_json(os.path.join(model_path, cd.JSON_PREDICTIONS_NAME))

    loaded_val_results = load_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME))
    metric_val_results = loaded_val_results[retrieval_metric]

    repetition_ids = get_per_fold_top_K_indexes(metric_val_results, top_K)

    if top_K == 1:
        repetition_ids = repetition_ids[:, np.newaxis]

    attention_mode = \
        load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))['extraction_info']['value'][
            'mode']

    # Special cases
    if 'discriminative' in model_type.lower():
        attention_mode = 'sigmoid'

    # Check inference repetitions
    if any(['inference-rep' in item for item in attention_weights]):
        inference_reps = 1
        for item in attention_weights:
            item_tokens = item.split('_')
            item_inference_rep = None
            for token_idx, token in enumerate(item_tokens):
                if token == 'inference-rep':
                    item_inference_rep = int(item_tokens[token_idx + 1])
                    break
            if item_inference_rep > inference_reps:
                inference_reps = item_inference_rep

        for K in range(top_K):

            avg_K_metrics = {}

            for inference_rep in range(inference_reps):
                K_attention_weights = [name for name in attention_weights
                                       if 'top{}'.format(K + 1) in name
                                       and 'inference-rep_{}'.format(inference_rep) in name]
                K_metrics = compute_coverage_info(
                    attention_weights=K_attention_weights,
                    predictions=predictions,
                    attention_mode=attention_mode,
                    K_repetition_ids=repetition_ids[:, K],
                    category=category,
                    target_column=target_column,
                    R=ranking,
                    threshold=threshold)

                for key, value in K_metrics._asdict().items():
                    avg_K_metrics.setdefault(key, []).append(value)

            for key, value in avg_K_metrics.items():
                logger.info('[Top {0}] {1}: {2}'.format(K, key, np.mean(value, axis=0) if value[0] is not None else None))

            logger.info('*' * 50)
            logger.info('{}'.format(os.linesep))
    else:
        for K in range(top_K):
            K_attention_weights = [name for name in attention_weights
                                   if 'top{}'.format(K + 1) in name]
            K_metrics = compute_coverage_info(
                attention_weights=K_attention_weights,
                predictions=predictions,
                attention_mode=attention_mode,
                K_repetition_ids=repetition_ids[:, K],
                category=category,
                target_column=target_column,
                R=ranking,
                threshold=threshold)

            for key, value in K_metrics._asdict().items():
                logger.info('[Top {0}] {1}: {2}'.format(K, key, value))

            logger.info('*' * 50)
            logger.info('{}'.format(os.linesep))


model_type = 'experimental_basic_memn2n_v2'
test_name = "ToS-30_A-WS-MLP-32"
retrieval_metric = 'F1'
set_suffix = 'test'
top_K = 1
ranking = 3
label_column = 'C_Label'
target_column = 'evidence_targets'
threshold = 0.0

# Parser
parser = argparse.ArgumentParser(description="Argument Parser")
parser.add_argument("--model_type", help="Model architecture name."
                                         " Check const_define.py -> MODEL_CONFIG variable for more info.",
                    type=str, default=model_type)
parser.add_argument("--test_name", help="Test unique ID (date).", type=str, default=test_name)
parser.add_argument("--retrieval_metric", help="Metric criterium for best runs retrieval for each fold.",
                    type=str, default=retrieval_metric)
parser.add_argument("--set_suffix", help="Which dataset split to consider: train, val, test",
                    type=str, default=set_suffix)
parser.add_argument("--top_K", help="How many top runs for which attention scores should be collected.",
                    type=int, default=top_K)
parser.add_argument("--ranking", help="How many top memory hits to consider when computing coverage precision",
                    type=int, default=ranking)
parser.add_argument("--threshold", help="Memory slots with attention value above or equal"
                                        " to threshold are considered as 'used' slots",
                    type=float, default=threshold)
args = parser.parse_args()

model_type = args.model_type
test_name = args.test_name
retrieval_metric = args.retrieval_metric
set_suffix = args.set_suffix
top_K = args.top_K
ranking = args.ranking
threshold = args.threshold

logger.info("Debugging argparse...")
for key, value in args.__dict__.items():
    logger.info("Arg: {0} -- Value: {1}".format(key, value))
logger.info("End of debugging... {}".format(os.linesep))

model_path = os.path.join(cd.CV_DIR, model_type, test_name)
converter_type = cd.MODEL_CONFIG[model_type]['converter']

loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
loader_info = loader_info['configs'][loader_info['type']]

if 'category' not in loader_info:
    if 'categories' in loader_info:
        categories = loader_info['categories']
        for cat in categories:
            logger.info("\n\n**** CATEGORY: {} ****\n\n".format(cat))
            show_coverage_for_category(cat, threshold=threshold)
    else:
        show_coverage_for_category(label_column, target_column, threshold=threshold)
else:
    category = loader_info['category']
    show_coverage_for_category(category, threshold=threshold)
