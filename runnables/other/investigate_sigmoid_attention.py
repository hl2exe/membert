from utility.json_utils import load_json
import os
import const_define as cd
import numpy as np

model_name = "experimental_gated_memn2n_v2"
test_names = [
    "25-02-2020-01-20-55",
    "25-02-2020-19-41-26",
    "25-02-2020-19-42-34",
    '27-02-2020-00-48-44',
    '27-02-2020-00-49-48'
]


def load_attention(filepath, fold):
    network_name = [name for name in os.listdir(filepath) if 'fold_{}_top1_attention_weights.json'.format(fold) in name][0]
    return load_json(os.path.join(filepath, network_name))


for test_name in test_names:
    print('*' * 50)
    print('Test name: {}'.format(test_name))
    print('*' * 50)

    base_path = os.path.join(cd.CV_DIR, model_name, test_name)

    for fold in range(10):
        attention = load_attention(base_path, fold)
        attention = attention.squeeze()
        sums = np.sum(attention, axis=1)
        print('Fold {0} -- Max {1} -- Min {2} -- Median {3}'.format(fold, np.max(sums), np.min(sums), np.median(sums)))
        print('Non-saturated values: {0}/{1}'.format(np.where((attention != 0.) & (attention != 1.))[1].shape[0],
                                                     attention.ravel().shape[0]))
