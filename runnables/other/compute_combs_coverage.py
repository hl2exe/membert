"""

For each memory slots combination, compute targets coverage

** BEFORE RUNNING **

1. Change the test unique identifier 'test_name' variable.
2. Change the 'model_type' variable as to match your tested model.

"""

from itertools import combinations
import os
import const_define as cd
from utility.json_utils import load_json
from tqdm import tqdm
import numpy as np
import operator
import pandas as pd
from utility.plot_utils import MemoryMetrics
import ast


def get_hits(target, predicted):
    target = set(target)
    predicted = set(predicted)
    intersection = predicted.intersection(target)
    hits = len(intersection)
    missed = target.difference(intersection)
    others = predicted.difference(intersection)
    return hits, missed, others


def get_best_target_rank(attention_values, target_values):
    # from highest to lowest
    sorted_attention_indexes = np.argsort(attention_values, axis=1)[:, ::-1]
    ranks = [np.where(np.in1d(sorted_attention_indexes[hop], target_values))[0] for hop in range(sorted_attention_indexes.shape[0])]
    return ranks[0][0]


model_name = 'memory-distilbert-base-uncased'
test_name = "ToS-30_A_WS"
label_column = 'TER'
target_column = 'TER_targets'

model_path = os.path.join(cd.CV_DIR, model_name, test_name)
loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
if 'category' not in loader_info['configs'][loader_info['type']]:
    category = label_column
else:
    category = loader_info['configs'][loader_info['type']]["category"]
    target_column = "{}_targets".format(category)

pred_combs = []
memory_slots = np.arange(8)
folds = 10

# comb_lengths = np.arange(5) + 1
comb_lengths = [1]

for comb_length in comb_lengths:
    combs = combinations(memory_slots, comb_length)
    pred_combs.extend(combs)

comb_results = {}

for pred_id, pred_comb in enumerate(tqdm(pred_combs)):

    R = 3
    total_usage, total_hits, total_correct, total_correct_and_hit, \
    total_correct_and_no_hit, total_samples, \
    avg_memory_percentage = 0, 0, 0, 0, 0, 0, 0
    total_top_R_hits = np.zeros(R)
    # total_mrr = []

    for fold in range(folds):
        fold_usage = 0
        fold_hits = 0
        fold_top_R_hits = [0] * R
        fold_correct = 0
        fold_correct_and_hit = 0
        fold_correct_and_no_hit = 0
        fold_avg_memory_percentage = 0
        # fold_mrr = []
        fold_real_samples = 0

        fold_test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold)))
        unfair_data = fold_test_df[fold_test_df[category] == 1]

        for row_id, row in unfair_data.iterrows():
            target_id = row_id
            target_values = row[target_column]
            if not ast.literal_eval(target_values):
                continue
            fold_real_samples += 1
            target_values = [int(item) for item in target_values[1:-1].split(',')]

            predicted_label = 1
            true_label = 1

            hits, missed, others = get_hits(target=target_values, predicted=pred_comb)

            if len(pred_comb) > 0:
                fold_usage += 1

                top_R_predictions = pred_comb[:R]
                fold_top_R_hits = [count + 1 if len(set(top_R_predictions[:idx + 1]).intersection(set(target_values)))
                                   else count
                                   for idx, count in enumerate(fold_top_R_hits)]

                fold_avg_memory_percentage += len(set(pred_comb)) / len(memory_slots)

                if hits > 0:
                    fold_hits += 1
                    fold_correct_and_hit += int(predicted_label == true_label)
                else:
                    fold_correct_and_no_hit += int(predicted_label == true_label)

                # get rank of predicted value
                # best_target_rank = get_best_target_rank(pred_comb, target_values)
                # fold_mrr.append(1 / (best_target_rank + 1))

            fold_correct += int(predicted_label == true_label)

        fold_top_R_hits = np.array(fold_top_R_hits)
        # fold_mrr = np.mean(fold_mrr) if len(fold_mrr) else 0

        total_usage += fold_usage
        total_hits += fold_hits
        total_correct += fold_correct
        total_correct_and_hit += fold_correct_and_hit
        total_correct_and_no_hit += fold_correct_and_no_hit
        total_samples += fold_real_samples
        total_top_R_hits += fold_top_R_hits
        # total_mrr.append(fold_mrr)
        avg_memory_percentage += fold_avg_memory_percentage

    memory_usage = total_usage / total_samples
    coverage = total_hits / total_samples
    recall = total_correct / total_samples
    supervision_precision = total_correct_and_hit / total_samples
    non_memory_accuracy = (total_correct - total_correct_and_hit - total_correct_and_no_hit) / total_samples
    # total_mrr = np.mean(total_mrr)

    try:
        coverage_precision = total_hits / total_usage
        memory_precision = total_correct_and_hit / total_usage
        avg_memory_percentage = avg_memory_percentage / total_usage
        top_R_coverage_precision = total_top_R_hits / total_usage
    except ZeroDivisionError:
        coverage_precision = None
        memory_precision = None
        avg_memory_percentage = None
        top_R_coverage_precision = None

    metrics = MemoryMetrics(memory_usage,
                            coverage,
                            coverage_precision,
                            recall,
                            memory_precision,
                            supervision_precision,
                            non_memory_accuracy,
                            top_R_coverage_precision,
                            avg_memory_percentage,
                            0)

    comb_results[pred_id] = metrics

    # for key, value in metrics._asdict().items():
    #     print('{0}: {1}'.format(key, value))

sorted_results = sorted(comb_results.items(), key=lambda item: item[1].coverage, reverse=True)

head = 10
print([(pred_combs[item[0]], item[1].coverage) for item in sorted_results[:head]])


def pretty_print_metrics(metrics):
    for key, value in metrics._asdict().items():
        print('{0}: {1}'.format(key, value))


for comb_id, comb_metric in sorted_results[:head]:
    print('Combination ID: {0}'
          ' -- Combination: {1}'
          ' -- Metrics:\n'.format(comb_id, pred_combs[comb_id]))
    pretty_print_metrics(comb_metric)
    print('*' * 20)
