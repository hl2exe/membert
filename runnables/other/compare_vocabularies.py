"""

Compares BERT and keras tokenizer vocabularies in order to find BERT
missed tokens

"""

import os
import pandas as pd
import const_define as cd
from utility.bert_utils import FullTokenizer, BasicTokenizer, WordpieceTokenizer, load_vocab
import tensorflow as tf
from collections import Counter
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from collections import OrderedDict

df_path = os.path.join(cd.TOS_100_DIR, 'dataset.csv')
df = pd.read_csv(df_path)
text = df.text.values

# BERT vocab

bert_vocab_file = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'vocab.txt')
bert_vocab = load_vocab(bert_vocab_file)

# Keras vocab

tok = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
tok.fit_on_texts(text)

keras_vocab = list(tok.word_index.keys())

# Check difference

remaining = set(keras_vocab).difference(set(bert_vocab))

print('Remaining: ', len(remaining))

# Get counts of remaining tokens

full_tok = FullTokenizer(vocab_file=bert_vocab_file)

text_tokens = list(map(lambda item: full_tok.tokenize(item), text))
text_tokens = [token for token_set in text_tokens for token in token_set]

token_counter = Counter(text_tokens)

remaining = list(map(lambda item: full_tok.tokenize(item) if type(item) == str else [item], remaining))
remaining = [token for token_set in remaining for token in token_set]

remaining_counts = [(key, value) for key, value in token_counter.items() if key in remaining]
remaining_counts = sorted(remaining_counts, key=lambda pair: pair[1], reverse=True)

print(remaining_counts[:1000])

# Get frequencies of remaining tokens

vec = TfidfVectorizer()
vec.fit_transform(text_tokens).todense()

vec_vocab = vec.vocabulary_
vec_idf = vec.idf_

indexes = np.argsort(vec_idf)[::-1][:1000]

top_tokens = []

for idx in indexes:
    for key, value in vec_vocab.items():
        if value == idx:
            top_tokens.append((key, vec_idf[idx]))

print(top_tokens)

adj_bert_vocab = [
    (key, value) if '[unused' not in key else (top_tokens[int(key.split('[unused')[1].split(']')[0])][0], value)
    for key, value in bert_vocab.items()]

adj_bert_vocab = OrderedDict(adj_bert_vocab)

print(bert_vocab.keys())

print('*' * 50)

print(adj_bert_vocab.keys())

adj_bert_vocab_keys = list(adj_bert_vocab.keys())
adj_bert_vocab_keys = list(map(lambda item: item + os.linesep, adj_bert_vocab_keys))

with open(os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'adj_vocab.txt'), 'w') as f:
    f.writelines(adj_bert_vocab_keys)
