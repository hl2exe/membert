"""

Visualizes attention weights in different ways:
Attention weights are in the form [#samples, #hops, memory_size]

** BEFORE RUNNING **

1. Change the test unique identifier 'test_name' variable.
2. Change the 'model_type' variable as to match your tested model.
3. Make sure the 'retrieval_metric' variable is correct for your needs.

TODO: automatic memory_labels

"""

import os

import matplotlib.pyplot as plt

import const_define as cd
from utility.json_utils import load_json
from collections import Counter
from utility.plot_utils import plot_attention_contour_v2, plot_memory_selections, show_target_coverage, \
    compute_trajectories_distribution, visualize_unfair_trajectories, plot_attention_trajectories, memory_histogram
import numpy as np
import pandas as pd

model_type = "experimental_gated_memn2n_v2"
test_name = "05-03-2020-10-50-53"
retrieval_metric = 'f1_score'
show_max_only = True

model_path = os.path.join(cd.CV_DIR, model_type, test_name)

loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
category = loader_info['configs'][loader_info['type']]["category"]

attention_weights = [name for name in os.listdir(model_path)
                     if 'attention_weights' in name.lower()]

annotated_samples = [name for name in os.listdir(model_path)
                     if 'annotated_samples' in name.lower()]
if not annotated_samples:
    available_targets = False
else:
    available_targets = True

kb = load_json(os.path.join(cd.KB_DIR, '{}_targets.json'.format(category)))

predictions = load_json(os.path.join(model_path, cd.JSON_PREDICTIONS_NAME))

loaded_val_results = load_json(os.path.join(model_path, cd.JSON_VALIDATION_INFO_NAME))
metric_val_results = loaded_val_results[retrieval_metric]
repetition_ids = np.argmax(metric_val_results, axis=0)

true_value_name = 'y_test_fold_{}.json'
true_values = [load_json(os.path.join(model_path, true_value_name.format(fold)))
               for fold in range(10)]

memory_explanations = pd.read_csv(os.path.join(cd.KB_DIR, '{}_explanations.csv'.format(category)))
memory_labels = {idx: exp for idx, exp in zip(memory_explanations.index.values, memory_explanations.Id.values)}

attention_mode = load_json(os.path.join(model_path, cd.JSON_MODEL_CONFIG_NAME))['extraction_info']['value']['mode']

# for attention_weight in attention_weights:
#     sub_path = os.path.join(model_path, attention_weight)
#     loaded_weights = load_json(sub_path)
#     plot_name = attention_weight.split('_attention_weights')[0]
#     fold_name = attention_weight.split('fold')[1].split('_')[1]

    # plot_attention_contour_v2(attention=loaded_weights, name=plot_name)
    # plot_memory_selections(attention=loaded_weights, name=plot_name)

    # trajectories_distribution = compute_trajectories_distribution(attention=loaded_weights)
    # print('Fold {0} distribution: {1}'.format(fold_name, trajectories_distribution))

    # correct_info = load_json(os.path.join(model_path, 'correct_unfair_info_fold_{}.json'.format(fold_name)))
    # visualize_unfair_trajectories(unfair_info=correct_info, fold_name=fold_name, key='correct', aggregate=True)

    # plot_attention_trajectories(unfair_info=correct_info, fold_name=fold_name, key='correct')

    # wrong_info = load_json(os.path.join(model_path, 'wrong_unfair_info_fold_{}.json'.format(fold_name)))
    # visualize_unfair_trajectories(unfair_info=wrong_info, fold_name=fold_name, key='wrong', aggregate=True)

    # plot_attention_trajectories(unfair_info=wrong_info, fold_name=fold_name, key='wrong')

# Plot memories histogram (distinct memories selections per sample)
# memory_histogram(attention_weights=attention_weights, model_path=model_path, attention_mode=attention_mode,
#                  show_max_only=show_max_only)
memory_histogram(attention_weights=attention_weights, model_path=model_path, filter_unfair=True,
                 true_values=true_values, memory_labels=None, attention_mode=attention_mode,
                 show_max_only=show_max_only)

plt.show()
