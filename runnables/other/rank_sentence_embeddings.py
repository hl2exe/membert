"""

Loads pre-trained sentence embeddings (ELMo, BERT) of ToS clauses and KBs
and tries to rank clauses w.r.t. given KB for qualitative analysis

ELMo (v2/v3): 0.12 F1 -> all positive similarities but narrower value range (w.r.t. BERT)
BERT (Large 768): 0.06 F1 -> wider range (w.r.t ELMo) but far higher scores

"""

import os

import numpy as np
import pandas as pd
from sklearn.metrics import classification_report

import const_define as cd
from utility.preprocessing_utils import filter_line, compute_jaccard_similarity

# Load Embeddings

embedding_model = 'BERT'
embedding_file = 'preloaded_sentence.npy'
emb_path = os.path.join(cd.EMBEDDING_MODELS_DIR, embedding_model, embedding_file)

embeddings = np.load(emb_path, allow_pickle=True).item()

# Load KB

category = 'LTD'
kb_path = os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category.upper()))

with open(kb_path, 'r') as f:
    kb = f.readlines()

# Load dataset

df_path = os.path.join(cd.TOS_100_DIR, 'dataset.csv')
df = pd.read_csv(df_path)

# Preprocess text

kb = list(map(lambda item: filter_line(item), kb))
df['text'] = df['text'].apply(lambda sentence: filter_line(line=sentence))

# Check retrieval

check_kb = all([item in embeddings for item in kb])

check_df = all([item in embeddings for item in df['text'].values])

if not check_kb:
    print('Check KB failed!')

if not check_df:
    print('Check dataset failed!')

# Retrieve embeddings

kb_embs = np.array([embeddings[sentence] for sentence in kb])

clauses_embs = np.array([embeddings[sentence] for sentence in df['text'].values])

print('KB embeddings shape: ', kb_embs.shape)
print('Clauses embeddings shape: ', clauses_embs.shape)
print('\n\n')

# Compute matching

# [#explanations, #clauses]

# Dot product
# matching = np.matmul(kb_embs, clauses_embs.transpose())

# Cosine Similarity

kb_embs = kb_embs / np.linalg.norm(kb_embs, axis=1)[:, np.newaxis]
clauses_embs = clauses_embs / np.linalg.norm(clauses_embs, axis=1)[:, np.newaxis]
matching = np.matmul(kb_embs, clauses_embs.transpose())

# Parse targets

targets = df['{}_targets'.format(category.upper())].values
targets = [[int(item) for item in target_set[1:-1].split(',')] if type(target_set) != float else [] for target_set in
           targets]

# Build dataframe

clauses = df['text'].values
labels = df[category.upper()].values

inspection_df = dict()

inspection_df['clause'] = clauses
inspection_df['label'] = labels

for target_set in targets:
    for explanation_id in range(len(kb)):
        inspection_df.setdefault('has_target_{}'.format(explanation_id), []).append(
            True if explanation_id in target_set else False)

for explanation_id in range(len(kb)):
    inspection_df['similarity_{}'.format(explanation_id)] = matching[explanation_id, :]

inspection_df = pd.DataFrame.from_dict(inspection_df)

# Compute ranking

k = 5

thresholds = []
for explanation_id, explanation in enumerate(kb):
    print('Ranking for explanation {}: '.format(explanation_id), explanation)

    # Sort for ranking
    sorted_exp_df = inspection_df.sort_values(by=['similarity_{}'.format(explanation_id)],
                                              ascending=False)

    # Non unfair

    print('-' * 20)
    print('Ranking stats for non-unfair clauses')
    print('-' * 20)

    print()

    exp_fair_df = sorted_exp_df[sorted_exp_df['label'] == 0]
    exp_fair_indexes = exp_fair_df.index.values

    print('Minimum index: ', np.min(exp_fair_indexes))
    print('Maximum index: ', np.max(exp_fair_indexes))

    print()

    print('Maximum similarity ({}): '.format(exp_fair_indexes[0]),
          exp_fair_df.iloc[0]['similarity_{}'.format(explanation_id)])
    print('Minimum similarity ({}): '.format(exp_fair_indexes[-1]),
          exp_fair_df.iloc[-1]['similarity_{}'.format(explanation_id)])

    print()

    print('Worst K={} clauses: \n'.format(k), exp_fair_df.iloc[-k:]['clause'].values)
    print('Worst K={} scoring: \n'.format(k), exp_fair_df.iloc[-k:]['similarity_{}'.format(explanation_id)].values)
    print('Worst K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_fair_df.iloc[-k:]['clause'].values)))

    print()

    print('Top K={} clauses: \n'.format(k), exp_fair_df.iloc[:k]['clause'].values)
    print('Top K={} scoring: \n'.format(k), exp_fair_df.iloc[:k]['similarity_{}'.format(explanation_id)].values)
    print('Top K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_fair_df.iloc[:k]['clause'].values)))

    print()

    print('5% Quantile index: ', np.quantile(exp_fair_indexes, q=0.05))
    print('5% Quantile score: ', np.quantile(exp_fair_df['similarity_{}'.format(explanation_id)], q=0.05))

    print()

    # Only unfair

    print('-' * 20)
    print('Ranking stats for unfair clauses')
    print('-' * 20)

    print()

    exp_unfair_df = sorted_exp_df[sorted_exp_df['label'] == 1]

    exp_unfair_indexes = exp_unfair_df.index.values

    print('Minimum index: ', np.min(exp_unfair_indexes))
    print('Maximum index: ', np.max(exp_unfair_indexes))

    print()

    print('Maximum similarity ({}): '.format(exp_unfair_indexes[0]),
          exp_unfair_df.iloc[0]['similarity_{}'.format(explanation_id)])
    print('Minimum similarity ({}): '.format(exp_unfair_indexes[-1]),
          exp_unfair_df.iloc[-1]['similarity_{}'.format(explanation_id)])

    print()

    print('Worst K={} clauses: \n'.format(k), exp_unfair_df.iloc[-k:]['clause'].values)
    print('Worst K={} scoring: \n'.format(k), exp_unfair_df.iloc[-k:]['similarity_{}'.format(explanation_id)].values)
    print('Worst K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_unfair_df.iloc[-k:]['clause'].values)))

    print()

    print('Top K={} clauses: \n'.format(k), exp_unfair_df.iloc[:k]['clause'].values)
    print('Top K={} scoring: \n'.format(k), exp_unfair_df.iloc[:k]['similarity_{}'.format(explanation_id)].values)
    print('Top K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_unfair_df.iloc[:k]['clause'].values)))

    print()

    print('5% Quantile index: ', np.quantile(exp_unfair_indexes, q=0.05))
    print('5% Quantile score: ', np.quantile(exp_unfair_df['similarity_{}'.format(explanation_id)], q=0.05))

    # Only unfair with matching target

    print('-' * 20)
    print('Ranking stats for unfair clauses with matching target only')
    print('-' * 20)

    print()

    exp_unfair_target_df = sorted_exp_df[(inspection_df['label'] == 1) &
                                         (inspection_df['has_target_{}'.format(explanation_id)] == True)]

    exp_unfair_target_indexes = exp_unfair_target_df.index.values

    print('Minimum index: ', np.min(exp_unfair_target_indexes))
    print('Maximum index: ', np.max(exp_unfair_target_indexes))

    print()

    print('Maximum similarity ({}): '.format(exp_unfair_target_indexes[0]),
          exp_unfair_target_df.iloc[0]['similarity_{}'.format(explanation_id)])
    print('Minimum similarity ({}): '.format(exp_unfair_target_indexes[-1]),
          exp_unfair_target_df.iloc[-1]['similarity_{}'.format(explanation_id)])

    print('Worst K={} clauses: \n'.format(k), exp_unfair_target_df.iloc[-k:]['clause'].values)
    print('Worst K={} scoring: \n'.format(k),
          exp_unfair_target_df.iloc[-k:]['similarity_{}'.format(explanation_id)].values)
    print('Worst K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_unfair_target_df.iloc[-k:]['clause'].values)))

    print()

    print('Top K={} clauses: \n'.format(k), exp_unfair_target_df.iloc[:k]['clause'].values)
    print('Top K={} scoring: \n'.format(k),
          exp_unfair_target_df.iloc[:k]['similarity_{}'.format(explanation_id)].values)
    print('Top K={} Jaccard scoring: \n'.format(k),
          list(map(lambda sent: compute_jaccard_similarity(sent, explanation),
                   exp_unfair_target_df.iloc[:k]['clause'].values)))

    print()

    print('5% Quantile index: ', np.quantile(exp_unfair_target_indexes, q=0.05))

    scoring_threshold = np.quantile(exp_unfair_target_df['similarity_{}'.format(explanation_id)], q=0.05)
    thresholds.append(scoring_threshold)
    print('5% Quantile score: ', scoring_threshold)

    print()

    print('*' * 50)
    print('\n\n')

# [#clauses, #explanations]
matching = matching.transpose()

predictions = matching
for explanation_id, explanation_threshold in enumerate(thresholds):
    above_threshold = predictions[:, explanation_id] >= explanation_threshold
    below_threshold = predictions[:, explanation_id] < explanation_threshold
    predictions[above_threshold, explanation_id] = 1
    predictions[below_threshold, explanation_id] = 0

predictions = np.max(predictions, axis=1)
true_values = df[category.upper()].values

print(classification_report(y_pred=predictions, y_true=true_values))
