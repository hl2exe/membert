"""

Checks malformed clauses in generated targets JSON file

** BEFORE RUNNING **

1. Change the 'category' variable as to match the unfairness category of your interest.
2. Make sure you have completed all the initial setup as reported in the repo README file.

"""

import const_define as cd
import os
from utility.json_utils import load_json
import pandas as pd

category = 'LTD'
targets = load_json(os.path.join(cd.KB_DIR, '{}_targets.json'.format(category)))

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))

text = df['text'].values
text = [item.strip().lower() for item in text]

debug_count = 0
for doc, clause, targets in targets:
    if clause.strip().lower() not in text:
        debug_count += 1
        print(clause)
        print('*' * 50)

print('Total malformed: ', debug_count)