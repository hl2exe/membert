"""

For a given category, shows memory usage for unfair examples, distiguishing for each explanation ID

"""

import const_define as cd
import os
import numpy as np
import pandas as pd
from utility.json_utils import load_json
import ast
from utility.python_utils import flatten_nested_array
import matplotlib.pyplot as plt

plt.style.use('seaborn-paper')

SMALL_SIZE = 3
MEDIUM_SIZE = 10
BIGGER_SIZE = 30

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# Step -1: functions

def get_predictions(attention_values, attention_mode):
    if attention_mode == 'softmax':
        return np.argmax(attention_values, axis=1).ravel().tolist()
    elif attention_mode == 'sigmoid':
        return np.where(attention_values >= 0.5)[-1].tolist()
    else:
        raise RuntimeError('Invalid attention mode! Got: {}'.format(attention_mode))


# Step 0: config
# Consider the best rep for each fold

model_type = "experimental_basic_memn2n_v2"
test_name = "AI_LAW_A_WS"
set_suffix = 'test'

ss_test_name = '_'.join(test_name.split('_')[:-1]) + '_SS'

model_path = os.path.join(cd.CV_DIR, model_type, test_name)
ss_model_path = os.path.join(cd.CV_DIR, model_type, ss_test_name)

loader_info = load_json(os.path.join(model_path, cd.JSON_DATA_LOADER_CONFIG_NAME))
category = loader_info['configs'][loader_info['type']]["category"]

attention_weights_names = [name for name in os.listdir(model_path) if
                           'top1_{}_attention_weights'.format(set_suffix) in name.lower()]
ss_attention_weights_names = [name for name in os.listdir(ss_model_path) if
                              'top1_{}_attention_weights'.format(set_suffix) in name.lower()]

attention_mode = load_json(os.path.join(model_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))['extraction_info']['value'][
    'mode']

explanations = pd.read_csv(os.path.join(cd.KB_DIR, '{}_explanations.csv'.format(category.upper())))
explanations_tags = explanations['Id'].values

memory_size = None

total_test_targets = []
total_predictions = []
ss_total_predictions = []

for fold_idx in range(10):

    # Load test df
    fold_test_df = pd.read_csv(os.path.join(model_path, 'test_df_fold_{}.csv'.format(fold_idx)))
    unfair_labels = load_json(os.path.join(model_path, 'y_test_fold_{}.json'.format(fold_idx)))
    fold_test_df = fold_test_df.iloc[np.where(unfair_labels == 1)[0]]

    # Get unfair targets
    test_targets = fold_test_df['{}_targets'.format(category)].values.tolist()
    test_targets = list(map(lambda x: ast.literal_eval(x), test_targets))
    flat_test_targets = flatten_nested_array(test_targets)
    total_test_targets += flat_test_targets

    # Load predictions (WS)
    fold_attention_name = [name for name in attention_weights_names if 'fold_{}'.format(fold_idx) in name][0]
    attention_weights = load_json(os.path.join(model_path, fold_attention_name))
    attention_weights = attention_weights[np.where(unfair_labels == 1)[0]]

    # Get targets (WS)
    predictions = [get_predictions(att_val, attention_mode) for att_val in attention_weights]
    flat_predictions = flatten_nested_array(predictions)
    total_predictions += flat_predictions

    # Load predictions (SS)
    ss_fold_attention_name = [name for name in ss_attention_weights_names if 'fold_{}'.format(fold_idx) in name][0]
    ss_attention_weights = load_json(os.path.join(ss_model_path, ss_fold_attention_name))
    ss_attention_weights = ss_attention_weights[np.where(unfair_labels == 1)[0]]

    # Get targets (SS)
    ss_predictions = [get_predictions(att_val, attention_mode) for att_val in ss_attention_weights]
    ss_flat_predictions = flatten_nested_array(ss_predictions)
    ss_total_predictions += ss_flat_predictions

    # Memory size
    if memory_size is None:
        memory_size = attention_weights.shape[-1]

# Get counts
(targets_unique, targets_counts) = np.unique(total_test_targets, return_counts=True)
targets_frequencies = np.asarray((targets_unique, targets_counts)).T
target_dict = {key: val for key, val in targets_frequencies}

# Get counts (WS)
(preds_unique, preds_counts) = np.unique(total_predictions, return_counts=True)
pred_frequencies = np.asarray((preds_unique, preds_counts)).T
pred_dict = {key: val for key, val in pred_frequencies}

# Get counts (SS)
(ss_preds_unique, ss_preds_counts) = np.unique(ss_total_predictions, return_counts=True)
ss_pred_frequencies = np.asarray((ss_preds_unique, ss_preds_counts)).T
ss_pred_dict = {key: val for key, val in ss_pred_frequencies}

# Fix to memory size
for mem_id in range(memory_size):
    if mem_id not in target_dict:
        target_dict[mem_id] = 0
    if mem_id not in pred_dict:
        pred_dict[mem_id] = 0
    if mem_id not in ss_pred_dict:
        ss_pred_dict[mem_id] = 0

# Sort decrementally
target_frequencies = [(mem_id, target_dict[mem_id]) for mem_id in range(memory_size)]
target_frequencies = sorted(target_frequencies, key=lambda x: x[1], reverse=True)
target_ids = [item[0] for item in target_frequencies]
target_values = [item[1] for item in target_frequencies]

# Sort (WS)
pred_frequencies = [(idx, pred_dict[idx]) for idx in target_ids]
pred_values = [item[1] for item in pred_frequencies]

# Sort (SS)
ss_pred_frequencies = [(idx, ss_pred_dict[idx]) for idx in target_ids]
ss_pred_values = [item[1] for item in ss_pred_frequencies]

# Plot time!

sorted_explanations_tags = explanations_tags[target_ids]

plt.plot(target_values, marker='o', linestyle='solid', color='black', markersize=MEDIUM_SIZE, linewidth=SMALL_SIZE)
plt.plot(pred_values, marker='s', linestyle='dashed', color='dimgray', markersize=MEDIUM_SIZE, linewidth=SMALL_SIZE)
plt.plot(ss_pred_values, marker='v', linestyle='dotted', color='silver', markersize=MEDIUM_SIZE, linewidth=SMALL_SIZE)

plt.xticks(ticks=target_ids, labels=sorted_explanations_tags, rotation=-90)

plt.legend(['Ground truth', 'MANN (WS)', 'MANN (SS)'])
plt.show()
