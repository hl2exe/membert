"""

Computes the number of annotated data (evidence, claim) for each topic.
Topics are ordered decrementally.
Only a sufficiently large sub-set of topics is then selected according to preference.

"""

import os
import const_define as cd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import ast


def plot_data(topic_ids, claims, evidences):
    fig, ax = plt.subplots(1, 1)
    ax.set_title('Per Topic Statistics')

    ax.plot(claims, marker='x')
    ax.plot(evidences, marker='o')
    ax.set_xticks(np.arange(len(topic_ids)))
    ax.set_xticklabels(topic_ids)

    # Labeling
    ax.set_xlabel('Topic id')
    ax.set_ylabel('Amount')

    ax.legend(['#claim', '#evidence'])

    return fig


data_path = os.path.join(cd.IBM2015_DIR, 'reduced_corrected_sentence_dataset.csv')
data = pd.read_csv(data_path)

sort_data_key = "claims"
do_unique = False
K_set = np.arange(15) + 1

grouped = data.groupby('Topic id')

topic_info = {}
for _, group in grouped:
    claims_df = group[group['C_Label'] == 1]
    claims = claims_df.Sentence.values
    evidence_memory = claims_df['Evidence indices'].values
    evidence_memory = [ast.literal_eval(item) for item in evidence_memory]
    evidence_memory = set([item for seq in evidence_memory for item in seq])

    not_arg = group[group['NA_Label'] == 1]['Sentence'].values

    if do_unique:
        claims = set(claims.tolist())
        not_arg = set(not_arg.tolist())

    topic_id = group['Topic id'].values[0]

    claim_amount = len(claims)
    evidence_amount = len(evidence_memory)
    not_arg_amount = len(not_arg)
    total = group.shape[0]

    topic_info[topic_id] = {
        'claims': claim_amount,
        'claims_%': claim_amount / total,
        'evidences': evidence_amount,
        'evidence_%': evidence_amount / total,
        'arg': claim_amount + evidence_amount,
        'arg_%': (claim_amount + evidence_amount) / total,
        'not_arg': not_arg_amount,
        'not_arg_%': not_arg_amount / total,
        'total': total
    }

# Ordered based on arg
topic_info = sorted(topic_info.items(), key=lambda item: item[1][sort_data_key], reverse=True)

# Debug
for value in topic_info:
    print("Topic id: {0} -- Info: {1}".format(value[0], value[1]))

# Plot
topic_ids = [item[0] for item in topic_info]
claims = [item[1]['claims'] for item in topic_info]
evidences = [item[1]['evidences'] for item in topic_info]
total = [item[1]['total'] for item in topic_info]

plot_data(topic_ids=topic_ids,
          claims=claims,
          evidences=evidences)
# plt.show()

print("\n\nFinal statistics (best slice)\n\n")

# Take best slice (according to K value)
for k_value in K_set:
    k_topic_ids = topic_ids[:k_value]
    k_claims = np.sum(claims[:k_value])
    k_evidences = np.sum(evidences[:k_value])
    k_total = np.sum(total[:k_value])
    print("Topics: {0} -- Claims (unique = {1}): {2}"
          " -- Evidence Memory (unique = True): {3} -- Total: {4}".format(k_topic_ids,
                                                                                                  do_unique,
                                                                                                  k_claims,
                                                                                                  k_evidences,
                                                                                                  k_total - k_evidences))

# Build dataset

selected_k = input("Which k do you want? [1 - {0}]".format(max(K_set) + 1))
selected_k = int(selected_k)

selected_topic_ids = topic_ids[:selected_k]

sub_df = data[data['Topic id'].isin(selected_topic_ids)]
print("Selected sub-dataset: ", sub_df.shape[0])

# Save df
save_path = os.path.join(cd.IBM2015_DIR, 'selected_subdataset_{}.csv'.format(selected_k))
sub_df.rename(columns={'Evidence indices': 'evidence_targets'}, inplace=True)
sub_df.to_csv(save_path, index=False)