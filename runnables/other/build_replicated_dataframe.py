"""

@Author: Federico Ruggeri
@Date: 07/01/2020
@Description: clauses are replicated for each given category in order to split up unfair labels, thus
allowing the definition of unique <clause, kb> pairs for easy and low memory consumption model implementation

"""

import os
import const_define as cd
from tqdm import tqdm
import numpy as np
from data_loader import load_dataset
import pandas as pd
from collections import Counter
tqdm.pandas()


dataset = 'tos_100'
categories = ['LTD', 'CH', 'CR', 'A', 'TER']
multi_path = os.path.join(cd.TOS_100_DIR, '{}_unfair.csv'.format('-'.join(categories)))

df_path = os.path.join(cd.DATASET_PATHS[dataset], 'dataset.csv')
df = load_dataset(df_path=df_path)
category_values = [df[cat].values for cat in categories]
logic_label = np.logical_or.reduce(category_values)
or_indexes = np.argwhere(logic_label == 1).ravel()
df = df.iloc[or_indexes]

orig_counter = {}
for row_id, row in df.iterrows():
    for cat in categories:
        if row[cat]:
            if cat in orig_counter:
                orig_counter[cat] += 1
            else:
                orig_counter[cat] = 0

# replicate dataset
rep_df = pd.DataFrame(columns=['text', 'document', 'document_ID', 'label', 'label_targets'] + categories)
for row_id, row in tqdm(df.iterrows()):
    for cat in categories:
        to_add = {'text': row['text'],
                  'document': row['document'],
                  'document_ID': row['document_ID'],
                  'label': cat if row[cat] else 'not-unfair',
                  'label_targets': row['{}_targets'.format(cat)],
                  'category': cat
                  }
        for c in categories:
            to_add[c] = 1 if c == cat else 0

        to_add = pd.Series(to_add)
        rep_df = rep_df.append(to_add, ignore_index=True)

rep_dist = []
for row_id, row in rep_df.iterrows():
        to_add = row['label']
        rep_dist.append(to_add)


rep_counter = Counter(rep_dist)

print('Original: \n', orig_counter)
print('Replicated: \n', rep_counter)
print()

rep_df.to_csv(multi_path, index=False)