"""

Testing DistilledBERT with hugging's face ktrain model (wrapper for TF2)

"""

import os
from datetime import datetime

import numpy as np
import tensorflow as tf

import const_define as cd
from data_loader import DataLoaderFactory
from utility.cross_validation_utils import PrebuiltCV
from utility.distributed_test_utils import ktrain_cross_validation
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger
from custom_callbacks_v2 import EarlyStopping, TrainingLogger

logger = get_logger(__name__)

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.experimental_run_functions_eagerly(True)

    # Settings

    model_type = 'distilbert-base-uncased'

    # Configs

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Distributed info
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    save_base_path = os.path.join(cd.CV_DIR, model_type, current_date)

    if cv_test_config['save_model'] and not os.path.isdir(save_base_path):
        os.makedirs(save_base_path)

    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    training_logger = TrainingLogger(filepath=save_base_path, suffix=None, save_model=cv_test_config['save_model'])

    callbacks = [
        early_stopper,
        # training_logger
        # supervision_controller
    ]

    if cv_test_config['save_model']:
        callbacks.append(training_logger)

    scores = ktrain_cross_validation(validation_percentage=cv_test_config['validation_percentage'],
                                     data_handle=data_handle,
                                     data_loader_info=data_loader_info,
                                     cv=cv,
                                     training_config=training_config,
                                     model_type=model_type,
                                     error_metrics=cv_test_config['error_metrics'],
                                     error_metrics_additional_info=cv_test_config['error_metrics_additional_info'],
                                     error_metrics_nicknames=cv_test_config['error_metrics_nicknames'],
                                     compute_test_info=True,
                                     save_predictions=True,
                                     save_model=cv_test_config['save_model'],
                                     test_path=save_base_path,
                                     split_key=cv_test_config['split_key'],
                                     distributed_info=distributed_info,
                                     callbacks=callbacks
                                     )

    # Validation
    logger.info('Average validation scores: {}'.format(
        {key: np.mean(item) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    # Test
    logger.info('Average test scores: {}'.format(
        {key: np.mean(item) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    # Test config
    if cv_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(save_base_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])
        save_json(os.path.join(save_base_path, cd.JSON_TRAINING_CONFIG_NAME), training_config)
        save_json(os.path.join(save_base_path, cd.JSON_CV_TEST_CONFIG_NAME), data=cv_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_CONFIG_NAME), data=distributed_info)