"""

Runs a sequence of tests with different configurations.
Alternates between
1. quick_test_setup.py + distributed_model_config.json
2. test_cross_validation.py

"""

import os
from subprocess import Popen

from tqdm import tqdm

import const_define as cd
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger

logger = get_logger(__name__)


def run_quick_setup(quick_setup_info):
    script_path = os.path.join(cd.PROJECT_DIR, 'runnables', 'other', 'quick_test_setup.py')
    cmd = ['python', script_path]
    for key, value in quick_setup_info.items():
        cmd.append(key)
        # cmd.append('\'{0}\''.format(value))
        cmd.append(value)

    process = Popen(cmd)
    process.wait()


def update_model_config(model_config_info):
    filepath = os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME)
    file_config = load_json(filepath)
    model_type = model_config_info['model_type']
    model_args = model_config_info['model_args']
    logger.info("[Model Config] Model Type -> {}".format(model_type))
    logger.info("[Model Config] Model Args -> {}".format(model_args))
    for key, value in model_args.items():
        file_config[model_type][key]['value'] = value

    save_json(filepath, file_config)


def run_test(test_script):
    script_path = os.path.join(cd.PROJECT_DIR, 'runnables', '{}.py'.format(test_script))
    assert os.path.isfile(script_path)
    cmd = ['python', script_path]
    process = Popen(cmd)
    process.wait()


tests_info = {
    'quick_setup': [
        {
            '-dn': 'squad',
            '-la': '{}',
            '-mt': "squad_mann",
            '-mdt': "",
            '-tt': 'squad',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_EM"}}',
            '-ivt': 'True',
            '-tety': "train_and_test",
            '-rf': "glove_100_cs",
            '-gsi': '0',
            '-gei': '1',
            '-bs': "128"
        },
        {
            '-dn': 'squad',
            '-la': '{}',
            '-mt': "squad_mann",
            '-mdt': "",
            '-tt': 'squad',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_EM"}}',
            '-ivt': 'True',
            '-tety': "train_and_test",
            '-rf': "glove_100_cs_hops2",
            '-gsi': '0',
            '-gei': '1',
            '-bs': "128"
        },
        {
            '-dn': 'squad',
            '-la': '{}',
            '-mt': "squad_mann",
            '-mdt': "",
            '-tt': 'squad',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_EM"}}',
            '-ivt': 'True',
            '-tety': "train_and_test",
            '-rf': "glove_100_mlp",
            '-gsi': '0',
            '-gei': '1',
            '-bs': "128"
        },
        {
            '-dn': 'squad',
            '-la': '{}',
            '-mt': "squad_mann",
            '-mdt': "",
            '-tt': 'squad',
            '-ca': '{"earlystopping": {"patience": 10, "monitor": "val_EM"}}',
            '-ivt': 'True',
            '-tety': "train_and_test",
            '-rf': "glove_100_mlp_hops2",
            '-gsi': '0',
            '-gei': '1',
            '-bs': "128"
        },
    ],
    'model_config': [
        {
            "model_type": "squad_mann",
            "model_args": {
                "embedding_model_type": "glove",
                "build_embedding_matrix": True,
                "memory_lookup_info": {
                    "mode": "cosine_similarity"
                },
            }
        },
        {
            "model_type": "squad_mann",
            "model_args": {
                "embedding_model_type": "glove",
                "build_embedding_matrix": True,
                "memory_lookup_info": {
                    "mode": "cosine_similarity"
                },
                "hops": 2
            }
        },
        {
            "model_type": "squad_mann",
            "model_args": {
                "embedding_model_type": "glove",
                "build_embedding_matrix": True,
                "memory_lookup_info": {
                    "mode": "mlp",
                    "weights": [256, 128]
                },
            }
        },
        {
            "model_type": "squad_mann",
            "model_args": {
                "embedding_model_type": "glove",
                "build_embedding_matrix": True,
                "memory_lookup_info": {
                    "mode": "mlp",
                    "weights": [256, 128]
                },
                "hops": 2
            }
        },
    ],
    'test_script': [
        'test_train_and_test_v2',
        'test_train_and_test_v2',
        'test_train_and_test_v2',
        'test_train_and_test_v2',
    ],
}

total_runs = len(tests_info['quick_setup'])
for key, value in tests_info.items():
    assert len(value) == total_runs

for run in tqdm(range(total_runs)):
    # Quick setup
    logger.info("[Run {0}] Quick setup ".format(run + 1))
    run_quick_setup_info = tests_info['quick_setup'][run]
    run_quick_setup(run_quick_setup_info)

    # Model config
    logger.info("[Run {0}] Model config".format(run + 1))
    run_model_config_info = tests_info['model_config'][run]
    update_model_config(run_model_config_info)

    # Test
    logger.info("[Run {0}] Test".format(run + 1))
    test_script = tests_info['test_script'][run]
    run_test(test_script)
