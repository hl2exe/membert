"""

@Author: Federico Ruggeri

@Date: 27/02/2020

"""

import os
from datetime import datetime

import numpy as np
import tensorflow as tf

import const_define as cd
from custom_callbacks_v2 import EarlyStopping, TrainingLogger
from data_loader import DataLoaderFactory
from utility.distributed_test_utils import train_and_test
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.python_utils import merge

logger = get_logger(__name__)

if __name__ == '__main__':

    # Ensure TF2
    assert tf.version.VERSION.startswith('2.')
    # tf.config.run_functions_eagerly(True)

    train_and_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME))

    # Limiting GPU access
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            gpu_start_index = train_and_test_config['gpu_start_index']
            gpu_end_index = train_and_test_config['gpu_end_index']
            tf.config.set_visible_devices(gpus[gpu_start_index:gpu_end_index], "GPU")  # avoid other GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME))[
        train_and_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Strategy
    distributed_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DISTRIBUTED_CONFIG_NAME))

    if train_and_test_config['pre_loaded_model'] is None:
        current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
        save_base_path = os.path.join(cd.TRAIN_AND_TEST_DIR, train_and_test_config['model_type'], current_date)

        if train_and_test_config['save_model'] and not os.path.isdir(save_base_path):
            os.makedirs(save_base_path)
    else:
        save_base_path = os.path.join(cd.TRAIN_AND_TEST_DIR, train_and_test_config['model_type'],
                                      train_and_test_config['pre_loaded_model'])
        if not os.path.isdir(save_base_path):
            msg = "Can't find given pre-trained model. Got: {}".format(save_base_path)
            raise RuntimeError(msg)

    # Callbacks
    # TODO: automatize callbacks creation
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    training_logger = TrainingLogger(filepath=save_base_path, save_model=train_and_test_config['save_model'])
    callbacks = [
        early_stopper,
    ]

    if train_and_test_config['save_model']:
        callbacks.append(training_logger)

    if train_and_test_config['seeds'] is None:
        train_and_test_config['seeds'] = np.random.randint(low=1, high=100000, size=train_and_test_config['repetitions'])

    model_routine_args = {key: value['value'] for key, value in model_config.items()
                          if 'routine' in value['flags']}

    scores = train_and_test(data_handle=data_handle,
                            callbacks=callbacks,
                            model_type=train_and_test_config['model_type'],
                            training_config=training_config,
                            error_metrics=train_and_test_config['error_metrics'],
                            error_metrics_additional_info=train_and_test_config['error_metrics_additional_info'],
                            error_metrics_nicknames=train_and_test_config['error_metrics_nicknames'],
                            error_metrics_flags=train_and_test_config['error_metrics_flags'],
                            compute_test_info=train_and_test_config['compute_test_info'],
                            network_args=model_config,
                            save_model=train_and_test_config['save_model'],
                            test_path=save_base_path,
                            repetitions=train_and_test_config['repetitions'],
                            data_loader_info=data_loader_info,
                            validation_percentage=train_and_test_config['validation_percentage'],
                            distributed_info=distributed_info,
                            seeds=train_and_test_config['seeds'],
                            **model_routine_args)

    # Validation
    if train_and_test_config['repetitions'] > 1:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))

    # Test
    if train_and_test_config['repetitions'] > 1:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if not key.startswith('avg')}))

    if train_and_test_config['save_model']:
        save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])
        save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])
        save_json(os.path.join(save_base_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME), model_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAINING_CONFIG_NAME), training_config)
        save_json(os.path.join(save_base_path, cd.JSON_TRAIN_AND_TEST_CONFIG_NAME), data=train_and_test_config)
        save_json(os.path.join(save_base_path, cd.JSON_CALLBACKS_NAME), data=callbacks_data)
        save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
        save_json(os.path.join(save_base_path, cd.JSON_DISTRIBUTED_CONFIG_NAME), data=distributed_info)

    # Rename folder
    if train_and_test_config['save_model'] and\
            train_and_test_config['rename_folder'] is not None and\
            train_and_test_config['pre_loaded_model'] is None:
        renamed_base_path = os.path.join(cd.TRAIN_AND_TEST_DIR,
                                         train_and_test_config['model_type'],
                                         train_and_test_config['rename_folder'])
        os.rename(save_base_path, renamed_base_path)
