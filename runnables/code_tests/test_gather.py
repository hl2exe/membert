

import tensorflow as tf
import numpy as np


# [batch_size, mem_size]
memory = np.arange(50).reshape(10, 5)

# [batch_size, padding_amount]
targets = np.array([[0, 1], [2, 3], [4, 1], [0, 0], [3, 2], [1, 3], [1, 4], [2, 3], [3, 4], [1, 3]])

print(memory.shape)
print(targets.shape)

print(memory)
print(targets)

result = tf.gather(memory, targets, batch_dims=1).numpy()

print(result)

assert result.shape == targets.shape

expected = [memory[idx, target_set] for idx, target_set in enumerate(targets)]
expected = np.array(expected)

assert np.equal(expected, result).all()