"""

Testing BERT embeddings on TF v1
Checking trainable weights

"""

"""

Testing BERT Embeddings

"""

import tensorflow_hub as hub
import keras.backend as K
import tensorflow as tf
from utility.bert_utils import FullTokenizer
from utility.embedding_utils import pad_data
import numpy as np


class CustomModel(tf.keras.Model):

    def __init__(self):
        super(CustomModel, self).__init__()
        self.bert_embedding = BERTEmbeddingLayer()

    def call(self, x):
        emb_x = self.bert_embedding(x)
        return emb_x


class BERTEmbeddingLayer(tf.keras.layers.Layer):
    def __init__(self, output_representation=1, **kwargs):
        self.bert = None
        self.dimensions = 1024
        super(BERTEmbeddingLayer, self).__init__(**kwargs)

        if output_representation:
            self.output_representation = 'sequence_output'
        else:
            self.output_representation = 'pooled_output'

    def build(self, input_shape):
        self.bert = hub.Module('https://tfhub.dev/google/bert_uncased_L-24_H-1024_A-16/1',
                               trainable=True,
                               name='{}_module'.format(self.name))

        # Check TF 1.X version in order to select fine-tuning trainable weights only
        self._trainable_weights += K.tf.trainable_variables(scope="^{}_module/.*".format(self.name))
        super(BERTEmbeddingLayer, self).build(input_shape)

    def call(self, x, mask=None):
        result = self.bert(inputs=x, as_dict=True, signature='tokens')[self.output_representation]
        return result

    def compute_mask(self, inputs, mask=None):
        return None

    def compute_output_shape(self, input_shape):
        return (None, 768)


if __name__ == '__main__':
    vocab_file = 'vocab.txt'
    tokenizer = FullTokenizer(vocab_file=vocab_file, do_lower_case=True)
    texts = [
        'the cat is on the table',
        "I'm stronger than you"
    ]
    parsed_texts = [['[CLS]'] + tokenizer.tokenize(item) + ['[SEP]'] for item in texts]
    print(parsed_texts)
    parsed_ids = [tokenizer.convert_tokens_to_ids(tokens) for tokens in parsed_texts]
    print(parsed_ids)
    parsed_ids = pad_data(parsed_ids, padding='post')
    parsed_ids = np.array(parsed_ids)

    input_mask = parsed_ids != 0
    input_mask = input_mask.astype(np.int32)

    print(parsed_ids)
    print(input_mask)

    segment_ids = np.zeros_like(input_mask)

    print(segment_ids)

    my_model = CustomModel()
    model_input = {'input_ids': tf.constant(parsed_ids, dtype=tf.int32),
                   'input_mask': tf.constant(input_mask, dtype=tf.int32),
                   'segment_ids': tf.constant(segment_ids, dtype=tf.int32)}
    res = my_model(model_input)
    print(res)
