"""

Testing tf.dataset from generator with memory sampling

"""

import numpy as np
import tensorflow as tf
import os
import const_define as cd
from sample_wrappers import TextKBSupervisionFeatures
from utility.pipeline_utils import load_single_dataset

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

max_memory_size = 2
positive_class = 1

data_path = os.path.join(cd.TESTS_DATA_DIR, 'tos_100_task1kb', 'experimental_basic_memn2n_v2', '0',
                         'test_data_fold_0')
converter_info_path = os.path.join(cd.TESTS_DATA_DIR, 'tos_100_task1kb', 'experimental_basic_memn2n_v2', '0',
                                   'converter_info_fold_0.npy')

conversion_args = np.load(converter_info_path, allow_pickle=True).item()

print("Conversion args: ", conversion_args)

# input_ids, attention_mask, token_type_ids,
# label_id,
# kb_input_ids, kb_attention_mask, kb_token_type_ids, kb_mask
# positive_indexes, negative_indexes, mask_indexes
feature_mappings = TextKBSupervisionFeatures.get_mappings(conversion_args=conversion_args,
                                                          converter_args={'partial_supervision': True})

print("Feature mappings: ", feature_mappings)

selector = TextKBSupervisionFeatures.get_dataset_selector()

data = load_single_dataset(data_path, feature_mappings)

data = data.map(selector)


# def test_intersection(a, b):
#     return tf.sets.intersection(a, b)
#
#
# x = tf.constant([1, 2, 3])
# y = tf.constant([5])
# inter = test_intersection(x[None, :], y[None, :])
# print(inter)
# print(tf.sparse.to_dense(inter))

# TODO: add priority here -> modify method
def random_choice(x, size, axis=0, unique=True):
    dim_x = tf.cast(tf.shape(x)[axis], tf.int32)
    indices = tf.range(0, dim_x, dtype=tf.int32)
    sample_index = tf.random.shuffle(indices)[:size]
    sample = tf.gather(x, sample_index, axis=axis)

    return sample, sample_index


# TODO: check this out
def weighted_random_choice(x, size, axis=0):
    # # Sampling k members of 1D tensor a using weights w
    # cum_dist = tf.math.cumsum(w)
    # cum_dist /= cum_dist[-1]  # to account for floating point errors
    # unif_samp = tf.random.uniform((k,), 0, 1)
    # idxs = tf.searchsorted(cum_dist, unif_samp)
    # samp = tf.gather(a, idxs)  # samp contains the k weighted samples
    pass


# test = np.arange(20).reshape(4, 5)
# test = tf.constant(test)
# choice, choice_indexes = random_choice(test, size=2, axis=0)
# print(choice)
# print(choice_indexes)
# print(tf.gather(test, choice_indexes, axis=0))


# Note: kb_mask here is useless since kb is the same for all examples...
def uniform_sampling(item):
    memory_size = conversion_args['max_kb_length']
    label_id = tf.argmax(item['label_id'])

    kb_input_ids = tf.reshape(item['kb_input_ids'], [memory_size, -1])
    kb_attention_mask = tf.reshape(item['kb_attention_mask'], [memory_size, -1])
    kb_token_type_ids = tf.reshape(item['kb_token_type_ids'], [memory_size, -1])
    kb_mask = item['kb_mask']

    # Uniform sampling
    sampled_kb_input_ids, sampled_indexes = random_choice(kb_input_ids, size=max_memory_size, axis=0)

    sampled_kb_attention_mask = tf.gather(kb_attention_mask, sampled_indexes, axis=0)
    sampled_kb_token_type_ids = tf.gather(kb_token_type_ids, sampled_indexes, axis=0)
    sampled_kb_mask = tf.gather(kb_mask, sampled_indexes, axis=0)

    # Check if unfair clause
    if tf.equal(label_id, positive_class):

        # Retrieve sampled positive indexes
        target_indexes = item['positive_indexes']
        mask_indexes = item['mask_indexes']
        indexes_length = tf.reduce_sum(mask_indexes)
        target_indexes = target_indexes[:indexes_length]
        target_indexes = tf.unique(target_indexes)[0]

        sampled_targets = tf.sets.intersection(sampled_indexes[None, :], target_indexes[None, :])
        sampled_targets = tf.sparse.to_dense(sampled_targets)[0]
        has_target = tf.not_equal(tf.size(sampled_targets), 0)

        # Check if initial sampling has at least one target kb entry
        if not has_target:
            random_target, random_target_idx = random_choice(target_indexes, size=1)
            random_to_remove, random_to_remove_idx = random_choice(sampled_indexes, size=1)

            sampled_indexes = tf.where(sampled_indexes == random_to_remove[0],
                                       tf.fill(sampled_indexes.shape, random_target[0]),
                                       sampled_indexes)

            sampled_kb_input_ids = tf.gather(kb_input_ids, sampled_indexes, axis=0)
            sampled_kb_attention_mask = tf.gather(kb_attention_mask, sampled_indexes, axis=0)
            sampled_kb_token_type_ids = tf.gather(kb_token_type_ids, sampled_indexes, axis=0)
            sampled_kb_mask = tf.gather(kb_mask, sampled_indexes, axis=0)

            # Modify supervision data as well
            sampled_targets = random_target

        # Modify supervision data as well
        mask_indexes = tf.ones_like(sampled_targets)

        non_targets = item['negative_indexes']
        non_targets = tf.unique(non_targets)[0]

        sampled_non_targets = tf.sets.intersection(sampled_indexes[None, :], non_targets[None, :])
        sampled_non_targets = tf.sparse.to_dense(sampled_non_targets)[0]

        # Re-pad supervision data (very naive method)
        padding_tensor = tf.zeros((conversion_args['max_supervision_padding']), dtype=sampled_targets.dtype)

        sampled_targets = tf.concat((sampled_targets, padding_tensor), axis=0)[
                          :conversion_args['max_supervision_padding']]
        sampled_mask_indexes = tf.concat((mask_indexes, padding_tensor), axis=0)[
                               :conversion_args['max_supervision_padding']]
        sampled_non_targets = tf.concat((sampled_non_targets, padding_tensor), axis=0)[
                              :conversion_args['max_supervision_padding']]

        item['positive_indexes'] = sampled_targets
        item['negative_indexes'] = sampled_non_targets
        item['mask_indexes'] = sampled_mask_indexes

    # Reshape to initial format
    item['kb_input_ids'] = tf.reshape(sampled_kb_input_ids, [-1])
    item['kb_attention_mask'] = tf.reshape(sampled_kb_attention_mask, [-1])
    item['kb_token_type_ids'] = tf.reshape(sampled_kb_token_type_ids, [-1])
    item['kb_mask'] = sampled_kb_mask

    return item


data = data.take(600)

for item in data.take(-1):
    x = item[0]
    y = item[1]
    if np.argmax(y.numpy()) == positive_class:
        print(x['context'].numpy().reshape(-1, conversion_args['max_kb_seq_length']))
        print(x['positive_indexes'])
        print(x['negative_indexes'])
        print(x['mask_indexes'])
        print('*' * 20)
        break

data = data.repeat()

from dataset_samplers import Memn2nSampler

sampler = Memn2nSampler(max_memory_size=max_memory_size, positive_class=positive_class)
sampler.set_conversion_args(conversion_args)

data = data.map(lambda x, y: sampler.sampling((x, y)))

for item in data.take(-1):
    x = item[0]
    y = item[1]
    # print(item)
    # print(item[0]['context'])
    # print('*' * 50)
    # print(uniform_sampling(item)['kb_input_ids'])
    if np.argmax(y.numpy()) == positive_class:
        print(x['context'].numpy().reshape(-1, conversion_args['max_kb_seq_length']))
        print(x['positive_indexes'])
        print(x['negative_indexes'])
        print(x['mask_indexes'])
        print('*' * 20)
        break
    #
    # kb_input_ids = item['kb_input_ids'].numpy().reshape(max_memory_size, -1)
    # if any(np.sum(kb_input_ids, axis=-1) == 0):
    #     print(item[kb_input_ids])

# def my_generator():
#     for item in data:
#         item = uniform_sampling(item)
#         yield item


# input_ids, attention_mask, token_type_ids,
# label_id,
# kb_input_ids, kb_attention_mask, kb_token_type_ids, kb_mask
# positive_indexes, negative_indexes, mask_indexes

# metadata = {
#     'input_ids': tf.int32,
#     'attention_mask': tf.int32,
#     'token_type_ids': tf.int32,
#     'label_id': tf.int32,
#     'kb_input_ids': tf.int32,
#     'kb_attention_mask': tf.int32,
#     'kb_token_type_ids': tf.int32,
#     'kb_mask': tf.int32,
#     'positive_indexes': tf.int32,
#     'negative_indexes': tf.int32,
#     'mask_indexes': tf.int32,
# }
#
# mod_data = tf.data.Dataset.from_generator(my_generator,
#                                           output_types=metadata)
#
# mod_data = mod_data.shuffle(buffer_size=10)
# mod_data = mod_data.repeat(6)
#
# for item in mod_data.take(6):
#     print(item['kb_input_ids'])
#     print('*' * 20)
