
import tensorflow as tf
import tf_sentencepiece as tfs
from utility.bert_utils import load_vocab
import os
import const_define as cd

vocab_path = os.path.join(cd.TOS_100_DIR, 'tos100.vocab')

t = load_vocab(vocab_path)
print(t)