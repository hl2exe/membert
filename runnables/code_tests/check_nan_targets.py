

import pandas as pd
import numpy as np
import os
import const_define as cd


df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))
categories = ['LTD', 'CH', 'CR', 'A', 'TER']

category_values = [df[cat].values for cat in categories]
logic_label = np.logical_or.reduce(category_values)
or_indexes = np.argwhere(logic_label == 1).ravel()
df = df.iloc[or_indexes]

# Check unfair clauses with no targets
print('Checking unfair clauses with no targets...')

all_invalid = []
for cat in categories:
    sub_df = df[df[cat] == 1]
    cat_targets = sub_df['{}_targets'.format(cat)].values
    cat_indexes = sub_df.index.values
    cat_invalid = [idx for idx, item in zip(cat_indexes, cat_targets) if type(item) == float]
    print('Invalid targets for category ', cat, ': ', cat_invalid)
    all_invalid.extend(cat_invalid)

all_invalid = list(set(all_invalid))
all_invalid = sorted(all_invalid)
print(all_invalid)

print('*' * 50)

# Check non-unfair clauses with targets
print('Checking non-unfair clauses with targets...')

for cat in categories:
    sub_df = df[df[cat] == 0]
    cat_targets = sub_df['{}_targets'.format(cat)].values
    cat_indexes = sub_df.index.values
    cat_invalid = [idx for idx, item in zip(cat_indexes, cat_targets) if type(item) != float]
    print('Invalid targets for category ', cat, ': ', cat_invalid)

print('*' * 50)

# Check unfair clause without any per category unfairness
sub_df = df[df.label == 1]
category_labels = [sub_df[cat].values for cat in ['A', 'CH', 'CR', 'J', 'LAW', 'LTD', 'PINC', 'TER', 'USE']]
category_labels = np.array(category_labels)
labels_union = np.logical_or.reduce(category_labels)
labels_union = labels_union.astype(np.int32)
union_invalid = np.argwhere(labels_union == 0).ravel()
print('Invalid union: ', union_invalid)

print('*' * 50)

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))

# Check non unfair category samples with targets
for category in categories:
    df['{}_check'.format(category)] = df['{}_targets'.format(category)].apply(lambda target_set: type(target_set) != float)
    normal_with_targets = df[(df['{}_check'.format(category)] == True) & (df[category] == 0)]
    print('[{}] Normal clauses with targets: '.format(category), normal_with_targets.index.values)
