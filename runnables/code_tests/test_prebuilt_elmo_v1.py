"""

Testing ELMo text conversion before model graph

"""

import pandas as pd
import os
import const_define as cd
import tensorflow_hub as hub
import numpy as np
import tensorflow as tf
from keras import backend as k

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.6
k.set_session(tf.Session(config=config))

dataset = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))
take_amount = 50
samples = dataset['text'].values[:take_amount]

elmo_samples = []
with hub.eval_function_for_module("https://tfhub.dev/google/elmo/2") as f:
    for text in samples:
        batch_in = np.array([text])
        batch_out = f(batch_in, as_dict=True)['elmo'][0]
        elmo_samples.append(batch_out)

max_length = max([item.shape[0] for item in elmo_samples])
print(max_length)