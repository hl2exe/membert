"""

Testing ELMo embeddings on TF v1
Checking trainable weights

"""

import tensorflow as tf
import tensorflow_hub as hub
from keras import backend as K

tf.executing_eagerly()


class CustomModel(tf.keras.Model):

    def __init__(self):
        super(CustomModel, self).__init__()
        self.elmo_embeddings = ElmoEmbeddingLayer()

    def call(self, x):
        embed_x = self.elmo_embeddings(x)
        return embed_x


class ElmoEmbeddingLayer(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        self.dimensions = 1024
        self.trainable = True
        super(ElmoEmbeddingLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.elmo = hub.Module('https://tfhub.dev/google/elmo/2', trainable=self.trainable,
                               name="{}_module".format(self.name))

        self._trainable_weights += K.tf.trainable_variables(scope="^{}_module/.*".format(self.name))
        super(ElmoEmbeddingLayer, self).build(input_shape)

    def call(self, x, mask=None):
        result = self.elmo(x,
                           as_dict=True,
                           signature='default',
                           )['elmo']
        return result

    def compute_mask(self, inputs, mask=None):
        return K.not_equal(inputs, '--PAD--')

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.dimensions)


if __name__ == '__main__':

    my_model = CustomModel()
    texts = ["the cake is a lie", "the cat is on the table"]
    res = my_model(texts)
    print(res)
    print(my_model.trainable_weights)