import tensorflow as tf
import numpy as np

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


def random_choice_uniform(x, size, axis=0):
    dim_x = tf.cast(tf.shape(x)[axis], tf.int32)
    indices = tf.range(0, dim_x, dtype=tf.int32)
    sample_index = tf.random.shuffle(indices)[:size]

    return sample_index


def body(i, indices, x, size):
    i = tf.add(i, 1)
    sampled_indices = random_choice_uniform(x, size)
    sampled_indices = tf.reshape(sampled_indices, [1, -1])

    if indices is None:
        indices = sampled_indices
    else:
        indices = tf.concat((indices, sampled_indices), axis=0)

    return i, indices, x


def batch_wise_uniform(size, batch_size=3):
    x = tf.zeros((10), dtype=tf.float32)
    i = tf.constant(0)
    indices = None
    c = lambda i, indices, x: tf.less(i, batch_size)
    b = lambda i, indices, x: body(i, indices, x, size)
    result = tf.while_loop(c, b, [i, indices, x])

    return result[1]


max_memory_size = 5
batch_size = 3
sampled_indices = batch_wise_uniform(size=max_memory_size, batch_size=batch_size)
print(sampled_indices)

# Test gather
# kb_ids = np.arange(30).reshape(10, 3)
# print(kb_ids)
# sampled_kb_ids = tf.gather(kb_ids, sampled_indices, axis=0)
#
# print(sampled_kb_ids)

target_mask = np.array([
    0, 0, 0, 0, 1, 1, 1, 0, 1, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
]).reshape(3, 10)
sampled_target_mask = tf.gather(target_mask, sampled_indices, batch_dims=1)
print(sampled_target_mask)