"""

Testing multiprocessing routines

"""

import multiprocessing as mp
from collections import OrderedDict
from functools import reduce

from utility.python_utils import merge
from itertools import product
import numpy as np
# import nn_models_v2


def mp_routine(info, fold_idx, rep_id, x_train, y_train):
    # importlib.reload(nn_models_v2)
    from old_stuff import nn_models_v2
    model = nn_models_v2.Baseline_LSTM(lstm_weights=[32], answer_weights=[], optimizer_args={'lr': 1e-3}, embedding_dimension=32)
    model.build_model({'vocab_size': 300, 'padding_max_length': 10, 'embedding_matrix': None})
    model.compute_output_weights(y_train, num_classes=1)
    model.fit(x=[x_train], y=y_train, batch_size=2)
    info.setdefault('metric', {}).setdefault(rep_id, {}).setdefault(fold_idx, fold_idx)
    return info


my_info = OrderedDict()

pool = mp.Pool(processes=2)

repetitions = range(2)
folds = range(4)
cv_info = product(repetitions, folds)

x_train = np.arange(300).reshape(-1, 20).astype(np.int32)
y_train = np.random.randint(low=0, high=2, size=15).reshape(-1).astype(np.int32)


results = [pool.apply_async(func=mp_routine,
                            args=(my_info,
                                  fold_id,
                                  rep_id,
                                  x_train,
                                  y_train))
           for rep_id, fold_id in cv_info]

output = [p.get() for p in results]

pool.close()
pool.join()

print(output)
test = reduce(lambda a, b: merge(a, b), output)
