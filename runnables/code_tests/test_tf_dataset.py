"""
Some examples for tf.data.Dataset
"""

import os

import tensorflow as tf
import tensorflow_datasets as tfds

import const_define as cd
import numpy as np


# Debug
def show_batch(dataset):
    for batch in dataset.take(1):
        for key, value in batch.items():
            print('{:20s}: {}'.format(key, value.numpy()))


def filter_function(item):
    my_filter = lambda x: x.lower()
    vfunc = np.vectorize(my_filter)
    item = vfunc(item)
    return item


def example_filter(batch):
    batch['text'] = tf.numpy_function(func=filter_function, inp=[batch['text']], Tout=tf.string)
    return batch


dataset_path = os.path.join(cd.TOS_100_DIR, 'dataset.csv')
max_take = 512

# Loading
tf_data = tf.data.experimental.make_csv_dataset(
    file_pattern=dataset_path,
    batch_size=2,
    num_epochs=1,
    shuffle=False,
    select_columns=['LTD', 'text', 'document']
)

# show_batch(tf_data)

# Preprocessing
tf_data = tf_data.map(lambda batch: example_filter(batch))
tf_data = tf_data.take(max_take)


def split_filter(batch, list_filter, negate=False):
    list_filter = tf.constant(list_filter, dtype=tf.string)
    list_filter = tf.reshape(list_filter, [1, -1])
    filtered_ids = tf.equal(tf.reshape(batch['document'], [-1, 1]), list_filter)
    reduced = tf.reduce_sum(tf.cast(filtered_ids, tf.float32))
    if negate:
        return tf.equal(reduced, tf.constant(0.))
    else:
        return tf.greater(reduced, tf.constant(0.))


# Splitting for cv
# dataset_list = ["Mozilla", "Pinterest", "9gag"]
# train_data = tf_data.filter(lambda x: split_filter(x, dataset_list, negate=True))
# test_data = tf_data.filter(lambda x: split_filter(x, dataset_list, negate=False))


# Converter
def my_gen(tensor_gen):
    for item in tensor_gen:
        item = item.numpy().astype(np.bytes_)
        item = np.char.decode(item, 'UTF-8')
        item = ' '.join(item)
        yield item


tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
test_filter = tf_data.map(lambda x: x['text'])
test_iterator = test_filter.__iter__()
test_gen = my_gen(test_iterator)
tokenizer.fit_on_texts(test_gen)

from keras.preprocessing.sequence import pad_sequences


def encode_batch(batch):
    def split_str(batch):
        batch = batch.astype(np.bytes_)
        batch = np.char.decode(batch, 'UTF-8')
        batch = tokenizer.texts_to_sequences(batch)
        batch = pad_sequences(batch, padding='post')
        return batch

    batch['text'] = tf.numpy_function(func=split_str, inp=[batch['text']], Tout=tf.int32)
    return batch


train_data = tf_data.map(lambda batch: encode_batch(batch))
show_batch(train_data)
print('*' * 20)

