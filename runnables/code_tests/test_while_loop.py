"""

Testing while loop for efficient iteration

"""

import tensorflow as tf
import numpy as np

x = np.random.rand(5 * 5).reshape(5, 5)
x = tf.convert_to_tensor(x, tf.float32)

index = tf.constant(0)

stack = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)

my_parameter = False


def body(index, stack, test):
    reduced = tf.keras.layers.Dense(1)(x)
    stack = stack.write(index, reduced)
    index = tf.add(index, 1)
    return index, stack, test


def condition(index, stack, test):
    return tf.less(index, 5)


iterations, result, _ = tf.while_loop(condition, body, [index, stack, my_parameter])
print('Iterations: ', iterations)
print('Result: ', result.stack())
