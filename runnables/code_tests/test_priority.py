import tensorflow as tf
import numpy as np

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


def scatter_update(reference, indices, updates):
    return tf.tensor_scatter_nd_add(reference, indices, updates)


indices = np.array([
    0, 5, 4, 1,
    6, 0, 2, 3
]).reshape(-1, 1)

scores = np.array([
    1, 1, 1, 1,
    1, 1, 1, 1
]).reshape(-1)

reference = np.zeros((10, ))

updated = scatter_update(reference=reference,
                         indices=indices,
                         updates=scores)
print(reference)
print(updated)