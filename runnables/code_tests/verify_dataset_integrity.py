

import pandas as pd
import const_define as cd
import os
import numpy as np

df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset.csv'))
old_df = pd.read_csv(os.path.join(cd.TOS_100_DIR, 'dataset_old.csv'))

categories = [
    # 'A',
    # 'CH',
    # 'CR',
    'LTD',
    # 'TER'
]

for cat in categories:

    print("Checking integrity for category: ", cat)

    # Check labels
    curr_labels = df[df[cat] == 1].values
    old_labels = old_df[old_df[cat] == 1].values

    assert curr_labels.shape == old_labels.shape

    curr_labels_idxs = df[df[cat] == 1].index.values
    old_labels_idxs = old_df[old_df[cat] == 1].index.values

    assert np.equal(curr_labels_idxs, old_labels_idxs).all()

    # Check targets

    curr_targets = df[df[cat] == 1]['{}_targets'.format(cat)].values
    old_targets = old_df[old_df[cat] == 1]['{}_targets'.format(cat)].values

    assert len([item for item in curr_targets if type(item) == float]) == 0
    assert len([item for item in old_targets if type(item) == float]) == 0

    curr_targets_idxs = df[df[cat] == 1]['{}_targets'.format(cat)].index.values
    old_targets_idxs = old_df[old_df[cat] == 1]['{}_targets'.format(cat)].index.values

    assert np.equal(curr_targets_idxs, old_targets_idxs).all()

    if cat not in ['CH', 'TER']:
        assert np.equal(curr_targets, old_targets).all()