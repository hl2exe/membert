"""

Testing loading logic info

"""

import const_define as cd
import os
from utility.json_utils import load_json
import numpy as np
from utility.preprocessing_utils import filter_line
from tqdm import tqdm
import nltk

aggregation_mapping = {
    'JJ': 'J',
    'JJR': 'J',
    'JJS': 'J',
    'RB': 'R',
    'RBR': 'R',
    'WDT': 'W',
    'WP': 'W',
    'WPZ': 'W',
    'WRB': 'W',
    'NN': 'N',
    'NNS': 'N',
    'NNSZ': 'N',
    'NP': 'N',
    'NPS': 'N',
    'NPSZ': 'N',
    'NPZ': 'N',
    'PP': 'P',
    'PPZ': 'P',
    'PRP': 'P',
    'VB': 'V',
    'VBD': 'V',
    'VBG': 'V',
    'VBN': 'V',
    'VBP': 'V',
    'VBZ': 'V',
    'VH': 'V',
    'VHD': 'V',
    'VHG': 'V',
    'VHN': 'V',
    'VHP': 'V',
    'VHZ': 'V',
    'VV': 'V',
    'VVD': 'V',
    'VVG': 'V',
    'VVN': 'V',
    'VVP': 'V',
    'VVZ': 'V'
}


def build_pos_tags(data, memo=None, return_memo=False):
    if memo is None:
        memo = {}
        pos_tags = []
        for item in tqdm(data):
            key = ''.join(item)
            if key in memo:
                pos_tags.append(memo[key])
            else:
                item_tokens = nltk.word_tokenize(item)
                tags = nltk.pos_tag(item_tokens)
                # TODO: check if some POS tags are left out
                tags = [aggregation_mapping[tup[1]]
                        if tup[1] in aggregation_mapping else tup[1]
                        for tup in tags]
                memo[key] = tags
                pos_tags.append(tags)
    else:
        pos_tags = [memo[item] for item in data]

    if return_memo:
        return memo
    return pos_tags


def load_logic_info(additional_data, key_vocab, category='label'):
    filename = '{}_logic_group_masks.json'.format(category) if category != 'label' else 'logic_group_mask.json'
    filepath = os.path.join(cd.KB_DIR, filename)
    logic_info = load_json(filepath)

    # [memory_size, max_logic_depth - 1, max_logic_groups, sentence_size]
    logic_masks = []

    # [memory_size, max_logic_depth, max_logic_groups]
    logic_operators = []

    logic_info_keys = list(logic_info.keys())
    logic_info_keys = sorted(logic_info_keys, key=lambda x: int(x))

    padding_length = max(additional_data['kb_lengths'])
    print('Max sentence length: ', padding_length)

    def get_max_groups(additional_data, logic_info, logic_info_keys, key_vocab):
        max_groups = None

        for sentence_id in logic_info_keys:
            if logic_info[sentence_id] is None:
                key_groups_len = len([item for item in additional_data['kb_pos'][int(sentence_id)]
                                      if item in key_vocab])
                if max_groups is None:
                    max_groups = key_groups_len
                elif max_groups < key_groups_len:
                    max_groups = key_groups_len
            else:
                level_0_groups = logic_info[sentence_id]['0']
                key_groups_len = len([item for item in additional_data['kb_pos'][int(sentence_id)]
                                      if item in key_vocab])
                level_0_groups_len = [len(item[0]) - 1 for item in level_0_groups]
                real_len = key_groups_len - sum(level_0_groups_len)
                if max_groups is None:
                    max_groups = real_len
                elif max_groups < real_len:
                    max_groups = real_len

        return max_groups

    padding_groups = get_max_groups(additional_data, logic_info, logic_info_keys, key_vocab)
    print('Max logic groups: ', padding_groups)

    def get_max_depth(logic_info):
        max_depth = None

        for item in logic_info.values():
            if item is not None:
                level_keys = list(item.keys())
                level_keys = [int(key) for key in level_keys if key != 'last']
                item_depth = max(level_keys) + 1
                if max_depth is None:
                    max_depth = item_depth
                elif max_depth < item_depth:
                    max_depth = item_depth

        return max_depth

    padding_depth = get_max_depth(logic_info)
    print('Max logic depth: ', padding_depth)

    def create_hot_encoding_logic_masks(sentence_pos, key_vocab, padding_length, padding_groups=None,
                                        seen_indices=None):
        logic_masks = []
        logic_operators = []

        for pos_idx, pos_tag in enumerate(sentence_pos):
            if seen_indices is not None and pos_idx in seen_indices:
                continue

            if pos_tag in key_vocab:
                pos_mask = [0] * padding_length
                pos_mask[pos_idx] = 1
                logic_masks.append(pos_mask)
                logic_operators.append(1)

        # Pad to max groups
        if padding_groups is not None:
            if len(logic_masks) < padding_groups:
                for pad_idx in range(len(logic_masks), padding_groups):
                    pad_encodings = [0] * padding_length
                    logic_masks.append(pad_encodings)
                    logic_operators.append(1)

        return logic_masks, logic_operators

    def create_upper_level_hot_encoding(previous_logic_masks, padding_length, padding_groups):

        logic_masks = []
        logic_operators = []

        # Filter out paddings
        previous_logic_masks = [mask for mask in previous_logic_masks if np.sum(mask) > 0]

        for mask_idx, mask in enumerate(previous_logic_masks):
            hot_encoding = [0] * padding_length
            hot_encoding[mask_idx] = 1
            logic_masks.append(hot_encoding)
            logic_operators.append(1)

        if len(logic_masks) < padding_groups:
            for pad_idx in range(len(logic_masks), padding_groups):
                pad_encodings = [0] * padding_length
                logic_masks.append(pad_encodings)
                logic_operators.append(1)

        return logic_masks, logic_operators

    def create_logic_group_masks(sentence_pos, group_masks, key_vocab, padding_length, padding_groups):
        logic_masks = []
        logic_operators = []
        seen_indices = []

        # mask_info: [indices, operator]
        for mask_info in group_masks:
            group_mask = [1 if idx in mask_info[0] else 0 for idx, _ in enumerate(sentence_pos)]
            group_mask.extend([0] * (padding_length - len(sentence_pos)))
            logic_masks.append(group_mask)
            logic_operators.append(1 if mask_info[1] == 'and' else 0)
            seen_indices.extend(mask_info[0])

        hot_encodings, hot_operators = create_hot_encoding_logic_masks(sentence_pos=sentence_pos,
                                                                       key_vocab=key_vocab,
                                                                       seen_indices=seen_indices,
                                                                       padding_length=padding_length)
        logic_masks.extend(hot_encodings)
        logic_operators.extend(hot_operators)

        # Sort logic masks and operators in order to keep position
        indexes = [np.argmax(mask) for mask in logic_masks]
        indexes = np.argsort(indexes)
        logic_masks = [logic_masks[idx] for idx in indexes]
        logic_operators = [logic_operators[idx] for idx in indexes]

        if len(logic_masks) < padding_groups:
            for pad_idx in range(len(logic_masks), padding_groups):
                pad_encodings = [0] * padding_length
                logic_masks.append(pad_encodings)
                logic_operators.append(1)

        return logic_masks, logic_operators

    def create_upper_level_logic_group_masks(previous_logic_masks, group_masks, padding_length, padding_groups):

        logic_masks = []
        logic_operators = []
        seen_indices = []

        # Sorting logic masks (filter out paddings)
        previous_mask_indexes = [np.argmax(mask) for mask in previous_logic_masks if np.sum(mask) > 0]
        previous_mask_indexes = sorted(previous_mask_indexes)

        for mask_info in group_masks:
            group_mask = [1 if idx in mask_info[0] else 0 for idx, _ in enumerate(previous_mask_indexes)]
            group_mask.extend([0] * (padding_length - len(group_mask)))
            logic_masks.append(group_mask)
            logic_operators.append(1 if mask_info[1] == 'and' else 0)
            seen_indices.extend(mask_info[0])

        for mask_idx, _ in enumerate(previous_mask_indexes):
            if mask_idx not in seen_indices:
                hot_encoding = [0] * padding_length
                hot_encoding[mask_idx] = 1
                logic_masks.append(hot_encoding)
                logic_operators.append(1)

        # Sort logic masks and operators in order to keep position
        indexes = [np.argmax(mask) for mask in logic_masks]
        indexes = np.argsort(indexes)
        logic_masks = [logic_masks[idx] for idx in indexes]
        logic_operators = [logic_operators[idx] for idx in indexes]

        if len(logic_masks) < padding_groups:
            for pad_idx in range(len(logic_masks), padding_groups):
                pad_encodings = [0] * padding_length
                logic_masks.append(pad_encodings)
                logic_operators.append(1)

        return logic_masks, logic_operators

    for sentence_id in logic_info_keys:
        mask_info = logic_info[sentence_id]
        int_sentence_id = int(sentence_id)
        sentence_pos = additional_data['kb_pos'][int(int_sentence_id)]

        # If no logic groups -> add hot encoding of unifiable terms
        if mask_info is None:
            hot_encodings, hot_operators = create_hot_encoding_logic_masks(sentence_pos=sentence_pos,
                                                                           key_vocab=key_vocab,
                                                                           padding_length=padding_length,
                                                                           padding_groups=padding_groups)
            logic_masks.append([hot_encodings])
            logic_operators.append([hot_operators])
        else:
            level_keys = list(mask_info.keys())
            level_keys = [key for key in level_keys if key != 'last']
            level_keys = sorted(level_keys, key=lambda x: int(x))

            # Loop over logic levels
            for level in level_keys:

                # Start with initial logic groups
                if level == '0':
                    initial_masks, initial_operators = create_logic_group_masks(sentence_pos=sentence_pos,
                                                                                key_vocab=key_vocab,
                                                                                group_masks=mask_info[level],
                                                                                padding_length=padding_length,
                                                                                padding_groups=padding_groups)
                    logic_masks.append([initial_masks])
                    logic_operators.append([initial_operators])

                # Build relative logic groups
                else:
                    previous_logic_masks = logic_masks[int_sentence_id][-1]
                    level_masks, level_operators = create_upper_level_logic_group_masks(
                        previous_logic_masks=previous_logic_masks,
                        group_masks=mask_info[level],
                        padding_length=padding_length,
                        padding_groups=padding_groups)
                    logic_masks[int_sentence_id].append(level_masks)
                    logic_operators[int_sentence_id].append(level_operators)

        # Pad to max logic depth
        if len(logic_masks[int_sentence_id]) < padding_depth:
            depth_to_add = padding_depth - len(logic_masks[int_sentence_id])
            previous_logic_masks = logic_masks[int_sentence_id][-1]
            for depth_idx in range(depth_to_add):
                pad_encodings, pad_operators = create_upper_level_hot_encoding(
                    previous_logic_masks=previous_logic_masks,
                    padding_length=padding_length,
                    padding_groups=padding_groups)
                logic_masks[int_sentence_id].append(pad_encodings)
                logic_operators[int_sentence_id].append(pad_operators)

        # Add last level operators
        if mask_info is None:
            logic_operators[int_sentence_id].append([1] + [0] * (padding_groups - 1))
        else:
            operator_gate = [1] if mask_info['last'] == 'and' else [0]
            operator_gate.extend([0] * (padding_groups - 1))
            logic_operators[int_sentence_id].append(operator_gate)

    return np.array(logic_masks, dtype=np.int32), np.array(logic_operators, dtype=np.int32)


kb = [
    "to the fullest extent permissible by law the provider is not liable",
    "the provider is not liable for any suspension, modification, discontinuance, limitation of services and features",
    "the provider is not liable for harm or damage to hardware and software, including viruses, worms, trojan horses, or any similar contamination or destructive program",
    "the provider is not liable for any special, direct, indirect, punitive, incidental or consequential  damage, including negligence, harm or failure, including lost profits, loss of data",
    "since the clause states that the compensation for liability or aggregate liability is limited to, or should not exceed, a certain amount",
    "since the clause states that the provider is not liable for personal injury, death",
    "the provider is not liable for any action taken from third parties or other people, including service and products, material and link posted by others",
    "the provider is not liable for any damage deriving from a security breach, including any unauthorised access",
    "the provider is not liable for damages resulting from disclosure of data and personal information",
    "the provider is not liable for reputational and goodwill damages or loss",
    "the provider is not liable for any loss resulting from the use of the service and or of the website",
    "the provider is not liable for gross negligence",
    "the provider is not liable whether or not he was or should have been aware about the possibility of any damage or loss",
    "the provider is not liable for any failure in performing contract and terms obligations, breach of agreement",
    "the provider is not liable for any unilateral change or unilateral termination",
    "the provider is not liable for any loss of data",
    "the limitation of liability uses a blanket phrase like to the fullest extent permissible by law, any indirect or incidental damages, liability arising out of or in connection with these terms, or similar",
    "liability is excluded in cases related to availability, usability or legality of service, website and or user's content",
    "liability is excluded for damage caused by viruses, trojan horses, malware and similar malicious activity",
    "liability is excluded also in cases of physical or personal injuries",
    "liability is excluded for the actions and or services of third parties"
]
kb = list(map(filter_line, kb))

kb_pos = build_pos_tags(data=kb)
kb_lengths = [len(item) for item in kb_pos]

key_vocab = ['J', 'MD', 'R', 'W', 'V', 'N', 'P']

additional_data = {
    'kb_pos': kb_pos,
    'kb_lengths': kb_lengths
}

logic_masks, logic_operators = load_logic_info(additional_data=additional_data,
                                               key_vocab=key_vocab,
                                               category='LTD')
print(logic_masks.shape)
print(logic_operators.shape)
