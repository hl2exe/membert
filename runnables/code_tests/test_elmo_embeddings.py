"""

Testing ELMo embedding layer

"""

import tensorflow as tf
import tensorflow_hub as hub


class CustomModel(tf.keras.Model):

    def __init__(self):
        super(CustomModel, self).__init__()
        self.elmo_embedding = ElmoEmbeddingLayer()

    def call(self, x):
        emb_x = self.elmo_embedding(x)
        return emb_x


class ElmoEmbeddingLayer(tf.keras.layers.Layer):
    def __init__(self, dimensions=32, **kwargs):
        super(ElmoEmbeddingLayer, self).__init__(**kwargs)
        self.dimensions = dimensions
        self.trainable = True

    def build(self, input_shape):
        self.elmo = hub.load('https://tfhub.dev/google/elmo/2')
        # Trainable weights restricted as described in the paper: https://tfhub.dev/google/elmo/2
        # self._trainable_weights += self.elmo.variables[-2:]
        super(ElmoEmbeddingLayer, self).build(input_shape)

    def call(self, x, mask=None):
        result = self.elmo.signatures['default'](x)
        return result['elmo']

    def compute_mask(self, inputs, mask=None):
        return tf.not_equal(inputs, '--PAD--')


if __name__ == '__main__':

    my_model = CustomModel()
    texts = ['the cat is on the table', 'the cake is a lie']
    texts = tf.constant(texts, dtype=tf.string)
    with tf.GradientTape() as tape:
        res = my_model(texts)
    print(res)