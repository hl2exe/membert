"""

Testing framework pipeline with tf.Dataset so far

"""
import os
from collections import OrderedDict

import const_define as cd


# Debug
def show_batch(dataset, takes=1):
    for batch in dataset.take(takes):
        if type(batch) in [dict, OrderedDict]:
            for key, value in batch.items():
                print('{:20s}: {}'.format(key, value.numpy()))
        else:
            print(batch.numpy())


# Step 1: load dataset
from old_stuff.tf_data_loader import Task1KBLoader

dataset_path = os.path.join(cd.TOS_100_DIR, 'dataset.csv')
max_take = 512
label = 'LTD'

loader = Task1KBLoader()
data_handle = loader.load(path=dataset_path,
                          category=label,
                          batch_size=1,
                          num_epochs=1,
                          shuffle=False,
                          select_columns=[label, 'text', 'document'])

# Select minor amount of data
# data_handle.data = data_handle.data.take(max_take)

# Debug
show_batch(data_handle.data, takes=1)

# Step 2: preprocessing
from old_stuff.experimental_preprocessing import TextPreprocessor

preprocessor = TextPreprocessor(function_names=[
    'punctuation_filtering',
    'sentence_to_lower',
    'tf_remove_special_words',
    'tf_number_replacing_with_constant'
])
tf_data, additional_info = preprocessor.parse(data=data_handle.data,
                                              additional_info=data_handle.get_additional_info(),
                                              return_info=True)

# Debug
# show_batch(tf_data, takes=1)

# Step 3: converter
from old_stuff.experimental_converter import BaseConverter

converter = BaseConverter(label=label, text_column='text')

x_train, y_train, text_info, additional_info = converter.fit_train_data(train_data=tf_data,
                                                                        additional_data=additional_info)

# Debug
show_batch(x_train)


# Model fitting
from nn_models_v2 import Baseline_LSTM

x_val = x_train.skip(int(max_take / 2))
y_val = y_train.skip(int(max_take / 2))
x_train = x_train.take(int(max_take / 2))
y_train = y_train.take(int(max_take / 2))

network = Baseline_LSTM(lstm_weights=[32],
                        answer_weights=[],
                        optimizer_args={'lr': 1e-3},
                        embedding_dimension=converter.embedding_dimension)
network.build_model(text_info=text_info)
network.compute_output_weights(y_train=y_train, num_classes=1)
additional_metrics_info = {
    "f1_score": {
      "labels": [
        0,
        1
      ],
      "pos_label": 1,
      "average": "binary"
    },
}
network.fit(x=x_train, y=y_train, batch_size=32, epochs=10, metrics=['f1_score'],
            validation_data=[x_val, y_val], additional_metrics_info=additional_metrics_info
            )
