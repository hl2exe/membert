"""

Testing python hyperopt nested spaces

"""

from configs.hyperopt_spaces import spaces
import pprint
from hyperopt import pyll

model_type = 'basic_memn2n_v1'
model_space = spaces[model_type]

pp = pprint.PrettyPrinter(indent=4, width=100)
samples = 10

for _ in range(samples):
    print('*' * 50)
    pp.pprint(pyll.stochastic.sample(model_space))
    print('*' * 50)
