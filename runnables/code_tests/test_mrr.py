"""

Testing MRR code

"""

import numpy as np


def get_best_target_rank(attention_values, target_values):
    sorted_attention_indexes = np.argsort(attention_values, axis=1)[:, ::-1]
    ranks = [np.where(np.in1d(sorted_attention_indexes[hop], target_values))[0] for hop in
             range(sorted_attention_indexes.shape[0])]
    return ranks[0][0]


def compute_mrr(attention_values, target_values):
    mrr = []
    for att_vals, targets in zip(attention_values, target_values):
        best_target_rank = get_best_target_rank(att_vals, targets)
        mrr.append(1 / (best_target_rank + 1))

    return np.mean(mrr)


# 1/3 + 1/1 + 1/1 -> 7/3 -> 7/9

my_atts = np.array([0.3, 0.5, 0.6, 0.8, 0.,
                    0., 0., 0.9, 0.1, 0.4,
                    0.3, 0.7, 0.8, 0., 0.1]).reshape(3, 1, 5)
my_targets = [[1, 5],
              [2, 0],
              [2]]

print(compute_mrr(my_atts, my_targets))
