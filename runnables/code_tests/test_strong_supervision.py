

import numpy as np
import tensorflow as tf


padding_amount = 2
margin = 0.2

# [batch_size, mem_size]
prob_dist = np.array([[0.9, 0.3, 0.1], [0.5, 0.6, 0.7]])

# [batch_size, paddding_amount]
positive_idxs = np.array([[0, 0], [1, 0]])
negative_idxs = np.array([[1, 2], [2, 2]])
mask_idxs = np.array([[1, 1], [1, 1]])


# Tf

mask_res = tf.tile(mask_idxs, multiples=[1, padding_amount])
mask_res = tf.reshape(mask_res, [-1, padding_amount])

pos_scores = tf.gather(prob_dist, positive_idxs, batch_dims=1)
pos_scores = tf.reshape(pos_scores, [-1, 1])

neg_scores = tf.gather(prob_dist, negative_idxs, batch_dims=1)
neg_scores = tf.tile(neg_scores, multiples=[1, padding_amount])
neg_scores = tf.reshape(neg_scores, [-1, padding_amount])

hop_supervision_loss = tf.maximum(0., margin - pos_scores + neg_scores)
hop_supervision_loss = hop_supervision_loss * tf.cast(mask_res, dtype=hop_supervision_loss.dtype)
hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, padding_amount, padding_amount])
hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=2)


print(hop_supervision_loss)