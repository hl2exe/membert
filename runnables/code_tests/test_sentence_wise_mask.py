"""

Sentence-wise mask for Transformer: KB sentences should be independent

"""

import numpy as np

concat_kb = np.arange(20) + 1
kb = [[1, 2, 3, 4, 5], [6, 7, 8], [9, 10, 11, 12], [13, 14, 15], [16, 17, 18, 19, 20]]

sentence_wise_mask = np.ones([len(concat_kb), len(concat_kb)])

start_idx = 0
for sentence in kb:
    current_triu = np.zeros(shape=[len(sentence), len(sentence)])
    current_triu[np.triu_indices(len(sentence), 1)] = 1
    sentence_wise_mask[start_idx:current_triu.shape[0] + start_idx,
    start_idx:current_triu.shape[1] + start_idx] = current_triu
    start_idx += len(sentence)

print(sentence_wise_mask)
