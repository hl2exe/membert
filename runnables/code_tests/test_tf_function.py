
import tensorflow as tf
import numpy as np

alpha = 1.

@tf.function
def test(x, y):
    return tf.reduce_sum(x, axis=1) * tf.expand_dims(y, -1)


placeholder = np.random.rand(3, 10).astype(np.float32)
result = test(tf.constant(placeholder), tf.constant(alpha))

print(placeholder)
print('Result: ', result)

alpha = 0.

result = test(tf.constant(placeholder), tf.constant(alpha))
print('Result: ', result)