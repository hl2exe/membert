"""

Mask based SS that is efficient and compliant with sampling strategies and example-wise sampling

"""

import numpy as np
import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


def index_based_SS(prob_dist, positive_idxs, negative_idxs, mask_idxs, padding_amount=3, margin=0.5):
    # Repeat mask for each positive element in each sample memory
    # Mask_idxs shape: [batch_size, padding_amount]
    # Mask res shape: [batch_size * padding_amount, padding_amount]
    mask_res = tf.tile(mask_idxs, multiples=[1, padding_amount])
    mask_res = tf.reshape(mask_res, [-1, padding_amount, padding_amount])
    mask_res = tf.transpose(mask_res, [0, 2, 1])
    mask_res = tf.reshape(mask_res, [-1, padding_amount])

    # Split each similarity score for a target into a separate sample
    # similarities shape: [batch_size, memory_max_length]
    # positive_idxs shape: [batch_size, padding_amount]
    # gather_nd shape: [batch_size, padding_amount]
    # pos_scores shape: [batch_size * padding_amount, 1]
    pos_scores = tf.gather(prob_dist, positive_idxs, batch_dims=1)
    pos_scores = tf.reshape(pos_scores, [-1, 1])

    # Repeat similarity scores for non-target memories for each positive score
    # similarities shape: [batch_size, memory_max_length]
    # negative_idxs shape: [batch_size, padding_amount]
    # neg_scores shape: [batch_size * padding_amount, padding_amount]
    neg_scores = tf.gather(prob_dist, negative_idxs, batch_dims=1)
    neg_scores = tf.tile(neg_scores, multiples=[1, padding_amount])
    neg_scores = tf.reshape(neg_scores, [-1, padding_amount])

    # Compare each single positive score with all corresponding negative scores
    # [batch_size * padding_amount, padding_amount]
    # [batch_size, padding_amount]
    # [batch_size, 1]
    # Samples without supervision are ignored by applying a zero mask (mask_res)
    hop_supervision_loss = tf.maximum(0., margin - pos_scores + neg_scores)
    hop_supervision_loss = hop_supervision_loss * tf.cast(mask_res, dtype=hop_supervision_loss.dtype)
    hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, padding_amount, padding_amount])

    hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=[1, 2])
    # hop_supervision_loss = tf.reduce_max(hop_supervision_loss, axis=1)
    normalization_factor = tf.cast(tf.reshape(mask_res, [-1, padding_amount, padding_amount]),
                                   hop_supervision_loss.dtype)
    normalization_factor = tf.reduce_sum(normalization_factor, axis=[1, 2])
    normalization_factor = tf.maximum(normalization_factor, tf.ones_like(normalization_factor))
    hop_supervision_loss = tf.reduce_sum(hop_supervision_loss / normalization_factor)

    # Normalize by number of unfair examples
    valid_examples = tf.reduce_sum(mask_idxs, axis=1)
    valid_examples = tf.cast(valid_examples, tf.float64)
    valid_examples = tf.minimum(valid_examples, 1.0)
    valid_examples = tf.reduce_sum(valid_examples)
    valid_examples = tf.maximum(valid_examples, 1.0)
    hop_supervision_loss = hop_supervision_loss / valid_examples

    return hop_supervision_loss


def mask_based_SS(scores, targets):

    targets = tf.cast(targets, scores.dtype)

    positive_scores = scores * targets
    negative_scores = scores * (1. - targets)

    supervision_loss = negative_scores[:, None, :] + margin - positive_scores[:, :, None]
    supervision_loss = tf.maximum(0., supervision_loss)

    normalization_factor = (1 - targets[:, None, :]) * targets[:, :, None]
    supervision_loss = supervision_loss * normalization_factor

    supervision_loss = tf.reduce_sum(supervision_loss, axis=[1, 2])

    normalization_factor = tf.reduce_sum(normalization_factor, axis=[1, 2])
    normalization_factor = tf.maximum(1.0, normalization_factor)

    supervision_loss = supervision_loss / normalization_factor
    return tf.reduce_mean(supervision_loss)


targets = np.array([
    0, 0, 0, 1, 1,
    1, 0, 0, 0, 0
]).reshape(2, -1)

attention_scores = np.array([
    0.5, 0.1, 0.3, 0.3, 0.8,
    1.0, 0.3, 0.5, 0.8, 0.
]).reshape(2, -1)

margin = 0.5

positive_indexes = np.array([
    3, 4, 4, 4,
    0, 0, 0, 0
]).reshape(2, -1)
negative_indexes = np.array([
    0, 1, 2, 2,
    1, 2, 3, 4
]).reshape(1, -1)
mask_indexes = np.array([
    1, 1, 0, 0,
    1, 0, 0, 0
]).reshape(2, -1)

padding_amount = positive_indexes.shape[-1]

# print(index_based_SS(prob_dist=attention_scores,
#                      positive_idxs=positive_indexes,
#                      negative_idxs=negative_indexes,
#                      mask_idxs=mask_indexes,
#                      margin=margin,
#                      padding_amount=padding_amount).numpy())

print(mask_based_SS(scores=attention_scores,
                    targets=targets).numpy())