"""

Testing tf.dataset from generator with memory sampling

"""

import numpy as np
import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

inputs = {
    "query1": {
        "memory": ["m1", "m2", "m3", "m4", "m5", "m6"],
        "targets": ["m3", "m6"]
    },
    "query2": {
        "memory": ["m1", "m2", "m3", "m4", "m5", "m6"],
        "targets": ["m1"]
    },
    "query3": {
        "memory": ["m1", "m2", "m3", "m4", "m5", "m6"],
        "targets": ["m4", "m2", "m1"]
    }
}

sampling_size = 3


def my_generator():
    for query, value in inputs.items():
        targets = value['targets']
        memory = value['memory']

        sampled_memory = np.random.choice(memory, size=sampling_size, replace=False)
        has_target = set(sampled_memory).intersection(set(targets))
        if not len(has_target):
            idx_to_remove = np.random.randint(low=0, high=len(sampled_memory), size=1)[0]
            target_to_pick = np.random.randint(low=0, high=len(targets), size=1)[0]
            sampled_memory[idx_to_remove] = targets[target_to_pick]

        yield query, sampled_memory


dataset = tf.data.Dataset.from_generator(my_generator,
                                         output_types=(tf.string, tf.string))
dataset = dataset.repeat(count=5)

for item in dataset:
    print(item)
