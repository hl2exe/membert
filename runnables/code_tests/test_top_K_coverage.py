import numpy as np


def get_predictions(attention_values):
    return np.where(attention_values >= 0.5)[-1].tolist()


def get_top_K_predictions(attention_values, K):
    best_indexes = np.argsort(attention_values, axis=1)[:, ::-1]
    valid_mask = attention_values < 0.5
    sorted_valid_mask = np.take_along_axis(valid_mask, best_indexes, axis=1)
    best_indexes[sorted_valid_mask] = -1
    best_indexes = best_indexes[:, :K]
    return best_indexes.ravel().tolist()


samples = 25
mem_size = 8
hops = 1
K = 3

attention_values = np.random.rand(samples, hops, mem_size)
targets = np.random.randint(low=0, high=mem_size, size=samples * 2).reshape(samples, 2)

total_top_K_hits = [0] * K
total_usage = 0
for att_weights, target_values in zip(attention_values, targets):
    predicted_values = get_predictions(att_weights)

    print('Values: ', att_weights)
    print('Targets: ', target_values)
    print('Predicted: ', predicted_values)

    if len(predicted_values) > 0:
        total_usage += 1
        top_K_predictions = get_top_K_predictions(att_weights, K=K)
        total_top_K_hits = [count + 1 if len(set(top_K_predictions[:idx + 1]).intersection(set(target_values)))
                            else count
                            for idx, count in enumerate(total_top_K_hits)]
        print('Top K Predictions: ', top_K_predictions)

    print('Total Top K Hits: ', total_top_K_hits)
    print()
    print()

total_top_K_hits = np.array(total_top_K_hits)
if total_usage == 0:
    top_K_coverage_precision = None
else:
    top_K_coverage_precision = total_top_K_hits / total_usage

print('Top K percentage: ', top_K_coverage_precision)
