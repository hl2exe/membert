import tensorflow as tf

data = tf.data.Dataset.range(12)
data = data.batch(2)
# data = data.repeat(2)

kb = tf.data.Dataset.range(2).batch(2).repeat()

to_fit = tf.data.Dataset.zip((data, kb))
to_fit = to_fit.shuffle(12)
# to_fit = to_fit.batch(2)

for epoch in range(1):
    for batch in to_fit:
        print(batch)