"""

@Author: Federico Ruggeri

@Date: 17/05/2019

"""

import string
from collections import OrderedDict
from functools import reduce

import six
from nltk import word_tokenize
from spacy.lang.en import English
from transformers import BertTokenizer, RobertaTokenizer, DistilBertTokenizer
from tokenization import SpacyTokenizer

squad_tokenizers = {
    'bert': BertTokenizer,
    'roberta': RobertaTokenizer,
    'distilbert': DistilBertTokenizer,
    'spacy': SpacyTokenizer

}

spacy_tokenizer = English()
special_words = [
    "-lrb-",
    "-rrb-",
    "i.e.",
    "``",
    "\'\'",
    "lrb",
    "rrb"
]


def is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
        return True
    return False


def clean_squad_text(text):
    text = text.replace("]", " ] ")
    text = text.replace("[", " [ ")
    text = text.replace("\n", " ")
    text = text.replace("''", '" ').replace("``", '" ')

    return text


def squad_get_token_indexes(text, tokenizer):
    current = 0
    spans = []
    tokens = tokenizer.tokenize(text)
    for token in tokens:
        current = text.find(token, current)
        if current < 0:
            raise RuntimeError("Token {} cannot be found".format(token))
        spans.append((current, current + len(token)))
        current += len(token)
    return spans, tokens


def squad_bert_get_token_indexes(text, tokenizer, support_tokenizer):
    # Use spacy tokenizer (more solid tokenization)
    tokens_spans, doc_tokens = squad_get_token_indexes(text=text, tokenizer=support_tokenizer)
    current = 0
    char_to_word_offset = []
    for idx, (start, end) in enumerate(tokens_spans):
        while current < start:
            char_to_word_offset.append(idx - 1)
            current += 1
        while current < end:
            char_to_word_offset.append(idx)
            current += 1

    # Add remaining indexes if spaces are left in text
    if len(char_to_word_offset) < len(text):
        char_to_word_offset += [char_to_word_offset[-1]] * (len(text) - len(char_to_word_offset))

    assert len(char_to_word_offset) == len(text), (len(char_to_word_offset), len(text))

    bert_tokens_ids = [tokenizer(token)['input_ids'][1:-1] for token in doc_tokens]
    bert_tokens = [tokenizer.convert_ids_to_tokens(tok_seq) for tok_seq in bert_tokens_ids]
    return bert_tokens_ids, bert_tokens, char_to_word_offset, doc_tokens


def get_squad_tokenizer(name):
    return squad_tokenizers[name]


def punctuation_filtering(line):
    """
    Filters given sentences by removing punctuation
    """

    table = str.maketrans('', '', string.punctuation)
    trans = [w.translate(table) for w in line.split()]

    return ' '.join([w for w in trans if w != ''])


def to_compact(text):
    table = str.maketrans('', '', string.punctuation)
    trans = [w.translate(table) for w in text.split()]
    return ''.join([w for w in trans if w != ''])


def convert_to_unicode(text):
    """Converts `text` to Unicode (if it's not already), assuming utf-8 input."""
    if six.PY3:
        if isinstance(text, str):
            return text
        elif isinstance(text, bytes):
            return text.decode("utf-8", "ignore")
        else:
            raise ValueError("Unsupported string type: %s" % (type(text)))
    elif six.PY2:
        if isinstance(text, str):
            return text.decode("utf-8", "ignore")
        elif isinstance(text, unicode):
            return text
        else:
            raise ValueError("Unsupported string type: %s" % (type(text)))
    else:
        raise ValueError("Not running on Python2 or Python 3?")


def remove_special_words(line):
    """
    Removes any pre-defined special word
    """

    words = word_tokenize(line)
    filtered = []
    for w in words:
        if w not in special_words:
            filtered.append(w)

    line = ' '.join(filtered)
    return line


def number_replacing_with_constant(line):
    """
    Replaces any number with a fixed special token
    """

    words = word_tokenize(line)
    filtered = []
    for w in words:
        try:
            int(w)
            filtered.append('SPECIALNUMBER')
        except ValueError:
            filtered.append(w)
            continue

    line = ' '.join(filtered)
    return line
    # return re.sub('[0-9][0-9.,-]*', 'SPECIALNUMBER', line)


def sentence_to_lower(line):
    return line.lower()


filter_methods = OrderedDict(
    [
        ('convert_to_unicode', convert_to_unicode),
        ('sentence_to_lower', sentence_to_lower),
        ('punctuation_filtering', punctuation_filtering),
        ('number_replacing_with_constant', number_replacing_with_constant),
        ('remove_special_words', remove_special_words),
    ]
)


def filter_line(line, function_names=None):
    """
    General filtering proxy function that applies a sub-set of the supported
    filtering methods to given sentences.
    """

    if function_names is None:
        function_names = list(filter_methods.keys())

    functions = [filter_methods[name] for name in function_names]
    return reduce(lambda r, f: f(r), functions, line.strip().lower())


def compute_jaccard_similarity(sent1, sent2, lemmatizer=None):
    """
    Hp: text has already been parsed.
    """

    sent1_words = set(sent1.split(' '))
    sent2_words = set(sent2.split(' '))

    if lemmatizer is not None:
        sent1_words = set(map(lambda word: lemmatizer.lemmatize(word), sent1_words))
        sent1_words = set(map(lambda word: lemmatizer.lemmatize(word), sent1_words))

    intersection = sent1_words.intersection(sent2_words)
    union = sent1_words.union(sent2_words)

    return float(len(intersection)) / len(union)
