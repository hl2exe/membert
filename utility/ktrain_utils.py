from ktrain.text.preprocessor import TransformerSequence
import tensorflow as tf
import numpy as np


class KBTransformerSequence(TransformerSequence):

    def __init__(self, kb_size, **kwargs):
        super(KBTransformerSequence, self).__init__(**kwargs)
        self.kb_size = kb_size

    @staticmethod
    def from_transformer_sequences(input_sequence, kb_sequence):

        # [#samples, 3, seq_len]
        input_samples = input_sequence.x

        # [|KB|, 3, seq_len]
        kb_samples = kb_sequence.x
        kb_size = kb_samples.shape[0]

        # [1, 3 * |KB|, seq_len]
        kb_samples = kb_samples.reshape([1, -1, kb_samples.shape[-1]])

        # [# samples, 3 * |KB|, seq_len]
        kb_samples = np.repeat(kb_samples, repeats=input_samples.shape[0], axis=0)

        # [#samples, 3 + 3 * |KB|, seq_len]
        x = np.concatenate((input_samples, kb_samples), axis=1)

        return KBTransformerSequence(x=x, y=input_sequence.y, batch_size=input_sequence.batch_size, kb_size=kb_size)

    def to_tfdataset(self, shuffle=True, repeat=True):
        """
        convert transformer features to tf.Dataset
        """
        tfdataset = tf.data.Dataset.from_tensor_slices((self.x, self.y))
        tfdataset = tfdataset.map(lambda x, y: ({'clause_input_ids': x[0],
                                                 'clause_attention_mask': x[1],
                                                 'clause_token_type_ids': x[2],
                                                 'memory_input_ids':
                                                     [x[3 + (idx * 3)] for idx in range(self.kb_size)],
                                                 'memory_attention_mask':
                                                     [x[4 + (idx * 3)] for idx in range(self.kb_size)],
                                                 'memory_token_type_ids':
                                                     [x[5 + (idx * 3)] for idx in range(self.kb_size)]
                                                 }
                                                , y))
        if shuffle:
            tfdataset = tfdataset.shuffle(self.x.shape[0])
        tfdataset = tfdataset.batch(self.batch_size)
        if repeat:
            tfdataset = tfdataset.repeat(-1)
        return tfdataset
