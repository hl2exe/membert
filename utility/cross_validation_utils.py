"""

@Author: Federico Ruggeri
@Date: 22/03/2019

TODO: merge compute_metrics and compute_iteration_validation_error

"""

import numpy as np
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.model_selection._split import _BaseKFold

from collections import OrderedDict

from utility.python_utils import merge
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.metric_utils import compute_batch_span_f1, compute_batch_em, compute_batch_answer_f1, pairwise_f1_score

logger = get_logger(__name__)

supported_custom_metrics = {
    'squad_answer_f1': compute_batch_answer_f1,
    'squad_f1': compute_batch_span_f1,
    'squad_exact_match': compute_batch_em,
    'pairwise_f1': pairwise_f1_score
}


def build_metrics(error_metrics):
    """
    Build validation metrics from given metrics name and problem type

    :param error_metrics: list of error metrics names (strings)
    :return: list of pointers to error metrics functions
    """

    parsed_metrics = []

    for metric in error_metrics:
        if hasattr(metrics, metric):
            parsed_metrics.append(getattr(metrics, metric))
        elif metric in supported_custom_metrics:
            parsed_metrics.append(supported_custom_metrics[metric])
        else:
            logger.warn('Unknown metric given (got {})! Skipping...'.format(metric))

    # Empty metrics list
    if not parsed_metrics:
        message = 'No valid metrics found! Aborting...'
        logger.error(message)
        raise RuntimeError(message)

    return parsed_metrics


def show_data_shapes(data, key):
    """
    Prints data shape (it could be a nested structure).
    Currently, the following nested structures are supported:

        1) List
        2) Numpy.ndarray
        3) Dict
    """

    if type(data) is np.ndarray:
        data_shape = data.shape
    elif type(data) is list:
        data_shape = [len(item) for item in data]
    else:
        data_shape = [(key, len(item)) for key, item in data.items()]

    logger.info('[{0}] Shape(s): {1}'.format(key, data_shape))


def _compute_iteration_validation_error(parsed_metrics, true_values, predicted_values,
                                        error_metrics_additional_info=None,
                                        error_metrics_nicknames=None,
                                        prefix=None,
                                        label_suffix=None):
    """
    Computes each given metric value, given true and predicted values.

    :param parsed_metrics: list of metric functions (typically sci-kit learn metrics)
    :param true_values: ground-truth values
    :param predicted_values: model predicted values
    :param error_metrics_additional_info: additional arguments for each metric
    :param error_metrics_nicknames: custom nicknames for each metric
    :return: dict as follows:

        key: metric.__name__
        value: computed metric value
    """

    error_metrics_additional_info = error_metrics_additional_info or {}
    fold_error_info = {}

    if len(true_values.shape) > 1 and true_values.shape[1] > 1:
        if predicted_values.shape == true_values.shape:
            predicted_values = np.argmax(predicted_values, axis=1)

        true_values = np.argmax(true_values, axis=1)

    if error_metrics_nicknames is None:
        error_metrics_nicknames = [metric.__name__ for metric in parsed_metrics]

    for metric, metric_info, metric_name in zip(parsed_metrics,
                                                error_metrics_additional_info,
                                                error_metrics_nicknames):
        signal_error = metric(y_true=true_values, y_pred=predicted_values,
                              **metric_info)

        if label_suffix is not None:
            metric_name = '{0}_{1}'.format(label_suffix, metric_name)
        if prefix is not None:
            metric_name = '{0}_{1}'.format(prefix, metric_name)

        fold_error_info.setdefault(metric_name, signal_error)

    return fold_error_info


def compute_iteration_validation_error(parsed_metrics, true_values, predicted_values,
                                       error_metrics_additional_info=None,
                                       error_metrics_nicknames=None,
                                       error_metrics_flags=None,
                                       prefix=None):
    """
    Computes each given metric value, given true and predicted values.

    :param parsed_metrics: list of metric functions (typically sci-kit learn metrics)
    :param true_values: ground-truth values
    :param predicted_values: model predicted values
    :param error_metrics_additional_info: additional arguments for each metric
    :param error_metrics_nicknames: custom nicknames for each metric
    :return: dict as follows:

        key: metric.__name__
        value: computed metric value
    """

    error_metrics_additional_info = error_metrics_additional_info or {}
    fold_error_info = {}

    # Multi-output scenario
    if type(true_values) in [dict, OrderedDict]:
        for key, true_value_set in true_values.items():
            pred_value_set = predicted_values[key]
            pred_value_set = np.reshape(pred_value_set, true_value_set.shape)

            if error_metrics_nicknames is not None and error_metrics_flags is not None:
                if key not in error_metrics_flags:
                    continue

                key_indexes = [error_metrics_nicknames.index(item) for item in error_metrics_flags[key]]
                key_parsed_metrics = [metric for idx, metric in enumerate(parsed_metrics) if idx in key_indexes]
                key_error_metrics_additional_info = [info for idx, info in enumerate(error_metrics_additional_info)
                                                     if idx in key_indexes]
                key_error_metrics_nicknames = [name for idx, name in enumerate(error_metrics_nicknames)
                                               if idx in key_indexes]
            else:
                key_parsed_metrics = parsed_metrics
                key_error_metrics_additional_info = error_metrics_additional_info
                key_error_metrics_nicknames = error_metrics_nicknames

            key_error_info = _compute_iteration_validation_error(parsed_metrics=key_parsed_metrics,
                                                                 true_values=true_value_set,
                                                                 predicted_values=pred_value_set,
                                                                 error_metrics_additional_info=key_error_metrics_additional_info,
                                                                 error_metrics_nicknames=key_error_metrics_nicknames,
                                                                 label_suffix=key,
                                                                 prefix=prefix)
            fold_error_info = merge(fold_error_info, key_error_info)

        # Add also summary metrics for compatibility
        if error_metrics_nicknames is None:
            error_metrics_nicknames = [metric.__name__ for metric in parsed_metrics]

        for metric, metric_info, metric_name in zip(parsed_metrics,
                                                    error_metrics_additional_info,
                                                    error_metrics_nicknames):
            metric_error_info = [fold_error_info[key] for key in fold_error_info if metric_name in key]
            summary_metric_error = np.mean(metric_error_info)

            if prefix is not None:
                metric_name = '{0}_{1}'.format(prefix, metric_name)

            fold_error_info[metric_name] = summary_metric_error

    # Case multiple model outputs for single output
    elif type(predicted_values) in [dict, OrderedDict] and type(true_values) not in [dict, OrderedDict]:
        for key, key_pred_values in predicted_values.items():
            key_error_metrics_nicknames = [item + '_{}'.format(key) for item in error_metrics_nicknames]
            key_fold_error_info = _compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=true_values,
                                                                      predicted_values=key_pred_values,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=key_error_metrics_nicknames,
                                                                      prefix=prefix)
            fold_error_info = merge(fold_error_info, key_fold_error_info)

    # Case single model output for single output
    else:
        fold_error_info = _compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                              true_values=true_values,
                                                              predicted_values=predicted_values,
                                                              error_metrics_additional_info=error_metrics_additional_info,
                                                              error_metrics_nicknames=error_metrics_nicknames,
                                                              prefix=prefix)

    return fold_error_info


# TODO: move to python_utils (make more general)
def update_cv_validation_info(test_validation_info, iteration_validation_info):
    """
    Updates a dictionary with given values
    """

    test_validation_info = test_validation_info or {}

    for metric in iteration_validation_info:
        test_validation_info.setdefault(metric, []).append(iteration_validation_info[metric])

    return test_validation_info


def mp_update_cv_info(iteration_info, repetition_id, fold_idx, info=None):
    """
    Updates a dictionary with given values.
    """

    info = info or {}

    for metric in iteration_info:
        info.setdefault(metric, {}).setdefault(fold_idx, {}).setdefault(repetition_id, iteration_info[metric])

    return info


def average_validation_info(test_validation_info):
    """
    Computes the mean value of each metric value list.
    """

    for metric in test_validation_info:
        test_validation_info[metric] = np.mean(test_validation_info[metric])

    return test_validation_info


def get_validate_condition_value(validation_error, validate_on):
    """
    Returns the metric to validate on during hyper-parameters calibration.
    """

    return validation_error[validate_on]


class PrebuiltCV(_BaseKFold):
    """
    Simple CV wrapper for custom fold definition.
    """

    def __init__(self, cv_type='kfold', held_out_key='validation', **kwargs):
        super(PrebuiltCV, self).__init__(**kwargs)
        self.folds = None
        self.key_listing = None
        self.held_out_key = held_out_key

        if cv_type == 'kfold':
            self.cv = KFold(n_splits=self.n_splits, shuffle=self.shuffle)
        elif cv_type == 'stratifiedkfold':
            self.cv = StratifiedKFold(n_splits=self.n_splits, shuffle=self.shuffle)
        else:
            raise AttributeError('Invalid cv_type! Got: {}'.format(cv_type))

    def build_folds(self, X, y):
        self.folds = {}
        for fold, (train_indexes, held_out_indexes) in enumerate(self.cv.split(X, y)):
            self.folds['fold_{}'.format(fold)] = {
                'train': train_indexes,
                self.held_out_key: held_out_indexes
            }

    def build_all_sets_folds(self, X, y, validation_n_splits=None):
        assert self.held_out_key == 'test'

        validation_n_splits = self.n_splits if validation_n_splits is None else validation_n_splits

        self.folds = {}
        for fold, (train_indexes, held_out_indexes) in enumerate(self.cv.split(X, y)):
            sub_X = X[train_indexes]
            sub_y = y[train_indexes]

            self.cv.n_splits = validation_n_splits
            sub_train_indexes, sub_val_indexes = list(self.cv.split(sub_X, sub_y))[0]
            self.cv.n_splits = self.n_splits

            self.folds['fold_{}'.format(fold)] = {
                'train': train_indexes[sub_train_indexes],
                self.held_out_key: held_out_indexes,
                'validation': train_indexes[sub_val_indexes]
            }

    def load_dataset_list(self, load_path):
        with open(load_path, 'r') as f:
            dataset_list = [item.strip() for item in f.readlines()]

        return dataset_list

    def save_folds(self, save_path, tolist=False):

        if tolist:
            to_save = {}
            for fold_key in self.folds:
                for split_set in self.folds[fold_key]:
                    to_save.setdefault(fold_key, {}).setdefault(split_set, self.folds[fold_key][split_set].tolist())
            save_json(save_path, to_save)
        else:
            save_json(save_path, self.folds)

    def load_folds(self, load_path):
        self.folds = load_json(load_path)
        self.n_splits = len(self.folds)
        key_path = load_path.split('.json')[0] + '.txt'
        with open(key_path, 'r') as f:
            self.key_listing = list(map(lambda item: item.strip(), f.readlines()))
        self.key_listing = np.array(self.key_listing)

    def _iter_test_indices(self, X=None, y=None, groups=None):

        fold_list = sorted(list(self.folds.keys()))

        for fold in fold_list:
            yield self.folds[fold][self.held_out_key]

    def split(self, X, y=None, groups=None):

        fold_list = sorted(list(self.folds.keys()))

        for fold in fold_list:
            val_indexes = self.key_listing[self.folds[fold]['validation']] if 'validation' in self.folds[fold] else None
            test_indexes = self.key_listing[self.folds[fold]['test']] if 'test' in self.folds[fold] else None

            assert val_indexes is not None or test_indexes is not None

            yield self.key_listing[self.folds[fold]['train']], \
                  val_indexes, \
                  test_indexes
