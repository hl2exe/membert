"""

@Author: Federico Ruggeri

@Date: 24/09/2019

"""

from __future__ import division

import os
from collections import OrderedDict
from copy import deepcopy

import numpy as np
import tensorflow as tf
from tensorflow.python.keras import backend as K

import const_define as cd
from custom_callbacks_v2 import TensorBoard
from data_converter import DataConverterFactory
from data_processor import ProcessorFactory
from dataset_samplers import SamplerFactory
from nn_models_v2 import ModelFactory, ClassificationNetwork
from tokenization import TokenizerFactory
from utility.cross_validation_utils import build_metrics, compute_iteration_validation_error, update_cv_validation_info
from utility.data_utils import get_data_config_id
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.pipeline_utils import get_dataset_fn, retrieve_numpy_labels
from utility.printing_utils import prettify_statistics
from utility.python_utils import flatten
from utility.python_utils import merge

logger = get_logger(__name__)


# TODO: update
def distributed_cross_validation(validation_percentage, data_handle, cv, strategy, data_loader_info,
                                 model_type, network_args, training_config, test_path,
                                 error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                                 callbacks=None, compute_test_info=True, save_predictions=False,
                                 use_tensorboard=False, repetitions=1, save_model=False,
                                 split_key=None, checkpoint=None, distributed_info=None, preloaded=False,
                                 load_externally=False, preloaded_model=None):
    """
    [Repeated] Cross-validation routine:

        1. [For each repetition]

        2. For each fold:
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        logger.error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    if preloaded and load_externally:
        tokenizer_class = TokenizerFactory.supported_tokenizers[tokenizer_type]
        tokenizer = tokenizer_class.from_pretrained(network_args['preloaded_name']['value'])
    else:
        tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
        tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)
    converter_instance_args = converter.get_instance_args()

    # Build sampler
    if 'sampler' in cd.MODEL_CONFIG[model_type]:
        sampler_type = cd.MODEL_CONFIG[model_type]['sampler']
        sampler_args = {key: arg['value'] for key, arg in network_args.items() if 'sampler' in arg['flags']}
        sampler = SamplerFactory.factory(sampler_type, **sampler_args)
        sampling = network_args['sampling']['value'] if 'sampling' in network_args else False
    else:
        sampler = None
        sampling = False

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        logger.info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        prediction_info = OrderedDict()

        for fold_idx, (train_indexes, test_indexes) in enumerate(cv.split(None)):
            logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                 prefix=save_prefix)
                converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            logger.info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            selector = converter.feature_class.get_dataset_selector()
            name_to_features = converter.feature_class.get_mappings(converter_info, converter_instance_args)

            if sampler is not None:
                sampler.set_conversion_args(converter_info)

            with strategy.scope():
                train_data = get_dataset_fn(filepath=train_filepath,
                                            batch_size=training_config['batch_size'],
                                            name_to_features=name_to_features,
                                            selector=selector,
                                            is_training=True,
                                            shuffle_amount=distributed_info['shuffle_amount'],
                                            reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                            prefetch_amount=distributed_info['prefetch_amount'],
                                            sampling=sampling,
                                            sampler=sampler)

                fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                                  batch_size=training_config['batch_size'],
                                                  name_to_features=name_to_features,
                                                  selector=selector,
                                                  is_training=False,
                                                  prefetch_amount=distributed_info['prefetch_amount'])

                val_data = get_dataset_fn(filepath=val_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=name_to_features,
                                          selector=selector,
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

                test_data = get_dataset_fn(filepath=test_filepath,
                                           batch_size=training_config['batch_size'],
                                           name_to_features=name_to_features,
                                           selector=selector,
                                           is_training=False,
                                           prefetch_amount=distributed_info['prefetch_amount'])

                # Build model

                network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                          if 'model_class' in value['flags']}
                network_retrieved_args['additional_data'] = data_handle.get_additional_info()
                network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                    cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
                network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

                # Useful stuff
                train_steps = int(np.ceil(train_df.shape[0] / training_config['batch_size']))
                eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
                test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

                np_train_y = retrieve_numpy_labels(data_fn=fixed_train_data, steps=train_steps)
                np_val_y = retrieve_numpy_labels(data_fn=val_data, steps=eval_steps)
                np_test_y = retrieve_numpy_labels(data_fn=test_data, steps=test_steps)

                logger.info('Total train steps: {}'.format(train_steps))
                logger.info('Total eval steps: {}'.format(eval_steps))
                logger.info('Total test steps: {}'.format(test_steps))

                # computing positive label weights (for unbalanced dataset)
                network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                               mode=network_retrieved_args['is_multilabel']
                                               if 'is_multilabel' in network_retrieved_args else False)

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_begin'):
                        callback.on_build_model_begin(logs={'network': network})

                text_info = merge(tokenizer_info, converter_info)
                text_info = merge(text_info, training_config)
                network.build_model(text_info=text_info)

                # Custom callbacks only
                for callback in callbacks:
                    if hasattr(callback, 'on_build_model_end'):
                        callback.on_build_model_end(logs={'network': network})

                if use_tensorboard:
                    fold_log_dir = os.path.join(tensorboard_base_dir,
                                                'repetition_{}'.format(repetition),
                                                'fold_{}'.format(fold_idx))
                    os.makedirs(fold_log_dir)
                    tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                              log_dir=fold_log_dir)
                    fold_callbacks = callbacks + [tensorboard]
                else:
                    fold_callbacks = callbacks

                if preloaded:
                    network.predict(x=iter(val_data()), steps=1, suffix='val')

                    initial_weights = [layer.get_weights() for layer in network.model.layers]

                    network.load(
                        os.path.join(cd.CV_DIR, preloaded_model, '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                                                         repetition,
                                                                                                         fold_idx)))

                    # Correct loading check (inherently makes sure that restore ops are run)
                    for layer, initial in zip(network.model.layers, initial_weights):
                        weights = layer.get_weights()
                        if weights and all(tf.nest.map_structure(np.array_equal, weights, initial)):
                            logger.info('Checkpoint contained no weights for layer {}!'.format(layer.name))

                # Training
                network.distributed_fit(train_data=train_data,
                                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                                        callbacks=fold_callbacks, validation_data=val_data,
                                        strategy=strategy, step_checkpoint=training_config['step_checkpoint'],
                                        metrics=training_config['error_metrics'],
                                        additional_metrics_info=training_config['additional_metrics_info'],
                                        metrics_nicknames=training_config['metrics_nicknames'],
                                        train_num_batches=train_steps,
                                        eval_num_batches=eval_steps,
                                        np_val_y=np_val_y)

            # Inference
            val_predictions = network.predict(x=iter(val_data()),
                                              steps=eval_steps,
                                              callbacks=fold_callbacks,
                                              suffix='val')

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np_val_y,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info,
                                                                            error_metrics_nicknames=error_metrics_nicknames)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            logger.info('Iteration validation info: {}'.format(iteration_validation_error))

            test_predictions = network.predict(x=iter(test_data()),
                                               steps=test_steps,
                                               callbacks=callbacks,
                                               suffix='test')

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=np_test_y,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info,
                                                                      error_metrics_nicknames=error_metrics_nicknames)

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                logger.info('Iteration test info: {}'.format(iteration_test_error))

                if save_predictions:
                    prediction_info[fold_idx] = test_predictions.ravel()

            # Save model
            if save_model:
                filepath = os.path.join(test_path,
                                        '{0}_repetition_{1}_fold_{2}'.format(
                                            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                            repetition,
                                            fold_idx))
                network.save(filepath=filepath)

                filepath = os.path.join(test_path, 'y_test_fold_{}.json'.format(fold_idx))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        if save_predictions:
            for key, item in prediction_info.items():
                total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        if save_predictions:
            total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if save_predictions:
        result['predictions'] = total_preds

    if compute_test_info:
        result['test_info'] = total_test_info

    return result


def distributed_cross_validation_forward_pass(validation_percentage, data_handle, cv, strategy, data_loader_info,
                                              model_type, network_args, training_config, test_path,
                                              error_metrics, error_metrics_additional_info=None,
                                              error_metrics_nicknames=None,
                                              callbacks=None, compute_test_info=True, save_predictions=False,
                                              repetition_ids=None, retrieval_metric=None,
                                              split_key=None, distributed_info=None, top_K=None, current_K=None,
                                              load_externally=False, preloaded_model=None, preloaded=False):
    """
    Simple CV variant that retrieves a pre-trained model to do the forward pass for each val/test fold sets.
    """

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 1: cross validation

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    if preloaded and load_externally:
        tokenizer_class = TokenizerFactory.supported_tokenizers[tokenizer_type]
        tokenizer = tokenizer_class.from_pretrained(network_args['preloaded_name']['value'])
    else:
        tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
        tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)
    converter_instance_args = converter.get_instance_args()

    # Retrieve repetition ids if not given
    if repetition_ids is None:
        if retrieval_metric is None or retrieval_metric not in error_metrics_nicknames:
            raise AttributeError('Invalid retrieval metric! It is required in'
                                 ' order to determine best folds. Got: {}'.format(retrieval_metric))
        loaded_val_results = load_json(os.path.join(test_path, cd.JSON_VALIDATION_INFO_NAME))
        metric_val_results = loaded_val_results[retrieval_metric]
        metric_val_results = np.array(metric_val_results)
        if top_K is None:
            repetition_ids = np.argmax(metric_val_results, axis=0)
        else:
            if current_K != 0:
                metric_val_results = metric_val_results.transpose()
                repetition_ids = np.argsort(metric_val_results, axis=1)[:, ::-1]
                repetition_ids = repetition_ids[:, :top_K][:, current_K]
            else:
                # Ensure that greedy result equals top 1 result (avoid multiple matches)
                repetition_ids = np.argmax(metric_val_results, axis=0)

    validation_info = OrderedDict()
    test_info = OrderedDict()
    all_preds = OrderedDict()

    for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                          key_values=test_indexes,
                                                          val_indexes=val_indexes,
                                                          validation_percentage=validation_percentage)

        test_df.to_csv(os.path.join(test_path, 'test_df_fold_{}.csv'.format(fold_idx)), index=None)

        train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
        val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
        test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

        save_prefix = 'fold_{}'.format(fold_idx)

        if not os.path.isfile(test_filepath):
            logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            converter.convert_data(examples=val_data,
                                   label_list=processor.get_labels(),
                                   output_file=val_filepath,
                                   tokenizer=tokenizer)

            converter.convert_data(examples=test_data,
                                   label_list=processor.get_labels(),
                                   output_file=test_filepath,
                                   tokenizer=tokenizer)
        else:
            tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                             prefix=save_prefix)
            converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        logger.info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        with strategy.scope():
            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                            converter_instance_args),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                             converter_instance_args),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Build model

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition_ids[fold_idx], fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
            test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

            np_val_y = retrieve_numpy_labels(data_fn=val_data, steps=eval_steps)
            np_test_y = retrieve_numpy_labels(data_fn=test_data, steps=test_steps)

            logger.info('Total eval steps: {}'.format(eval_steps))
            logger.info('Total test steps: {}'.format(test_steps))

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            # Setup model by feeding an input
            network.predict(x=iter(val_data()), steps=1)

            if preloaded:
                initial_weights = [layer.get_weights() for layer in network.model.layers]

                network.load(
                    os.path.join(cd.CV_DIR, preloaded_model, '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                                                     repetition_ids[
                                                                                                         fold_idx],
                                                                                                     fold_idx)))

                # Correct loading check (inherently makes sure that restore ops are run)
                for layer, initial in zip(network.model.layers, initial_weights):
                    weights = layer.get_weights()
                    if weights and all(tf.nest.map_structure(np.array_equal, weights, initial)):
                        logger.info('Checkpoint contained no weights for layer {}!'.format(layer.name))

            # load pre-trained weights
            current_weight_filename = '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                              repetition_ids[fold_idx],
                                                                              fold_idx)
            network.load(os.path.join(test_path, current_weight_filename))

        # Inference
        val_predictions = network.predict(x=iter(val_data()),
                                          callbacks=callbacks,
                                          steps=eval_steps,
                                          suffix='val')

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np_val_y,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info,
                                                                        error_metrics_nicknames=error_metrics_nicknames)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions = network.predict(x=iter(test_data()),
                                           steps=test_steps,
                                           callbacks=callbacks,
                                           suffix='test')

        iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                  true_values=np_test_y,
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info,
                                                                  error_metrics_nicknames=error_metrics_nicknames)

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            if save_predictions:
                all_preds[fold_idx] = test_predictions

        # Flush
        K.clear_session()

    result = {
        'validation_info': validation_info,
    }

    if compute_test_info:
        result['test_info'] = test_info
        if save_predictions:
            result['predictions'] = all_preds
    else:
        if save_predictions:
            result['predictions'] = all_preds

    return result


def cross_validation(validation_percentage, data_handle, cv, data_loader_info,
                     model_type, network_args, training_config, test_path,
                     error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                     callbacks=None, compute_test_info=True, save_predictions=False,
                     use_tensorboard=False, repetitions=1, save_model=False,
                     split_key=None, checkpoint=None, distributed_info=None,
                     preloaded=False, preloaded_model=None, load_externally=False,
                     inference_repetitions=1, seeds=None):
    """
    [Repeated] Cross-validation routine:

        1. [For each repetition]

        2. For each fold:
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        logger.error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)
    converter_instance_args = converter.get_instance_args()

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    if seeds is not None:
        assert len(seeds) == repetitions
    else:
        seeds = np.random.randint(low=1, high=10000, size=repetitions)

    for repetition in range(repetitions):
        logger.info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        # Seeds
        current_seed = seeds[repetition]
        np.random.seed(current_seed)
        tf.random.set_seed(current_seed)
        logger.info('Random seed: {0}'.format(current_seed))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
            logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              val_indexes=val_indexes,
                                                              validation_percentage=validation_percentage)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                tokenizer.build_vocab(data=[train_data], filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            else:
                tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                 prefix=save_prefix)
                converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            logger.info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            train_data = get_dataset_fn(filepath=train_filepath,
                                        batch_size=training_config['batch_size'],
                                        name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                              converter_instance_args),
                                        selector=converter.feature_class.get_dataset_selector(),
                                        is_training=True,
                                        shuffle_amount=distributed_info['shuffle_amount'],
                                        reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                        prefetch_amount=distributed_info['prefetch_amount'])

            fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                              batch_size=training_config['batch_size'],
                                              name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                                    converter_instance_args),
                                              selector=converter.feature_class.get_dataset_selector(),
                                              is_training=False,
                                              prefetch_amount=distributed_info['prefetch_amount'])

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                            converter_instance_args),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                             converter_instance_args),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Build model

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            train_steps = int(np.ceil(data_handle.train_size / training_config['batch_size']))
            eval_steps = int(np.ceil(data_handle.val_size / training_config['batch_size']))
            test_steps = int(np.ceil(data_handle.test_size / training_config['batch_size']))

            np_train_y = retrieve_numpy_labels(data_fn=fixed_train_data, steps=train_steps)
            np_val_y = retrieve_numpy_labels(data_fn=val_data, steps=eval_steps)
            np_test_y = retrieve_numpy_labels(data_fn=test_data, steps=test_steps)

            logger.info('Total train steps: {}'.format(train_steps))
            logger.info('Total eval steps: {}'.format(eval_steps))
            logger.info('Total test steps: {}'.format(test_steps))

            # computing positive label weights (for unbalanced dataset)
            network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                           mode='multi-label'
                                           if 'is_multilabel' in network_retrieved_args and network_retrieved_args[
                                               'is_multilabel']
                                           else "multi-class")

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            if use_tensorboard:
                fold_log_dir = os.path.join(tensorboard_base_dir,
                                            'repetition_{}'.format(repetition),
                                            'fold_{}'.format(fold_idx))
                os.makedirs(fold_log_dir)
                tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                          log_dir=fold_log_dir,
                                          )
                fold_callbacks = callbacks + [tensorboard]
            else:
                fold_callbacks = callbacks

            if preloaded:
                network.predict(x=iter(val_data()), steps=1)

                initial_weights = [layer.get_weights() for layer in network.model.layers]

                network.load(
                    os.path.join(cd.CV_DIR, preloaded_model, '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                                                     repetition,
                                                                                                     fold_idx)))

                # Correct loading check (inherently makes sure that restore ops are run)
                for layer, initial in zip(network.model.layers, initial_weights):
                    weights = layer.get_weights()
                    if weights and all(tf.nest.map_structure(np.array_equal, weights, initial)):
                        logger.info('Checkpoint contained no weights for layer {}!'.format(layer.name))

            # Training
            network.fit(train_data=train_data,
                        fixed_train_data=fixed_train_data,
                        epochs=training_config['epochs'], verbose=training_config['verbose'],
                        callbacks=fold_callbacks, validation_data=val_data,
                        step_checkpoint=training_config['step_checkpoint'],
                        metrics=training_config['error_metrics'],
                        additional_metrics_info=training_config['error_metrics_additional_info'],
                        metrics_nicknames=training_config['error_metrics_nicknames'],
                        train_num_batches=train_steps,
                        eval_num_batches=eval_steps,
                        np_val_y=np_val_y,
                        np_train_y=np_train_y,
                        val_inference_repetitions=inference_repetitions)

            # Inference
            val_predictions, iteration_validation_error = network.average_predict(
                data=val_data,
                steps=eval_steps,
                callbacks=fold_callbacks,
                suffix='val',
                parsed_metrics=parsed_metrics,
                true_values=np_val_y,
                error_metrics_additional_info=error_metrics_additional_info,
                error_metrics_nicknames=error_metrics_nicknames,
                metric_function=compute_iteration_validation_error,
                repetitions=inference_repetitions
            )

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            logger.info('Iteration validation info: \n{}'.format(prettify_statistics(iteration_validation_error)))

            test_predictions, iteration_test_error = network.average_predict(
                data=test_data,
                steps=test_steps,
                callbacks=fold_callbacks,
                suffix='test',
                parsed_metrics=parsed_metrics,
                true_values=np_test_y,
                error_metrics_additional_info=error_metrics_additional_info,
                error_metrics_nicknames=error_metrics_nicknames,
                metric_function=compute_iteration_validation_error,
                repetitions=inference_repetitions
            )

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                logger.info('Iteration test info: \n{}'.format(prettify_statistics(iteration_test_error)))

                if save_predictions:
                    all_preds[fold_idx] = test_predictions

            # Save model
            if save_model:
                filepath = os.path.join(test_path,
                                        '{0}_repetition_{1}_fold_{2}'.format(
                                            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                            repetition,
                                            fold_idx))
                network.save(filepath=filepath)

                filepath = os.path.join(test_path, 'y_test_fold_{}.json'.format(fold_idx))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=np_test_y)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        if save_predictions:
            result['predictions'] = total_preds
    else:
        if save_predictions:
            result['predictions'] = total_preds

    return result


def cross_validation_forward_pass(validation_percentage, data_handle, cv, data_loader_info,
                                  model_type, network_args, training_config, test_path,
                                  error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                                  callbacks=None, compute_test_info=True, save_predictions=False,
                                  repetition_ids=None, retrieval_metric=None,
                                  split_key=None, distributed_info=None, top_K=None, current_K=None,
                                  load_externally=False, preloaded_model=None, preloaded=False,
                                  inference_repetitions=1):
    """
    Simple CV variant that retrieves a pre-trained model to do the forward pass for each val/test fold sets.
    """

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 1: cross validation

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    if preloaded and load_externally:
        tokenizer_class = TokenizerFactory.supported_tokenizers[tokenizer_type]
        tokenizer = tokenizer_class.from_pretrained(network_args['preloaded_name']['value'])
    else:
        tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
        tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)
    converter_instance_args = converter.get_instance_args()

    # Retrieve repetition ids if not given
    if repetition_ids is None:
        if retrieval_metric is None or retrieval_metric not in error_metrics_nicknames:
            raise AttributeError('Invalid retrieval metric! It is required in'
                                 ' order to determine best folds. Got: {}'.format(retrieval_metric))
        loaded_val_results = load_json(os.path.join(test_path, cd.JSON_VALIDATION_INFO_NAME))
        metric_val_results = loaded_val_results[retrieval_metric]
        metric_val_results = np.array(metric_val_results)
        if top_K is None:
            repetition_ids = np.argmax(metric_val_results, axis=0)
        else:
            if current_K != 0:
                metric_val_results = metric_val_results.transpose()
                repetition_ids = np.argsort(metric_val_results, axis=1)[:, ::-1]
                repetition_ids = repetition_ids[:, :top_K][:, current_K]
            else:
                # Ensure that greedy result equals top 1 result (avoid multiple matches)
                repetition_ids = np.argmax(metric_val_results, axis=0)

    validation_info = OrderedDict()
    test_info = OrderedDict()
    all_preds = OrderedDict()

    for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                          key_values=test_indexes,
                                                          val_indexes=val_indexes,
                                                          validation_percentage=validation_percentage)

        test_df.to_csv(os.path.join(test_path, 'test_df_fold_{}.csv'.format(fold_idx)), index=None)

        train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
        val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
        test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

        save_prefix = 'fold_{}'.format(fold_idx)

        if not os.path.isfile(test_filepath):
            logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
            val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
            test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

            # Tokenizer

            train_texts = train_data.get_data()
            tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            converter.convert_data(examples=val_data,
                                   label_list=processor.get_labels(),
                                   output_file=val_filepath,
                                   tokenizer=tokenizer)

            converter.convert_data(examples=test_data,
                                   label_list=processor.get_labels(),
                                   output_file=test_filepath,
                                   tokenizer=tokenizer)
        else:
            tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                             prefix=save_prefix)
            converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        tokenizer.show_info(tokenizer_info)
        logger.info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        val_data = get_dataset_fn(filepath=val_filepath,
                                  batch_size=training_config['batch_size'],
                                  name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                        converter_instance_args),
                                  selector=converter.feature_class.get_dataset_selector(),
                                  is_training=False,
                                  prefetch_amount=distributed_info['prefetch_amount'])

        test_data = get_dataset_fn(filepath=test_filepath,
                                   batch_size=training_config['batch_size'],
                                   name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                         converter_instance_args),
                                   selector=converter.feature_class.get_dataset_selector(),
                                   is_training=False,
                                   prefetch_amount=distributed_info['prefetch_amount'])

        # Build model

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition_ids[fold_idx], fold_idx)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        eval_steps = int(np.ceil(val_df.shape[0] / training_config['batch_size']))
        test_steps = int(np.ceil(test_df.shape[0] / training_config['batch_size']))

        np_val_y = np.concatenate([item for item in val_data().map(lambda x, y: y)])
        np_test_y = np.concatenate([item for item in test_data().map(lambda x, y: y)])

        logger.info('Total eval steps: {}'.format(eval_steps))
        logger.info('Total test steps: {}'.format(test_steps))

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        # Setup model by feeding an input
        network.predict(x=iter(val_data()), steps=1)

        if preloaded:
            initial_weights = [layer.get_weights() for layer in network.model.layers]

            network.load(
                os.path.join(cd.CV_DIR, preloaded_model, '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                                                 repetition_ids[
                                                                                                     fold_idx],
                                                                                                 fold_idx)))

            # Correct loading check (inherently makes sure that restore ops are run)
            for layer, initial in zip(network.model.layers, initial_weights):
                weights = layer.get_weights()
                if weights and all(tf.nest.map_structure(np.array_equal, weights, initial)):
                    logger.info('Checkpoint contained no weights for layer {}!'.format(layer.name))

        # load pre-trained weights
        current_weight_filename = '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                          repetition_ids[fold_idx],
                                                                          fold_idx)
        network.load(os.path.join(test_path, current_weight_filename))

        # Inference
        val_predictions, iteration_validation_error = network.average_predict(
            data=val_data,
            steps=eval_steps,
            callbacks=callbacks,
            suffix='val',
            parsed_metrics=parsed_metrics,
            true_values=np_val_y,
            error_metrics_additional_info=error_metrics_additional_info,
            error_metrics_nicknames=error_metrics_nicknames,
            metric_function=compute_iteration_validation_error,
            repetitions=inference_repetitions
        )

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions, iteration_test_error = network.average_predict(
            data=test_data,
            steps=test_steps,
            callbacks=callbacks,
            suffix='test',
            parsed_metrics=parsed_metrics,
            true_values=np_test_y,
            error_metrics_additional_info=error_metrics_additional_info,
            error_metrics_nicknames=error_metrics_nicknames,
            metric_function=compute_iteration_validation_error,
            repetitions=inference_repetitions
        )

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            if save_predictions:
                all_preds[fold_idx] = test_predictions.ravel()

        # Flush
        K.clear_session()

    result = {
        'validation_info': validation_info,
    }

    if compute_test_info:
        result['test_info'] = test_info
        if save_predictions:
            result['predictions'] = all_preds
    else:
        if save_predictions:
            result['predictions'] = all_preds

    return result


def cross_validation_complete_forward(validation_percentage, data_handle, cv, data_loader_info,
                                      model_type, network_args, training_config, test_path,
                                      error_metrics, error_metrics_additional_info=None, error_metrics_nicknames=None,
                                      callbacks=None, compute_test_info=True, save_predictions=False,
                                      repetitions=1, split_key=None, distributed_info=None,
                                      inference_repetitions=1, seeds=None):
    """
    Simple CV variant that retrieves a pre-trained model to do the forward pass for each val/test fold sets.
    """

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 1: cross validation

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)
    converter_instance_args = converter.get_instance_args()

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    if seeds is not None:
        assert len(seeds) == repetitions
    else:
        seeds = np.random.randint(low=1, high=10000, size=repetitions)

    for repetition in range(repetitions):
        logger.info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        # Seeds
        current_seed = seeds[repetition]
        np.random.seed(current_seed)
        tf.random.set_seed(current_seed)
        logger.info('Random seed: {0}'.format(current_seed))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        for fold_idx, (train_indexes, val_indexes, test_indexes) in enumerate(cv.split(None)):
            logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                              key_values=test_indexes,
                                                              val_indexes=val_indexes,
                                                              validation_percentage=validation_percentage)

            test_df.to_csv(os.path.join(test_path, 'test_df_fold_{}.csv'.format(fold_idx)), index=None)

            train_filepath = os.path.join(model_path, 'train_data_fold_{}'.format(fold_idx))
            val_filepath = os.path.join(model_path, 'val_data_fold_{}'.format(fold_idx))
            test_filepath = os.path.join(model_path, 'test_data_fold_{}'.format(fold_idx))

            save_prefix = 'fold_{}'.format(fold_idx)

            if not os.path.isfile(test_filepath):
                logger.info('Dataset not found! Building new one from scratch....it may require some minutes')

                # Processor

                train_data = processor.get_train_examples(data=train_df, ids=np.arange(train_df.shape[0]))
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(val_df.shape[0]))
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(test_df.shape[0]))

                # Tokenizer

                train_texts = train_data.get_data()
                tokenizer.build_vocab(data=train_texts, filepath=model_path, prefix=save_prefix)
                tokenizer.save_info(filepath=model_path, prefix=save_prefix)
                tokenizer_info = tokenizer.get_info()

                # Conversion

                # WARNING: suffers multi-threading (what if another processing is building the same data?)
                # This may happen only the first time an input pipeline is used. Usually calibration is on
                # model parameters
                converter.convert_data(examples=train_data,
                                       label_list=processor.get_labels(),
                                       output_file=train_filepath,
                                       tokenizer=tokenizer,
                                       is_training=True)
                converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
                converter_info = converter.get_conversion_args()

                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer)

                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer)
            else:
                tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                                 prefix=save_prefix)
                converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                    filepath=model_path,
                    prefix=save_prefix)

            # Debug
            tokenizer.show_info(tokenizer_info)
            logger.info('Converter info: \n{}'.format(converter_info))

            # Create Datasets

            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                            converter_instance_args),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])

            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info,
                                                                                             converter_instance_args),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])

            # Build model

            network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = data_handle.get_additional_info()
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # Useful stuff
            eval_steps = int(np.ceil(data_handle.val_size / training_config['batch_size']))
            test_steps = int(np.ceil(data_handle.test_size / training_config['batch_size']))

            np_val_y = retrieve_numpy_labels(data_fn=val_data, steps=eval_steps)
            np_test_y = retrieve_numpy_labels(data_fn=test_data, steps=test_steps)

            logger.info('Total eval steps: {}'.format(eval_steps))
            logger.info('Total test steps: {}'.format(test_steps))

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            text_info = merge(tokenizer_info, converter_info)
            text_info = merge(text_info, training_config)
            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            # Setup model by feeding an input
            network.predict(x=iter(val_data()), steps=1)

            # load pre-trained weights
            current_weight_filename = '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                              repetition,
                                                                              fold_idx)
            network.load(os.path.join(test_path, current_weight_filename))

            # Inference
            # Inference
            val_predictions, iteration_validation_error = network.average_predict(
                data=val_data,
                steps=eval_steps,
                callbacks=callbacks,
                suffix='val',
                parsed_metrics=parsed_metrics,
                true_values=np_val_y,
                error_metrics_additional_info=error_metrics_additional_info,
                error_metrics_nicknames=error_metrics_nicknames,
                metric_function=compute_iteration_validation_error,
                repetitions=inference_repetitions
            )
            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)
            logger.info('Iteration validation info: \n{}'.format(prettify_statistics(iteration_validation_error)))

            test_predictions, iteration_test_error = network.average_predict(
                data=test_data,
                steps=test_steps,
                callbacks=callbacks,
                suffix='test',
                parsed_metrics=parsed_metrics,
                true_values=np_test_y,
                error_metrics_additional_info=error_metrics_additional_info,
                error_metrics_nicknames=error_metrics_nicknames,
                metric_function=compute_iteration_validation_error,
                repetitions=inference_repetitions
            )
            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                logger.info('Iteration test info: \n{}'.format(prettify_statistics(iteration_test_error)))

                if save_predictions:
                    all_preds[fold_idx] = test_predictions

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        if save_predictions:
            result['predictions'] = total_preds
    else:
        if save_predictions:
            result['predictions'] = total_preds

    return result


def train_and_test(data_handle, test_path, data_loader_info,
                   callbacks, model_type, network_args, training_config,
                   error_metrics, error_metrics_additional_info=None,
                   error_metrics_nicknames=None, error_metrics_flags=None, compute_test_info=True,
                   save_model=False, validation_percentage=None, repetitions=1, seeds=None,
                   use_tensorboard=False, checkpoint=None, distributed_info=None,
                   inference_repetitions=1):
    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        logger.error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(test_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # Associates an ID to each combination for easy file naming while maintaining whole info
    config_args = {key: arg['value']
                   for key, arg in network_args.items()
                   if 'processor' in arg['flags']
                   or 'tokenizer' in arg['flags']
                   or 'converter' in arg['flags']
                   or 'data_loader' in arg['flags']}
    config_args = flatten(config_args)
    config_args = merge(config_args, data_loader_info)
    config_args_tuple = [(key, value) for key, value in config_args.items()]
    config_args_tuple = sorted(config_args_tuple, key=lambda item: item[0])

    config_name = '_'.join(['{0}-{1}'.format(name, value) for name, value in config_args_tuple])
    model_base_path = os.path.join(cd.TESTS_DATA_DIR,
                                   data_handle.data_name,
                                   model_type)
    config_id = get_data_config_id(filepath=model_base_path, config=config_name)
    model_path = os.path.join(model_base_path, str(config_id))

    if not os.path.isdir(model_path):
        os.makedirs(model_path)

    # Build pipeline: each step here is guaranteed to be idempotent (make sure of it!)

    # Build processor
    processor_type = cd.MODEL_CONFIG[model_type]['processor']
    processor_args = {key: arg['value'] for key, arg in network_args.items() if 'processor' in arg['flags']}
    processor_args['loader_info'] = data_handle.get_additional_info()
    processor = ProcessorFactory.factory(processor_type, **processor_args)

    # Build tokenizer
    tokenizer_type = cd.MODEL_CONFIG[model_type]['tokenizer']
    tokenizer_args = {key: arg['value'] for key, arg in network_args.items() if 'tokenizer' in arg['flags']}
    tokenizer = TokenizerFactory.factory(tokenizer_type, **tokenizer_args)

    # Build converter
    converter_type = cd.MODEL_CONFIG[model_type]['converter']
    converter_args = {key: arg['value'] for key, arg in network_args.items() if 'converter' in arg['flags']}
    converter = DataConverterFactory.factory(converter_type, **converter_args)

    # Step 1: Train and test

    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    if seeds is not None:
        assert len(seeds) == repetitions
    else:
        seeds = np.random.randint(low=1, high=10000, size=repetitions)

    for repetition in range(repetitions):
        logger.info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        # Seeds
        current_seed = seeds[repetition]
        np.random.seed(current_seed)
        tf.random.set_seed(current_seed)
        logger.info('Random seed: {0}'.format(current_seed))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        train_df, val_df, test_df = data_handle.get_data(validation_percentage)

        train_filepath = os.path.join(model_path, 'train_data')
        val_filepath = os.path.join(model_path, 'val_data')
        test_filepath = os.path.join(model_path, 'test_data')

        save_prefix = None

        if (not os.path.isfile(test_filepath) and test_df is not None) \
                or (test_df is None and not os.path.isfile(val_filepath)):
            logger.info(
                'Dataset not found! Building new one from scratch....it may require some minutes')

            # Processor

            train_data = processor.get_train_examples(data=train_df, ids=np.arange(len(train_df)))
            if val_df is not None:
                val_data = processor.get_dev_examples(data=val_df, ids=np.arange(len(val_df)))
            else:
                val_data = None
            if test_df is not None:
                test_data = processor.get_test_examples(data=test_df, ids=np.arange(len(test_df)))
            else:
                test_data = None
            processor_info = processor.save_info(filepath=model_path, prefix=save_prefix)

            # Tokenizer

            tokenizer.build_vocab(data=[train_data, val_data, test_data], filepath=model_path, prefix=save_prefix)
            tokenizer.save_info(filepath=model_path, prefix=save_prefix)
            tokenizer_info = tokenizer.get_info()

            # Conversion

            # WARNING: suffers multi-threading (what if another processing is building the same data?)
            # This may happen only the first time an input pipeline is used. Usually calibration is on
            # model parameters
            converter.convert_data(examples=train_data,
                                   label_list=processor.get_labels(),
                                   output_file=train_filepath,
                                   tokenizer=tokenizer,
                                   checkpoint=checkpoint,
                                   is_training=True)
            converter.save_conversion_args(filepath=model_path, prefix=save_prefix)
            converter_info = converter.get_conversion_args()

            if val_df is not None:
                converter.convert_data(examples=val_data,
                                       label_list=processor.get_labels(),
                                       output_file=val_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
            if test_df is not None:
                converter.convert_data(examples=test_data,
                                       label_list=processor.get_labels(),
                                       output_file=test_filepath,
                                       tokenizer=tokenizer,
                                       checkpoint=checkpoint)
        else:
            processor_info = ProcessorFactory.supported_processors[processor_type].load_info(filepath=model_path,
                                                                                             prefix=save_prefix)
            tokenizer_info = TokenizerFactory.supported_tokenizers[tokenizer_type].load_info(filepath=model_path,
                                                                                             prefix=save_prefix)
            converter_info = DataConverterFactory.supported_data_converters[converter_type].load_conversion_args(
                filepath=model_path,
                prefix=save_prefix)

        # Debug
        logger.info('Processor info: \n{}'.format(processor_info))
        tokenizer.show_info(tokenizer_info)
        logger.info('Converter info: \n{}'.format(converter_info))

        # Create Datasets

        train_data = get_dataset_fn(filepath=train_filepath,
                                    batch_size=training_config['batch_size'],
                                    name_to_features=converter.feature_class.get_mappings(converter_info),
                                    selector=converter.feature_class.get_dataset_selector(),
                                    is_training=True,
                                    shuffle_amount=distributed_info['shuffle_amount'],
                                    reshuffle_each_iteration=distributed_info['reshuffle_each_iteration'],
                                    prefetch_amount=distributed_info['prefetch_amount'])

        fixed_train_data = get_dataset_fn(filepath=train_filepath,
                                          batch_size=training_config['batch_size'],
                                          name_to_features=converter.feature_class.get_mappings(converter_info),
                                          selector=converter.feature_class.get_dataset_selector(),
                                          is_training=False,
                                          prefetch_amount=distributed_info['prefetch_amount'])

        if os.path.isfile(val_filepath):
            val_data = get_dataset_fn(filepath=val_filepath,
                                      batch_size=training_config['batch_size'],
                                      name_to_features=converter.feature_class.get_mappings(converter_info),
                                      selector=converter.feature_class.get_dataset_selector(),
                                      is_training=False,
                                      prefetch_amount=distributed_info['prefetch_amount'])
        else:
            val_data = None

        if os.path.isfile(test_filepath):
            test_data = get_dataset_fn(filepath=test_filepath,
                                       batch_size=training_config['batch_size'],
                                       name_to_features=converter.feature_class.get_mappings(converter_info),
                                       selector=converter.feature_class.get_dataset_selector(),
                                       is_training=False,
                                       prefetch_amount=distributed_info['prefetch_amount'])
        else:
            test_data = None

        # Building network

        network_retrieved_args = {key: deepcopy(value['value']) for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = data_handle.get_additional_info()
        network_retrieved_args['name'] = '{0}_repetition_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # Useful stuff
        train_steps = int(np.ceil(processor_info['train_samples'] / training_config['batch_size']))

        if val_df is not None:
            eval_steps = int(np.ceil(processor_info['val_samples'] / training_config['batch_size']))
        else:
            eval_steps = None

        if test_df is not None:
            test_steps = int(np.ceil(processor_info['test_samples'] / training_config['batch_size']))
        else:
            test_steps = None

        np_train_y = [item for item in fixed_train_data().map(lambda x, y: y).take(train_steps)]
        if type(np_train_y[0]) == dict:
            keys = list(np_train_y[0].keys())
            np_train_y = {key: np.concatenate([item[key] for item in np_train_y]) for key in keys}

        if val_df is not None:
            np_val_y = [item for item in val_data().map(lambda x, y: y)]
            if type(np_val_y[0]) == dict:
                keys = list(np_val_y[0].keys())
                np_val_y = {key: np.concatenate([item[key] for item in np_val_y]) for key in keys}
        else:
            np_val_y = None

        if test_df is not None:
            np_test_y = [item for item in test_data().map(lambda x, y: y)]
            if type(np_test_y[0]) == dict:
                keys = list(np_test_y[0].keys())
                np_test_y = {key: np.concatenate([item[key] for item in np_test_y]) for key in keys}
        else:
            np_test_y = None

        logger.info('Total train steps: {}'.format(train_steps))
        if val_data is not None:
            logger.info('Total eval steps: {}'.format(eval_steps))
        if test_data is not None:
            logger.info('Total test steps: {}'.format(test_steps))

        # computing positive label weights (for unbalanced dataset)
        if isinstance(network, ClassificationNetwork):
            network.compute_output_weights(y_train=np_train_y, num_classes=data_handle.num_classes,
                                           mode='multi-label' if network.is_multilabel else 'multi-class')

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        text_info = merge(tokenizer_info, converter_info)
        text_info = merge(text_info, training_config)
        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        if use_tensorboard:
            specific_log_dir = os.path.join(tensorboard_base_dir, 'repetition_{}'.format(repetition))
            os.makedirs(specific_log_dir)
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      log_dir=specific_log_dir)
            callbacks = callbacks + [tensorboard]

        # Training
        network.fit(train_data=train_data,
                    epochs=training_config['epochs'], verbose=training_config['verbose'],
                    callbacks=callbacks, validation_data=val_data,
                    step_checkpoint=training_config['step_checkpoint'],
                    metrics=training_config['error_metrics'],
                    additional_metrics_info=training_config['error_metrics_additional_info'],
                    metrics_nicknames=training_config['error_metrics_nicknames'],
                    error_metrics_flags=training_config['error_metrics_flags'],
                    train_num_batches=train_steps,
                    eval_num_batches=eval_steps,
                    np_val_y=np_val_y,
                    np_train_y=np_train_y,
                    val_inference_repetitions=inference_repetitions)

        # Inference
        if val_data is not None:
            val_predictions, iteration_validation_error = network.average_predict(
                data=val_data,
                steps=eval_steps,
                callbacks=callbacks,
                suffix='val',
                parsed_metrics=parsed_metrics,
                true_values=np_val_y,
                error_metrics_additional_info=error_metrics_additional_info,
                error_metrics_nicknames=error_metrics_nicknames,
                error_metrics_flags=error_metrics_flags,
                metric_function=compute_iteration_validation_error,
                repetitions=inference_repetitions
            )

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info and test_data is not None:
            test_predictions, \
            iteration_test_error = network.average_predict(data=test_data,
                                                           steps=test_steps,
                                                           callbacks=callbacks,
                                                           suffix='test',
                                                           parsed_metrics=parsed_metrics,
                                                           true_values=np_test_y,
                                                           error_metrics_additional_info=error_metrics_additional_info,
                                                           error_metrics_flags=error_metrics_flags,
                                                           error_metrics_nicknames=error_metrics_nicknames,
                                                           metric_function=compute_iteration_validation_error,
                                                           repetitions=inference_repetitions)
            all_preds[repetition] = test_predictions
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)

            logger.info('Iteration test info: {}'.format(iteration_test_error))

        # Save model
        if save_model:
            filepath = os.path.join(test_path, '{0}_repetition_{1}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                repetition))
            network.save(filepath=filepath)

            # Save ground truth
            filepath = os.path.join(test_path, 'y_test.json')
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=np_test_y)

        # Flush
        K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        result['predictions'] = total_preds
    else:
        result['predictions'] = total_preds

    return result
