import tensorflow as tf
import numpy as np


def decode_record(record, name_to_features):
    """
    TPU does not support int64
    """

    example = tf.io.parse_single_example(record, name_to_features)

    for name in list(example.keys()):
        t = example[name]
        if t.dtype == tf.int64:
            t = tf.cast(t, tf.int32)
        example[name] = t

    return example


def load_single_dataset(filepath, name_to_features):
    data = tf.data.TFRecordDataset(filepath)
    data = data.map(lambda record: decode_record(record, name_to_features))

    # Disabling auto-sharding -> each worker receives the full dataset
    # if isinstance(filepath, str) or len(filepath) == 1:
    # options = tf.data.Options()
    # options.experimental_distribute.auto_shard_policy = (
    #     tf.data.experimental.AutoShardPolicy.OFF)
    # data = data.with_options(options)
    return data


def create_dataset(filepath, batch_size, name_to_features, selector, is_training=True,
                   input_pipeline_context=None, shuffle_amount=10000, prefetch_amount=1024,
                   reshuffle_each_iteration=True, sampling=False, sampler=None):
    dataset = load_single_dataset(filepath=filepath, name_to_features=name_to_features)

    # Dataset is sharded by the number of hosts (num_input_pipelines == num_hosts)
    if input_pipeline_context and input_pipeline_context.num_input_pipelines > 1:
        dataset = dataset.shard(input_pipeline_context.num_input_pipelines,
                                input_pipeline_context.input_pipeline_id)

    dataset = dataset.map(selector, num_parallel_calls=tf.data.experimental.AUTOTUNE)

    # def data_generator():
    #     for item in dataset.take(-1):
    #         item = sampler.sampling(item)
    #         yield item
    #
    # dataset = None

    # if sampling:
    #     assert sampler is not None
    #     output_types = sampler.get_output_types(dataset.element_spec)
    #     output_shapes = sampler.get_output_shapes(dataset.element_spec)
    #
    #     dataset = tf.data.Dataset.from_generator(data_generator,
    #                                                    output_types=output_types,
    #                                                    output_shapes=output_shapes)

    if is_training:
        dataset = dataset.shuffle(buffer_size=shuffle_amount, reshuffle_each_iteration=reshuffle_each_iteration)
        dataset = dataset.repeat()
        if sampling:
            dataset = dataset.map(lambda x, y: sampler.sampling((x, y)), num_parallel_calls=tf.data.experimental.AUTOTUNE)

    dataset = dataset.batch(batch_size, drop_remainder=is_training)
    dataset = dataset.prefetch(prefetch_amount)
    return dataset


def get_dataset_fn(filepath, batch_size, name_to_features, selector, is_training=True,
                   shuffle_amount=10000, prefetch_amount=1024,
                   reshuffle_each_iteration=True,
                   sampling=False, sampler=None):
    """Gets a closure to create a dataset."""

    def _dataset_fn(ctx=None):
        """Returns tf.data.Dataset"""
        bs = ctx.get_per_replica_batch_size(batch_size) if ctx else batch_size
        dataset = create_dataset(filepath=filepath, batch_size=bs,
                                 name_to_features=name_to_features,
                                 selector=selector,
                                 is_training=is_training,
                                 input_pipeline_context=ctx,
                                 shuffle_amount=shuffle_amount,
                                 reshuffle_each_iteration=reshuffle_each_iteration,
                                 prefetch_amount=prefetch_amount,
                                 sampling=sampling,
                                 sampler=sampler)
        return dataset

    return _dataset_fn


def retrieve_numpy_labels(data_fn, steps):
    numpy_data = list(data_fn().map(lambda x, y: y).take(steps).as_numpy_iterator())
    if type(numpy_data[0]) == dict:
        numpy_data = {key: np.concatenate([item[key] for item in numpy_data]) for key in numpy_data[0].keys()}
    else:
        numpy_data = np.concatenate([item for item in data_fn().map(lambda x, y: y).take(steps)])

    return numpy_data