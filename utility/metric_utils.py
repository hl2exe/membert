import re
import string
from collections import Counter
import numpy as np
from sklearn.metrics import f1_score


def normalize_answer(s):
    """Convert to lowercase and remove punctuation, articles and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def get_tokens(s):
    if not s:
        return []
    return normalize_answer(s).split()


def compute_span_f1(y_true, y_pred):
    gold_toks = get_tokens(y_true)
    pred_toks = get_tokens(y_pred)
    common = Counter(gold_toks) & Counter(pred_toks)
    num_same = sum(common.values())
    if len(gold_toks) == 0 or len(pred_toks) == 0:
        # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
        return int(gold_toks == pred_toks)
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(pred_toks)
    recall = 1.0 * num_same / len(gold_toks)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


def compute_batch_span_f1(y_true, y_pred):
    y_true = [item.decode('utf-8').split('<DIV>') for item in y_true.tolist()]
    avg_f1 = []
    for y_true_candidates, y_pred_candidate in zip(y_true, y_pred):
        max_f1 = 0.
        for y_true_candidate in y_true_candidates:
            candidate_f1 = compute_span_f1(y_true=y_true_candidate,
                                           y_pred=y_pred_candidate)
            if candidate_f1 > max_f1:
                max_f1 = candidate_f1

        avg_f1.append(max_f1)

    return np.mean(avg_f1)


def compute_em(y_true, y_pred):
    return int(normalize_answer(y_true) == normalize_answer(y_pred))


def compute_batch_em(y_true, y_pred):
    y_true = [item.decode('utf-8').split('<DIV>') for item in y_true.tolist()]
    avg_em = []
    for y_true_candidates, y_pred_candidate in zip(y_true, y_pred):
        max_em = 0.
        for y_true_candidate in y_true_candidates:
            candidate_em = compute_em(y_true=y_true_candidate,
                                      y_pred=y_pred_candidate)
            if candidate_em > max_em:
                max_em = candidate_em

        avg_em.append(max_em)

    return np.mean(avg_em)


def compute_batch_answer_f1(y_true, y_pred, **kwargs):
    y_true = y_true.tolist()
    y_true = [item.decode('utf-8').split('<DIV>') for item in y_true]

    avg_f1 = []

    for y_true_candidates, y_pred_candidate in zip(y_true, y_pred):
        max_f1 = 0.
        for y_true_candidate in y_true_candidates:
            candidate_f1 = f1_score(y_true=y_true_candidate, y_pred=y_pred_candidate, **kwargs)
            if candidate_f1 > max_f1:
                max_f1 = candidate_f1

        avg_f1.append(max_f1)

    return np.mean(avg_f1)


def pairwise_f1_score(y_true, y_pred, **kwargs):
    # y_true -> numpy array of integer values
    # y_pred -> numpy array of [example_id, raw_values...]

    # Reduce predictions
    merged_predictions = np.split(y_pred[:, 1:],
                                  np.unique(y_pred[:, 0], return_index=True)[1][1:])

    # Since we have multiple unfair clauses with target and non-target explanations,
    # we consider the greediest prediction (at least one positive)
    # merged_predictions = [np.sum(split, axis=0) / np.sum(split) for split in merged_predictions]
    merged_predictions = [np.minimum(np.sum(np.argmax(split, axis=-1)), 1.) for split in merged_predictions]
    merged_predictions = np.array(merged_predictions)

    # merged_predictions = np.max(merged_predictions[:, 1], axis=-1)
    # merged_predictions = np.round(merged_predictions)
    # merged_predictions = np.argmax(merged_predictions, axis=1)

    merged_ground_truth = np.hstack((y_pred[:, :1], y_true[:, None]))
    merged_ground_truth = np.split(merged_ground_truth[:, 1:],
                                   np.unique(merged_ground_truth[:, 0], return_index=True)[1][1:])
    merged_ground_truth = [np.minimum(np.sum(split), 1.) for split in merged_ground_truth]
    # merged_ground_truth = y_true[np.unique(y_pred[:, 0], return_index=True)[1]]

    return f1_score(y_true=merged_ground_truth, y_pred=merged_predictions, **kwargs)
