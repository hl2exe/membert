"""

"""

from collections import OrderedDict

import pandas as pd
from tqdm import tqdm
from utility.json_utils import load_json

from sample_wrappers import TextExampleList, TextKBExampleList, TextExample, TextRationaleExample, \
    TextSupervisionExample, TextSquadExample, BertTextSquadExample, PairwiseTextExample
from utility import preprocessing_utils
import os
import numpy as np
from utility.log_utils import get_logger
from tokenization import SpacyTokenizer

logger = get_logger(__name__)


class DataProcessor(object):
    """Base class for data converters for sequence classification data sets."""

    def __init__(self, loader_info, filter_names=None, retrieve_label=True):
        self.loader_info = loader_info
        self.filter_names = filter_names if filter_names is not None else preprocessing_utils.filter_methods
        self.retrieve_label = retrieve_label

        self.data_info = {
            'train_samples': 0,
            'val_samples': 0,
            'test_samples': 0
        }

    def _retrieve_default_label(self, row):
        if self.retrieve_label:
            if type(self.loader_info['label']) == str:
                label = row[self.loader_info['label']]
            else:
                label = OrderedDict([(key, row[key]) for key in self.loader_info['label']])
        else:
            label = None

        return label

    def get_train_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the train set."""
        raise NotImplementedError()

    def get_dev_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the dev set."""
        raise NotImplementedError()

    def get_test_examples(self, filepath=None, ids=None, data=None):
        """Gets a collection of `Example`s for the test set."""
        raise NotImplementedError()

    def get_labels(self):
        """Gets the list of labels for this data set."""
        return self.loader_info['inference_labels'] if self.retrieve_label else None

    def get_processor_name(self):
        """Gets the string identifier of the processor."""
        return self.loader_info['data_name']

    @classmethod
    def read_csv(cls, input_file, quotechar=None):
        """Reads a tab separated value file."""
        df = pd.read_csv(input_file)
        return df

    def save_info(self, filepath, prefix=None):
        if prefix:
            filename = 'processor_info_{}.npy'.format(prefix)
        else:
            filename = 'processor_info.npy'
        filepath = os.path.join(filepath, filename)
        np.save(filepath, self.data_info)

        return self.data_info

    @staticmethod
    def load_info(filepath, prefix=None):
        if prefix:
            filename = 'processor_info_{}.npy'.format(prefix)
        else:
            filename = 'processor_info.npy'
        filepath = os.path.join(filepath, filename)
        return np.load(filepath, allow_pickle=True).item()


class TextProcessor(DataProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = preprocessing_utils.filter_line(row[text_data_key], function_names=self.filter_names)
            label = self._retrieve_default_label(row)
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples

    def get_train_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='train')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='train')

    def get_dev_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='dev')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='dev')

    def get_test_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='test')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='test')


class PairwiseTextProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            text_data_key = self.loader_info['data_keys']['text']
            text = preprocessing_utils.filter_line(row[text_data_key], function_names=self.filter_names)
            explanation_key = self.loader_info['data_keys']['explanation']
            explanation = row[explanation_key]
            if type(explanation) == str:
                explanation = preprocessing_utils.filter_line(explanation, function_names=self.filter_names)
            else:
                explanation = None

            label = self._retrieve_default_label(row)
            example = PairwiseTextExample(example_id=row['example_ID'], text=text, explanation=explanation, label=label)
            examples.append(example)

        return examples


class BertTextProcessor(DataProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()
        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = row[text_data_key]
            label = self._retrieve_default_label(row)
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples

    def get_train_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='train')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='train')

    def get_dev_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='dev')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='dev')

    def get_test_examples(self, filepath=None, ids=None, data=None):

        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if not isinstance(data, pd.DataFrame):
            raise AttributeError('Data must be a pandas.DataFrame')

        if filepath is not None:
            df = self.read_csv(filepath)
            return self._get_examples_from_df(df, suffix='test')
        else:
            if ids is not None:
                data = data.iloc[ids]

            return self._get_examples_from_df(data, suffix='test')


class TextKBProcessor(TextProcessor):

    def __init__(self, **kwargs):
        super(TextKBProcessor, self).__init__(**kwargs)

        kb = self.loader_info['kb']
        self.kb = self._preprocess_kb(kb)

    def _preprocess_kb(self, kb):
        kb = OrderedDict([(key, list(
            map(lambda item: preprocessing_utils.filter_line(item, function_names=self.filter_names), value)))
                          for key, value in kb.items()])
        return kb

    def _get_examples_from_df(self, df, suffix):
        examples = TextKBExampleList()

        examples.add_state(property_name='kb',
                           property_value=self.kb)

        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = preprocessing_utils.filter_line(row[text_data_key], function_names=self.filter_names)
            label = self._retrieve_default_label(row)
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples


class BertTextKBProcessor(BertTextProcessor):

    def __init__(self, **kwargs):
        super(BertTextKBProcessor, self).__init__(**kwargs)

        kb = self.loader_info['kb']
        self.kb = self._preprocess_kb(kb)

    def _preprocess_kb(self, kb):
        kb = OrderedDict([(key, list(
            map(lambda item: preprocessing_utils.filter_line(item, function_names=self.filter_names), value)))
                          for key, value in kb.items()])
        return kb

    def _get_examples_from_df(self, df, suffix):
        examples = TextKBExampleList()

        examples.add_state(property_name='kb',
                           property_value=self.kb)

        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = row[text_data_key]
            label = self._retrieve_default_label(row)
            example = TextExample(guid=guid, text=text, label=label)
            examples.append(example)

        return examples


class TextKBSupervisionProcessor(TextKBProcessor):

    def _retrieve_default_targets(self, row):
        targets = OrderedDict()
        for key in self.kb:
            targets[key] = row['{}_targets'.format(key)]

        return targets

    def _get_examples_from_df(self, df, suffix):
        examples = TextKBExampleList()

        examples.add_state(property_name='kb',
                           property_value=self.kb)

        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = preprocessing_utils.filter_line(row[text_data_key], function_names=self.filter_names)
            label = self._retrieve_default_label(row)

            targets = self._retrieve_default_targets(row)

            example = TextSupervisionExample(guid=guid, text=text, label=label, targets=targets)
            examples.append(example)

        return examples


class BertTextKBSupervisionProcessor(BertTextKBProcessor):

    def _retrieve_default_targets(self, row):
        targets = OrderedDict()
        for key in self.kb:
            targets[key] = row['{}_targets'.format(key)]

        return targets

    def _get_examples_from_df(self, df, suffix):
        examples = TextKBExampleList()

        examples.add_state(property_name='kb',
                           property_value=self.kb)

        for row_id, row in tqdm(df.iterrows()):
            guid = '{0}-{1}'.format(suffix, row_id)
            text_data_key = self.loader_info['data_keys']['text']
            text = row[text_data_key]
            label = self._retrieve_default_label(row)

            targets = self._retrieve_default_targets(row)

            example = TextSupervisionExample(guid=guid, text=text, label=label, targets=targets)
            examples.append(example)

        return examples


class RationaleCNNProcessor(TextProcessor):

    def _get_examples_from_df(self, df, suffix):
        examples = TextExampleList()

        grouped_df = df.groupby('clause_id')

        for group_id, group in tqdm(grouped_df.__iter__()):
            guid = '{0}--{1}'.format(suffix, group_id)
            doc = group['sentence'].values
            doc = list(map(lambda item: preprocessing_utils.filter_line(item, function_names=self.filter_names), doc))

            assert len(doc) == self.loader_info['kb_size'] + 1

            if self.retrieve_label:
                label = group['clause_lbl'].values[0]
            else:
                label = None

            sentence_targets = group['sentence_lbl'].values

            example = TextRationaleExample(guid=guid, doc=doc, doc_label=label, sentence_targets=sentence_targets)
            examples.append(example)

        return examples


class SquadTextProcessor(DataProcessor):

    def __init__(self, tokenizer_name, tokenizer_args, **kwargs):
        super(SquadTextProcessor, self).__init__(**kwargs)
        self.tokenizer_name = tokenizer_name
        self.tokenizer_args = tokenizer_args

    # NOTE: this is not a dataframe but a json file
    def _get_examples(self, data, suffix):
        examples = TextExampleList()
        tokenizer = preprocessing_utils.get_squad_tokenizer(name=self.tokenizer_name)
        tokenizer = tokenizer(tokenizer_args=self.tokenizer_args)

        debug_errors = 0
        impossible_questions_debug = 0

        for document in tqdm(data):
            for paragraph in document['paragraphs']:
                context = paragraph['context']
                context = preprocessing_utils.clean_squad_text(context)
                spans, context_tokens = preprocessing_utils.squad_get_token_indexes(text=context,
                                                                                    tokenizer=tokenizer)
                for qa_pair in paragraph['qas']:
                    question = qa_pair['question']
                    question = preprocessing_utils.clean_squad_text(question)
                    question_tokens = tokenizer.tokenize(question)
                    answers = qa_pair['answers']

                    # Concatenate as strings to avoid padding
                    answer_texts = ""
                    answer_starts = ""
                    answer_ends = ""

                    for answer in answers:
                        answer_start = answer['answer_start']
                        answer_text = answer['text']
                        answer_end = answer_start + len(answer_text)
                        answer_span = []

                        # Re-map start and end indexes
                        for idx, span in enumerate(spans):
                            if span[0] <= answer_start <= span[1]:
                                answer_span.append(idx)
                            if span[0] <= answer_end <= span[1]:
                                answer_span.append(idx)

                        assert len(answer_span)
                        span_answer = ' '.join(context_tokens[answer_span[0]:answer_span[-1] + 1])

                        if preprocessing_utils.to_compact(span_answer.lower().strip()) != \
                                preprocessing_utils.to_compact(answer_text.lower().strip()):
                            debug_errors += 1

                        if not len(answer_texts):
                            answer_texts = span_answer
                        else:
                            answer_texts += '<DIV>' + span_answer

                        # Add +1 to account for NULL token at start (used for unanswerable questions)
                        if not len(answer_starts):
                            answer_starts = '{}'.format(answer_span[0] + 1)
                        else:
                            answer_starts += '<DIV>' + str(answer_span[0] + 1)

                        if not len(answer_ends):
                            answer_ends = '{}'.format(answer_span[-1] + 1)
                        else:
                            answer_ends += '<DIV>' + str(answer_span[-1] + 1)

                    if 'is_impossible' in qa_pair and qa_pair['is_impossible']:
                        answer_starts = '{}'.format(0)
                        answer_ends = '{}'.format(0)
                        answer_texts = tokenizer.null_token
                        impossible_questions_debug += 1

                    example = TextSquadExample(context_tokens=context_tokens,
                                               question_tokens=question_tokens,
                                               answer_starts=answer_starts,
                                               answer_ends=answer_ends,
                                               answer_texts=answer_texts)
                    examples.append(example)

        logger.info(f'[{suffix}] Found {debug_errors} potential errors!')
        logger.info(f'[{suffix}] Found {impossible_questions_debug} unanswerable questions!')
        self.data_info['{}_samples'.format(suffix)] = len(examples)
        return examples

    def get_train_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='train')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='train')

    def get_dev_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='val')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='val')

    def get_test_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='test')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='test')

    def get_labels(self):
        return None


class BertSquadTextProcessor(DataProcessor):

    def __init__(self, tokenizer_name, preloaded_name, **kwargs):
        super(BertSquadTextProcessor, self).__init__(**kwargs)
        self.preloaded_name = preloaded_name
        self.tokenizer_name = tokenizer_name

    def _get_examples(self, data, suffix):
        examples = TextExampleList()
        tokenizer = preprocessing_utils.get_squad_tokenizer(name=self.tokenizer_name)
        tokenizer = tokenizer.from_pretrained(self.preloaded_name)
        support_tokenizer = SpacyTokenizer()

        debug_errors = 0
        impossible_questions_debug = 0

        for document in tqdm(data):
            for paragraph in document['paragraphs']:
                context = paragraph['context']
                context = preprocessing_utils.clean_squad_text(context)
                spans_ids, spans, char_doc_tokens_map, doc_tokens = preprocessing_utils.squad_bert_get_token_indexes(
                    text=context,
                    tokenizer=tokenizer,
                    support_tokenizer=support_tokenizer)
                bert_tokens = [item for seq in spans for item in seq]
                bert_tokens_ids = [tokenizer.cls_token_id] + [item for seq in spans_ids for item in seq] + [
                    tokenizer.sep_token_id]

                for qa_pair in paragraph['qas']:
                    question = qa_pair['question']
                    question = preprocessing_utils.clean_squad_text(question)
                    question_inputs = tokenizer(question)
                    question_ids = question_inputs['input_ids']
                    question_attention_mask = question_inputs['attention_mask']
                    answers = qa_pair['answers']

                    # Concatenate as strings to avoid padding
                    answer_texts = ""
                    answer_starts = ""
                    answer_ends = ""

                    for answer in answers:
                        answer_start = answer['answer_start']
                        answer_text = answer['text']
                        answer_end = answer_start + len(answer_text) - 1

                        # 1. Convert char index to token index
                        doc_answer_start = char_doc_tokens_map[answer_start]
                        doc_answer_end = char_doc_tokens_map[answer_end]

                        # Sanity check
                        doc_answer_text = ' '.join(doc_tokens[doc_answer_start:doc_answer_end + 1]).lower()

                        # 2. Remap token index to bert token index
                        doc_to_bert_map = [0]
                        current = 0
                        for idx, bert_seq in enumerate(spans[1:]):
                            current += len(spans[idx])
                            doc_to_bert_map.append(current)

                        # Re-map start and end indexes
                        answer_start = doc_to_bert_map[doc_answer_start]
                        answer_end = doc_to_bert_map[doc_answer_end] + len(spans[doc_answer_end]) - 1

                        # Sanity check
                        bert_answer_text = tokenizer.convert_tokens_to_string(bert_tokens[answer_start:answer_end + 1])
                        if not preprocessing_utils.to_compact(doc_answer_text.lower().strip()) \
                               == preprocessing_utils.to_compact(bert_answer_text.lower().strip()) \
                               == preprocessing_utils.to_compact(answer_text.lower().strip()):
                            debug_errors += 1

                        # Add +1 to consider [CLS] token
                        answer_start += 1
                        answer_end += 1

                        if not len(answer_texts):
                            answer_texts = bert_answer_text
                        else:
                            answer_texts += '<DIV>' + bert_answer_text

                        if not len(answer_starts):
                            answer_starts = '{}'.format(answer_start)
                        else:
                            answer_starts += '<DIV>' + str(answer_start)

                        if not len(answer_ends):
                            answer_ends = '{}'.format(answer_end)
                        else:
                            answer_ends += '<DIV>' + str(answer_end)

                    if 'is_impossible' in qa_pair and qa_pair['is_impossible']:
                        answer_starts = '{}'.format(0)
                        answer_ends = '{}'.format(0)
                        answer_texts = tokenizer.cls_token
                        impossible_questions_debug += 1

                    example = BertTextSquadExample(context_ids=bert_tokens_ids,
                                                   context_attention_mask=[1] * len(bert_tokens_ids),
                                                   question_ids=question_ids,
                                                   question_attention_mask=question_attention_mask,
                                                   answer_starts=answer_starts,
                                                   answer_ends=answer_ends,
                                                   answer_texts=answer_texts)
                    examples.append(example)

        logger.info(f'[{suffix}] Found {debug_errors} potential errors!')
        logger.info(f'[{suffix}] Found {impossible_questions_debug} unanswerable questions!')
        self.data_info['{}_samples'.format(suffix)] = len(examples)

        return examples

    def get_train_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='train')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='train')

    def get_dev_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='val')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='val')

    def get_test_examples(self, filepath=None, ids=None, data=None):
        if filepath is None and data is None:
            raise AttributeError('Either filepath or data must be not None')

        if filepath is not None:
            data = load_json(filepath)['data']
            return self._get_examples(data, suffix='test')
        else:
            if ids is not None:
                data = [item for idx, item in enumerate(data) if idx in ids]

            return self._get_examples(data, suffix='test')

    def get_labels(self):
        return None


class ProcessorFactory(object):
    supported_processors = {
        'text_processor': TextProcessor,
        'pairwise_text_processor': PairwiseTextProcessor,
        'text_kb_processor': TextKBProcessor,
        'text_kb_supervision_processor': TextKBSupervisionProcessor,

        'bert_text_processor': BertTextProcessor,
        'bert_text_kb_processor': BertTextKBProcessor,
        'bert_text_kb_supervision_processor': BertTextKBSupervisionProcessor,

        'rationale_cnn_processor': RationaleCNNProcessor,

        'squad_text_processor': SquadTextProcessor,
        'bert_squad_text_processor': BertSquadTextProcessor
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ProcessorFactory.supported_processors[key]:
            return ProcessorFactory.supported_processors[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
