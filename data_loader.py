"""

@Author: Federico Ruggeri

@Date: 22/07/2019

"""
import ast
import os
from collections import OrderedDict

import numpy as np
import pandas as pd

import const_define as cd
from utility.json_utils import load_json


def load_file_data(file_path):
    """
    Reads a file line by line.

    :param file_path: path to the file
    :return: list of sentences (string)
    """

    sentences = []

    with open(file_path, 'r') as f:
        for line in f:
            sentences.append(line)

    return sentences


def load_dataset(df_path):
    """
    Loads the ToS dataset given the DataFrame path.

    :param df_path: path to the .csv file
    :return pandas.DataFrame
    """

    df = pd.read_csv(df_path)
    return df


class Task1Loader(object):
    """
    Basic DataFrame-compliant loader for ToS Task 1: unfair clauses classification
    """

    def load(self, category=None, sub_suffix=None):
        df_name = 'dataset'
        if sub_suffix is not None:
            df_name += '_' + sub_suffix
        df_name += '.csv'

        df_path = os.path.join(cd.TOS_100_DIR, df_name)
        df = load_dataset(df_path=df_path)
        df['category'] = [category] * df.shape[0]

        return DataHandle(data=df, num_classes=2, label=category,
                          data_name='tos_{}_task1'.format('100' if sub_suffix is None else sub_suffix),
                          inference_labels=[0, 1], data_keys={'text': 'text'}, )


class Task1KBPairsLoader(object):

    def load(self, category, sub_suffix=None):
        assert category is not None

        df_name = f'pairwise_dataset_{category}.csv' if sub_suffix is None \
            else f'pairwise_dataset_{category}_{sub_suffix}.csv'
        data_name = f'tos_pairwise_{category}_{sub_suffix}' if sub_suffix is not None else f'tos_pairwise_{category}'

        df_path = os.path.join(cd.TOS_100_DIR, df_name)
        df = load_dataset(df_path=df_path)
        df['category'] = [category] * df.shape[0]

        return DataHandle(data=df, num_classes=2, label=category,
                          data_name=data_name,
                          inference_labels=[0, 1], data_keys={'text': 'text', 'explanation': 'explanation'}, )


class MultiTask1Loader(object):

    def load(self, categories=["A", "CH", "CR", "LTD", "TER"], sub_suffix=None):
        df_name = 'dataset'
        if sub_suffix is not None:
            df_name += '_' + sub_suffix
        df_name += '.csv'

        df_path = os.path.join(cd.TOS_100_DIR, df_name)
        df = load_dataset(df_path=df_path)

        num_classes = OrderedDict([(key, 2) for key in categories])
        inference_labels = OrderedDict([(key, [0, 1]) for key in categories])

        return DataHandle(data=df, num_classes=num_classes, category=categories,
                          data_name='tos_{}_multi_task1'.format('100' if sub_suffix is None else sub_suffix),
                          inference_labels=inference_labels)


class Task1KBLoader(object):
    """
    ToS Task 1 loader is extended by loading an external KB.
    This loader is used by Memory Networks.
    """

    def load(self, category=None, sub_suffix=None):

        df_name = 'dataset'
        if sub_suffix is not None:
            df_name += '_' + sub_suffix
        df_name += '.csv'

        df_path = os.path.join(cd.TOS_100_DIR, df_name)
        df = load_dataset(df_path=df_path)
        df['category'] = [category] * df.shape[0]

        if category:
            kb_name = '{}_KB.txt'.format(category)
        else:
            kb_name = 'KB.txt'

        kb_path = os.path.join(cd.KB_DIR, kb_name)
        kb_data = {category: load_file_data(kb_path)}

        return KBDataHandle(data=df,
                            data_name='tos_{}_task1kb_simple'.format('100' if sub_suffix is None else sub_suffix),
                            num_classes=2,
                            label=category,
                            data_keys={'text': 'text'},
                            kb_data=kb_data,
                            inference_labels=[0, 1])


class MultiTask1KBLoader(object):

    def load(self, categories=["A", "CH", "CR", "LTD", "TER"], sub_suffix=None):

        df_name = 'dataset'
        if sub_suffix is not None:
            df_name += '_' + sub_suffix
        df_name += '.csv'

        df_path = os.path.join(cd.TOS_100_DIR, df_name)
        df = load_dataset(df_path=df_path)

        num_classes = OrderedDict([(key, 2) for key in categories])
        inference_labels = OrderedDict([(key, [0, 1]) for key in categories])

        if len(categories) == 0:
            raise RuntimeError("Expected at least a category")

        kb_data = {}
        for cat in categories:
            cat_kb_name = '{}_KB.txt'.format(cat)
            cat_kb_path = os.path.join(cd.KB_DIR, cat_kb_name)
            kb_data[cat] = load_file_data(cat_kb_path)

        return KBDataHandle(data=df,
                            data_name='tos_{}_multi_task1kb'.format('100' if sub_suffix is None else sub_suffix),
                            num_classes=num_classes,
                            label=categories,
                            kb_data=kb_data,
                            data_keys={'text': 'text'},
                            to_upper=False,
                            inference_labels=inference_labels)


class Task1RationaleLoader(object):

    def load(self, dataset, category=None):
        df_path = os.path.join(cd.DATASET_PATHS[dataset], 'rationale_dataset_{}.csv'.format(category))
        df = load_dataset(df_path=df_path)
        df['category'] = [category] * df.shape[0]

        kb_df = pd.read_csv(os.path.join(cd.KB_DIR, '{}_explanations.csv'.format(category.upper())))
        kb_size = kb_df.shape[0]

        return RationaleDataHandle(data=df, data_name='{}_rationale_task1'.format(dataset), num_classes=2,
                                   category=category,
                                   inference_labels=[0, 1], kb_size=kb_size)


class SquadLoader(object):

    def load(self):
        train_data = load_json(os.path.join(cd.LOCAL_DATASETS_DIR, 'squad1.1', 'train-v1.1.json'))['data']
        val_data = load_json(os.path.join(cd.LOCAL_DATASETS_DIR, 'squad1.1', 'dev-v1.1.json'))['data']

        return GeneralTrainAndTestDataHandle(train_data=train_data,
                                             val_data=val_data,
                                             test_data=None,
                                             data_name='squad1.0')


class Squad2Loader(object):

    def load(self):
        train_data = load_json(os.path.join(cd.LOCAL_DATASETS_DIR, 'squad2.0', 'train-v2.0.json'))['data']
        val_data = load_json(os.path.join(cd.LOCAL_DATASETS_DIR, 'squad2.0', 'dev-v2.0.json'))['data']

        return GeneralTrainAndTestDataHandle(train_data=train_data,
                                             val_data=val_data,
                                             test_data=None,
                                             data_name='squad2.0')


class IBM2015Loader(object):

    def load(self, topic_subset):
        df_path = os.path.join(cd.IBM2015_DIR, 'selected_subdataset_{}.csv'.format(topic_subset))
        df = load_dataset(df_path=df_path)

        kb_path = os.path.join(cd.IBM2015_DIR, 'ibm2015_memory_{}.txt'.format(topic_subset))
        kb_data = {'evidence': load_file_data(kb_path)}

        return KBDataHandle(data=df,
                            data_name='ibm2015_{}'.format(topic_subset),
                            num_classes=2,
                            label='C_Label',
                            kb_data=kb_data,
                            to_upper=False,
                            data_keys={'text': 'Sentence'},
                            inference_labels=[0, 1])


class DataLoaderFactory(object):
    supported_data_loaders = {
        'task1': Task1Loader,
        'task1_kb': Task1KBLoader,
        'task1_rationale': Task1RationaleLoader,
        'pairwise_task1': Task1KBPairsLoader,

        'task1_kb_simple': Task1KBLoader,

        'multi_task1': MultiTask1Loader,
        'multi_task1_kb': MultiTask1KBLoader,

        'ibm2015': IBM2015Loader,

        'squad': SquadLoader,
        'squad2': Squad2Loader
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataLoaderFactory.supported_data_loaders[key]:
            return DataLoaderFactory.supported_data_loaders[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))


#### Data Handles ####

class DataHandle(object):
    """
    General dataset wrapper. Additional behaviour can be expressed via attributes.
    """

    def __init__(self, data, data_name, num_classes, inference_labels,
                 data_keys, label=None, to_upper=False, has_fixed_test=False, test_data=None,
                 has_fixed_validation=False, val_data=None):
        self.data = data
        self.data_name = data_name
        self.num_classes = num_classes
        self.to_upper = to_upper
        self.inference_labels = inference_labels
        self.has_fixed_test = has_fixed_test
        self.test_data = test_data
        self.has_fixed_validation = has_fixed_validation
        self.val_data = val_data

        if label:
            category = label.upper() if to_upper else label
            self.label = category
        else:
            self.label = 'label'

        self.data_keys = data_keys

    def get_split(self, key_values, key=None, val_indexes=None, validation_percentage=None):
        if key is None:
            if self.data.index.name is None:
                self.data['index'] = np.arange(self.data.shape[0])
                self.data.set_index('index')
                key = 'index'
            else:
                key = self.data.index.name

            key_values = key_values.astype(self.data[key].values.dtype)

            if val_indexes is not None:
                val_indexes = val_indexes.astype(key_values.dtype)

        train_df = self.data[np.logical_not(self.data[key].isin(key_values))]

        if self.has_fixed_test:
            # self.data should not contain test_data
            test_df = self.test_data
            val_df = self.data[self.data[key].isin(key_values)]
        else:
            test_df = self.data[self.data[key].isin(key_values)]

            if self.has_fixed_validation:
                val_df = self.val_data
            else:
                if val_indexes is None:
                    assert validation_percentage is not None

                    validation_amount = int(len(train_df) * validation_percentage)
                    train_amount = len(train_df) - validation_amount
                    val_df = train_df[train_amount:]
                    train_df = train_df[:train_amount]
                else:
                    val_df = train_df[train_df[key].isin(val_indexes)]
                    train_df = train_df[np.logical_not(train_df[key].isin(val_indexes))]

        self.train_size = train_df.shape[0]
        self.val_size = val_df.shape[0]
        self.test_size = test_df.shape[0]

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {
            'label': self.label,
            'inference_labels': self.inference_labels,
            'data_keys': self.data_keys
        }


@DeprecationWarning
class RationaleDataHandle(DataHandle):

    def __init__(self, kb_size, **kwargs):
        super(RationaleDataHandle, self).__init__(**kwargs)
        self.kb_size = kb_size

    def get_split(self, key_values, key=None, validation_percentage=None):
        if key is None:
            key = 'Unnamed: 0' if self.data.index.name is None else self.data.index.name

        train_df = self.data[np.logical_not(self.data[key].isin(key_values))]
        test_df = self.data[self.data[key].isin(key_values)]

        if validation_percentage is not None:
            validation_amount = int(len(train_df) * validation_percentage)
            validation_amount = (validation_amount // (self.kb_size + 1)) * (self.kb_size + 1)
            train_amount = len(train_df) - validation_amount
            val_df = train_df[train_amount:]
            train_df = train_df[:train_amount]
        else:
            raise NotImplementedError('No validation data right now! ;)')

        self.train_size = train_df.shape[0] // (self.kb_size + 1)
        self.val_size = val_df.shape[0] // (self.kb_size + 1)
        self.test_size = test_df.shape[0] // (self.kb_size + 1)

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {
            'label': self.label,
            'inference_labels': self.inference_labels,
            'data_keys': self.data_keys,
            'kb_size': self.kb_size
        }


class TrainAndTestDataHandle(object):

    def __init__(self, train_data, test_data, data_name, num_classes, inference_labels,
                 label, data_keys, validation_data=None, build_validation=True, pairwise_labels=None):
        self.train_data = train_data
        self.test_data = test_data
        self.data_name = data_name
        self.num_classes = num_classes
        self.inference_labels = inference_labels
        self.label = label
        self.validation_data = validation_data
        self.data_keys = data_keys
        self.pairwise_labels = pairwise_labels
        self.build_validation = build_validation

    def get_data(self, validation_percentage=None):

        train_df = self.train_data
        test_df = self.test_data

        if self.build_validation:
            if validation_percentage is None:
                val_df = self.validation_data
            else:
                validation_amount = int(len(train_df) * validation_percentage)
                train_amount = len(train_df) - validation_amount
                val_df = train_df[train_amount:]
                train_df = train_df[:train_amount]
        else:
            val_df = None

        return train_df, val_df, test_df

    def get_additional_info(self):
        return {'label': self.label,
                'inference_labels': self.inference_labels,
                'pairwise_labels': self.pairwise_labels,
                'data_keys': self.data_keys
                }


class GeneralTrainAndTestDataHandle(object):

    def __init__(self, train_data, data_name, val_data=None, test_data=None):
        self.train_data = train_data
        self.val_data = val_data
        self.test_data = test_data
        self.data_name = data_name

    def get_data(self, validation_percentage=None):
        if self.val_data is not None:
            val_data = self.val_data
            train_data = self.train_data
        else:
            raise NotImplementedError('TODO')

        return train_data, val_data, self.test_data

    def get_additional_info(self):
        return {}


class KBDataHandle(DataHandle):
    """
    DataHandle wrapper that stores a KB as additional input.
    """

    def __init__(self, kb_data, **kwargs):
        super(KBDataHandle, self).__init__(**kwargs)
        self.kb_data = kb_data

    def get_additional_info(self):
        info = super(KBDataHandle, self).get_additional_info()
        info['kb'] = self.kb_data
        return info
