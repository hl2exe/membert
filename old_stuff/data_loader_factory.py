"""

@Author: Federico Ruggeri

@Date: 16/01/18

"""

from utility.log_utils import get_logger
from data_loader import Task1Loader, Task2Loader, Task1KBLoader, Task1SingleLoader, Task1SingleKBLoader, SupervisedTask1SingleLoader

logger = get_logger(__name__)


class DataLoaderFactory(object):

    supported_data_loaders = {
        'task1': Task1Loader,
        'task2': Task2Loader,
        'task1_kb': Task1KBLoader,
        'task1_single': Task1SingleLoader,
        'task1_single_kb': Task1SingleKBLoader,
        'task1_supervised_single_kb': SupervisedTask1SingleLoader
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataLoaderFactory.supported_data_loaders[key]:
            return DataLoaderFactory.supported_data_loaders[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
