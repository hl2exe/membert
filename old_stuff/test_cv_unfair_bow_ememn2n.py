"""

@Author: Federico Ruggeri

@Date: 27/03/2019

"""

import os
from collections import OrderedDict
from datetime import datetime

import numpy as np
from keras import backend as K
from keras.callbacks import EarlyStopping
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import f1_score

import const_define as cd
import reporters
from data_loader import Task1SingleKBLoader
from nn_models_v1 import Bow_EMemN2N_tf
from preprocessing import MemoryPreprocessor
from splitter import GeneralSplitter
from utility.cross_validation_utils import PrebuiltCV
from utility.embedding_utils import convert_labels
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger

logger = get_logger(__name__)

if __name__ == '__main__':

    # Step 1: load dataset
    data_base_path = os.path.join(cd.TOS_100_DIR, 'sentences')
    labels_base_path = os.path.join(cd.TOS_100_DIR, 'labels')

    task1_loader = Task1SingleKBLoader()

    key = 'CH'
    data_handle = task1_loader.load(data_base_path=data_base_path,
                                    labels_base_path=labels_base_path,
                                    category=key)

    # Step 2: prepare for CV

    # Loading training settings -> check training_config.json
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Callbacks: Early stopper
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])

    # If you want to use validation loss as stopping criteria:
    # Replace 'f1_score' with 'val_loss' and set monitor_op = np.less
    early_stopper.monitor = 'f1_score'
    early_stopper.monitor_op = np.greater

    # Loading network configuration -> check model_config.json
    network_args = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))['bow_unfair_ememn2n_tf']

    # Build the data converter: from sentences (text) to bag of words
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}

    # Retrieve model specific parameters (for model definition)
    network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                              if 'model_class' in value['flags']}

    # Retrieve CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key='test')
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, 'tos_100_splits_10_kfold.json')
    cv.load_folds(load_path=folds_path)
    list_path = os.path.join(cd.PREBUILT_FOLDS_DIR, 'tos_100_splits_10_kfold.txt')
    dataset_list = cv.load_dataset_list(load_path=list_path)

    # Build the preprocessor: re-arranges sentences (text) according to a certain criteria
    preprocessor_args = {key: value['value'] for key, value in network_args.items()
                         if 'preprocessing' in value['flags']}
    preprocessor = MemoryPreprocessor(**preprocessor_args)

    # Build  the splitter
    splitter_args = {key: value['value'] for key, value in network_args.items()
                     if 'splitter' in value['flags']}
    splitter_args['validation_split'] = 0.1
    splitter = GeneralSplitter(**splitter_args)

    # Step 3: CV test
    cv_results = OrderedDict()
    all_labels = OrderedDict()
    all_weights = OrderedDict()
    all_preds = OrderedDict()

    for fold_idx, (train_indexes, test_indexes) in enumerate(cv.split(None)):

        test_docs = [name for idx, name in enumerate(dataset_list)
                     if idx in test_indexes]

        x_train, y_train, x_test, y_test = data_handle.get_split(docs=test_docs)

        # Pre-process fold data
        x_train, y_train = preprocessor.parse(x=x_train, y=y_train, additional_info=data_handle.get_additional_info())
        x_test, y_test = preprocessor.parse(x=x_test, y=y_test, additional_info=data_handle.get_additional_info())

        # Convert fold data

        # Ignore context data
        train_queries = list(x_train[1].values())
        train_queries = [item for group in train_queries for item in group]

        # BoW fitting
        vectorizer = TfidfVectorizer()
        vectorizer.fit(train_queries + data_handle.get_additional_info())

        train_queries = vectorizer.transform(train_queries).toarray()
        kb_conv = vectorizer.transform(data_handle.get_additional_info()).toarray()

        # The embedding_dimension parameter is now considered as the BoW size
        network_retrieved_args['answer_weights'] = [train_queries.shape[-1]]
        network_retrieved_args['embedding_dimension'] = train_queries.shape[-1]

        y_train = convert_labels(y_train)

        # Split train fold data into train/validation
        x_train, \
        y_train, \
        x_val, \
        y_val = splitter.split(x=train_queries, y=y_train)

        test_queries = list(x_test[1].values())
        test_queries = [item for group in test_queries for item in group]

        x_test = vectorizer.transform(test_queries).toarray()
        y_test = convert_labels(y_test)

        logger.info('KB clauses: {}'.format(len(data_handle.get_additional_info())))

        # Build memory data
        memory_clauses = np.reshape(kb_conv, [1, kb_conv.shape[0], kb_conv.shape[1]])
        max_memory_length = len(memory_clauses)

        new_context_train = np.repeat(memory_clauses, repeats=x_train.shape[0], axis=0)
        new_context_val = np.repeat(memory_clauses, repeats=x_val.shape[0], axis=0)
        new_context_test = np.repeat(memory_clauses, repeats=x_test.shape[0], axis=0)

        x_train = [new_context_train, x_train]
        x_val = [new_context_val, x_val]
        x_test = [new_context_test, x_test]

        logger.info('Total train memory: {}'.format(x_train[0].shape))
        logger.info('Total val memory: {}'.format(x_val[0].shape))
        logger.info('Total test memory: {}'.format(x_test[0].shape))

        # Build the network model
        network_retrieved_args['max_batch_size'] = training_config['batch_size']
        network = Bow_EMemN2N_tf(**network_retrieved_args)

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

        network.build_model(vocab_size=None, text_info=None, embedding_matrix=None)

        # Training
        network.fit(x=x_train, y=y_train,
                    callbacks=[early_stopper],
                    validation_data=(x_val, y_val),
                    **training_config)

        # Predictions
        val_predictions = network.predict(x=x_val,
                                          batch_size=training_config['batch_size'],
                                          verbose=0).ravel()

        train_predictions = network.predict(x=x_train,
                                            batch_size=training_config['batch_size'],
                                            verbose=0).ravel()

        test_predictions = network.predict(x=x_test,
                                           batch_size=training_config['batch_size']).ravel()

        # Storing results
        train_f1 = f1_score(y_true=y_train, y_pred=train_predictions, average='binary', pos_label=1)
        val_f1 = f1_score(y_true=y_val, y_pred=val_predictions, average='binary', pos_label=1)
        test_f1 = f1_score(y_true=y_test, y_pred=test_predictions, average='binary', pos_label=1)
        cv_results[fold_idx] = [train_f1, val_f1, test_f1]

        all_labels[fold_idx] = y_test
        all_preds[fold_idx] = test_predictions.ravel()
        all_weights[fold_idx] = network.get_attentions_weights(x=x_test, batch_size=training_config['batch_size'])

        logger.info('Fold results: {}'.format(cv_results[fold_idx]))

        # Flush
        K.clear_session()

    # Macro F1
    results = list(cv_results.values())
    logger.info('Macro f1: {}'.format(np.mean(results, axis=0)))

    # Micro F1

    all_f1 = f1_score(y_true=np.hstack(all_labels.values()),
                      y_pred=np.hstack(all_preds.values()),
                      average='binary', pos_label=1)
    logger.info('Micro F1: {}'.format(all_f1))

    # Saving data
    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    base_path = os.path.join(cd.PROJECT_DIR, 'cv_test_{}'.format(key.lower()),
                             'bow_unfair_ememn2n_tf', current_date)

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    save_path = os.path.join(base_path, 'scores.json')
    save_json(filepath=save_path, data=cv_results)

    reporters.save_model_info(folder=base_path, config_data=network_args)
    reporters.save_training_info(folder=base_path, training_info=training_config)

    save_json(os.path.join(base_path, 'predictions.json'), all_preds)
    save_json(os.path.join(base_path, 'weights.json'), all_weights)
