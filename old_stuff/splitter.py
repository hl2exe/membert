"""

@Author: Federico Ruggeri

@Date: 23/07/2019

"""

import numpy as np

from utility.log_utils import get_logger

logger = get_logger(__name__)


class BaseSplitter(object):

    def __init__(self, validation_split):
        self.validation_split = validation_split

    # TODO: if test is none then split x into train and test
    def split(self, x, y, x_test=None, y_test=None, text_info=None, return_text_info=False, data_keys=['text']):
        validation_samples = int(x.shape[0] * self.validation_split)
        x_val = x[-validation_samples:]
        y_val = y[-validation_samples:]
        x_train = x[:-validation_samples]
        y_train = y[:-validation_samples]

        if x_test is not None:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info, text_info, x_test, y_test
            else:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
        else:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info
            else:
                return x_train, y_train, x_val, y_val, text_info

    def split_by_indexes(self, x, y, val_indexes, train_indexes, text_info=None,
                         x_test=None, y_test=None, return_text_info=False):

        x_val = x[val_indexes]
        x_train = x[train_indexes]

        y_val = y[val_indexes]
        y_train = y[train_indexes]

        if x_test is None:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info
            else:
                return x_train, y_train, x_val, y_val
        else:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
            else:
                return x_train, y_train, x_val, y_val, x_test, y_test


class GeneralSplitter(BaseSplitter):

    def split(self, x, y, x_test=None, y_test=None, text_info=None, return_text_info=False, data_keys=['text']):
        validation_samples = int(len(x[data_keys[0]]) * self.validation_split)

        if type(x) == list:
            x_val = [item[-validation_samples:] for item in x]
            x_train = [item[:-validation_samples] for item in x]
        elif type(x) == np.ndarray:
            x_train = x[:-validation_samples]
            x_val = x[-validation_samples:]
        else:
            x_train = {key: item[:-validation_samples] for key, item in x.items()}
            x_val = {key: item[-validation_samples:] for key, item in x.items()}

        y_val = y[-validation_samples:]
        y_train = y[:-validation_samples]

        if x_test is None:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info
            else:
                return x_train, y_train, x_val, y_val
        else:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
            else:
                return x_train, y_train, x_val, y_val, x_test, y_test

    def split_by_indexes(self, x, y, val_indexes, train_indexes, text_info=None,
                         x_test=None, y_test=None, return_text_info=False):

        if type(x) == list:
            x_val = [[sample for idx, sample in enumerate(item) if idx in val_indexes] for item in x]
            x_train = [[sample for idx, sample in enumerate(item) if idx in train_indexes] for item in x]
        elif type(x) == np.ndarray:
            x_val = x[val_indexes]
            x_train = x[train_indexes]
        else:
            x_train = {key: [sample for idx, sample in enumerate(item) if idx in train_indexes] for key, item in
                       x.items()}
            x_val = {key: [sample for idx, sample in enumerate(item) if idx in val_indexes] for key, item in
                     x.items()}

        if type(y) == np.ndarray:
            y_val = y[val_indexes]
            y_train = y[train_indexes]
        else:
            y_val = [item for idx, item in enumerate(y) if idx in val_indexes]
            y_train = [item for idx, item in enumerate(y) if idx in train_indexes]

        if x_test is None:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info
            else:
                return x_train, y_train, x_val, y_val
        else:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
            else:
                return x_train, y_train, x_val, y_val, x_test, y_test


class SplitterFactory(object):
    supported_splitters = {
        'base_splitter': BaseSplitter,
        'general_splitter': GeneralSplitter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if SplitterFactory.supported_splitters[key]:
            return SplitterFactory.supported_splitters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
