"""

@Author: Federico Ruggeri

@Date: 29/11/18

TODO: remove this file since it is redundant

"""

import os

import const_define as cd
from utility.json_utils import save_json, load_json


def save_model_info(folder, model_type=None, config_data=None):
    if config_data is None:
        model_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))
        config_data = model_info[model_type]

    save_path = os.path.join(folder, cd.JSON_DISTRIBUTED_MODEL_CONFIG_NAME)
    save_json(filepath=save_path, data=config_data)


def save_training_info(folder, training_info=None):
    if training_info is None:
        training_info = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    save_path = os.path.join(folder, cd.JSON_TRAINING_CONFIG_NAME)
    save_json(filepath=save_path, data=training_info)
