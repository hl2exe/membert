import tensorflow as tf
import numpy as np
from tensorflow.contrib.sparsemax import sparsemax

if __name__ == '__main__':
    x = tf.placeholder(tf.float32, shape=[None])

    p1 = sparsemax(tf.reshape(x, shape=[1, tf.shape(x)[0]]))
    p1 = tf.reshape(p1, shape=[-1])
    p2 = tf.nn.softmax(x)

    my_x = [0, 50, 0, 0, 50]

    with tf.Session() as sess:
        my_p1, my_p2 = sess.run([p1, p2], {x: my_x})

    print(my_p1)
    print(my_p2)
