"""

Graph and Tree utils for GNNs

"""

import nltk
from utility.embedding_utils import pad_data
import numpy as np
from tqdm import tqdm


def depth_node_search(nodes, parent, depth, edges, node_indexes, edge_indices,
                      node_indices, node_segments):
    parent_value = parent.label()
    for node in parent:
        # If not leaf
        if type(node) is nltk.Tree:
            node_value = node.label()
            nodes.append(node_value)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])

            depth_node_search(nodes=nodes, parent=node, depth=depth + 1,
                              edges=edges, node_indexes=node_indexes,
                              edge_indices=edge_indices, node_indices=node_indices,
                              node_segments=node_segments)
        else:
            node_value = node
            nodes.append(node_value)

            edges.append(depth + 1)
            edge_idx = len(edges) - 1

            parent_idx = node_indexes[parent_value]
            current_idx = max(list(node_indexes.values())) + 1
            edge_indices.append([parent_idx, current_idx])
            node_indexes[node_value] = current_idx

            if parent_idx < len(node_indices):
                node_indices[parent_idx].append(edge_idx)
                node_segments[parent_idx].append(parent_idx)
            else:
                node_indices.append([edge_idx])
                node_segments.append([parent_idx])
            node_indices.append([edge_idx])
            node_segments.append([current_idx])

    return nodes, edge_indices, edges, node_indices, node_segments


def retrieve_tree_info(tree_text):
    tree = nltk.tree.Tree.fromstring(tree_text)

    nodes = ['ROOT']
    edges = []
    depth = 0
    node_indexes = {'ROOT': 0}
    node_edges_info = []
    node_segments = []
    edge_indices = []

    full_nodes, full_edge_indices, \
    full_edge_features, full_node_indices, full_node_segments = depth_node_search(nodes=nodes,
                                                                                  parent=tree,
                                                                                  depth=depth,
                                                                                  node_indices=node_edges_info,
                                                                                  node_indexes=node_indexes,
                                                                                  edge_indices=edge_indices,
                                                                                  node_segments=node_segments,
                                                                                  edges=edges)

    full_node_indices = [item for seq in full_node_indices for item in seq]
    full_node_segments = [item for seq in full_node_segments for item in seq]

    return full_nodes, full_edge_indices, full_edge_features, full_node_indices, full_node_segments


def retrieve_trees_info(trees, memo=None):
    nodes, edge_indices, edge_features, node_indices, node_segments = [], [], [], [], []

    for tree in tqdm(trees):

        if memo is not None and tree in memo:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments = memo[tree]
        else:
            tree_nodes, tree_edge_indices, tree_edge_features, \
            tree_node_indices, tree_node_segments = retrieve_tree_info(tree)

            if memo is not None:
                memo[tree] = (tree_nodes, tree_edge_indices, tree_edge_features, tree_node_indices, tree_node_segments)

        nodes.append(tree_nodes)
        edge_indices.append(tree_edge_indices)
        edge_features.append(tree_edge_features)
        node_indices.append(tree_node_indices)
        node_segments.append(tree_node_segments)

    return (nodes, edge_indices, edge_features, node_indices, node_segments), memo


def pad_tree_data(nodes, edge_indices, edge_features, node_indices, node_segments,
                  padding_length=None, features_padding=None, padding='post'):
    if padding_length is None:
        padding_length = max([len(graph) for graph in nodes])

    if features_padding is None:
        features_padding = max([len(seq) for features in edge_features for seq in features])

    graphs_pad_amount = [padding_length - len(graph) for graph in nodes]

    padded_nodes = pad_data(nodes, padding=padding, padding_length=padding_length)
    padded_edge_features = pad_data(list(map(lambda features: pad_data(features,
                                                                       padding='pre',
                                                                       padding_length=features_padding),
                                             edge_features)),
                                    padding=padding,
                                    padding_length=padding_length - 1)

    padded_edge_indices = np.array([edge_indices + [[0, 0] for _ in range(pad_amount)] for edge_indices, pad_amount in
                                    zip(edge_indices, graphs_pad_amount)])
    edge_mask = np.array(
        [[1] * (padding_length - 1 - pad_amount) + [0] * pad_amount for pad_amount in graphs_pad_amount])

    padded_node_indices = np.array([indices + [max(indices)] * pad_amount * 2 for indices, pad_amount in
                                    zip(node_indices, graphs_pad_amount)])

    padded_node_segments = []
    for idx, (segments, pad_amount) in enumerate(zip(node_segments, graphs_pad_amount)):
        segment_max = max(segments)
        to_add = np.arange(segment_max + 1, (segment_max + 1) + pad_amount).tolist()
        padded_segments = segments + [val for val in to_add for _ in (0, 1)]
        padded_segments = [item + idx * padding_length for item in padded_segments]
        padded_node_segments.append(padded_segments)

    padded_node_segments = np.array(padded_node_segments)

    return padded_nodes, padded_edge_indices, padded_edge_features, \
           padded_node_indices, padded_node_segments, edge_mask
