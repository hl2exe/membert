"""

@Author: Federico Ruggeri

@Date: 29/11/2018

"""

import os
from collections import OrderedDict
from old_stuff import cleaner
from utility.log_utils import get_logger

logger = get_logger(__name__)


# TODO: documentation
def iterate_over_files(base_path, doc_format='txt', verbose=0):

    for doc_name in os.listdir(base_path):
        sub_path = os.path.join(base_path, doc_name)
        if os.path.isfile(sub_path) and doc_name.endswith('.{}'.format(doc_format)):
            if verbose:
                logger.info('Loading document: {}'.format(doc_name))
            yield sub_path, doc_name


# TODO: documentation
def load_file_data(sub_path):
    """

    :param sub_path:
    :return:
    """

    sentences = []

    with open(sub_path, 'r') as f:
        for line in f:
            # sample = cleaner.filter_line(line=line,
            #                              function_names=['punctuation_filtering',
            #                                              'number_replacing_with_constant',
            #                                              "remove_special_words"])
            sentences.append(line)

    return sentences


# TODO: documentation
def load_data_labels(sub_path):
    """

    :param base_path:
    :param doc_name:
    :return:
    """

    labels = []

    with open(sub_path, 'r') as f:
        for line in f:
            label = int(line.strip())
            if label < 0:
                label = 0
            labels.append(label)

    return labels


# TODO: documentation
def load_all_data(data_base_path, labels_base_path, doc_format='txt'):
    """

    :param data_base_path:
    :param labels_base_path:
    :param doc_format:
    :return:
    """

    x_data = OrderedDict()
    y_data = OrderedDict()

    # Load
    for (data_sub_path, doc_name) in iterate_over_files(base_path=data_base_path,
                                                        doc_format=doc_format):
        # Load data
        sentences = load_file_data(data_sub_path)
        x_data[doc_name] = sentences

        # Load labels
        labels = load_data_labels(os.path.join(labels_base_path, doc_name))
        y_data[doc_name] = labels

    return x_data, y_data