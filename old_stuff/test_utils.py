"""

@Author: Federico Ruggeri

@Date: 29/11/2018

"""

import multiprocessing as mp
import os

import numpy as np
from keras import backend as K
from keras.callbacks import EarlyStopping, LambdaCallback
from sklearn import metrics
from sklearn.utils import class_weight
from tqdm import tqdm

import const_define as cd
import loader
from callbacks.keras_callbacks import EpochLogger
from factories.converter_factory import ConverterFactory
from factories.model_factory import ModelFactory
from factories.preprocessing_factory import PreprocessingFactory
from factories.splitter_factory import SplitterFactory
from utility import embedding_utils
from utility.json_utils import load_json
from utility.log_utils import get_logger
from collections import OrderedDict
from custom_callbacks import TensorBoard
from datetime import datetime

logger = get_logger(__name__)

supported_custom_metrics = {

}


def build_metrics(error_metrics):
    """
    Build validation metrics from given metrics name and problem type

    :param error_metrics: list of error metrics names (strings)
    :return: list of pointers to error metrics functions
    """

    parsed_metrics = []

    for metric in error_metrics:
        if hasattr(metrics, metric):
            parsed_metrics.append(getattr(metrics, metric))
        elif metric in supported_custom_metrics:
            parsed_metrics.append(supported_custom_metrics[metric])
        else:
            logger.warn('Unknown metric given (got {})! Skipping...'.format(metric))

    # Empty metrics list
    if not parsed_metrics:
        message = 'No valid metrics found! Aborting...'
        logger.error(message)
        raise RuntimeError(message)

    return parsed_metrics


# TODO: documentation
def compute_iteration_validation_error(parsed_metrics, true_values, predicted_values,
                                       error_metrics_additional_info=None):
    error_metrics_additional_info = error_metrics_additional_info or {}
    fold_error_info = {}

    for metric in parsed_metrics:
        additional_info = error_metrics_additional_info[metric.__name__] \
            if metric.__name__ in error_metrics_additional_info else {}
        signal_error = metric(y_true=true_values, y_pred=predicted_values,
                              **additional_info)
        fold_error_info.setdefault(metric.__name__, signal_error)

    return fold_error_info


# TODO: documentation
def update_cv_validation_info(test_validation_info, iteration_validation_info):
    test_validation_info = test_validation_info or {}

    for metric in iteration_validation_info:
        test_validation_info.setdefault(metric, []).append(iteration_validation_info[metric])

    return test_validation_info


# TODO: documentation
def average_validation_info(test_validation_info):
    for metric in test_validation_info:
        test_validation_info[metric] = np.mean(test_validation_info[metric])

    return test_validation_info


# TODO: documentation
def get_validate_condition_value(validation_error, validate_on):
    return validation_error[validate_on]


# TODO: documentation
def _apply_test(excluded_key, x_data, y_data,
                embedding_model, embedding_dimension, build_embedding_matrix,
                model_type, network_args, batch_size, verbose, epochs, shuffle,
                validation_percentage, callbacks, error_metrics, preprocessor_type,
                preprocessor_args,
                error_metrics_additional_info=None, compute_test_info=True):
    validation_info = {}
    test_info = {}

    logger.info('Excluding: {}'.format(excluded_key))

    x_train = {key: value for key, value in x_data.items() if key != excluded_key}
    y_train = {key: value for key, value in y_data.items() if key != excluded_key}

    x_test = {excluded_key: x_data[excluded_key]}
    y_test = {excluded_key: y_data[excluded_key]}

    preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                **preprocessor_args)

    x_train, y_train = preprocessor.parse(x=x_train, y=y_train)
    x_test, y_test = preprocessor.parse(x=x_test, y=y_test)

    vocab_size, \
    padding_max_length, \
    embedding_matrix, \
    tokenizer = embedding_utils.build_embeddings_info(x_data=x_data,
                                                      x_train=x_train,
                                                      embedding_model=embedding_model,
                                                      embedding_dimension=embedding_dimension,
                                                      build_embedding_matrix=build_embedding_matrix)

    logger.info('Vocab size: {}'.format(vocab_size))
    logger.info('Padding max length: {}'.format(padding_max_length))

    x_train = embedding_utils.convert_data(data=x_train, tokenizer=tokenizer, padding_length=padding_max_length)
    y_train = embedding_utils.convert_labels(labels=y_train)

    validation_samples = int(x_train.shape[0] * validation_percentage)
    x_val = x_train[-validation_samples:]
    y_val = y_train[-validation_samples:]
    x_train = x_train[:-validation_samples]
    y_train = y_train[:-validation_samples]

    logger.info('Train shape: {}'.format(x_train.shape))
    logger.info('Validation shape: {}'.format(x_val.shape))

    x_test = embedding_utils.convert_data(data=x_test, tokenizer=tokenizer, padding_length=padding_max_length)
    y_test = embedding_utils.convert_labels(labels=y_test)

    network = ModelFactory.factory(cl_type=model_type, **network_args)
    model = network.build_remaining_model(vocab_size=vocab_size, padding_max_length=padding_max_length,
                                          embedding_dimension=embedding_dimension, embedding_matrix=embedding_matrix)

    labels = np.unique(y_train)
    weights = class_weight.compute_class_weight('balanced', labels, y_train)
    weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
    model.fit(x=x_train, y=y_train, batch_size=batch_size,
              class_weight=weights_dict, verbose=verbose, epochs=epochs, shuffle=shuffle,
              callbacks=callbacks,
              validation_data=(x_val, y_val))

    val_predictions = model.predict_classes(x=x_val,
                                            batch_size=batch_size,
                                            verbose=verbose)

    iteration_validation_error = compute_iteration_validation_error(parsed_metrics=error_metrics,
                                                                    true_values=y_val,
                                                                    predicted_values=val_predictions,
                                                                    error_metrics_additional_info=error_metrics_additional_info)

    validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                iteration_validation_info=iteration_validation_error)

    logger.info('Iteration validation info: {}'.format(iteration_validation_error))

    if compute_test_info:
        test_predictions = model.predict_classes(x=x_test,
                                                 batch_size=batch_size,
                                                 verbose=verbose)
        iteration_test_error = compute_iteration_validation_error(parsed_metrics=error_metrics,
                                                                  true_values=y_test,
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info)

        test_info = update_cv_validation_info(test_validation_info=test_info,
                                              iteration_validation_info=iteration_test_error)
        logger.info('Iteration test info: {}'.format(iteration_test_error))

    # Flush
    K.clear_session()

    if compute_test_info:
        return validation_info, test_info
    else:
        return validation_info


# TODO: documentation
def loo_test_mt(validation_percentage, data_base_path, labels_base_path,
                embedding_model_type, embedding_dimension, build_embedding_matrix,
                model_type, network_args, batch_size, verbose, epochs, shuffle,
                error_metrics, error_metrics_additional_info=None, doc_format='txt',
                compute_test_info=True, n_jobs=1):
    # Step 0: build metrics

    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 1: Loading data

    x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                          labels_base_path=labels_base_path)

    if build_embedding_matrix:
        embedding_model = embedding_utils.load_embedding_model(model_type=embedding_model_type,
                                                               embedding_dimension=embedding_dimension)
    else:
        embedding_model = None

    dataset_list = list(loader.iterate_over_files(base_path=data_base_path,
                                                  doc_format=doc_format))
    dataset_list = [item[1] for item in dataset_list]

    # Step 3: LOO test

    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    epoch_logger = EpochLogger()
    # training_logger = LambdaCallback(on_epoch_end=lambda epoch, logs: logger.info('Epoch: {}'.format(epoch + 1)))

    if n_jobs == -1:
        n_jobs = mp.cpu_count()

    pool = mp.Pool(processes=n_jobs)

    results = [pool.apply_async(func=_apply_test,
                                args=(
                                    excluded_key,
                                    x_data,
                                    y_data,
                                    embedding_model,
                                    embedding_dimension,
                                    build_embedding_matrix,
                                    model_type,
                                    network_args,
                                    batch_size,
                                    verbose,
                                    epochs,
                                    shuffle,
                                    validation_percentage,
                                    [early_stopper, epoch_logger],
                                    parsed_metrics,
                                    error_metrics_additional_info,
                                    compute_test_info,
                                ))
               for excluded_key in dataset_list]

    output = [p.get() for p in results]
    pool.close()
    pool.join()

    return output


# TODO: documentation
def loo_test(validation_percentage, data_handle,
             model_type, network_args, training_config,
             error_metrics, error_metrics_additional_info=None,
             compute_test_info=True):
    # Step 0: build metrics

    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # [Step 1]: load embedding model

    if model_type in cd.MODEL_CONFIG:
        converter_type = cd.MODEL_CONFIG[model_type]['converter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        converter_type = cd.ALGORITHM_CONFIG[archetype]['converter']
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}
    converter = ConverterFactory.factory(cl_type=converter_type, **converter_args)
    converter.load_embedding_model()

    dataset_list = list(data_handle.input_data.keys())

    # Step 2: LOO test

    # TODO: pass callbacks data as argument
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    training_logger = LambdaCallback(on_epoch_end=lambda epoch, logs: logger.info('Epoch: {}'.format(epoch + 1)))

    validation_info = OrderedDict()
    test_info = OrderedDict()
    for test_doc in tqdm(dataset_list):
        logger.info('Excluding: {}'.format(test_doc))

        x_train, y_train, x_test, y_test = data_handle.get_loo_split(test_doc=test_doc)

        # Pre-processing
        if model_type in cd.MODEL_CONFIG:
            preprocessor_type = cd.MODEL_CONFIG[model_type]['preprocessor']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            preprocessor_type = cd.ALGORITHM_CONFIG[archetype]['preprocessor']
        preprocessor_args = {key: value['value'] for key, value in network_args.items()
                             if 'preprocessing' in value['flags']}
        preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                    **preprocessor_args)

        x_train, y_train = preprocessor.parse(x=x_train, y=y_train, additional_info=data_handle.get_additional_info())
        x_test, y_test = preprocessor.parse(x=x_test, y=y_test, additional_info=data_handle.get_additional_info())

        # Data conversion

        vocab_size, \
        embedding_matrix, \
        text_info = converter.retrieve_data_info(data=x_train)

        logger.info('Vocab size: {}'.format(vocab_size))
        for key in text_info:
            logger.info('{0} size: {1}'.format(key, text_info[key]))

        x_train, y_train, text_info = converter.convert_data(x=x_train, y=y_train, text_info=text_info,
                                                             return_text_info=True)
        x_test, y_test = converter.convert_data(x=x_test, y=y_test, text_info=text_info)

        # Train/Validation split
        if model_type in cd.MODEL_CONFIG:
            splitter_type = cd.MODEL_CONFIG[model_type]['splitter']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            splitter_type = cd.ALGORITHM_CONFIG[archetype]['splitter']

        splitter_args = {key: value['value'] for key, value in network_args.items()
                         if 'splitter' in value['flags']}
        splitter_args['validation_split'] = validation_percentage
        splitter = SplitterFactory.factory(cl_type=splitter_type,
                                           **splitter_args)

        x_train, \
        y_train, \
        x_val, \
        y_val, \
        text_info, \
        x_test, \
        y_test = splitter.split(x=x_train, y=y_train, x_test=x_test, y_test=y_test,
                                text_info=text_info,
                                return_text_info=True)

        logger.info('Train shapes: {}'.format(' '.join([str(len(item)) for item in x_train])))
        logger.info('Validation shapes: {}'.format(' '.join([str(len(item)) for item in x_val])))
        logger.info('Test shapes: {}'.format(' '.join([str(len(item)) for item in x_test])))

        network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

        network.build_model(vocab_size=vocab_size, text_info=text_info,
                            embedding_matrix=embedding_matrix)

        network.fit(x=x_train, y=y_train,
                    callbacks=[early_stopper, training_logger],
                    validation_data=(x_val, y_val),
                    **training_config)

        val_predictions = network.predict(x=x_val,
                                          batch_size=training_config['batch_size'],
                                          verbose=training_config['verbose'])

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=y_val,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        if compute_test_info:
            test_predictions = network.predict(x=x_test,
                                               batch_size=training_config['batch_size'],
                                               verbose=training_config['verbose'])

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=y_test,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info)

            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

        # Flush
        K.clear_session()

    if compute_test_info:
        return validation_info, test_info
    else:
        return validation_info


def cross_validation(validation_percentage, data_handle, cv,
                     model_type, network_args, training_config, dataset_list,
                     error_metrics, error_metrics_additional_info=None,
                     callbacks=None, compute_test_info=True, save_predictions=False,
                     use_tensorboard=False):

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', now)
        os.makedirs(tensorboard_base_dir)

    # [Step 1]: load embedding model

    if model_type in cd.MODEL_CONFIG:
        converter_type = cd.MODEL_CONFIG[model_type]['converter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        converter_type = cd.ALGORITHM_CONFIG[archetype]['converter']
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}
    converter = ConverterFactory.factory(cl_type=converter_type, **converter_args)
    converter.load_embedding_model()

    # Step 2: cross validation
    validation_info = OrderedDict()
    test_info = OrderedDict()

    if save_predictions:
        all_preds = OrderedDict()
        # all_attentions = OrderedDict()

    for fold_idx, (train_indexes, test_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        test_docs = [name for idx, name in enumerate(dataset_list)
                     if idx in test_indexes]

        x_train, y_train, x_test, y_test = data_handle.get_split(docs=test_docs)

        # Pre-processing
        if model_type in cd.MODEL_CONFIG:
            preprocessor_type = cd.MODEL_CONFIG[model_type]['preprocessor']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            preprocessor_type = cd.ALGORITHM_CONFIG[archetype]['preprocessor']
        preprocessor_args = {key: value['value'] for key, value in network_args.items()
                             if 'preprocessing' in value['flags']}
        preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                    **preprocessor_args)

        x_train, y_train = preprocessor.parse(x=x_train, y=y_train, additional_info=data_handle.get_additional_info())
        x_test, y_test = preprocessor.parse(x=x_test, y=y_test, additional_info=data_handle.get_additional_info())

        # Data conversion

        x_train, y_train, text_info = converter.fit_train_data(x=x_train, y=y_train)

        if text_info is not None:
            for key in text_info:
                logger.info('{0} size: {1}'.format(key, text_info[key]))

        x_test, y_test = converter.convert_data(x=x_test, y=y_test, text_info=text_info)

        # Train/Validation split
        if model_type in cd.MODEL_CONFIG:
            splitter_type = cd.MODEL_CONFIG[model_type]['splitter']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            splitter_type = cd.ALGORITHM_CONFIG[archetype]['splitter']

        splitter_args = {key: value['value'] for key, value in network_args.items()
                         if 'splitter' in value['flags']}
        splitter_args['validation_split'] = validation_percentage
        splitter = SplitterFactory.factory(cl_type=splitter_type,
                                           **splitter_args)

        x_train, \
        y_train, \
        x_val, \
        y_val, \
        text_info, \
        x_test, \
        y_test = splitter.split(x=x_train, y=y_train, x_test=x_test, y_test=y_test,
                                text_info=text_info,
                                return_text_info=True)

        logger.info('Train shapes: {}'.format(' '.join([str(len(item)) for item in x_train])))
        logger.info('Validation shapes: {}'.format(' '.join([str(len(item)) for item in x_val])))
        logger.info('Test shapes: {}'.format(' '.join([str(len(item)) for item in x_test])))

        network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

        network.build_model(text_info=text_info)

        if use_tensorboard:
            fold_log_dir = os.path.join(tensorboard_base_dir, 'fold_{}'.format(fold_idx))
            os.makedirs(fold_log_dir)
            tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                      histogram_freq=True,
                                      log_dir=fold_log_dir,
                                      visualization_freq=15)
            fold_callbacks = callbacks + [tensorboard]
        else:
            fold_callbacks = callbacks

        network.fit(x=x_train, y=y_train,
                    callbacks=fold_callbacks,
                    validation_data=(x_val, y_val),
                    **training_config)

        val_predictions = network.predict(x=x_val,
                                          batch_size=training_config['batch_size'],
                                          verbose=training_config['verbose'])

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=y_val,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions = network.predict(x=x_test,
                                           batch_size=training_config['batch_size'],
                                           verbose=training_config['verbose'])

        iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                  true_values=y_test,
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info)

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            # network_attentions = network.get_attentions_weights(x=x_test, batch_size=training_config['batch_size'])

            if save_predictions:
                all_preds[fold_idx] = test_predictions.ravel()
                # all_attentions[fold_idx] = network_attentions

        # Flush
        K.clear_session()

    if compute_test_info:
        if save_predictions:
            return validation_info, test_info, all_preds
        else:
            return validation_info, test_info
    else:
        if save_predictions:
            return validation_info, all_preds
        else:
            return validation_info
