import tensorflow as tf
from keras import activations
from keras import backend as K
from keras import constraints
from keras import initializers
from keras import regularizers
from keras.engine.topology import Layer
from keras.initializers import RandomNormal
from keras.layers import Dense, GRU
from tensorflow.python.ops import array_ops

from utility.python_utils import merge


class SimpleMemory(Layer):

    def __init__(self, vocab_size, embedding_dim,
                 init_mean, init_std,
                 hops, layer_wise,
                 position_encoding=None, embedding_initialier=None,
                 response_linearity=False, store_attention=True,
                 **kwargs):
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.init_mean = init_mean
        self.init_std = init_std
        self.hops = hops
        self.layer_wise = layer_wise
        self.position_encoding = position_encoding
        self.response_linearity = response_linearity
        self.store_attention = store_attention

        self.attention_weights = None

        if embedding_initialier is None:
            self.embedding_initializer = tf.random_normal([self.vocab_size - 1, self.embedding_dim],
                                                          stddev=self.init_std,
                                                          mean=self.init_mean)
        else:
            self.embedding_initializer = embedding_initialier

        self.hid = []

        super(SimpleMemory, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'vocab_size': self.vocab_size,
            'embedding_dim': self.embedding_dim,
            'init_mean': self.init_mean,
            'init_std': self.init_std,
            'hops': self.hops,
            'layer_wise': self.layer_wise,
            'position_encoding': self.position_encoding,
            'response_linearity': self.response_linearity,
            'store_attention': self.store_attention
        }
        base_config = super(SimpleMemory, self).get_config()
        return merge(config, base_config)

    def _layer_wise(self, A, B, C):
        self.A = []
        self.C = []

        A_var = tf.Variable(A, name='A_1')
        C_var = tf.Variable(C, name='C_1')

        self.B = tf.Variable(B, name='B_1')

        for hop in range(self.hops):
            self.A.append(A_var)
            self.C.append(C_var)

        self._trainable_weights = [A_var, self.B, C_var]

    def _adjacent_sharing(self, A, B, C):

        self.A = []
        self.B = tf.Variable(B, name='B_1')
        self.C = []

        A_var = tf.Variable(A, name='A_1')

        for hop in range(self.hops):
            if hop == 0:
                self.A.append(A_var)
            else:
                self.A.append(self.C[hop - 1])

            self.C.append(tf.Variable(C, name='C_{}'.format(hop + 1)))

        self._trainable_weights = [A_var, self.B] + self.C

    def build(self, input_shape):
        # embedding variables

        # reserve 0 index for pad element
        zeros = tf.constant(0, tf.float32, [1, self.embedding_dim])
        A = tf.concat(axis=0, values=[zeros, self.embedding_initializer])
        B = tf.concat(axis=0, values=[zeros, self.embedding_initializer])
        C = tf.concat(axis=0, values=[zeros, self.embedding_initializer])

        if self.layer_wise:
            self._layer_wise(A=A, B=B, C=C)
        else:
            self._adjacent_sharing(A=A, B=B, C=C)

        if self.response_linearity:
            self.H = tf.Variable(tf.random_normal([self.embedding_dim, self.embedding_dim],
                                                  mean=self.init_mean,
                                                  stddev=self.init_std))
            self._trainable_weights.append(self.H)

        super(SimpleMemory, self).build(input_shape)

    def call(self, inputs, **kwargs):
        # inputs -> tuple(context, query)
        context = inputs[0]
        context_dim = context.shape[1]
        query = inputs[1]

        with tf.name_scope('query_embedding'):
            # [batch_size, sentence_length, embedding_dim]
            query_emb = tf.nn.embedding_lookup(self.B, query)

            # [batch_size, sentence_length, sentence_length]
            if self.position_encoding is not None:
                query_emb = query_emb * self.position_encoding

            # [batch_size, embedding_dim]
            query_emb = tf.reduce_sum(query_emb, axis=1)

            self.hid.append(query_emb)

        with tf.name_scope('memn2n'):
            for hop in range(self.hops):

                with tf.name_scope('embeddings_hop_{}'.format(hop + 1)):

                    # [batch_size, paragraph_dim, sentence_length, embedding_dim]
                    A_emb = tf.nn.embedding_lookup(self.A[hop],
                                                   context)

                    # [batch_size, paragraph_dim, sentence_length, embedding_dim] * [sentence_length, embedding_dim]
                    # -> [batch_size, paragraph_dim, sentence_length, embedding_dim] (element-wise dot)
                    if self.position_encoding is not None:
                        A_emb = A_emb * self.position_encoding

                    # Sentence embedding: summing word embeddings of each sentence together
                    # [batch_size, paragraph_dim, embedding_dim]
                    A_emb = tf.reduce_sum(A_emb, axis=2)

                    # [batch_size, paragraph_dim, sentence_length, embedding_dim]
                    C_emb = tf.nn.embedding_lookup(self.C[hop],
                                                   context)

                    # [batch_size, paragraph_dim, sentence_length, embedding_dim]
                    if self.position_encoding is not None:
                        C_emb = C_emb * self.position_encoding

                    # [batch_size, paragraph_dim, embedding_dim]
                    C_emb = tf.reduce_sum(C_emb, axis=2)

                with tf.name_scope('hop_{}'.format(hop + 1)):

                    # [batch_size, embedding_dim] -> [batch_size, embedding_dim, 1]
                    hidden_resized = tf.expand_dims(self.hid[-1], -1)

                    # [batch_size, embedding_dim, 1] -> [batch_size, 1, embedding_dim]
                    hidden_resized = tf.transpose(hidden_resized, [0, 2, 1])

                    # [batch_size, 1, embedding_dim] X [batch_size, embedding_dim, paragraph_dim]
                    # similarity = tf.matmul(hidden_resized, A_emb,
                    #                        transpose_b=True)

                    # [batch_size, paragraph_dim, embedding_dim] * [batch_size, 1, embedding_dim]
                    # -> [batch_size, paragraph_dim, embedding_dim]
                    # -> [batch_size, paragraph_dim]
                    similarity = tf.reduce_sum(A_emb * hidden_resized, 2)

                    # [batch_size, paragraph_dim]
                    # similarity = tf.reshape(similarity, [-1, context_dim],
                    #                         name='similarity')

                    # batch_size, paragraph_dim]
                    attention_weights = tf.nn.softmax(similarity)

                    if self.store_attention:
                        self.attention_weights = attention_weights

                    # [batch_size, 1, paragraph_dim]
                    attention_weights = tf.reshape(attention_weights,
                                                   [-1, 1, context_dim],
                                                   name='attention_weights')

                    # [batch_size, 1, paragraph_dim] X [batch_size, paragraph_dim, embedding_dim]
                    # o_tensor = tf.matmul(attention_weights, C_emb)

                    # [batch_size, embedding_dim, paragraph_dim]
                    C_emb = tf.transpose(C_emb, [0, 2, 1])

                    # [batch_size, embedding_dim]
                    o_tensor = tf.reduce_sum(C_emb * attention_weights, 2)

                    # [batch_size, embedding_dim]
                    # o_tensor = tf.reshape(o_tensor, [-1, o_tensor.shape[-1]], name='o_tensor')

                    # [batch_size, embedding_dim]
                    if self.response_linearity:
                        self.hid[-1] = tf.matmul(self.hid[-1], self.H)

                    # [batch_size, embedding_dim]
                    response = tf.add(o_tensor, self.hid[-1], name='response')
                    self.hid.append(response)  # [query, response_1, response_2, ..., response_hops]

        tran_C = tf.transpose(self.C[-1], [1, 0])
        return tf.matmul(self.hid[-1], tran_C)
        # return self.hid[-1]

    def compute_output_shape(self, input_shape):
        return input_shape[0][0], self.vocab_size


class WordMemory(Layer):

    def __init__(self, vocab_size, embedding_dim,
                 init_mean, init_std,
                 hops, layer_wise,
                 position_encoding=None, embedding_initialier=None,
                 response_linearity=False, store_attention=True,
                 **kwargs):
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.init_mean = init_mean
        self.init_std = init_std
        self.hops = hops
        self.layer_wise = layer_wise
        self.position_encoding = position_encoding
        self.response_linearity = response_linearity
        self.store_attention = store_attention

        self.attention_weights = None

        if embedding_initialier is None:
            self.embedding_initializer = tf.random_normal([self.vocab_size - 1, self.embedding_dim],
                                                          stddev=self.init_std,
                                                          mean=self.init_mean)
        else:
            self.embedding_initializer = embedding_initialier

        self.hid = []

        super(WordMemory, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'vocab_size': self.vocab_size,
            'embedding_dim': self.embedding_dim,
            'init_mean': self.init_mean,
            'init_std': self.init_std,
            'hops': self.hops,
            'layer_wise': self.layer_wise,
            'position_encoding': self.position_encoding,
            'response_linearity': self.response_linearity,
            'store_attention': self.store_attention
        }
        base_config = super(WordMemory, self).get_config()
        return merge(config, base_config)

    def _layer_wise(self, A, B, C):
        self.A = []
        self.C = []

        A_var = tf.Variable(A, name='A_1')
        C_var = tf.Variable(C, name='C_1')

        self.B = tf.Variable(B, name='B_1')

        for hop in range(self.hops):
            self.A.append(A_var)
            self.C.append(C_var)

        self._trainable_weights = [A_var, self.B, C_var]

    def _adjacent_sharing(self, A, B, C):

        self.A = []
        self.B = tf.Variable(B, name='B_1')
        self.C = []

        A_var = tf.Variable(A, name='A_1')

        for hop in range(self.hops):
            if hop == 0:
                self.A.append(A_var)
            else:
                self.A.append(self.C[hop - 1])

            self.C.append(tf.Variable(C, name='C_{}'.format(hop + 1)))

        self._trainable_weights = [A_var, self.B] + self.C

    def build(self, input_shape):
        # embedding variables

        # reserve 0 index for pad element
        zeros = tf.constant(0, tf.float32, [1, self.embedding_dim])
        A = tf.concat(axis=0, values=[zeros, self.embedding_initializer])
        B = tf.concat(axis=0, values=[zeros, self.embedding_initializer])
        C = tf.concat(axis=0, values=[zeros, self.embedding_initializer])

        if self.layer_wise:
            self._layer_wise(A=A, B=B, C=C)
        else:
            self._adjacent_sharing(A=A, B=B, C=C)

        if self.response_linearity:
            self.H = tf.Variable(tf.random_normal([self.embedding_dim, self.embedding_dim],
                                                  mean=self.init_mean,
                                                  stddev=self.init_std))
            self._trainable_weights.append(self.H)

        super(WordMemory, self).build(input_shape)

    def call(self, inputs, **kwargs):
        # inputs -> tuple(context, query)

        # [batch_size, context_dim, sentence_length]
        context = inputs[0]
        context_dim = context.shape[1]
        sentence_length = context.shape[2]

        # [batch_size, query_length]
        query = inputs[1]

        with tf.name_scope('query_embedding'):
            # [batch_size, query_length, embedding_dim]
            query_emb = tf.nn.embedding_lookup(self.B, query)

            # [batch_size, query_length, embedding_dim]
            if self.position_encoding is not None:
                query_emb = query_emb * self.position_encoding      # element-wise dot (Hadamard)

            self.hid.append(query_emb)

        with tf.name_scope('memn2n'):
            for hop in range(self.hops):

                with tf.name_scope('embeddings_hop_{}'.format(hop + 1)):

                    # [batch_size, context_dim, sentence_length, embedding_dim]
                    A_emb = tf.nn.embedding_lookup(self.A[hop],
                                                   context)

                    # [batch_size, context_dim, sentence_length, embedding_dim] * [sentence_length, embedding_dim]
                    # -> [batch_size, context_dim, sentence_length, embedding_dim] (element-wise dot)
                    if self.position_encoding is not None:
                        A_emb = A_emb * self.position_encoding

                    # [batch_size, context_dim, sentence_length, embedding_dim]
                    C_emb = tf.nn.embedding_lookup(self.C[hop],
                                                   context)

                    # [batch_size, context_dim, sentence_length, embedding_dim]
                    if self.position_encoding is not None:
                        C_emb = C_emb * self.position_encoding

                    # [batch_size, context_dim * sentence_length, embedding_dim]
                    # A_emb = tf.reshape(A_emb, [-1, context_dim * sentence_length, self.embedding_dim])

                    # [batch_size, context_dim * sentence_length, embedding_dim]
                    # C_emb = tf.reshape(C_emb, [-1, context_dim * sentence_length, self.embedding_dim])

                with tf.name_scope('hop_{}'.format(hop + 1)):

                    # [batch_size, 1, query_length, embedding_dim]
                    #                           X
                    # [batch_size, context_dim, sentence_length, embedding_dim]
                    # -> [batch_size, context_dim,  sentence_length, embedding_dim]
                    temp_hid = tf.expand_dims(self.hid[-1], 1)
                    similarity = temp_hid * A_emb

                    # batch_size, context_dim * sentence_length, embedding_dim]
                    attention_weights = tf.nn.softmax(similarity, name='attention_weights')

                    if self.store_attention:
                        self.attention_weights = attention_weights

                    # [batch_size, context_dim, sentence_length, query_length]
                    #                       X
                    # [batch_size, context_dim, sentence_length, embedding_dim]
                    # -> [batch_size, query_length, embedding_dim]
                    o_tensor = attention_weights * C_emb
                    o_tensor = tf.reduce_sum(o_tensor, 1)

                    # [batch_size, query_length, embedding_dim]
                    if self.response_linearity:
                        self.hid[-1] = tf.tensordot(self.hid[-1], self.H, axes=[[2], [0]])

                    # [batch_size, query_length, embedding_dim]
                    response = tf.add(o_tensor, self.hid[-1], name='response')
                    self.hid.append(response)  # [query, response_1, response_2, ..., response_hops]

        res = tf.reduce_sum(self.hid[-1], 1)
        # res = tf.matmul(res, self.C[-1], transpose_b=True)
        return res

    def compute_output_shape(self, input_shape):
        return input_shape[0][0], self.embedding_dim


class SimpleMatrix(Layer):

    def __init__(self, init_mean, init_std, **kwargs):
        self.init_mean = init_mean
        self.init_std = init_std

        super(SimpleMatrix, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'init_mean': self.init_mean,
            'init_std': self.init_std
        }
        base_config = super(SimpleMatrix, self).get_config()
        return merge(config, base_config)

    def build(self, input_shape):
        num_units = input_shape[1]

        self.W = self.add_weight(name='w_matrix',
                                 shape=[num_units, 1],
                                 initializer=RandomNormal(mean=self.init_mean, stddev=self.init_std),
                                 trainable=True)

        super(SimpleMatrix, self).build(input_shape)

    def call(self, inputs, **kwargs):
        with tf.name_scope('simple_matrix'):
            a_hat = tf.matmul(inputs, self.W, name='a_hat')
        return a_hat

    def compute_output_shape(self, input_shape):
        return input_shape[0], 1


class SimpleEmbedding(Layer):

    def __init__(self, vocab_size, embedding_dim, embedding_initializer,
                 init_std, init_mean, **kwargs):
        self.embedding_dim = embedding_dim
        self.vocab_size = vocab_size
        self.init_std = init_std
        self.init_mean = init_mean

        if embedding_initializer is None:
            self.embedding_initializer = tf.random_normal([self.vocab_size - 1, self.embedding_dim],
                                                          stddev=self.init_std,
                                                          mean=self.init_mean)
        else:
            self.embedding_initializer = embedding_initializer
        super(SimpleEmbedding, self).__init__(**kwargs)

    def build(self, input_shape):
        zeros = tf.constant(0, tf.float32, [1, self.embedding_dim])
        embeddings = tf.concat(axis=0, values=[zeros, self.embedding_initializer])
        self.embedding_matrix = tf.Variable(embeddings, name='embedding_matrix')
        self._trainable_weights.append(self.embedding_matrix)
        super(SimpleEmbedding, self).build(input_shape)

    def call(self, inputs, **kwargs):
        return tf.nn.embedding_lookup(self.embedding_matrix, inputs)

    def compute_output_shape(self, input_shape):
        return tuple(list(input_shape) + [self.embedding_dim])


class FactMasking(Layer):

    def __init__(self, max_fact_count, **kwargs):
        self.max_fact_count = max_fact_count
        super(FactMasking, self).__init__(**kwargs)

    def call(self, inputs, **kwargs):
        # [batch_size, sentence_length * context_dim, hidden_size]
        input_states = inputs[0]
        hidden_size = input_states.shape[-1].value

        # [batch_size, sentence_length * context_dim]
        context_mask = inputs[1]

        # If max_fact_count > 1 (we have at least 2 sentences)
        # Otherwise go for word_level mode
        if self.max_fact_count > 1:
            # [batch_size * max_fact_count, hidden_size]
            _, filtered = tf.dynamic_partition(input_states, context_mask, num_partitions=2)
            # [batch_size, max_fact_count, hidden_size]
            facts = tf.reshape(filtered, shape=[tf.shape(input_states)[0], self.max_fact_count, hidden_size])

            return facts

        return input_states

    def compute_output_shape(self, input_shape):

        hidden_size = input_shape[0][-1]

        if self.max_fact_count > 1:
            total_facts = self.max_fact_count
        else:
            total_facts = input_shape[0][1]  # sentence_length

        return input_shape[0][0], total_facts, hidden_size


class EpisodicMemory(Layer):

    def __init__(self, max_iterations, attention_hidden_size,
                 init_mean, init_std, **kwargs):
        self.max_iterations = max_iterations
        self.attention_hidden_size = attention_hidden_size
        self.init_mean = init_mean
        self.init_std = init_std
        super(EpisodicMemory, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'max_iterations': self.max_iterations,
            'attention_hidden_size': self.attention_hidden_size,
            'init_mean': self.init_mean,
            'init_std': self.init_std
        }
        base_config = super(EpisodicMemory, self).get_config()
        return merge(config, base_config)

    def build(self, input_shape):

        hidden_size = input_shape[0][1]

        self.Wq = tf.Variable(tf.random_normal(shape=[hidden_size, hidden_size],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='Wq')
        self.Wm = tf.Variable(tf.random_normal(shape=[hidden_size, hidden_size],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='Wm')
        self.W1 = tf.Variable(tf.random_normal(shape=[5 * hidden_size + 4, self.attention_hidden_size],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='W1')
        self.b1 = tf.get_variable(name='b1', shape=[1], dtype=tf.float32)
        self.W2 = tf.Variable(tf.random_normal(shape=[self.attention_hidden_size, 1],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='W2')
        self.b2 = tf.get_variable(name='b2', shape=[1], dtype=tf.float32)

        self._trainable_weights.extend([self.Wq,
                                        self.Wm,
                                        self.W1,
                                        self.b1,
                                        self.W2,
                                        self.b2
                                        ])

        self.ep_gru = GRU(units=hidden_size, return_sequences=False)
        self.ep_gru.build(input_shape=(None, 1, hidden_size))
        self._trainable_weights += self.ep_gru.trainable_weights

        self.mem_gru = GRU(units=hidden_size, return_sequences=False)
        self.mem_gru.build(input_shape=(None, 1, hidden_size))
        self._trainable_weights += self.mem_gru.trainable_weights

        super(EpisodicMemory, self).build(input_shape)

    def get_attention_weight(self, fact, query, memory):

        # Fact: [batch_size, hidden_size]
        # Query: [batch_size, hidden_size]
        # Memory: [batch_size, hidden_size]

        def bilinear_matmul(x, W, y):
            # x -> [batch_size, hidden_size]
            # W -> [hidden_size, hidden_size]
            # y -> [batch_size, hidden_size]

            # Step 1: tf.matmul(x, W)                  ->   [batch_size, hidden_size]
            # Step 2: tf.expand_dims(step_1, -2)       ->   [batch_size, 1, hidden_size]
            # Step 3: tf.expand_dims(y, - 1)           ->   [batch_size, hidden_size, 1]
            # Step 4: tf.batch_mat_mul(step_2, step_3) ->   [batch_size, 1, 1]
            result = tf.matmul(tf.expand_dims(tf.matmul(x, W), -2), tf.expand_dims(y, -1))
            # [batch_size, 1]
            return tf.squeeze(result, [-1])

        # [batch_size, 5*input_module_hidden_size + 4]
        factors = tf.concat(axis=1, values=[fact,
                                            memory,
                                            query,
                                            fact * query,  # element-wise dot
                                            fact * memory,  # element-wise dot
                                            tf.expand_dims(tf.reduce_sum((fact - query) ** 2, 1), -1),
                                            # [batch_size, 1]
                                            tf.expand_dims(tf.reduce_sum((fact - memory) ** 2, 1), -1),
                                            # [batch_size, 1]
                                            bilinear_matmul(fact, self.Wq, query),  # [batch_size, 1]
                                            bilinear_matmul(fact, self.Wm, memory)  # [batch_size, 1]
                                            ])
        # Forward neural network

        # [batch_size, attention_hidden_size]
        l1 = tf.tanh(tf.matmul(factors, self.W1) + self.b1)

        # [batch_size, 1]
        l2 = tf.sigmoid(tf.matmul(l1, self.W2) + self.b2)

        return l2

    def get_episode(self, facts, query, memory):

        # Facts: [batch_size, max_fact_count, hidden_size]
        # Query: [batch_size, hidden_size]
        # Memory: [batch_size, hidden_size]

        max_facts = facts.shape[1]

        # [max_fact_count, batch_size, hidden_size]
        unpacked_facts = tf.unstack(tf.transpose(facts, [1, 0, 2]), num=max_facts)

        # [batch_size, hidden_size]
        initial_state = tf.zeros_like(query)
        hidden_state = initial_state
        with tf.name_scope('Episode'):
            # [batch_size, hidden_size]
            for fact in unpacked_facts:
                # [batch_size, 1]
                attention_weight = self.get_attention_weight(fact=fact,
                                                             query=query,
                                                             memory=memory)

                # [batch_size, 1] * [batch_size, hidden_size]  + [batch_size, 1] * [batch_size, hidden_size]
                # -> [batch_size, hidden_size]
                ep_hidden = self.ep_gru(tf.expand_dims(fact, -2), initial_state=hidden_state)
                hidden_state = attention_weight * ep_hidden + (
                            1 - attention_weight) * hidden_state

        episode = hidden_state

        # [batch_size, hidden_size]
        return episode

    def call(self, inputs, **kwargs):

        # [batch_size, hidden_size]
        query_fact = inputs[0]

        # [batch_size, max_fact_count, hidden_size]
        input_facts = inputs[1]

        # [batch_size, hidden_size]
        memory = tf.identity(query_fact)

        with tf.name_scope('Memory_Module'):
            # Outer loop
            for idx in range(self.max_iterations):
                # Inner loop
                # [batch_size, hidden_size]
                episode = self.get_episode(facts=input_facts,
                                           query=query_fact,
                                           memory=memory)
                # [batch_size, hidden_size]
                memory = self.mem_gru(tf.expand_dims(episode, -2), initial_state=memory)

        # Return the final memory to the answer module
        # [batch_size, hidden_size]
        return memory

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class EpisodicMemoryPlus(Layer):

    def __init__(self, max_iterations,
                 init_mean, init_std,
                 attention_hidden_size, **kwargs):
        self.max_iterations = max_iterations
        self.init_mean = init_mean
        self.init_std = init_std
        self.attention_hidden_size = attention_hidden_size

        super(EpisodicMemoryPlus, self).__init__(**kwargs)
        self.supports_masking = True

    def get_config(self):
        config = {
            'max_iterations': self.max_iterations,
            'attention_hidden_size': self.attention_hidden_size,
            'init_mean': self.init_mean,
            'init_std': self.init_std
        }
        base_config = super(EpisodicMemoryPlus, self).get_config()
        return merge(config, base_config)

    def build(self, input_shape):

        hidden_size = input_shape[0][1]

        self.W1 = tf.Variable(tf.random_normal(shape=[4 * hidden_size, self.attention_hidden_size],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='W1')
        self.b1 = tf.get_variable(name='b1', shape=[self.attention_hidden_size, ], dtype=tf.float32)
        self.W2 = tf.Variable(tf.random_normal(shape=[self.attention_hidden_size, 1],
                                               mean=self.init_mean,
                                               stddev=self.init_std),
                              dtype=tf.float32,
                              name='W2')
        self.b2 = tf.get_variable(name='b2', shape=[1], dtype=tf.float32)

        self._trainable_weights.extend([self.W1, self.b1, self.W2, self.b2])

        self.attGRU = SoftAttnGRU(units=hidden_size,
                                  return_sequences=False,
                                  use_bias=True)
        self.attGRU.build(input_shape=(None, input_shape[1][1], input_shape[1][2] + 1))
        self._trainable_weights += self.attGRU.trainable_weights

        # TODO: add parameter for sharable weights
        self.memory_nets = []
        for it in range(self.max_iterations):
            self.memory_nets.append(Dense(units=hidden_size, activation='relu'))
            self.memory_nets[it].build(input_shape=(None, 3 * hidden_size))
            self._trainable_weights += self.memory_nets[it].trainable_weights

        super(EpisodicMemoryPlus, self).build(input_shape)

    def get_attention(self, query_fact, previous_mem, context_fact, reuse):
        """
        Use query fact and previous memory to create scalar attention for current fact
        """

        # query_fact: [batch_size, hidden_size]
        # previous_mem: [batch_size, hidden_size]
        # context_fact: [batch_size, hidden_size]

        with tf.variable_scope('attention', reuse=reuse):
            features = [
                context_fact * query_fact,
                context_fact * previous_mem,
                tf.abs(context_fact - query_fact),
                tf.abs(context_fact - previous_mem)
            ]

            # [batch_size, 4*hidden_size]
            feature_vec = tf.concat(features, 1)

            # [batch_size, 4*hidden_size] * [4*hidden_size, attention_hidden_size]
            # -> [batch_size, attention_hidden_size]
            attention = tf.matmul(feature_vec, self.W1) + self.b1

            # [batch_size, attention_hidden_size] * [attention_hidden_size, 1]
            # -> [batch_size, 1]
            attention = tf.tanh(tf.matmul(attention, self.W2) + self.b2)

        return attention

    def get_episode(self, facts, query, memory, hop_index):
        # Facts: [batch_size, context_dim, hidden_size]
        # Query: [batch_size, hidden_size]
        # Memory: [batch_size, hidden_size]

        attention_weights = [tf.squeeze(self.get_attention(query_fact=query,
                                                           previous_mem=memory,
                                                           context_fact=fact,
                                                           reuse=bool(hop_index) or bool(i)),
                                        axis=1)
                             for i, fact in enumerate(tf.unstack(facts, axis=1))]

        # list (context_dim) of shape [batch_size, 1]
        # [batch_size, context_dim]
        attention_weights = tf.transpose(tf.stack(attention_weights))
        attention_weights = tf.nn.softmax(attention_weights)
        # [batch_size, context_dim, 1]
        attention_weights = tf.expand_dims(attention_weights, axis=-1)

        reuse = True if hop_index > 0 else False

        # Concatenating fact vectors and attention weights for input to attention GRU
        # [batch_size, context_dim, hidden_size + 1]
        gru_inputs = tf.concat([facts, attention_weights], 2)

        with tf.variable_scope('attention_gru', reuse=reuse):
            # [batch_size, hidden_size]
            episode = self.attGRU(gru_inputs)

        return episode

    def call(self, inputs, **kwargs):
        # [batch_size, hidden_size]
        query_fact = inputs[0]

        # [batch_size, context_dim, hidden_size]
        input_facts = inputs[1]

        # [batch_size, hidden_size]
        memory = query_fact

        with tf.name_scope('Memory_Module'):
            # Outer loop
            for idx in range(self.max_iterations):
                # Inner loop
                # [batch_size, hidden_size]
                episode = self.get_episode(facts=input_facts,
                                           query=query_fact,
                                           memory=memory,
                                           hop_index=idx)

                with tf.variable_scope('Episode_{}'.format(idx + 1)):
                    # [batch_size, 3*hidden_size] * [3*hidden_size, hidden_size] -> [batch_size, hidden_size]
                    memory = self.memory_nets[idx](tf.concat([memory, episode, query_fact], 1))

        # Return the final memory to the answer module
        return memory

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class SoftAttnGRU(Layer):

    def __init__(self,
                 units,
                 activation='tanh',
                 recurrent_activation='hard_sigmoid',
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 recurrent_initializer='orthogonal',
                 bias_initializer='zeros',
                 kernel_regularizer=None,
                 recurrent_regularizer=None,
                 bias_regularizer=None,
                 kernel_constraint=None,
                 recurrent_constraint=None,
                 bias_constraint=None,
                 dropout=0.,
                 recurrent_dropout=0.,
                 implementation=1,
                 return_sequences=False,
                 **kwargs):
        """Identical to keras.recurrent.GRUCell.
        The difference comes from the computation in self.call
        """

        super(SoftAttnGRU, self).__init__(**kwargs)

        self.units = units
        self.return_sequences = return_sequences
        self.activation = activations.get(activation)
        self.recurrent_activation = activations.get(recurrent_activation)
        self.use_bias = use_bias

        self.kernel_initializer = initializers.get(kernel_initializer)
        self.recurrent_initializer = initializers.get(recurrent_initializer)
        self.bias_initializer = initializers.get(bias_initializer)

        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.recurrent_regularizer = regularizers.get(recurrent_regularizer)
        self.bias_regularizer = regularizers.get(bias_regularizer)

        self.kernel_constraint = constraints.get(kernel_constraint)
        self.recurrent_constraint = constraints.get(recurrent_constraint)
        self.bias_constraint = constraints.get(bias_constraint)

        self.dropout = min(1., max(0., dropout))
        self.recurrent_dropout = min(1., max(0., recurrent_dropout))
        self.implementation = implementation
        self.state_size = self.units
        self._dropout_mask = None
        self._recurrent_dropout_mask = None

        self._input_map = {}

        super(SoftAttnGRU, self).__init__(**kwargs)

    def compute_output_shape(self, input_shape):

        out = list(input_shape)
        out[-1] = self.units
        if self.return_sequences:
            return out
        else:
            return out[0], out[-1]

    def build(self, input_shape):

        input_dim = input_shape[-1] - 1

        self.kernel = self.add_weight(shape=(input_dim, self.units * 3),
                                      name='kernel',
                                      initializer=self.kernel_initializer,
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)

        self.recurrent_kernel = self.add_weight(
            shape=(self.units, self.units * 3),
            name='recurrent_kernel',
            initializer=self.recurrent_initializer,
            regularizer=self.recurrent_regularizer,
            constraint=self.recurrent_constraint)

        if self.use_bias:
            self.bias = self.add_weight(shape=(self.units * 3,),
                                        name='bias',
                                        initializer=self.bias_initializer,
                                        regularizer=self.bias_regularizer,
                                        constraint=self.bias_constraint)
        else:
            self.bias = None

        self.kernel_z = self.kernel[:, :self.units]
        self.recurrent_kernel_z = self.recurrent_kernel[:, :self.units]
        self.kernel_r = self.kernel[:, self.units: self.units * 2]
        self.recurrent_kernel_r = self.recurrent_kernel[:,
                                  self.units:
                                  self.units * 2]
        self.kernel_h = self.kernel[:, self.units * 2:]
        self.recurrent_kernel_h = self.recurrent_kernel[:, self.units * 2:]

        if self.use_bias:
            self.bias_z = self.bias[:self.units]
            self.bias_r = self.bias[self.units: self.units * 2]
            self.bias_h = self.bias[self.units * 2:]
        else:
            self.bias_z = None
            self.bias_r = None
            self.bias_h = None
        super(SoftAttnGRU, self).build(input_shape)

    def step(self, inputs, states, training=None):
        """Computes the output of a single step. Unlike the vanilla GRU, attention is applied to the
        output, as per https://arxiv.org/pdf/1603.01417.pdf
        ----------
        inputs : (K.Tensor)
            A tensor of shape [batch_size, input_size+1]. The last element of each example is the
            attention score.
        states : (K.Tensor)
            Initial (list) of states
        training : (bool)
            Whether the network is in training mode or not.
        Returns
        -------
        (K.Tensor)
            The output for the current step, modified by attention
        """
        # Needs question as an input
        x_i, attn_gate = array_ops.split(inputs,
                                         num_or_size_splits=[self.units, 1], axis=1)
        h_tm1 = states[0]

        # dropout matrices for input units
        dp_mask = self._dropout_mask
        # dropout matrices for recurrent units
        rec_dp_mask = self._recurrent_dropout_mask

        if self.implementation == 1:
            if 0. < self.dropout < 1.:
                inputs_z = x_i * dp_mask[0]
                inputs_r = x_i * dp_mask[1]
                inputs_h = x_i * dp_mask[2]
            else:
                inputs_z = x_i
                inputs_r = x_i
                inputs_h = x_i
            x_z = K.dot(inputs_z, self.kernel_z)
            x_r = K.dot(inputs_r, self.kernel_r)
            x_h = K.dot(inputs_h, self.kernel_h)
            if self.use_bias:
                x_z = K.bias_add(x_z, self.bias_z)
                x_r = K.bias_add(x_r, self.bias_r)
                x_h = K.bias_add(x_h, self.bias_h)

            if 0. < self.recurrent_dropout < 1.:
                h_tm1_z = h_tm1 * rec_dp_mask[0]
                h_tm1_r = h_tm1 * rec_dp_mask[1]
                h_tm1_h = h_tm1 * rec_dp_mask[2]
            else:
                h_tm1_z = h_tm1
                h_tm1_r = h_tm1
                h_tm1_h = h_tm1

            z = self.recurrent_activation(
                x_z + K.dot(h_tm1_z, self.recurrent_kernel_z))
            r = self.recurrent_activation(
                x_r + K.dot(h_tm1_r, self.recurrent_kernel_r))

            hh = self.activation(x_h + K.dot(r * h_tm1_h,
                                             self.recurrent_kernel_h))
        else:
            if 0. < self.dropout < 1.:
                x_i *= dp_mask[0]
            matrix_x = K.dot(x_i, self.kernel)
            if self.use_bias:
                matrix_x = K.bias_add(matrix_x, self.bias)
            if 0. < self.recurrent_dropout < 1.:
                h_tm1 *= rec_dp_mask[0]
            matrix_inner = K.dot(h_tm1,
                                 self.recurrent_kernel[:, :2 * self.units])

            x_z = matrix_x[:, :self.units]
            x_r = matrix_x[:, self.units: 2 * self.units]
            recurrent_z = matrix_inner[:, :self.units]
            recurrent_r = matrix_inner[:, self.units: 2 * self.units]

            z = self.recurrent_activation(x_z + recurrent_z)
            r = self.recurrent_activation(x_r + recurrent_r)

            x_h = matrix_x[:, 2 * self.units:]
            recurrent_h = K.dot(r * h_tm1,
                                self.recurrent_kernel[:, 2 * self.units:])
            hh = self.activation(x_h + recurrent_h)
        h = z * h_tm1 + (1 - z) * hh

        # Attention modulated output.
        h = attn_gate * h + (1 - attn_gate) * h_tm1

        if 0 < self.dropout + self.recurrent_dropout:
            if training is None:
                h._uses_learning_phase = True
        return h, [h]

    def call(self, input_list, initial_state=None, mask=None, training=None):

        inputs = input_list

        self._generate_dropout_mask(inputs, training=training)
        self._generate_recurrent_dropout_mask(inputs, training=training)

        # if has_arg(self.layer.call, 'training'):
        self.training = training
        uses_learning_phase = False
        initial_state = self.get_initial_state(inputs)

        input_shape = K.int_shape(inputs)
        last_output, outputs, _ = K.rnn(self.step,
                                        inputs=inputs,
                                        constants=[],
                                        initial_states=initial_state,
                                        input_length=input_shape[1],
                                        unroll=False)
        if self.return_sequences:
            y = outputs
        else:
            y = last_output

        if (hasattr(self, 'activity_regularizer') and
                self.activity_regularizer is not None):
            regularization_loss = self.activity_regularizer(y)
            self.add_loss(regularization_loss, inputs)

        if uses_learning_phase:
            y._uses_learning_phase = True

        if self.return_sequences:
            timesteps = input_shape[1]
            new_time_steps = list(y.get_shape())
            new_time_steps[1] = timesteps
            y.set_shape(new_time_steps)
        return y

    def _generate_dropout_mask(self, inputs, training=None):
        if 0 < self.dropout < 1:
            ones = K.ones_like(K.squeeze(inputs[:, 0:1, :-1], axis=1))

            def dropped_inputs():
                return K.dropout(ones, self.dropout)

            self._dropout_mask = [K.in_train_phase(
                dropped_inputs,
                ones,
                training=training)
                for _ in range(3)]
        else:
            self._dropout_mask = None

    def _generate_recurrent_dropout_mask(self, inputs, training=None):
        if 0 < self.recurrent_dropout < 1:
            ones = K.ones_like(K.reshape(inputs[:, 0, 0], (-1, 1)))
            ones = K.tile(ones, (1, self.units))

            def dropped_inputs():
                return K.dropout(ones, self.dropout)

            self._recurrent_dropout_mask = [K.in_train_phase(
                dropped_inputs,
                ones,
                training=training)
                for _ in range(3)]
        else:
            self._recurrent_dropout_mask = None

    def get_initial_state(self, inputs):
        # build an all-zero tensor of shape (samples, output_dim)
        initial_state = K.zeros_like(inputs)  # (samples, timesteps, input_dim)
        initial_state = initial_state[:, :, :-1]
        initial_state = K.sum(initial_state, axis=(1, 2))  # (samples,)
        initial_state = K.expand_dims(initial_state)  # (samples, 1)
        if hasattr(self.state_size, '__len__'):
            return [K.tile(initial_state, [1, dim])
                    for dim in self.state_size]
        else:
            return [K.tile(initial_state, [1, self.state_size])]
