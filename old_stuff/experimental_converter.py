"""

@Author: Federico Ruggeri

@Date: 17/09/19

"""

import itertools

import nltk
import numpy as np
import tensorflow as tf
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer

from utility.embedding_utils import build_embeddings_matrix, pad_data, \
    load_embedding_model
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger
from utility.preprocessing_utils import windowing, group_data_including, group_data_excluding
import os
import const_define as cd

logger = get_logger(__name__)


# TODO: add support for OOV tokens (more flexible)
class BaseConverter(object):
    """
    Base converter interface. Text and labels are converted into numerical format (if needed).
    """

    def __init__(self, label, build_embedding_matrix=False,
                 embedding_model_type="",
                 embedding_dimension=32,
                 padding=True,
                 preload=False,
                 save_path=None,
                 text_column='text'):
        self.label = label
        self.build_embedding_matrix = build_embedding_matrix
        self.embedding_model = None
        self.embedding_model_type = embedding_model_type
        self.embedding_dimension = embedding_dimension
        self.padding = padding
        self.text_column = text_column
        self.preload = preload
        split_save_path = os.path.split(save_path)
        if save_path is not None:
            self.save_or_load_path = os.path.join(cd.SAVED_INFO_DIR,
                                                  os.path.split(split_save_path[0])[1],
                                                  split_save_path[1])
        else:
            logger.warn('Disabling pre-loading since save mode is disabled! '
                        'Enable model saving if you want to exploit pre-loading utility')
            self.preload = False

    def load_embedding_model(self):
        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

    def _preload_routine(self, load_path):
        # Load vocabulary
        table = load_json(load_path)
        self.tokenizer.index_word = table

    def fit_train_data(self, train_data, additional_data=None):

        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)

        save_or_load_path = os.path.join(self.save_or_load_path, 'vocab_{}.json'.format(additional_data['fold_idx']))
        if os.path.isdir(save_or_load_path):
            os.makedirs(save_or_load_path)

        def numpy_gen(tensor_batch):
            for item in tensor_batch:
                item = item.numpy()
                item = np.vectorize(lambda text: text.decode('utf-8'))(item)
                item = ' '.join(item)
                yield item

        if self.preload:
            self._preload_routine(save_or_load_path)
        else:
            data_to_fit = train_data.map(lambda x: x[self.text_column])
            data_to_fit_iterator = data_to_fit.__iter__()
            data_to_fit_gen = numpy_gen(data_to_fit_iterator)
            self.tokenizer.fit_on_texts(data_to_fit_gen)
            save_json(save_or_load_path, self.tokenizer.index_word)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        # Compute padding length
        encoded_data = train_data.map(lambda batch: self._encode(batch[self.text_column]))
        self.padding_max_length = max([len(item.numpy()) for item in encoded_data])

        # [Optional] Build embedding matrix
        embedding_matrix = None
        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        text_info = {'vocab_size': vocab_size,
                     'embedding_matrix': embedding_matrix,
                     'padding_max_length': self.padding_max_length}

        x_train, y_train, text_info = self.convert_data(
            data=train_data,
            text_info=text_info,
            return_text_info=True,
            additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def _py_encode_batch(self, batch, padding=False):
        batch = np.vectorize(lambda text: text.decode('utf-8'))(batch)
        batch = self.tokenizer.texts_to_sequences(batch)
        if padding:
            batch = pad_data(batch, padding='post', padding_length=self.padding_max_length)
        return batch

    def _encode(self, batch, padding=False):
        batch = tf.numpy_function(func=self._py_encode_batch,
                                  inp=[batch, padding],
                                  Tout=tf.int64)
        return batch

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):
        """
        I'm expecting the output of a BasePrepocessor -> default x and y
        """

        encoded_data = data.map(lambda batch: self._encode(batch[self.text_column], padding=True))
        label_data = data.map(lambda x: x[self.label])

        if return_text_info:
            return encoded_data, label_data, text_info
        else:
            return encoded_data, label_data


class ContextConverter(BaseConverter):
    """
    Context-aware converter.
    """

    def __init__(self, context_dim, mode, **kwargs):
        super(ContextConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def fit_train_data(self, train_data, additional_data=None):

        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)

        save_or_load_path = os.path.join(self.save_or_load_path, 'vocab_{}.json'.format(additional_data['fold_idx']))
        if os.path.isdir(save_or_load_path):
            os.makedirs(save_or_load_path)

        def numpy_gen(tensor_batch):
            for item in tensor_batch:
                item = item.numpy()
                item = np.vectorize(lambda text: text.decode('utf-8'))(item)
                item = ' '.join(item)
                yield item

        if self.preload:
            self._preload_routine(save_or_load_path)
        else:
            data_to_fit = train_data.map(lambda x: x[self.text_column])
            data_to_fit_iterator = data_to_fit.__iter__()
            data_to_fit_gen = numpy_gen(data_to_fit_iterator)
            self.tokenizer.fit_on_texts(data_to_fit_gen)
            save_json(save_or_load_path, self.tokenizer.index_word)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        # Compute padding length
        encoded_data = train_data.map(lambda batch: self._encode(batch[self.text_column]))
        self.padding_max_length = max([len(item.numpy()) for item in encoded_data])

        # [Optional] Build embedding matrix
        embedding_matrix = None
        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        text_info = {
            'sentence_max_length': self.padding_max_length,
            'query_max_length': self.padding_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix
        }

        x_train, y_train, text_info = self.convert_data(data=train_data,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):

        encoded_data = data.map(lambda batch: self._encode(batch[self.text_column], padding=True))
        label_data = data.map(lambda x: x[self.label])

        # TODO: build context
        if self.context_dim > 1:
            raise NotImplementedError()

        if return_text_info:
            return encoded_data, label_data, text_info
        else:
            return encoded_data, label_data


class KBContextConverter(ContextConverter):

    def __init__(self, use_kb=True, **kwargs):
        super(KBContextConverter, self).__init__(**kwargs)
        self.use_kb = use_kb

        if not use_kb and self.context_dim == 1:
            raise RuntimeError('Potential error detected! Found context dimension set to 1 and no KB allowed')

    def fit_train_data(self, train_data, additional_data=None):

        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)

        save_or_load_path = os.path.join(self.save_or_load_path, 'vocab_{}.json'.format(additional_data['fold_idx']))
        if os.path.isdir(save_or_load_path):
            os.makedirs(save_or_load_path)

        def numpy_gen(tensor_batch):
            for item in tensor_batch:
                item = item.numpy()
                item = np.vectorize(lambda text: text.decode('utf-8'))(item)
                item = ' '.join(item)
                yield item

        if self.preload:
            self._preload_routine(save_or_load_path)
        else:
            data_to_fit = train_data.map(lambda x: x[self.text_column])
            data_to_fit_iterator = data_to_fit.__iter__()
            if self.use_kb and 'kb' in additional_data:
                data_to_fit_gen = itertools.chain(numpy_gen(data_to_fit_iterator), additional_data['kb'])
            else:
                data_to_fit_gen = numpy_gen(data_to_fit_iterator)
            self.tokenizer.fit_on_texts(data_to_fit_gen)
            # save_json(save_or_load_path, self.tokenizer.index_word)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        # Compute padding length
        encoded_data = train_data.map(lambda batch: self._encode(batch[self.text_column]))
        self.padding_max_length = max([len(item.numpy()) for item in encoded_data])

        # [Optional] Build embedding matrix
        embedding_matrix = None
        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        # Converting KB
        if self.use_kb and 'kb' in additional_data:
            additional_data['kb'] = self.tokenizer.texts_to_sequences(additional_data['kb'])
            kb_max_length = max([len(item) for item in additional_data['kb']])
        else:
            kb_max_length = 0

        text_info = {
            'sentence_max_length': max(self.padding_max_length,
                                       kb_max_length) if self.context_dim > 1 else kb_max_length,
            'query_max_length': self.padding_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix
        }

        x_train, y_train, text_info = self.convert_data(data=train_data,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):
        output = super(KBContextConverter, self).convert_data(text_info=text_info,
                                                              data=data,
                                                              return_text_info=return_text_info,
                                                              additional_data=additional_data)

        if return_text_info:
            text_info = output[-1]

        # Adding KB to context input
        # Check use_kb
        if self.context_dim > 1:
            # if return_text_info:
            # text_info['memory_max_length'] = text_info['memory_max_length'] + len(additional_data['kb'])
            raise NotImplementedError()
        else:
            if return_text_info:
                text_info['memory_max_length'] = len(additional_data['kb'])
            kb_data = pad_data(data=additional_data['kb'],
                               padding='post')
            kb_len = len(kb_data)
            kb_data = tf.data.Dataset.from_tensors(tf.constant(kb_data, dtype=tf.int64))
            kb_data = kb_data.batch(kb_len).repeat()
            output = [tf.data.Dataset.zip((output[0], kb_data))] + list(output)[1:]

        # Updating text info
        if return_text_info:
            output = list(output)[:-1] + [text_info]

        return output


# TODO: adapt for tf.Dataset
class TransformerConverter(KBContextConverter):

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(TransformerConverter, self).convert_data(text_info=text_info,
                                                                df=df,
                                                                return_text_info=return_text_info,
                                                                additional_data=additional_data)

        # Build input mask
        # tokens that have a mask value of 1 are ignored during multi-head attention
        query_input = output[0][0]
        query_mask = query_input == 0
        query_mask = query_mask.reshape([query_mask.shape[0], 1, 1, query_mask.shape[1]]).astype(np.float64)

        # Build look-ahead mask: shape [samples, sample_length, sample_length]
        # tokens that have a mask value of 1 are ignored during multi-head attention
        look_ahead_mask = np.zeros(shape=(query_input.shape[1], query_input.shape[1]))
        look_ahead_mask[np.triu_indices(query_input.shape[1], 1)] = 1.
        look_ahead_mask = look_ahead_mask.reshape((1, 1,) + look_ahead_mask.shape)
        look_ahead_mask = np.repeat(look_ahead_mask, repeats=query_input.shape[0], axis=0)

        # Compute combined mask
        combined_mask = np.maximum(look_ahead_mask, query_mask)
        output[0].append(combined_mask)

        return output


class SupervisionContextConverter(KBContextConverter):
    """
    Context-aware converter that also builds supervision targets.
    """

    def __init__(self, partial_supervision=None, **kwargs):
        super(SupervisionContextConverter, self).__init__(**kwargs)
        # Tuple: [Flag, Coefficient]
        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
        else:
            self.partial_supervision = False

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):

        output = super(SupervisionContextConverter, self).convert_data(text_info=text_info,
                                                                       data=data,
                                                                       return_text_info=return_text_info,
                                                                       additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision:
            if return_text_info:
                text_info = output[-1]

            additional_data['supervision'] = {
                '-'.join(np.array(self.tokenizer.texts_to_sequences([key])[0]).astype(np.str)): item
                for key, item in additional_data['supervision'].items()
            }

            def get_supervision_target(batch, memory_max_length, context_dim):
                batch_targets = []
                for enc_text in batch:
                    enc_text = enc_text[np.argwhere(enc_text).ravel()]
                    enc_key = '-'.join(enc_text.astype(np.str))
                    if enc_key in additional_data['supervision']:
                        text_targets = [1 if idx + (context_dim - 1) in additional_data['supervision'][enc_key]
                                        else 0
                                        for idx in range(memory_max_length)]
                    else:
                        text_targets = [0] * memory_max_length
                    batch_targets.append(text_targets)
                return tf.constant(batch_targets, dtype=tf.int64)

            targets = output[0].map(lambda query, context: tf.numpy_function(func=get_supervision_target,
                                                                             inp=[query,
                                                                                  text_info['memory_max_length'],
                                                                                  self.context_dim],
                                                                             Tout=tf.int64))

            merged = tf.data.Dataset.zip((output[0], targets))
            output[0] = merged.map(lambda text_data, targets: (text_data[0], text_data[1], targets))

            if return_text_info:
                max_positive_amount = np.max([np.sum(item.numpy()[0]) for item in targets])
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


# TODO: test
class BoWConverter(BaseConverter):

    # TODO: parametrize code -> PCA | Chi^2, BoW n-grams, PoS tags [Y|N]
    def __init__(self, reduction_method='pca', bow_ngrams=(1, 2), use_pos_tags=True,
                 incremental_pca_bs=1000, n_iter=5, **kwargs):
        super(BoWConverter, self).__init__(embedding_model_type="",
                                           build_embedding_matrix=False,
                                           **kwargs)
        self.reduction_method = reduction_method
        self.bow_ngrams = bow_ngrams
        self.use_pos_tags = use_pos_tags
        self.incremental_pca_bs = incremental_pca_bs
        self.n_iter = n_iter
        self.sparse_support = False

    def build_pos_tags(self, data):
        data = data.decode('utf-8')
        pos_tags = []
        for item in data:
            item_tokens = nltk.word_tokenize(item)
            tags = nltk.pos_tag(item_tokens)
            tags = ' '.join(['-'.join(tup) for tup in tags])
            pos_tags.append(tags)

        return pos_tags

    def get_reduction_method(self):

        if self.reduction_method == 'pca':
            return PCA(n_components=self.embedding_dimension)
        elif self.reduction_method == 'incremental_pca':
            return IncrementalPCA(n_components=self.embedding_dimension, batch_size=self.incremental_pca_bs)
        elif self.reduction_method == 'truncated_svd':
            self.sparse_support = True
            return TruncatedSVD(n_components=self.embedding_dimension, n_iter=self.n_iter)
        else:
            raise RuntimeError('Specified reduction method has not been'
                               ' implemented yet or it is spelled wrong! Got: {}'.format(self.reduction_method))

    def _bow_encode(self, batch, padding=False):
        batch = np.vectorize(lambda text: text.decode('utf-8'))(batch)
        batch = self.bow_vectorizer.texts_to_sequences(batch)
        if padding:
            batch = pad_data(batch, padding='post', padding_length=self.padding_max_length)
        return batch

    def _pos_encode(self, batch, padding=False):
        batch = np.vectorize(lambda text: text.decode('utf-8'))(batch)
        batch = self.pos_vectorizer.texts_to_sequences(batch)
        if padding:
            batch = pad_data(batch, padding='post', padding_length=self.padding_max_length)
        return batch

    def _reducer_encode(self, batch, padding=False):
        batch = np.vectorize(lambda text: text.decode('utf-8'))(batch)
        batch = self.reducer.transform(batch)
        if padding:
            batch = pad_data(batch, padding='post', padding_length=self.padding_max_length)
        return batch

    def _reducer_concat(self, batch):
        bow_batch = batch[0]
        pos_batch = batch[1]
        merged_batch = np.hstack((bow_batch, pos_batch))
        return merged_batch

    def _preload_routine(self, load_path):
        # BoW
        bow_table = load_json(load_path.format('bow'))
        self.bow_vectorizer.index_word = bow_table

        # PoS
        pos_table = load_json(load_path.format('pos'))
        self.pos_vectorizer.index_word = pos_table

    def fit_train_data(self, train_data, additional_data=None):

        self.bow_vectorizer = TfidfVectorizer(ngram_range=self.bow_ngrams)
        self.pos_vectorizer = TfidfVectorizer()
        self.reducer = self.get_reduction_method()

        save_or_load_path = os.path.join(self.save_or_load_path, 'vocab_{}'.format(additional_data['fold_idx']))
        save_or_load_path += '_{}.json'

        bow_to_fit = train_data.map(lambda x: x[self.text_column])
        pos_to_fit = bow_to_fit.map(lambda item: tf.numpy_function(func=self.build_pos_tags,
                                                                   inp=[item, True],
                                                                   Tout=tf.string))

        def numpy_gen(tensor_batch):
            for item in tensor_batch:
                item = item.numpy()
                item = np.vectorize(lambda text: text.decode('utf-8'))(item)
                item = ' '.join(item)
                yield item

        if self.preload:
            self._preload_routine(save_or_load_path)
        else:
            bow_to_fit_iterator = bow_to_fit.__iter__()
            bow_to_fit_gen = numpy_gen(bow_to_fit_iterator)

            # BoW fitting
            self.bow_vectorizer.fit(bow_to_fit_gen)
            save_json(save_or_load_path.format('bow'), self.bow_vectorizer.index_word)

            # PoS fitting
            pos_to_fit_iterator = pos_to_fit.__iter__()
            pos_to_fit_gen = numpy_gen(pos_to_fit_iterator)
            self.pos_vectorizer.fit(pos_to_fit_gen)
            save_json(save_or_load_path.format('pos'), self.pos_vectorizer.index_word)

        # PCA fitting
        conv_data = bow_to_fit.map(lambda item: tf.numpy_function(func=self._bow_encode,
                                                                  inp=[item],
                                                                  Tout=tf.int64))
        conv_data_tags = pos_to_fit.map(lambda item: tf.numpy_function(func=self._pos_encode,
                                                                       inp=[item],
                                                                       Tout=tf.int64))

        def reducer_gen(batch):
            for item in batch:
                yield item

        reducer_to_fit = tf.data.Dataset.zip((conv_data, conv_data_tags))
        reducer_to_fit = reducer_to_fit.map(lambda item: tf.numpy_function(func=self._reducer_concat,
                                                                           inp=[item],
                                                                           Tout=tf.int64))
        reducer_to_fit_iterator = reducer_to_fit.__iter__()
        reducer_to_fit_gen = reducer_gen(reducer_to_fit_iterator)
        self.reducer.partial_fit(X=reducer_to_fit_gen)

        x_train, y_train, text_info = self.convert_data(text_info={},
                                                        data=train_data,
                                                        return_text_info=True,
                                                        additional_data=additional_data)
        self.padding_max_length = max([len(item.numpy()) for item in x_train])

        text_info['padding_max_length'] = self.padding_max_length

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):

        conv_text = data.map(lambda batch: self._bow_encode(batch[self.text_column]))

        text_pos = data.map(lambda item: tf.numpy_function(func=self.build_pos_tags,
                                                           inp=[item[self.text_column], True],
                                                           Tout=tf.string))
        conv_text_pos = text_pos.map(lambda item: self._pos_encode(item))

        conv_reduce = tf.data.Dataset.zip((conv_text, conv_text_pos))
        conv_reduce = conv_reduce.map(lambda item: tf.numpy_function(func=self._reducer_concat,
                                                                     inp=[item],
                                                                     Tout=tf.int64))

        conv_reduce = conv_reduce.map(lambda item: self._reducer_encode(item))
        label = data.map(lambda x: x[self.label])

        if return_text_info:
            return conv_reduce, label, text_info
        else:
            return conv_reduce, label


# TODO: test
class BowContextConverter(BoWConverter):

    def __init__(self, context_dim, mode, **kwargs):
        super(BowContextConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):

        output = super(BowContextConverter, self).convert_data(text_info=text_info,
                                                               data=data,
                                                               return_text_info=return_text_info,
                                                               additional_data=additional_data)

        if self.context_dim > 1:
            raise NotImplementedError()
        else:
            if return_text_info:
                text_info['sentence_max_length'] = output[0][0].shape[1]
                text_info['query_max_length'] = output[0][0].shape[1]
            return output


# TODO: test
class BowKBContextConverter(BowContextConverter):

    def fit_train_data(self, train_data, additional_data=None):

        self.bow_vectorizer = TfidfVectorizer(ngram_range=self.bow_ngrams)
        self.pos_vectorizer = TfidfVectorizer()
        self.reducer = self.get_reduction_method()

        save_or_load_path = os.path.join(self.save_or_load_path, 'vocab_{}'.format(additional_data['fold_idx']))
        save_or_load_path += '_{}.json'

        bow_to_fit = train_data.map(lambda x: x[self.text_column])

        pos_to_fit = bow_to_fit.map(lambda item: tf.numpy_function(func=self.build_pos_tags,
                                                                   inp=[item, True],
                                                                   Tout=tf.string))

        def numpy_gen(tensor_batch):
            for item in tensor_batch:
                item = item.numpy()
                item = np.vectorize(lambda text: text.decode('utf-8'))(item)
                item = ' '.join(item)
                yield item

        if self.preload:
            self._preload_routine(save_or_load_path)
        else:
            bow_to_fit_iterator = bow_to_fit.__iter__()
            bow_to_fit_gen = itertools.chain(numpy_gen(bow_to_fit_iterator), additional_data['kb'])

            # BoW fitting
            self.bow_vectorizer.fit(bow_to_fit_gen)
            save_json(save_or_load_path.format('bow'), self.bow_vectorizer.index_word)

            # PoS fitting
            pos_to_fit_iterator = pos_to_fit.__iter__()
            pos_to_fit_gen = numpy_gen(pos_to_fit_iterator)
            self.pos_vectorizer.fit(pos_to_fit_gen)
            save_json(save_or_load_path.format('pos'), self.pos_vectorizer.index_word)

        # PCA fitting
        conv_data = bow_to_fit.map(lambda item: tf.numpy_function(func=self._bow_encode,
                                                                  inp=[item],
                                                                  Tout=tf.int64))
        self.query_max_length = max([len(item.numpy()) for item in conv_data])

        conv_data_tags = pos_to_fit.map(lambda item: tf.numpy_function(func=self._bow_encode,
                                                                       inp=[item],
                                                                       Tout=tf.int64))

        def reducer_gen(batch):
            for item in batch:
                yield item

        reducer_to_fit = tf.data.Dataset.zip((conv_data, conv_data_tags))
        reducer_to_fit = reducer_to_fit.map(lambda item: tf.numpy_function(func=self._reducer_concat,
                                                                           inp=[item],
                                                                           Tout=tf.int64))
        reducer_to_fit_iterator = reducer_to_fit.__iter__()
        reducer_to_fit_gen = reducer_gen(reducer_to_fit_iterator)
        self.reducer.partial_fit(X=reducer_to_fit_gen)

        samples_amount = np.max([idx for idx, _ in reducer_to_fit.enumerate()]) + 1
        kb_samples = reducer_to_fit.skip(samples_amount - len(additional_data['kb']))
        self.sentence_max_length = max([len(item.numpy()) for item in kb_samples])

        additional_data['kb'] = self.reducer.transform([item.numpy()[0] for item in kb_samples])

        text_info = {
            'query_max_length': self.query_max_length,
            'sentence_max_length': self.sentence_max_length
        }

        x_train, y_train, text_info = self.convert_data(text_info=text_info,
                                                        data=train_data,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):
        output = super(BowKBContextConverter, self).convert_data(text_info=text_info,
                                                                 data=data,
                                                                 return_text_info=return_text_info,
                                                                 additional_data=additional_data)

        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                raise NotImplementedError()
            else:
                text_info['memory_max_length'] = len(additional_data['kb'])

            output = list(output)[:-1] + [text_info]

        return output


# TODO: test
class BowSupervisionConverter(BowKBContextConverter):

    def __init__(self, partial_supervision=False, enhance_partial_supervision=False,
                 use_max_margin_loss=False, **kwargs):
        super(BowSupervisionConverter, self).__init__(**kwargs)
        self.partial_supervision = partial_supervision
        self.enhance_partial_supervision = enhance_partial_supervision
        self.use_max_margin_loss = use_max_margin_loss

    def convert_data(self, text_info, data, return_text_info=False, additional_data=None):

        output = super(BowSupervisionConverter, self).convert_data(text_info=text_info,
                                                                   data=data,
                                                                   return_text_info=return_text_info,
                                                                   additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision:
            if return_text_info:
                text_info = output[-1]

            additional_data['supervision'] = {
                '-'.join(np.array(self.tokenizer.texts_to_sequences([key])[0]).astype(np.str)): item
                for key, item in additional_data['supervision'].items()
            }

            def get_supervision_target(batch, memory_max_length, context_dim):
                batch_targets = []
                for enc_text in batch:
                    enc_text = enc_text[np.argwhere(enc_text).ravel()]
                    enc_key = '-'.join(enc_text.astype(np.str))
                    if enc_key in additional_data['supervision']:
                        text_targets = [1 if idx + (context_dim - 1) in additional_data['supervision'][enc_key]
                                        else 0
                                        for idx in range(memory_max_length)]
                    else:
                        text_targets = [0] * memory_max_length
                    batch_targets.append(text_targets)
                return tf.constant(batch_targets, dtype=tf.int64)

            targets = output[0].map(lambda query, context: tf.numpy_function(func=get_supervision_target,
                                                                             inp=[query,
                                                                                  text_info['memory_max_length'],
                                                                                  self.context_dim],
                                                                             Tout=tf.int64))

            merged = tf.data.Dataset.zip((output[0], targets))
            output[0] = merged.map(lambda text_data, targets: (text_data[0], text_data[1], targets))

            if return_text_info:
                max_positive_amount = np.max([np.sum(item.numpy()[0]) for item in targets])
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


class ConverterFactory(object):
    supported_converters = {
        'bow_converter': BoWConverter,
        'bow_context_converter': BowContextConverter,
        'bow_kb_context_converter': BowKBContextConverter,
        'bow_supervised_context_converter': BowSupervisionConverter,
        'context_converter': ContextConverter,
        'kb_context_converter': KBContextConverter,
        'supervised_context_converter': SupervisionContextConverter,
        'base_converter': BaseConverter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ConverterFactory.supported_converters[key]:
            return ConverterFactory.supported_converters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
