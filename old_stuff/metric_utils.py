"""

@Author: Federico Ruggeri

@Date: 29/11/2018

"""

from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
import numpy as np


# TODO: documentation
def get_metric_data(y_true, y_pred, pos_label=1, average='binary'):

    labels = np.unique(y_true)

    acc = accuracy_score(y_true=y_true, y_pred=y_pred)
    prec = precision_score(y_true=y_true, y_pred=y_pred, labels=labels, pos_label=pos_label,
                           average=average)
    recall = recall_score(y_true=y_true, y_pred=y_pred, labels=labels,
                          pos_label=pos_label, average=average)
    f1 = f1_score(y_true=y_true, y_pred=y_pred, labels=labels,
                  pos_label=pos_label, average=average)

    return {'accuracy': acc,
            'precision': prec,
            'recall': recall,
            'f1': f1}
