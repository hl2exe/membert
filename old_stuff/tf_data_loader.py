"""

@Author: Federico Ruggeri

@Date: 17/09/2019

Experimental tf.data.Dataset

"""

import os

import tensorflow as tf

import const_define as cd
from utility.json_utils import load_json


def load_file_data(sub_path):
    """

    :param sub_path:
    :return:
    """

    sentences = []

    with open(sub_path, 'r') as f:
        for line in f:
            # sample = cleaner.filter_line(line=line,
            #                              function_names=['punctuation_filtering',
            #                                              'number_replacing_with_constant',
            #                                              "remove_special_words"])
            sentences.append(line)

    return sentences


def load_dataset(path, batch_size=1,
                 num_epochs=1, shuffle=False,
                 **kwargs):
    tf_data = tf.data.experimental.make_csv_dataset(
        file_pattern=path,
        batch_size=batch_size,
        num_epochs=num_epochs,
        shuffle=shuffle,
        **kwargs)

    return tf_data


class Task1Loader(object):
    """
    DataHandle wrapper
    """

    def load(self, path, category=None, split_column='document', **kwargs):
        dataset = load_dataset(path=path, **kwargs)
        return DataHandle(data=dataset, num_classes=1,
                          category=category, split_column=split_column)


class Task1KBLoader(object):
    """
    DataHandle wrapper that loads a KB as additional input
    """

    def load(self, path, category=None, split_column='document', **kwargs):
        dataset = load_dataset(path=path, **kwargs)

        if category:
            kb_name = '{}_KB.txt'.format(category)
        else:
            kb_name = 'KB.txt'

        kb_path = os.path.join(cd.KB_DIR, kb_name)
        kb_data = load_file_data(kb_path)

        return KBDataHandle(data=dataset,
                            num_classes=1,
                            category=category,
                            kb_data=kb_data,
                            split_column=split_column)


class SupervisedTask1Loader(object):
    """
    DataHandle wrapper that loads a KB and supervision targets as additional inputs
    """

    def load(self, path, category=None, split_column='document', **kwargs):
        # Load data
        dataset = load_dataset(path=path, **kwargs)

        # Load KB
        if category:
            kb_name = '{}_KB.txt'.format(category)
        else:
            kb_name = 'KB.txt'

        kb_path = os.path.join(cd.KB_DIR, kb_name)
        kb_data = load_file_data(kb_path)

        # Load supervision targets
        if category:
            targets_name = '{}_targets.json'.format(category)
        else:
            targets_name = 'targets.json'

        targets_path = os.path.join(cd.KB_DIR, targets_name)
        supervision_targets = load_json(targets_path)

        return SupervisedKBDataHandle(data=dataset,
                                      num_classes=1,
                                      kb_data=kb_data,
                                      supervision_targets=supervision_targets,
                                      category=category,
                                      split_column=split_column)


class DataLoaderFactory(object):
    supported_data_loaders = {
        'task1': Task1Loader,
        'task1_kb': Task1KBLoader,
        'task1_supervised_kb': SupervisedTask1Loader
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataLoaderFactory.supported_data_loaders[key]:
            return DataLoaderFactory.supported_data_loaders[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))


# TODO: documentation
class DataHandle(object):

    def __init__(self, data, num_classes, category=None, split_column='document'):
        self.data = data
        self.num_classes = num_classes
        self.label = category.upper() if category else 'label'
        self.split_column = split_column

    def get_split(self, docs):

        def split_filter(batch, list_filter, negate=False):
            list_filter = tf.constant(list_filter, dtype=tf.string)
            list_filter = tf.reshape(list_filter, [1, -1])
            filtered_ids = tf.equal(tf.reshape(batch[self.split_column], [-1, 1]), list_filter)
            reduced = tf.reduce_sum(tf.cast(filtered_ids, tf.float32))
            if negate:
                return tf.equal(reduced, tf.constant(0.))
            else:
                return tf.greater(reduced, tf.constant(0.))

        train_data = self.data.filter(lambda x: split_filter(x, docs, negate=True))
        test_data = self.data.filter(lambda x: split_filter(x, docs, negate=False))

        return train_data, test_data

    def get_additional_info(self):
        pass


# TODO: documentation
class KBDataHandle(DataHandle):

    def __init__(self, kb_data, **kwargs):
        super(KBDataHandle, self).__init__(**kwargs)
        self.kb_data = kb_data

    def get_additional_info(self):
        return {
            'kb': self.kb_data
        }


class SupervisedKBDataHandle(KBDataHandle):

    def __init__(self, supervision_targets, **kwargs):
        super(SupervisedKBDataHandle, self).__init__(**kwargs)
        self.supervision_targets = supervision_targets

    def get_additional_info(self):
        return {
            'kb': self.kb_data,
            'supervision': self.supervision_targets
        }
