"""

@Author: Federico Ruggeri

@Date: 24/07/19

TODO: fix ELMo and BERT memn2n

"""

import tensorflow as tf

# Limiting GPU access
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

import math
import numpy as np
from keras.preprocessing.sequence import pad_sequences

from custom_tf_v2 import M_Basic_Memn2n, M_Word_Memn2n, M_Baseline_LSTM, M_Baseline_CNN, M_Bow_Memn2n, M_Transformer, \
    M_ELMo_Basic_Memn2n, M_BERT_Basic_Memn2n, M_ParallelDLU, M_Entailment_Memn2n, M_SemanticIteratedTransformer, \
    M_SemanticCoAttention, M_ALBERTTextClassifier, M_Gated_Memn2n, M_Discriminative_Memn2n
from utility.cross_validation_utils import build_metrics, compute_metrics
from utility.log_utils import get_logger
from utility.python_utils import merge
from utility.tensorflow_utils_v2 import TransformerSchedule, add_gradient_noise
from tqdm import tqdm

logger = get_logger(__name__)


class Network(object):

    def __init__(self, embedding_dimension, additional_data=None, name='network'):
        self.embedding_dimension = embedding_dimension
        self.model = None
        self.optimizer = None
        self.name = name
        self.additional_data = additional_data

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        self.model.set_weights(weights)

    def save(self, filepath, overwrite=True):
        if not filepath.endswith('.h5'):
            filepath += '.h5'
        self.model.save_weights(filepath)

    def load(self, filepath):
        self.model.load_weights(filepath=filepath)

    def get_attentions_weights(self, x, batch_size=32):
        return None

    def build_tensorboard_info(self):
        pass

    def compute_output_weights(self, y_train, num_classes):

        self.num_classes = num_classes
        # single label scenario
        if self.model is None:
            if num_classes == 1:
                if len(y_train[y_train == 1]) > 0:
                    self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                else:
                    self.pos_weight = 1
            else:
                if type(y_train[0]) not in [list, np.ndarray]:
                    self.pos_weight = [len(y_train[y_train != cat]) / len(y_train[y_train == cat])
                                       for cat in range(num_classes)]
                else:
                    self.pos_weight = [len(y_train[y_train[:, cat] == 0]) / len(y_train[y_train[:, cat] == 1])
                                       for cat in range(num_classes)]
        else:
            if num_classes == 1:
                self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                self.class_weights = {0: 1, 1: self.pos_weight}
            else:
                raise RuntimeError('Not implemented yet')

    def get_batches(self, data, batch_size=32, num_batches=None):

        return_count = False

        if type(data) is list:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data[0]) / batch_size)

            batches = []
            for item_idx in range(len(data)):
                batches.append([data[item_idx][idx * batch_size: (idx + 1) * batch_size]
                                for idx in range(num_batches)])
        elif type(data) is np.ndarray:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data) / batch_size)
            batches = [data[idx * batch_size: (idx + 1) * batch_size] for idx in range(num_batches)]
        else:
            if num_batches is None:
                return_count = True
                data_keys = list(data.keys())
                num_batches = math.ceil(len(data[data_keys[0]]) / batch_size)

            batches = {}
            for key, item in data.items():
                batches[key] = [item[idx * batch_size: (idx + 1) * batch_size] for idx in range(num_batches)]

        if return_count:
            return batches, num_batches
        else:
            return batches

    def predict(self, x, batch_size=32, callbacks=None):

        callbacks = callbacks or []

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
        elif type(x) is np.ndarray:
            num_batches = math.ceil(len(x) / batch_size)
        else:
            x_keys = list(x.keys())
            num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)

        batches_x = self.get_batches(x, batch_size=batch_size, num_batches=num_batches)
        total_preds = []

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self.model)
                callback.on_prediction_begin(logs=None)

        for batch_idx in range(num_batches):

            if type(x) is list:
                batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
            elif type(x) is np.ndarray:
                batch_x = batches_x[batch_idx]
            else:
                batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_begin(batch=batch_idx, logs={'batch': batch_x})

            preds = self.batch_predict(x=batch_x)
            preds = preds.numpy()

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model': self.model})

            if type(preds) is list:
                if batch_idx == 0:
                    total_preds.extend(preds)
                else:
                    for idx, pred in enumerate(preds):
                        total_preds[idx] = np.append(total_preds[idx], pred, axis=0)
            else:
                total_preds.extend(preds)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs=None)

        return np.array(total_preds)

    def evaluate(self, x, y, batch_size=32, verbose=1):
        total_loss = {}

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
        elif type(x) is np.ndarray:
            num_batches = math.ceil(len(x) / batch_size)
        else:
            x_keys = list(x.keys())
            num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)

        batches_x = self.get_batches(x, batch_size=batch_size, num_batches=num_batches)
        batches_y = self.get_batches(y, batch_size=batch_size, num_batches=num_batches)

        for batch_idx in range(num_batches):

            if type(x) is list:
                batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
            elif type(x) is np.ndarray:
                batch_x = batches_x[batch_idx]
            else:
                batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

            if type(y) is list:
                batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
            else:
                batch_y = batches_y[batch_idx]

            batch_info = self.batch_evaluate(x=batch_x, y=batch_y)
            batch_info = {key: item.numpy() for key, item in batch_info.items()}

            for key, item in batch_info.items():
                if key not in total_loss:
                    total_loss[key] = item
                else:
                    total_loss[key] += item

        total_loss = {key: item / num_batches if not key.startswith('plot') else item
                      for key, item in total_loss.items()}
        return total_loss

    def fit(self, x=None, y=None, batch_size=None,
            epochs=1, verbose=1,
            callbacks=None, validation_data=None,
            shuffle=True, step_checkpoint=None,
            metrics=None, additional_metrics_info=None):

        self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self.model)
            callback.on_train_begin(logs=None)

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
            samples_indexes = np.arange(len(x[0]))
        elif type(x) is np.ndarray:
            num_batches = math.ceil(len(x) / batch_size)
            samples_indexes = np.arange(len(x))
        else:
            x_keys = list(x.keys())
            num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)
            samples_indexes = np.arange(len(x[x_keys[0]]))

        if verbose:
            logger.info('Start Training! Total batches: {}'.format(num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(num_batches * step_checkpoint)
                logger.info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > num_batches:
                    step_checkpoint = int(num_batches * 0.1)
                    logger.info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        for epoch in range(epochs):

            if hasattr(self.model, 'stop_training') and self.model.stop_training:
                break

            if shuffle:
                np.random.shuffle(samples_indexes)

            for callback in callbacks:
                callback.on_epoch_begin(epoch=epoch, logs={'epochs': epochs,
                                                           'sample_indexes': samples_indexes})

            # TODO: wrap condition
            if type(x) is list:
                temp_x = []
                for item in range(len(x)):
                    if type(x[item]) is np.ndarray:
                        temp_x.append(x[item][samples_indexes])
                    else:
                        temp_x.append([x[item][index] for index in samples_indexes])
                s_x = temp_x
            elif type(x) is np.ndarray:
                s_x = x[samples_indexes]
            else:
                s_x = {}
                for key, item in x.items():
                    if type(item) is np.ndarray:
                        s_x[key] = item[samples_indexes]
                    else:
                        s_x[key] = [item[index] for index in samples_indexes]

            if type(y) is list:
                temp_y = []
                for item in range(len(y)):
                    if type(y[item]) is np.ndarray:
                        temp_y.append(y[item][samples_indexes])
                    else:
                        temp_y.append(y[item][index] for index in samples_indexes)
                s_y = temp_y
            else:
                s_y = y[samples_indexes]

            batches_x = self.get_batches(s_x, batch_size=batch_size, num_batches=num_batches)
            batches_y = self.get_batches(s_y, batch_size=batch_size, num_batches=num_batches)

            train_loss = {}

            for batch_idx in tqdm(range(num_batches), leave=True, position=0):

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                # TODO: wrap condition
                if type(x) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
                elif type(x) is np.ndarray:
                    batch_x = batches_x[batch_idx]
                else:
                    batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

                if type(y) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
                else:
                    batch_y = batches_y[batch_idx]

                batch_info = self.batch_fit(x=batch_x, y=batch_y)
                batch_info = {key: item.numpy() for key, item in batch_info.items()}

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                # Monitoring at step checkpoint
                # TODO: wrap condition
                if step_checkpoint is not None and batch_idx > 0 and batch_idx % step_checkpoint == 0:
                    if validation_data is not None and metrics is not None:
                        val_info = self.evaluate(x=validation_data[0], y=validation_data[1], batch_size=batch_size)

                        if type(y) is list:
                            val_metrics_str_result = []
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            for idx in range(len(y)):
                                all_val_metrics = compute_metrics(parsed_metrics,
                                                                  true_values=validation_data[1][idx].astype(
                                                                      np.float32),
                                                                  predicted_values=val_predictions[idx].astype(
                                                                      np.float32),
                                                                  additional_metrics_info=additional_metrics_info[idx],
                                                                  suffix=idx)
                                val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])
                                val_metrics_str_result.append(val_metrics_str)
                        else:
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            all_val_metrics = compute_metrics(parsed_metrics,
                                                              true_values=validation_data[1].astype(np.float32),
                                                              predicted_values=val_predictions.astype(np.float32),
                                                              additional_metrics_info=additional_metrics_info)
                            val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                                   for key, value in all_val_metrics.items()])]
                        logger.info(
                            'Epoch: {0} -- Train Loss: {1} -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                        train_loss[
                                                                                                            'train_loss'],
                                                                                                        val_info[
                                                                                                            'val_loss'],
                                                                                                        ' -- '.join(
                                                                                                            val_metrics_str_result)))

                for key, item in batch_info.items():
                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

            train_loss = {key: item / num_batches if not key.startswith('plot') else item
                          for key, item in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                val_info = self.evaluate(x=validation_data[0], y=validation_data[1], batch_size=batch_size)

                if metrics is not None:
                    if type(y) is list:
                        val_metrics_str_result = []
                        all_val_metrics = {}
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        for idx in range(len(y)):
                            val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1][idx].astype(np.float32),
                                                          predicted_values=val_predictions[idx].astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info[idx],
                                                          suffix=idx)
                            val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in val_metrics.items()])
                            val_metrics_str_result.append(val_metrics_str)
                            merge(all_val_metrics, val_metrics)
                    else:
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        all_val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1].astype(np.float32),
                                                          predicted_values=val_predictions.astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info,
                                                          prefix='val')
                        val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])]
                    logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                            train_loss,
                                                                                                            val_info,
                                                                                                            ' -- '.join(
                                                                                                                val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                            train_loss,
                                                                                            val_info))

            for callback in callbacks:
                callback_args = train_loss
                callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def _parse_input(self, x, y=None):
        return x

    def _parse_predictions(self, predictions):
        return predictions

    def batch_fit(self, x, y):
        x = self._parse_input(x, y)
        loss, loss_info, grads = self.train_op(x, y)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))
        train_loss_info = {'train_{}'.format(key): item for key, item in loss_info.items()}
        train_loss_info['train_loss'] = loss
        return train_loss_info

    def batch_predict(self, x):
        x = self._parse_input(x)
        predictions = self.model(x, state='prediction', training=False)
        predictions = self._parse_predictions(predictions)
        return predictions

    def batch_evaluate(self, x, y):
        x = self._parse_input(x, y)
        loss, loss_info = self.loss_op(x, y, training=False)
        val_loss_info = {'val_{}'.format(key): item for key, item in loss_info.items()}
        val_loss_info['val_loss'] = loss
        return val_loss_info

    @tf.function
    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        return loss, loss_info, grads

    def build_model(self, text_info):
        raise NotImplementedError()

    @tf.function
    def loss_op(self, x, targets, training=False):
        raise NotImplementedError()


class Basic_Memn2n(Network):
    """
    Basic version of the end-to-end memory network.
    The model architecture is defined as follows:
        1. Memory Lookup: stack of Memory Lookup layers
        2. Memory Reasoning: stack of Dense layers
        3. Memory reasoning is done only at the end of the sequence of memory lookups.
    """

    def __init__(self, hops, optimizer, optimizer_args, embedding_info,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info, dropout_rate=.4,
                 positional_encoding=False, max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True,
                 l2_regularization=None, answer_weights=(256, 64), output_size=1,
                 accumulate_attention=False, **kwargs):

        super(Basic_Memn2n, self).__init__(**kwargs)

        self.hops = hops
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.embedding_info = embedding_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.partial_supervision_info = partial_supervision_info
        self.positional_encoding = positional_encoding
        self.accumulate_attention = accumulate_attention

        self.additional_data['categories'] = list(map(lambda x: x.upper(), self.additional_data['categories']))
        self.categories = self.additional_data['categories'] + ['not-unfair']
        self.output_size = output_size

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self.model = M_Basic_Memn2n(query_size=self.query_size,
                                    position_encoding=self.positional_encoding,
                                    sentence_size=self.sentence_size,
                                    accumulate_attention=self.accumulate_attention,
                                    vocab_size=self.vocab_size,
                                    embedding_dimension=self.embedding_dimension,
                                    hops=self.hops,
                                    embedding_info=self.embedding_info,
                                    answer_weights=self.answer_weights,
                                    dropout_rate=self.dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    memory_lookup_info=self.memory_lookup_info,
                                    extraction_info=self.extraction_info,
                                    reasoning_info=self.reasoning_info,
                                    partial_supervision_info=self.partial_supervision_info,
                                    output_size=self.output_size,
                                    padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x, y=None):
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat]
                cat_kb = pad_sequences(cat_kb, padding='post', maxlen=self.sentence_size)
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        x['context'] = np.array(context)
        x['context_mask'] = np.array(context_mask)

        if self.partial_supervision_info['flag']:
            targets = x['targets']
            query = x['text']
            x['targets'] = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            x['positive_indexes'] = positive_indexes
            x['negative_indexes'] = negative_indexes
            x['mask_indexes'] = mask_indexes

        return x

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            supervision_loss = tf.reduce_mean(tf.stack(self.model.supervision_losses))
            total_loss += supervision_loss * self.partial_supervision_info['coefficient']
            loss_info['supervision_loss'] = supervision_loss

        return total_loss, loss_info

    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class Gated_Memn2n(Basic_Memn2n):

    def __init__(self, gating_info, **kwargs):
        super(Gated_Memn2n, self).__init__(**kwargs)
        self.gating_info = gating_info

    def build_model(self, text_info):
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self.model = M_Gated_Memn2n(query_size=self.query_size,
                                    position_encoding=self.positional_encoding,
                                    sentence_size=self.sentence_size,
                                    accumulate_attention=self.accumulate_attention,
                                    vocab_size=self.vocab_size,
                                    embedding_dimension=self.embedding_dimension,
                                    hops=self.hops,
                                    embedding_info=self.embedding_info,
                                    answer_weights=self.answer_weights,
                                    dropout_rate=self.dropout_rate,
                                    l2_regularization=self.l2_regularization,
                                    embedding_matrix=text_info['embedding_matrix'],
                                    gating_info=self.gating_info,
                                    memory_lookup_info=self.memory_lookup_info,
                                    extraction_info=self.extraction_info,
                                    reasoning_info=self.reasoning_info,
                                    partial_supervision_info=self.partial_supervision_info,
                                    output_size=self.output_size,
                                    padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Discriminative_Memn2n(Basic_Memn2n):

    def __init__(self, discriminator_weights=(), confidence_coefficient=1e-3, **kwargs):
        super(Discriminative_Memn2n, self).__init__(hops=1, **kwargs)
        self.discriminator_weights = discriminator_weights
        self.confidence_coefficient = confidence_coefficient

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self.model = M_Discriminative_Memn2n(query_size=self.query_size,
                                             position_encoding=self.positional_encoding,
                                             sentence_size=self.sentence_size,
                                             accumulate_attention=self.accumulate_attention,
                                             vocab_size=self.vocab_size,
                                             embedding_dimension=self.embedding_dimension,
                                             hops=self.hops,
                                             embedding_info=self.embedding_info,
                                             answer_weights=self.answer_weights,
                                             dropout_rate=self.dropout_rate,
                                             l2_regularization=self.l2_regularization,
                                             embedding_matrix=text_info['embedding_matrix'],
                                             memory_lookup_info=self.memory_lookup_info,
                                             extraction_info=self.extraction_info,
                                             reasoning_info=self.reasoning_info,
                                             partial_supervision_info=self.partial_supervision_info,
                                             output_size=self.output_size,
                                             padding_amount=self.padding_amount if hasattr(self,
                                                                                           'padding_amount') else None,
                                             discriminator_weights=self.discriminator_weights)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):
        # query_answer -> [batch_size,]
        # mem_answer -> [batch_size,]
        # discriminator_answer -> [batch_size,]
        query_answer, mem_answer, discriminator_answer = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=query_answer.dtype)
        targets = tf.reshape(targets, query_answer.shape)

        query_probs = tf.clip_by_value(tf.nn.sigmoid(query_answer), 1e-10, 1.0)
        mem_probs = tf.clip_by_value(tf.nn.sigmoid(mem_answer), 1e-10, 1.0)

        # Query answer cross-entropy
        query_ce = tf.nn.weighted_cross_entropy_with_logits(logits=query_answer,
                                                            labels=targets,
                                                            pos_weight=self.pos_weight,
                                                            name='query_ce')
        self.query_ce = tf.reduce_mean(query_ce)
        total_loss = self.query_ce

        # Memory answer cross-entropy
        memory_ce = tf.nn.weighted_cross_entropy_with_logits(logits=mem_answer,
                                                             labels=targets,
                                                             pos_weight=self.pos_weight,
                                                             name='memory_ce')
        memory_weights = self.model.dist.prob(tf.stop_gradient(query_probs))

        self.memory_ce = tf.reduce_mean(memory_ce * memory_weights)
        total_loss += self.memory_ce

        # Discriminator loss
        # query_weights = tf.math.divide(query_probs, query_probs + mem_probs)
        # query_weights = tf.clip_by_value(query_weights, 1e-10, 1.0)
        # discriminator_loss = tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_answer,
        #                                                              labels=query_weights,
        #                                                              name='discriminator_ce')

        query_weights = tf.math.divide(query_probs, query_probs + mem_probs)
        query_weights = tf.clip_by_value(query_weights, 1e-10, 1.0)
        discriminator_loss_pos = tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_answer,
                                                                         labels=query_weights,
                                                                         name='discriminator_ce')

        mem_weights = tf.math.divide(mem_probs, query_probs + mem_probs)
        mem_weights = tf.clip_by_value(mem_weights, 1e-10, 1.0)
        discriminator_loss_neg = tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_answer,
                                                                         labels=mem_weights,
                                                                         name='discriminator_ce')

        discriminator_loss = discriminator_loss_pos * targets + discriminator_loss_neg * (1 - targets)

        self.discriminator_loss = tf.reduce_mean(discriminator_loss)
        total_loss += self.discriminator_loss

        # Confidence penalization
        confidence_entropy = tf.nn.sigmoid_cross_entropy_with_logits(logits=query_answer,
                                                                     labels=query_probs)
        # confidence_entropy = tf.reduce_sum(query_probs * tf.math.log(query_probs), axis=1)
        self.confidence_loss = -tf.reduce_mean(confidence_entropy) * self.confidence_coefficient
        total_loss += self.confidence_loss

        loss_info = {
            'query_cross_entropy': self.query_ce,
            'memory_cross_entropy': self.memory_ce,
            'discriminator_loss': self.discriminator_loss,
            'confidence_loss': self.confidence_loss
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Partial supervision
        if self.partial_supervision_info['flag']:
            supervision_loss = tf.reduce_mean(tf.stack(self.model.supervision_losses))
            total_loss += supervision_loss * self.partial_supervision_info['coefficient']
            loss_info['supervision_loss'] = supervision_loss

        return total_loss, loss_info

    def _parse_predictions(self, predictions):
        query_preds, mem_preds, discriminator_preds = predictions

        query_preds = tf.round(tf.nn.sigmoid(query_preds))
        query_preds = tf.reshape(query_preds, [-1, ])
        mem_preds = tf.round(tf.nn.sigmoid(mem_preds))
        mem_preds = tf.reshape(mem_preds, [-1, ])
        discriminator_preds = tf.round(tf.nn.sigmoid(discriminator_preds))
        discriminator_preds = tf.cast(discriminator_preds, tf.float32)
        discriminator_preds = tf.reshape(discriminator_preds, [-1, ])

        final_predictions = query_preds * discriminator_preds + mem_preds * (1 - discriminator_preds)

        return final_predictions


class Entailment_Memn2n(Basic_Memn2n):

    def __init__(self, entailment_supervision=None, **kwargs):
        super(Entailment_Memn2n, self).__init__(**kwargs)
        if entailment_supervision is not None:
            self.entailment_supervision = True
            self.entailment_margin = entailment_supervision[0]
            self.entailment_coefficient = entailment_supervision[1]
        else:
            self.entailment_supervision = False
            self.entailment_margin = None

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self.model = M_Entailment_Memn2n(query_size=self.query_size,
                                         sentence_size=self.sentence_size,
                                         vocab_size=self.vocab_size,
                                         embedding_dimension=self.embedding_dimension,
                                         hops=self.hops,
                                         response_linearity=self.response_linearity,
                                         share_memory=self.share_memory,
                                         answer_weights=self.answer_weights,
                                         dropout_rate=self.dropout_rate,
                                         l2_regularization=self.l2_regularization,
                                         embedding_matrix=text_info['embedding_matrix'],
                                         similarity_weights=self.similarity_weights,
                                         attention_operation=self.attention_operation,
                                         partial_supervision=self.partial_supervision,
                                         supervision_margin=self.supervision_margin,
                                         padding_amount=self.padding_amount if hasattr(self,
                                                                                       'padding_amount') else None,
                                         accumulate_attention=self.accumulate_attention,
                                         use_positional_encoding=self.use_positional_encoding,
                                         aggregation_mode=self.aggregation_mode,
                                         entailment_margin=self.entailment_margin,
                                         entailment_supervision=self.entailment_supervision)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x, y=None):
        if self.use_kb and 'kb' in self.additional_data:
            to_add = pad_sequences(self.additional_data['kb'], padding='post')
            if 'context' in x:
                for idx in range(len(x['context'])):
                    x['context'][idx] = np.append(x['context'][idx], to_add)
            else:
                x['context'] = np.array([to_add for _ in range(len(x['text']))], dtype=np.int32)

        if self.partial_supervision:
            targets = x['targets']
            query = x['text']
            x['targets'] = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            x['memory_positive_indexes'] = positive_indexes
            x['memory_negative_indexes'] = negative_indexes
            x['memory_mask_indexes'] = mask_indexes

        if self.entailment_supervision and y is not None:
            x['entailment_positive_indexes'] = np.argwhere(y == 1)
            x['entailment_negative_indexes'] = np.argwhere(y == 0)

        return x

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True, state='training')
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # Partial supervision
        if self.partial_supervision:
            supervision_loss = tf.reduce_mean(tf.stack(self.model.supervision_losses))
            total_loss += supervision_loss * self.partial_supervision_coeff
            loss_info['supervision_loss'] = supervision_loss

        # Entailment supervision
        if self.entailment_supervision:
            entailment_loss = tf.reduce_mean(tf.stack(self.model.entailment_supervision_losses))
            total_loss += entailment_loss * self.entailment_coefficient
            loss_info['entailment_loss'] = entailment_loss

        return total_loss, loss_info


class Basic_ELMo_Memn2n(Basic_Memn2n):

    def __init__(self, embedding_mode='sentence', **kwargs):
        super(Basic_ELMo_Memn2n, self).__init__(**kwargs)
        self.embedding_mode = embedding_mode

    def build_model(self, text_info):

        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self.model = M_ELMo_Basic_Memn2n(
            embedding_mode=self.embedding_mode,
            vocab_size=self.vocab_size,
            hops=self.hops,
            response_linearity=self.response_linearity,
            share_memory=self.share_memory,
            answer_weights=self.answer_weights,
            dropout_rate=self.dropout_rate,
            l2_regularization=self.l2_regularization,
            similarity_weights=self.similarity_weights,
            attention_operation=self.attention_operation,
            partial_supervision=self.partial_supervision,
            supervision_margin=self.supervision_margin,
            padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None,
            accumulate_attention=self.accumulate_attention,
            use_positional_encoding=self.use_positional_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x):
        if self.use_kb and 'kb' in self.additional_data:
            to_add = self.additional_data['kb']
            to_add_mask = self.additional_data['kb_mask']

            if 'context' in x:
                for idx in range(len(x['context'])):
                    x['context'][idx] = np.append(x['context'][idx], to_add)
                    x['context_mask'][idx] = np.append(x['context_mask'][idx], to_add_mask)
            else:
                x['context'] = [to_add for _ in range(len(x['text']))]
                x['context_mask'] = [to_add_mask for _ in range(len(x['text']))]

        if self.partial_supervision:
            targets = x['targets']
            query = x['text']
            x['targets'] = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            x['positive_indexes'] = positive_indexes
            x['negative_indexes'] = negative_indexes
            x['mask_indexes'] = mask_indexes

        return x


class Basic_BERT_Memn2n(Basic_Memn2n):

    def __init__(self, embedding_mode='sentence', **kwargs):
        super(Basic_BERT_Memn2n, self).__init__(**kwargs)
        self.embedding_mode = embedding_mode

    def build_model(self, text_info):

        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self.model = M_BERT_Basic_Memn2n(
            embedding_mode=self.embedding_mode,
            vocab_size=self.vocab_size,
            hops=self.hops,
            response_linearity=self.response_linearity,
            share_memory=self.share_memory,
            answer_weights=self.answer_weights,
            dropout_rate=self.dropout_rate,
            l2_regularization=self.l2_regularization,
            similarity_weights=self.similarity_weights,
            attention_operation=self.attention_operation,
            partial_supervision=self.partial_supervision,
            supervision_margin=self.supervision_margin,
            padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None,
            accumulate_attention=self.accumulate_attention,
            use_positional_encoding=self.use_positional_encoding)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x):
        if self.use_kb and 'parsed_kb' in self.additional_data:
            to_add = self.additional_data['parsed_kb']
            to_add_mask = self.additional_data['kb_mask']
            to_add_ids = self.additional_data['kb_segment_ids']

            if 'context' in x:
                for idx in range(len(x['context'])):
                    x['context'][idx] = np.append(x['context'][idx], to_add)
                    x['context_mask'][idx] = np.append(x['context_mask'][idx], to_add_mask)
                    x['context_segment_ids'][idx] = np.append(x['context_segment_ids'][idx], to_add_ids)
            else:
                x['context'] = np.array([to_add for _ in range(len(x['text']))], dtype=np.int32)
                x['context_mask'] = np.array([to_add_mask for _ in range(len(x['text']))], dtype=np.int32)
                x['context_segment_ids'] = np.array([to_add_ids for _ in range(len(x['text']))], dtype=np.int32)

        if self.partial_supervision:
            targets = x['targets']
            query = x['text']
            x['targets'] = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            x['positive_indexes'] = positive_indexes
            x['negative_indexes'] = negative_indexes
            x['mask_indexes'] = mask_indexes

        return x


class Word_Memn2n(Basic_Memn2n):
    """
    Advanced version of the end-to-end memory network.
    The model architecture is defined as follows:
        1. Memory Lookup: one Memory Lookup layer per hop
        2. Memory Reasoning: encoder-decoder with co-attention per hop
        3. Memory reasoning is done at each hop in order to build the next query.
    """

    def __init__(self, enc_units=32, dec_units=32, att_units=32, **kwargs):
        super(Word_Memn2n, self).__init__(**kwargs)
        self.enc_units = enc_units
        self.dec_units = dec_units
        self.att_units = att_units

    def build_model(self, text_info):
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Word_Memn2n(query_size=self.query_size,
                                   sentence_size=self.sentence_size,
                                   vocab_size=self.vocab_size,
                                   embedding_dimension=self.embedding_dimension,
                                   hops=self.hops,
                                   share_memory=self.share_memory,
                                   answer_weights=self.answer_weights,
                                   dropout_rate=self.dropout_rate,
                                   l2_regularization=self.l2_regularization,
                                   embedding_matrix=text_info['embedding_matrix'],
                                   enc_units=self.enc_units,
                                   dec_units=self.dec_units,
                                   att_units=self.att_units,
                                   similarity_weights=self.similarity_weights,
                                   attention_operation=self.attention_operation,
                                   partial_supervision=self.partial_supervision,
                                   supervision_margin=self.supervision_margin,
                                   padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None,
                                   accumulate_attention=self.accumulate_attention)

        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)


class Bow_Memn2n(Basic_Memn2n):
    """
    Bag-of-Words Basic Memory Network variant. The only difference is that embedding layers are not required.
    """

    def build_model(self, text_info):
        self.memory_size = text_info['memory_max_length']

        self.model = M_Bow_Memn2n(hops=self.hops,
                                  response_linearity=self.response_linearity,
                                  share_memory=self.share_memory,
                                  answer_weights=self.answer_weights,
                                  dropout_rate=self.dropout_rate,
                                  l2_regularization=self.l2_regularization,
                                  similarity_weights=self.similarity_weights,
                                  attention_operation=self.attention_operation,
                                  partial_supervision=self.partial_supervision,
                                  supervision_margin=self.supervision_margin,
                                  padding_amount=self.padding_amount if hasattr(self, 'padding_amount') else None,
                                  accumulate_attention=self.accumulate_attention)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x):
        if self.use_kb and 'kb' in self.additional_data:
            to_add = pad_sequences(self.additional_data['kb'], padding='post', dtype=np.float32)
            if 'context' in x:
                for idx in range(len(x['context'])):
                    x['context'][idx] = np.append(x['context'][idx], to_add)
            else:
                x['context'] = np.array([to_add for _ in range(len(x['text']))], dtype=np.float32)

        if self.partial_supervision:
            targets = x['targets']
            query = x['text']
            x['targets'] = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            x['positive_indexes'] = positive_indexes
            x['negative_indexes'] = negative_indexes
            x['mask_indexes'] = mask_indexes

        return x


class Transformer(Network):
    """
    Implementation on transformer for unfairness detection
    """

    def __init__(self, num_layers, num_heads, dff, optimizer_args, use_kb=True, add_gradient_noise=True,
                 l2_regularization=None, clip_gradient=False, max_grad_norm=40.0, additional_data=None,
                 answer_weights=[], dropout_rate=0.1, accumulate_attention=False, **kwargs):
        super(Transformer, self).__init__(**kwargs)
        self.num_layers = num_layers
        self.num_heads = num_heads
        self.dff = dff
        self.optimizer_args = optimizer_args
        self.use_kb = use_kb
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.clip_gradient = clip_gradient
        self.max_grad_norm = max_grad_norm
        self.additional_data = additional_data
        self.answer_weights = answer_weights
        self.dropout_rate = dropout_rate
        self.accumulate_attention = accumulate_attention

    def build_model(self, text_info):
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Transformer(num_layers=self.num_layers,
                                   embedding_dimension=self.embedding_dimension,
                                   num_heads=self.num_heads,
                                   dff=self.dff,
                                   vocab_size=self.vocab_size,
                                   dropout_rate=self.dropout_rate,
                                   accumulate_attention=self.accumulate_attention,
                                   answer_weights=self.answer_weights,
                                   l2_regularization=self.l2_regularization,
                                   sentence_size=self.sentence_size,
                                   query_size=self.query_size,
                                   embedding_matrix=text_info['embedding_matrix'])

        learning_rate = TransformerSchedule(self.embedding_dimension)
        self.optimizer_args['learning_rate'] = learning_rate
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x, y=None):

        if self.use_kb and 'kb' in self.additional_data:
            concat_kb = [item for seq in self.additional_data['kb'] for item in seq]
            concat_kb = pad_sequences([concat_kb], padding='post')
            concat_kb = np.reshape(concat_kb, newshape=(1, concat_kb.shape[1]))

            if 'context' in x:
                raise NotImplementedError()
            else:
                x['context'] = np.repeat(concat_kb, repeats=len(x['text']), axis=0)

                # Build kb mask
                kb_mask = x['context'] == 0
                kb_mask = kb_mask.reshape(kb_mask.shape[0], 1, 1, kb_mask.shape[1]).astype(np.float32)
                x['context_mask'] = kb_mask

                # Compute sentence-wise mask: self-attention should restricted to tokens of the same sentence only
                # Since KB sentences are independent.
                sentence_wise_mask = np.ones(shape=[self.sentence_size, self.sentence_size])

                start_idx = 0
                for sentence in self.additional_data['kb']:
                    current_triu = np.zeros(shape=[len(sentence), len(sentence)])
                    current_triu[np.triu_indices(len(sentence), 1)] = 1
                    sentence_wise_mask[start_idx:current_triu.shape[0] + start_idx,
                    start_idx:current_triu.shape[1] + start_idx] = current_triu

                    start_idx += len(sentence)

                # sentence_wise_mask = np.reshape(sentence_wise_mask, (1, 1,) + sentence_wise_mask.shape)
                # sentence_wise_mask = np.repeat(sentence_wise_mask, repeats=kb_mask.shape[0], axis=0)
                combined_mask = np.maximum(sentence_wise_mask, kb_mask)
                x['context_combined_mask'] = combined_mask

        return x

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        return total_loss, loss_info

    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class ParallelDLU(Network):

    def __init__(self, optimizer_args, key_vocab, max_grad_norm=40.0, dropout_rate=0.2,
                 clip_gradient=False, add_gradient_noise=True, l2_regularization=None,
                 answer_weights=[], additional_data=None, use_positional_encoding=False,
                 use_kb=True, **kwargs):
        super(ParallelDLU, self).__init__(**kwargs)
        self.optimizer_args = optimizer_args
        self.key_vocab = key_vocab
        self.max_grad_norm = max_grad_norm
        self.dropout_rate = dropout_rate
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.answer_weights = answer_weights
        self.additional_data = additional_data
        self.use_kb = use_kb
        self.use_positional_encoding = use_positional_encoding

    def build_model(self, text_info):
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']
        self.max_logic_groups = text_info['max_logic_groups']
        self.max_logic_depth = text_info['max_logic_depth']

        self.model = M_ParallelDLU(
            query_size=self.query_size,
            sentence_size=self.sentence_size,
            vocab_size=self.vocab_size,
            embedding_dimension=self.embedding_dimension,
            answer_weights=self.answer_weights,
            l2_regularization=self.l2_regularization,
            embedding_matrix=text_info['embedding_matrix'],
            use_positional_encoding=self.use_positional_encoding,
            max_logic_groups=self.max_logic_groups,
            max_logic_depth=self.max_logic_depth,
            key_vocab=self.key_vocab
        )

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        # TODO: Grounding loss

        # TODO: Unification strong supervision

        # TODO: Partial supervision

        return total_loss, loss_info

    # TODO: use_kb flag should come from converter (right now it is redundant)
    def _parse_input(self, x):

        # compute query lengths
        x['text_lengths'] = [np.count_nonzero(item) for item in x['text']]

        if self.use_kb and 'kb' in self.additional_data:
            to_add = self.additional_data['kb']
            to_add_logic_masks = self.additional_data['kb_logic_masks']
            to_add_logic_operators = self.additional_data['kb_logic_operators']
            to_add_unification_masks = self.additional_data['kb_unification_masks']

            # compute kb lengths
            to_add_lengths = [len(item) for item in to_add]

            # pad
            to_add = pad_sequences(to_add, padding='post')

            if 'context' in x:
                raise NotImplementedError()
            else:
                x['context'] = np.array([to_add for _ in range(len(x['text']))], dtype=np.int32)
                x['logic_group_masks'] = np.repeat(np.reshape(to_add_logic_masks, (1,) + to_add_logic_masks.shape),
                                                   repeats=len(x['text']),
                                                   axis=0)
                x['logic_operator_gates'] = np.repeat(
                    np.reshape(to_add_logic_operators, (1,) + to_add_logic_operators.shape),
                    repeats=len(x['text']),
                    axis=0)
                x['context_unification_masks'] = np.repeat(
                    np.reshape(to_add_unification_masks, (1,) + to_add_unification_masks.shape),
                    repeats=len(x['text']),
                    axis=0)

                x['context_segments'] = np.array([[idx for _ in range(len(to_add))] for idx in range(len(x['text']))],
                                                 dtype=np.int32)
                x['context_lengths'] = np.array([to_add_lengths for _ in range(len(x['text']))], dtype=np.int32)

        return x

    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class SemanticIteratedTransformer(Network):

    def __init__(self, mha_layers, mha_heads, mha_dff, optimizer_args, segments_amount,
                 use_kb=True, hops=1, reduction_weights=None, share_memory=True, pooling_att_dimension=256,
                 answer_weights=[], dropout_rate=0.2, l2_regularization=0., pooling_coefficient=1.,
                 partial_supervision=None, accumulate_attention=False, use_positional_encoding=False,
                 max_grad_norm=40., clip_gradient=True, additional_data=None,
                 concatenate_context=True, **kwargs):
        super(SemanticIteratedTransformer, self).__init__(**kwargs)

        self.hops = hops
        self.optimizer_args = optimizer_args
        self.share_memory = share_memory
        self.dropout_rate = dropout_rate
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.answer_weights = answer_weights
        self.additional_data = additional_data
        self.use_kb = use_kb
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.mha_layers = mha_layers
        self.mha_heads = mha_heads
        self.mha_dff = mha_dff
        self.optimizer_args = optimizer_args
        self.use_kb = use_kb
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.clip_gradient = clip_gradient
        self.max_grad_norm = max_grad_norm
        self.additional_data = additional_data
        self.answer_weights = answer_weights
        self.dropout_rate = dropout_rate
        self.accumulate_attention = accumulate_attention
        self.reduction_weights = reduction_weights
        self.concatenate_context = concatenate_context
        self.segments_amount = segments_amount
        self.pooling_att_dimension = pooling_att_dimension
        self.pooling_coefficient = pooling_coefficient

        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
            self.partial_supervision_coeff = partial_supervision[1]
            self.supervision_margin = partial_supervision[2]
        else:
            self.partial_supervision = False
            self.partial_supervision_coeff = 1
            self.supervision_margin = 0.1

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self.model = M_SemanticIteratedTransformer(query_size=self.query_size,
                                                   sentence_size=self.sentence_size,
                                                   mha_layers=self.mha_layers,
                                                   mha_heads=self.mha_heads,
                                                   mha_dff=self.mha_dff,
                                                   vocab_size=self.vocab_size,
                                                   embedding_dimension=self.embedding_dimension,
                                                   hops=self.hops,
                                                   reduction_weights=self.reduction_weights,
                                                   share_memory=self.share_memory,
                                                   answer_weights=self.answer_weights,
                                                   dropout_rate=self.dropout_rate,
                                                   l2_regularization=self.l2_regularization,
                                                   embedding_matrix=text_info['embedding_matrix'],
                                                   partial_supervision=self.partial_supervision,
                                                   supervision_margin=self.supervision_margin,
                                                   accumulate_attention=self.accumulate_attention,
                                                   use_positional_encoding=self.use_positional_encoding,
                                                   segments_amount=self.segments_amount,
                                                   pooling_att_dimension=self.pooling_att_dimension)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):

        # [batch_size * memory_size, ]
        logits = self.model(x, training=True)
        logits = tf.reshape(logits, [-1, self.memory_size])
        logits = tf.reduce_sum(logits, axis=1)

        # [batch_size, 1]
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')

        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        pooling_regularization = self.model.pooling_regularization
        pooling_regularization = tf.reduce_sum(self.model.pooling_regularization) * self.pooling_coefficient
        total_loss += pooling_regularization
        loss_info['pooling_regularization'] = pooling_regularization

        # TODO: model strong supervision as a series of binary cross entropies (0 or 1)

        return total_loss, loss_info

    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_input(self, x, y=None):

        if self.use_kb and 'kb' in self.additional_data:
            kb = self.additional_data['kb']
            kb = pad_sequences(kb, padding='post', maxlen=self.sentence_size)
            kb = np.reshape(kb, newshape=(1, kb.shape[0], kb.shape[1]))

            if 'context' in x:
                raise NotImplementedError()
            else:
                x['context'] = np.repeat(kb, repeats=len(x['text']), axis=0)

                # Build kb mask
                # [batch_size * memory_size, 1, 1, sentence_size]
                kb_mask = x['context'] == 0
                kb_mask = kb_mask.reshape(-1, 1, 1, self.sentence_size).astype(np.float32)
                x['context_mask'] = kb_mask

        return x

    def _parse_predictions(self, predictions):

        # [batch_size, memory_size]
        predictions = tf.reshape(predictions, [-1, self.memory_size])
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        predictions = tf.cast(predictions, tf.bool)
        predictions = tf.reduce_any(predictions, axis=1)
        predictions = tf.cast(predictions, tf.float32)
        return predictions


class SemanticCoAttention(Network):

    def __init__(self, optimizer_args, clause_segments_amount, explanation_segments_amount, use_kb=True,
                 pooling_use_masking=False, pooling_att_dimension=128,
                 additive_pooling_dimension=512, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 pooling_coefficient=1., partial_supervision=None, accumulate_attention=False,
                 use_positional_encoding=False, max_grad_norm=40., clip_gradient=True, additional_data=None,
                 **kwargs):
        super(SemanticCoAttention, self).__init__(**kwargs)

        self.clause_segments_amount = clause_segments_amount
        self.explanation_segments_amount = explanation_segments_amount
        self.pooling_use_masking = pooling_use_masking
        self.pooling_att_dimension = pooling_att_dimension
        self.additive_pooling_dimension = additive_pooling_dimension
        self.optimizer_args = optimizer_args
        self.dropout_rate = dropout_rate
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.answer_weights = answer_weights
        self.additional_data = additional_data
        self.use_kb = use_kb
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.optimizer_args = optimizer_args
        self.use_kb = use_kb
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.clip_gradient = clip_gradient
        self.max_grad_norm = max_grad_norm
        self.additional_data = additional_data
        self.answer_weights = answer_weights
        self.dropout_rate = dropout_rate
        self.accumulate_attention = accumulate_attention
        self.pooling_coefficient = pooling_coefficient

        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
            self.partial_supervision_coeff = partial_supervision[1]
            self.supervision_margin = partial_supervision[2]
        else:
            self.partial_supervision = False
            self.partial_supervision_coeff = 1
            self.supervision_margin = 0.1

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self.model = M_SemanticCoAttention(query_size=self.query_size,
                                           sentence_size=self.sentence_size,
                                           clause_segments_amount=self.clause_segments_amount,
                                           explanation_segments_amount=self.explanation_segments_amount,
                                           vocab_size=self.vocab_size,
                                           embedding_dimension=self.embedding_dimension,
                                           answer_weights=self.answer_weights,
                                           dropout_rate=self.dropout_rate,
                                           l2_regularization=self.l2_regularization,
                                           embedding_matrix=text_info['embedding_matrix'],
                                           accumulate_attention=self.accumulate_attention,
                                           use_positional_encoding=self.use_positional_encoding,
                                           pooling_use_masking=self.pooling_use_masking,
                                           pooling_att_dimension=self.pooling_att_dimension)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):

        # [batch_size, ]
        logits = self.model(x, training=True)
        # logits = tf.reshape(logits, [-1, self.memory_size])
        # logits = tf.reduce_sum(logits, axis=1)

        # [batch_size, 1]
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')

        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        pooling_regularization = self.model.pooling_regularization
        pooling_regularization = tf.reduce_mean(pooling_regularization) * self.pooling_coefficient
        total_loss += pooling_regularization
        loss_info['pooling_regularization'] = pooling_regularization

        # TODO: model strong supervision as a series of binary cross entropies (0 or 1)

        return total_loss, loss_info

    def train_op(self, x, y):
        with tf.GradientTape() as tape:
            loss, loss_info = self.loss_op(x, y, training=True)
        grads = tape.gradient(loss, self.model.trainable_variables)
        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v)
                              for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in zip(grads, self.model.trainable_variables)]
            grads = [item[0] for item in grads_and_vars]
        return loss, loss_info, grads

    def _parse_input(self, x, y=None):

        if self.use_kb and 'kb' in self.additional_data:
            kb = self.additional_data['kb']
            kb = pad_sequences(kb, padding='post', maxlen=self.sentence_size)
            kb = np.reshape(kb, newshape=(1, kb.shape[0], kb.shape[1]))

            if 'context' in x:
                raise NotImplementedError()
            else:
                x['context'] = np.repeat(kb, repeats=len(x['text']), axis=0)

                # Build kb mask
                # [batch_size * memory_size, 1, 1, sentence_size]
                kb_mask = x['context'] == 0
                kb_mask = kb_mask.reshape(-1, 1, 1, self.sentence_size).astype(np.float32)
                x['context_mask'] = kb_mask

        return x

    def _parse_predictions(self, predictions):

        # [batch_size, memory_size]
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


# Baselines #

class Baseline_LSTM(Network):

    def __init__(self, lstm_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_LSTM, self).__init__(**kwargs)
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def build_model(self, text_info):
        self.sentence_size = text_info['padding_max_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Baseline_LSTM(sentence_size=self.sentence_size,
                                     vocab_size=self.vocab_size,
                                     lstm_weights=self.lstm_weights,
                                     answer_weights=self.answer_weights,
                                     embedding_dimension=self.embedding_dimension,
                                     l2_regularization=self.l2_regularization,
                                     dropout_rate=self.dropout_rate,
                                     embedding_matrix=text_info['embedding_matrix'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.cast(targets, logits.dtype)
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, {'cross_entropy': cross_entropy,
                            'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class Baseline_CNN(Network):

    def __init__(self, cnn_weights, answer_weights,
                 optimizer_args=None, l2_regularization=None, dropout_rate=0.2,
                 additional_data=None, **kwargs):
        super(Baseline_CNN, self).__init__(**kwargs)
        self.cnn_weights = cnn_weights
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def build_model(self, text_info):
        self.sentence_size = text_info['padding_max_length']
        self.vocab_size = text_info['vocab_size']

        self.model = M_Baseline_CNN(sentence_size=self.sentence_size,
                                    vocab_size=self.vocab_size,
                                    cnn_weights=self.cnn_weights,
                                    answer_weights=self.answer_weights,
                                    embedding_dimension=self.embedding_dimension,
                                    l2_regularization=self.l2_regularization,
                                    dropout_rate=self.dropout_rate,
                                    embedding_matrix=text_info['embedding_matrix'])

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)

        additional_losses = tf.reduce_sum(self.model.losses)

        total_loss = cross_entropy + additional_losses

        return total_loss, {'cross_entropy': cross_entropy,
                            'l2_regularization': additional_losses}

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class ALBERTTextClassifier(Network):

    def __init__(self, optimizer, optimizer_args, embedding_mode='sentence',
                 bert_model='https://tfhub.dev/google/albert_base/2',
                 embedding_dimension=768,
                 answer_weights=(),
                 l2_regularization=None,
                 dropout_rate=0.2,
                 additional_data=None,
                 **kwargs):
        super(ALBERTTextClassifier, self).__init__(embedding_dimension=embedding_dimension, **kwargs)
        self.embedding_mode = embedding_mode
        self.bert_model = bert_model
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.optimizer = optimizer
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data

    def build_model(self, text_info):
        self.vocab_size = text_info['vocab_size']

        self.model = M_ALBERTTextClassifier(embedding_mode=self.embedding_mode,
                                            albert_model=self.bert_model,
                                            answer_weights=self.answer_weights,
                                            l2_regularization=self.l2_regularization,
                                            dropout_rate=self.dropout_rate)

        # TODO: build optimizer instance from args
        self.optimizer = tf.keras.optimizers.Adam(**self.optimizer_args)

    def _parse_input(self, x, y=None):
        x['text'] = pad_sequences(x['text'], padding='post')
        x['text_mask'] = pad_sequences(x['text_mask'], padding='post', dtype=np.int32)
        x['text_segment_ids'] = pad_sequences(x['text_segment_ids'], padding='post', dtype=np.int32)

        return x

    def loss_op(self, x, targets, training=False):
        logits = self.model(x, training=True)
        targets = tf.convert_to_tensor(targets, dtype=logits.dtype)
        targets = tf.reshape(targets, logits.shape)

        # Cross entropy
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logits,
                                                                 labels=targets,
                                                                 pos_weight=self.pos_weight,
                                                                 name='cross_entropy')
        cross_entropy = tf.reduce_mean(cross_entropy)
        total_loss = cross_entropy
        loss_info = {
            'cross_entropy': cross_entropy
        }

        # L2 regularization
        if self.model.losses:
            additional_losses = tf.reduce_sum(self.model.losses)
            total_loss += additional_losses
            loss_info['l2_regularization'] = additional_losses

        return total_loss, loss_info

    def _parse_predictions(self, predictions):
        predictions = tf.nn.sigmoid(predictions)
        predictions = tf.round(predictions)
        return predictions


class ModelFactory(object):
    supported_models = {
        'basic_memn2n_v2': Basic_Memn2n,
        'gated_memn2n_v2': Gated_Memn2n,
        'discriminative_memn2n_v2': Discriminative_Memn2n,
        'entailment_memn2n_v2': Entailment_Memn2n,
        'word_memn2n_v2': Word_Memn2n,
        'bow_memn2n_v2': Bow_Memn2n,
        'baseline_lstm_v2': Baseline_LSTM,
        'baseline_cnn_v2': Baseline_CNN,
        'albert_text_classifier_v2': ALBERTTextClassifier,
        'transformer_v2': Transformer,
        'basic_elmo_memn2n_v2': Basic_ELMo_Memn2n,
        'basic_bert_memn2n_v2': Basic_BERT_Memn2n,
        'parallel_dlu': ParallelDLU,
        'semantic_iterated_transformer_v2': SemanticIteratedTransformer,
        "semantic_coattention_v2": SemanticCoAttention
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ModelFactory.supported_models[key]:
            return ModelFactory.supported_models[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
