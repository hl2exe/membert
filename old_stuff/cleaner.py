"""


"""

import string
from functools import reduce

from nltk import word_tokenize

special_words = [
    "-lrb-",
    "-rrb-",
    "i.e.",
    "``",
    "\'\'",
    "lrb",
    "rrb"
]


# TODO: documentation
def punctuation_filtering(line):
    """
    Filters given sentences by removing punctuation

    :return:
    """

    table = str.maketrans('', '', string.punctuation)
    trans = [w.translate(table) for w in line.split()]

    return ' '.join([w for w in trans if w != ''])


def remove_special_words(line):
    words = word_tokenize(line)
    filtered = []
    for w in words:
        if w not in special_words:
            filtered.append(w)

    line = ' '.join(filtered)
    return line


# TODO: documentation
def number_replacing_with_constant(line):
    words = word_tokenize(line)
    filtered = []
    for w in words:
        try:
            int(w)
            filtered.append('SPECIALNUMBER')
        except ValueError:
            filtered.append(w)
            continue

    line = ' '.join(filtered)
    return line
    # return re.sub('[0-9][0-9.,-]*', 'SPECIALNUMBER', line)


filter_methods = {
    'punctuation_filtering': punctuation_filtering,
    'number_replacing_with_constant': number_replacing_with_constant,
    'remove_special_words': remove_special_words
}


# TODO: documentation
def filter_line(line, function_names=None):
    """
    General filtering proxy function that applies a sub-set of the supported
    filtering methods to given sentences.
    """

    if function_names is None:
        function_names = list(filter_methods.keys())

    functions = [filter_methods[name] for name in function_names]
    return reduce(lambda r, f: f(r), functions, line.strip().lower())
