"""

@Author: Federico Ruggeri

@Date: 08/02/2019

@Description:

Testing MemN2N network where the query is the sentence to classify and the memory is:

A) A knowledge base (KB) containing a set of rules giving detals about each category of unfairness

B) A knowledge base (KB) containing unfair clauses (different from the given query) directly retrieved from
  an additional validation set

"""

import os
from collections import OrderedDict
from datetime import datetime

import numpy as np
from keras import backend as K
from keras.callbacks import EarlyStopping
from sklearn.metrics import f1_score

import const_define as cd
import reporters
from converter import KBMemoryConverter
from data_loader import Task1SingleKBLoader
from nn_models_v1 import MemN2N_tf
from preprocessing import KBMemoryPreprocessor
from splitter import GeneralSplitter
from utility.json_utils import load_json
from utility.json_utils import save_json
from utility.log_utils import get_logger

logger = get_logger(__name__)

if __name__ == '__main__':

    # Step 1: Load dataset
    data_base_path = os.path.join(cd.TOS_100_DIR, 'sentences')
    labels_base_path = os.path.join(cd.TOS_100_DIR, 'labels')

    task1_loader = Task1SingleKBLoader()

    key = 'CH'
    data_handle = task1_loader.load(data_base_path=data_base_path,
                                    labels_base_path=labels_base_path,
                                    category=key)

    # Step 2: prepare for Leave One Out test (LOO)

    # Callbacks: Early stopper
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])

    # If you want to use validation loss as stopping criteria:
    # Replace 'f1_score' with 'val_loss' and set monitor_op = np.less
    early_stopper.monitor = 'f1_score'
    early_stopper.monitor_op = np.greater

    # Loading network configuration -> check model_config.json
    network_args = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))['unfair_memn2n_tf']

    # Build the data converter: from sentences (text) to bag of words
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}

    converter = KBMemoryConverter(**converter_args)

    # Load embedding model
    converter.load_embedding_model()

    # Validation percentage for splitting
    total_validation_percentage = 0.1

    # Loading training settings -> check training_config.json
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Retrieve model specific parameters (for model definition)
    network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                              if 'model_class' in value['flags']}

    # Retrieve LOO documents list
    dataset_list = list(data_handle.input_data.keys())
    dataset_list_len = len(dataset_list)

    # Build the preprocessor: re-arranges sentences (text) according to a certain criteria
    preprocessor_args = {key: value['value'] for key, value in network_args.items()
                         if 'preprocessing' in value['flags']}
    preprocessor = KBMemoryPreprocessor(**preprocessor_args)

    # Build  the splitter
    splitter_args = {key: value['value'] for key, value in network_args.items()
                     if 'splitter' in value['flags']}
    splitter_args['validation_split'] = total_validation_percentage
    splitter = GeneralSplitter(**splitter_args)

    # Step 3: LOO test
    loo_results = OrderedDict()
    all_labels = OrderedDict()
    all_preds = OrderedDict()

    for idx, test_doc in enumerate(dataset_list):
        print('[{0}/{1}] Excluding: {2}'.format(idx + 1, dataset_list_len, test_doc))

        # Get fold train/test data
        x_train, y_train, x_test, y_test = data_handle.get_loo_split(test_doc=test_doc)

        # Pre-process fold data
        x_train, y_train = preprocessor.parse(x=x_train, y=y_train, additional_info=data_handle.get_additional_info())
        x_test, y_test = preprocessor.parse(x=x_test, y=y_test, additional_info=data_handle.get_additional_info())

        # Convert fold data

        vocab_size, \
        embedding_matrix, \
        text_info = converter.retrieve_data_info(data=x_train)

        x_train, y_train = converter.convert_data(x=x_train, y=y_train, text_info=text_info)
        x_test, y_test = converter.convert_data(x=x_test, y=y_test, text_info=text_info)

        # Split train fold data into train/validation

        splitter.validation_split = total_validation_percentage
        x_train, \
        y_train, \
        x_val, \
        y_val = splitter.split(x=x_train, y=y_train)

        # Build the network model

        network = MemN2N_tf(**network_retrieved_args)

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

        text_info['memory_max_length'] = x_train[0][0].shape[0]

        logger.info('Vocab size: {}'.format(vocab_size))
        for key in text_info:
            logger.info('{0} size: {1}'.format(key, text_info[key]))

        network.build_model(vocab_size=vocab_size, text_info=text_info,
                            embedding_matrix=embedding_matrix)

        # Training

        network.fit(x=x_train, y=y_train,
                    callbacks=[early_stopper],
                    validation_data=(x_val, y_val),
                    **training_config)

        # Predictions

        val_predictions = network.predict(x=x_val,
                                          batch_size=training_config['batch_size'],
                                          verbose=0).ravel()

        train_predictions = network.predict(x=x_train,
                                            batch_size=training_config['batch_size'],
                                            verbose=0).ravel()

        test_predictions = network.predict(x=x_test,
                                           batch_size=training_config['batch_size']).ravel()

        # Storing results

        train_f1 = f1_score(y_true=y_train, y_pred=train_predictions, average='binary', pos_label=1)
        val_f1 = f1_score(y_true=y_val, y_pred=val_predictions, average='binary', pos_label=1)
        test_f1 = f1_score(y_true=y_test, y_pred=test_predictions, average='binary', pos_label=1)
        loo_results[test_doc] = [train_f1, val_f1, test_f1]

        all_labels[test_doc] = y_test
        all_preds[test_doc] = test_predictions.ravel()

        logger.info('Fold results: {}'.format(loo_results[test_doc]))

        # Flush
        K.clear_session()

    # Macro F1
    results = list(loo_results.values())
    logger.info('Macro f1: {}'.format(np.mean(results, axis=0)))

    # Micro F1
    all_labels = np.hstack(all_labels.values())
    all_preds = np.hstack(all_preds.values())

    all_f1 = f1_score(y_true=all_labels, y_pred=all_preds, average='binary', pos_label=1)
    logger.info('Micro F1: {}'.format(all_f1))

    # Saving data
    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    base_path = os.path.join(cd.PROJECT_DIR, 'loo_test_{}'.format(key.lower()),
                             'unfair_memn2n_tf', current_date)

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    save_path = os.path.join(base_path, 'scores.json')
    save_json(filepath=save_path, data=loo_results)

    reporters.save_model_info(folder=base_path, config_data=network_args)
    reporters.save_training_info(folder=base_path, training_info=training_config)
