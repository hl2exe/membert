"""

@Author: Federico Ruggeri

@Date: 14/01/2019

"""

import os

import const_define as cd
from utility.json_utils import load_json
import numpy as np


def get_scores(path, metrics):
    score_path = os.path.join(path, 'scores.json')
    score_data = load_json(score_path)

    val_scores = []
    test_scores = []
    for metric in metrics:
        val_scores.append(np.mean(score_data[0][metric]))
        test_scores.append(np.mean(score_data[1][metric]))

    return tuple(val_scores + test_scores)


if __name__ == '__main__':

    all_scores = []
    metrics = ['f1_score', 'recall_score', 'precision_score']

    # Loop through network models
    for model_name in os.listdir(cd.LOO_DIR):
        model_path = os.path.join(cd.LOO_DIR, model_name)
        if os.path.isdir(model_path):
            # Loop through LOO tests
            for test_name in os.listdir(model_path):
                test_path = os.path.join(model_path, test_name)
                if os.path.isdir(test_path):
                    scores = get_scores(test_path, metrics=metrics)
                    key = '{0}--{1}'.format(model_name, test_name)
                    all_scores.append((key,) + scores)

    # Sort scores (test data)
    sort_index = input('Sorting by?\n1. F1 score\n2. Recall\n3. Precision\nChoice: ')

    all_scores = sorted(all_scores, key=lambda t: t[int(sort_index) + len(metrics)])

    # Printing scores
    for score_data in all_scores:
        model_name = score_data[0]
        val_text = '\t'.join(['Validation {0}: {1}'.format(key, value)
                              for key, value in zip(metrics, score_data[1:len(metrics) + 1])])
        test_text = '\t'.join(['Test {0}: {1}'.format(key, value)
                              for key, value in zip(metrics, score_data[1 + len(metrics):])])
        print('Model name: {0}\t {1}\t {2}'.format(model_name, val_text, test_text))
