"""

@Author: Federico Ruggeri

@Date: 25/09/18

"""

# Limiting GPU access
import tensorflow as tf
from keras import backend as k

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.6
k.set_session(tf.Session(config=config))
#####

from keras import regularizers
from keras.initializers import Constant
from keras import optimizers
from utility.log_utils import get_logger
from keras.layers import Embedding, Dropout, LSTM, Dense, Conv1D, MaxPooling1D
from keras.models import Sequential
import tensorflow as tf
from keras import backend as K
import numpy as np
from keras.layers import Bidirectional, GRU
from utility.model_utils import position_encoder_extra, position_encoding_intra
from utility.model_utils import add_gradient_noise, zero_nil_slot
import math
from tensorflow.contrib.sparsemax import sparsemax
from keras.models import load_model
from tensorflow.contrib.layers import xavier_initializer
from utility.evaluation_utils import build_metrics, compute_metrics
from utility.python_utils import merge
from utility.embedding_utils import pad_data
from utility.model_utils import batch_norm_relu
from tensorflow.contrib.layers import fully_connected
from tensorflow.contrib.rnn import LSTMCell
from custom_tf import DynamicMemoryCell
import custom_tf
from functools import reduce

logger = get_logger(__name__)


# TODO: add relation network
# TODO: add recurrent relation network
# TODO: add DNC
# TODO: add EntNet


# TODO: documentation
class Network(object):

    def __init__(self, embedding_dimension, session=None):
        self.model = None
        self.class_weights = None
        self.embedding_dimension = embedding_dimension
        self.pos_weight = None
        self.num_classes = None

        if session is None:
            self.session = K.get_session()
        else:
            self.session = session

    def get_weights(self):
        if self.model is not None:
            return self.model.get_weights()
        else:
            vars = tf.trainable_variables()
            values = self.session.run(vars)
            return values

    def get_named_weights(self):
        if self.model is not None:
            return self.model.get_weights()
        else:
            vars = tf.trainable_variables()
            return vars

    def save(self, filepath, overwrite=True):
        if self.model is not None:
            return self.model.save(filepath=filepath, overwrite=overwrite)
        else:
            saver = tf.train.Saver()
            saver.save(sess=self.session, save_path=filepath)

    def load(self, filepath):
        if self.model is not None:
            return load_model(filepath=filepath)
        else:
            saver = tf.train.Saver()
            return saver.restore(sess=self.session, save_path=filepath)

    def set_weights(self, weights):
        if self.model is not None:
            self.model.set_weights(weights)
        else:
            vars = tf.trainable_variables()
            for var, value in zip(vars, weights):
                var.load(value, self.session)

    def get_attentions_weights(self, x, batch_size=32):
        return None

    def compute_output_weights(self, y_train, num_classes):

        self.num_classes = num_classes
        # single label scenario
        if self.model is None:
            if num_classes == 1:
                if len(y_train[y_train == 1]) > 0:
                    self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                else:
                    self.pos_weight = 1
            else:
                self.pos_weight = [len(y_train[y_train[:, cat] == 0]) / len(y_train[y_train[:, cat] == 1])
                                   for cat in range(num_classes)]
        else:
            if num_classes == 1:
                self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                self.class_weights = {0: 1, 1: self.pos_weight}
            else:
                raise RuntimeError('Not implemented yet')

    def build_model(self, text_info):
        raise NotImplementedError()

    def _batch_fit(self, x, y):
        raise NotImplementedError()

    def _batch_predict(self, x):
        raise NotImplementedError()

    def _batch_evaluate(self, x, y):
        raise NotImplementedError()

    def _get_batches(self, data, batch_size=32, num_batches=None):

        return_count = False

        if type(data) is list:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data[0]) / batch_size)

            batches = []
            for item_idx in range(len(data)):
                batches.append([data[item_idx][idx * batch_size: (idx + 1) * batch_size]
                                for idx in range(num_batches)])
        else:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data) / batch_size)
            batches = [data[idx * batch_size: (idx + 1) * batch_size] for idx in range(num_batches)]

        if return_count:
            return batches, num_batches
        else:
            return batches

    def evaluate(self, x=None, y=None, batch_size=32, verbose=1, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            return self.model.evaluate(x=x, y=y, batch_size=batch_size, verbose=verbose, **kwargs)
        else:
            total_loss = {}
            if type(x) is list:
                num_batches = math.ceil(len(x[0]) / batch_size)
            else:
                num_batches = math.ceil(len(x) / batch_size)

            batches_x = self._get_batches(x, batch_size=batch_size, num_batches=num_batches)
            batches_y = self._get_batches(y, batch_size=batch_size, num_batches=num_batches)

            for batch_idx in range(num_batches):

                if type(x) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
                else:
                    batch_x = batches_x[batch_idx]

                if type(y) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
                else:
                    batch_y = batches_y[batch_idx]

                batch_info = self._batch_evaluate(x=batch_x, y=batch_y)

                for key, item in batch_info.items():
                    if not key.startswith('plot'):
                        if key not in total_loss:
                            total_loss[key] = item / batch_size
                        else:
                            total_loss[key] += item / batch_size
                    else:
                        if key not in total_loss:
                            total_loss[key] = item
                        else:
                            if type(item) is list:
                                for el_idx, el in enumerate(item):
                                    total_loss[key][el_idx] = np.concatenate((total_loss[key][el_idx], el))
                            else:
                                last_index = max(total_loss[key])
                                item = item + last_index + 1
                                total_loss[key] = np.concatenate((total_loss[key], item))

            total_loss = {key: item / num_batches if not key.startswith('plot') else item
                          for key, item in total_loss.items()}
            return total_loss

    def _tf_fit(self, x=None, y=None, batch_size=None, epochs=1, verbose=1,
                callbacks=None, validation_data=None, shuffle=True, step_checkpoint=None,
                metrics=None, additional_metrics_info=None):

        self.stop_training = False
        self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            callback.on_train_begin(logs=None)

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
            samples_indexes = np.arange(len(x[0]))
        else:
            num_batches = math.ceil(len(x) / batch_size)
            samples_indexes = np.arange(len(x))

        if verbose:
            logger.info('Start Training! Total batches: {}'.format(num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(num_batches * step_checkpoint)
                logger.info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > num_batches:
                    step_checkpoint = int(num_batches * 0.1)
                    logger.info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        for epoch in range(epochs):

            if self.stop_training:
                break

            if shuffle:
                np.random.shuffle(samples_indexes)

            # TODO: wrap condition
            if type(x) is list:
                temp_x = []
                for item in range(len(x)):
                    if type(x[item]) is np.ndarray:
                        temp_x.append(x[item][samples_indexes])
                    else:
                        temp_x.append([x[item][index] for index in samples_indexes])
                s_x = temp_x
            else:
                s_x = x[samples_indexes]

            if type(y) is list:
                temp_y = []
                for item in range(len(y)):
                    if type(y[item]) is np.ndarray:
                        temp_y.append(y[item][samples_indexes])
                    else:
                        temp_y.append(y[item][index] for index in samples_indexes)
                s_y = temp_y
            else:
                s_y = y[samples_indexes]

            batches_x = self._get_batches(s_x, batch_size=batch_size, num_batches=num_batches)
            batches_y = self._get_batches(s_y, batch_size=batch_size, num_batches=num_batches)

            train_loss = {}

            for batch_idx in range(num_batches):

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                # TODO: wrap condition
                if type(x) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
                else:
                    batch_x = batches_x[batch_idx]

                if type(y) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
                else:
                    batch_y = batches_y[batch_idx]

                batch_info = self._batch_fit(x=batch_x, y=batch_y)

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                # Monitoring at step checkpoint
                # TODO: wrap condition
                if step_checkpoint is not None and batch_idx > 0 and batch_idx % step_checkpoint == 0:
                    if validation_data is not None and metrics is not None:
                        if type(y) is list:
                            val_metrics_str_result = []
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            for idx in range(len(y)):
                                all_val_metrics = compute_metrics(parsed_metrics,
                                                                  true_values=validation_data[1][idx].astype(
                                                                      np.float32),
                                                                  predicted_values=val_predictions[idx].astype(
                                                                      np.float32),
                                                                  additional_metrics_info=additional_metrics_info[idx],
                                                                  suffix=idx)
                                val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])
                                val_metrics_str_result.append(val_metrics_str)
                        else:
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            all_val_metrics = compute_metrics(parsed_metrics,
                                                              true_values=validation_data[1].astype(np.float32),
                                                              predicted_values=val_predictions.astype(np.float32),
                                                              additional_metrics_info=additional_metrics_info)
                            val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                                   for key, value in all_val_metrics.items()])]
                        logger.info('Epoch: {0} -- Step: {1} -- {2}'.format(epoch + 1,
                                                                            batch_idx,
                                                                            ' -- '.join(val_metrics_str_result)))

                for key, item in batch_info.items():
                    if not key.startswith('plot'):
                        if key not in train_loss:
                            train_loss[key] = item / batch_size
                        else:
                            train_loss[key] += item / batch_size
                    else:
                        if key not in train_loss:
                            train_loss[key] = item
                        else:
                            if type(item) is list:
                                for el_idx, el in enumerate(item):
                                    train_loss[key][el_idx] = np.concatenate((train_loss[key][el_idx], el))
                            else:
                                last_index = max(train_loss[key])
                                item = item + last_index + 1
                                train_loss[key] = np.concatenate((train_loss[key], item))

            train_loss = {key: item / num_batches if not key.startswith('plot') else item
                          for key, item in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                val_info = self.evaluate(x=validation_data[0], y=validation_data[1], batch_size=batch_size)

                if metrics is not None:
                    if type(y) is list:
                        val_metrics_str_result = []
                        all_val_metrics = {}
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        for idx in range(len(y)):
                            val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1][idx].astype(np.float32),
                                                          predicted_values=val_predictions[idx].astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info[idx],
                                                          suffix=idx)
                            val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in val_metrics.items()])
                            val_metrics_str_result.append(val_metrics_str)
                            merge(all_val_metrics, val_metrics)
                    else:
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        all_val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1].astype(np.float32),
                                                          predicted_values=val_predictions.astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info)
                        val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])]
                    logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                            train_loss[
                                                                                                                'train_loss'],
                                                                                                            val_info[
                                                                                                                'val_loss'],
                                                                                                            ' -- '.join(
                                                                                                                val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                            train_loss['train_loss'],
                                                                                            val_info['val_loss']))

            for callback in callbacks:
                callback_args = train_loss
                callback_args['train_x'] = x
                callback_args['train_y'] = y
                callback_args['batch_size'] = batch_size
                callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs=None)

    def _tf_predict(self, x, batch_size=32, verbose=0):
        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
        else:
            num_batches = math.ceil(len(x) / batch_size)

        batches_x = self._get_batches(x, batch_size=batch_size, num_batches=num_batches)
        total_preds = []

        for batch_idx in range(num_batches):
            if type(x) is list:
                preds = self._batch_predict(x=[batches_x[item][batch_idx] for item in range(len(x))])
            else:
                preds = self._batch_predict(x=batches_x[batch_idx])

            if type(preds) is list:
                if batch_idx == 0:
                    total_preds.extend(preds)
                else:
                    for idx, pred in enumerate(preds):
                        total_preds[idx] = np.append(total_preds[idx], pred, axis=0)
            else:
                total_preds.extend(preds)

        return np.array(total_preds)

    def fit(self, x=None, y=None, batch_size=None, epochs=1, verbose=1,
            callbacks=None, validation_data=None, shuffle=True, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            return self.model.fit(x=x, y=y, batch_size=batch_size,
                                  epochs=epochs, verbose=verbose,
                                  callbacks=callbacks, validation_data=validation_data,
                                  shuffle=shuffle,
                                  class_weight=self.class_weights, **kwargs)
        else:
            return self._tf_fit(x=x, y=y, batch_size=batch_size,
                                epochs=epochs, verbose=verbose,
                                callbacks=callbacks,
                                validation_data=validation_data,
                                shuffle=shuffle, **kwargs)

    def predict(self, x, batch_size=32, verbose=0, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            preds = self.model.predict(x=x, batch_size=batch_size, verbose=verbose, **kwargs)
            return self.parse_predictions(predictions=preds)
        else:
            return self._tf_predict(x=x, batch_size=batch_size, verbose=verbose)

    def parse_predictions(self, predictions):
        return np.round(predictions)

    def get_feed_dict(self, x, y=None, phase='train'):
        raise NotImplementedError()

    def set_lr(self, new_lr):
        if self.model is not None:
            K.set_value(self.model.optimizer.lr, new_lr)
        else:
            self.optimizer_args['learning_rate'] = new_lr

    def get_lr(self):
        if self.model is not None:
            return K.get_value(self.model.optimizer.lr)
        else:
            return self.optimizer_args['learning_rate']

    def build_tensorboard_info(self):
        pass


# TODO: documentation
class VanillaLSTM(Network):

    def __init__(self, lstm_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam', optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(VanillaLSTM, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.lstm_neurons = lstm_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension,
                                input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding,
                                mask_zero=True))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension,
                                input_length=padding_max_length,
                                mask_zero=True))

        for neuron_value in self.lstm_neurons[:-1]:
            model.add(LSTM(neuron_value, return_sequences=True,
                           kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(LSTM(self.lstm_neurons[-1], return_sequences=False,
                       kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


# TODO: documentation
class BiGRU(Network):

    def __init__(self, gru_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam', optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(BiGRU, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.gru_neurons = gru_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding,
                                mask_zero=True))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                mask_zero=True))

        for neuron_value in self.gru_neurons[:-1]:
            model.add(Bidirectional(GRU(neuron_value, return_sequences=True,
                                        kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
            model.add(Dropout(self.dropout_rate))

        model.add(Bidirectional(GRU(self.gru_neurons[-1], return_sequences=False,
                                    kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


# TODO: documentation
class CBiGRU(Network):

    def __init__(self, conv_info, gru_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam',
                 optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(CBiGRU, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.conv_info = conv_info
        self.gru_neurons = gru_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length))

        for info in self.conv_info:
            model.add(Conv1D(info[0], info[1], activation='relu'))
            model.add(MaxPooling1D())
            model.add(Dropout(self.dropout_rate))

        for neuron_value in self.gru_neurons[:-1]:
            model.add(Bidirectional(GRU(neuron_value, return_sequences=True,
                                        kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
            model.add(Dropout(self.dropout_rate))

        model.add(Bidirectional(GRU(self.gru_neurons[-1], return_sequences=False,
                                    kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


# TODO: documentation
# TODO: use_question_for_answer
# TODO: add_condensed_state
# TODO: recurrent_embeddings
# TODO: bidirectional_embeddings
class MemN2N_tf(Network):
    """
    End-to-End memory networks (MemN2N)
    """

    def __init__(self, hops, context_dim, embedding_dimension,
                 optimizer, optimizer_args, session=None, group_classification=False,
                 layer_wise=True, position_encoding=False, position_encoding_type='intra',
                 response_linearity=False, max_grad_norm=40.0, share_input_embedding='True',
                 query_mode='use_sentence', clip_gradient=True, add_gradient_noise=True,
                 add_nil_vars=True, similarity_weights=(128, 64, 1),
                 attention_type='softmax', add_projection_layer=False, similarity_operation='dot_product',
                 l2_regularization=None, share_response=True, answer_weights=(256, 64),
                 use_cross_entropy=True):

        super(MemN2N_tf, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.hops = hops
        self.share_input_embedding = share_input_embedding
        self.context_dim = context_dim
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.layer_wise = layer_wise
        self.position_encoding = position_encoding
        self.position_encoding_type = position_encoding_type
        self.response_linearity = response_linearity
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.add_nil_vars = add_nil_vars
        self.attention_type = attention_type
        self.query_mode = query_mode
        self.add_projection_layer = add_projection_layer
        self.similarity_operation = similarity_operation
        self.similarity_weights = similarity_weights
        self.l2_regularization = l2_regularization
        self.share_response = share_response
        self.answer_weights = answer_weights
        self.use_cross_entropy = use_cross_entropy

        if group_classification:
            self.output_size = context_dim
        else:
            self.output_size = 1

    def _build_inputs(self):
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        if self.query_mode == 'placeholder':
            self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')
        else:
            self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        self.keep_prob = tf.placeholder(name='keep_prob', dtype=tf.float32)
        self.learning_rate = tf.placeholder(name='lr', dtype=tf.float32)

    def get_attentions_weights(self, x, batch_size=32):
        batches, num_batches = self._get_batches(data=x, batch_size=batch_size)
        weights = []
        # I know for sure that x is a list
        offset = 0
        memory_indices = []
        for sample in x[0]:
            samples_indices = list(range(offset, offset + len(sample)))
            memory_indices.append(samples_indices)
            offset += len(sample)

        for batch_idx in range(num_batches):
            batch_x = [batches[item][batch_idx] for item in range(len(x))]

            feed_dict = self.get_feed_dict(x=batch_x,
                                           y=None,
                                           phase='test')

            batch_weights = self.session.run(self.probs, feed_dict)
            # I cannot reshape to memory size if I have variable memory dimension per sample!
            # Instead, I can store indices
            # batch_weights = [item.reshape(-1, self.memory_size) for item in batch_weights]
            if batch_idx == 0:
                weights.extend(batch_weights)
            else:
                for idx, weight in enumerate(batch_weights):
                    weights[idx] = np.append(weights[idx], weight, axis=0)

        selected = np.array([[np.argmax(hop_weights[ind_slice])
                              for hop_weights in weights]
                             for ind_slice in memory_indices])
        return selected

    def _layer_wise(self, A, C, B=None, embedding_matrix=None):
        """
        Input embeddings are shared respectively
        across hops:

        A_1 = A_2 = ... = A_k
        C_1 = C_2 = ... =C_k
        """

        self.A = []
        self.C = []

        if embedding_matrix is not None:
            if not self.use_cross_entropy and self.hops == 1:
                A_var = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix), name='A_1',
                                        shape=embedding_matrix.shape, trainable=True)
                C_var = A_var
            else:
                A_var = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix), name='A_1',
                                        shape=embedding_matrix.shape, trainable=True)
                C_var = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix), name='C_1',
                                        shape=embedding_matrix.shape, trainable=True)

        else:
            if not self.use_cross_entropy and self.hops == 1:
                A_var = tf.Variable(A, name='A_1')
                C_var = A_var
            else:
                A_var = tf.Variable(A, name='A_1')
                C_var = tf.Variable(C, name='C_1')

        if not self.share_input_embedding and self.query_mode != 'placeholder':
            if embedding_matrix is not None:
                self.B = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix), name='B',
                                         shape=embedding_matrix.shape, trainable=True)
            else:
                self.B = tf.Variable(B, name='B')
        else:
            self.B = A_var

        for hop in range(self.hops):
            self.A.append(A_var)
            self.C.append(C_var)

    def _adjacent_sharing(self, A, C, B=None, embedding_matrix=None):
        """
        Input embeddings are subject to the
        following rule:

            A_i+1 = C_i

        In other words, the input memory embeddings matrix
        is equal to the output memory embeddings of the
        previous hop
        """

        self.A = []
        self.C = []

        if embedding_matrix is not None:
            A_var = tf.get_variable(name='A_1', shape=embedding_matrix.shape,
                                    initializer=tf.constant_initializer(embedding_matrix), trainable=True)
        else:
            A_var = tf.Variable(A, name='A_1')

        if not self.share_input_embedding and self.query_mode != 'placeholder':
            if embedding_matrix is not None:
                self.B = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix), name='B',
                                         shape=embedding_matrix.shape, trainable=True)
            else:
                self.B = tf.Variable(B, name='B')
        else:
            self.B = A_var

        for hop in range(self.hops - 1):
            if hop == 0:
                self.A.append(A_var)
            else:
                self.A.append(self.C[hop - 1])

            if embedding_matrix is not None:
                hop_C = tf.get_variable(name='C_{}'.format(hop + 1), shape=embedding_matrix.shape,
                                        initializer=tf.constant_initializer(embedding_matrix), trainable=True)
            else:
                hop_C = tf.Variable(C, name='C_{}'.format(hop + 1))

            self.C.append(hop_C)

        if self.use_cross_entropy:
            self.A.append(self.C[-1])
            if embedding_matrix is not None:
                hop_C = tf.get_variable(name='C_{}'.format(self.hops - 1), shape=embedding_matrix.shape,
                                        initializer=tf.constant_initializer(embedding_matrix), trainable=True)
            else:
                hop_C = tf.Variable(C, name='C_{}'.format(self.hops - 1))

            self.C.append(hop_C)
        else:
            self.A.append(self.C[-1])
            self.C.append(self.A[-1])

    def _build_vars(self, vocab_size, embedding_matrix=None):
        """
        A, B and C are the embeddings weights.
        H is the response weight matrix that describes a linearity
        mapping between accumulated vectors.
        Projection_H is a weight matrix concerning input to memory mapping.
        """

        with tf.variable_scope('MemN2N'):
            nil_word_slot = tf.zeros([1, self.embedding_dimension])

            self.initializer = xavier_initializer()
            A = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
            C = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])

            if not self.share_input_embedding:
                B = tf.concat(axis=0,
                              values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
            else:
                B = None

            if self.layer_wise:
                self._layer_wise(A, C, B, embedding_matrix)
            else:
                self._adjacent_sharing(A, C, B, embedding_matrix)

            if self.response_linearity:
                self.H = []
                if self.share_response:
                    H_var = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_1")
                    for hop in range(self.hops):
                        self.H.append(H_var)
                else:
                    for hop in range(self.hops):
                        self.H.append(
                            tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_{}".format(hop + 1)))

            if self.add_projection_layer:
                self.projection_H = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                                name='projection_H')

        self._nil_vars = set([x.name for x in self.A] + [x.name for x in self.C])

        if not self.share_input_embedding:
            self._nil_vars.add(self.B.name)

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        if self.add_nil_vars:
            nil_grads_and_vars = []
            for g, v in grads_and_vars:
                if v.name in self._nil_vars:
                    nil_grads_and_vars.append((zero_nil_slot(g), v))
                else:
                    nil_grads_and_vars.append((g, v))
            self.train_op = self.opt.apply_gradients(nil_grads_and_vars, name="train_op")
        else:
            self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _calculate_similarity(self, memory, query, hop):

        apply_softmax = True

        if self.similarity_operation == 'dot_product':
            if self.query_mode != 'placeholder':
                q_temp = tf.transpose(tf.expand_dims(query, -1), [0, 2, 1])
                dotted = tf.reduce_sum(memory * q_temp, 2)
            else:
                # hack to get around no reduce_dot
                # [batch_size, 1, embedding_dim]
                u_temp = tf.expand_dims(query, 1)

                # [batch_size, mem_size, embedding_dim]
                # [batch_size, 1, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size]
                dotted = tf.reduce_sum(memory * u_temp, 2)
        elif self.similarity_operation == 'nn_attention':

            m_A_list = tf.unstack(tf.transpose(memory, [1, 0, 2]))
            att_input = []
            for item in m_A_list:
                att_input.append(tf.concat([item, query], axis=1))

            # [batch_size * mem_size, 2*embedding_dim]
            att_input = tf.stack(att_input)
            att_input = tf.transpose(att_input, [1, 0, 2])
            att_input = tf.reshape(att_input, shape=[-1, att_input.shape[-1]])

            if hop > 0:
                reuse = True
            else:
                reuse = None
            with tf.variable_scope("nn_attention", reuse=reuse):
                # -> [batch_size, mem_size, 1]
                similarities = []

                for idx, weight in enumerate(self.similarity_weights):
                    if idx == 0:
                        dotted = tf.contrib.layers.fully_connected(att_input,
                                                                   weight,
                                                                   activation_fn=tf.nn.leaky_relu,
                                                                   scope="nn_att_{}".format(idx),
                                                                   reuse=reuse)
                    else:
                        dotted = tf.contrib.layers.fully_connected(similarities[-1],
                                                                   weight,
                                                                   activation_fn=None,
                                                                   scope="nn_att_{}".format(idx),
                                                                   reuse=reuse)
                    similarities.append(dotted)

                # [batch_size, mem_size]
                last_similarity = tf.reshape(similarities[-1], shape=[-1, memory.shape[1], similarities[-1].shape[-1]])
                dotted = tf.squeeze(last_similarity, axis=2)
        elif self.similarity_operation == 'scaled_dot_product':
            if self.query_mode != 'placeholder':
                # [batch_size, mem_size, query_length]
                dotted = tf.matmul(memory, query, transpose_b=True) * self.embedding_dimension ** 0.5

                # [batch_size, mem_size]
                dotted = tf.reduce_sum(dotted, 2)
            else:
                # hack to get around no reduce_dot
                # [batch_size, 1, embedding_dim]
                u_temp = tf.expand_dims(query, 1)

                # [batch_size, mem_size, embedding_dim]
                # [batch_size, 1, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size]
                dotted = tf.reduce_sum(memory * u_temp, 2) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted, apply_softmax

    def _compute_answer(self, final_response):

        answers = []

        with tf.variable_scope('Answer'):

            for idx, weight in enumerate(self.answer_weights):
                if idx == 0:
                    dotted = tf.contrib.layers.fully_connected(final_response,
                                                               weight,
                                                               activation_fn=tf.nn.leaky_relu,
                                                               scope="answer_{}".format(idx),
                                                               reuse=False)
                else:
                    dotted = tf.contrib.layers.fully_connected(answers[-1],
                                                               weight,
                                                               activation_fn=tf.nn.leaky_relu,
                                                               scope="answer_{}".format(idx),
                                                               reuse=False)
                answers.append(dotted)

            final_answer = tf.contrib.layers.fully_connected(answers[-1],
                                                             self.output_size,
                                                             activation_fn=None,
                                                             scope="final_answer",
                                                             reuse=False)

        return final_answer

    def _zero_out_padding(self, embedding_matrix):
        return tf.scatter_update(embedding_matrix,
                                 0,
                                 tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

    def get_output(self):
        with tf.variable_scope('Output'):

            if self.query_mode != 'placeholder':
                # Use A_1 for thee question embedding as per Adjacent Weight Sharing
                # [batch_size, query_length, embedding_dim]
                with tf.control_dependencies([self._zero_out_padding(self.B)]):
                    q_emb = tf.nn.embedding_lookup(self.B, self.query)

                if self.query_encoding is not None:
                    q_emb = q_emb * self.query_encoding

                # [batch_size, embedding_dim]
                u_0 = tf.reduce_sum(q_emb, 1)
            else:
                u_0 = self.query
            u = [u_0]

            self.probs = []

            for hopn in range(self.hops):
                with tf.variable_scope('hop_{}'.format(hopn)):
                    # [batch_size, mem_size, story_length, embedding_dim]
                    with tf.control_dependencies([self._zero_out_padding(self.A[hopn])]):
                        m_emb_A = tf.nn.embedding_lookup(self.A[hopn], self.context)

                    if self.context_encoding is not None:
                        m_emb_A = m_emb_A * self.context_encoding

                    # TODO: maybe it's better to reduce_sum first and then apply some fully connected layers
                    if self.add_projection_layer:
                        # [batch_size, mem_size, story_length, embedding_dim]
                        m_emb_A = tf.tensordot(m_emb_A, self.projection_H, axes=[[3], [0]])

                # [batch_size, mem_size, embedding_dim]
                m_A = tf.reduce_sum(m_emb_A, 2)

                dotted, apply_softmax = self._calculate_similarity(memory=m_A, query=u[-1], hop=hopn)
                self.dotted = dotted

                # Calculate probabilities
                if self.attention_type == 'sparsemax':
                    probs = sparsemax(dotted)
                else:
                    if apply_softmax:
                        probs = tf.nn.softmax(dotted, axis=1)
                    else:
                        probs = dotted

                self.probs.append(probs)
                probs_temp = tf.transpose(tf.expand_dims(probs, -1), [0, 2, 1])
                with tf.variable_scope('hop_{}'.format(hopn)):
                    with tf.control_dependencies([self._zero_out_padding(self.C[hopn])]):
                        m_emb_C = tf.nn.embedding_lookup(self.C[hopn], self.context)

                    if self.context_encoding is not None:
                        m_emb_C = m_emb_C * self.context_encoding

                    # TODO: maybe it's better to reduce_sum first and then apply some fully connected layers
                    if self.add_projection_layer:
                        # [batch_size, mem_size, embedding_dim]
                        m_emb_C = tf.tensordot(m_emb_C, self.projection_H, axes=[[3], [0]])

                m_C = tf.reduce_sum(m_emb_C, 2)

                # [batch_size, embedding_dim, mem_size]
                c_temp = tf.transpose(m_C, [0, 2, 1])

                # [batch_size, 1, mem_size]
                # [batch_size, embedding_dim, mem_size]
                # -> [batch_size, embedding_dim, mem_size]
                # -> [batch_size, embedding_dim]
                o_k = tf.reduce_sum(c_temp * probs_temp, 2)

                # Dont use projection layer for adj weight sharing
                if self.response_linearity:
                    u_k = tf.matmul(u[-1], self.H[hopn]) + o_k
                else:
                    u_k = u[-1] + o_k

                u.append(u_k)

            return self._compute_answer(final_response=u[-1])

    def build_model(self, text_info):

        # FIXME
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            if self.position_encoding_type == 'intra':
                to_apply = position_encoding_intra
            else:
                to_apply = position_encoder_extra
            self.context_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                         sentence_size=self.sentence_size),
                                                name='context_position_encoding', dtype=np.float32)
            self.query_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                       sentence_size=self.query_size),
                                              name='query_position_encoding', dtype=np.float32)
        else:
            self.context_encoding = None
            self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        feed_dict = {
            self.context: context,
            self.query: query,
            self.learning_rate: self.optimizer_args['learning_rate']
        }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss, _ = self.session.run([self.loss_op, self.train_op], feed_dict=feed_dict)
        return loss

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


class EMemN2N_tf(MemN2N_tf):

    def __init__(self, dropout_rate, max_batch_size=16,
                 use_question_for_answer=False, add_condensed_state=False,
                 recurrent_embeddings=False, bidirectional_embeddings=False,
                 use_batch_norm=False, use_gates=False, use_gate_loss=False,
                 partial_supervision=None, use_custom_prediction=False,
                 use_cross_entropy=True, use_max_margin_loss=False,
                 max_negative_samples=10, margin=0.1, **kwargs):
        super(EMemN2N_tf, self).__init__(**kwargs)
        self.max_batch_size = max_batch_size
        self.dropout_rate = dropout_rate
        self.use_question_for_answer = use_question_for_answer
        self.add_condensed_state = add_condensed_state
        self.recurrent_embeddings = recurrent_embeddings
        self.bidirectional_embeddings = bidirectional_embeddings
        self.use_batch_norm = use_batch_norm
        self.use_gates = use_gates
        self.use_gate_loss = use_gate_loss
        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
            self.partial_supervision_coeff = partial_supervision[1]
        else:
            self.partial_supervision = False
            self.partial_supervision_coeff = 1
        self.use_custom_prediction = use_custom_prediction
        self.use_cross_entropy = use_cross_entropy
        self.use_max_margin_loss = use_max_margin_loss
        self.max_negative_samples = max_negative_samples
        self.margin = margin

        # TODO: is it true?
        # Disabled due to no contributes
        if not self.use_cross_entropy:
            self.response_linearity = False
            self.add_condensed_state = False
            self.add_projection_layer = False

        if self.use_batch_norm:
            self.mlp = lambda **x: batch_norm_relu(phase=self.phase, activation=True, **x)
        else:
            self.mlp = fully_connected

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, None], name="context")

        if self.recurrent_embeddings:
            self.context_len = tf.placeholder(tf.int32, [None], name="context_len")

        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')
        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, None], name='query')

        if self.recurrent_embeddings:
            self.query_len = tf.placeholder(tf.int32, shape=[None], name='query_len')

        if self.partial_supervision:
            if self.use_max_margin_loss:
                self.positive_idxs = tf.placeholder(tf.int32,
                                                    shape=[None, self.padding_amount, 2],
                                                    name='positive_indexes')
                self.negative_idxs = tf.placeholder(tf.int32,
                                                    shape=[None, self.padding_amount, 2],
                                                    name='negative_indexes')
                self.mask_idxs = tf.placeholder(tf.float32,
                                                shape=[None, self.padding_amount],
                                                name='mask_indexes')
            else:
                self.targets = tf.placeholder(tf.float32, shape=[None, None], name='supervision_targets')
                self.target_segments = tf.placeholder(tf.int32, shape=[None, ], name='supervision_target_segments')

        self.keep_prob = tf.placeholder(name='keep_prob', dtype=tf.float32)
        self.memory_dimension = tf.placeholder(name='memory_dimension', dtype=tf.int32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        self.learning_rate = tf.placeholder(name='lr', dtype=tf.float32)

    def build_model(self, text_info):

        # FIXME
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']
        if self.use_max_margin_loss:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            if self.position_encoding_type == 'intra':
                to_apply = position_encoding_intra
            else:
                to_apply = position_encoder_extra
            self.context_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                         sentence_size=self.sentence_size),
                                                name='context_position_encoding', dtype=np.float32)
            self.query_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                       sentence_size=self.query_size),
                                              name='query_position_encoding', dtype=np.float32)
        else:
            self.context_encoding = None
            self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def build_predict_op(self):
        if not self.use_custom_prediction:
            predict_op = tf.nn.sigmoid(self.logits)
            self.predict_op = tf.round(predict_op, name='predict_op')
        elif self.use_gates:
            reduced = reduce(lambda x, y: tf.concat((x, y), axis=1), self.gates)
            reduced = tf.nn.sigmoid(reduced)
            reduced = tf.round(reduced)
            reduced = tf.reduce_sum(reduced, axis=1)

            ones = tf.ones_like(reduced)
            zeros = tf.zeros_like(reduced)

            self.predict_op = tf.where(reduced > 0, ones, zeros, name='predict_op')

            # TODO: attention

        else:
            raise RuntimeError('Could not build predict op! Check your flags...')

    def _build_vars(self, vocab_size, embedding_matrix=None):
        super(EMemN2N_tf, self)._build_vars(vocab_size=vocab_size,
                                            embedding_matrix=embedding_matrix)

        if self.add_condensed_state:
            if self.embedding_dimension < 2 ** self.hops:
                self.add_condensed_state = False
                logger.info('Disabling condensed state due to high number of hops!')
            else:
                self.D = []
                current_dimension = self.embedding_dimension
                for hop in range(self.hops):
                    self.D.append(
                        tf.Variable(self.initializer([current_dimension, int(current_dimension / 2)]),
                                    name="condensed_projection_{}".format(hop + 1)))
                    current_dimension = self.embedding_dimension + int(current_dimension / 2)

        if self.recurrent_embeddings:

            self.input_scope = []
            self.input_reuse = []
            self.output_scope = []
            self.output_reuse = []
            if self.layer_wise:
                self.input_scope = ['input_encoding'] * self.hops
                self.input_reuse = [False] + [True] * (self.hops - 1)
                self.output_scope = ['output_encoding'] * self.hops
                self.output_reuse = [False] + [True] * (self.hops - 1)
            else:
                for hop in range(self.hops):
                    if hop == 0:
                        self.input_scope.append('input_encoding')
                        self.input_reuse.append(False)
                    else:
                        self.input_scope.append(self.output_scope[hop - 1])
                        self.input_reuse.append(True)

                    self.output_scope.append('output_encoding_{}'.format(hop))
                    self.output_reuse.append(False)

    def _calculate_similarity(self, memory, query, hop, scope='nn_attention'):
        # memory: [batch_size * memory, embedding_dim]
        # query: [batch_size, embedding_dim]

        apply_softmax = True

        # [batch_size * memory, embedding_dim]
        repeated_query = tf.gather(query, self.context_segments)

        if self.similarity_operation == 'dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1)
        elif self.similarity_operation == 'nn_attention':

            # [batch_size * memory, 2 * embedding_dim]
            att_input = tf.concat((memory, repeated_query), axis=1)

            if hop > 0:
                reuse = True
            else:
                reuse = None
            with tf.variable_scope(scope, reuse=reuse):
                # -> [batch_size * mem_size, 1]
                similarities = [att_input]

                for idx, weight in enumerate(self.similarity_weights[:-1]):
                    dotted = self.mlp(inputs=similarities[-1],
                                      num_outputs=weight,
                                      scope="nn_att_{}".format(idx),
                                      reuse=reuse)

                    dotted = tf.nn.dropout(dotted, self.keep_prob)
                    similarities.append(dotted)

                dotted = tf.contrib.layers.fully_connected(similarities[-1],
                                                           self.similarity_weights[-1],
                                                           activation_fn=None,
                                                           scope="nn_att_{}".format(idx + 1),
                                                           reuse=reuse)
                similarities.append(dotted)

                # [batch_size * mem_size,]
                dotted = tf.squeeze(similarities[-1], axis=1)
        elif self.similarity_operation == 'scaled_dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted, apply_softmax

    def get_attentions_weights(self, x, batch_size=32):
        batches, num_batches = self._get_batches(data=x, batch_size=batch_size)
        attentions = {}
        # I know for sure that x is a list
        offset = 0
        memory_indices = []
        for sample in x[1]:
            samples_indices = list(range(offset, offset + len(sample)))
            memory_indices.append(samples_indices)
            offset += len(sample)

        to_feed = [self.probs]
        if self.use_gates:
            to_feed = [self.probs, self.gates]

        for batch_idx in range(num_batches):
            batch_x = [batches[item][batch_idx] for item in range(len(x))]

            feed_dict = self.get_feed_dict(x=batch_x,
                                           y=None,
                                           phase='test')

            batch_info = self.session.run(to_feed, feed_dict)
            batch_weights = batch_info[0]
            if self.use_gates:
                batch_gates = batch_info[1]

            if self.attention_type == 'sigmoid':
                batch_weights = [np.round(item) for item in batch_weights]

            # I cannot reshape to memory size if I have variable memory dimension per sample!
            # Instead, I can store indices
            # batch_weights = [item.reshape(-1, self.memory_size) for item in batch_weights]
            if batch_idx == 0:
                attentions.setdefault('memory_slots', []).extend(batch_weights)
                if self.use_gates:
                    attentions.setdefault('gates', []).extend(batch_gates)
            else:
                for idx, weight in enumerate(batch_weights):
                    attentions['memory_slots'][idx] = np.append(attentions['memory_slots'][idx], weight, axis=0)

                if self.use_gates:
                    for idx, gate in enumerate(batch_gates):
                        attentions['gates'][idx] = np.append(attentions['gates'][idx], gate, axis=0)

        # if self.attention_type == 'sigmoid':
        #     op = np.argwhere
        # else:
        #     op = np.argmax
        #
        # selected = np.array([[op(hop_weights[ind_slice])
        #                       for hop_weights in attentions['memory_slots']]
        #                      for ind_slice in memory_indices])

        # to_return = {
        #     'memory_slots': selected
        # }
        # if self.use_gates:
        #     to_return['hop_gates'] = np.array(attentions['gates']).reshape(attentions['gates'][0].shape[0],
        #                                                                    len(attentions['gates']))

        # return to_return
        return attentions, memory_indices

    def _compute_answer(self, final_response, initial_question=None):

        answers = []

        # [batch_size, 2 * embedding_dimension]
        if initial_question is not None:
            final_response = tf.concat((final_response, initial_question), axis=1)

        with tf.variable_scope('Answer'):

            for idx, weight in enumerate(self.answer_weights):
                if idx == 0:
                    dotted = self.mlp(inputs=final_response,
                                      num_outputs=weight,
                                      scope="answer_{}".format(idx),
                                      reuse=False)
                else:
                    dotted = self.mlp(inputs=answers[-1],
                                      num_outputs=weight,
                                      scope="answer_{}".format(idx),
                                      reuse=False)

                if idx < len(self.answer_weights) - 1:
                    dotted = tf.nn.dropout(dotted, self.keep_prob)

                answers.append(dotted)

            final_answer = tf.contrib.layers.fully_connected(answers[-1],
                                                             self.output_size,
                                                             activation_fn=None,
                                                             scope="final_answer",
                                                             reuse=False)

        return final_answer

    def _convert_input(self, data, scope, data_len=None, reuse=False):
        """
        Converts input via RNN network
        """

        # Data shape: [batch_size, sequence_length, embedding_dimension]
        # Data length shape: [batch_size]

        if self.bidirectional_embeddings:

            with tf.variable_scope(scope, reuse=reuse):
                fwd_rnn_cell = tf.contrib.rnn.GRUCell(int(self.embedding_dimension / 2))
                bwd_rnn_cell = tf.contrib.rnn.GRUCell(int(self.embedding_dimension / 2))
                _, outputs = tf.nn.bidirectional_dynamic_rnn(fwd_rnn_cell,
                                                             bwd_rnn_cell,
                                                             inputs=data,
                                                             sequence_length=data_len,
                                                             dtype=tf.float32)

                outputs = tf.concat((outputs[0], outputs[1]), axis=1)
        else:

            with tf.variable_scope(scope, reuse=reuse):
                rnn_cell = tf.contrib.rnn.GRUCell(self.embedding_dimension)

                _, outputs = tf.nn.dynamic_rnn(rnn_cell,
                                               data,
                                               dtype=tf.float32,
                                               sequence_length=data_len
                                               )

                # outputs = outputs.c

        # [batch_size, hidden_size]
        return outputs

    def get_output(self):
        # query: [batch_size, q_max_length]
        # context: [batch_size * memory, s_max_length]

        with tf.variable_scope('Output'):

            if self.query_mode != 'placeholder':
                # Use A_1 for thee question embedding as per Adjacent Weight Sharing
                # [batch_size, query_length, embedding_dim]
                with tf.control_dependencies([self._zero_out_padding(self.B)]):
                    q_emb = tf.nn.embedding_lookup(self.B, self.query)

                # [batch_size, embedding_dim]
                if self.recurrent_embeddings:
                    u_0 = self._convert_input(data=q_emb, data_len=self.query_len, scope='query_encoding', reuse=False)
                else:
                    u_0 = tf.reduce_sum(q_emb, 1)
            else:
                u_0 = self.query
            u = [u_0]

            if self.add_condensed_state:
                condensed_states = [u_0]

            self.probs = []
            self.gates = []
            self.dotted = []

            for hopn in range(self.hops):
                with tf.variable_scope('hop_{}'.format(hopn)):
                    # [batch_size * mem_size, s_max_length, embedding_dim]
                    with tf.control_dependencies([self._zero_out_padding(self.A[hopn])]):
                        m_emb_A = tf.nn.embedding_lookup(self.A[hopn], self.context)

                    # TODO: maybe it's better to reduce_sum first and then apply some fully connected layers
                    if self.add_projection_layer:
                        # [batch_size * mem_size, s_max_length, embedding_dim]
                        m_emb_A = tf.tensordot(m_emb_A, self.projection_H, axes=[[2], [0]])
                        m_emb_A = tf.nn.relu(m_emb_A)

                # [batch_size * mem_size, embedding_dim]
                if self.recurrent_embeddings:
                    m_A = self._convert_input(data=m_emb_A, data_len=self.context_len,
                                              scope=self.input_scope[hopn], reuse=self.input_reuse[hopn])
                else:
                    m_A = tf.reduce_sum(m_emb_A, 1)

                # [batch_size * mem_size, ]
                dotted, apply_softmax = self._calculate_similarity(memory=m_A, query=u[-1], hop=hopn)
                self.dotted.append(dotted)

                # Dynamic partition for segment-wise softmax
                dotted_mems = tf.dynamic_partition(dotted, self.context_segments, num_partitions=self.max_batch_size)
                filtered_mems = []

                def attention_method(mem):
                    if self.attention_type == 'softmax':
                        return tf.nn.softmax(mem)
                    elif self.attention_type == 'sparsemax':
                        return tf.reshape(sparsemax(tf.reshape(mem, shape=[1, tf.shape(mem)[0]])), shape=[-1])
                    else:
                        return tf.nn.sigmoid(mem)

                for mem in dotted_mems:
                    filtered = tf.cond(tf.equal(tf.size(mem), 0),
                                       true_fn=lambda: tf.zeros(shape=(1,), dtype=tf.float32),
                                       false_fn=lambda: attention_method(mem))
                    filtered_mems.append(filtered)

                filtered_mems = tf.concat(filtered_mems, axis=0)
                true_indexes = tf.range(0, tf.shape(self.context)[0])
                true_filtered_mems = tf.gather(filtered_mems, true_indexes)
                probs = tf.reshape(true_filtered_mems, shape=[-1])

                self.probs.append(probs)
                with tf.variable_scope('hop_{}'.format(hopn)):
                    with tf.control_dependencies([self._zero_out_padding(self.C[hopn])]):
                        m_emb_C = tf.nn.embedding_lookup(self.C[hopn], self.context)

                    # TODO: maybe it's better to reduce_sum first and then apply some fully connected layers
                    if self.add_projection_layer:
                        # [batch_size * mem_size, s_max_length, embedding_dim]
                        m_emb_C = tf.tensordot(m_emb_C, self.projection_H, axes=[[2], [0]])
                        m_emb_C = tf.nn.relu(m_emb_C)

                # [batch_size * mem_size, embedding_dim]
                if self.recurrent_embeddings:
                    m_C = self._convert_input(data=m_emb_C, data_len=self.context_len,
                                              scope=self.output_scope[hopn], reuse=self.output_reuse[hopn])
                else:
                    m_C = tf.reduce_sum(m_emb_C, 1)

                # [batch_size * mem_size, 1]
                probs_temp = tf.expand_dims(probs, -1)

                # [batch_size * mem_size, embedding_dim]
                # [batch_size * mem_size, 1]
                # -> [batch_size * mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                o_k = m_C * probs_temp
                o_k = tf.segment_sum(o_k, self.context_segments)

                if self.use_gates:
                    # g(sim(u_k, m)) instead of g(u_k)
                    mem_gate = fully_connected(tf.reshape(dotted, shape=(-1, self.memory_size)),
                                               1,
                                               activation_fn=None,
                                               scope="mem_gate_{}".format(hopn + 1),
                                               reuse=tf.AUTO_REUSE)

                    self.gates.append(mem_gate)

                # Dont use projection layer for adj weight sharing
                if self.response_linearity:
                    if self.use_gates:
                        # u_k = mem_gate * o_k + tf.matmul(u[-1], self.H[hopn]) * (1 - mem_gate)
                        u_k = tf.nn.sigmoid(mem_gate) * o_k + tf.matmul(u[-1], self.H[hopn])
                    else:
                        u_k = tf.matmul(u[-1], self.H[hopn]) + o_k
                else:
                    if self.use_gates:
                        # u_k = mem_gate * o_k + u[-1] * (1 - mem_gate)
                        u_k = tf.nn.sigmoid(mem_gate) * o_k + u[-1]
                    else:
                        u_k = u[-1] + o_k

                u.append(u_k)

                if self.add_condensed_state:
                    new_condensed_state = tf.concat((u_k, tf.matmul(condensed_states[-1], self.D[hopn])),
                                                    axis=1)
                    condensed_states.append(new_condensed_state)

            if self.use_question_for_answer:
                initial_question = u_0
            else:
                initial_question = None

            if self.add_condensed_state:
                final_response = condensed_states[-1]
            else:
                final_response = u[-1]

            if self.use_cross_entropy:
                return self._compute_answer(final_response=final_response,
                                            initial_question=initial_question)
            else:
                return final_response

    def build_loss(self):
        self.logits = self.get_output()
        self.loss_op = None
        if self.use_cross_entropy:
            cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name="cross_entropy")
            # reduce_mean should be more stable w.r.t. batch size
            cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
            self.cross_entropy_mean = cross_entropy_mean  # for visualization / debugging
            self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_l2 = loss_l2  # for visualization/debugging
            if self.loss_op is None:
                self.loss_op = loss_l2
            else:
                self.loss_op += loss_l2

        # given the gates and the true labels I know when the model should look over the memory
        # this is very similar to the answer loss -> by default I cannot say anything except for unfair clauses
        if self.use_gates and self.use_gate_loss:
            gate_losses = []
            # weights = tf.ones_like(self.answer)
            # mask = tf.zeros_like(self.answer)
            # weights = tf.where(tf.equal(self.answer, weights), weights, mask)
            for idx, gate in enumerate(self.gates):
                gate_loss = tf.nn.weighted_cross_entropy_with_logits(logits=gate,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name='gate_entropy_{}'.format(idx))
                # gate_loss = gate_loss * weights
                gate_loss = tf.reduce_mean(gate_loss)
                gate_losses.append(gate_loss)

            gate_ov_loss = tf.stack(gate_losses)
            gate_ov_loss = tf.reduce_mean(gate_ov_loss)

            self.gate_ov_loss = gate_ov_loss  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = gate_ov_loss
            else:
                self.loss_op += gate_ov_loss

        if self.partial_supervision:

            if self.use_max_margin_loss:
                supervision_losses = []

                mask_res = tf.tile(self.mask_idxs, multiples=[1, self.padding_amount])
                mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

                for sim in self.dotted:
                    res_dotted = tf.reshape(sim, shape=[-1, self.memory_size])
                    pos_scores = tf.gather_nd(res_dotted, self.positive_idxs)
                    pos_scores = tf.reshape(pos_scores, [-1, 1])
                    neg_scores = tf.gather_nd(res_dotted, self.negative_idxs)
                    neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
                    neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])
                    hop_supervision_loss = tf.maximum(0., self.margin - pos_scores + neg_scores)
                    hop_supervision_loss = hop_supervision_loss * mask_res
                    hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
                    supervision_losses.append(hop_supervision_loss)

                supervision_losses = tf.stack(supervision_losses)
                supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_coeff
            else:
                # partial supervision:
                # for each hop select the minimum between each hot encoded target distribution:
                # the model is asked to just select one correct memory slot, and not necessarily all of them.
                def compute_supervision_loss(sim, target):

                    expanded_sim = tf.expand_dims(sim, 0)
                    expanded_sim = tf.tile(expanded_sim, multiples=(tf.shape(target)[0], 1))
                    sample_loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=target,
                                                                             logits=expanded_sim,
                                                                             name='supervision_loss_{}'.format(idx))

                    return tf.reduce_min(sample_loss)

                def pass_fake():
                    fake = tf.zeros(shape=(), dtype=tf.float32)
                    return fake

                supervision_losses = []
                for similarity in self.dotted:
                    hop_loss = []

                    sample_similarity = tf.dynamic_partition(data=similarity,
                                                             partitions=self.context_segments,
                                                             num_partitions=self.max_batch_size)
                    sample_targets = tf.dynamic_partition(data=self.targets,
                                                          partitions=self.target_segments,
                                                          num_partitions=self.max_batch_size)

                    for idx, (sim, target) in enumerate(zip(sample_similarity, sample_targets)):
                        # hack to avoid weights -> have to take into account when averaging!
                        filtered = tf.cond(tf.equal(tf.size(target), 0),
                                           true_fn=lambda: pass_fake(),
                                           false_fn=lambda: compute_supervision_loss(sim, target))
                        hop_loss.append(filtered)

                    hop_loss = tf.stack(hop_loss, axis=0)
                    true_indexes = tf.range(0, tf.shape(self.query)[0])
                    hop_loss = tf.gather(hop_loss, true_indexes)

                    # Masked average
                    mask = tf.ones(shape=tf.shape(hop_loss)[0])
                    zeros = tf.zeros(shape=tf.shape(hop_loss)[0])
                    mask = tf.where(tf.equal(hop_loss, 0), zeros, mask)
                    # Avoid division by zero
                    mask_sum = tf.reduce_sum(mask)
                    avg_hop_loss = tf.cond(tf.equal(mask_sum, 0),
                                           true_fn=lambda: tf.zeros(shape=()),
                                           false_fn=lambda: tf.reduce_sum(hop_loss) / mask_sum)
                    supervision_losses.append(avg_hop_loss)

                supervision_losses = tf.stack(supervision_losses)
                supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_coeff

            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = [item for group in context for item in group]

        if self.recurrent_embeddings:
            context_len = [len(item) for item in flattened_context]
            query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        if phase == 'train':
            keep_prob = 1. - self.dropout_rate
        else:
            keep_prob = 1.0

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.keep_prob: keep_prob,
                     self.memory_dimension: len(context[0]),
                     self.learning_rate: self.optimizer_args['learning_rate']
                     }

        if self.partial_supervision:
            targets = x[2]
            targets = pad_data(targets)

            if self.use_max_margin_loss:
                positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
                negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
                mask_indexes = np.ones(shape=(len(query), self.padding_amount))

                # Option A: select the first positive target
                for batch_idx, target in enumerate(targets):
                    pos_indexes = np.argwhere(target)
                    pos_indexes = pos_indexes.ravel()
                    if len(pos_indexes):
                        pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, pos_idx in enumerate(pos_choices):
                            positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                        neg_indexes = np.argwhere(np.logical_not(target))
                        neg_indexes = neg_indexes.ravel()
                        neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, neg_idx in enumerate(neg_choices):
                            negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                    else:
                        mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

                feed_dict[self.positive_idxs] = positive_indexes
                feed_dict[self.negative_idxs] = negative_indexes
                feed_dict[self.mask_idxs] = mask_indexes
            else:
                # build hot encodings
                split_targets = []
                target_segments = []
                count = 0
                for target in targets:
                    # Check enhanced target
                    if -1 not in target:
                        indexes = np.argwhere(target)
                        if len(indexes):
                            # Single target
                            hot_encoding = np.zeros(len(target))
                            hot_encoding[indexes[0]] = 1
                            split_targets.append(hot_encoding)
                            target_segments.append(count)
                            # Multiple targets
                            # for idx in indexes:
                            #     hot_encoding = np.zeros(len(target))
                            #     hot_encoding[idx] = 1
                            #     split_targets.append(hot_encoding)
                            #     target_segments.append(count)

                        else:
                            split_targets.append(target)
                            target_segments.append(count)
                    else:
                        target = [1 / len(target) for _ in range(len(target))]
                        split_targets.append(target)
                        target_segments.append(count)

                    count += 1

                feed_dict[self.targets] = split_targets
                feed_dict[self.target_segments] = target_segments

        if self.recurrent_embeddings:
            feed_dict[self.context_len] = context_len
            feed_dict[self.query_len] = query_len

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        if self.use_cross_entropy:
            loss_values.append(self.cross_entropy_mean)
            loss_names.append('cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.use_gate_loss:
            loss_values.append(self.gate_ov_loss)
            loss_names.append('gate_loss')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        if self.use_cross_entropy:
            loss_values.append(self.cross_entropy_mean)
            loss_names.append('val_cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.use_gate_loss:
            loss_values.append(self.gate_ov_loss)
            loss_names.append('val_gate_loss')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info

    def build_tensorboard_info(self):
        tf.summary.histogram(values=self.probs[0], name='attention_weights')
        # if self.partial_supervision:
        #     tf.summary.histogram(values=self.debug_indexes, name='supervision_debug_indexes')


class Word_EMemn2n_tf(EMemN2N_tf):

    def __init__(self, enc_units=32, dec_units=32,
                 att_units=32, **kwargs):
        super(Word_EMemn2n_tf, self).__init__(**kwargs)
        self.enc_units = enc_units
        self.dec_units = dec_units
        self.att_units = att_units

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, self.sentence_size], name="context")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')
        self.context_len = tf.placeholder(tf.int32, [None], name='context_len')

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

        self.keep_prob = tf.placeholder(name='keep_prob', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        self.learning_rate = tf.placeholder(name='lr', dtype=tf.float32)

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _calculate_similarity(self, memory, query, hop, scope='dot_product'):
        # memory: [batch_size * memory, embedding_dim]
        # query: [batch_size, embedding_dim]

        apply_softmax = True

        # [batch_size * memory, embedding_dim]
        repeated_query = tf.gather(query, self.context_segments)

        if self.similarity_operation == 'dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1)
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted, apply_softmax

    # TODO: sum scores timewise for each decoder iteration -> for timewise cross-entropy
    def encoder_decoder(self, query):
        # query: [batch_size, q_max_length, embedding_dim]
        # memory: [batch_size, s_max_length, embedding_dim]

        query_words = tf.unstack(query, axis=1)

        query_scores = []
        self.dec_hiddens = []
        for idx, (mem, mem_length) in enumerate(self.selected_mems):

            # Encoder
            init_state = tf.zeros((tf.shape(query)[0], self.enc_units))
            enc_cell = tf.contrib.rnn.GRUCell(self.enc_units)
            with tf.variable_scope('encoder_gru', reuse=tf.AUTO_REUSE):
                enc_output, enc_state = tf.nn.dynamic_rnn(enc_cell,
                                                          mem,
                                                          initial_state=init_state,
                                                          dtype=tf.float32,
                                                          sequence_length=mem_length
                                                          )
                self.enc_state = enc_state
                self.enc_output = enc_output

            # Decoder with attention
            dec_hiddens = []
            dec_cell = tf.contrib.rnn.GRUCell(self.dec_units)
            dec_state = enc_state
            for dec_word in query_words:
                with tf.variable_scope('attention_score', reuse=tf.AUTO_REUSE):
                    scores = fully_connected(tf.nn.tanh(
                        fully_connected(enc_output, self.att_units) + fully_connected(tf.expand_dims(dec_state, 1), self.att_units)), 1)
                weights = tf.nn.softmax(scores, axis=1)
                context_vector = tf.reduce_sum(enc_output * weights, axis=1)

                augmented_input = tf.concat((context_vector, dec_word), axis=-1)
                _, dec_state = dec_cell(augmented_input, dec_state)
                self.dec_hiddens.append(dec_state)
                dec_hiddens.append(tf.expand_dims(dec_state, 1))

            dec_hiddens = tf.concat(dec_hiddens, axis=1)
            # [batch_size, timesteps, 1]
            with tf.variable_scope('decoder_score', reuse=tf.AUTO_REUSE):
                dec_hiddens = fully_connected(dec_hiddens, 1, activation_fn=tf.nn.tanh)
            dec_hiddens = tf.reduce_sum(dec_hiddens, axis=1)
            query_scores.append(dec_hiddens)

        query_scores = tf.concat(query_scores, axis=1)
        unfairness_score = fully_connected(query_scores, 1, activation_fn=None, scope='unfair_score')
        return unfairness_score

    def get_output(self):
        # query: [batch_size, q_max_length]
        # context: [batch_size * memory, s_max_length]

        with tf.variable_scope('Output'):
            with tf.control_dependencies([self._zero_out_padding(self.B)]):
                q_emb = tf.nn.embedding_lookup(self.B, self.query)
                u_0 = tf.reduce_sum(q_emb, 1)
            u = [u_0]

            self.probs = []
            self.gates = []
            self.dotted = []
            self.selected_mems = []

            for hopn in range(self.hops):
                with tf.variable_scope('hop_{}'.format(hopn)):
                    # [batch_size * mem_size, s_max_length, embedding_dim]
                    with tf.control_dependencies([self._zero_out_padding(self.A[hopn])]):
                        m_emb_A = tf.nn.embedding_lookup(self.A[hopn], self.context)

                    if self.add_projection_layer:
                        # [batch_size * mem_size, s_max_length, embedding_dim]
                        m_emb_A = tf.tensordot(m_emb_A, self.projection_H, axes=[[2], [0]])
                        m_emb_A = tf.nn.relu(m_emb_A)

                    sent_m_A = tf.reduce_sum(m_emb_A, 1)

                # [batch_size * mem_size, ]
                dotted, apply_softmax = self._calculate_similarity(memory=sent_m_A, query=u[-1], hop=hopn)
                dotted = tf.reshape(dotted, [-1, self.memory_size])
                self.dotted.append(dotted)

                probs = tf.nn.softmax(dotted, axis=1)
                self.probs.append(probs)

                with tf.variable_scope('hop_{}'.format(hopn)):
                    with tf.control_dependencies([self._zero_out_padding(self.C[hopn])]):
                        m_emb_C = tf.nn.embedding_lookup(self.C[hopn], self.context)

                    if self.add_projection_layer:
                        # [batch_size * mem_size, s_max_length, embedding_dim]
                        m_emb_C = tf.tensordot(m_emb_C, self.projection_H, axes=[[2], [0]])
                        m_emb_C = tf.nn.relu(m_emb_C)

                    sent_m_C = tf.reduce_sum(m_emb_C, 1)

                # Encoder-Decoder info
                most_selected = tf.math.argmax(probs, axis=1)
                abs_indexes = tf.range(tf.shape(self.query)[0]) * self.memory_size
                abs_indexes = tf.cast(abs_indexes, tf.int64)
                selected_indexes = most_selected + abs_indexes
                selected_mems = tf.gather(m_emb_C, selected_indexes)
                selected_mems = tf.reshape(selected_mems,
                                           [tf.shape(self.query)[0], self.sentence_size, self.embedding_dimension])
                selected_mems_length = tf.gather(self.context_len, abs_indexes)
                self.selected_mems.append([selected_mems, selected_mems_length])

                # [batch_size * mem_size, 1]
                probs_temp = tf.reshape(probs, [-1, 1])

                # [batch_size * mem_size, embedding_dim]
                # [batch_size * mem_size, 1]
                # -> [batch_size * mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                o_k = sent_m_C * probs_temp
                o_k = tf.segment_sum(o_k, self.context_segments)
                u_k = u[-1] + o_k
                u.append(u_k)

            return self.encoder_decoder(query=q_emb)

    def build_loss(self):
        self.logits = self.get_output()
        self.loss_op = None

        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.cross_entropy_mean = cross_entropy_mean  # for visualization / debugging
        self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_l2 = loss_l2  # for visualization/debugging
            if self.loss_op is None:
                self.loss_op = loss_l2
            else:
                self.loss_op += loss_l2

        if self.partial_supervision:

            supervision_losses = []

            mask_res = tf.tile(self.mask_idxs, multiples=[1, self.padding_amount])
            mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

            for sim in self.dotted:
                res_dotted = tf.reshape(sim, shape=[-1, self.memory_size])
                pos_scores = tf.gather_nd(res_dotted, self.positive_idxs)
                pos_scores = tf.reshape(pos_scores, [-1, 1])
                neg_scores = tf.gather_nd(res_dotted, self.negative_idxs)
                neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
                neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])
                hop_supervision_loss = tf.maximum(0., self.margin - pos_scores + neg_scores)
                hop_supervision_loss = hop_supervision_loss * mask_res
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
                supervision_losses.append(hop_supervision_loss)

            supervision_losses = tf.stack(supervision_losses)
            supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_coeff
            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = [item for group in context for item in group]

        context_len = [len(item) for item in flattened_context]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        if phase == 'train':
            keep_prob = 1. - self.dropout_rate
        else:
            keep_prob = 1.0

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.keep_prob: keep_prob,
                     self.context_len: context_len,
                     self.learning_rate: self.optimizer_args['learning_rate']
                     }

        if self.partial_supervision:
            targets = x[2]
            targets = pad_data(targets)

            if self.use_max_margin_loss:
                positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
                negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
                mask_indexes = np.ones(shape=(len(query), self.padding_amount))

                # Option A: select the first positive target
                for batch_idx, target in enumerate(targets):
                    pos_indexes = np.argwhere(target)
                    pos_indexes = pos_indexes.ravel()
                    if len(pos_indexes):
                        pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, pos_idx in enumerate(pos_choices):
                            positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                        neg_indexes = np.argwhere(np.logical_not(target))
                        neg_indexes = neg_indexes.ravel()
                        neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, neg_idx in enumerate(neg_choices):
                            negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                    else:
                        mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

                feed_dict[self.positive_idxs] = positive_indexes
                feed_dict[self.negative_idxs] = negative_indexes
                feed_dict[self.mask_idxs] = mask_indexes
            else:
                # build hot encodings
                split_targets = []
                target_segments = []
                count = 0
                for target in targets:
                    # Check enhanced target
                    if -1 not in target:
                        indexes = np.argwhere(target)
                        if len(indexes):
                            # Single target
                            hot_encoding = np.zeros(len(target))
                            hot_encoding[indexes[0]] = 1
                            split_targets.append(hot_encoding)
                            target_segments.append(count)
                            # Multiple targets
                            # for idx in indexes:
                            #     hot_encoding = np.zeros(len(target))
                            #     hot_encoding[idx] = 1
                            #     split_targets.append(hot_encoding)
                            #     target_segments.append(count)

                        else:
                            split_targets.append(target)
                            target_segments.append(count)
                    else:
                        target = [1 / len(target) for _ in range(len(target))]
                        split_targets.append(target)
                        target_segments.append(count)

                    count += 1

                feed_dict[self.targets] = split_targets
                feed_dict[self.target_segments] = target_segments

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op, self.cross_entropy_mean]
        loss_names = ['train_loss', 'train_cross_entropy']
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        loss_values = [self.loss_op, self.cross_entropy_mean]
        loss_names = ['val_loss', 'val_cross_entropy']
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


class Bow_EMemN2N_tf(MemN2N_tf):

    def __init__(self, dropout_rate, max_batch_size=16,
                 use_question_for_answer=False, add_condensed_state=False,
                 recurrent_embeddings=False, bidirectional_embeddings=False,
                 use_gates=False, use_gate_loss=False, partial_supervision=False,
                 use_custom_prediction=False, use_cross_entropy=True,
                 partial_supervision_coeff=0.1, use_max_margin_loss=False,
                 max_negative_samples=10, margin=0.1, **kwargs):
        super(Bow_EMemN2N_tf, self).__init__(position_encoding=False,
                                             position_encoding_type='intra',
                                             group_classification=False,
                                             **kwargs)
        self.max_batch_size = max_batch_size
        self.dropout_rate = dropout_rate
        self.use_question_for_answer = use_question_for_answer
        self.add_condensed_state = add_condensed_state
        self.recurrent_embeddings = recurrent_embeddings
        self.bidirectional_embeddings = bidirectional_embeddings
        self.use_gates = use_gates
        self.use_gate_loss = use_gate_loss
        self.partial_supervision = partial_supervision
        self.use_custom_prediction = use_custom_prediction
        self.use_cross_entropy = use_cross_entropy
        self.partial_supervision_coeff = partial_supervision_coeff
        self.use_max_margin_loss = use_max_margin_loss
        self.max_negative_samples = max_negative_samples
        self.margin = margin

        # Disabled due to no contributes
        if not self.use_cross_entropy:
            self.response_linearity = False
            self.add_condensed_state = False
            self.add_projection_layer = False

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.float32, [None, self.embedding_dimension], name="context")

        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')
        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')

        if self.partial_supervision:
            if self.use_max_margin_loss:
                self.positive_idxs = tf.placeholder(tf.int32,
                                                    shape=[None, self.padding_amount, 2],
                                                    name='positive_indexes')
                self.negative_idxs = tf.placeholder(tf.int32,
                                                    shape=[None, self.padding_amount, 2],
                                                    name='negative_indexes')
            else:
                self.targets = tf.placeholder(tf.float32, shape=[None, None], name='supervision_targets')
                self.target_segments = tf.placeholder(tf.int32, shape=[None, ], name='supervision_target_segments')

        self.keep_prob = tf.placeholder(name='keep_prob', dtype=tf.float32)

        self.learning_rate = tf.placeholder(name='lr', dtype=tf.float32)

    def build_predict_op(self):
        if not self.use_custom_prediction:
            predict_op = tf.nn.sigmoid(self.logits)
            self.predict_op = tf.round(predict_op, name='predict_op')
        elif self.use_gates:
            reduced = reduce(lambda x, y: tf.concat((x, y), axis=1), self.gates)
            reduced = tf.nn.sigmoid(reduced)
            reduced = tf.round(reduced)
            reduced = tf.reduce_sum(reduced, axis=1)

            ones = tf.ones_like(reduced)
            zeros = tf.zeros_like(reduced)

            self.predict_op = tf.where(reduced > 0, ones, zeros, name='predict_op')

            # TODO: attention

        else:
            raise RuntimeError('Could not build predict op! Check your flags...')

    def _build_vars(self, vocab_size, embedding_matrix=None):
        self.initializer = xavier_initializer()

        with tf.variable_scope('MemN2N'):

            if self.response_linearity:
                self.H = []
                if self.share_response:
                    H_var = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_1")
                    for hop in range(self.hops):
                        self.H.append(H_var)
                else:
                    for hop in range(self.hops):
                        self.H.append(
                            tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_{}".format(hop + 1)))

            if self.add_projection_layer:
                self.projection_H = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                                name='projection_H')

            if self.add_condensed_state:
                if self.embedding_dimension < 2 ** self.hops:
                    self.add_condensed_state = False
                    logger.info('Disabling condensed state due to high number of hops!')
                else:
                    self.D = []
                    current_dimension = self.embedding_dimension
                    for hop in range(self.hops):
                        self.D.append(
                            tf.Variable(self.initializer([current_dimension, int(current_dimension / 2)]),
                                        name="condensed_projection_{}".format(hop + 1)))
                        current_dimension = self.embedding_dimension + int(current_dimension / 2)

    def _calculate_similarity(self, memory, query, hop, scope='nn_attention'):
        # memory: [batch_size * memory, embedding_dim]
        # query: [batch_size, embedding_dim]

        apply_softmax = True

        # [batch_size * memory, embedding_dim]
        repeated_query = tf.gather(query, self.context_segments)

        if self.similarity_operation == 'dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1)
        elif self.similarity_operation == 'nn_attention':

            # [batch_size * memory, 2 * embedding_dim]
            att_input = tf.concat((memory, repeated_query), axis=1)

            if hop > 0:
                reuse = True
            else:
                reuse = None
            with tf.variable_scope(scope, reuse=reuse):
                # -> [batch_size * mem_size, 1]
                similarities = []

                for idx, weight in enumerate(self.similarity_weights):
                    if idx == 0:
                        dotted = tf.contrib.layers.fully_connected(att_input,
                                                                   weight,
                                                                   activation_fn=tf.nn.leaky_relu,
                                                                   scope="nn_att_{}".format(idx),
                                                                   reuse=reuse)
                    else:
                        dotted = tf.contrib.layers.fully_connected(similarities[-1],
                                                                   weight,
                                                                   activation_fn=None,
                                                                   scope="nn_att_{}".format(idx),
                                                                   reuse=reuse)
                    if idx < len(self.similarity_weights) - 1:
                        dotted = tf.nn.dropout(dotted, self.keep_prob)

                    similarities.append(dotted)

                # [batch_size * mem_size,]
                dotted = tf.squeeze(similarities[-1], axis=1)
        elif self.similarity_operation == 'scaled_dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted, apply_softmax

    def get_attentions_weights(self, x, batch_size=32):
        batches, num_batches = self._get_batches(data=x, batch_size=batch_size)
        attentions = {}
        # I know for sure that x is a list
        offset = 0
        memory_indices = []
        for sample in x[1]:
            samples_indices = list(range(offset, offset + len(sample)))
            memory_indices.append(samples_indices)
            offset += len(sample)

        to_feed = [self.probs]
        if self.use_gates:
            to_feed = [self.probs, self.gates]

        for batch_idx in range(num_batches):
            batch_x = [batches[item][batch_idx] for item in range(len(x))]

            feed_dict = self.get_feed_dict(x=batch_x,
                                           y=None,
                                           phase='test')

            batch_info = self.session.run(to_feed, feed_dict)
            batch_weights = batch_info[0]
            if self.use_gates:
                batch_gates = batch_info[1]

            if self.attention_type == 'sigmoid':
                batch_weights = [np.round(item) for item in batch_weights]

            # I cannot reshape to memory size if I have variable memory dimension per sample!
            # Instead, I can store indices
            # batch_weights = [item.reshape(-1, self.memory_size) for item in batch_weights]
            if batch_idx == 0:
                attentions.setdefault('memory_slots', []).extend(batch_weights)
                if self.use_gates:
                    attentions.setdefault('gates', []).extend(batch_gates)
            else:
                for idx, weight in enumerate(batch_weights):
                    attentions['memory_slots'][idx] = np.append(attentions['memory_slots'][idx], weight, axis=0)

                if self.use_gates:
                    for idx, gate in enumerate(batch_gates):
                        attentions['gates'][idx] = np.append(attentions['gates'][idx], gate, axis=0)

        # if self.attention_type == 'sigmoid':
        #     op = np.argwhere
        # else:
        #     op = np.argmax
        #
        # selected = np.array([[op(hop_weights[ind_slice])
        #                       for hop_weights in attentions['memory_slots']]
        #                      for ind_slice in memory_indices])

        # to_return = {
        #     'memory_slots': selected
        # }
        # if self.use_gates:
        #     to_return['hop_gates'] = np.array(attentions['gates']).reshape(attentions['gates'][0].shape[0],
        #                                                                    len(attentions['gates']))

        # return to_return
        return attentions, memory_indices

    def _compute_answer(self, final_response, initial_question=None):

        answers = []

        # [batch_size, 2 * embedding_dimension]
        if initial_question is not None:
            final_response = tf.concat((final_response, initial_question), axis=1)

        with tf.variable_scope('Answer'):

            for idx, weight in enumerate(self.answer_weights):
                if idx == 0:
                    dotted = tf.contrib.layers.fully_connected(final_response,
                                                               weight,
                                                               activation_fn=tf.nn.leaky_relu,
                                                               scope="answer_{}".format(idx),
                                                               reuse=False)
                else:
                    dotted = tf.contrib.layers.fully_connected(answers[-1],
                                                               weight,
                                                               activation_fn=tf.nn.leaky_relu,
                                                               scope="answer_{}".format(idx),
                                                               reuse=False)

                if idx < len(self.answer_weights) - 1:
                    dotted = tf.nn.dropout(dotted, self.keep_prob)

                answers.append(dotted)

            final_answer = tf.contrib.layers.fully_connected(answers[-1],
                                                             self.output_size,
                                                             activation_fn=None,
                                                             scope="final_answer",
                                                             reuse=False)

        return final_answer

    def _convert_input(self, data, data_len, scope, reuse=False):
        """
        Converts input via RNN network
        """

        # Data shape: [batch_size, sequence_length, embedding_dimension]
        # Data length shape: [batch_size]

        if self.bidirectional_embeddings:

            with tf.variable_scope(scope, reuse=reuse):
                fwd_rnn_cell = tf.contrib.rnn.GRUCell(int(self.embedding_dimension / 2))
                bwd_rnn_cell = tf.contrib.rnn.GRUCell(int(self.embedding_dimension / 2))
                _, outputs = tf.nn.bidirectional_dynamic_rnn(fwd_rnn_cell,
                                                             bwd_rnn_cell,
                                                             inputs=data,
                                                             sequence_length=data_len,
                                                             dtype=tf.float32)

                outputs = tf.concat((outputs[0].c, outputs[1].c), axis=1)
        else:

            with tf.variable_scope(scope, reuse=reuse):
                rnn_cell = tf.contrib.rnn.GRUCell(self.embedding_dimension)

                _, outputs = tf.nn.dynamic_rnn(rnn_cell,
                                               data,
                                               dtype=tf.float32,
                                               sequence_length=data_len
                                               )

                outputs = outputs.c

        # [batch_size, hidden_size]
        return outputs

    def get_output(self):
        # query: [batch_size, bow_size]
        # context: [batch_size * memory, bow_size]

        with tf.variable_scope('Output'):

            u = [self.query]

            if self.add_condensed_state:
                condensed_states = [self.query]

            self.probs = []
            self.gates = []
            self.dotted = []

            for hopn in range(self.hops):

                # [batch_size * mem_size, ]
                dotted, apply_softmax = self._calculate_similarity(memory=self.context, query=u[-1], hop=hopn)
                self.dotted.append(dotted)

                # Dynamic partition for segment-wise softmax
                dotted_mems = tf.dynamic_partition(dotted, self.context_segments, num_partitions=self.max_batch_size)

                filtered_mems = []

                def attention_method(mem):
                    if self.attention_type == 'softmax':
                        return tf.nn.softmax(mem)
                    elif self.attention_type == 'sparsemax':
                        return tf.reshape(sparsemax(tf.reshape(mem, shape=[1, tf.shape(mem)[0]])), shape=[-1])
                    else:
                        return tf.nn.sigmoid(mem)

                for mem in dotted_mems:
                    filtered = tf.cond(tf.equal(tf.size(mem), 0),
                                       true_fn=lambda: tf.zeros(shape=(1,), dtype=tf.float32),
                                       false_fn=lambda: attention_method(mem))
                    filtered_mems.append(filtered)

                filtered_mems = tf.concat(filtered_mems, axis=0)
                true_indexes = tf.range(0, tf.shape(self.context)[0])
                true_filtered_mems = tf.gather(filtered_mems, true_indexes)
                probs = tf.reshape(true_filtered_mems, shape=[-1])

                self.probs.append(probs)

                # [batch_size * mem_size, 1]
                probs_temp = tf.expand_dims(probs, -1)

                # [batch_size * mem_size, bow_size]
                # [batch_size * mem_size, 1]
                # -> [batch_size * mem_size, bow_size]
                # -> [batch_size, bow_size]
                o_k = self.context * probs_temp
                o_k = tf.segment_sum(o_k, self.context_segments)

                if self.use_gates:
                    # g(sim(u_k, m)) instead of g(u_k)
                    mem_gate = fully_connected(tf.reshape(dotted, shape=(-1, self.memory_size)),
                                               1,
                                               activation_fn=None,
                                               scope="mem_gate_{}".format(hopn + 1),
                                               reuse=tf.AUTO_REUSE)

                    self.gates.append(mem_gate)

                # Dont use projection layer for adj weight sharing
                if self.response_linearity:
                    if self.use_gates:
                        # u_k = mem_gate * o_k + tf.matmul(u[-1], self.H[hopn]) * (1 - mem_gate)
                        u_k = tf.nn.sigmoid(mem_gate) * o_k + tf.matmul(u[-1], self.H[hopn])
                    else:
                        u_k = tf.matmul(u[-1], self.H[hopn]) + o_k
                else:
                    if self.use_gates:
                        # u_k = mem_gate * o_k + u[-1] * (1 - mem_gate)
                        u_k = tf.nn.sigmoid(mem_gate) * o_k + u[-1]
                    else:
                        u_k = u[-1] + o_k

                u.append(u_k)

                if self.add_condensed_state:
                    new_condensed_state = tf.concat((u_k, tf.matmul(condensed_states[-1], self.D[hopn])))
                    condensed_states.append(new_condensed_state)

            if self.use_question_for_answer:
                initial_question = self.query
            else:
                initial_question = None

            if self.add_condensed_state:
                final_response = condensed_states[-1]
            else:
                final_response = u[-1]

            if self.use_cross_entropy:
                return self._compute_answer(final_response=final_response,
                                            initial_question=initial_question)
            else:
                return final_response

    def build_model(self, text_info):

        self.memory_size = text_info['memory_size']
        if self.use_max_margin_loss:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=None, embedding_matrix=None)

        self.context_encoding = None
        self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def build_loss(self):
        self.logits = self.get_output()
        self.loss_op = None
        if self.use_cross_entropy:
            cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name="cross_entropy")
            # reduce_mean should be more stable w.r.t. batch size
            cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
            self.cross_entropy_mean = cross_entropy_mean  # for visualization / debugging
            self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_l2 = loss_l2  # for visualization/debugging
            if self.loss_op is None:
                self.loss_op = loss_l2
            else:
                self.loss_op += loss_l2

        # given the gates and the true labels I know when the model should look over the memory
        # this is very similar to the answer loss -> by default I cannot say anything except for unfair clauses
        if self.use_gates and self.use_gate_loss:
            gate_losses = []
            # weights = tf.ones_like(self.answer)
            # mask = tf.zeros_like(self.answer)
            # weights = tf.where(tf.equal(self.answer, weights), weights, mask)
            for idx, gate in enumerate(self.gates):
                gate_loss = tf.nn.weighted_cross_entropy_with_logits(logits=gate,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name='gate_entropy_{}'.format(idx))
                # gate_loss = gate_loss * weights
                gate_loss = tf.reduce_mean(gate_loss)
                gate_losses.append(gate_loss)

            gate_ov_loss = tf.stack(gate_losses)
            gate_ov_loss = tf.reduce_mean(gate_ov_loss)

            self.gate_ov_loss = gate_ov_loss  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = gate_ov_loss
            else:
                self.loss_op += gate_ov_loss

        # TODO: weight positive samples with pos_weight?
        # TODO: weight based on memory frequency?
        if self.partial_supervision:

            if self.use_max_margin_loss:
                res_dotted = tf.reshape(self.dotted[-1], shape=[-1, self.memory_size])
                pos_scores = tf.gather_nd(res_dotted, self.positive_idxs)
                # pos_scores = tf.expand_dims(pos_scores, -1)
                neg_scores = tf.gather_nd(res_dotted, self.negative_idxs)
                supervision_losses = tf.maximum(0., self.margin - pos_scores + neg_scores)
                supervision_losses = tf.reduce_sum(supervision_losses) * self.partial_supervision_coeff
            else:
                # partial supervision:
                # for each hop select the minimum between each hot encoded target distribution:
                # the model is asked to just select one correct memory slot, and not necessarily all of them.
                def compute_supervision_loss(sim, target):

                    expanded_sim = tf.expand_dims(sim, 0)
                    expanded_sim = tf.tile(expanded_sim, multiples=(tf.shape(target)[0], 1))
                    sample_loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=target,
                                                                             logits=expanded_sim,
                                                                             name='supervision_loss_{}'.format(idx))

                    return tf.reduce_min(sample_loss)

                def pass_fake():
                    fake = tf.zeros(shape=(), dtype=tf.float32)
                    return fake

                supervision_losses = []
                for similarity in self.dotted:
                    hop_loss = []

                    sample_similarity = tf.dynamic_partition(data=similarity,
                                                             partitions=self.context_segments,
                                                             num_partitions=self.max_batch_size)
                    sample_targets = tf.dynamic_partition(data=self.targets,
                                                          partitions=self.target_segments,
                                                          num_partitions=self.max_batch_size)

                    for idx, (sim, target) in enumerate(zip(sample_similarity, sample_targets)):
                        # hack to avoid weights -> have to take into account when averaging!
                        filtered = tf.cond(tf.equal(tf.size(target), 0),
                                           true_fn=lambda: pass_fake(),
                                           false_fn=lambda: compute_supervision_loss(sim, target))
                        hop_loss.append(filtered)

                    hop_loss = tf.stack(hop_loss, axis=0)
                    true_indexes = tf.range(0, tf.shape(self.query)[0])
                    hop_loss = tf.gather(hop_loss, true_indexes)

                    # Masked average
                    mask = tf.ones(shape=tf.shape(hop_loss)[0])
                    zeros = tf.zeros(shape=tf.shape(hop_loss)[0])
                    mask = tf.where(tf.equal(hop_loss, 0), zeros, mask)
                    # Avoid division by zero
                    mask_sum = tf.reduce_sum(mask)
                    avg_hop_loss = tf.cond(tf.equal(mask_sum, 0),
                                           true_fn=lambda: tf.zeros(shape=()),
                                           false_fn=lambda: tf.reduce_sum(hop_loss) / mask_sum)
                    supervision_losses.append(avg_hop_loss)

                supervision_losses = tf.stack(supervision_losses)
                supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_coeff

            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses

    def get_feed_dict(self, x, y=None, phase='train'):
        # [batch_size, bow_size]
        query = x[0]

        # [batch_size, mem_size, bow_size]
        context = x[1]

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        context = context.reshape(-1, context.shape[-1])

        if phase == 'train':
            keep_prob = 1. - self.dropout_rate
        else:
            keep_prob = 1.0

        feed_dict = {self.context: context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.keep_prob: keep_prob,
                     self.learning_rate: self.optimizer_args['learning_rate']
                     }

        if self.partial_supervision:
            targets = x[2]
            targets = pad_data(targets)

            if self.use_max_margin_loss:
                positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
                negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))

                # Option A: select the first positive target
                for batch_idx, target in enumerate(targets):
                    pos_indexes = np.argwhere(target)
                    pos_indexes = pos_indexes.ravel()
                    if len(pos_indexes):
                        pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, pos_idx in enumerate(pos_choices):
                            positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                        neg_indexes = np.argwhere(np.logical_not(target))
                        neg_indexes = neg_indexes.ravel()
                        neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                        for choice_idx, neg_idx in enumerate(neg_choices):
                            negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]

                feed_dict[self.positive_idxs] = positive_indexes
                feed_dict[self.negative_idxs] = negative_indexes
            else:
                # build hot encodings
                split_targets = []
                target_segments = []
                count = 0
                for target in targets:
                    # Check enhanced target
                    if -1 not in target:
                        indexes = np.argwhere(target)
                        if len(indexes):
                            # Single target
                            hot_encoding = np.zeros(len(target))
                            hot_encoding[indexes[0]] = 1
                            split_targets.append(hot_encoding)
                            target_segments.append(count)
                            # Multiple targets
                            # for idx in indexes:
                            #     hot_encoding = np.zeros(len(target))
                            #     hot_encoding[idx] = 1
                            #     split_targets.append(hot_encoding)
                            #     target_segments.append(count)

                        else:
                            split_targets.append(target)
                            target_segments.append(count)
                    else:
                        target = [1 / len(target) for _ in range(len(target))]
                        split_targets.append(target)
                        target_segments.append(count)

                    count += 1

                feed_dict[self.targets] = split_targets
                feed_dict[self.target_segments] = target_segments

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        if self.use_cross_entropy:
            loss_values.append(self.cross_entropy_mean)
            loss_names.append('cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.use_gate_loss:
            loss_values.append(self.gate_ov_loss)
            loss_names.append('gate_loss')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


# TODO: documentation
class RRN_Network(Network):

    def __init__(self, embedding_dimension, optimizer_args, clip_gradient, add_gradient_noise,
                 add_nil_vars, timesteps, post_message_weights, message_weights,
                 input_weights, attention_weights, response_weights,
                 input_lstm_size, max_grad_norm=40, session=None, use_position_labels=False,
                 use_argument_labels=False, l2_regularization=None, dropout_rate=.4):
        super(RRN_Network, self).__init__(embedding_dimension=embedding_dimension,
                                          session=session)
        self.optimizer_args = optimizer_args
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.add_nil_vars = add_nil_vars
        self.max_grad_norm = max_grad_norm
        self.use_argument_labels = use_argument_labels
        self.use_position_labels = use_position_labels
        self.l2_regularization = l2_regularization
        self.timesteps = timesteps
        self.input_weights = input_weights
        self.attention_weights = attention_weights
        self.response_weights = response_weights
        self.message_weights = message_weights
        self.input_lstm_size = input_lstm_size
        self.post_message_weights = post_message_weights
        self.dropout_rate = dropout_rate
        self.output_size = 1

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, None], name="context")
        self.context_len = tf.placeholder(tf.int32, [None, ], name="context_len")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')

        self.edge_indices = tf.placeholder(tf.int32, shape=(None, 2), name='edge_indices')

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, None], name='query')
        self.query_len = tf.placeholder(tf.int32, shape=[None, ], name='query_len')

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.phase = tf.placeholder(tf.bool, name='batch_norm_phase')
        self.keep_prob = tf.placeholder(tf.float32, name='keep_prob')

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits[-1])
        self.predict_op = tf.round(predict_op, name='predict_op')

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)
        # self.grads_and_vars = grads_and_vars

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        if self.add_nil_vars:
            nil_grads_and_vars = []
            for g, v in grads_and_vars:
                if v.name in self._nil_vars:
                    nil_grads_and_vars.append((zero_nil_slot(g), v))
                else:
                    nil_grads_and_vars.append((g, v))
            self.train_op = self.opt.apply_gradients(nil_grads_and_vars, name="train_op")
        else:
            self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_model(self, text_info):

        self.max_sentence_length = text_info['sentence_max_length']
        self.max_memory_length = text_info['memory_max_length']
        self.max_question_length = text_info['sentence_max_length']

        self._build_inputs()
        self._build_vars(vocab_size=text_info['vocab_size'], embedding_matrix=text_info['embedding_matrix'])

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def _build_vars(self, vocab_size, embedding_matrix=None):

        nil_word_slot = tf.zeros([1, self.embedding_dimension])
        self.initializer = xavier_initializer()

        if embedding_matrix is None:
            A = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
        else:
            A = tf.concat(axis=0, values=[nil_word_slot, embedding_matrix])

        self.A = tf.Variable(A, name='Arguments_embedding')
        self._nil_vars = {self.A.name}

    def _convert_input(self, arguments, arguments_len, reuse=False, scope=None):
        # Input shape: # [batch_size, max_sentence_length, embedding_dimension]

        with tf.variable_scope(scope, reuse=reuse):
            forward_lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.input_lstm_size, reuse=reuse)
            backward_lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.input_lstm_size, reuse=reuse)

            _, outputs = tf.nn.bidirectional_dynamic_rnn(forward_lstm_cell,
                                                         backward_lstm_cell,
                                                         arguments,
                                                         dtype=tf.float32,
                                                         sequence_length=arguments_len
                                                         )

        # [batch_size, hidden_size * 2]
        outputs = tf.concat((outputs[0].c, outputs[1].c), axis=1)

        # [batch_size, hidden_size * 2]
        return outputs

    def compute_attention(self, x, weights, scope, reuse=False):

        attentions = []
        for idx, weight in enumerate(weights):
            with tf.variable_scope('{0}_attention_mask_{1}'.format(scope, idx)) as att_scope:

                if idx == 0:
                    attention = batch_norm_relu(x, weight, activation=True, reuse=reuse,
                                                scope=att_scope)
                elif idx < len(weights) - 1:
                    attention = batch_norm_relu(attentions[-1], weight, activation=True, reuse=reuse,
                                                scope=att_scope)
                else:
                    attention = batch_norm_relu(attentions[-1], weight, activation=True, reuse=reuse,
                                                scope=att_scope)

                attentions.append(attention)

        return attentions[-1]

    def build_loss(self):
        self.logits = self.get_output()

        entropies = []
        for logit in self.logits:
            cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logit,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name="cross_entropy")
            cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
            entropies.append(cross_entropy_mean)

        entropies = tf.stack(entropies, axis=0)
        entropies_mean = tf.reduce_mean(entropies, axis=0)

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n(
                [tf.nn.l2_loss(v) for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op = entropies_mean + loss_l2
        else:
            self.loss_op = entropies_mean

    def mlp(self, x, weights, scope, reuse):

        inputs = [x]

        with tf.variable_scope(scope):
            for idx, weight in enumerate(weights):

                if idx < len(weights) - 1:
                    current_input = batch_norm_relu(inputs=inputs[-1],
                                                    num_outputs=weight,
                                                    activation=True,
                                                    scope='{0}_{1}'.format(scope, idx),
                                                    reuse=reuse)
                    current_input = tf.nn.dropout(current_input, self.keep_prob)
                else:
                    current_input = batch_norm_relu(inputs=inputs[-1],
                                                    num_outputs=weight,
                                                    activation=False,
                                                    scope='{0}_{1}'.format(scope, idx),
                                                    reuse=reuse)

                inputs.append(current_input)

        return inputs[-1]

    def message_passing(self, nodes, edges, scope, reuse):

        # [#edges, 2, hidden_size * 2]
        pairs = tf.gather(nodes, edges)

        # [#edges, 4 * hidden_size]
        reshaped_pairs = tf.reshape(pairs, shape=[-1, 2 * pairs.shape[-1]])
        messages = self.mlp(x=reshaped_pairs,
                            weights=self.message_weights,
                            scope=scope,
                            reuse=reuse)

        # [#edges, 1]
        attention_weights = self.compute_attention(x=reshaped_pairs,
                                                   weights=self.attention_weights,
                                                   scope='attention',
                                                   reuse=reuse)

        messages = messages * attention_weights

        idx_i, _ = tf.split(edges, 2, 1)
        out_shape = (tf.shape(nodes)[0], messages.shape[-1])
        updates = tf.scatter_nd(idx_i, messages, out_shape)

        # [batch_size * memory_real_length, hidden_size * 2]
        return updates

    def get_output(self):
        mask_args_padding_zero_op = tf.scatter_update(self.A,
                                                      0,
                                                      tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

        with tf.control_dependencies([mask_args_padding_zero_op]):
            emb_query = tf.nn.embedding_lookup(self.A, self.query)
            emb_context = tf.nn.embedding_lookup(self.A, self.context)

        # [batch_size, hidden_size * 2]
        query_conv = self._convert_input(arguments=emb_query, arguments_len=self.query_len, scope='question_encoder')

        # [batch_size * memory, hidden_size * 2]
        context_conv = self._convert_input(arguments=emb_context, arguments_len=self.context_len,
                                           scope='context_encoder')

        initial_input = tf.concat([context_conv, tf.gather(query_conv, self.context_segments)], axis=1)
        initial_input = self.mlp(initial_input, scope='initial_encoding', weights=self.input_weights, reuse=False)

        current_inputs = [initial_input]

        # RRN
        responses = []

        with tf.variable_scope('steps'):
            lstm_cell = LSTMCell(initial_input.shape[-1])
            state = lstm_cell.zero_state(tf.shape(initial_input)[0], tf.float32)

            for timestep in range(self.timesteps):
                # [batch_size * memory_real_length, hidden_size * 2]
                current_input = self.message_passing(current_inputs[-1],
                                                     self.edge_indices,
                                                     reuse=True if timestep > 0 else False,
                                                     scope='message_passing')
                # [batch_size * memory_real_length, hidden_size * 4]
                current_input = self.mlp(x=tf.concat((current_input, current_inputs[0]), axis=1),
                                         reuse=True if timestep > 0 else False,
                                         scope='post_message_passing',
                                         weights=self.post_message_weights)

                # [batch_size * memory_real_length, hidden_size * 2]
                current_input, state = lstm_cell(current_input, state)

                with tf.variable_scope('graph_sum'):
                    graph_sum = tf.segment_sum(current_input, self.context_segments)
                    output = self.mlp(x=graph_sum,
                                      weights=self.response_weights,
                                      scope='graph_fn',
                                      reuse=True if timestep > 0 else False)
                    responses.append(output)

        # [batch_size, num_classes]
        return responses

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Build support placeholders
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        edge_indices = []

        offset = 0

        for item in x[1]:
            n_facts = len(item)
            edge_indices.extend([[i + offset, j + offset] for i in range(n_facts) for j in range(n_facts)])

        # Pad data
        flattened_context = []
        for item in context:
            flattened_context += item

        context_len = [len(item) for item in flattened_context]
        query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        if phase == 'train':
            bn_phase = True
            keep_prob = 1 - self.dropout_rate
        else:
            bn_phase = False
            keep_prob = 1.0

        feed_dict = {
            self.query: query,
            self.query_len: query_len,
            self.context: flattened_context,
            self.context_segments: context_segments,
            self.context_len: context_len,
            self.edge_indices: edge_indices,
            self.phase: bn_phase,
            self.keep_prob: keep_prob
        }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')

        loss, _ = self.session.run(
            [self.loss_op, self.train_op],
            feed_dict=feed_dict)

        return loss

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


# TODO: studia repository pytorch
class EntNet(Network):

    def __init__(self, num_blocks, optimizer, optimizer_args, l2_regularization=None,
                 clip_gradient=True, add_gradient_noise=True, max_grad_norm=40.0, **kwargs):
        super(EntNet, self).__init__(**kwargs)
        self.num_blocks = num_blocks
        self.l2_regularization = l2_regularization
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.max_grad_norm = max_grad_norm
        self.output_size = 1

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")
        self.context_len = tf.placeholder(tf.int32, [None], name="context_len")

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.query = tf.placeholder(tf.int32, shape=[None, self.sentence_size], name='query')
        self.query_len = tf.placeholder(tf.int32, shape=[None], name='query_len')

        self.keys = tf.placeholder(tf.int32, shape=[None, self.num_blocks], name='keys')

    def _build_vars(self, vocab_size, embedding_matrix=None):

        nil_word_slot = tf.zeros([1, self.embedding_dimension])
        self.initializer = xavier_initializer()

        if embedding_matrix is None:
            A = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
        else:
            A = tf.concat(axis=0, values=[nil_word_slot, embedding_matrix])

        self.A = tf.Variable(A, name='Arguments_embedding')

        self.H = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                             name='projection_H')

        self.P = tf.get_variable(
            name='positional_mask',
            shape=[self.sentence_size, self.embedding_dimension])

    def _get_input_encoding(self, inputs, initializer=None, scope=None):
        """
        Implementation of the learned multiplicative mask from Section 2.1, Equation 1.
        This module is also described in [End-To-End Memory Networks](https://arxiv.org/abs/1502.01852)
        as Position Encoding (PE). The mask allows the ordering of words in a sentence to affect the
        encoding.
        """
        with tf.variable_scope(scope, 'Encoding', initializer=initializer):
            encoded_input = tf.reduce_sum(inputs * self.P, axis=2)
            return encoded_input

    def _compute_answer(self, last_state, encoded_query, initializer):

        with tf.variable_scope('output', initializer=initializer):
            last_state = tf.stack(tf.split(last_state, self.num_blocks, axis=1), axis=1)

            # use the encoded query to attend over memories (dot product)
            attention = tf.reduce_sum(last_state * encoded_query, axis=2)

            # Subtract max for numerical stability (softmax is shift invariant
            attention_max = tf.reduce_max(attention, axis=-1, keep_dims=True)
            attention = tf.nn.softmax(attention - attention_max)
            attention = tf.expand_dims(attention, axis=2)

            # weight memories by attention vectors
            u = tf.reduce_sum(last_state * attention, axis=1)

            return tf.contrib.layers.fully_connected(tf.matmul(u, self.H),
                                                     1,
                                                     activation_fn=None,
                                                     scope='answer',
                                                     reuse=False)

    def get_output(self):

        ones_initializer = tf.constant_initializer(1.0)
        normal_initializer = tf.random_normal_initializer(stddev=0.1)

        mask_args_padding_zero_op = tf.scatter_update(self.A,
                                                      0,
                                                      tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

        with tf.control_dependencies([mask_args_padding_zero_op]):
            emb_query = tf.nn.embedding_lookup(self.A, self.query)
            emb_context = tf.nn.embedding_lookup(self.A, self.context)

        encoded_query = self._get_input_encoding(inputs=emb_query,
                                                 initializer=ones_initializer,
                                                 scope='query_encoding')

        encoded_context = self._get_input_encoding(inputs=emb_context,
                                                   initializer=ones_initializer,
                                                   scope='context_encoding')

        dmc = DynamicMemoryCell(
            num_blocks=self.num_blocks,
            num_units_per_block=self.embedding_dimension,
            keys=self.keys,
            initializer=normal_initializer,
            recurrent_initializer=normal_initializer,
            activation=tf.keras.layers.PReLU(alpha_initializer=ones_initializer)
        )

        # Recurrence
        initial_state = dmc.zero_state(batch_size=tf.shape(self.query)[0], dtype=tf.float32)
        _, last_state = tf.nn.dynamic_rnn(cell=dmc,
                                          inputs=encoded_context,
                                          sequence_length=self.context_len,
                                          initial_state=initial_state)

        outputs = self._compute_answer(last_state=last_state,
                                       encoded_query=encoded_query,
                                       initializer=normal_initializer)

        return outputs

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_model(self, text_info):

        # FIXME
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size, embedding_matrix=text_info['embedding_matrix'])

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = []
        for item in context:
            flattened_context += item

        context_len = [len(item) for item in flattened_context]
        query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.context_len: context_len,
                     self.query_len: query_len
                     }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def _batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss, _ = self.session.run([self.loss_op, self.train_op], feed_dict=feed_dict)
        return loss

    def _batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def _batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


# TODO: implementazione per bAbI -> input representation?
class DNC(Network):
    """
    Differential Neural Computer: reads one word at a time
    """

    def __init__(self, memory_size, word_size, num_read_heads,
                 num_write_heads, ctrl_hidden_size, clip_value=20, batch_size=16, **kwargs):
        super(DNC, self).__init__(**kwargs)
        self.memory_size = memory_size
        self.word_size = word_size
        self.num_read_heads = num_read_heads
        self.num_write_heads = num_write_heads
        self.ctrl_hidden_size = ctrl_hidden_size
        self.clip_value = clip_value
        self.batch_size = batch_size

    def _build_inputs(self):
        self.input_sequence = tf.placeholder(shape=[None, 1, self.embedding_dimension], name='input_sequence',
                                             dtype=tf.float32)
        self.answer = tf.placeholder(shape=[None, 1], name='answer', dtype=tf.float32)

    def get_output(self):
        dnc_core = custom_tf.DNC(memory_size=self.memory_size, word_size=self.word_size,
                                 num_reads=self.num_read_heads, num_writes=self.num_write_heads,
                                 ctrl_hidden_size=self.ctrl_hidden_size, clip_value=self.clip_value, output_size=1)
        initial_state = dnc_core.initial_state(self.batch_size)
        output_sequence, _ = tf.nn.dynamic_rnn(
            cell=dnc_core,
            inputs=self.input_sequence,
            time_major=True,
            initial_state=initial_state
        )

        return output_sequence
