"""

@Author: Federico Ruggeri

@Date: 29/11/18

"""

from utility.log_utils import get_logger
from preprocessing import BasePreprocessor, SimpleContextPreprocessor, SimpleMemoryPreprocessor, SimpleSupervisionMemoryPreprocessor

logger = get_logger(__name__)


class PreprocessingFactory(object):

    supported_preprocessors = {
        'base_preprocessor': BasePreprocessor,
        'simple_context_preprocessor': SimpleContextPreprocessor,
        'simple_memory_preprocessor': SimpleMemoryPreprocessor,
        'simple_supervised_memory_preprocessor': SimpleSupervisionMemoryPreprocessor
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if PreprocessingFactory.supported_preprocessors[key]:
            return PreprocessingFactory.supported_preprocessors[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
