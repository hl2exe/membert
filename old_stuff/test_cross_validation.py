"""

@Author: Federico Ruggeri

@Date: 27/03/2019

"""

import os
print(os.environ['PATH'])
print(os.environ['LD_LIBRARY_PATH'])
from datetime import datetime

import numpy as np

import const_define as cd
import reporters
from factories.data_loader_factory import DataLoaderFactory
from utility.json_utils import save_simplejson_to_file_from_path, load_json
from utility.log_utils import get_logger
from utility.test_utils import cross_validation
from utility.cross_validation_utils import PrebuiltCV
from keras.callbacks import EarlyStopping

logger = get_logger(__name__)


if __name__ == '__main__':

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))

    dataset_path = cd.DATASET_PATHS[cv_test_config['dataset']]

    data_base_path = os.path.join(dataset_path, cv_test_config['data_sub_folder'])
    labels_base_path = os.path.join(dataset_path, cv_test_config['labels_sub_folder'])

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))[cv_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    error_metrics = [
        "f1_score",
        "accuracy_score",
        "recall_score",
        "precision_score"
    ]
    error_metrics_additional_info = {
        "f1_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        },
        "recall_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        },
        "precision_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        }
    }

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    data_loader_info['data_base_path'] = data_base_path
    data_loader_info['labels_base_path'] = labels_base_path

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Callbacks
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    # annealer = ReduceLROnPlateau(**callbacks_data['reducelronplateau'])
    callbacks = [
        early_stopper,
        # annealer
    ]

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key='test')
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)
    list_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.txt'.format(cv_test_config['prebuilt_folds']))
    dataset_list = cv.load_dataset_list(load_path=list_path)

    scores = cross_validation(validation_percentage=cv_test_config['validation_percentage'],
                              data_handle=data_handle,
                              network_args=model_config,
                              model_type=cv_test_config['model_type'],
                              error_metrics=error_metrics,
                              error_metrics_additional_info=error_metrics_additional_info,
                              training_config=training_config,
                              dataset_list=dataset_list,
                              cv=cv,
                              callbacks=callbacks,
                              save_predictions=True,
                              use_tensorboard=cv_test_config['use_tensorboard'])

    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    base_path = os.path.join(cd.CV_DIR, cv_test_config['model_type'], current_date)

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    logger.info('Average validation f1: {}'.format(np.mean(scores[0]['f1_score'])))
    logger.info('Average test f1: {}'.format(np.mean(scores[1]['f1_score'])))

    save_path = os.path.join(base_path, 'scores.json')
    save_simplejson_to_file_from_path(filepath=save_path, data=scores[:2])

    reporters.save_model_info(folder=base_path, config_data=model_config)
    reporters.save_training_info(folder=base_path, training_info=training_config)

    save_simplejson_to_file_from_path(os.path.join(base_path, 'predictions.json'), scores[2])

    # for key, item in scores[-3].items():
    #     save_simplejson_to_file_from_path(os.path.join(base_path, '{}.json'.format(key)), item)
