"""

@Author: Federico Ruggeri

@Date: 22/07/2019

"""

from utility.log_utils import get_logger
from utility.preprocessing_utils import filter_line
from utility.bert_utils import preprocess_text

logger = get_logger(__name__)


class BasePreprocessor(object):
    """
    Base pre-processing interface
    """

    def parse(self, df, additional_info=None, return_info=False):
        if return_info:
            return df, additional_info
        else:
            return df


class TextPreprocessor(BasePreprocessor):
    """
    Simple sentence-level text preprocessor.
    Currently, the following filters are implemented:

        1) Sentence to lower
        2) Punctuation filtering
        3) Number replacing with constant
        4) Special words/artifacts removal
    """

    def __init__(self, function_names=None, **kwargs):
        super(TextPreprocessor, self).__init__(**kwargs)
        if function_names is None:
            self.function_names = ['sentence_to_lower',
                                   'punctuation_filtering',
                                   'number_replacing_with_constant',
                                   'remove_special_words']
        else:
            self.function_names = function_names

    def parse(self, df, additional_info=None, return_info=False, data_keys=['text']):
        for key in data_keys:
            df[key] = df[key].apply(lambda sentence: filter_line(line=sentence,
                                                                 function_names=self.function_names))

        if additional_info is not None:
            for key, item in additional_info.items():
                if type(item) == list:
                    additional_info[key] = [filter_line(sent) for sent in item]
                elif type(item) == dict:
                    additional_info[key] = {key: list(map(lambda item: filter_line(item), value)) for key, value in item.items()}

        if return_info:
            return df, additional_info
        else:
            return df


class BERTPreprocessor(BasePreprocessor):

    def __init__(self, remove_space=True, lower_case=True, **kwargs):
        super(BERTPreprocessor, self).__init__(**kwargs)
        self.remove_space = remove_space
        self.lower_case = lower_case

    def parse(self, df, additional_info=None, return_info=False, data_keys=['text']):
        for key in data_keys:
            df[key] = df[key].apply(lambda sentence: preprocess_text(sentence,
                                                                     remove_space=self.remove_space,
                                                                     lower=self.lower_case))

        if additional_info is not None:
            for key, item in additional_info.items():
                if type(item) == list:
                    additional_info[key] = [preprocess_text(sent,
                                                            remove_space=self.remove_space,
                                                            lower=self.lower_case)
                                            for sent in item]
                elif type(item) == dict:
                    additional_info[key] = {key: list(map(lambda x: preprocess_text(x,
                                                                                    remove_space=self.remove_space,
                                                                                    lower=self.lower_case), value))
                                            for key, value in item.items()}

        if return_info:
            return df, additional_info
        else:
            return df


class PreprocessingFactory(object):
    supported_preprocessors = {
        'base_preprocessor': BasePreprocessor,
        'text_preprocessor': TextPreprocessor,
        'bert_preprocessor': BERTPreprocessor
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if PreprocessingFactory.supported_preprocessors[key]:
            return PreprocessingFactory.supported_preprocessors[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
