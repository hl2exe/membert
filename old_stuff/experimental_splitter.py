"""

@Author: Federico Ruggeri

@Date: 24/09/2019

"""

from utility.log_utils import get_logger

logger = get_logger(__name__)


# TODO: documentation
# TODO: add test data
class BaseSplitter(object):

    def __init__(self, validation_split):
        self.validation_split = validation_split

    # TODO: if test is none then split x into train and test
    def split(self, x, y, x_test=None, y_test=None, text_info=None, return_text_info=False):
        x_samples = len([_ for _ in x])
        validation_samples = int(x_samples * self.validation_split)
        x_val = x.skip(x_samples - validation_samples)
        y_val = y.skip(x_samples - validation_samples)
        x_train = x.take(x_samples - validation_samples)
        y_train = y.take(x_samples - validation_samples)

        if x_test is not None:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
            else:
                return x_train, y_train, x_val, y_val, text_info, x_test, y_test
        else:
            if return_text_info:
                return x_train, y_train, x_val, y_val, text_info
            else:
                return x_train, y_train, x_val, y_val, text_info


class SplitterFactory(object):

    supported_splitters = {
        'base_splitter': BaseSplitter,
        'general_splitter': BaseSplitter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if SplitterFactory.supported_splitters[key]:
            return SplitterFactory.supported_splitters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))