"""

@Author: Federico Ruggeri

@Date: 29/05/2019

With this simple rule I get 620 unfair clauses out of 666 (coverage 92.3%)

"""

import os

from nltk import word_tokenize

from old_stuff import cleaner
import const_define as cd
from data_loader import Task1SingleKBLoader
from utility.json_utils import save_json

strong_supervision = {
    "9GAG, Inc may also impose limits on certain features and services or restrict Subscriber’s access to parts or all of the Services without notice or liability.": [
        1, 16],
    "You agree that neither 9GAG, Inc nor the Site will be liable in any event to you or any other party for any suspension, modification, discontinuance or lack of availability of the Site, the service, your Subscriber Content or other Content.": [
        1, 17],
    "To the fullest extent allowed by law, 9GAG, Inc disclaims any liability or responsibility for the accuracy, reliability, availability, completeness, legality or operability of the material or services provided on this Site.": [
        0, 16, 17],
    "By using this Site, you acknowledge that 9GAG, Inc is not responsible or liable for any harm resulting from (1) use of the Site; (2) downloading information contained on the Site including but not limited to downloads of content posted by subscribers; (3) unauthorized disclosure of images, information or data that results from the upload, download or storage of content posted by subscribers; (4) the temporary or permanent inability to access or retrieve any Subscriber Content from the Site, including, without limitation, harm caused by viruses, worms, trojan horses, or any similar contamination or destructive program.": [
        2, 3, 17, 18],
    "In no event shall 9GAG, Inc, its directors, officers, shareholders, employees or members be liable with respect to the Site or the Services for (a) any indirect, incidental, punitive, or consequential damages of any kind whatsoever; (b) damages for loss of use, profits, data, images, Subscriber Content or other intangibles; (c) damages for unauthorized use, non-performance of the Site, errors or omissions; or (d) damages related to downloading or posting Content.": [
        3, 10, 16, 17, 15],
    "9GAG, Inc's and the Site's collective liability under this agreement shall be limited to three hundred U.S. Dollars.": [
        4, 16],
    "9GAG, Inc shall not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond 9GAG, Inc’s reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation.": [
        3, 17],
    "IN NO EVENT WILL ACADEMIA.EDU’S AGGREGATE LIABILITY ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SITE, SERVICES OR COLLECTIVE CONTENT EXCEED THE GREATER OF FIFTY DOLLARS ($50) AND SUBSCRIPTION FEES PAID BY YOU DURING THE 12 MONTH PERIOD BEFORE THE ACT GIVING RISE TO THE LIABILITY.": [
        4, 17],
    "NEITHER ACADEMIA.EDU NOR ANY OTHER PERSON OR ENTITY INVOLVED IN CREATING, PRODUCING, OR DELIVERING THE SITE, SERVICES OR COLLECTIVE CONTENT WILL BE LIABLE FOR ANY INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES, OR LOST PROFITS, LOSS OF DATA OR LOSS OF GOODWILL, SERVICE INTERRUPTION, COMPUTER DAMAGE OR SYSTEM FAILURE OR THE COST OF SUBSTITUTE PRODUCTS OR SERVICES, OR FOR ANY DAMAGES FOR PERSONAL OR BODILY INJURY OR EMOTIONAL DISTRESS ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SITE, SERVICES OR COLLECTIVE CONTENT, OR FROM ANY COMMUNICATIONS, INTERACTIONS OR MEETINGS WITH OTHER USERS OF THE SITE OR SERVICES OR OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE OR SERVICES, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR ANY OTHER LEGAL THEORY, WHETHER OR NOT ACADEMIA.EDU HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, AND WHETHER OR NOT FORESEEABLE, EVEN IF A LIMITED REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.": [
        5, 3, 2, 10, 16, 17, 19, 15],
    "Airbnb is not responsible or liable for the availability or accuracy of such Third-Party Services, or the content, products, or services available from such Third-Party Services.": [
        6, 20, 17],
    "Except for our obligations to pay amounts to applicable Hosts pursuant to these Terms or an approved payment request under the Airbnb Host Guarantee, in no event will Airbnb’s aggregate liability arising out of or in connection with these Terms and your use of the Airbnb Platform including, but not limited to, from your publishing or booking of any Listings via the Airbnb Platform, or from the use of or inability to use the Airbnb Platform or Collective Content and in connection with any Accommodation, Experiences, Event or other Host Service, or interactions with any other Members, exceed the amounts you have paid or owe for bookings via the Airbnb Platform as a Guest in the twelve (12) month period prior to the event giving rise to the liability, or if you are a Host, the amounts paid by Airbnb to you in the twelve (12) month period prior to the event giving rise to the liability, or one hundred U.S. dollars (US$100), if no such payments have been made, as applicable.": [
        4, 16, 17],
    "Neither Airbnb nor any other party involved in creating, producing, or delivering the Airbnb Platform or Collective Content will be liable for any incidental, special, exemplary or consequential damages, including lost profits, loss of data or loss of goodwill, service interruption, computer damage or system failure or the cost of substitute products or services, or for any damages for personal or bodily injury or emotional distress arising out of or in connection with (i) these Terms, (ii) from the use of or inability to use the Airbnb Platform or Collective Content, (iii) from any communications, interactions or meetings with other Members or other persons with whom you communicate, interact or meet with as a result of your use of the Airbnb Platform, or (iv) from your publishing or booking of a Listing, including the provision or use of a Listing’s Host Services, whether based on warranty, contract, tort (including negligence), product liability or any other legal theory, and whether or not Airbnb has been informed of the possibility of such damage, even if a limited remedy set forth herein is found to have failed of its essential purpose.": [
        5, 3, 2, 9, 20, 16, 19, 17, 15],
    "Member also agrees that in case of the multiple use of your account or Member’s failure to maintain the security of your account, Alibaba.com shall not be liable for any loss or damages arising from such a breach and shall have the right to suspend or terminate Member’s account without liability to Member.": [
        7, 16],
    "Alibaba.com shall not be liable for damages or results arising from such disclosure, and Member agrees not to bring any action or claim against Alibaba.com for such disclosure.": [
        8, 16, 15],
    "Each User agrees that Alibaba.com shall not be liable or responsible for any damages, claims, liabilities, costs, harms, inconveniences, business disruptions or expenditures of any kind that may arise a result of or in connection with any Transaction Risks.": [
        3, 16],
    "In no event shall Alibaba.com and our affiliates be held liable for any such services or products.": [6, 16],
    "Each User hereby further agrees that Alibaba.com is not responsible and shall have no liability to you, for any material posted by others, including defamatory, offensive or illicit material and that the risk of damages from such material rests entirely with each User.": [
        6, 16, 20],
    "Alibaba.com shall not be liable for any special, direct, indirect, punitive, incidental or consequential damages or any damages whatsoever (including but not limited to damages for loss of profits or savings, business interruption, loss of information), whether in contract, negligence, tort, equity or otherwise or any other damages resulting from any of the following:a) the use or the inability to use the Sites or Services;b) any defect in goods, samples, data, information or services purchased or obtained from a User or any other third party through the Sites;c) violation of Third Party Rights or claims or demands that User's manufacture, importation, export, distribution, offer, display, purchase, sale and/or use of products or services offered or displayed on the Sites may violate or may be asserted to violate Third Party Rights; or claims by any party that they are entitled to defense or indemnification in relation to assertions of rights, demands or claims by Third Party Rights claimants;d) unauthorized access by third parties to data or private information of any User;e) statements or conduct of any User of the Sites; or;f) any matters relating to Services however arising, including negligence.": [
        3, 6, 10, 16],
    "Notwithstanding any of the foregoing provisions, the aggregate liability of Alibaba.com, our employees, agents, affiliates, representatives or anyone acting on our behalf with respect to each User for all claims arising from the use of the Sites or Services during any calendar year shall be limited to the greater of (a) the amount of fees the User has paid to Alibaba.com or our affiliates during the calendar year and (b) the maximum amount permitted in the applicable law.": [
        4, 16],
    "The limitations and exclusions of liability to you under the Terms shall apply to the maximum extent permitted by law and shall apply whether or not Alibaba.com has been advised of or should have been aware of the possibility of any such losses arising.": [
        0, 10, 16, 12],
    "Amazon takes no responsibility and assumes no liability for any content posted by you or any third party.": [6, 20, 17],
    "TO THE FULL EXTENT PERMISSIBLE BY LAW, AMAZON WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF ANY AMAZON SERVICE, OR FROM ANY INFORMATION, CONTENT, MATERIALS, PRODUCTS (INCLUDING SOFTWARE) OR OTHER SERVICES INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH ANY AMAZON SERVICE, INCLUDING, BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES, UNLESS OTHERWISE SPECIFIED IN WRITING.": [
        3, 16, 17],
    "We do not assume any liability for any content posted by you or any other 3rd party users of our website.": [6, 16, 17],
    "In conjunction with the Limitation of Warranties as explained above, you expressly understand and agree that any claim against us shall be limited to the amount you paid, if any, for use of products and/or services.": [
        4, 16],
    " Atlas Solutions Group Inc. will not be liable for any direct, indirect, incidental, consequential or exemplary loss or damages which may be incurred by you as a result of using our Resources, or as a result of any changes, data loss or corruption, cancellation, loss of access, or downtime to the full extent that applicable limitation of liability laws apply.": [
        3, 10, 16, 17, 15],
    "If you don’t, Badoo is not liable for any unauthorised access to your account.": [7, 17],
    "To the fullest extent permitted by law, Badoo expressly excludes:all conditions, representations, warranties and other terms which might otherwise be implied by statute, common law or the law of equity; andany liability incurred by you arising from use of Badoo, its services or these Terms, including without limitation for any claims, charges, demands, damages, liabilities, losses or expenses of whatever nature and howsoever direct, indirect, incidental, special, exemplary, punitive or consequential damages (however arising including negligence), loss of use, loss of data, loss caused by a computer or electronic virus, loss of income or profit, loss of or damage to property, wasted management or office time, breach of contract or claims of third parties or other losses of any kind or character, even if Badoo has been advised of the possibility of such damages or losses, arising out of or in connection with the use of Badoo.": [
        0, 3, 2, 10, 17, 18, 12, 13, 15],
    "Badoo’s total liability to you in respect of losses arising under or in connection with the Terms, whether in contract, tort (including negligence, breach of statutory duty, or otherwise) shall in no circumstances exceed £20.": [
        4, 16],
    "Badoo is not responsible for any damage to your computer hardware, computer software, or other equipment or technology including, but without limitation damage from any security breach or from any virus, bugs, tampering, fraud, error, omission, interruption, defect, delay in operation or transmission, computer line or network failure or any other technical or other malfunction.": [
        7, 2, 18],
    "This limitation on liability applies to, but is not limited to, the transmission of any disabling device or virus that may infect your equipment, failure or mechanical or electrical equipment or communication lines, telephone or other interconnect problems (e.g., you cannot access your internet service provider), unauthorized access, theft, bodily injury, property damage, operator errors, strikes or other labor problems or any act of god in connection with Badoo including, without limitation, any liability for loss of revenue or income, loss of profits or contracts, loss of business, loss of anticipated savings, loss of goodwill, loss of data, wasted management or office time and any other loss or damage of any kind, however arising and whether caused by tort (including, but not limited to, negligence), breach of contract or otherwise, even if foreseeable whether arising directly or indirectly.": [
        2, 7, 5, 3, 10, 9, 18, 19, 16, 12, 13, 15],
    "We expressly exclude, to the fullest extent permitted by law, all liability of betterpoints.uk, its directors, employees or other representatives, howsoever arising, for any loss suffered as a result of your use of this website or involvement in any way with betterpoints.uk.": [
        0, 3, 10, 16],
    "Betterpoints.uk is also not responsible or liable for any reputational goodwill damage or loss to you through being associated with betterpoints.uk by being a member, partner, merchant or any other involvement with betterpoints.uk.": [
        9, 10, 16],
    "You acknowledge that we are not responsible for suspension of the Service, regardless of the cause of the interruption or suspension.": [
        1, 17],
    "These are provided for your convenience and we are not responsible for and do not give any warranties or make any representations regarding any External Links and are not responsible for or liable in relation to the content or your use of any External Links.": [
        6, 20],
    "We are not responsible for and do not give any warranties or make any representations regarding any Causes that are introduced and do not give any warranties and are not responsible for or liable in relation to involvement in these Causes or any outcome from any involvement.": [16],
    "betterpoints.uk is also not responsible or liable for any reputational and goodwill damage or loss to you through being associated with betterpoints.uk by being a member, partner, merchant or any other involvement with betterpoints.uk within the area of Causes.": [
        9, 16],
    "betterpoints.uk  is also not responsible or liable for any reputational and goodwill damage or loss to you through being associated with betterpoints.uk by being a member, partner, merchant or any other involvement with betterpoints.uk within the area of Transfer of Funds.": [
        9, 16],
    "We expressly exclude, to the fullest extent permitted by law, all liability of betterpoints.uk, its directors, employees or other representatives, howsoever arising, for any loss suffered as a result of your use of this website or involvement in any way with betterpoints.uk._x000B_": [
        0, 10, 16],
    "In its capacity of intermediary, BlaBlaCar cannot be held liable for the effective occurrence of a Trip, and notably owing to:(i)    erroneous information communicated by the Driver in his Advert, or by any other means, with regard to the Trip and its terms;(ii)    cancellation or modification of a Trip by a Member;(iii)    non-payment of the Cost Contribution by a Passenger in the context of a Trip without Booking;(iv)    the behaviour of its Members during, before or after the Trip.": [
        6, 20],
    "Booking.com disclaims any liability or responsibility for any communication with the Supplier on or through its platform.": [
        6, 20],
    "We are not responsible or liable for (and have no obligation to verify) any wrong or misspelled email address or inaccurate or wrong (mobile) phone number or credit card number.": [
        6, 20],
    "Subject to the limitations set out in these terms and conditions and to the extent permitted by law, we shall only be liable for direct damages actually suffered, paid or incurred by you due to an attributable shortcoming of our obligations in respect to our services, up to an aggregate amount of the aggregate cost of your reservation as set out in the confirmation email (whether for one event or series of connected events).": [
        0, 4, 16],
    "However and to the extent permitted by law, neither we nor any of our officers, directors, employees, representatives, subsidiaries, affiliated companies, distributors, affiliate (distribution) partners, licensees, agents or others involved in creating, sponsoring, promoting, or otherwise making available the site and its contents shall be liable for (i) any punitive, special, indirect or consequential loss or damages, any loss of production, loss of profit, loss of revenue, loss of contract, loss of or damage to goodwill or reputation, loss of claim, (ii) any inaccuracy relating to the (descriptive) information (including rates, availability and ratings) of the Supplier as made available on our Platform, (iii) the services rendered or the products offered by the Supplier or other business partners, (iv) any (direct, indirect, consequential or punitive) damages, losses or costs suffered, incurred or paid by you, pursuant to, arising out of or in connection with the use, inability to use or delay of our Platform, or (v) any (personal) injury, death, property damage, or other (direct, indirect, special, consequential or punitive) damages, losses or costs suffered, incurred or paid by you, whether due to (legal) acts, errors, breaches, (gross) negligence, willful misconduct, omissions, non-performance, misrepresentations, tort or strict liability by or (wholly or partly) attributable to the Supplier or any of our other business partners (including any of their employees, directors, officers, agents, representatives or affiliated companies) whose products or service are (directly or indirectly) made available, offered or promoted on or through the Platform, including any (partial) cancellation, overbooking, strike, force majeure or any other event beyond our control.": [
        0, 3, 10, 6, 9, 5, 7, 11, 16, 20, 17, 19, 13],
    "TO THE EXTENT NOT PROHIBITED BY LAW, IN NO EVENT WILL BOX, ITS AFFILIATES, RESELLERS, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS OR LICENSORS BE LIABLE FOR: ANY INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, COVER OR CONSEQUENTIAL DAMAGES (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOST PROFITS, REVENUE, GOODWILL, USE OR CONTENT) HOWEVER CAUSED, UNDER ANY THEORY OF LIABILITY, INCLUDING, WITHOUT LIMITATION, CONTRACT, TORT, WARRANTY, NEGLIGENCE OR OTHERWISE, EVEN IF BOX HAS BEEN ADVISED AS TO THE POSSIBILITY OF SUCH DAMAGES.": [
        0, 3, 10, 0, 9, 16, 12],
    "THE AGGREGATE LIABILITY OF BOX AND ITS AFFILIATES, OFFICERS, RESELLERS, EMPLOYEES, AGENTS, SUPPLIERS OR LICENSORS, RELATING TO THE SERVICES WILL BE LIMITED TO THE GREATER OF: (A) ONE AND A HALF (1.5) TIMES THE MOST RECENT MONTHLY OR YEARLY FEE THAT YOU PAID FOR THAT SERVICE; OR (B) ONE HUNDRED DOLLARS ($100 U.S.D.). THE LIMITATIONS AND EXCLUSIONS ALSO APPLY IF THIS REMEDY DOES NOT FULLY COMPENSATE YOU FOR ANY LOSSES OR FAILS OF ITS ESSENTIAL PURPOSE.": [
        4],
    "SUBJECT TO APPLICABLE LAW, IN NO EVENT SHALL COUCHSURFING, OR OUR DIRECTORS, MEMBERS, EMPLOYEES OR AGENTS BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, INCLUDING, BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS OR LOSS OF DATA, WHETHER IN AN ACTION IN CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE) OR OTHERWISE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, ARISING FROM OR RELATING TO: (A) THE USE OR INABILITY TO USE OUR SERVICES; (B) THE COST OF REPLACEMENT OF ANY GOODS, SERVICES OR INFORMATION PURCHASED OR OBTAINED AS A RESULT OF ANY INFORMATION OBTAINED FROM OR TRANSACTIONS ENTERED INTO THROUGH OR FROM OUR SERVICES; (C) DISCLOSURE OF, UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR CONTENT; (D) DAMAGES FOR LOSS OR CORRUPTION OF DATA OR PROGRAMS, SERVICE INTERRUPTIONS OR PROCUREMENT OF SUBSTITUTE SERVICES, EVEN IF WE KNOW OR HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; (E) STATEMENTS, CONDUCT OR OMISSIONS OF ANY SERVICE PROVIDERS OR OTHER THIRD PARTY ON OUR SERVICES; (F) YOUR OR ANYONE ELSE’S CONDUCT OR ACTS IN CONNECTION WITH THE USE OF THE SERVICES; OR (G) ANY OTHER MATTER ARISING FROM, RELATING TO OR CONNECTED WITH OUR SERVICES OR THESE TERMS.": [
        0, 3, 10, 1, 7, 2, 6, 12, 15],
    "WE SHALL NOT BE LIABLE FOR ANY FAILURE OR DELAY IN PERFORMING UNDER THESE TERMS, WHETHER OR NOT SUCH FAILURE OR DELAY IS DUE TO CAUSES BEYOND OUR REASONABLE CONTROL.": [13],
    "IN NO EVENT WILL OUR AGGREGATE LIABILITY TO YOU OR ANY THIRD PARTY IN ANY MATTER ARISING FROM OR RELATING TO OUR SERVICES OR THESE TERMS EXCEED THE SUM OF ONE HUNDRED US DOLLARS ($100).": [
        4],
    "TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CROWDTANGLE, FACEBOOK, ITS AFFILIATES, AGENTS, DIRECTORS, EMPLOYEES, SUPPLIERS OR LICENSORS (“CROWDTANGLE PARTIES”) BE LIABLE FOR ANY INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES, ARISING OUT OF OR RELATING TO THE USE OF, OR INABILITY TO USE, THESE SERVICES. UNDER NO CIRCUMSTANCES WILL CROWDTANGLE PARTIES BE RESPONSIBLE FOR ANY DAMAGE, LOSS OR INJURY RESULTING FROM HACKING, TAMPERING OR OTHER UNAUTHORIZED ACCESS OR USE OF THE SERVICE OR YOUR ACCOUNT OR THE INFORMATION CONTAINED THEREIN. ": [
        0, 3, 9, 10, 5, 7, 15],
    "In those cases, CrowdTangle will not be held liable for any disruption of service.": [1],
    "WITH THE EXCEPTION OF ANY ACTS OR OMISSIONS OF WILLFUL MISCONDUCT, ANY INDEMNIFICATION OBLIGATIONS HEREUNDER, AND ANY CLAIM OF BREACH OF THE CONFIDENTIALITY OBLIGATIONS SET FORTH HEREIN, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, CROWDTANGLE PARTIES DISCLAIMS ALL LIABILITY, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE, GROSS NEGLIGENCE OR WILLFUL MISCONDUCT), PRODUCT LIABILITY, STRICT LIABILITY, STATUTORY LIABILITY, BREACH OF A FUNDAMENTAL TERM, FUNDAMENTAL BREACH, OR ANY OTHER LEGAL THEORY, FOR ANY LOSS OR DAMAGES OF ANY KIND (INCLUDING, WITHOUT LIMITATION ANY (I) DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE LOSS OR DAMAGE, (II) LOST PROFITS OR SAVINGS, (III) BUSINESS INTERRUPTION, (IV) LOSS OF PROGRAMS OR DATA (INCLUDING ANY CONTENT), (V) LOST REVENUE OR FAILURE TO REALIZE EXPECTED SAVINGS, (VI) LOSS OF USE, (VII) PERSONAL INJURY, (VIII) FINES, FEES, PENALTIES, OR (VI) ANY OTHER LOSSES OR DAMAGES WHETHER OR NOT CROWDTANGLE IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, RESULTING FROM THE USE OF, OR THE INABILITY TO MAKE USE OF, THE SERVICES. ": [0, 3, 5, 10, 12, 1, 11, 13, 15],
    "The DM Parties also disclaim all warranties, take no responsibility and assume no liability for the content present on the Dailymotion Service, including but not limited to any mistakes, defamation, libel, slander, omissions, falsehoods, obscenity, pornography or profanity You may encounter on the Dailymotion Service.": [6],
    "Nothing herein shall exclude or limit Dailymotion's liability for losses which may not be lawfully excluded or limited by applicable law.": [0],
    "Subject to this overall provision above, Dailymotion shall not be liable for: (a) any indirect or consequential losses which may be incurred by You.": [10],
    "This shall include; (i) any loss of profit (whether incurred directly or indirectly); (ii) any loss of goodwill or business reputation; (iii) any loss of opportunity; or (iv) any loss of data suffered; (b) any loss or damage which may be incurred as a result of: (i) any reliance placed by You on the completeness, accuracy or existence of any advertising, or as a result of any relationship or transaction between You and any advertiser or sponsor whose advertising appears on the Dailymotion Service; (ii) any changes which Dailymotion may make to the Dailymotion Service, or for any permanent or temporary cessation in the provision of the Dailymotion Service (or any of its features); (iii) the deletion of, corruption of, or failure to store, any content and other communications data maintained or transmitted by or through Your use of the Dailymotion Service; (iv) Your failure to provide Dailymotion with accurate account information (v) Your failure to keep Your password or your accounts details secure and confidential.": [10, 3, 9, 1, 6, 15],
    "The limitations on Dailymotion's liability defined in this section shall apply whether or not Dailymotion has been advised of or should have been aware of the possibility of any such losses arising.": [10, 12],
    "Subject as provided below, neither Deliveroo nor any Partner Restaurant shall have have any liability to you for any direct, indirect, special or consequential losses or damages arising in contract, tort (including negligence) or otherwise arising from your use of or your inability to use our Service.": [10, 3],
    "In the event that Deliveroo or the Partner Restaurant is found to be liable to you our total aggregate liability is limited to the purchase price of the Meals you have paid for in your order.": [4],
    "We will not be liable if, for any reason, our Site or our Service is unavailable at any time or for any period.": [1],
    "However, we will not be responsible for any errors or omissions in relation to such content or for any technical problems you may experience with our Site or our Service.": [1, 6],
    "To the extent permitted by law, we exclude all liability (whether arising in contract, in negligence or otherwise) for loss or damage which you or any third party may incur in connection with our Site, our Service, and any website linked to our Site and any materials posted on it.": [0, 6, 10, 3],
    "You agree that DeviantArt is and will not be liable to you for any modification, suspension or discontinuance of the Service.": [1],
    "EXCEPT FOR THE EXPRESS, LIMITED REMEDIES PROVIDED HEREIN, AND TO THE FULLEST EXTENT ALLOWED BY LAW, DEVIANTART SHALL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM USE OF THE SERVICE, INCLUDING BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, OR PUNITIVE DAMAGES, EVEN IF DEVIANTART HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.": [0, 3, 12],
    # "THE FOREGOING DISCLAIMERS, WAIVERS AND LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.",
    "IN NO EVENT SHALL DEVIANTART'S AGGREGATE LIABILITY TO YOU EXCEED THE AMOUNTS PAID BY YOU TO DEVIANTART PURSUANT TO THIS AGREEMENT.": [4],
    "Without limiting the generality of Section 9, DeviantArt shall have no responsibility or liability for the deletion or failure to store any Content maintained on the Service and you are solely responsible for creating back-ups of Your Content.": [10],
    "You cannot collect any damages from Diply for any reason (whether under legal theories of contract, tort, negligence, strict liability, operation of law or otherwise) that are not direct damages or exceed the amount paid by you to Diply hereunder for any goods or services that the claim arises from.": [4, 3],
    "IN COUNTRIES WHERE THE FOLLOWING TYPES OF EXCLUSIONS AREN'T ALLOWED, WE'RE RESPONSIBLE TO YOU ONLY FOR LOSSES AND DAMAGES THAT ARE A REASONABLY FORESEEABLE RESULT OF OUR FAILURE TO USE REASONABLE CARE AND SKILL OR OUR BREACH OF OUR CONTRACT WITH YOU.": [0],
    "IN COUNTRIES WHERE EXCLUSIONS OR LIMITATIONS OF LIABILITY ARE ALLOWED, DROPBOX, ITS AFFILIATES, SUPPLIERS OR DISTRIBUTORS WON'T BE LIABLE FOR: i) ANY INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, EXEMPLARY OR CONSEQUENTIAL DAMAGES, OR ii) ANY LOSS OF USE, DATA, BUSINESS, OR PROFITS, REGARDLESS OF LEGAL THEORY.": [0, 3, 10, 15],
    "THESE EXCLUSIONS OR LIMITATIONS WILL APPLY REGARDLESS OF WHETHER OR NOT DROPBOX OR ANY OF ITS AFFILIATES HAS BEEN WARNED OF THE POSSIBILITY OF SUCH DAMAGES.": [12],
    "OTHER THAN FOR THE TYPES OF LIABILITY WE CANNOT LIMIT BY LAW (AS DESCRIBED IN THIS SECTION), WE LIMIT OUR LIABILITY TO YOU TO THE GREATER OF $20 USD OR 100% OF ANY AMOUNT YOU'VE PAID UNDER YOUR CURRENT SERVICE PLAN WITH DROPBOX.": [0, 4],
    "Duolingo shall have no liability to you or any third party in the event that Duolingo exercises any such rights.": [14],
    "In no event will Duolingo be responsible for the actions or inactions of any third party payment processor, including, but not limited to, system downtime or payment service outages.": [6],
    "In the event that Duolingo suspends or terminates your use of the Service or these Terms and Conditions or you close your account voluntarily, you understand and agree that you will receive no refund or exchange of any kind, including for any unused virtual currency or other Virtual Item, any Content or data associated with your use of the Service, or for anything else.": [14],
    "You expressly acknowledge and agree that Duolingo shall not be responsible or liable, directly or indirectly, for any damage or loss arising from your use of any third-party website, service, or content.": [6],
    "IN NO EVENT WILL DUOLINGO BE LIABLE TO YOU OR ANY THIRD PARTY CLAIMING THROUGH YOU (WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY OR OTHER THEORY) FOR INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES ARISING OUT OF OR RELATING TO THE ACCESS OR USE OF, OR THE INABILITY TO ACCESS OR USE, THE SERVICE OR ANY PORTION THEREOF, INCLUDING BUT NOT LIMITED TO THE LOSS OF USE OF THE SERVICE, INACCURATE RESULTS, LOSS OF PROFITS, BUSINESS INTERRUPTION, OR DAMAGES STEMMING FROM LOSS OR CORRUPTION OF DATA OR DATA BEING RENDERED INACCURATE, THE COST OF RECOVERING ANY DATA, THE COST OF SUBSTITUTE SERVICES OR CLAIMS BY THIRD PARTIES FOR ANY DAMAGE TO COMPUTERS, SOFTWARE, MODEMS, TELEPHONES OR OTHER PROPERTY, EVEN IF DUOLINGO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. DUOLINGO�S LIABILITY TO YOU OR ANY THIRD PARTY CLAIMING THROUGH YOU FOR ANY CAUSE WHATSOEVER, AND REGARDLESS OF THE FORM OF THE ACTION, IS LIMITED TO THE AMOUNT PAID, IF ANY, BY YOU TO DUOLINGO FOR THE SERVICE IN THE 12 MONTHS PRIOR TO THE INITIAL ACTION GIVING RISE TO LIABILITY.": [3, 1, 10, 12, 2, 4, 15],
    "EA does not assume any responsibility or liability for UGC, for removing it, or not removing it or other Content.": [6],
    "IF YOU LIVE IN THE EEA OR SWITZERLAND, EA AND ITS EMPLOYEES, LICENSORS AND BUSINESS PARTNERS WILL NOT BE LIABLE TO YOU FOR ANY LOSSES OR DAMAGES ARISING FROM YOUR ACTIONS OR BREACH OF THIS AGREEMENT, OR WHICH ARISE AS A RESULT OF A THIRD PARTY'S (OR ANY OTHER) ACTS OR OMISSIONS BEYOND OUR CONTROL.": [13, 10, 3],
    "IF YOU LIVE OUTSIDE THE EEA AND SWITZERLAND, TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, EA AND ITS EMPLOYEES, LICENSORS AND BUSINESS PARTNERS SHALL NOT BE LIABLE TO YOU FOR ANY LOSSES THAT WERE NOT CAUSED BY EA'S BREACH OF THIS AGREEMENT, OR INDIRECT, INCIDENTIAL, CONSEQUENTIAL, PUNITIVE OR SPECIAL DAMAGES.": [0, 10, 3],
    "THE TYPES OF EXCLUDED DAMAGES INCLUDE, FOR EXAMPLE, FINANCIAL LOSS (SUCH AS LOSS INCOME OR PROFITS), COST OF SUBSTITUTE GOODS OR SERVICES, BUSINESS INTERRUPTION OR STOPPAGE, LOSS OF DATA, LOSS OF GOODWILL, AND COMPUTER FAILURE OR MALFUNCTION.": [3, 10, 1, 9, 15],
    # "THIS LIMITATION APPLIES TO ANY CLAIM ARISING OUT OF OR RELATED TO THIS LICENSE OR EA SERVICE, WHETHER BASED IN CONTRACT, TORT, STATUTE, STRICT LIABILITY OR OTHERWISE.",
    "IT ALSO APPLIES EVEN IF EA KNEW OR SHOULD HAVE KNOWN ABOUT THE POSSIBILITY OF SUCH DAMAGE.": [12],
    "YOU MAY RECOVER ONLY DIRECT DAMAGES IN ANY AMOUNT NO GREATER THAN WHAT YOU ACTUALLY PAID FOR THE APPLICABLE EA SERVICE.": [4],
    "We may also impose limits on certain Services or restrict your access to part or all of the Services without notice or liability.": [14],
    "Under no circumstances will MyFitnessPal be responsible for any loss or damage resulting from your reliance on nutritional information.": [10, 3],
    "As such, we are not liable for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such Social Networking Services.": [10, 3],
    "We reserve the right, in our absolute discretion, to modify or terminate any Free Trial offer, your access to the Premium Services during the Free Trial, or any of these terms without notice and with no liability.": [14],
    "To the maximum extent permitted by applicable law, you also expressly agree that we do not assume responsibility for any Third-Party Activity or any other race, contest, class, athletic activity or event that utilizes or is promoted by or accessed via the Services.": [6],
    "To the maximum extent permitted by applicable law, under no circumstances (including, without limitation, negligence) shall Under Armour, its subsidiaries, partners or any wireless carriers be liable to you or any third party for (a) any indirect, incidental, special, reliance, exemplary, punitive, or consequential damages of any kind whatsoever; (b) loss of profits, revenue, data, use, goodwill, or other intangible losses; (c) damages relating to your access to, use of, or inability to access or use the Services; (d) damages relating to any conduct or content of any third party or athlete using the Services, including without limitation, defamatory, offensive or illegal conduct or content; and/or (e) damages in any manner relating to any Third-Party Content, Third-party Products or Third-Party Activities accessed via the Services.": [0, 3, 10, 6, 9, 1, 15],
    "To the maximum extent permitted by applicable law, this limitation applies to all claims, whether based on warranty, contract, tort, or any other legal theory, whether or not Under Armour has been informed of the possibility of such damage, and further where a remedy set forth herein is found to have failed its essential purpose.": [0, 4, 12, 3],
    "To the maximum extent permitted by applicable law, the total liability of Under Armour, for any claim under these Terms, including for any implied warranties, is limited to the greater of one thousand dollars (us $1,000.00) or the amount you paid us to use the applicable Service(s) in the past twelve months.": [0, 4],
    "In particular, to the extent permitted by applicable law, we are not liable for any claims arising out of (a) your use of the Services (including but not limited to your participation in any activities promoted by or accessed via the Services), (b) the use, disclosure, display, or maintenance of an athlete�s Personal Data and/or Location Data, (c) any other interactions with us or any other athletes using the Services, even if we have been advised of the possibility of such damages, or (d) other Content, information, services or goods received through or advertised on the Services or received through any links provided with the Services.": [0, 12, 3, 6, 8],
    "Except as otherwise set out in these Terms, and to the maximum extent permitted by applicable law, we are not responsible or liable, either directly or indirectly, for any injuries or damages that are sustained from your physical activities or your use of, or inability to use, any Services or features of the Services, including any Content or activities that you access or learn about through our Services (e.g., a Third-Party Activity such as a yoga class), even if caused in whole or part by the action, inaction or negligence of Under Armour or by the action, inaction or negligence of others.": [0, 3, 5, 6],
    "You also acknowledge that a variety of Evernote actions may impair or prevent you from accessing your Content or using the Service at certain times and/or in the same way, for limited periods or permanently, and agree that Evernote has no responsibility or liability as a result of any such actions or results, including, without limitation, for the deletion of, or failure to make available to you, any Content.": [1],
    "You agree that we shall not be liable to you or to any third party for any modification, suspension or discontinuance of any part of the Service.": [1],
    "Some advertising or other messaging content we provide will be based upon information provided by third parties, and we shall not be responsible or liable for any loss or damage of any sort incurred by you as a result of any advertisements or other messages.": [6],
    "We may have little or no control over such sites or developers and, accordingly, you acknowledge and agree that (i) we are not responsible for the availability of such external sites, content or applications; (ii) we are not responsible or liable for any content or other materials or performance available from such sites or applications and (iii) we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, materials or applications.": [6],
    "ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER OR OTHER DEVICE OR LOSS OF DATA RESULTING FROM THE DOWNLOAD OR USE OF ANY SUCH MATERIAL.": [2, 15],
    "YOU EXPRESSLY UNDERSTAND AND AGREE THAT EVERNOTE, ITS SUBSIDIARIES, AFFILIATES, SERVICE PROVIDERS, AND LICENSORS, AND OUR AND THEIR RESPECTIVE OFFICERS, EMPLOYEES, AGENTS AND SUCCESSORS SHALL NOT BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA, COVER OR OTHER INTANGIBLE LOSSES (EVEN IF EVERNOTE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES) RESULTING FROM: (i) THE USE OR THE INABILITY TO USE THE SERVICE OR TO USE PROMOTIONAL CODES OR EVERNOTE POINTS; (ii) THE COST OF PROCUREMENT OF SUBSTITUTE SERVICES RESULTING FROM ANY DATA, INFORMATION OR SERVICE PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO THROUGH OR FROM THE SERVICE; (iii) UNAUTHORIZED ACCESS TO OR THE LOSS, CORRUPTION OR ALTERATION OF YOUR TRANSMISSIONS, CONTENT OR DATA; (iv) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON OR USING THE SERVICE, OR PROVIDING ANY SERVICES RELATED TO THE OPERATION OF THE SERVICE; (v) EVERNOTE�S ACTIONS OR OMISSIONS IN RELIANCE UPON YOUR BASIC SUBSCRIBER INFORMATION AND ANY CHANGES THERETO OR NOTICES RECEIVED THEREFROM; (vi) YOUR FAILURE TO PROTECT THE CONFIDENTIALITY OF ANY PASSWORDS OR ACCESS RIGHTS TO YOUR ACCOUNT; (vii) THE ACTS OR OMISSIONS OF ANY THIRD PARTY USING OR INTEGRATING WITH THE SERVICE; (viii) ANY ADVERTISING CONTENT OR YOUR PURCHASE OR USE OF ANY ADVERTISED OR OTHER THIRD-PARTY PRODUCT OR SERVICE; (ix) THE TERMINATION OF YOUR ACCOUNT IN ACCORDANCE WITH THE TERMS OF THESE TERMS OF SERVICE; OR (x) ANY OTHER MATTER RELATING TO THE SERVICE.": [3, 9, 15, 10, 6, 1],
    # "You agree that regardless of any statute or law to the contrary or the applicable dispute resolution process, any claim or cause of action you may have arising out of or related to use of the Service or otherwise under these must be filed within one (1) year after such claim or cause of action arose or you hereby agree to be forever barred from bringing such claim."
}

common_kb_words = ['provider', 'liable', 'liability', 'suspension', 'modification',
                   'discontinuance', 'limitation', 'services', 'features',
                   'harm', 'damage', 'hardware', 'software', 'contamination', 'destructive', 'program',
                   'negligence', 'failure', 'profits', 'clause', 'compensation', 'injury', 'death',
                   'third', 'parties', 'loss', 'blanket', 'fullest', 'permissible', 'legality', 'availability',
                   'usability', 'physical', 'injuries', 'personal', 'actions', 'services', 'losses', 'damages']


def check_common_kb_words(sentence):
    tokens = word_tokenize(sentence)
    checks = sum([common in tokens for common in common_kb_words])
    return checks


data_base_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'ToS_100', 'sentences')
labels_base_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'ToS_100', 'labels_LTD')

loader = Task1SingleKBLoader()
data_handle = loader.load(data_base_path=data_base_path, labels_base_path=labels_base_path, category='LTD')

unfair = {}

for doc, item in data_handle.input_data.items():
    for sent, label in zip(item, data_handle.labels[doc]):
        if label > 0:
            unfair[sent] = label

print('Found: {}'.format(len(unfair)))
unfair_sentences = list(unfair.keys())

common_check = {}
for sentence in unfair_sentences:
    check = check_common_kb_words(sentence)
    if check > 0:
        common_check[sentence] = check_common_kb_words(sentence)
    else:
        print('Removed: {}'.format(sentence))

print()
print()
print('Remaining: {}'.format(len(common_check)))
for sent, matches in common_check.items():
    print('[{0}] {1}'.format(matches, sent))

print()
print()
print('Evaluating strong supervision!')
print('Total: {}'.format(len(strong_supervision)))
count = 0
for key, item in strong_supervision.items():
    key = cleaner.filter_line(key)
    check = check_common_kb_words(key)
    if check > 0:
        count += 1
print('Success: {}'.format(count))

save_path = os.path.join(cd.LOCAL_DATASETS_DIR, cd.KB_DIR, 'LTD_targets.json')
save_json(path=save_path, data=strong_supervision)