"""

@Author: Federico Ruggeri

@Date: 30/11/2018

"""

import os

import const_define as cd
from calibrators import HyperOptCalibrator
from old_stuff.data_loader_factory import DataLoaderFactory
from utility.json_utils import load_json
from utility.test_utils import loo_test

if __name__ == '__main__':

    # Step 1: Validator config

    loo_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_LOO_TEST_CONFIG_NAME))

    dataset_path = cd.DATASET_PATHS[loo_test_config['dataset']]

    data_base_path = os.path.join(dataset_path, loo_test_config['data_sub_folder'])
    labels_base_path = os.path.join(dataset_path, loo_test_config['labels_sub_folder'])

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))[loo_test_config['model_type']]
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    data_loader_info['data_base_path'] = data_base_path
    data_loader_info['labels_base_path'] = labels_base_path

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Step 2: Calibrator config

    calibrator_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALIBRATOR_INFO_NAME))

    validator_base_args = {
        'validation_percentage': loo_test_config['validation_percentage'],
        'data_handle': data_handle,
        'network_args': model_config,
        'model_type': loo_test_config['model_type'],
        'compute_test_info': False,
        'training_config': training_config
    }

    calibrator = HyperOptCalibrator(model_type=loo_test_config['model_type'],
                                    validator_method=loo_test,
                                    validator_base_args=validator_base_args,
                                    **calibrator_config)

    space = calibrator.load_space()
    calibrator.run(space=space)
