"""

@Author: Federico Ruggeri

@Date: 17/12/2018

"""

import numpy as np
import tensorflow as tf
from tensorflow.contrib.layers import fully_connected, batch_norm


def position_encoder_extra(embedding_size, sentence_size):
    """
    Position encoding giving more weight to external sentence words.
    """

    encoding = np.ones((embedding_size, sentence_size), dtype=np.float32)
    # 1 base indexing
    for k in range(1, embedding_size + 1):
        for j in range(1, sentence_size + 1):
            encoding[k - 1, j - 1] = (1 - j / sentence_size) - (k / embedding_size) * (1 - 2 * j / sentence_size)

    # make last word immune to encoding
    encoding[:, -1] = 1.0

    return np.transpose(encoding)


def position_encoding_intra(sentence_size, embedding_size):
    """
    Position Encoding giving more weight to internal sentence words.
    """
    encoding = np.ones((embedding_size, sentence_size), dtype=np.float32)
    ls = sentence_size + 1
    le = embedding_size + 1
    for i in range(1, le):
        for j in range(1, ls):
            encoding[i - 1, j - 1] = (i - (embedding_size + 1) / 2) * (j - (sentence_size + 1) / 2)
    encoding = 1 + 4 * encoding / embedding_size / sentence_size

    # Make position encoding of time words identity to avoid modifying them
    encoding[:, -1] = 1.0
    return np.transpose(encoding)


def zero_nil_slot(t, name=None):
    """
    Overwrites the nil_slot (first row) of the input Tensor with zeros.
    The nil_slot is a dummy slot and should not be trained and influence
    the training algorithm.
    """

    with tf.name_scope(name, "zero_nil_slot", values=[t]) as name:
        t = tf.convert_to_tensor(t, name="t")
        s = tf.shape(t)[1]
        z = tf.zeros(tf.stack([1, s]))
        return tf.concat(axis=0, values=[z, tf.slice(t, [1, 0], [-1, -1])], name=name)


def add_gradient_noise(t, stddev=1e-3, name="add_gradient_noise"):
    """
    Adds gradient noise as described in http://arxiv.org/abs/1511.06807 [2].
    The input Tensor `t` should be a gradient.
    The output will be `t` + gaussian noise.
    0.001 was said to be a good fixed value for memory networks [2].
    """

    with tf.name_scope(name) as name:
        t = tf.convert_to_tensor(t, name="t")
        gn = tf.random.normal(tf.shape(t), stddev=stddev)
        return tf.add(t, gn, name=name)


def batch_norm_relu(inputs, num_outputs, phase, scope=None, activation=True, reuse=False):
    """
    Batch Normalization as described in https://arxiv.org/abs/1502.03167
    """

    with tf.variable_scope(scope, reuse=reuse):
        h1 = fully_connected(inputs, num_outputs, activation_fn=None, scope="dense")
        h2 = batch_norm(h1, decay=0.95, center=True, scale=True,
                        is_training=phase, scope='bn', updates_collections=None)
        if activation:
            out = tf.nn.relu(h2, 'relu')
        else:
            out = h2

    return out


def scaled_dot_product_attention(q, k, v, mask):
    """Calculate the attention weights.
    q, k, v must have matching leading dimensions.
    k, v must have matching penultimate dimension, i.e.: seq_len_k = seq_len_v.
    The mask has different shapes depending on its type(padding or look ahead)
    but it must be broadcastable for addition.

    Args:
      q: query shape == (..., seq_len_q, depth)
      k: key shape == (..., seq_len_k, depth)
      v: value shape == (..., seq_len_v, depth_v)
      mask: Float tensor with shape broadcastable
            to (..., seq_len_q, seq_len_k). Defaults to None.

    Returns:
      output, attention_weights
    """

    matmul_qk = tf.matmul(q, k, transpose_b=True)  # (..., seq_len_q, seq_len_k)

    # scale matmul_qk
    dk = tf.cast(tf.shape(k)[-1], tf.float32)
    scaled_attention_logits = matmul_qk / tf.math.sqrt(dk)

    # add the mask to the scaled tensor.
    if mask is not None:
        scaled_attention_logits += (tf.cast(mask, tf.float32) * tf.constant(-1e9))

    # softmax is normalized on the last axis (seq_len_k) so that the scores
    # add up to 1.
    attention_weights = tf.nn.softmax(scaled_attention_logits, axis=-1)  # (..., seq_len_q, seq_len_k)

    output = tf.matmul(attention_weights, v)  # (..., seq_len_q, depth_v)

    return output, attention_weights


def point_wise_feed_forward_network(x, d_model, dff):
    with tf.variable_scope('PWFFN', tf.AUTO_REUSE):
        layer1 = tf.contrib.layers.fully_connected(x,
                                                   dff,
                                                   activation_fn=tf.nn.relu,
                                                   scope="pwffn_1",
                                                   reuse=tf.AUTO_REUSE)
        layer2 = tf.contrib.layers.fully_connected(layer1,
                                                   d_model,
                                                   activation_fn=None,
                                                   scope="pwffn_2",
                                                   reuse=tf.AUTO_REUSE)
        return layer2


def get_angles(pos, i, d_model):
    angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(d_model))
    return pos * angle_rates


def positional_encoding(position, d_model):
    """
    Positional encoding adopted in the Transformer implementation
    as described in https://www.tensorflow.org/tutorials/text/transformer
    """

    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                            np.arange(d_model)[np.newaxis, :],
                            d_model)

    # apply sin to even indices in the array; 2i
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

    # apply cos to odd indices in the array; 2i+1
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

    pos_encoding = angle_rads[np.newaxis, ...]

    return tf.cast(pos_encoding, dtype=tf.float32)

