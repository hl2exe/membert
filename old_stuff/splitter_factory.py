"""

@Author: Federico Ruggeri

@Date: 15/12/18

"""

from utility.log_utils import get_logger
from splitter import BaseSplitter, MemorySplitter, DMNSplitter, KeyValueSplitter, GeneralSplitter, ClausesSplitter

logger = get_logger(__name__)


class SplitterFactory(object):

    supported_splitters = {
        'base_splitter': BaseSplitter,
        'memory_splitter': MemorySplitter,
        'keyvalue_splitter': KeyValueSplitter,
        'general_splitter': GeneralSplitter,
        'dmn_splitter': DMNSplitter,
        'clauses_splitter': ClausesSplitter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if SplitterFactory.supported_splitters[key]:
            return SplitterFactory.supported_splitters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
