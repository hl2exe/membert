"""

@Author: Federico Ruggeri

@Date: 22/07/19

Core part of Tensorflow 1 framework. All neural network models are defined in this file.
In particular, each model follows the general Network superclass behaviour (this is just a Keras work-around for TF1).

TODO: finish implementing EntNet
TODO: finish implementing DNC (what's bAbI version input?)
TODO: fix BERT memn2n

"""

import math

import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from keras import backend as K
from keras import optimizers
from keras import regularizers
from keras.initializers import Constant
from keras.layers import Bidirectional, GRU
from keras.layers import Embedding, Dropout, LSTM, Dense, Conv1D, MaxPooling1D
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from tensorflow.contrib.layers import xavier_initializer, fully_connected, layer_norm
from tensorflow.contrib.rnn import LSTMCell, GRUCell, OutputProjectionWrapper
from tensorflow.contrib.seq2seq import LuongAttention, AttentionWrapper
from tensorflow.contrib.sparsemax import sparsemax
from tensorflow.python.keras import initializers
from tqdm import tqdm

# from custom_tf_v1 import DynamicMemoryCell
import custom_tf_v1
from utility.cross_validation_utils import build_metrics, compute_metrics
from utility.embedding_utils import pad_data
from utility.log_utils import get_logger
from utility.python_utils import merge
from old_stuff.tensorflow_utils_v1 import add_gradient_noise, zero_nil_slot
from old_stuff.tensorflow_utils_v1 import batch_norm_relu
from old_stuff.tensorflow_utils_v1 import position_encoder_extra, position_encoding_intra, scaled_dot_product_attention, \
    point_wise_feed_forward_network
from old_stuff.tensorflow_utils_v1 import positional_encoding

logger = get_logger(__name__)


# TODO: add relation network
# TODO: add recurrent relation network
# TODO: add DNC
# TODO: add EntNet


class Network(object):
    """
    Simple Keras-compliant work-around interface that allows to define neural network models with a behaviour
    comparable to Keras ones.

    In particular, models following the sequential or functional Keras APIs can extend this class without any
    additional change, since each class method wraps-up Keras APIs. For example, the fit() routine checks if the
    model is Keras compliant or not: if yes, the Keras.fit() routine is called, a custom TF1-compliant fit()
    routine is called otherwise.

    Overall, the Network superclass wrapper offers the following features:

        1) Model Save/Load
        2) Training (fit())
        3) Evaluation (evaluate())
        4) Inference (predict()
        5) Batch split (get_batches())

    Each routine can be juxtaposed with Keras-like callbacks for enhanced behaviour.

    Moreover, batch-level routines (batch_fit(), batch_evaluate(), batch_predict()) are defined, which allow total
    control.

    """

    def __init__(self, embedding_dimension, session=None, additional_data=None, name='network'):

        self.model = None
        self.class_weights = None
        self.embedding_dimension = embedding_dimension
        self.pos_weight = None
        self.num_classes = None
        self.additional_data = additional_data
        self.name = name
        if session is None:
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            config.gpu_options.per_process_gpu_memory_fraction = 0.6
            # config.device_count.update({'GPU': 0})
            K.set_session(tf.Session(config=config))
            self.session = K.get_session()
        else:
            self.session = session

    def get_weights(self):
        if self.model is not None:
            return self.model.get_weights()
        else:
            vars = tf.trainable_variables()
            values = self.session.run(vars)
            return values

    def get_named_weights(self):
        if self.model is not None:
            return self.model.get_weights()
        else:
            vars = tf.trainable_variables()
            return vars

    def save(self, filepath, overwrite=True):
        if self.model is not None:
            return self.model.save(filepath=filepath, overwrite=overwrite)
        else:
            saver = tf.train.Saver()
            saver.save(sess=self.session, save_path=filepath)

    def load(self, filepath):
        if self.model is not None:
            return load_model(filepath=filepath)
        else:
            saver = tf.train.Saver()
            return saver.restore(sess=self.session, save_path=filepath)

    def set_weights(self, weights):
        if self.model is not None:
            self.model.set_weights(weights)
        else:
            vars = tf.trainable_variables()
            for var, value in zip(vars, weights):
                var.load(value, self.session)

    def get_attentions_weights(self, x, batch_size=32):
        return None

    def compute_output_weights(self, y_train, num_classes):

        self.num_classes = num_classes
        # single label scenario
        if self.model is None:
            if num_classes == 1:
                if len(y_train[y_train == 1]) > 0:
                    self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                else:
                    self.pos_weight = 1
            else:
                if type(y_train[0]) not in [list, np.ndarray]:
                    self.pos_weight = [len(y_train[y_train != cat]) / len(y_train[y_train == cat])
                                       for cat in range(num_classes)]
                else:
                    self.pos_weight = [len(y_train[y_train[:, cat] == 0]) / len(y_train[y_train[:, cat] == 1])
                                       for cat in range(num_classes)]
        else:
            if num_classes == 1:
                self.pos_weight = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
                self.class_weights = {0: 1, 1: self.pos_weight}
            else:
                raise RuntimeError('Not implemented yet')

    def build_model(self, text_info):
        raise NotImplementedError()

    def batch_fit(self, x, y):
        raise NotImplementedError()

    def batch_predict(self, x):
        raise NotImplementedError()

    def batch_evaluate(self, x, y):
        raise NotImplementedError()

    def get_batches(self, data, batch_size=32, num_batches=None):

        return_count = False

        if type(data) is list:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data[0]) / batch_size)

            batches = []
            for item_idx in range(len(data)):
                batches.append([data[item_idx][idx * batch_size: (idx + 1) * batch_size]
                                for idx in range(num_batches)])
        elif type(data) is np.ndarray:
            if num_batches is None:
                return_count = True
                num_batches = math.ceil(len(data) / batch_size)
            batches = [data[idx * batch_size: (idx + 1) * batch_size] for idx in range(num_batches)]
        else:
            if num_batches is None:
                return_count = True
                data_keys = list(data.keys())
                num_batches = math.ceil(len(data[data_keys[0]]) / batch_size)

            batches = {}
            for key, item in data.items():
                batches[key] = [item[idx * batch_size: (idx + 1) * batch_size] for idx in range(num_batches)]

        if return_count:
            return batches, num_batches
        else:
            return batches

    def evaluate(self, x=None, y=None, batch_size=32, verbose=1, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            return self.model.evaluate(x=x, y=y, batch_size=batch_size, verbose=verbose, **kwargs)
        else:
            total_loss = {}
            if type(x) is list:
                num_batches = math.ceil(len(x[0]) / batch_size)
            elif type(x) is np.ndarray:
                num_batches = math.ceil(len(x) / batch_size)
            else:
                x_keys = list(x.keys())
                num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)

            batches_x = self.get_batches(x, batch_size=batch_size, num_batches=num_batches)
            batches_y = self.get_batches(y, batch_size=batch_size, num_batches=num_batches)

            for batch_idx in range(num_batches):

                if type(x) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
                elif type(x) is np.ndarray:
                    batch_x = batches_x[batch_idx]
                else:
                    batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

                if type(y) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
                else:
                    batch_y = batches_y[batch_idx]

                batch_info = self.batch_evaluate(x=batch_x, y=batch_y)

                for key, item in batch_info.items():
                    if key not in total_loss:
                        total_loss[key] = item
                    else:
                        total_loss[key] += item

            total_loss = {key: item / num_batches if not key.startswith('plot') else item
                          for key, item in total_loss.items()}
            return total_loss

    def _tf_fit(self, x=None, y=None, batch_size=None, epochs=1, verbose=1,
                callbacks=None, validation_data=None, shuffle=True, step_checkpoint=None,
                metrics=None, additional_metrics_info=None, metrics_nicknames=None):

        self.stop_training = False
        self.validation_data = validation_data
        callbacks = callbacks or []

        for callback in callbacks:
            callback.set_model(model=self)
            callback.on_train_begin(logs=None)

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
            samples_indexes = np.arange(len(x[0]))
        elif type(x) is np.ndarray:
            num_batches = math.ceil(len(x) / batch_size)
            samples_indexes = np.arange(len(x))
        else:
            x_keys = list(x.keys())
            num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)
            samples_indexes = np.arange(len(x[x_keys[0]]))

        if verbose:
            logger.info('Start Training! Total batches: {}'.format(num_batches))

        if step_checkpoint is not None:
            if type(step_checkpoint) == float:
                step_checkpoint = int(num_batches * step_checkpoint)
                logger.info('Converting percentage step checkpoint to: {}'.format(step_checkpoint))
            else:
                if step_checkpoint > num_batches:
                    step_checkpoint = int(num_batches * 0.1)
                    logger.info('Setting step checkpoint to: {}'.format(step_checkpoint))

        parsed_metrics = None
        if metrics:
            parsed_metrics = build_metrics(metrics)

        for epoch in range(epochs):

            if self.stop_training:
                break

            if shuffle:
                np.random.shuffle(samples_indexes)

            # TODO: wrap condition
            if type(x) is list:
                temp_x = []
                for item in range(len(x)):
                    if type(x[item]) is np.ndarray:
                        temp_x.append(x[item][samples_indexes])
                    else:
                        temp_x.append([x[item][index] for index in samples_indexes])
                s_x = temp_x
            elif type(x) is np.ndarray:
                s_x = x[samples_indexes]
            else:
                s_x = {}
                for key, item in x.items():
                    if type(item) is np.ndarray:
                        s_x[key] = item[samples_indexes]
                    else:
                        s_x[key] = [item[index] for index in samples_indexes]

            if type(y) is list:
                temp_y = []
                for item in range(len(y)):
                    if type(y[item]) is np.ndarray:
                        temp_y.append(y[item][samples_indexes])
                    else:
                        temp_y.append(y[item][index] for index in samples_indexes)
                s_y = temp_y
            else:
                s_y = y[samples_indexes]

            batches_x = self.get_batches(s_x, batch_size=batch_size, num_batches=num_batches)
            batches_y = self.get_batches(s_y, batch_size=batch_size, num_batches=num_batches)

            train_loss = {}

            for batch_idx in tqdm(range(num_batches), leave=True, position=0):

                for callback in callbacks:
                    callback.on_batch_begin(batch=batch_idx, logs=None)

                # TODO: wrap condition
                if type(x) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
                elif type(x) is np.ndarray:
                    batch_x = batches_x[batch_idx]
                else:
                    batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

                if type(y) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(y))]
                else:
                    batch_y = batches_y[batch_idx]

                batch_info = self.batch_fit(x=batch_x, y=batch_y)

                for callback in callbacks:
                    callback.on_batch_end(batch=batch_idx, logs=batch_info)

                # Monitoring at step checkpoint
                # TODO: wrap condition
                if step_checkpoint is not None and batch_idx > 0 and batch_idx % step_checkpoint == 0:
                    if validation_data is not None and metrics is not None:
                        val_info = self.evaluate(x=validation_data[0], y=validation_data[1], batch_size=batch_size)

                        if type(y) is list:
                            val_metrics_str_result = []
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            for idx in range(len(y)):
                                all_val_metrics = compute_metrics(parsed_metrics,
                                                                  true_values=validation_data[1][idx].astype(
                                                                      np.float32),
                                                                  predicted_values=val_predictions[idx].astype(
                                                                      np.float32),
                                                                  additional_metrics_info=additional_metrics_info[idx],
                                                                  suffix=idx)
                                val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])
                                val_metrics_str_result.append(val_metrics_str)
                        else:
                            val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                            all_val_metrics = compute_metrics(parsed_metrics,
                                                              true_values=validation_data[1].astype(np.float32),
                                                              predicted_values=val_predictions.astype(np.float32),
                                                              additional_metrics_info=additional_metrics_info,
                                                              prefix='val')
                            val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                                   for key, value in all_val_metrics.items()])]
                        logger.info(
                            'Epoch: {0} -- Train Loss: {1} -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                        train_loss[
                                                                                                            'train_loss'],
                                                                                                        val_info[
                                                                                                            'val_loss'],
                                                                                                        ' -- '.join(
                                                                                                            val_metrics_str_result)))

                for key, item in batch_info.items():
                    if key in train_loss:
                        train_loss[key] += item
                    else:
                        train_loss[key] = item

            train_loss = {key: item / num_batches if not key.startswith('plot') else item
                          for key, item in train_loss.items()}

            val_info = None

            # Compute metrics at the end of each epoch
            callback_additional_args = {}

            if validation_data is not None:
                val_info = self.evaluate(x=validation_data[0], y=validation_data[1], batch_size=batch_size)

                if metrics is not None:
                    if type(y) is list:
                        val_metrics_str_result = []
                        all_val_metrics = {}
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        for idx in range(len(y)):
                            val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1][idx].astype(np.float32),
                                                          predicted_values=val_predictions[idx].astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info[idx],
                                                          metrics_nicknames=metrics_nicknames,
                                                          suffix=idx)
                            val_metrics_str = ' -- '.join(['{0}: {1}'.format(key, value)
                                                           for key, value in val_metrics.items()])
                            val_metrics_str_result.append(val_metrics_str)
                            merge(all_val_metrics, val_metrics)
                    else:
                        val_predictions = self.predict(x=validation_data[0], batch_size=batch_size)
                        all_val_metrics = compute_metrics(parsed_metrics,
                                                          true_values=validation_data[1].astype(np.float32),
                                                          predicted_values=val_predictions.astype(np.float32),
                                                          additional_metrics_info=additional_metrics_info,
                                                          metrics_nicknames=metrics_nicknames,
                                                          prefix='val')
                        val_metrics_str_result = [' -- '.join(['{0}: {1}'.format(key, value)
                                                               for key, value in all_val_metrics.items()])]
                    logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2} -- Val Metrics: {3}'.format(epoch + 1,
                                                                                                            train_loss,
                                                                                                            val_info,
                                                                                                            ' -- '.join(
                                                                                                                val_metrics_str_result)))
                    callback_additional_args = all_val_metrics
                else:
                    if verbose:
                        logger.info('Epoch: {0} -- Train Loss: {1} -- Val Loss: {2}'.format(epoch + 1,
                                                                                            train_loss,
                                                                                            val_info))

            for callback in callbacks:
                callback_args = train_loss
                callback_args = merge(callback_args, val_info)
                callback_args = merge(callback_args,
                                      callback_additional_args,
                                      overwrite_conflict=False)
                callback.on_epoch_end(epoch=epoch, logs=callback_args)

        for callback in callbacks:
            callback.on_train_end(logs={'name': self.name})

    def _tf_predict(self, x, batch_size=32, callbacks=None):

        callbacks = callbacks or []

        if type(x) is list:
            num_batches = math.ceil(len(x[0]) / batch_size)
        elif type(x) is np.ndarray:
            num_batches = math.ceil(len(x) / batch_size)
        else:
            x_keys = list(x.keys())
            num_batches = math.ceil(len(x[x_keys[0]]) / batch_size)

        batches_x = self.get_batches(x, batch_size=batch_size, num_batches=num_batches)
        total_preds = []

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_begin'):
                if not hasattr(callback, 'model'):
                    callback.set_model(model=self.model)
                callback.on_prediction_begin(logs=None)

        for batch_idx in range(num_batches):

            if type(x) is list:
                batch_x = [batches_x[item][batch_idx] for item in range(len(x))]
            elif type(x) is np.ndarray:
                batch_x = batches_x[batch_idx]
            else:
                batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_begin'):
                    callback.on_batch_prediction_begin(batch=batch_idx, logs={'batch': batch_x})

            preds = self.batch_predict(x=batch_x)

            for callback in callbacks:
                if hasattr(callback, 'on_batch_prediction_end'):
                    callback.on_batch_prediction_end(batch=batch_idx, logs={'predictions': preds,
                                                                            'model': self.model})

            if type(preds) is list:
                if batch_idx == 0:
                    total_preds.extend(preds)
                else:
                    for idx, pred in enumerate(preds):
                        total_preds[idx] = np.append(total_preds[idx], pred, axis=0)
            else:
                total_preds.extend(preds)

        for callback in callbacks:
            if hasattr(callback, 'on_prediction_end'):
                callback.on_prediction_end(logs=None)

        return np.array(total_preds)

    def fit(self, x=None, y=None, batch_size=None, epochs=1, verbose=1,
            callbacks=None, validation_data=None, shuffle=True, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            return self.model.fit(x=x, y=y, batch_size=batch_size,
                                  epochs=epochs, verbose=verbose,
                                  callbacks=callbacks, validation_data=validation_data,
                                  shuffle=shuffle,
                                  class_weight=self.class_weights, **kwargs)
        else:
            return self._tf_fit(x=x, y=y, batch_size=batch_size,
                                epochs=epochs, verbose=verbose,
                                callbacks=callbacks,
                                validation_data=validation_data,
                                shuffle=shuffle, **kwargs)

    def predict(self, x, batch_size=32, verbose=0, **kwargs):

        # Checking keras compliant model
        if self.model is not None:
            preds = self.model.predict(x=x, batch_size=batch_size, verbose=verbose, **kwargs)
            return self.parse_predictions(predictions=preds)
        else:
            return self._tf_predict(x=x, batch_size=batch_size, **kwargs)

    def parse_predictions(self, predictions):
        return np.round(predictions)

    def get_feed_dict(self, x, y=None, phase='train'):
        raise NotImplementedError()

    def set_lr(self, new_lr):
        if self.model is not None:
            K.set_value(self.model.optimizer.lr, new_lr)
        else:
            self.optimizer_args['learning_rate'] = new_lr

    def get_lr(self):
        if self.model is not None:
            return K.get_value(self.model.optimizer.lr)
        else:
            return self.optimizer_args['learning_rate']

    def build_tensorboard_info(self):
        pass


# TODO: fix to dict input format
class VanillaLSTM(Network):
    """
    LSTM baseline for ToS Task 1. This is a simple stacked-LSTMs model.
    """

    def __init__(self, lstm_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam', optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(VanillaLSTM, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.lstm_neurons = lstm_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension,
                                input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding,
                                mask_zero=True))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension,
                                input_length=padding_max_length,
                                mask_zero=True))

        for neuron_value in self.lstm_neurons[:-1]:
            model.add(LSTM(neuron_value, return_sequences=True,
                           kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(LSTM(self.lstm_neurons[-1], return_sequences=False,
                       kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


# TODO: fix to dict input format
class BiGRU(Network):
    """
    GRU baseline for ToS Task 1. This is a simple stacked bidirectional GRUs model.
    """

    def __init__(self, gru_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam', optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(BiGRU, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.gru_neurons = gru_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding,
                                mask_zero=True))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                mask_zero=True))

        for neuron_value in self.gru_neurons[:-1]:
            model.add(Bidirectional(GRU(neuron_value, return_sequences=True,
                                        kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
            model.add(Dropout(self.dropout_rate))

        model.add(Bidirectional(GRU(self.gru_neurons[-1], return_sequences=False,
                                    kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


# TODO: fix to dict input format
class CBiGRU(Network):
    """
    CNN-GRU baseline for ToS Task 1. This is a simple stacked bidirectional GRUs model on top of stacked CNN layers.
    """

    def __init__(self, conv_info, gru_neurons, dense_neurons, embedding_dimension,
                 session=None, optimizer='adam',
                 optimizer_args=None,
                 trainable_embedding=True, regularizer="l2", regularizer_coeff=None,
                 dropout_rate=0.2):

        super(CBiGRU, self).__init__(session=session, embedding_dimension=embedding_dimension)

        self.conv_info = conv_info
        self.gru_neurons = gru_neurons
        self.dense_neurons = dense_neurons
        self.trainable_embedding = trainable_embedding
        self.dropout_rate = dropout_rate
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args

        self.regularizer = getattr(regularizers, regularizer)
        if regularizer_coeff is None:
            if regularizer == 'l1_l2':
                self.regularizer_coeff = [0.00, 0.00]
            else:
                self.regularizer_coeff = [0.00]
        else:
            self.regularizer_coeff = regularizer_coeff

    def build_model(self, vocab_size, text_info, embedding_matrix=None):

        padding_max_length = text_info['padding_max_length']

        model = Sequential()

        if embedding_matrix is None:
            embeddings_initializer = "uniform"
        else:
            embeddings_initializer = Constant(embedding_matrix)

        if embedding_matrix is not None:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length,
                                embeddings_initializer=embeddings_initializer,
                                trainable=self.trainable_embedding))
        else:
            model.add(Embedding(vocab_size, self.embedding_dimension, input_length=padding_max_length))

        for info in self.conv_info:
            model.add(Conv1D(info[0], info[1], activation='relu'))
            model.add(MaxPooling1D())
            model.add(Dropout(self.dropout_rate))

        for neuron_value in self.gru_neurons[:-1]:
            model.add(Bidirectional(GRU(neuron_value, return_sequences=True,
                                        kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
            model.add(Dropout(self.dropout_rate))

        model.add(Bidirectional(GRU(self.gru_neurons[-1], return_sequences=False,
                                    kernel_regularizer=self.regularizer(*self.regularizer_coeff))))
        model.add(Dropout(self.dropout_rate))

        for neuron_value in self.dense_neurons:
            model.add(Dense(neuron_value,
                            activation='relu',
                            kernel_regularizer=self.regularizer(*self.regularizer_coeff)))
            model.add(Dropout(self.dropout_rate))

        model.add(Dense(1, activation='sigmoid'))

        self.optimizer = getattr(optimizers, self.optimizer)
        self.optimizer = self.optimizer(**self.optimizer_args)

        model.compile(loss='binary_crossentropy', optimizer=self.optimizer)
        logger.info(model.summary())

        self.model = model


class Basic_MemN2N_v1(Network):
    """
    Basic Memory Network implementation. The model is comprised of three steps:
        1. Memory Lookup: a similarity operation is computed between query and memory cells.
         A content vector is extracted.

        2. Memory Reasoning: extracted content vector is used along with the query to build a new query.

        3. Lookup and Reasoning can be iterated multiple times.

        4. Eventually, an answer module takes care of formulating a prediction.
    """

    def __init__(self, hops, optimizer, optimizer_args, embedding_info,
                 memory_lookup_info, extraction_info, reasoning_info, partial_supervision_info, dropout_rate=.4,
                 position_encoding=False, max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True,
                 l2_regularization=None, answer_weights=(256, 64), output_size=1, **kwargs):

        super(Basic_MemN2N_v1, self).__init__(**kwargs)

        self.hops = hops
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.embedding_info = embedding_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.partial_supervision_info = partial_supervision_info
        self.position_encoding = position_encoding

        self.additional_data['categories'] = list(map(lambda x: x.upper(), self.additional_data['categories']))
        self.categories = self.additional_data['categories'] + ['not-unfair']
        self.output_size = output_size

        self.mlp = lambda **x: fully_connected(**x)

    def _build_inputs(self):
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")
        self.context_mask = tf.placeholder(tf.float32, [None, self.memory_size], name='context_mask')

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.partial_supervision_info['flag']:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('embedding_weights'):

            self.initializer = initializers.get('uniform')

            if embedding_matrix is None:
                query_embedding = self.initializer([vocab_size, self.embedding_dimension])
                memory_embedding = self.initializer([vocab_size, self.embedding_dimension])

                self.query_embedding = tf.Variable(query_embedding, name='query_embedding')
                self.memory_embedding = tf.Variable(memory_embedding, name='memory_embedding')
            else:
                self.query_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                       name='query_embedding',
                                                       shape=embedding_matrix.shape, trainable=True)
                self.memory_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                        name='memory_embedding',
                                                        shape=embedding_matrix.shape, trainable=True)

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 labels=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

        if self.partial_supervision_info['flag']:
            supervision_losses = []

            mask_res = tf.tile(self.mask_idxs, multiples=[1, self.padding_amount])
            mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

            for sim in self.probs:
                pos_scores = tf.gather_nd(sim, self.positive_idxs)
                pos_scores = tf.reshape(pos_scores, [-1, 1])

                neg_scores = tf.gather_nd(sim, self.negative_idxs)
                neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
                neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])

                hop_supervision_loss = tf.maximum(0., self.partial_supervision_info['margin'] - pos_scores + neg_scores)
                hop_supervision_loss = hop_supervision_loss * mask_res
                hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, self.padding_amount, self.padding_amount])
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=2)
                hop_supervision_loss = tf.reduce_min(hop_supervision_loss, axis=1)
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
                supervision_losses.append(hop_supervision_loss)

            supervision_losses = tf.stack(supervision_losses)
            supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_info['coefficient']

            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _memory_lookup(self, memory, query, hop):

        if self.memory_lookup_info['mode'] == 'dot_product':
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1].value / memory.shape[-1].value, tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2)
        elif self.memory_lookup_info['mode'] == 'mlp':

            if self.reasoning_info['mode'] == 'concat':
                query_dimension = query.shape[-1]
                repeat_amount = query_dimension // memory.shape[-1]
                memory = tf.tile(memory, multiples=[1, 1, repeat_amount])
                mlp_scope = 'lookup_mlp_{}'.format(hop)
            else:
                mlp_scope = 'lookup_mlp'

            # [batch_size, mem_size, embedding_dim]
            repeated_query = tf.expand_dims(query, axis=1)
            repeated_query = tf.tile(repeated_query, [1, memory.shape[1], 1])

            # [batch_size, mem_size, embedding_dim * 2]
            att_input = tf.concat((memory, repeated_query), axis=-1)
            att_input = tf.reshape(att_input, [-1, att_input.shape[-1]])

            with tf.variable_scope(mlp_scope, reuse=tf.AUTO_REUSE):
                # -> [batch_size, mem_size, 1]

                dotted = att_input
                for idx, weight in enumerate(self.memory_lookup_info['weights']):
                    dotted = self.mlp(inputs=dotted,
                                      num_outputs=weight,
                                      scope="lookup_mlp_{}".format(idx),
                                      reuse=tf.AUTO_REUSE)

                    dotted = tf.nn.dropout(dotted, rate=self.dropout)

                dotted = tf.contrib.layers.fully_connected(dotted,
                                                           1,
                                                           activation_fn=None,
                                                           scope="lookup_mlp_{}".format(
                                                               len(self.memory_lookup_info['weights'])),
                                                           reuse=tf.AUTO_REUSE)

                # [batch_size, mem_size]
                dotted = tf.reshape(dotted, [-1, memory.shape[1]])
        elif self.memory_lookup_info['mode'] == 'scaled_dot_product':
            # [batch_size, mem_size, embedding_dim]
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1].value / memory.shape[-1].value, tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        return dotted

    def _memory_extraction(self, similarities, mask):

        # Zeroing-out padding
        similarities += (mask * -1e9)

        if self.extraction_info['mode'] == 'sparsemax':
            probs = sparsemax(similarities)
        elif self.extraction_info['mode'] == 'softmax':
            probs = tf.nn.softmax(similarities, axis=1)
        elif self.extraction_info['mode'] == 'sigmoid':
            probs = tf.nn.sigmoid(similarities)
        else:
            raise RuntimeError('Invalid extraction mode! Got: {}'.format(self.extraction_info['mode']))

        return probs

    def _memory_reasoning(self, query, memory_search, hopn):

        if self.reasoning_info['mode'] == 'sum':
            query = query + memory_search
        elif self.reasoning_info['mode'] == 'concat':
            query = tf.concat((query, memory_search), axis=1)
        elif self.reasoning_info['mode'] == 'rnn':
            with tf.variable_scope('aggregation_controller', reuse=tf.AUTO_REUSE):
                cell = GRUCell(query.shape[-1])
                _, query = cell(memory_search, query)
        elif self.reasoning_info['mode'] == 'mlp':
            query = fully_connected(tf.concat((query, memory_search), axis=1),
                                    query.shape[-1].value,
                                    activation_fn=tf.nn.relu,
                                    scope='aggregation_controller',
                                    reuse=tf.AUTO_REUSE)
        else:
            raise RuntimeError(
                'Invalid aggregation mode! Got: {} -- Supported: [sum, concat]'.format(self.reasoning_info['mode']))

        return query

    def _compute_answer(self, final_response):

        current_answer = final_response
        with tf.variable_scope('Answer', reuse=tf.AUTO_REUSE):
            for idx, weight in enumerate(self.answer_weights):
                current_answer = self.mlp(inputs=current_answer,
                                          num_outputs=weight,
                                          scope="answer_{}".format(idx),
                                          reuse=tf.AUTO_REUSE)
                if idx < len(self.answer_weights) - 1:
                    current_answer = tf.nn.dropout(current_answer, rate=self.dropout)

            current_answer = tf.contrib.layers.fully_connected(current_answer,
                                                               self.output_size,
                                                               activation_fn=None,
                                                               scope="final_answer",
                                                               reuse=tf.AUTO_REUSE)

        return current_answer

    def _sentence_embedding(self, x, x_real_length):

        if self.embedding_info['mode'] == 'sum':
            return tf.reduce_sum(x, axis=1)
        elif self.embedding_info['mode'] == 'mean':
            return tf.reduce_mean(x, axis=1)
        elif self.embedding_info['mode'] == 'generalized_pooling':
            with tf.variable_scope('generalized_pooling_embedding', reuse=tf.AUTO_REUSE):
                reduced_input = self.mlp(inputs=x,
                                         num_outputs=self.embedding_info['attention_dimension'],
                                         scope="pooling1",
                                         reuse=tf.AUTO_REUSE)
                reduced_input = self.mlp(inputs=reduced_input,
                                         num_outputs=x.shape[-1].value,
                                         scope='pooling2',
                                         reuse=tf.AUTO_REUSE)
                reduction_weights = tf.nn.softmax(reduced_input, axis=1)
                pooled_input = tf.reduce_sum(x * reduction_weights, axis=1)
                return pooled_input
        elif self.embedding_info['mode'] == 'recurrent':
            with tf.variable_scope('recurrent_embedding', reuse=tf.AUTO_REUSE):
                if self.embedding_info['bidirectional']:
                    fwd_cell = GRUCell(self.embedding_info['hidden_size'])
                    bwd_cell = GRUCell(self.embedding_info['hidden_size'])

                    _, encoding = tf.nn.bidirectional_dynamic_rnn(cell_fw=fwd_cell,
                                                                  cell_bw=bwd_cell,
                                                                  inputs=x,
                                                                  sequence_length=x_real_length,
                                                                  dtype=tf.float32)
                    encoding = tf.concat((encoding[0], encoding[1]), axis=1)
                else:
                    cell = GRUCell(self.embedding_info['hidden_size'])

                    _, encoding = tf.nn.dynamic_rnn(cell=cell,
                                                    inputs=x,
                                                    sequence_length=x_real_length,
                                                    dtype=tf.float32)

            return encoding
        else:
            raise RuntimeError('Invalid sentence embedding mode! Got: {}'.format(self.embedding_info['mode']))

    def _zero_out_padding(self, embedding_matrix):
        return tf.scatter_update(embedding_matrix,
                                 0,
                                 tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

    def get_output(self):
        with tf.variable_scope('Output'):

            # [batch_size, ]
            query_real_length = tf.where(tf.equal(self.query, 0), tf.zeros_like(self.query), tf.ones_like(self.query))
            query_real_length = tf.reduce_sum(query_real_length, axis=1)

            # [batch_size * memory_size, ]
            memory = tf.reshape(self.context, [-1, self.context.shape[-1]])
            memory_real_length = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
            memory_real_length = tf.reduce_sum(memory_real_length, axis=1)

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Position encoding
            if self.position_encoding:
                query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]

            # [batch_size, embedding_dim]
            query_emb = self._sentence_embedding(query_emb, query_real_length)
            query_emb = tf.reshape(query_emb, [-1, query_emb.shape[-1]])

            # Memory Embedding

            # [batch_size * mem_size, sentence_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
            memory_emb = tf.nn.embedding_lookup(self.memory_embedding, memory)

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            # [batch_size, mem_size, embedding_dim]

            memory_emb = self._sentence_embedding(memory_emb, memory_real_length)
            memory_emb = tf.reshape(memory_emb, [-1, self.memory_size, memory_emb.shape[-1]])

            self.probs = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                # [batch_size, mem_size]
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)

                # Calculate probabilities
                # [batch_size, mem_size]
                probs = self._memory_extraction(similarities=dotted, mask=self.context_mask)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(memory_emb * probs_temp, axis=1)

                query_emb = self._memory_reasoning(query=query_emb,
                                                   memory_search=memory_search,
                                                   hopn=hopn)

            return self._compute_answer(final_response=query_emb)

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            self.pos_encoding = positional_encoding(self.vocab_size,
                                                    self.embedding_dimension)

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat]
                cat_kb = pad_sequences(cat_kb, padding='post', maxlen=self.sentence_size)
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        context = np.array(context)
        context_mask = np.array(context_mask)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.context: context,
            self.context_mask: context_mask,
            self.query: query,
            self.dropout: dropout_rate
        }

        if self.partial_supervision_info['flag']:
            targets = x['targets']
            targets = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')
        if self.partial_supervision_info['flag']:
            loss_values.append(self.supervision_losses)
            loss_names.append('train_supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision_info['flag']:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


class MultiBasic_MemN2N_v1(Basic_MemN2N_v1):

    def __init__(self, **kwargs):
        super(MultiBasic_MemN2N_v1, self).__init__(**kwargs)
        self.output_size = len(self.categories)

    def _build_inputs(self):
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")
        self.context_mask = tf.placeholder(tf.float32, [None, self.memory_size], name='context_mask')

        self.answer = tf.placeholder(tf.int32, [None, ], name="answer")

        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        self.weights = tf.placeholder(tf.float32, shape=[None, ], name='cross_entropy_weights')

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        if self.partial_supervision_info['flag']:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def build_predict_op(self):
        self.predict_op = tf.argmax(self.logits, axis=1)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat.lower()]
                cat_kb = pad_sequences(cat_kb, padding='post', maxlen=self.sentence_size)
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        context = np.array(context)
        context_mask = np.array(context_mask)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.context: context,
            self.context_mask: context_mask,
            self.query: query,
            self.dropout: dropout_rate
        }

        if self.partial_supervision_info['flag']:
            targets = x['targets']
            targets = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase == 'train':
            feed_dict[self.phase] = True
        else:
            feed_dict[self.phase] = False

        if phase != 'test':
            # Transform labels to multi-class setting
            # Computing cross-entropy weights
            weights = [self.pos_weight[np.argmax(val)] for val in y]

            feed_dict[self.answer] = y
            feed_dict[self.weights] = weights

        return feed_dict

    def build_loss(self):
        # [batch_size, #categories]
        self.logits = self.get_output()

        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits,
                                                                       labels=self.answer,
                                                                       name='cross_entropy')

        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_sum(cross_entropy * self.weights, name="cross_entropy_mean")
        self.cross_entropy_mean = self.cross_entropy_mean / tf.reduce_sum(self.weights)
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

        if self.partial_supervision_info['flag']:
            supervision_losses = []

            mask_res = tf.tile(self.mask_idxs, multiples=[1, self.padding_amount])
            mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

            for sim in self.probs:
                pos_scores = tf.gather_nd(sim, self.positive_idxs)
                pos_scores = tf.reshape(pos_scores, [-1, 1])

                neg_scores = tf.gather_nd(sim, self.negative_idxs)
                neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
                neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])

                hop_supervision_loss = tf.maximum(0., self.partial_supervision_info['margin'] - pos_scores + neg_scores)
                hop_supervision_loss = hop_supervision_loss * mask_res
                hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, self.padding_amount, self.padding_amount])
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=2)
                hop_supervision_loss = tf.reduce_min(hop_supervision_loss, axis=1)
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
                supervision_losses.append(hop_supervision_loss)

            supervision_losses = tf.stack(supervision_losses)
            supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_info['coefficient']

            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses


class Similarity_MemN2N_v1(Network):

    def __init__(self, optimizer, optimizer_args, embedding_info,
                 memory_lookup_info, dropout_rate=.4, position_encoding=False, max_grad_norm=40.0,
                 clip_gradient=True, add_gradient_noise=True, l2_regularization=None, use_batch_norm=False,
                 **kwargs):

        super(Similarity_MemN2N_v1, self).__init__(**kwargs)

        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.embedding_info = embedding_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.use_batch_norm = use_batch_norm
        self.position_encoding = position_encoding

        self.additional_data['categories'] = list(map(lambda x: x.upper(), self.additional_data['categories']))
        self.categories = self.additional_data['categories'] + ['not-unfair']

        if self.use_batch_norm:
            self.mlp = lambda **x: batch_norm_relu(phase=self.phase, activation=True, **x)
        else:
            self.mlp = fully_connected

    def _build_inputs(self):
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")
        self.context_mask = tf.placeholder(tf.float32, [None, self.memory_size], name='context_mask')

        self.targets = tf.placeholder(tf.float32, [None, self.max_targets], name="targets")

        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('embedding_weights'):

            self.initializer = initializers.get('uniform')

            if embedding_matrix is None:
                query_embedding = self.initializer([vocab_size, self.embedding_dimension])
                memory_embedding = self.initializer([vocab_size, self.embedding_dimension])

                self.query_embedding = tf.Variable(query_embedding, name='query_embedding')
                self.memory_embedding = tf.Variable(memory_embedding, name='memory_embedding')
            else:
                self.query_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                       name='query_embedding',
                                                       shape=embedding_matrix.shape, trainable=True)
                self.memory_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                        name='memory_embedding',
                                                        shape=embedding_matrix.shape, trainable=True)

    def build_loss(self):
        self.logits = self.get_output()

        multilabel_ce = tf.nn.sigmoid_cross_entropy_with_logits(logits=self.logits,
                                                                labels=self.targets,
                                                                name="multilabel_ce")

        # [batch_size, mem_size]
        multilabel_ce *= (1 - self.context_mask)
        multilabel_ce = tf.reduce_sum(multilabel_ce, axis=1)

        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(multilabel_ce, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _memory_lookup(self, memory, query):

        if self.memory_lookup_info['mode'] == 'mlp':

            # [batch_size, mem_size, embedding_dim]
            repeated_query = tf.expand_dims(query, axis=1)
            repeated_query = tf.tile(repeated_query, [1, memory.shape[1], 1])

            # [batch_size, mem_size, embedding_dim * 2]
            att_input = tf.concat((memory, repeated_query), axis=-1)
            att_input = tf.reshape(att_input, [-1, att_input.shape[-1]])

            with tf.variable_scope("lookup_mlp", reuse=tf.AUTO_REUSE):
                # -> [batch_size, mem_size, 1]

                dotted = att_input
                for idx, weight in enumerate(self.memory_lookup_info['weights']):
                    dotted = self.mlp(inputs=dotted,
                                      num_outputs=weight,
                                      scope="lookup_mlp_{}".format(idx),
                                      reuse=tf.AUTO_REUSE,
                                      activation_fn=tf.nn.relu)

                    dotted = tf.nn.dropout(dotted, rate=self.dropout)

                dotted = tf.contrib.layers.fully_connected(dotted,
                                                           1,
                                                           activation_fn=None,
                                                           scope="lookup_mlp_{}".format(
                                                               len(self.memory_lookup_info['weights'])),
                                                           reuse=tf.AUTO_REUSE)

                # [batch_size, mem_size]
                dotted = tf.reshape(dotted, [-1, memory.shape[1]])
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        return dotted

    def _sentence_embedding(self, x, x_real_length):

        if self.embedding_info['mode'] == 'sum':
            return tf.reduce_sum(x, axis=1)
        elif self.embedding_info['mode'] == 'mean':
            return tf.reduce_mean(x, axis=1)
        elif self.embedding_info['mode'] == 'generalized_pooling':
            with tf.variable_scope('generalized_pooling_embedding', reuse=tf.AUTO_REUSE):
                reduced_input = self.mlp(inputs=x,
                                         num_outputs=self.embedding_info['attention_dimension'],
                                         scope="pooling1",
                                         reuse=tf.AUTO_REUSE,
                                         activation_fn=tf.nn.relu)
                reduced_input = self.mlp(inputs=reduced_input,
                                         num_outputs=x.shape[-1].value,
                                         scope='pooling2',
                                         reuse=tf.AUTO_REUSE)
                reduction_weights = tf.nn.softmax(reduced_input, axis=1)
                pooled_input = tf.reduce_sum(x * reduction_weights, axis=1)
                return pooled_input
        elif self.embedding_info['mode'] == 'recurrent':
            with tf.variable_scope('recurrent_embedding', reuse=tf.AUTO_REUSE):
                if self.embedding_info['bidirectional']:
                    fwd_cell = GRUCell(self.embedding_info['hidden_size'])
                    bwd_cell = GRUCell(self.embedding_info['hidden_size'])

                    _, encoding = tf.nn.bidirectional_dynamic_rnn(cell_fw=fwd_cell,
                                                                  cell_bw=bwd_cell,
                                                                  inputs=x,
                                                                  sequence_length=x_real_length,
                                                                  dtype=tf.float32)
                    encoding = tf.concat((encoding[0], encoding[1]), axis=1)
                else:
                    cell = GRUCell(self.embedding_info['hidden_size'])

                    _, encoding = tf.nn.dynamic_rnn(cell=cell,
                                                    inputs=x,
                                                    sequence_length=x_real_length,
                                                    dtype=tf.float32)

            return encoding
        else:
            raise RuntimeError('Invalid sentence embedding mode! Got: {}'.format(self.embedding_info['mode']))

    def get_output(self):
        with tf.variable_scope('Output'):

            # [batch_size, ]
            query_real_length = tf.where(tf.equal(self.query, 0), tf.zeros_like(self.query), tf.ones_like(self.query))
            query_real_length = tf.reduce_sum(query_real_length, axis=1)

            # [batch_size * memory_size, ]
            memory = tf.reshape(self.context, [-1, self.context.shape[-1]])
            memory_real_length = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
            memory_real_length = tf.reduce_sum(memory_real_length, axis=1)

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Position encoding
            if self.position_encoding:
                query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]

            # [batch_size, embedding_dim]
            query_emb = self._sentence_embedding(query_emb, query_real_length)
            query_emb = tf.reshape(query_emb, [-1, query_emb.shape[-1]])

            # Memory Embedding

            # [batch_size * mem_size, sentence_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
            memory_emb = tf.nn.embedding_lookup(self.memory_embedding, memory)

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            # [batch_size, mem_size, embedding_dim]

            memory_emb = self._sentence_embedding(memory_emb, memory_real_length)
            memory_emb = tf.reshape(memory_emb, [-1, self.memory_size, memory_emb.shape[-1]])

            dotted = self._memory_lookup(memory=memory_emb, query=query_emb)

            return dotted

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']
        self.max_targets = text_info['max_targets']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            self.pos_encoding = positional_encoding(self.vocab_size,
                                                    self.embedding_dimension)

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat.lower()]
                cat_kb = pad_sequences(cat_kb, padding='post', maxlen=self.sentence_size)
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        context = np.array(context)
        context_mask = np.array(context_mask)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.context: context,
            self.context_mask: context_mask,
            self.query: query,
            self.dropout: dropout_rate
        }

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        if phase != 'test':
            # Parse targets
            # [batch_size, memory_size]
            y = pad_data(y, padding_length=self.memory_size, padding='post', dtype=np.int32)
            y = y.astype(np.float32)
            feed_dict[self.targets] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: other gating mechanisms: fake memory, threshold filter
class Gated_MemN2N_v1(Basic_MemN2N_v1):

    def __init__(self, gating_info, **kwargs):
        super(Gated_MemN2N_v1, self).__init__(**kwargs)
        self.gating_info = gating_info

    def compute_gate(self, similarities):

        if self.gating_info['mode'] == 'mlp_gating':
            with tf.variable_scope("gating", reuse=tf.AUTO_REUSE):
                # -> [batch_size, mem_size]
                original_shape = tf.shape(similarities)

                # [batch_size * mem_size, 1]
                similarities = tf.reshape(similarities, [-1, ])
                similarities = tf.expand_dims(similarities, axis=-1)

                gate = similarities
                for idx, weight in enumerate(self.gating_info['gating_weights']):
                    gate = self.mlp(inputs=gate,
                                    activation_fn=None,
                                    num_outputs=weight,
                                    scope="gating_{}".format(idx),
                                    reuse=tf.AUTO_REUSE)

                    gate = tf.nn.dropout(gate, rate=self.dropout)

                gate = tf.contrib.layers.fully_connected(gate,
                                                         1,
                                                         activation_fn=tf.nn.sigmoid,
                                                         scope="gating_{}".format(
                                                             len(self.gating_info['gating_weights'])),
                                                         reuse=tf.AUTO_REUSE)

                # [batch_size, mem_size]
                gate = tf.reshape(gate, original_shape)
                return gate
        else:
            raise RuntimeError(
                'Invalid gating mode! Got: {} -- Supported: [mlp_gating]'.format(self.gating_info['mode']))

    def get_output(self):
        with tf.variable_scope('Output'):

            # [batch_size, ]
            query_real_length = tf.where(tf.equal(self.query, 0), tf.zeros_like(self.query), tf.ones_like(self.query))
            query_real_length = tf.reduce_sum(query_real_length, axis=1)

            # [batch_size * memory_size, ]
            memory = tf.reshape(self.context, [-1, self.context.shape[-1]])
            memory_real_length = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
            memory_real_length = tf.reduce_sum(memory_real_length, axis=1)

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Position encoding
            if self.position_encoding:
                query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]

            # [batch_size, embedding_dim]
            query_emb = self._sentence_embedding(query_emb, query_real_length)
            query_emb = tf.reshape(query_emb, [-1, query_emb.shape[-1]])

            # Memory Embedding

            # [batch_size * mem_size, sentence_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
            memory_emb = tf.nn.embedding_lookup(self.memory_embedding, memory)

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            # [batch_size, mem_size, embedding_dim]

            memory_emb = self._sentence_embedding(memory_emb, memory_real_length)
            memory_emb = tf.reshape(memory_emb, [-1, self.memory_size, memory_emb.shape[-1]])

            self.probs = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                # [batch_size, mem_size]
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)

                # [batch_size, mem_size]
                gating = self.compute_gate(similarities=dotted)

                # Calculate probabilities
                # [batch_size, mem_size]
                probs = self._memory_extraction(similarities=dotted, mask=self.context_mask)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                gating_temp = tf.expand_dims(gating, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(memory_emb * probs_temp * gating_temp, axis=1)

                query_emb = self._memory_reasoning(query=query_emb,
                                                   memory_search=memory_search,
                                                   hopn=hopn)

            return self._compute_answer(final_response=query_emb)


class Entailment_MemN2N_v1(Basic_MemN2N_v1):

    def __init__(self, entailment_supervision=None, **kwargs):
        super(Entailment_MemN2N_v1, self).__init__(**kwargs)
        if entailment_supervision is not None:
            self.entailment_supervision = True
            self.entailment_margin = entailment_supervision[0]
            self.entailment_coefficient = entailment_supervision[1]
        else:
            self.entailment_supervision = False

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

        if self.partial_supervision:
            supervision_losses = []

            mask_res = tf.tile(self.mask_idxs, multiples=[1, self.padding_amount])
            mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

            for sim in self.probs:
                pos_scores = tf.gather_nd(sim, self.positive_idxs)
                pos_scores = tf.reshape(pos_scores, [-1, 1])

                neg_scores = tf.gather_nd(sim, self.negative_idxs)
                neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
                neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])

                hop_supervision_loss = tf.maximum(0., self.supervision_margin - pos_scores + neg_scores)
                hop_supervision_loss = hop_supervision_loss * mask_res
                hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, self.padding_amount, self.padding_amount])
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=2)
                hop_supervision_loss = tf.reduce_min(hop_supervision_loss, axis=1)
                hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
                supervision_losses.append(hop_supervision_loss)

            supervision_losses = tf.stack(supervision_losses)
            supervision_losses = tf.reduce_mean(supervision_losses) * self.partial_supervision_coeff

            self.supervision_losses = supervision_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = supervision_losses
            else:
                self.loss_op += supervision_losses

        if self.entailment_supervision:
            entailment_losses = []

            answer = tf.squeeze(self.answer)

            positive_indices = tf.where(tf.equal(answer, 1))
            negative_indices = tf.where(tf.equal(answer, 0))

            for similarity in self.dotted:
                # [batch_size, 1]
                total_scores = tf.reduce_sum(similarity, axis=1)
                total_scores = tf.nn.sigmoid(total_scores)

                positive_scores = tf.gather(total_scores, positive_indices)
                negative_scores = tf.gather(total_scores, negative_indices)

                positive_scores = tf.tile(positive_scores, multiples=[1, tf.shape(negative_scores)[0]])
                negative_scores = tf.transpose(negative_scores, [1, 0])

                hop_entailment_loss = tf.maximum(0., self.entailment_margin - positive_scores + negative_scores)
                hop_entailment_loss = tf.reduce_sum(hop_entailment_loss)

                entailment_losses.append(hop_entailment_loss)

            entailment_losses = tf.stack(entailment_losses)
            entailment_losses = tf.reduce_mean(entailment_losses) * self.entailment_coefficient

            self.entailment_losses = entailment_losses  # for visualization/debugging

            if self.loss_op is None:
                self.loss_op = entailment_losses
            else:
                self.loss_op += entailment_losses

    def get_output(self):
        with tf.variable_scope('Output'):

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.query_embedding)]):
            query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Position encoding
            if self.query_encoding is not None:
                query_emb = query_emb * self.query_encoding

            # [batch_size, embedding_dim]
            query_emb = tf.reduce_mean(query_emb, axis=1)

            # Memory Embedding

            # [batch_size, mem_size, story_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
            memory_emb = tf.nn.embedding_lookup(self.memory_embedding, self.context)

            # Position encoding
            if self.context_encoding is not None:
                memory_emb = memory_emb * self.context_encoding

            # [batch_size, mem_size, embedding_dim]
            memory_emb = tf.reduce_mean(memory_emb, axis=2)

            self.probs = []
            self.dotted = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)
                self.dotted.append(dotted)

                # If not last iteration: build new query
                if hopn < self.hops - 1:
                    # Calculate probabilities
                    probs = self._memory_extraction(similarities=dotted)
                    self.probs.append(probs)

                    probs_temp = tf.expand_dims(probs, axis=-1)

                    # [batch_size, mem_size, 1]
                    # [batch_size, mem_size, embedding_dim]
                    # -> [batch_size, mem_size, embedding_dim]
                    # -> [batch_size, embedding_dim]
                    memory_search = tf.reduce_sum(memory_emb * probs_temp, axis=1)

                    query_emb = self._memory_reasoning(query=query_emb,
                                                       memory_search=memory_search,
                                                       hopn=hopn)

            return self._compute_answer(final_response=dotted)

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('train_supervision_loss')
        if self.entailment_supervision:
            loss_values.append(self.entailment_losses)
            loss_names.append('train_entailment_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        if self.entailment_supervision:
            loss_values.append(self.entailment_losses)
            loss_names.append('val_entailment_loss')

        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


class Basic_ELMo_MemN2N_v1(Basic_MemN2N_v1):
    """
    Basic Memory Network ELMo-compliant variant. Since input text is already in embeddings format (ELMo), an
    embedding layer is no longer needed.
    """

    def __init__(self, embedding_dimension=1024, embedding_mode='sentence', **kwargs):
        super(Basic_ELMo_MemN2N_v1, self).__init__(embedding_dimension=embedding_dimension, **kwargs)
        self.embedding_mode = embedding_mode

    def _build_inputs(self):

        if self.embedding_mode == 'sentence':
            self.context = tf.placeholder(tf.float32, [None, self.memory_size, self.embedding_dimension],
                                          name="context")
            self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')
        else:
            self.context = tf.placeholder(tf.float32, [None, self.memory_size, None,
                                                       self.embedding_dimension],
                                          name="context")
            self.query = tf.placeholder(tf.float32, shape=[None, None, self.embedding_dimension],
                                        name='query')

        self.context_mask = tf.placeholder(tf.float32, [None, self.memory_size], name='context_mask')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        if self.partial_supervision_info['flag']:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def _build_vars(self, vocab_size, embedding_matrix=None):
        return

    def get_output(self):
        with tf.variable_scope('Output'):

            # Query Embedding

            # [batch_size, query_length, embedding_dim] -> word embeddings
            # [batch_size, embbeding_dim] -> sentence embeddings
            query_emb = self.query

            # Position encoding
            if self.position_encoding:
                query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]

            if self.embedding_mode == 'word':
                # [batch_size, ]
                query_real_length = tf.where(tf.equal(self.query, 0), tf.zeros_like(self.query),
                                             tf.ones_like(self.query))
                query_real_length = tf.reduce_sum(query_real_length, axis=1)

                # [batch_size, embedding_dim]
                query_emb = self._sentence_embedding(query_emb, query_real_length)

            # Memory Embedding
            # [batch_size, mem_size, mem_length, embedding_dim] -> word embeddings
            # [batch_size, mem_size, embedding_dim] -> sentence embeddings
            memory_emb = self.context

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            if self.embedding_mode == 'word':
                # [batch_size * memory_size, ]
                memory = tf.reshape(self.context, [-1, self.context.shape[-1]])
                memory_real_length = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
                memory_real_length = tf.reduce_sum(memory_real_length, axis=1)

                memory_emb = self._sentence_embedding(memory_emb, memory_real_length)
                memory_emb = tf.reshape(memory_emb, [-1, self.memory_size, memory_emb.shape[-1]])

            self.probs = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)

                # Calculate probabilities

                # [batch_size, mem_size]
                probs = self._memory_extraction(similarities=dotted, mask=self.context_mask)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(memory_emb * probs_temp, axis=1)

                query_emb = self._memory_reasoning(query=query_emb,
                                                   memory_search=memory_search,
                                                   hopn=hopn)

            return self._compute_answer(final_response=query_emb)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat.lower()]
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.context: x['context'],
            self.context_mask: context_mask,
            self.query: x['text'],
            self.dropout: dropout_rate
        }

        if self.partial_supervision_info['flag']:
            targets = x['targets']
            targets = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase == 'train':
            feed_dict[self.phase] = True
        else:
            feed_dict[self.phase] = False

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def build_model(self, text_info):

        # self.sentence_size = text_info['sentence_max_length']
        # self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision_info['flag']:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size)

        if self.position_encoding:
            self.pos_encoding = positional_encoding(self.vocab_size,
                                                    self.embedding_dimension)

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)


class Basic_BERT_MemN2N_v1(Basic_MemN2N_v1):
    """
    Basic Memory Network BERT-compliant variant. Since input text is already in embeddings format (BERT), an
    embedding layer is no longer needed.
    """

    def __init__(self, embedding_dimension=768, embedding_mode='sentence', **kwargs):
        super(Basic_BERT_MemN2N_v1, self).__init__(embedding_dimension=embedding_dimension, **kwargs)
        self.embedding_mode = embedding_mode

    def _build_inputs(self):
        if self.embedding_mode == 'sentence':
            self.context = tf.placeholder(tf.float32, [None, self.memory_size, self.embedding_dimension],
                                          name="context")
            self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')
        else:
            self.context = tf.placeholder(tf.float32, [None, self.memory_size, self.sentence_size,
                                                       self.embedding_dimension],
                                          name="context")
            self.query = tf.placeholder(tf.float32, shape=[None, self.query_size, self.embedding_dimension],
                                        name='query')

        self.context_mask = tf.placeholder(tf.float32, [None, self.memory_size], name='context_mask')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        if self.partial_supervision_info['flag']:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def _build_vars(self, vocab_size, embedding_matrix=None):
        return

    def get_output(self):
        with tf.variable_scope('Output'):

            # Query Embedding

            # [batch_size, query_length, embedding_dim] -> word embeddings
            # [batch_size, embbeding_dim] -> sentence embeddings
            query_emb = self.query

            # Position encoding
            if self.position_encoding:
                query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]

            if self.embedding_mode == 'word':
                # [batch_size, ]
                query_real_length = tf.where(tf.equal(self.query, 0), tf.zeros_like(self.query),
                                             tf.ones_like(self.query))
                query_real_length = tf.reduce_sum(query_real_length, axis=1)

                # [batch_size, embedding_dim]
                query_emb = self._sentence_embedding(query_emb, query_real_length)

            # Memory Embedding
            # [batch_size, mem_size, mem_length, embedding_dim] -> word embeddings
            # [batch_size, mem_size, embedding_dim] -> sentence embeddings
            memory_emb = self.context

            # Position encoding
            if self.context_encoding is not None:
                memory_emb = memory_emb * self.context_encoding

            # Position encoding
            if self.position_encoding:
                memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

            if self.embedding_mode == 'word':
                # [batch_size * memory_size, ]
                memory = tf.reshape(self.context, [-1, self.context.shape[-1]])
                memory_real_length = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
                memory_real_length = tf.reduce_sum(memory_real_length, axis=1)

                memory_emb = self._sentence_embedding(memory_emb, memory_real_length)
                memory_emb = tf.reshape(memory_emb, [-1, self.memory_size, memory_emb.shape[-1]])

            self.probs = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)

                # Calculate probabilities

                # [batch_size, mem_size]
                probs = self._memory_extraction(similarities=dotted, mask=self.context_mask)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(memory_emb * probs_temp, axis=1)

                query_emb = self._memory_reasoning(query=query_emb,
                                                   memory_search=memory_search,
                                                   hopn=hopn)

            return self._compute_answer(final_response=query_emb)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        category = x['category']

        # Pad memory
        context_mask = []
        if 'context' not in x:
            context = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat.lower()]
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                context_mask.append([0] * cat_kb.shape[0] + [1] * memory_padding.shape[0])
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                context.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.context: x['context'],
            self.context_mask: context_mask,
            self.query: x['text'],
            self.dropout: dropout_rate
        }

        if self.partial_supervision_info['flag']:
            targets = x['targets']
            targets = pad_sequences(targets, maxlen=None, padding='post', dtype=np.int32)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            positive_indexes = positive_indexes.astype(dtype=np.int32)
            negative_indexes = negative_indexes.astype(dtype=np.int32)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase == 'train':
            feed_dict[self.phase] = True
        else:
            feed_dict[self.phase] = False

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size)

        if self.position_encoding:
            if self.position_encoding_type == 'intra':
                to_apply = position_encoding_intra
            else:
                to_apply = position_encoder_extra
            self.context_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                         sentence_size=self.sentence_size),
                                                name='context_position_encoding', dtype=np.float32)
            self.query_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                       sentence_size=self.query_size),
                                              name='query_position_encoding', dtype=np.float32)
        else:
            self.context_encoding = None
            self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)


class SemanticCoAttention(Network):

    def __init__(self, optimizer_args, clause_segments_amount, explanation_segments_amount, use_kb=True,
                 pooling_use_masking=False, pooling_att_dimension=128,
                 additive_pooling_dimension=512, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 pooling_coefficient=1., partial_supervision=None, accumulate_attention=False,
                 use_positional_encoding=False, max_grad_norm=40., clip_gradient=True, additional_data=None,
                 **kwargs):
        super(SemanticCoAttention, self).__init__(**kwargs)

        self.clause_segments_amount = clause_segments_amount
        self.explanation_segments_amount = explanation_segments_amount
        self.pooling_use_masking = pooling_use_masking
        self.pooling_att_dimension = pooling_att_dimension
        self.additive_pooling_dimension = additive_pooling_dimension
        self.optimizer_args = optimizer_args
        self.dropout_rate = dropout_rate
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.answer_weights = answer_weights
        self.additional_data = additional_data
        self.use_kb = use_kb
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.optimizer_args = optimizer_args
        self.use_kb = use_kb
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.clip_gradient = clip_gradient
        self.max_grad_norm = max_grad_norm
        self.additional_data = additional_data
        self.answer_weights = answer_weights
        self.dropout_rate = dropout_rate
        self.accumulate_attention = accumulate_attention
        self.pooling_coefficient = pooling_coefficient

        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
            self.partial_supervision_coeff = partial_supervision[1]
            self.supervision_margin = partial_supervision[2]
        else:
            self.partial_supervision = False
            self.partial_supervision_coeff = 1
            self.supervision_margin = 0.1

    def _build_inputs(self):
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('MemN2N'):

            self.initializer = initializers.get('uniform')

            if embedding_matrix is None:
                query_embedding = self.initializer([vocab_size, self.embedding_dimension])
                memory_embedding = self.initializer([vocab_size, self.embedding_dimension])

                self.query_embedding = tf.Variable(query_embedding, name='query_embedding')
                self.memory_embedding = tf.Variable(memory_embedding, name='memory_embedding')
            else:
                self.query_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                       name='query_embedding',
                                                       shape=embedding_matrix.shape, trainable=True)
                self.memory_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                        name='memory_embedding',
                                                        shape=embedding_matrix.shape, trainable=True)

        self._nil_vars = [self.query_embedding, self.memory_embedding]

    def _generalized_pooling(self, input, segments_amount):

        segment_embeddings = []
        for head_attention in range(segments_amount):
            pooled_embeddings = self._head_attention(input, head_attention)
            segment_embeddings.append(pooled_embeddings)

        #

    def get_output(self):
        with tf.variable_scope('Output'):

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.query_embedding)]):
            query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Position encoding
            if self.query_encoding is not None:
                query_emb = query_emb * self.query_encoding

            # [batch_size, embedding_dim]
            query_emb = tf.reduce_sum(query_emb, axis=1)

            # Memory Embedding

            # [batch_size, mem_size, story_length, embedding_dim]
            # with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
            memory_emb = tf.nn.embedding_lookup(self.memory_embedding, self.context)

            # Position encoding
            if self.context_encoding is not None:
                memory_emb = memory_emb * self.context_encoding

            # [batch_size, mem_size, embedding_dim]
            memory_emb = tf.reduce_sum(memory_emb, axis=2)

            self.probs = []

            for hopn in range(self.hops):
                # Compute query-memory similarity
                dotted = self._calculate_similarity(memory=memory_emb, query=query_emb, hop=hopn)

                # Calculate probabilities
                probs = self._compute_weights(similarities=dotted)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(memory_emb * probs_temp, axis=1)

                query_emb = self._aggregate_attention(query=query_emb,
                                                      memory_search=memory_search,
                                                      hopn=hopn)

            return self._compute_answer(final_response=query_emb)


class Bow_MemN2N_v1(Basic_MemN2N_v1):

    def _build_inputs(self):
        self.context = tf.placeholder(tf.float32,
                                      [None, self.memory_size, self.embedding_dimension],
                                      name="context")

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('BoW_MemN2N'):

            if self.response_linearity:
                self.H = []
                if self.share_response:
                    H_var = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_1")
                    for hop in range(self.hops):
                        self.H.append(H_var)
                else:
                    for hop in range(self.hops):
                        self.H.append(
                            tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_{}".format(hop + 1)))

        self._nil_vars = []

    def get_output(self):
        with tf.variable_scope('Output'):
            self.probs = []

            query_emb = self.query

            for hopn in range(self.hops):
                # Compute query-memory similarity
                dotted = self._memory_lookup(memory=self.context, query=query_emb, hop=hopn)

                # Calculate probabilities
                probs = self._memory_extraction(similarities=dotted)

                self.probs.append(probs)
                probs_temp = tf.expand_dims(probs, axis=-1)

                # [batch_size, mem_size, 1]
                # [batch_size, mem_size, embedding_dim]
                # -> [batch_size, mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = tf.reduce_sum(self.context * probs_temp, axis=1)

                query_emb = self._memory_reasoning(query=query_emb,
                                                   memory_search=memory_search,
                                                   hopn=hopn)

            return self._compute_answer(final_response=query_emb)

    def build_model(self, text_info):

        self.memory_size = text_info['memory_max_length']
        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=None, embedding_matrix=None)

        self.context_encoding = None
        self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)


class Transformer_v1(Network):
    """
    Transformer implementation according to Tensorflow documentation.
    https://www.tensorflow.org/tutorials/text/transformer
    """

    def __init__(self, num_layers, num_heads, dff, optimizer_args, use_kb=True, add_gradient_noise=True,
                 l2_regularization=None, clip_gradient=False, max_grad_norm=40.0, additional_data=None,
                 answer_weights=[], dropout_rate=0.1, use_batch_norm=False, **kwargs):
        super(Transformer_v1, self).__init__(**kwargs)
        self.num_layers = num_layers
        self.num_heads = num_heads
        self.dff = dff
        self.optimizer_args = optimizer_args
        self.use_kb = use_kb
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.clip_gradient = clip_gradient
        self.max_grad_norm = max_grad_norm
        self.additional_data = additional_data
        self.answer_weights = answer_weights
        self.dropout_rate = dropout_rate
        self.use_batch_norm = use_batch_norm

        if self.use_batch_norm:
            self.mlp = lambda **x: batch_norm_relu(phase=self.phase, activation=True, **x)
        else:
            self.mlp = fully_connected

    def _build_inputs(self):
        self.query = tf.placeholder(dtype=tf.int32, shape=[None, self.query_size], name='query')
        self.query_look_ahead_mask = tf.placeholder(dtype=tf.int32,
                                                    shape=[None, 1, self.query_size, self.query_size],
                                                    name='query_look_ahead_mask')

        self.context = tf.placeholder(dtype=tf.int32, shape=[None, self.sentence_size],
                                      name='context')
        self.context_mask = tf.placeholder(dtype=tf.int32, shape=[None, 1, 1, self.sentence_size],
                                           name='context_mask')
        self.context_sentence_wise_mask = tf.placeholder(dtype=tf.int32,
                                                         shape=[None, 1, self.sentence_size,
                                                                self.sentence_size],
                                                         name='context_sentence_wise_mask')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('Embeddings'):

            self.initializer = initializers.get('uniform')

            if embedding_matrix is None:
                embedding = self.initializer([vocab_size, self.embedding_dimension])

                self.embedding = tf.Variable(embedding, name='embedding')
            else:
                self.embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                 name='embedding',
                                                 shape=embedding_matrix.shape, trainable=True)

        self._nil_vars = [self.embedding]

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _compute_answer(self, final_response):

        current_answer = final_response
        with tf.variable_scope('Answer', reuse=tf.AUTO_REUSE):
            for idx, weight in enumerate(self.answer_weights):
                current_answer = self.mlp(inputs=current_answer,
                                          num_outputs=weight,
                                          activation_fn=None,
                                          scope="answer_{}".format(idx),
                                          reuse=tf.AUTO_REUSE)
                if idx < len(self.answer_weights) - 1:
                    current_answer = tf.nn.dropout(current_answer, rate=self.dropout)

            current_answer = tf.contrib.layers.fully_connected(current_answer,
                                                               1,
                                                               activation_fn=None,
                                                               scope="final_answer",
                                                               reuse=tf.AUTO_REUSE)

        return current_answer

    def multihead_attention(self, value, key, query, mask):

        def split_heads(x, batch_size, num_heads, depth):
            """
            Split the last dimension into (num_heads, depth).
            Transpose the result such that the shape is (batch_size, num_heads, seq_len, depth)
            """

            x = tf.reshape(x, (batch_size, -1, num_heads, depth))
            return tf.transpose(x, perm=[0, 2, 1, 3])

        batch_size = tf.shape(query)[0]

        # [batch_size, seq_len, d_model]
        query = tf.contrib.layers.fully_connected(query,
                                                  self.embedding_dimension,
                                                  activation_fn=None,
                                                  scope="query_mha",
                                                  reuse=tf.AUTO_REUSE)

        # [batch_size, seq_len, d_model]
        key = tf.contrib.layers.fully_connected(key,
                                                self.embedding_dimension,
                                                activation_fn=None,
                                                scope="key_mha",
                                                reuse=tf.AUTO_REUSE)

        # [batch_size, seq_len, d_model]
        value = tf.contrib.layers.fully_connected(value,
                                                  self.embedding_dimension,
                                                  activation_fn=None,
                                                  scope="value_mha",
                                                  reuse=tf.AUTO_REUSE)

        depth = self.embedding_dimension // self.num_heads

        # (batch_size, num_heads, seq_len_q, depth)
        query = split_heads(query, batch_size, num_heads=self.num_heads, depth=depth)

        # (batch_size, num_heads, seq_len_k, depth)
        key = split_heads(key, batch_size, num_heads=self.num_heads, depth=depth)

        # (batch_size, num_heads, seq_len_v, depth)
        value = split_heads(value, batch_size, num_heads=self.num_heads, depth=depth)

        # scaled_attention.shape == (batch_size, num_heads, seq_len_q, depth)
        # attention_weights.shape == (batch_size, num_heads, seq_len_q, seq_len_k)
        scaled_attention, attention_weights = scaled_dot_product_attention(
            query, key, value, mask)

        scaled_attention = tf.transpose(scaled_attention,
                                        perm=[0, 2, 1, 3])  # (batch_size, seq_len_q, num_heads, depth)

        concat_attention = tf.reshape(scaled_attention,
                                      (batch_size, -1, self.embedding_dimension))  # (batch_size, seq_len_q, d_model)

        # [batch_size, seq_len_q, d_model]
        output = tf.contrib.layers.fully_connected(concat_attention,
                                                   self.embedding_dimension,
                                                   activation_fn=None,
                                                   scope="output_mha",
                                                   reuse=tf.AUTO_REUSE)

        return output, attention_weights

    def encoder_pass(self, x, mask):
        attn_output, _ = self.multihead_attention(x, x, x, mask)  # (batch_size, input_seq_len, d_model)
        attn_output = tf.nn.dropout(attn_output, rate=self.dropout)

        with tf.variable_scope('encoder_norm1', reuse=tf.AUTO_REUSE):
            out1 = layer_norm(x + attn_output)  # (batch_size, input_seq_len, d_model)

        # (batch_size, input_seq_len, d_model)
        ffn_output = point_wise_feed_forward_network(out1, self.embedding_dimension, self.dff)
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout)

        with tf.variable_scope('encoder_norm2', reuse=tf.AUTO_REUSE):
            out2 = layer_norm(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)

        return out2

    def encoder(self, context, sentence_wise_mask):

        seq_len = tf.shape(context)[1]

        # [batch_size, input_seq_len, embedding_dimension]
        context = tf.nn.embedding_lookup(self.embedding, self.context)
        context *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))
        context += self.pos_encoding[:, :seq_len, :]

        context = tf.nn.dropout(context, rate=self.dropout)

        for i in range(self.num_layers):
            context = self.encoder_pass(context, sentence_wise_mask)

        # (batch_size, input_seq_len, embedding_dimension)
        return context

    def decoder_pass(self, x, enc_output, look_ahead_mask, padding_mask):
        # enc_output.shape == (batch_size, input_seq_len, d_model)

        # Self-attention restricted to past tokens only
        attn1, attn_weights_block1 = self.multihead_attention(x, x, x,
                                                              look_ahead_mask)  # (batch_size, target_seq_len, d_model)
        attn1 = tf.nn.dropout(attn1, rate=self.dropout)
        with tf.variable_scope('decoder_layer_norm1', reuse=tf.AUTO_REUSE):
            out1 = layer_norm(attn1 + x)

        # Attention on x given encoder output
        attn2, attn_weights_block2 = self.multihead_attention(
            enc_output, enc_output, out1, padding_mask)  # (batch_size, target_seq_len, d_model)
        attn2 = tf.nn.dropout(attn2, rate=self.dropout)

        with tf.variable_scope('decoder_layer_norm2', reuse=tf.AUTO_REUSE):
            out2 = layer_norm(attn2 + out1)  # (batch_size, target_seq_len, d_model)

        ffn_output = point_wise_feed_forward_network(out2, self.embedding_dimension,
                                                     self.dff)  # (batch_size, target_seq_len, d_model)
        ffn_output = tf.nn.dropout(ffn_output, rate=self.dropout)

        with tf.variable_scope('decoder_layer_norm3', reuse=tf.AUTO_REUSE):
            out3 = layer_norm(ffn_output + out2)  # (batch_size, target_seq_len, d_model)

        return out3, attn_weights_block1, attn_weights_block2

    def decoder(self, query, enc_output, query_look_ahead_mask, context_mask):

        seq_len = tf.shape(query)[1]

        query = tf.nn.embedding_lookup(self.embedding, query)  # (batch_size, target_seq_len, d_model)
        query *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))
        query += self.pos_encoding[:, :seq_len, :]

        query = tf.nn.dropout(query, rate=self.dropout)

        for i in range(self.num_layers):
            query, block1, block2 = self.decoder_pass(query, enc_output,
                                                      query_look_ahead_mask, context_mask)

        # x.shape == (batch_size, target_seq_len, d_model)
        return query

    def get_output(self):

        enc_output = self.encoder(context=self.context,
                                  sentence_wise_mask=self.context_sentence_wise_mask)

        dec_output = self.decoder(query=self.query,
                                  enc_output=enc_output,
                                  query_look_ahead_mask=self.query_look_ahead_mask,
                                  context_mask=self.context_mask)

        answer = tf.reduce_sum(dec_output, axis=1)
        answer = self._compute_answer(final_response=answer)

        return answer

    def build_model(self, text_info):
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self.pos_encoding = positional_encoding(self.vocab_size, self.embedding_dimension)

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)

        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']
        query_mask = x['text_combined_mask']

        concat_kb = [item for seq in self.additional_data['kb'] for item in seq]

        feed_dict = {
            self.query: query,
            self.query_look_ahead_mask: query_mask
        }

        if 'context' in x:
            raise NotImplementedError()
        else:
            context = np.array([concat_kb for _ in range(len(x['text']))])
            feed_dict[self.context] = context

            # Build kb mask
            kb_mask = context == 0
            kb_mask = kb_mask.reshape(kb_mask.shape[0], 1, 1, kb_mask.shape[1]).astype(np.float64)
            feed_dict[self.context_mask] = kb_mask

            # Compute sentence-wise mask: self-attention should restricted to tokens of the same sentence only
            # Since KB sentences are independent.
            sentence_wise_mask = np.ones(shape=[len(concat_kb), len(concat_kb)])

            start_idx = 0
            for sentence in self.additional_data['kb']:
                current_triu = np.zeros(shape=[len(sentence), len(sentence)])
                current_triu[np.triu_indices(len(sentence), 1)] = 1
                sentence_wise_mask[start_idx:current_triu.shape[0] + start_idx,
                start_idx:current_triu.shape[1] + start_idx] = current_triu

                start_idx += len(sentence)

            sentence_wise_mask = np.reshape(sentence_wise_mask, (1, 1,) + sentence_wise_mask.shape)
            sentence_wise_mask = np.repeat(sentence_wise_mask, repeats=kb_mask.shape[0], axis=0)
            combined_mask = np.maximum(sentence_wise_mask, kb_mask)
            feed_dict[self.context_sentence_wise_mask] = combined_mask

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict[self.dropout] = dropout_rate

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')

        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


class ALBERTTextClassifier(Network):

    def __init__(self, optimizer, optimizer_args, embedding_mode='sentence',
                 bert_model='https://tfhub.dev/google/albert_base/2',
                 embedding_dimension=768, answer_weights=(),
                 l2_regularization=None, dropout_rate=.2, max_grad_norm=40.,
                 additional_data=None, clip_gradient=True, add_gradient_noise=True, **kwargs):
        super(ALBERTTextClassifier, self).__init__(embedding_dimension=embedding_dimension,
                                                   **kwargs)
        self.embedding_mode = embedding_mode
        self.bert_model = bert_model
        self.answer_weights = answer_weights
        self.optimizer_args = optimizer_args
        self.optimizer = optimizer
        self.l2_regularization = 0. if l2_regularization is None else l2_regularization
        self.dropout_rate = dropout_rate
        self.additional_data = additional_data
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.max_grad_norm = max_grad_norm

        self.mlp = lambda **x: fully_connected(**x)

    def _build_inputs(self):

        self.text = tf.placeholder(dtype=tf.int32, shape=[None, None], name='text')
        self.text_mask = tf.placeholder(dtype=tf.int32, shape=[None, None], name='text_mask')
        self.text_segment_ids = tf.placeholder(dtype=tf.int32, shape=[None, None], name='text_segment_ids')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.dropout = tf.placeholder(name='dropout_rate', dtype=tf.float32)

    def _build_vars(self, vocab_size, embedding_matrix=None):
        return

    def _compute_answer(self, final_response):

        current_answer = final_response
        with tf.variable_scope('Answer', reuse=tf.AUTO_REUSE):
            for idx, weight in enumerate(self.answer_weights):
                current_answer = self.mlp(inputs=current_answer,
                                          num_outputs=weight,
                                          scope="answer_{}".format(idx),
                                          reuse=tf.AUTO_REUSE)
                if idx < len(self.answer_weights) - 1:
                    current_answer = tf.nn.dropout(current_answer, rate=self.dropout)

            current_answer = tf.contrib.layers.fully_connected(current_answer,
                                                               1,
                                                               activation_fn=None,
                                                               scope="final_answer",
                                                               reuse=tf.AUTO_REUSE)

        return current_answer

    def get_output(self):

        albert = hub.Module(self.bert_model, trainable=True)

        # [batch_size, embedding_dim]
        text_emb = albert({
            'input_ids': self.text,
            'input_mask': self.text_mask,
            'segment_ids': self.text_segment_ids
        }, signature='tokens', as_dict=True)['pooled_output']

        return self._compute_answer(text_emb)

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 labels=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def build_model(self, text_info):

        self.vocab_size = text_info['vocab_size']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size)

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        text = x['text']
        text_mask = x['text_mask']
        text_segment_ids = x['text_segment_ids']

        text = pad_sequences(text, padding='post', dtype=np.int32)
        text_mask = pad_sequences(text_mask, padding='post', dtype=np.int32)
        text_segment_ids = pad_sequences(text_segment_ids, padding='post', dtype=np.int32)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.text: text,
            self.text_mask: text_mask,
            self.text_segment_ids: text_segment_ids,
            self.dropout: dropout_rate
        }

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: strong supervision can be modelled at sentence attention level
# or maybe, just replace task specific attention with something that depends on the clause only
# and on which we can apply attention
class HAN(Network):

    def __init__(self, optimizer, optimizer_args, word_layers, sentence_layers, word_cell_type, sentence_cell_type,
                 word_projection, sentence_projection, max_grad_norm=40.0, clip_gradient=True, add_gradient_noise=True,
                 l2_regularization=None, answer_weights=(256, 64), output_size=1, position_encoding=False, **kwargs):
        super(HAN, self).__init__(**kwargs)
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.word_layers = word_layers
        self.sentence_layers = sentence_layers
        self.word_cell_type = word_cell_type
        self.sentence_cell_type = sentence_cell_type
        self.word_projection = word_projection
        self.sentence_projection = sentence_projection
        self.max_grad_norm = max_grad_norm
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.l2_regularization = l2_regularization
        self.answer_weights = answer_weights
        self.output_size = output_size
        self.position_encoding = position_encoding

        self.additional_data['categories'] = list(map(lambda x: x.upper(), self.additional_data['categories']))
        self.categories = self.additional_data['categories'] + ['not-unfair']

        self.mlp = lambda **x: fully_connected(**x)

    def _build_inputs(self):
        self.memory = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="memory")

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.clause = tf.placeholder(tf.int32, shape=[None, self.clause_size], name='clause')

        self.dropout_rate = tf.placeholder(name='dropout_rate', dtype=tf.float32)

    def _build_vars(self, vocab_size, embedding_matrix=None):

        with tf.variable_scope('embedding_weights'):

            self.initializer = initializers.get('uniform')

            if embedding_matrix is None:
                query_embedding = self.initializer([vocab_size, self.embedding_dimension])
                memory_embedding = self.initializer([vocab_size, self.embedding_dimension])

                self.clause_embedding = tf.Variable(query_embedding, name='clause_embedding')
                self.memory_embedding = tf.Variable(memory_embedding, name='memory_embedding')
            else:
                self.clause_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                        name='query_embedding',
                                                        shape=embedding_matrix.shape, trainable=True)
                self.memory_embedding = tf.get_variable(initializer=tf.constant_initializer(embedding_matrix),
                                                        name='memory_embedding',
                                                        shape=embedding_matrix.shape, trainable=True)

            self.word_attention_context_vector = tf.get_variable(name='word_attention_context_vector',
                                                                 shape=[self.word_projection],
                                                                 initializer=self.initializer,
                                                                 dtype=tf.float32)

            self.sentence_attention_context_vector = tf.get_variable(name='sentence_attention_context_vector',
                                                                     shape=[self.sentence_projection],
                                                                     initializer=self.initializer,
                                                                     dtype=tf.float32)

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 labels=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        self.cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = self.cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            self.loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                     for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += self.loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def rnn_encode(self, inputs, inputs_len, rnn_layers, cell_type, scope, reuse):

        current_input = inputs
        for idx, layer_size in enumerate(rnn_layers):
            with tf.variable_scope('{0}_{1}'.format(scope, idx), reuse=reuse):
                if cell_type == 'lstm':
                    fwd_cell = LSTMCell(layer_size)
                    bwd_cell = LSTMCell(layer_size)
                else:
                    fwd_cell = GRUCell(layer_size)
                    bwd_cell = GRUCell(layer_size)

                encoding, state = tf.nn.bidirectional_dynamic_rnn(cell_fw=fwd_cell,
                                                                  cell_bw=bwd_cell,
                                                                  inputs=current_input,
                                                                  sequence_length=inputs_len,
                                                                  dtype=tf.float32)
                current_input = tf.concat((encoding[0], encoding[1]), axis=1)

        return current_input

    def task_specific_attention(self, inputs, output_size, attention_vector, scope):

        # word level attention: [# sentences * # samples, # tokens, embedding_dim]
        # ->                    [# sentences * # samples, # tokens, output_size]
        # -->                   [# sentences * # samples, # tokens, 1]
        # --->                  [# sentences * # samples, # tokens, output_size]
        # ----->                [# sentences * # samples, output_size]

        # sentence level attention: [# samples, # sentences, dim]
        # ->                        [# samples, # sentences, output_size]
        # -->                       [# samples, # sentences, 1]
        # --->                      [# samples, # sentences, output_size]
        # --->                      [# samples, output_size]

        input_projection = self.mlp(inputs=inputs,
                                    num_outputs=output_size,
                                    activation_fn=tf.tanh,
                                    scope=scope,
                                    reuse=tf.AUTO_REUSE)

        vector_attn = tf.reduce_sum(tf.multiply(input_projection, attention_vector), axis=2, keep_dims=True)
        attention_weights = tf.nn.softmax(vector_attn, axis=1)
        weighted_projection = tf.multiply(input_projection, attention_weights)

        outputs = tf.reduce_sum(weighted_projection, axis=1)

        return outputs

    def _compute_answer(self, final_response):

        current_answer = final_response
        with tf.variable_scope('Answer', reuse=tf.AUTO_REUSE):
            for idx, weight in enumerate(self.answer_weights):
                current_answer = self.mlp(inputs=current_answer,
                                          num_outputs=weight,
                                          scope="answer_{}".format(idx),
                                          reuse=tf.AUTO_REUSE)
                if idx < len(self.answer_weights) - 1:
                    current_answer = tf.nn.dropout(current_answer, rate=self.dropout)

            current_answer = tf.contrib.layers.fully_connected(current_answer,
                                                               self.output_size,
                                                               activation_fn=None,
                                                               scope="final_answer",
                                                               reuse=tf.AUTO_REUSE)

        return current_answer

    def get_output(self):

        # [batch_size, ]
        clause_len = tf.where(tf.equal(self.clause, 0), tf.zeros_like(self.clause), tf.ones_like(self.clause))
        clause_len = tf.reduce_sum(clause_len, axis=1)

        # [batch_size * memory_size, ]
        memory = tf.reshape(self.memory, [-1, self.memory.shape[-1]])
        memory_len = tf.where(tf.equal(memory, 0), tf.zeros_like(memory), tf.ones_like(memory))
        memory_len = tf.reduce_sum(memory_len, axis=1)

        # Query Embedding

        # [batch_size, clause_length, embedding_dim]
        clause_emb = tf.nn.embedding_lookup(self.clause_embedding, self.clause)

        # Position encoding
        if self.position_encoding:
            clause_emb += self.pos_encoding[:, :tf.shape(clause_emb)[1], :]

        # Memory Embedding

        # [batch_size * mem_size, sentence_length, embedding_dim]
        memory_emb = tf.nn.embedding_lookup(self.memory_embedding, self.memory)
        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])

        # Position encoding
        if self.position_encoding:
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

        # WORD REPR #

        # RNN encoding

        # [bs, clause_length, hidden_dim]
        clause_emb = self.rnn_encode(inputs=clause_emb,
                                     inputs_len=clause_len,
                                     rnn_layers=self.word_layers,
                                     cell_type=self.word_cell_type,
                                     scope='word_encoding',
                                     reuse=True)

        # [bs * mem_size, sentence_length, hidden_dim]
        memory_emb = self.rnn_encode(inputs=memory_emb,
                                     inputs_len=memory_len,
                                     rnn_layers=self.word_layers,
                                     cell_type=self.word_cell_type,
                                     scope='word_encoding',
                                     reuse=True)

        # Word level attention

        # [bs, output_dim]
        clause_emb = self.task_specific_attention(inputs=clause_emb,
                                                  output_size=self.word_projection,
                                                  attention_vector=self.word_attention_context_vector,
                                                  scope='word_attention')

        # [bs * mem_size, output_dim]
        memory_emb = self.task_specific_attention(inputs=memory_emb,
                                                  output_size=self.word_projection,
                                                  attention_vector=self.word_attention_context_vector,
                                                  scope='word_attention')

        clause_emb = tf.nn.dropout(clause_emb, rate=self.dropout_rate)
        memory_emb = tf.nn.dropout(memory_emb, rate=self.dropout_rate)

        # SENTENCE REPR #

        # Stack clause with memory

        clause_emb = tf.expand_dims(clause_emb, axis=1)
        memory_emb = tf.reshape(memory_emb, [tf.shape(clause_emb)[0], -1, memory_emb.shape[-1]])

        # [bs, mem_size + 1, output_dim]
        stack_emb = tf.concat((clause_emb, memory_emb), axis=1)

        # RNN encoding

        # [bs, mem_size + 1, hidden_dim]
        stack_emb = self.rnn_encode(inputs=stack_emb,
                                    inputs_len=None,
                                    rnn_layers=self.sentence_layers,
                                    cell_type=self.sentence_cell_type,
                                    scope='sentence_encoding',
                                    reuse=True)

        # Sentence level attention

        stack_emb = self.task_specific_attention(inputs=stack_emb,
                                                 output_size=self.sentence_projection,
                                                 attention_vector=self.sentence_attention_context_vector,
                                                 scope='sentence_attention')

        stack_emb = tf.nn.dropout(stack_emb, rate=self.dropout_rate)

        return self._compute_answer(final_response=stack_emb)

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.clause_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            self.pos_encoding = positional_encoding(self.vocab_size,
                                                    self.embedding_dimension)

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        clause = x['text']
        category = x['category']

        # Pad memory
        if 'context' not in x:
            memory = []
            for cat in category:
                cat_kb = self.additional_data['kb'][cat]
                cat_kb = pad_sequences(cat_kb, padding='post', maxlen=self.sentence_size)
                memory_padding = np.zeros(shape=[self.memory_size - cat_kb.shape[0], cat_kb.shape[1]],
                                          dtype=cat_kb.dtype)
                cat_kb = np.concatenate((cat_kb, memory_padding), axis=0)
                memory.append(cat_kb)
        else:
            raise NotImplementedError('Sorry, non-kb context is currently disabled!')

        memory = np.array(memory)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {
            self.memory: memory,
            self.clause: clause,
            self.dropout_rate: dropout_rate
        }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('train_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('train_l2_regularization')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: test new modifications
@DeprecationWarning
class EMemN2N_tf(Basic_MemN2N_v1):
    """
    Basic MemN2N_v1 variant that allows variables memory size for each sample.
    """

    def __init__(self, max_batch_size=64, **kwargs):
        super(EMemN2N_tf, self).__init__(**kwargs)
        self.max_batch_size = max_batch_size

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, None], name="context")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, None], name='query')

        self.dropout_rate = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

    def build_model(self, text_info):

        # FIXME
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            if self.position_encoding_type == 'intra':
                to_apply = position_encoding_intra
            else:
                to_apply = position_encoder_extra
            self.context_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                         sentence_size=self.sentence_size),
                                                name='context_position_encoding', dtype=np.float32)
            self.query_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                       sentence_size=self.query_size),
                                              name='query_position_encoding', dtype=np.float32)
        else:
            self.context_encoding = None
            self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _memory_lookup(self, memory, query, hop, scope='nn_attention'):
        # memory: [batch_size * memory, embedding_dim]
        # query: [batch_size, embedding_dim]

        # [batch_size * memory, embedding_dim]
        repeated_query = tf.gather(query, self.context_segments)

        if self.similarity_operation == 'dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1)
        elif self.similarity_operation == 'nn_attention':

            # [batch_size * memory, 2 * embedding_dim]
            att_input = tf.concat((memory, repeated_query), axis=1)

            if hop > 0:
                reuse = True
            else:
                reuse = None
            with tf.variable_scope(scope, reuse=reuse):
                # -> [batch_size * mem_size, 1]
                similarities = [att_input]

                for idx, weight in enumerate(self.similarity_weights[:-1]):
                    dotted = self.mlp(inputs=similarities[-1],
                                      num_outputs=weight,
                                      scope="nn_att_{}".format(idx),
                                      reuse=reuse)

                    dotted = tf.nn.dropout(dotted, rate=self.dropout_rate)
                    similarities.append(dotted)

                dotted = tf.contrib.layers.fully_connected(similarities[-1],
                                                           self.similarity_weights[-1],
                                                           activation_fn=None,
                                                           scope="nn_att_{}".format(idx + 1),
                                                           reuse=reuse)
                similarities.append(dotted)

                # [batch_size * mem_size,]
                dotted = tf.squeeze(similarities[-1], axis=1)
        elif self.similarity_operation == 'scaled_dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted

    def get_attentions_weights(self, x, batch_size=32):
        batches, num_batches = self.get_batches(data=x, batch_size=batch_size)
        attentions = {}
        # I know for sure that x is a list
        offset = 0
        memory_indices = []

        context = []

        if len(x) == 3 or (len(x) == 2 and not self.partial_supervision):
            context = x[1]
        else:
            for batch_idx in range(len(x[0])):
                context.append(self.additional_data['kb'])

        for sample in context:
            samples_indices = list(range(offset, offset + len(sample)))
            memory_indices.append(samples_indices)
            offset += len(sample)

        to_feed = [self.probs]
        if self.use_gates:
            to_feed = [self.probs, self.gates]

        for batch_idx in range(num_batches):
            batch_x = [batches[item][batch_idx] for item in range(len(x))]

            feed_dict = self.get_feed_dict(x=batch_x,
                                           y=None,
                                           phase='test')

            batch_info = self.session.run(to_feed, feed_dict)
            batch_weights = batch_info[0]
            if self.use_gates:
                batch_gates = batch_info[1]

            if self.attention_type == 'sigmoid':
                batch_weights = [np.round(item) for item in batch_weights]

            # I cannot reshape to memory size if I have variable memory dimension per sample!
            # Instead, I can store indices
            # batch_weights = [item.reshape(-1, self.memory_size) for item in batch_weights]
            if batch_idx == 0:
                attentions.setdefault('memory_slots', []).extend(batch_weights)
                if self.use_gates:
                    attentions.setdefault('gates', []).extend(batch_gates)
            else:
                for idx, weight in enumerate(batch_weights):
                    attentions['memory_slots'][idx] = np.append(attentions['memory_slots'][idx], weight, axis=0)

                if self.use_gates:
                    for idx, gate in enumerate(batch_gates):
                        attentions['gates'][idx] = np.append(attentions['gates'][idx], gate, axis=0)

        # if self.attention_type == 'sigmoid':
        #     op = np.argwhere
        # else:
        #     op = np.argmax
        #
        # selected = np.array([[op(hop_weights[ind_slice])
        #                       for hop_weights in attentions['memory_slots']]
        #                      for ind_slice in memory_indices])

        # to_return = {
        #     'memory_slots': selected
        # }
        # if self.use_gates:
        #     to_return['hop_gates'] = np.array(attentions['gates']).reshape(attentions['gates'][0].shape[0],
        #                                                                    len(attentions['gates']))

        # return to_return
        return attentions, memory_indices

    def get_output(self):
        # query: [batch_size, q_max_length]
        # context: [batch_size * memory, s_max_length]

        with tf.variable_scope('Output'):

            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            with tf.control_dependencies([self._zero_out_padding(self.query_embedding)]):
                query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            query_emb = tf.reduce_sum(query_emb, 1)

            # Memory Embedding

            # [batch_size, mem_size, story_length, embedding_dim]
            with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
                memory_emb = tf.nn.embedding_lookup(self.memory_embedding, self.context)

            # Position encoding
            if self.context_encoding is not None:
                memory_emb = memory_emb * self.context_encoding

            # [batch_size, mem_size, embedding_dim]
            memory_emb = tf.reduce_sum(memory_emb, 2)

            self.probs = []

            for hopn in range(self.hops):

                # [batch_size * mem_size, ]
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)

                # Dynamic partition for segment-wise softmax
                dotted_mems = tf.dynamic_partition(dotted, self.context_segments, num_partitions=self.max_batch_size)
                filtered_mems = []

                def attention_method(mem):
                    if self.attention_type == 'softmax':
                        return tf.nn.softmax(mem)
                    elif self.attention_type == 'sparsemax':
                        return tf.reshape(sparsemax(tf.reshape(mem, shape=[1, tf.shape(mem)[0]])), shape=[-1])
                    else:
                        return tf.nn.sigmoid(mem)

                for mem in dotted_mems:
                    filtered = tf.cond(tf.equal(tf.size(mem), 0),
                                       true_fn=lambda: tf.zeros(shape=(1,), dtype=tf.float32),
                                       false_fn=lambda: attention_method(mem))
                    filtered_mems.append(filtered)

                filtered_mems = tf.concat(filtered_mems, axis=0)
                true_indexes = tf.range(0, tf.shape(self.context)[0])
                true_filtered_mems = tf.gather(filtered_mems, true_indexes)
                probs = tf.reshape(true_filtered_mems, shape=[-1])

                self.probs.append(probs)

                # [batch_size * mem_size, 1]
                probs_temp = tf.expand_dims(probs, -1)

                # [batch_size * mem_size, embedding_dim]
                # [batch_size * mem_size, 1]
                # -> [batch_size * mem_size, embedding_dim]
                # -> [batch_size, embedding_dim]
                memory_search = memory_emb * probs_temp
                memory_search = tf.segment_sum(memory_search, self.context_segments)

                # Dont use projection layer for adj weight sharing
                if self.response_linearity:
                    query_emb = tf.matmul(query_emb, self.H[hopn]) + memory_search
                else:
                    query_emb = query_emb + memory_search

            return self._compute_answer(final_response=query_emb)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['query']
        context = x['context']

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = [item for group in context for item in group]
        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.dropout_rate: dropout_rate,
                     }

        if self.partial_supervision:
            targets = x['targets']
            targets = pad_data(targets)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            # Option A: select the first positive target
            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: test new modifications
class Word_EMemn2n_tf(EMemN2N_tf):
    """
    Word-level Memory Network. Instead of immediately computing a weighted sentence embedding. Text inputs (query and
    memory cells) are evaluated at word-level during the Memory Lookup and Memory Reasoning phases.
    In this implementation, the content vector is at word-level at the end of the Memory Lookup phase. Subsequently,
    a Seq2Seq architecture is employed for query->content and content->query for query and memory update.
    """

    def __init__(self, enc_units=32, dec_units=32,
                 att_units=32, **kwargs):
        super(Word_EMemn2n_tf, self).__init__(**kwargs)
        self.enc_units = enc_units
        self.dec_units = dec_units
        self.att_units = att_units

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, self.sentence_size], name="context")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')
        self.context_len = tf.placeholder(tf.int32, [None], name='context_len')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, self.query_size], name='query')
        self.query_len = tf.placeholder(tf.int32, shape=[None], name='query_len')

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

        self.dropout_rate = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

    def build_model(self, text_info):

        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size,
                         embedding_matrix=text_info['embedding_matrix'])

        if self.position_encoding:
            if self.position_encoding_type == 'intra':
                to_apply = position_encoding_intra
            else:
                to_apply = position_encoder_extra
            self.context_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                         sentence_size=self.sentence_size),
                                                name='context_position_encoding', dtype=np.float32)
            self.query_encoding = tf.constant(to_apply(embedding_size=self.embedding_dimension,
                                                       sentence_size=self.query_size),
                                              name='query_position_encoding', dtype=np.float32)
        else:
            self.context_encoding = None
            self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def encoder_decoder(self, enc, dec, scope, enc_length=None, dec_length=None):

        # Encoder
        init_state = tf.zeros((tf.shape(enc)[0], self.enc_units))
        enc_cell = tf.contrib.rnn.GRUCell(self.enc_units)
        with tf.variable_scope('encoder_{}'.format(scope), reuse=tf.AUTO_REUSE):
            enc_output, enc_state = tf.nn.dynamic_rnn(enc_cell,
                                                      enc,
                                                      initial_state=init_state,
                                                      dtype=tf.float32,
                                                      sequence_length=enc_length
                                                      )

        # Decoder with attention
        with tf.variable_scope('decoder_{}'.format(scope), reuse=tf.AUTO_REUSE):
            attention_mechanism = LuongAttention(num_units=self.att_units,
                                                 memory=enc_output,
                                                 memory_sequence_length=enc_length)

            dec_cell = GRUCell(self.dec_units)
            dec_cell = AttentionWrapper(dec_cell, attention_mechanism)
            dec_cell = OutputProjectionWrapper(dec_cell, output_size=1)
            dec_init_state = dec_cell.zero_state(tf.shape(enc)[0], dtype=tf.float32).clone(cell_state=enc_state)

            token_scores, dec_state = tf.nn.dynamic_rnn(dec_cell,
                                                        dec,
                                                        dtype=tf.float32,
                                                        initial_state=dec_init_state,
                                                        sequence_length=dec_length)

        # [batch_size, timesteps, 1]
        return token_scores

    def co_attention(self, query, query_search, compute_memory_scores=True):
        # query: [batch_size, q_max_length, embedding_dim]
        # query_search: [batch_size, s_max_length embedding_dim]

        # Phase 1
        # Query Search as Encoder
        # Query as Decoder
        # -> Query scores

        # [batch_size, timesteps, 1]
        query_scores = self.encoder_decoder(enc=query_search,
                                            dec=query,
                                            scope='search_to_query',
                                            enc_length=None,
                                            dec_length=self.query_len)

        if compute_memory_scores:
            # Phase 2
            # Queryas Encoder
            # Query Search as Decoder
            # -> Query scores

            # [batch_size, timesteps, 1]
            search_scores = self.encoder_decoder(enc=query,
                                                 dec=query_search,
                                                 scope='query_to_search',
                                                 enc_length=self.query_len,
                                                 dec_length=None)

            return query_scores, search_scores

        return query_scores

    def _memory_lookup(self, memory, query, hop, scope='nn_attention'):
        # memory: [batch_size, memory, embedding_dim]
        # query: [batch_size, embedding_dim]

        # [batch_size * memory, embedding_dim]
        memory = tf.reshape(memory, [-1, self.embedding_dimension])
        query = tf.reduce_sum(query, 1)
        repeated_query = tf.gather(query, self.context_segments)

        if self.similarity_operation == 'dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1)
        elif self.similarity_operation == 'nn_attention':

            # [batch_size * memory, 2 * embedding_dim]
            att_input = tf.concat((memory, repeated_query), axis=1)

            if hop > 0:
                reuse = True
            else:
                reuse = None
            with tf.variable_scope(scope, reuse=reuse):
                # -> [batch_size * mem_size, 1]
                similarities = [att_input]

                for idx, weight in enumerate(self.similarity_weights[:-1]):
                    dotted = self.mlp(inputs=similarities[-1],
                                      num_outputs=weight,
                                      scope="nn_att_{}".format(idx),
                                      reuse=reuse)

                    dotted = tf.nn.dropout(dotted, self.keep_prob)
                    similarities.append(dotted)

                dotted = tf.contrib.layers.fully_connected(similarities[-1],
                                                           self.similarity_weights[-1],
                                                           activation_fn=None,
                                                           scope="nn_att_{}".format(idx + 1),
                                                           reuse=reuse)
                similarities.append(dotted)

                # [batch_size * mem_size,]
                dotted = tf.squeeze(similarities[-1], axis=1)
        elif self.similarity_operation == 'scaled_dot_product':
            # [batch_size * memory,]
            dotted = tf.reduce_sum(memory * repeated_query, 1) * self.embedding_dimension ** 0.5
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.similarity_operation))

        return dotted

    def get_output(self):
        # query: [batch_size, q_max_length]
        # context: [batch_size * mem_size, s_max_length]

        context = tf.reshape(self.context, [-1, self.memory_size, self.sentence_size])

        with tf.variable_scope('Output'):
            # Query Embedding

            # [batch_size, query_length, embedding_dim]
            with tf.control_dependencies([self._zero_out_padding(self.query_embedding)]):
                query_emb = tf.nn.embedding_lookup(self.query_embedding, self.query)

            # Memory Embedding

            # [batch_size, mem_size, story_length, embedding_dim]
            with tf.control_dependencies([self._zero_out_padding(self.memory_embedding)]):
                memory_emb = tf.nn.embedding_lookup(self.memory_embedding, context)

            # Position encoding
            if self.context_encoding is not None:
                memory_emb = memory_emb * self.context_encoding

            self.probs = []

            for hopn in range(self.hops):

                # [batch_size * mem_size, ]
                dotted = self._memory_lookup(memory=memory_emb, query=query_emb, hop=hopn)
                dotted = tf.reshape(dotted, [-1, self.memory_size])

                # [batch_size, mem_size]
                probs = self._memory_extraction(similarities=dotted)
                self.probs.append(probs)

                # [batch_size, mem_size, 1, 1]
                probs_temp = tf.expand_dims(probs, -1)
                probs_temp = tf.expand_dims(probs_temp, -1)

                # [batch_size, mem_size, s_max_length, embedding_dim]
                # [batch_size, mem_size, 1, 1]
                # -> [batch_size, mem_size, s_max_length, embedding_dim]
                query_search = memory_emb * probs_temp

                # [batch_size, s_max_length, embedding_dim]
                query_search = tf.reduce_sum(query_search, 1)

                if hopn < self.hops - 1:
                    # [batch_size, q_max_length]
                    # [batch_size, s_max_length]
                    query_scores, \
                    search_scores = self.co_attention(query=query_emb,
                                                      query_search=query_search)
                    search_scores = tf.reshape(search_scores, [-1, self.sentence_size])
                    temp_mem_att_scores = tf.expand_dims(tf.nn.sigmoid(search_scores), -1)
                    temp_mem_att_scores = tf.expand_dims(search_scores, 1)
                    memory_emb *= temp_mem_att_scores
                else:
                    # [batch_size, q_max_length]
                    query_scores = self.co_attention(query=query_emb,
                                                     query_search=query_search,
                                                     compute_memory_scores=False)

                query_scores = tf.reshape(query_scores, [-1, self.query_size])

                # Compute new query
                # [batch_size, q_max_length, embedding_dim]
                # [batch_size, q_max_length, 1]
                temp_query_att_scores = tf.expand_dims(tf.nn.sigmoid(query_scores), -1)
                query_emb = query_emb * temp_query_att_scores

            answer = tf.reduce_sum(query_emb, axis=1)
            for weight in self.answer_weights:
                answer = fully_connected(answer, weight)
            answer = fully_connected(answer, 1)
            return answer

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x['text']

        if 'context' not in x:
            context = []
            for _ in range(len(query)):
                context.append(self.additional_data['kb'])
        else:
            context = x['context']
            for batch_idx in range(len(query)):
                context[batch_idx].extend(self.additional_data['kb'])

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = [item for group in context for item in group]

        context_len = [len(item) for item in flattened_context]
        query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context, padding_length=self.sentence_size)
        query = pad_data(data=query, padding_length=self.query_size)

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.query_len: query_len,
                     self.dropout_rate: dropout_rate,
                     self.context_len: context_len,
                     }

        if self.partial_supervision:
            targets = x['targets']
            targets = pad_data(targets)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            mask_indexes = np.ones(shape=(len(query), self.padding_amount))

            # Option A: select the first positive target
            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]
                else:
                    mask_indexes[batch_idx] = np.zeros(shape=self.padding_amount)

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes
            feed_dict[self.mask_idxs] = mask_indexes

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        if self.use_batch_norm:
            if phase == 'train':
                feed_dict[self.phase] = True
            else:
                feed_dict[self.phase] = False

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op, self.cross_entropy_mean]
        loss_names = ['train_loss', 'train_cross_entropy']
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        loss_values = [self.loss_op, self.cross_entropy_mean]
        loss_names = ['val_loss', 'val_cross_entropy']
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: test new modifications
class Bow_EMemN2N_tf(Basic_MemN2N_v1):
    """
    Bag-of-Words Basic Memory Network variant. The only difference is that embedding layers are not required.
    """

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.float32, [None, self.embedding_dimension], name="context")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')

        self.answer = tf.placeholder(tf.float32, [None, 1], name="answer")

        self.query = tf.placeholder(tf.float32, shape=[None, self.embedding_dimension], name='query')

        if self.partial_supervision:
            self.positive_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='positive_indexes')
            self.negative_idxs = tf.placeholder(tf.int32,
                                                shape=[None, self.padding_amount, 2],
                                                name='negative_indexes')
            self.mask_idxs = tf.placeholder(tf.float32,
                                            shape=[None, self.padding_amount],
                                            name='mask_indexes')

        self.dropout_rate = tf.placeholder(name='dropout_rate', dtype=tf.float32)

        if self.use_batch_norm:
            self.phase = tf.placeholder(name='phase', dtype=tf.bool)

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def _build_vars(self, vocab_size, embedding_matrix=None):
        self.initializer = xavier_initializer()

        with tf.variable_scope('MemN2N'):

            if self.response_linearity:
                self.H = []
                if self.share_response:
                    H_var = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_1")
                    for hop in range(self.hops):
                        self.H.append(H_var)
                else:
                    for hop in range(self.hops):
                        self.H.append(
                            tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                                        name="H_{}".format(hop + 1)))

    def get_attentions_weights(self, x, batch_size=32):
        batches, num_batches = self.get_batches(data=x, batch_size=batch_size)
        attentions = {}
        # I know for sure that x is a list
        offset = 0
        memory_indices = []
        for sample in x[1]:
            samples_indices = list(range(offset, offset + len(sample)))
            memory_indices.append(samples_indices)
            offset += len(sample)

        to_feed = [self.probs]
        if self.use_gates:
            to_feed = [self.probs, self.gates]

        for batch_idx in range(num_batches):
            batch_x = [batches[item][batch_idx] for item in range(len(x))]

            feed_dict = self.get_feed_dict(x=batch_x,
                                           y=None,
                                           phase='test')

            batch_info = self.session.run(to_feed, feed_dict)
            batch_weights = batch_info[0]
            if self.use_gates:
                batch_gates = batch_info[1]

            if self.attention_type == 'sigmoid':
                batch_weights = [np.round(item) for item in batch_weights]

            # I cannot reshape to memory size if I have variable memory dimension per sample!
            # Instead, I can store indices
            # batch_weights = [item.reshape(-1, self.memory_size) for item in batch_weights]
            if batch_idx == 0:
                attentions.setdefault('memory_slots', []).extend(batch_weights)
                if self.use_gates:
                    attentions.setdefault('gates', []).extend(batch_gates)
            else:
                for idx, weight in enumerate(batch_weights):
                    attentions['memory_slots'][idx] = np.append(attentions['memory_slots'][idx], weight, axis=0)

                if self.use_gates:
                    for idx, gate in enumerate(batch_gates):
                        attentions['gates'][idx] = np.append(attentions['gates'][idx], gate, axis=0)

        # if self.attention_type == 'sigmoid':
        #     op = np.argwhere
        # else:
        #     op = np.argmax
        #
        # selected = np.array([[op(hop_weights[ind_slice])
        #                       for hop_weights in attentions['memory_slots']]
        #                      for ind_slice in memory_indices])

        # to_return = {
        #     'memory_slots': selected
        # }
        # if self.use_gates:
        #     to_return['hop_gates'] = np.array(attentions['gates']).reshape(attentions['gates'][0].shape[0],
        #                                                                    len(attentions['gates']))

        # return to_return
        return attentions, memory_indices

    def get_output(self):
        # query: [batch_size, bow_size]
        # context: [batch_size * memory, bow_size]

        with tf.variable_scope('Output'):

            query = self.query
            context = self.context

            self.probs = []

            for hopn in range(self.hops):

                # [batch_size * mem_size, ]
                dotted = self._memory_lookup(memory=context, query=query, hop=hopn)

                # Dynamic partition for segment-wise softmax
                dotted_mems = tf.dynamic_partition(dotted, self.context_segments, num_partitions=self.max_batch_size)

                filtered_mems = []

                def attention_method(mem):
                    if self.attention_type == 'softmax':
                        return tf.nn.softmax(mem)
                    elif self.attention_type == 'sparsemax':
                        return tf.reshape(sparsemax(tf.reshape(mem, shape=[1, tf.shape(mem)[0]])), shape=[-1])
                    else:
                        return tf.nn.sigmoid(mem)

                for mem in dotted_mems:
                    filtered = tf.cond(tf.equal(tf.size(mem), 0),
                                       true_fn=lambda: tf.zeros(shape=(1,), dtype=tf.float32),
                                       false_fn=lambda: attention_method(mem))
                    filtered_mems.append(filtered)

                filtered_mems = tf.concat(filtered_mems, axis=0)
                true_indexes = tf.range(0, tf.shape(context)[0])
                true_filtered_mems = tf.gather(filtered_mems, true_indexes)
                probs = tf.reshape(true_filtered_mems, shape=[-1])

                self.probs.append(probs)

                # [batch_size * mem_size, 1]
                probs_temp = tf.expand_dims(probs, -1)

                # [batch_size * mem_size, bow_size]
                # [batch_size * mem_size, 1]
                # -> [batch_size * mem_size, bow_size]
                # -> [batch_size, bow_size]
                memory_search = context * probs_temp
                memory_search = tf.segment_sum(memory_search, self.context_segments)

                if self.response_linearity:
                    query = tf.matmul(query, self.H[hopn]) + memory_search
                else:
                    query = query + memory_search

            return self._compute_answer(final_response=query)

    def build_model(self, text_info):

        self.memory_size = text_info['memory_size']
        if self.partial_supervision:
            self.padding_amount = text_info['padding_amount']

        self._build_inputs()
        self._build_vars(vocab_size=None, embedding_matrix=None)

        self.context_encoding = None
        self.query_encoding = None

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        # [batch_size, bow_size]
        query = x['query']

        # [batch_size, mem_size, bow_size]
        context = x['context']

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        context = context.reshape(-1, context.shape[-1])

        if phase == 'train':
            dropout_rate = self.dropout_rate
        else:
            dropout_rate = 0.

        feed_dict = {self.context: context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.dropout_rate: dropout_rate,
                     }

        if self.partial_supervision:
            targets = x['targets']
            targets = pad_data(targets)

            positive_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))
            negative_indexes = np.zeros(shape=(len(query), self.padding_amount, 2))

            # Option A: select the first positive target
            for batch_idx, target in enumerate(targets):
                pos_indexes = np.argwhere(target)
                pos_indexes = pos_indexes.ravel()
                if len(pos_indexes):
                    pos_choices = np.random.choice(a=pos_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, pos_idx in enumerate(pos_choices):
                        positive_indexes[batch_idx][choice_idx] = [batch_idx, pos_idx]

                    neg_indexes = np.argwhere(np.logical_not(target))
                    neg_indexes = neg_indexes.ravel()
                    neg_choices = np.random.choice(a=neg_indexes, size=self.padding_amount, replace=True)
                    for choice_idx, neg_idx in enumerate(neg_choices):
                        negative_indexes[batch_idx][choice_idx] = [batch_idx, neg_idx]

            feed_dict[self.positive_idxs] = positive_indexes
            feed_dict[self.negative_idxs] = negative_indexes

        if phase != 'test':
            y = y.reshape(-1, 1)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss_values = [self.loss_op]
        loss_names = ['train_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('supervision_loss')

        loss_values.append(self.train_op)

        loss_info = self.session.run(loss_values, feed_dict=feed_dict)
        loss_info = {name: value for name, value in zip(loss_names, loss_info[:-1])}
        return loss_info

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x, None, phase='test')
        preds = self.session.run(self.predict_op, feed_dict=feed_dict)
        return preds

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x, y, phase='validation')
        loss_values = [self.loss_op]
        loss_names = ['val_loss']
        loss_values.append(self.cross_entropy_mean)
        loss_names.append('val_cross_entropy_mean')
        if self.l2_regularization:
            loss_values.append(self.loss_l2)
            loss_names.append('val_l2_regularization')
        if self.partial_supervision:
            loss_values.append(self.supervision_losses)
            loss_names.append('val_supervision_loss')
        val_info = self.session.run(loss_values, feed_dict=feed_dict)
        val_info = {name: value for name, value in zip(loss_names, val_info)}
        return val_info


# TODO: fix to dict input format
@DeprecationWarning
class RRN_Network(Network):
    """
    Relational Recurrent Network from:
    http://papers.nips.cc/paper/7597-recurrent-relational-networks
    """

    def __init__(self, embedding_dimension, optimizer_args, clip_gradient, add_gradient_noise,
                 add_nil_vars, timesteps, post_message_weights, message_weights,
                 input_weights, attention_weights, response_weights,
                 input_lstm_size, max_grad_norm=40, session=None, use_position_labels=False,
                 use_argument_labels=False, l2_regularization=None, dropout_rate=.4):
        super(RRN_Network, self).__init__(embedding_dimension=embedding_dimension,
                                          session=session)
        self.optimizer_args = optimizer_args
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.add_nil_vars = add_nil_vars
        self.max_grad_norm = max_grad_norm
        self.use_argument_labels = use_argument_labels
        self.use_position_labels = use_position_labels
        self.l2_regularization = l2_regularization
        self.timesteps = timesteps
        self.input_weights = input_weights
        self.attention_weights = attention_weights
        self.response_weights = response_weights
        self.message_weights = message_weights
        self.input_lstm_size = input_lstm_size
        self.post_message_weights = post_message_weights
        self.dropout_rate = dropout_rate
        self.output_size = 1

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, None], name="context")
        self.context_len = tf.placeholder(tf.int32, [None, ], name="context_len")
        self.context_segments = tf.placeholder(tf.int32, [None, ], name='context_segments')

        self.edge_indices = tf.placeholder(tf.int32, shape=(None, 2), name='edge_indices')

        # [batch_size, q_max_length]
        self.query = tf.placeholder(tf.int32, shape=[None, None], name='query')
        self.query_len = tf.placeholder(tf.int32, shape=[None, ], name='query_len')

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.phase = tf.placeholder(tf.bool, name='batch_norm_phase')
        self.keep_prob = tf.placeholder(tf.float32, name='keep_prob')

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits[-1])
        self.predict_op = tf.round(predict_op, name='predict_op')

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)
        # self.grads_and_vars = grads_and_vars

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        if self.add_nil_vars:
            nil_grads_and_vars = []
            for g, v in grads_and_vars:
                if v.name in self._nil_vars:
                    nil_grads_and_vars.append((zero_nil_slot(g), v))
                else:
                    nil_grads_and_vars.append((g, v))
            self.train_op = self.opt.apply_gradients(nil_grads_and_vars, name="train_op")
        else:
            self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_model(self, text_info):

        self.max_sentence_length = text_info['sentence_max_length']
        self.max_memory_length = text_info['memory_max_length']
        self.max_question_length = text_info['sentence_max_length']

        self._build_inputs()
        self._build_vars(vocab_size=text_info['vocab_size'], embedding_matrix=text_info['embedding_matrix'])

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def _build_vars(self, vocab_size, embedding_matrix=None):

        nil_word_slot = tf.zeros([1, self.embedding_dimension])
        self.initializer = xavier_initializer()

        if embedding_matrix is None:
            A = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
        else:
            A = tf.concat(axis=0, values=[nil_word_slot, embedding_matrix])

        self.A = tf.Variable(A, name='Arguments_embedding')
        self._nil_vars = {self.A.name}

    def _convert_input(self, arguments, arguments_len, reuse=False, scope=None):
        # Input shape: # [batch_size, max_sentence_length, embedding_dimension]

        with tf.variable_scope(scope, reuse=reuse):
            forward_lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.input_lstm_size, reuse=reuse)
            backward_lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.input_lstm_size, reuse=reuse)

            _, outputs = tf.nn.bidirectional_dynamic_rnn(forward_lstm_cell,
                                                         backward_lstm_cell,
                                                         arguments,
                                                         dtype=tf.float32,
                                                         sequence_length=arguments_len
                                                         )

        # [batch_size, hidden_size * 2]
        outputs = tf.concat((outputs[0].c, outputs[1].c), axis=1)

        # [batch_size, hidden_size * 2]
        return outputs

    def compute_attention(self, x, weights, scope, reuse=False):

        attentions = []
        for idx, weight in enumerate(weights):
            with tf.variable_scope('{0}_attention_mask_{1}'.format(scope, idx)) as att_scope:

                if idx == 0:
                    attention = batch_norm_relu(x, weight, activation=True, reuse=reuse,
                                                scope=att_scope)
                elif idx < len(weights) - 1:
                    attention = batch_norm_relu(attentions[-1], weight, activation=True, reuse=reuse,
                                                scope=att_scope)
                else:
                    attention = batch_norm_relu(attentions[-1], weight, activation=True, reuse=reuse,
                                                scope=att_scope)

                attentions.append(attention)

        return attentions[-1]

    def build_loss(self):
        self.logits = self.get_output()

        entropies = []
        for logit in self.logits:
            cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=logit,
                                                                     targets=self.answer,
                                                                     pos_weight=self.pos_weight,
                                                                     name="cross_entropy")
            cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
            entropies.append(cross_entropy_mean)

        entropies = tf.stack(entropies, axis=0)
        entropies_mean = tf.reduce_mean(entropies, axis=0)

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n(
                [tf.nn.l2_loss(v) for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op = entropies_mean + loss_l2
        else:
            self.loss_op = entropies_mean

    def mlp(self, x, weights, scope, reuse):

        inputs = [x]

        with tf.variable_scope(scope):
            for idx, weight in enumerate(weights):

                if idx < len(weights) - 1:
                    current_input = batch_norm_relu(inputs=inputs[-1],
                                                    num_outputs=weight,
                                                    activation=True,
                                                    scope='{0}_{1}'.format(scope, idx),
                                                    reuse=reuse)
                    current_input = tf.nn.dropout(current_input, self.keep_prob)
                else:
                    current_input = batch_norm_relu(inputs=inputs[-1],
                                                    num_outputs=weight,
                                                    activation=False,
                                                    scope='{0}_{1}'.format(scope, idx),
                                                    reuse=reuse)

                inputs.append(current_input)

        return inputs[-1]

    def message_passing(self, nodes, edges, scope, reuse):

        # [#edges, 2, hidden_size * 2]
        pairs = tf.gather(nodes, edges)

        # [#edges, 4 * hidden_size]
        reshaped_pairs = tf.reshape(pairs, shape=[-1, 2 * pairs.shape[-1]])
        messages = self.mlp(x=reshaped_pairs,
                            weights=self.message_weights,
                            scope=scope,
                            reuse=reuse)

        # [#edges, 1]
        attention_weights = self.compute_attention(x=reshaped_pairs,
                                                   weights=self.attention_weights,
                                                   scope='attention',
                                                   reuse=reuse)

        messages = messages * attention_weights

        idx_i, _ = tf.split(edges, 2, 1)
        out_shape = (tf.shape(nodes)[0], messages.shape[-1])
        updates = tf.scatter_nd(idx_i, messages, out_shape)

        # [batch_size * memory_real_length, hidden_size * 2]
        return updates

    def get_output(self):
        mask_args_padding_zero_op = tf.scatter_update(self.A,
                                                      0,
                                                      tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

        with tf.control_dependencies([mask_args_padding_zero_op]):
            emb_query = tf.nn.embedding_lookup(self.A, self.query)
            emb_context = tf.nn.embedding_lookup(self.A, self.context)

        # [batch_size, hidden_size * 2]
        query_conv = self._convert_input(arguments=emb_query, arguments_len=self.query_len, scope='question_encoder')

        # [batch_size * memory, hidden_size * 2]
        context_conv = self._convert_input(arguments=emb_context, arguments_len=self.context_len,
                                           scope='context_encoder')

        initial_input = tf.concat([context_conv, tf.gather(query_conv, self.context_segments)], axis=1)
        initial_input = self.mlp(initial_input, scope='initial_encoding', weights=self.input_weights, reuse=False)

        current_inputs = [initial_input]

        # RRN
        responses = []

        with tf.variable_scope('steps'):
            lstm_cell = LSTMCell(initial_input.shape[-1])
            state = lstm_cell.zero_state(tf.shape(initial_input)[0], tf.float32)

            for timestep in range(self.timesteps):
                # [batch_size * memory_real_length, hidden_size * 2]
                current_input = self.message_passing(current_inputs[-1],
                                                     self.edge_indices,
                                                     reuse=True if timestep > 0 else False,
                                                     scope='message_passing')
                # [batch_size * memory_real_length, hidden_size * 4]
                current_input = self.mlp(x=tf.concat((current_input, current_inputs[0]), axis=1),
                                         reuse=True if timestep > 0 else False,
                                         scope='post_message_passing',
                                         weights=self.post_message_weights)

                # [batch_size * memory_real_length, hidden_size * 2]
                current_input, state = lstm_cell(current_input, state)

                with tf.variable_scope('graph_sum'):
                    graph_sum = tf.segment_sum(current_input, self.context_segments)
                    output = self.mlp(x=graph_sum,
                                      weights=self.response_weights,
                                      scope='graph_fn',
                                      reuse=True if timestep > 0 else False)
                    responses.append(output)

        # [batch_size, num_classes]
        return responses

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Build support placeholders
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        edge_indices = []

        offset = 0

        for item in x[1]:
            n_facts = len(item)
            edge_indices.extend([[i + offset, j + offset] for i in range(n_facts) for j in range(n_facts)])

        # Pad data
        flattened_context = []
        for item in context:
            flattened_context += item

        context_len = [len(item) for item in flattened_context]
        query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        if phase == 'train':
            bn_phase = True
            keep_prob = 1 - self.dropout_rate
        else:
            bn_phase = False
            keep_prob = 1.0

        feed_dict = {
            self.query: query,
            self.query_len: query_len,
            self.context: flattened_context,
            self.context_segments: context_segments,
            self.context_len: context_len,
            self.edge_indices: edge_indices,
            self.phase: bn_phase,
            self.keep_prob: keep_prob
        }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')

        loss, _ = self.session.run(
            [self.loss_op, self.train_op],
            feed_dict=feed_dict)

        return loss

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


@DeprecationWarning
class EntNet(Network):
    """
    EntNet implementation from: Tracking the World State with Recurrent Entity Networks
    (https://arxiv.org/abs/1612.03969)
    """

    def __init__(self, num_blocks, optimizer, optimizer_args, l2_regularization=None,
                 clip_gradient=True, add_gradient_noise=True, max_grad_norm=40.0, **kwargs):
        super(EntNet, self).__init__(**kwargs)
        self.num_blocks = num_blocks
        self.l2_regularization = l2_regularization
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.clip_gradient = clip_gradient
        self.add_gradient_noise = add_gradient_noise
        self.max_grad_norm = max_grad_norm
        self.output_size = 1

    def _build_inputs(self):
        # [batch_size * memory, s_max_length]
        self.context = tf.placeholder(tf.int32, [None, self.memory_size, self.sentence_size], name="context")
        self.context_len = tf.placeholder(tf.int32, [None], name="context_len")

        self.answer = tf.placeholder(tf.float32, [None, self.output_size], name="answer")

        self.query = tf.placeholder(tf.int32, shape=[None, self.sentence_size], name='query')
        self.query_len = tf.placeholder(tf.int32, shape=[None], name='query_len')

        self.keys = tf.placeholder(tf.int32, shape=[None, self.num_blocks], name='keys')

    def _build_vars(self, vocab_size, embedding_matrix=None):

        nil_word_slot = tf.zeros([1, self.embedding_dimension])
        self.initializer = xavier_initializer()

        if embedding_matrix is None:
            A = tf.concat(axis=0, values=[nil_word_slot, self.initializer([vocab_size - 1, self.embedding_dimension])])
        else:
            A = tf.concat(axis=0, values=[nil_word_slot, embedding_matrix])

        self.A = tf.Variable(A, name='Arguments_embedding')

        self.H = tf.Variable(self.initializer([self.embedding_dimension, self.embedding_dimension]),
                             name='projection_H')

        self.P = tf.get_variable(
            name='positional_mask',
            shape=[self.sentence_size, self.embedding_dimension])

    def _get_input_encoding(self, inputs, initializer=None, scope=None):
        """
        Implementation of the learned multiplicative mask from Section 2.1, Equation 1.
        This module is also described in [End-To-End Memory Networks](https://arxiv.org/abs/1502.01852)
        as Position Encoding (PE). The mask allows the ordering of words in a sentence to affect the
        encoding.
        """
        with tf.variable_scope(scope, 'Encoding', initializer=initializer):
            encoded_input = tf.reduce_sum(inputs * self.P, axis=2)
            return encoded_input

    def _compute_answer(self, last_state, encoded_query, initializer):

        with tf.variable_scope('output', initializer=initializer):
            last_state = tf.stack(tf.split(last_state, self.num_blocks, axis=1), axis=1)

            # use the encoded query to attend over memories (dot product)
            attention = tf.reduce_sum(last_state * encoded_query, axis=2)

            # Subtract max for numerical stability (softmax is shift invariant
            attention_max = tf.reduce_max(attention, axis=-1, keep_dims=True)
            attention = tf.nn.softmax(attention - attention_max)
            attention = tf.expand_dims(attention, axis=2)

            # weight memories by attention vectors
            u = tf.reduce_sum(last_state * attention, axis=1)

            return tf.contrib.layers.fully_connected(tf.matmul(u, self.H),
                                                     1,
                                                     activation_fn=None,
                                                     scope='answer',
                                                     reuse=False)

    def get_output(self):

        ones_initializer = tf.constant_initializer(1.0)
        normal_initializer = tf.random_normal_initializer(stddev=0.1)

        mask_args_padding_zero_op = tf.scatter_update(self.A,
                                                      0,
                                                      tf.zeros([self.embedding_dimension, ], dtype=tf.float32))

        with tf.control_dependencies([mask_args_padding_zero_op]):
            emb_query = tf.nn.embedding_lookup(self.A, self.query)
            emb_context = tf.nn.embedding_lookup(self.A, self.context)

        encoded_query = self._get_input_encoding(inputs=emb_query,
                                                 initializer=ones_initializer,
                                                 scope='query_encoding')

        encoded_context = self._get_input_encoding(inputs=emb_context,
                                                   initializer=ones_initializer,
                                                   scope='context_encoding')

        dmc = DynamicMemoryCell(
            num_blocks=self.num_blocks,
            num_units_per_block=self.embedding_dimension,
            keys=self.keys,
            initializer=normal_initializer,
            recurrent_initializer=normal_initializer,
            activation=tf.keras.layers.PReLU(alpha_initializer=ones_initializer)
        )

        # Recurrence
        initial_state = dmc.zero_state(batch_size=tf.shape(self.query)[0], dtype=tf.float32)
        _, last_state = tf.nn.dynamic_rnn(cell=dmc,
                                          inputs=encoded_context,
                                          sequence_length=self.context_len,
                                          initial_state=initial_state)

        outputs = self._compute_answer(last_state=last_state,
                                       encoded_query=encoded_query,
                                       initializer=normal_initializer)

        return outputs

    def build_predict_op(self):
        predict_op = tf.nn.sigmoid(self.logits)
        self.predict_op = tf.round(predict_op, name='predict_op')

    def build_loss(self):
        self.logits = self.get_output()
        cross_entropy = tf.nn.weighted_cross_entropy_with_logits(logits=self.logits,
                                                                 targets=self.answer,
                                                                 pos_weight=self.pos_weight,
                                                                 name="cross_entropy")
        # reduce_mean should be more stable w.r.t. batch size
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name="cross_entropy_mean")
        self.loss_op = cross_entropy_mean

        if self.l2_regularization is not None:
            trainable_vars = tf.trainable_variables()
            loss_l2 = tf.add_n([tf.nn.l2_loss(v)
                                for v in trainable_vars if 'bias' not in v.name]) * self.l2_regularization
            self.loss_op += loss_l2

    def build_train_op(self):
        grads_and_vars = self.opt.compute_gradients(self.loss_op)

        if self.clip_gradient:
            grads_and_vars = [(tf.clip_by_norm(g, self.max_grad_norm), v) for g, v in grads_and_vars]
        if self.add_gradient_noise:
            grads_and_vars = [(add_gradient_noise(g), v) for g, v in grads_and_vars]

        self.train_op = self.opt.apply_gradients(grads_and_vars, name="train_op")

    def build_model(self, text_info):

        # FIXME
        self.sentence_size = text_info['sentence_max_length']
        self.query_size = text_info['query_max_length']
        self.memory_size = text_info['memory_max_length']
        self.vocab_size = text_info['vocab_size']

        self._build_inputs()
        self._build_vars(vocab_size=self.vocab_size, embedding_matrix=text_info['embedding_matrix'])

        self.opt = tf.train.AdamOptimizer(**self.optimizer_args)
        self.build_loss()
        self.build_train_op()
        self.build_predict_op()

        init_op = tf.global_variables_initializer()
        self.session.run(init_op)

    def get_feed_dict(self, x, y=None, phase='train'):
        query = x[0]
        context = x[1]

        # Compute context_segments
        context_segments = [idx for idx, item in enumerate(context) for _ in range(len(item))]
        context_segments = np.array(context_segments)

        # Pad data
        flattened_context = []
        for item in context:
            flattened_context += item

        context_len = [len(item) for item in flattened_context]
        query_len = [len(item) for item in query]

        flattened_context = pad_data(data=flattened_context)
        query = pad_data(data=query)

        feed_dict = {self.context: flattened_context,
                     self.context_segments: context_segments,
                     self.query: query,
                     self.context_len: context_len,
                     self.query_len: query_len
                     }

        if phase != 'test':
            y = y.reshape(-1, self.output_size)
            feed_dict[self.answer] = y

        return feed_dict

    def batch_fit(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='train')
        loss, _ = self.session.run([self.loss_op, self.train_op], feed_dict=feed_dict)
        return loss

    def batch_predict(self, x):
        feed_dict = self.get_feed_dict(x=x, y=None, phase='test')
        return self.session.run(self.predict_op, feed_dict=feed_dict)

    def batch_evaluate(self, x, y):
        feed_dict = self.get_feed_dict(x=x, y=y, phase='validation')
        return self.session.run(self.loss_op, feed_dict=feed_dict)


class DNC(Network):
    """
    Differential Neural Computer from: Hybrid computing using a neural network with dynamic external memory
    """

    def __init__(self, memory_size, word_size, num_read_heads,
                 num_write_heads, ctrl_hidden_size, clip_value=20, batch_size=16, **kwargs):
        super(DNC, self).__init__(**kwargs)
        self.memory_size = memory_size
        self.word_size = word_size
        self.num_read_heads = num_read_heads
        self.num_write_heads = num_write_heads
        self.ctrl_hidden_size = ctrl_hidden_size
        self.clip_value = clip_value
        self.batch_size = batch_size

    def _build_inputs(self):
        self.input_sequence = tf.placeholder(shape=[None, 1, self.embedding_dimension], name='input_sequence',
                                             dtype=tf.float32)
        self.answer = tf.placeholder(shape=[None, 1], name='answer', dtype=tf.float32)

    def get_output(self):
        dnc_core = custom_tf_v1.DNC(memory_size=self.memory_size, word_size=self.word_size,
                                    num_reads=self.num_read_heads, num_writes=self.num_write_heads,
                                    ctrl_hidden_size=self.ctrl_hidden_size, clip_value=self.clip_value, output_size=1)
        initial_state = dnc_core.initial_state(self.batch_size)
        output_sequence, _ = tf.nn.dynamic_rnn(
            cell=dnc_core,
            inputs=self.input_sequence,
            time_major=True,
            initial_state=initial_state
        )

        return output_sequence


class ModelFactory(object):
    supported_models = {
        'vanilla_lstm_v1': VanillaLSTM,
        'bi_gru_v1': BiGRU,
        'cbi_gru_v1': CBiGRU,
        'basic_memn2n_v1': Basic_MemN2N_v1,
        'multi_basic_memn2n_v1': MultiBasic_MemN2N_v1,
        'similarity_memn2n_v1': Similarity_MemN2N_v1,
        'gated_memn2n_v1': Gated_MemN2N_v1,
        'entailment_memn2n_v1': Entailment_MemN2N_v1,
        'ememn2n_v1': EMemN2N_tf,
        'bow_memn2n_v1': Bow_MemN2N_v1,
        'recurrent_relation_network_v1': RRN_Network,
        'word_ememn2n_v1': Word_EMemn2n_tf,
        'basic_elmo_memn2n_v1': Basic_ELMo_MemN2N_v1,
        'basic_bert_memn2n_v1': Basic_BERT_MemN2N_v1,
        'transformer_v1': Transformer_v1,
        'albert_text_classifier_v1': ALBERTTextClassifier,
        'han_v1': HAN
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ModelFactory.supported_models[key]:
            return ModelFactory.supported_models[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
