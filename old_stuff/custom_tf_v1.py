"""

@Author: Federico Ruggeri

@Date: 20/12/18

TODO: re-organize everything. This is kinda messy.

"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections

# import numpy as np
# import sonnet as snt
# import tensorflow as tf
# from keras import backend as K
# from tensorflow.python.ops import array_ops
# from tensorflow.python.ops import init_ops
# from tensorflow.python.ops import math_ops
# from tensorflow.python.ops import nn_ops
# from tensorflow.python.ops import variable_scope as vs
# from tensorflow.python.ops.math_ops import sigmoid
# from tensorflow.python.ops.math_ops import tanh
# from tensorflow.python.ops.rnn_cell_impl import RNNCell
# from tensorflow.python.platform import tf_logging as logging
# from tensorflow.python.util import nest

SkipGRUStateTuple = collections.namedtuple("SkipGRUStateTuple", ("h", "update_prob", "cum_update_prob"))
SkipGRUOutputTuple = collections.namedtuple("SkipGRUOutputTuple", ("h", "state_gate"))


# class SkipGRUCell(tf.nn.rnn_cell.RNNCell):
#     """
#     Single Skip GRU cell. Augments the basic GRU cell with a binary output that decides whether to
#     update or copy the cell state. The binary neuron is optimized using the Straight Through Estimator.
#     """
#
#     def __init__(self, num_units, activation=tf.tanh, layer_norm=False, update_bias=1.0):
#         """
#         Initialize the Skip GRU cell
#         :param num_units: int, the number of units in the GRU cell
#         :param activation: activation function of the inner states
#         :param layer_norm: bool, whether to use layer normalization
#         :param update_bias: float, initial value for the bias added to the update state gate
#         """
#         self._num_units = num_units
#         self._activation = activation
#         self._layer_norm = layer_norm
#         self._update_bias = update_bias
#
#     @property
#     def state_size(self):
#         return SkipGRUStateTuple(self._num_units, 1, 1)
#
#     @property
#     def output_size(self):
#         return SkipGRUOutputTuple(self._num_units, 1)
#
#     def __call__(self, inputs, state, scope=None):
#         with tf.variable_scope(scope or type(self).__name__):
#             h_prev, update_prob_prev, cum_update_prob_prev = state
#
#             # Parameters of gates are concatenated into one multiply for efficiency.
#             with tf.variable_scope("gates"):
#                 concat = rnn_ops.linear([inputs, h_prev], 2 * self._num_units, bias=True, bias_start=1.0)
#
#             # r = reset_gate, u = update_gate
#             r, u = tf.split(value=concat, num_or_size_splits=2, axis=1)
#
#             if self._layer_norm:
#                 r = rnn_ops.layer_norm(r, name="r")
#                 u = rnn_ops.layer_norm(u, name="u")
#
#             # Apply non-linearity after layer normalization
#             r = tf.sigmoid(r)
#             u = tf.sigmoid(u)
#
#             with tf.variable_scope("candidate"):
#                 new_c_tilde = self._activation(rnn_ops.linear([inputs, r * h_prev], self._num_units, True))
#             new_h_tilde = u * h_prev + (1 - u) * new_c_tilde
#
#             # Compute value for the update prob
#             with tf.variable_scope('state_update_prob'):
#                 new_update_prob_tilde = rnn_ops.linear(new_h_tilde, 1, True, bias_start=self._update_bias)
#                 new_update_prob_tilde = tf.sigmoid(new_update_prob_tilde)
#
#             # Compute value for the update gate
#             cum_update_prob = cum_update_prob_prev + tf.minimum(update_prob_prev, 1. - cum_update_prob_prev)
#             update_gate = _binary_round(cum_update_prob)
#
#             # Apply update gate
#             new_h = update_gate * new_h_tilde + (1. - update_gate) * h_prev
#             new_update_prob = update_gate * new_update_prob_tilde + (1. - update_gate) * update_prob_prev
#             new_cum_update_prob = update_gate * 0. + (1. - update_gate) * cum_update_prob
#
#             new_state = SkipGRUStateTuple(new_h, new_update_prob, new_cum_update_prob)
#             new_output = SkipGRUOutputTuple(new_h, update_gate)
#
#             return new_output, new_state
#
#     def trainable_initial_state(self, batch_size):
#         """
#         Create a trainable initial state for the SkipGRUCell
#         :param batch_size: number of samples per batch
#         :return: SkipGRUStateTuple
#         """
#         with tf.variable_scope('initial_h'):
#             initial_h = rnn_ops.create_initial_state(batch_size, self._num_units)
#         with tf.variable_scope('initial_update_prob'):
#             initial_update_prob = rnn_ops.create_initial_state(batch_size, 1, trainable=False,
#                                                                initializer=tf.ones_initializer())
#         with tf.variable_scope('initial_cum_update_prob'):
#             initial_cum_update_prob = rnn_ops.create_initial_state(batch_size, 1, trainable=False,
#                                                                    initializer=tf.zeros_initializer())
#         return SkipGRUStateTuple(initial_h, initial_update_prob, initial_cum_update_prob)


# Ensure values are greater than epsilon to avoid numerical instability.
_EPSILON = 1e-6

TemporalLinkageState = collections.namedtuple('TemporalLinkageState',
                                              ('link', 'precedence_weights'))

DNCState = collections.namedtuple('DNCState', ('access_output', 'access_state',
                                               'controller_state'))

AccessState = collections.namedtuple('AccessState', ('memory', 'read_weights', 'write_weights', 'linkage', 'usage'))


# def f1_loss(y_true, y_pred):
#     tp = K.sum(K.cast(y_true * y_pred, 'float'), axis=0)
#     # tn = K.sum(K.cast((1 - y_true) * (1 - y_pred), 'float'), axis=0)
#     fp = K.sum(K.cast((1 - y_true) * y_pred, 'float'), axis=0)
#     fn = K.sum(K.cast(y_true * (1 - y_pred), 'float'), axis=0)
#
#     p = tp / (tp + fp + K.epsilon())
#     r = tp / (tp + fn + K.epsilon())
#
#     f1 = 2 * p * r / (p + r + K.epsilon())
#     f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
#     return 1 - K.mean(f1)
#
#
# class AttentionGRUCell(RNNCell):
#     """Gated Recurrent Unit incoporating attention (cf. https://arxiv.org/abs/1603.01417).
#        Adapted from https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/rnn/python/ops/core_rnn_cell_impl.py
#        NOTE: Takes an input of shape:  (batch_size, max_time_step, input_dim + 1)
#        Where an input vector of shape: (batch_size, max_time_step, input_dim)
#        and scalar attention of shape:  (batch_size, max_time_step, 1)
#        are concatenated along the final axis"""
#
#     def __init__(self, num_units, input_size=None, activation=tanh):
#         super(AttentionGRUCell, self).__init__()
#         if input_size is not None:
#             logging.warn("%s: The input_size parameter is deprecated.", self)
#         self._num_units = num_units
#         self._activation = activation
#
#     @property
#     def state_size(self):
#         return self._num_units
#
#     @property
#     def output_size(self):
#         return self._num_units
#
#     def __call__(self, inputs, state, scope=None):
#         """Attention GRU with nunits cells."""
#         with vs.variable_scope(scope or "attention_gru_cell"):
#             with vs.variable_scope("gates"):  # Reset gate and update gate.
#                 # We start with bias of 1.0 to not reset and not update.
#                 if inputs.get_shape()[-1] != self._num_units + 1:
#                     raise ValueError("Input should be passed as word input concatenated with 1D attention on end axis")
#                 # extract input vector and attention
#                 inputs, g = array_ops.split(inputs,
#                                             num_or_size_splits=[self._num_units, 1],
#                                             axis=1)
#                 r = _linear([inputs, state], self._num_units, True)
#                 r = sigmoid(r)
#             with vs.variable_scope("candidate"):
#                 r = r * _linear(state, self._num_units, False)
#             with vs.variable_scope("input"):
#                 x = _linear(inputs, self._num_units, True)
#             h_hat = self._activation(r + x)
#
#             new_h = (1 - g) * state + g * h_hat
#         return new_h, new_h


# def _linear(args, output_size, bias, bias_start=0.0):
#     """Linear map: sum_i(args[i] * W[i]), where W[i] is a variable.
#     Args:
#     args: a 2D Tensor or a list of 2D, batch x n, Tensors.
#     output_size: int, second dimension of W[i].
#     bias: boolean, whether to add a bias term or not.
#     bias_start: starting value to initialize the bias; 0 by default.
#     Returns:
#     A 2D Tensor with shape [batch x output_size] equal to
#     sum_i(args[i] * W[i]), where W[i]s are newly created matrices.
#     Raises:
#     ValueError: if some of the arguments has unspecified or wrong shape.
#     """
#     if args is None or (nest.is_sequence(args) and not args):
#         raise ValueError("`args` must be specified")
#     if not nest.is_sequence(args):
#         args = [args]
#
#     # Calculate the total size of arguments on dimension 1.
#     total_arg_size = 0
#     shapes = [a.get_shape() for a in args]
#     for shape in shapes:
#         if shape.ndims != 2:
#             raise ValueError("linear is expecting 2D arguments: %s" % shapes)
#         if shape[1].value is None:
#             raise ValueError("linear expects shape[1] to be provided for shape %s, "
#                              "but saw %s" % (shape, shape[1]))
#         else:
#             total_arg_size += shape[1].value
#
#     dtype = [a.dtype for a in args][0]
#
#     # Now the computation.
#     scope = vs.get_variable_scope()
#     with vs.variable_scope(scope) as outer_scope:
#         weights = vs.get_variable(
#             "weights", [total_arg_size, output_size], dtype=dtype)
#         if len(args) == 1:
#             res = math_ops.matmul(args[0], weights)
#         else:
#             res = math_ops.matmul(array_ops.concat(args, 1), weights)
#         if not bias:
#             return res
#         with vs.variable_scope(outer_scope) as inner_scope:
#             inner_scope.set_partitioner(None)
#             biases = vs.get_variable(
#                 "biases", [output_size],
#                 dtype=dtype,
#                 initializer=init_ops.constant_initializer(bias_start, dtype=dtype))
#     return nn_ops.bias_add(res, biases)


# https://github.com/jimfleming/recurrent-entity-networks
# class DynamicMemoryCell(tf.contrib.rnn.RNNCell):
#     """
#     Implementation of a dynamic memory cell as a gated recurrent network.
#     The cell's hidden state is divided into blocks and each block's weights are tied.
#     """
#
#     def __init__(self,
#                  num_blocks,
#                  num_units_per_block,
#                  keys,
#                  initializer=None,
#                  recurrent_initializer=None,
#                  activation=tf.nn.relu):
#         self._num_blocks = num_blocks  # M
#         self._num_units_per_block = num_units_per_block  # d
#         self._keys = keys
#         self._activation = activation  # \phi
#         self._initializer = initializer
#         self._recurrent_initializer = recurrent_initializer
#
#     @property
#     def state_size(self):
#         "Return the total state size of the cell, across all blocks."
#         return self._num_blocks * self._num_units_per_block
#
#     @property
#     def output_size(self):
#         "Return the total output size of the cell, across all blocks."
#         return self._num_blocks * self._num_units_per_block
#
#     def zero_state(self, batch_size, dtype):
#         "Initialize the memory to the key values."
#         zero_state = tf.concat([tf.expand_dims(key, axis=0) for key in self._keys], axis=1)
#         zero_state_batch = tf.tile(zero_state, [batch_size, 1])
#         return zero_state_batch
#
#     def get_gate(self, state_j, key_j, inputs):
#         """
#         Implements the gate (scalar for each block). Equation 2:
#         g_j <- \sigma(s_t^T h_j + s_t^T w_j)
#         """
#         a = tf.reduce_sum(inputs * state_j, axis=1)
#         b = tf.reduce_sum(inputs * key_j, axis=1)
#         return tf.sigmoid(a + b)
#
#     def get_candidate(self, state_j, key_j, inputs, U, V, W, U_bias):
#         """
#         Represents the new memory candidate that will be weighted by the
#         gate value and combined with the existing memory. Equation 3:
#         h_j^~ <- \phi(U h_j + V w_j + W s_t)
#         """
#         key_V = tf.matmul(key_j, V)
#         state_U = tf.matmul(state_j, U) + U_bias
#         inputs_W = tf.matmul(inputs, W)
#         return self._activation(state_U + inputs_W + key_V)
#
#     def __call__(self, inputs, state, scope=None):
#         with tf.variable_scope(scope or type(self).__name__, initializer=self._initializer):
#             U = tf.get_variable('U', [self._num_units_per_block, self._num_units_per_block],
#                                 initializer=self._recurrent_initializer)
#             V = tf.get_variable('V', [self._num_units_per_block, self._num_units_per_block],
#                                 initializer=self._recurrent_initializer)
#             W = tf.get_variable('W', [self._num_units_per_block, self._num_units_per_block],
#                                 initializer=self._recurrent_initializer)
#
#             U_bias = tf.get_variable('U_bias', [self._num_units_per_block])
#
#             # Split the hidden state into blocks (each U, V, W are shared across blocks).
#             state = tf.split(state, self._num_blocks, axis=1)
#
#             next_states = []
#             for j, state_j in enumerate(state):  # Hidden State (j)
#                 key_j = tf.expand_dims(self._keys[j], axis=0)
#                 gate_j = self.get_gate(state_j, key_j, inputs)
#                 candidate_j = self.get_candidate(state_j, key_j, inputs, U, V, W, U_bias)
#
#                 # Equation 4: h_j <- h_j + g_j * h_j^~
#                 # Perform an update of the hidden state (memory).
#                 state_j_next = state_j + tf.expand_dims(gate_j, -1) * candidate_j
#
#                 # Equation 5: h_j <- h_j / \norm{h_j}
#                 # Forget previous memories by normalization.
#                 state_j_next_norm = tf.norm(
#                     tensor=state_j_next,
#                     ord='euclidean',
#                     axis=-1,
#                     keep_dims=True)
#                 state_j_next_norm = tf.where(
#                     tf.greater(state_j_next_norm, 0.0),
#                     state_j_next_norm,
#                     tf.ones_like(state_j_next_norm))
#                 state_j_next = state_j_next / state_j_next_norm
#
#                 next_states.append(state_j_next)
#             state_next = tf.concat(next_states, axis=1)
#         return state_next, state_next
#
#
# # DNC: https://github.com/deepmind/dnc
# class DNC(snt.RNNCore):
#     """DNC core module.
#     Contains controller and memory access module.
#     """
#
#     def __init__(self,
#                  access_config, controller_config,
#                  output_size,
#                  clip_value=None,
#                  name='dnc'):
#         """Initializes the DNC core.
#         Args:
#           access_config: dictionary of access module configurations.
#           controller_config: dictionary of controller (LSTM) module configurations.
#           output_size: output dimension size of core.
#           clip_value: clips controller and core output values to between
#               `[-clip_value, clip_value]` if specified.
#           name: module name (default 'dnc').
#         Raises:
#           TypeError: if direct_input_size is not None for any access module other
#             than KeyValueMemory.
#         """
#         super(DNC, self).__init__(name=name)
#
#         with self._enter_variable_scope():
#             self._controller = snt.LSTM(**controller_config)
#             self._access = MemoryAccess(**access_config)
#
#         self._access_output_size = np.prod(self._access.output_size.as_list())
#         self._output_size = output_size
#         self._clip_value = clip_value or 0
#
#         self._output_size = tf.TensorShape([output_size])
#         self._state_size = DNCState(
#             access_output=self._access_output_size,
#             access_state=self._access.state_size,
#             controller_state=self._controller.state_size)
#
#     def _clip_if_enabled(self, x):
#         if self._clip_value > 0:
#             return tf.clip_by_value(x, -self._clip_value, self._clip_value)
#         else:
#             return x
#
#     def _build(self, inputs, prev_state):
#         """Connects the DNC core into the graph.
#         Args:
#           inputs: Tensor input.
#           prev_state: A `DNCState` tuple containing the fields `access_output`,
#               `access_state` and `controller_state`. `access_state` is a 3-D Tensor
#               of shape `[batch_size, num_reads, word_size]` containing read words.
#               `access_state` is a tuple of the access module's state, and
#               `controller_state` is a tuple of controller module's state.
#         Returns:
#           A tuple `(output, next_state)` where `output` is a tensor and `next_state`
#           is a `DNCState` tuple containing the fields `access_output`,
#           `access_state`, and `controller_state`.
#         """
#
#         prev_access_output = prev_state.access_output
#         prev_access_state = prev_state.access_state
#         prev_controller_state = prev_state.controller_state
#
#         batch_flatten = snt.BatchFlatten()
#         controller_input = tf.concat(
#             [batch_flatten(inputs), batch_flatten(prev_access_output)], 1)
#
#         controller_output, controller_state = self._controller(
#             controller_input, prev_controller_state)
#
#         controller_output = self._clip_if_enabled(controller_output)
#         controller_state = tf.contrib.framework.nest.map_structure(self._clip_if_enabled, controller_state)
#
#         access_output, access_state = self._access(controller_output,
#                                                    prev_access_state)
#
#         output = tf.concat([controller_output, batch_flatten(access_output)], 1)
#         output = snt.Linear(
#             output_size=self._output_size.as_list()[0],
#             name='output_linear')(output)
#         output = self._clip_if_enabled(output)
#
#         return output, DNCState(
#             access_output=access_output,
#             access_state=access_state,
#             controller_state=controller_state)
#
#     def initial_state(self, batch_size, dtype=tf.float32, trainable=False,
#                       trainable_initializers=None, trainable_regularizers=None,
#                       name=None, **unused_kwargs):
#         return DNCState(
#             controller_state=self._controller.initial_state(batch_size, dtype),
#             access_state=self._access.initial_state(batch_size, dtype),
#             access_output=tf.zeros(
#                 [batch_size] + self._access.output_size.as_list(), dtype))
#
#     @property
#     def state_size(self):
#         return self._state_size
#
#     @property
#     def output_size(self):
#         return self._output_size
#
#
# def batch_invert_permutation(permutations):
#     """Returns batched `tf.invert_permutation` for every row in `permutations`."""
#     with tf.name_scope('batch_invert_permutation', values=[permutations]):
#         perm = tf.cast(permutations, tf.float32)
#         dim = int(perm.get_shape()[-1])
#         size = tf.cast(tf.shape(perm)[0], tf.float32)
#         delta = tf.cast(tf.shape(perm)[-1], tf.float32)
#         rg = tf.range(0, size * delta, delta, dtype=tf.float32)
#         rg = tf.expand_dims(rg, 1)
#         rg = tf.tile(rg, [1, dim])
#         perm = tf.add(perm, rg)
#         flat = tf.reshape(perm, [-1])
#         perm = tf.invert_permutation(tf.cast(flat, tf.int32))
#         perm = tf.reshape(perm, [-1, dim])
#         return tf.subtract(perm, tf.cast(rg, tf.int32))
#
#
# def batch_gather(values, indices):
#     """Returns batched `tf.gather` for every row in the input."""
#     with tf.name_scope('batch_gather', values=[values, indices]):
#         idx = tf.expand_dims(indices, -1)
#         size = tf.shape(indices)[0]
#         rg = tf.range(size, dtype=tf.int32)
#         rg = tf.expand_dims(rg, -1)
#         rg = tf.tile(rg, [1, int(indices.get_shape()[-1])])
#         rg = tf.expand_dims(rg, -1)
#         gidx = tf.concat([rg, idx], -1)
#         return tf.gather_nd(values, gidx)
#
#
# def one_hot(length, index):
#     """Return an nd array of given `length` filled with 0s and a 1 at `index`."""
#     result = np.zeros(length)
#     result[index] = 1
#     return result
#
#
# def reduce_prod(x, axis, name=None):
#     """Efficient reduce product over axis.
#     Uses tf.cumprod and tf.gather_nd as a workaround to the poor performance of calculating tf.reduce_prod's gradient on CPU.
#     """
#     with tf.name_scope(name, 'util_reduce_prod', values=[x]):
#         cp = tf.cumprod(x, axis, reverse=True)
#         size = tf.shape(cp)[0]
#         idx1 = tf.range(tf.cast(size, tf.float32), dtype=tf.float32)
#         idx2 = tf.zeros([size], tf.float32)
#         indices = tf.stack([idx1, idx2], 1)
#         return tf.gather_nd(cp, tf.cast(indices, tf.int32))
#
#
# def _vector_norms(m):
#     squared_norms = tf.reduce_sum(m * m, axis=2, keepdims=True)
#     return tf.sqrt(squared_norms + _EPSILON)
#
#
# def weighted_softmax(activations, strengths, strengths_op):
#     """Returns softmax over activations multiplied by positive strengths.
#     Args:
#       activations: A tensor of shape `[batch_size, num_heads, memory_size]`, of
#         activations to be transformed. Softmax is taken over the last dimension.
#       strengths: A tensor of shape `[batch_size, num_heads]` containing strengths to
#         multiply by the activations prior to the softmax.
#       strengths_op: An operation to transform strengths before softmax.
#     Returns:
#       A tensor of same shape as `activations` with weighted softmax applied.
#     """
#     transformed_strengths = tf.expand_dims(strengths_op(strengths), -1)
#     sharp_activations = activations * transformed_strengths
#     softmax = snt.BatchApply(module_or_op=tf.nn.softmax)
#     return softmax(sharp_activations)
#
#
# class CosineWeights(snt.AbstractModule):
#     """Cosine-weighted attention.
#     Calculates the cosine similarity between a query and each word in memory, then
#     applies a weighted softmax to return a sharp distribution.
#     """
#
#     def __init__(self,
#                  num_heads,
#                  word_size,
#                  strength_op=tf.nn.softplus,
#                  name='cosine_weights'):
#         """Initializes the CosineWeights module.
#         Args:
#           num_heads: number of memory heads.
#           word_size: memory word size.
#           strength_op: operation to apply to strengths (default is tf.nn.softplus).
#           name: module name (default 'cosine_weights')
#         """
#         super(CosineWeights, self).__init__(name=name)
#         self._num_heads = num_heads
#         self._word_size = word_size
#         self._strength_op = strength_op
#
#     def _build(self, memory, keys, strengths):
#         """Connects the CosineWeights module into the graph.
#         Args:
#           memory: A 3-D tensor of shape `[batch_size, memory_size, word_size]`.
#           keys: A 3-D tensor of shape `[batch_size, num_heads, word_size]`.
#           strengths: A 2-D tensor of shape `[batch_size, num_heads]`.
#         Returns:
#           Weights tensor of shape `[batch_size, num_heads, memory_size]`.
#         """
#         # Calculates the inner product between the query vector and words in memory.
#         dot = tf.matmul(keys, memory, adjoint_b=True)
#
#         # Outer product to compute denominator (euclidean norm of query and memory).
#         memory_norms = _vector_norms(memory)
#         key_norms = _vector_norms(keys)
#         norm = tf.matmul(key_norms, memory_norms, adjoint_b=True)
#
#         # Calculates cosine similarity between the query vector and words in memory.
#         similarity = dot / (norm + _EPSILON)
#
#         return weighted_softmax(similarity, strengths, self._strength_op)
#
#
# class TemporalLinkage(snt.RNNCore):
#     """Keeps track of write order for forward and backward addressing.
#     This is a pseudo-RNNCore module, whose state is a pair `(link,
#     precedence_weights)`, where `link` is a (collection of) graphs for (possibly
#     multiple) write heads (represented by a tensor with values in the range
#     [0, 1]), and `precedence_weights` records the "previous write locations" used
#     to build the link graphs.
#     The function `directional_read_weights` computes addresses following the
#     forward and backward directions in the link graphs.
#     """
#
#     def __init__(self, memory_size, num_writes, name='temporal_linkage'):
#         """Construct a TemporalLinkage module.
#         Args:
#           memory_size: The number of memory slots.
#           num_writes: The number of write heads.
#           name: Name of the module.
#         """
#         super(TemporalLinkage, self).__init__(name=name)
#         self._memory_size = memory_size
#         self._num_writes = num_writes
#
#     def _build(self, write_weights, prev_state):
#         """Calculate the updated linkage state given the write weights.
#         Args:
#           write_weights: A tensor of shape `[batch_size, num_writes, memory_size]`
#               containing the memory addresses of the different write heads.
#           prev_state: `TemporalLinkageState` tuple containg a tensor `link` of
#               shape `[batch_size, num_writes, memory_size, memory_size]`, and a
#               tensor `precedence_weights` of shape `[batch_size, num_writes,
#               memory_size]` containing the aggregated history of recent writes.
#         Returns:
#           A `TemporalLinkageState` tuple `next_state`, which contains the updated
#           link and precedence weights.
#         """
#         link = self._link(prev_state.link, prev_state.precedence_weights,
#                           write_weights)
#         precedence_weights = self._precedence_weights(prev_state.precedence_weights,
#                                                       write_weights)
#         return TemporalLinkageState(
#             link=link, precedence_weights=precedence_weights)
#
#     def directional_read_weights(self, link, prev_read_weights, forward):
#         """Calculates the forward or the backward read weights.
#         For each read head (at a given address), there are `num_writes` link graphs
#         to follow. Thus this function computes a read address for each of the
#         `num_reads * num_writes` pairs of read and write heads.
#         Args:
#           link: tensor of shape `[batch_size, num_writes, memory_size,
#               memory_size]` representing the link graphs L_t.
#           prev_read_weights: tensor of shape `[batch_size, num_reads,
#               memory_size]` containing the previous read weights w_{t-1}^r.
#           forward: Boolean indicating whether to follow the "future" direction in
#               the link graph (True) or the "past" direction (False).
#         Returns:
#           tensor of shape `[batch_size, num_reads, num_writes, memory_size]`
#         """
#         with tf.name_scope('directional_read_weights'):
#             # We calculate the forward and backward directions for each pair of
#             # read and write heads; hence we need to tile the read weights and do a
#             # sort of "outer product" to get this.
#             expanded_read_weights = tf.stack([prev_read_weights] * self._num_writes,
#                                              1)
#             result = tf.matmul(expanded_read_weights, link, adjoint_b=forward)
#             # Swap dimensions 1, 2 so order is [batch, reads, writes, memory]:
#             return tf.transpose(result, perm=[0, 2, 1, 3])
#
#     def _link(self, prev_link, prev_precedence_weights, write_weights):
#         """Calculates the new link graphs.
#         For each write head, the link is a directed graph (represented by a matrix
#         with entries in range [0, 1]) whose vertices are the memory locations, and
#         an edge indicates temporal ordering of writes.
#         Args:
#           prev_link: A tensor of shape `[batch_size, num_writes, memory_size,
#               memory_size]` representing the previous link graphs for each write
#               head.
#           prev_precedence_weights: A tensor of shape `[batch_size, num_writes,
#               memory_size]` which is the previous "aggregated" write weights for
#               each write head.
#           write_weights: A tensor of shape `[batch_size, num_writes, memory_size]`
#               containing the new locations in memory written to.
#         Returns:
#           A tensor of shape `[batch_size, num_writes, memory_size, memory_size]`
#           containing the new link graphs for each write head.
#         """
#         with tf.name_scope('link'):
#             batch_size = tf.shape(prev_link)[0]
#             write_weights_i = tf.expand_dims(write_weights, 3)
#             write_weights_j = tf.expand_dims(write_weights, 2)
#             prev_precedence_weights_j = tf.expand_dims(prev_precedence_weights, 2)
#             prev_link_scale = 1 - write_weights_i - write_weights_j
#             new_link = write_weights_i * prev_precedence_weights_j
#             link = prev_link_scale * prev_link + new_link
#             # Return the link with the diagonal set to zero, to remove self-looping
#             # edges.
#             return tf.matrix_set_diag(
#                 link,
#                 tf.zeros(
#                     [batch_size, self._num_writes, self._memory_size],
#                     dtype=link.dtype))
#
#     def _precedence_weights(self, prev_precedence_weights, write_weights):
#         """Calculates the new precedence weights given the current write weights.
#         The precedence weights are the "aggregated write weights" for each write
#         head, where write weights with sum close to zero will leave the precedence
#         weights unchanged, but with sum close to one will replace the precedence
#         weights.
#         Args:
#           prev_precedence_weights: A tensor of shape `[batch_size, num_writes,
#               memory_size]` containing the previous precedence weights.
#           write_weights: A tensor of shape `[batch_size, num_writes, memory_size]`
#               containing the new write weights.
#         Returns:
#           A tensor of shape `[batch_size, num_writes, memory_size]` containing the
#           new precedence weights.
#         """
#         with tf.name_scope('precedence_weights'):
#             write_sum = tf.reduce_sum(write_weights, 2, keepdims=True)
#             return (1 - write_sum) * prev_precedence_weights + write_weights
#
#     @property
#     def state_size(self):
#         """Returns a `TemporalLinkageState` tuple of the state tensors' shapes."""
#         return TemporalLinkageState(
#             link=tf.TensorShape(
#                 [self._num_writes, self._memory_size, self._memory_size]),
#             precedence_weights=tf.TensorShape([self._num_writes,
#                                                self._memory_size]), )
#
#
# class Freeness(snt.RNNCore):
#     """Memory usage that is increased by writing and decreased by reading.
#     This module is a pseudo-RNNCore whose state is a tensor with values in
#     the range [0, 1] indicating the usage of each of `memory_size` memory slots.
#     The usage is:
#     *   Increased by writing, where usage is increased towards 1 at the write
#         addresses.
#     *   Decreased by reading, where usage is decreased after reading from a
#         location when free_gate is close to 1.
#     The function `write_allocation_weights` can be invoked to get free locations
#     to write to for a number of write heads.
#     """
#
#     def __init__(self, memory_size, name='freeness'):
#         """Creates a Freeness module.
#         Args:
#           memory_size: Number of memory slots.
#           name: Name of the module.
#         """
#         super(Freeness, self).__init__(name=name)
#         self._memory_size = memory_size
#
#     def _build(self, write_weights, free_gate, read_weights, prev_usage):
#         """Calculates the new memory usage u_t.
#         Memory that was written to in the previous time step will have its usage
#         increased; memory that was read from and the controller says can be "freed"
#         will have its usage decreased.
#         Args:
#           write_weights: tensor of shape `[batch_size, num_writes,
#               memory_size]` giving write weights at previous time step.
#           free_gate: tensor of shape `[batch_size, num_reads]` which indicates
#               which read heads read memory that can now be freed.
#           read_weights: tensor of shape `[batch_size, num_reads,
#               memory_size]` giving read weights at previous time step.
#           prev_usage: tensor of shape `[batch_size, memory_size]` giving
#               usage u_{t - 1} at the previous time step, with entries in range
#               [0, 1].
#         Returns:
#           tensor of shape `[batch_size, memory_size]` representing updated memory
#           usage.
#         """
#         # Calculation of usage is not differentiable with respect to write weights.
#         write_weights = tf.stop_gradient(write_weights)
#         usage = self._usage_after_write(prev_usage, write_weights)
#         usage = self._usage_after_read(usage, free_gate, read_weights)
#         return usage
#
#     def write_allocation_weights(self, usage, write_gates, num_writes):
#         """Calculates freeness-based locations for writing to.
#         This finds unused memory by ranking the memory locations by usage, for each
#         write head. (For more than one write head, we use a "simulated new usage"
#         which takes into account the fact that the previous write head will increase
#         the usage in that area of the memory.)
#         Args:
#           usage: A tensor of shape `[batch_size, memory_size]` representing
#               current memory usage.
#           write_gates: A tensor of shape `[batch_size, num_writes]` with values in
#               the range [0, 1] indicating how much each write head does writing
#               based on the address returned here (and hence how much usage
#               increases).
#           num_writes: The number of write heads to calculate write weights for.
#         Returns:
#           tensor of shape `[batch_size, num_writes, memory_size]` containing the
#               freeness-based write locations. Note that this isn't scaled by
#               `write_gate`; this scaling must be applied externally.
#         """
#         with tf.name_scope('write_allocation_weights'):
#             # expand gatings over memory locations
#             write_gates = tf.expand_dims(write_gates, -1)
#
#             allocation_weights = []
#             for i in range(num_writes):
#                 allocation_weights.append(self._allocation(usage))
#                 # update usage to take into account writing to this new allocation
#                 usage += ((1 - usage) * write_gates[:, i, :] * allocation_weights[i])
#
#             # Pack the allocation weights for the write heads into one tensor.
#             return tf.stack(allocation_weights, axis=1)
#
#     def _usage_after_write(self, prev_usage, write_weights):
#         """Calcualtes the new usage after writing to memory.
#         Args:
#           prev_usage: tensor of shape `[batch_size, memory_size]`.
#           write_weights: tensor of shape `[batch_size, num_writes, memory_size]`.
#         Returns:
#           New usage, a tensor of shape `[batch_size, memory_size]`.
#         """
#         with tf.name_scope('usage_after_write'):
#             # Calculate the aggregated effect of all write heads
#             write_weights = 1 - reduce_prod(1 - write_weights, 1)
#             return prev_usage + (1 - prev_usage) * write_weights
#
#     def _usage_after_read(self, prev_usage, free_gate, read_weights):
#         """Calcualtes the new usage after reading and freeing from memory.
#         Args:
#           prev_usage: tensor of shape `[batch_size, memory_size]`.
#           free_gate: tensor of shape `[batch_size, num_reads]` with entries in the
#               range [0, 1] indicating the amount that locations read from can be
#               freed.
#           read_weights: tensor of shape `[batch_size, num_reads, memory_size]`.
#         Returns:
#           New usage, a tensor of shape `[batch_size, memory_size]`.
#         """
#         with tf.name_scope('usage_after_read'):
#             free_gate = tf.expand_dims(free_gate, -1)
#             free_read_weights = free_gate * read_weights
#             phi = reduce_prod(1 - free_read_weights, 1, name='phi')
#             return prev_usage * phi
#
#     def _allocation(self, usage):
#         r"""Computes allocation by sorting `usage`.
#         This corresponds to the value a = a_t[\phi_t[j]] in the paper.
#         Args:
#           usage: tensor of shape `[batch_size, memory_size]` indicating current
#               memory usage. This is equal to u_t in the paper when we only have one
#               write head, but for multiple write heads, one should update the usage
#               while iterating through the write heads to take into account the
#               allocation returned by this function.
#         Returns:
#           Tensor of shape `[batch_size, memory_size]` corresponding to allocation.
#         """
#         with tf.name_scope('allocation'):
#             # Ensure values are not too small prior to cumprod.
#             usage = _EPSILON + (1 - _EPSILON) * usage
#
#             nonusage = 1 - usage
#             sorted_nonusage, indices = tf.nn.top_k(
#                 nonusage, k=self._memory_size, name='sort')
#             sorted_usage = 1 - sorted_nonusage
#             prod_sorted_usage = tf.cumprod(sorted_usage, axis=1, exclusive=True)
#             sorted_allocation = sorted_nonusage * prod_sorted_usage
#             inverse_indices = batch_invert_permutation(indices)
#
#             # This final line "unsorts" sorted_allocation, so that the indexing
#             # corresponds to the original indexing of `usage`.
#             return batch_gather(sorted_allocation, inverse_indices)
#
#     @property
#     def state_size(self):
#         """Returns the shape of the state tensor."""
#         return tf.TensorShape([self._memory_size])
#
#
# def _erase_and_write(memory, address, reset_weights, values):
#     """Module to erase and write in the external memory.
#     Erase operation:
#       M_t'(i) = M_{t-1}(i) * (1 - w_t(i) * e_t)
#     Add operation:
#       M_t(i) = M_t'(i) + w_t(i) * a_t
#     where e are the reset_weights, w the write weights and a the values.
#     Args:
#       memory: 3-D tensor of shape `[batch_size, memory_size, word_size]`.
#       address: 3-D tensor `[batch_size, num_writes, memory_size]`.
#       reset_weights: 3-D tensor `[batch_size, num_writes, word_size]`.
#       values: 3-D tensor `[batch_size, num_writes, word_size]`.
#     Returns:
#       3-D tensor of shape `[batch_size, num_writes, word_size]`.
#     """
#     with tf.name_scope('erase_memory', values=[memory, address, reset_weights]):
#         expand_address = tf.expand_dims(address, 3)
#         reset_weights = tf.expand_dims(reset_weights, 2)
#         weighted_resets = expand_address * reset_weights
#         reset_gate = reduce_prod(1 - weighted_resets, 1)
#         memory *= reset_gate
#
#     with tf.name_scope('additive_write', values=[memory, address, values]):
#         add_matrix = tf.matmul(address, values, adjoint_a=True)
#         memory += add_matrix
#
#     return memory
#
#
# class MemoryAccess(snt.RNNCore):
#     """Access module of the Differentiable Neural Computer.
#     This memory module supports multiple read and write heads. It makes use of:
#     *   `addressing.TemporalLinkage` to track the temporal ordering of writes in
#         memory for each write head.
#     *   `addressing.FreenessAllocator` for keeping track of memory usage, where
#         usage increase when a memory location is written to, and decreases when
#         memory is read from that the controller says can be freed.
#     Write-address selection is done by an interpolation between content-based
#     lookup and using unused memory.
#     Read-address selection is done by an interpolation of content-based lookup
#     and following the link graph in the forward or backwards read direction.
#     """
#
#     def __init__(self,
#                  memory_size=128,
#                  word_size=20,
#                  num_reads=1,
#                  num_writes=1,
#                  name='memory_access'):
#         """Creates a MemoryAccess module.
#         Args:
#           memory_size: The number of memory slots (N in the DNC paper).
#           word_size: The width of each memory slot (W in the DNC paper)
#           num_reads: The number of read heads (R in the DNC paper).
#           num_writes: The number of write heads (fixed at 1 in the paper).
#           name: The name of the module.
#         """
#         super(MemoryAccess, self).__init__(name=name)
#         self._memory_size = memory_size
#         self._word_size = word_size
#         self._num_reads = num_reads
#         self._num_writes = num_writes
#
#         self._write_content_weights_mod = CosineWeights(
#             num_writes, word_size, name='write_content_weights')
#         self._read_content_weights_mod = CosineWeights(
#             num_reads, word_size, name='read_content_weights')
#
#         self._linkage = TemporalLinkage(memory_size, num_writes)
#         self._freeness = Freeness(memory_size)
#
#     def _build(self, inputs, prev_state):
#         """Connects the MemoryAccess module into the graph.
#         Args:
#           inputs: tensor of shape `[batch_size, input_size]`. This is used to
#               control this access module.
#           prev_state: Instance of `AccessState` containing the previous state.
#         Returns:
#           A tuple `(output, next_state)`, where `output` is a tensor of shape
#           `[batch_size, num_reads, word_size]`, and `next_state` is the new
#           `AccessState` named tuple at the current time t.
#         """
#         inputs = self._read_inputs(inputs)
#
#         # Update usage using inputs['free_gate'] and previous read & write weights.
#         usage = self._freeness(
#             write_weights=prev_state.write_weights,
#             free_gate=inputs['free_gate'],
#             read_weights=prev_state.read_weights,
#             prev_usage=prev_state.usage)
#
#         # Write to memory.
#         write_weights = self._write_weights(inputs, prev_state.memory, usage)
#         memory = _erase_and_write(
#             prev_state.memory,
#             address=write_weights,
#             reset_weights=inputs['erase_vectors'],
#             values=inputs['write_vectors'])
#
#         linkage_state = self._linkage(write_weights, prev_state.linkage)
#
#         # Read from memory.
#         read_weights = self._read_weights(
#             inputs,
#             memory=memory,
#             prev_read_weights=prev_state.read_weights,
#             link=linkage_state.link)
#         read_words = tf.matmul(read_weights, memory)
#
#         return (read_words, AccessState(
#             memory=memory,
#             read_weights=read_weights,
#             write_weights=write_weights,
#             linkage=linkage_state,
#             usage=usage))
#
#     def _read_inputs(self, inputs):
#         """Applies transformations to `inputs` to get control for this module."""
#
#         def _linear(first_dim, second_dim, name, activation=None):
#             """Returns a linear transformation of `inputs`, followed by a reshape."""
#             linear = snt.Linear(first_dim * second_dim, name=name)(inputs)
#             if activation is not None:
#                 linear = activation(linear, name=name + '_activation')
#             return tf.reshape(linear, [-1, first_dim, second_dim])
#
#         # v_t^i - The vectors to write to memory, for each write head `i`.
#         write_vectors = _linear(self._num_writes, self._word_size, 'write_vectors')
#
#         # e_t^i - Amount to erase the memory by before writing, for each write head.
#         erase_vectors = _linear(self._num_writes, self._word_size, 'erase_vectors',
#                                 tf.sigmoid)
#
#         # f_t^j - Amount that the memory at the locations read from at the previous
#         # time step can be declared unused, for each read head `j`.
#         free_gate = tf.sigmoid(
#             snt.Linear(self._num_reads, name='free_gate')(inputs))
#
#         # g_t^{a, i} - Interpolation between writing to unallocated memory and
#         # content-based lookup, for each write head `i`. Note: `a` is simply used to
#         # identify this gate with allocation vs writing (as defined below).
#         allocation_gate = tf.sigmoid(
#             snt.Linear(self._num_writes, name='allocation_gate')(inputs))
#
#         # g_t^{w, i} - Overall gating of write amount for each write head.
#         write_gate = tf.sigmoid(
#             snt.Linear(self._num_writes, name='write_gate')(inputs))
#
#         # \pi_t^j - Mixing between "backwards" and "forwards" positions (for
#         # each write head), and content-based lookup, for each read head.
#         num_read_modes = 1 + 2 * self._num_writes
#         read_mode = snt.BatchApply(tf.nn.softmax)(
#             _linear(self._num_reads, num_read_modes, name='read_mode'))
#
#         # Parameters for the (read / write) "weights by content matching" modules.
#         write_keys = _linear(self._num_writes, self._word_size, 'write_keys')
#         write_strengths = snt.Linear(self._num_writes, name='write_strengths')(
#             inputs)
#
#         read_keys = _linear(self._num_reads, self._word_size, 'read_keys')
#         read_strengths = snt.Linear(self._num_reads, name='read_strengths')(inputs)
#
#         result = {
#             'read_content_keys': read_keys,
#             'read_content_strengths': read_strengths,
#             'write_content_keys': write_keys,
#             'write_content_strengths': write_strengths,
#             'write_vectors': write_vectors,
#             'erase_vectors': erase_vectors,
#             'free_gate': free_gate,
#             'allocation_gate': allocation_gate,
#             'write_gate': write_gate,
#             'read_mode': read_mode,
#         }
#         return result
#
#     def _write_weights(self, inputs, memory, usage):
#         """Calculates the memory locations to write to.
#         This uses a combination of content-based lookup and finding an unused
#         location in memory, for each write head.
#         Args:
#           inputs: Collection of inputs to the access module, including controls for
#               how to chose memory writing, such as the content to look-up and the
#               weighting between content-based and allocation-based addressing.
#           memory: A tensor of shape  `[batch_size, memory_size, word_size]`
#               containing the current memory contents.
#           usage: Current memory usage, which is a tensor of shape `[batch_size,
#               memory_size]`, used for allocation-based addressing.
#         Returns:
#           tensor of shape `[batch_size, num_writes, memory_size]` indicating where
#               to write to (if anywhere) for each write head.
#         """
#         with tf.name_scope('write_weights', values=[inputs, memory, usage]):
#             # c_t^{w, i} - The content-based weights for each write head.
#             write_content_weights = self._write_content_weights_mod(
#                 memory, inputs['write_content_keys'],
#                 inputs['write_content_strengths'])
#
#             # a_t^i - The allocation weights for each write head.
#             write_allocation_weights = self._freeness.write_allocation_weights(
#                 usage=usage,
#                 write_gates=(inputs['allocation_gate'] * inputs['write_gate']),
#                 num_writes=self._num_writes)
#
#             # Expands gates over memory locations.
#             allocation_gate = tf.expand_dims(inputs['allocation_gate'], -1)
#             write_gate = tf.expand_dims(inputs['write_gate'], -1)
#
#             # w_t^{w, i} - The write weightings for each write head.
#             return write_gate * (allocation_gate * write_allocation_weights +
#                                  (1 - allocation_gate) * write_content_weights)
#
#     def _read_weights(self, inputs, memory, prev_read_weights, link):
#         """Calculates read weights for each read head.
#         The read weights are a combination of following the link graphs in the
#         forward or backward directions from the previous read position, and doing
#         content-based lookup. The interpolation between these different modes is
#         done by `inputs['read_mode']`.
#         Args:
#           inputs: Controls for this access module. This contains the content-based
#               keys to lookup, and the weightings for the different read modes.
#           memory: A tensor of shape `[batch_size, memory_size, word_size]`
#               containing the current memory contents to do content-based lookup.
#           prev_read_weights: A tensor of shape `[batch_size, num_reads,
#               memory_size]` containing the previous read locations.
#           link: A tensor of shape `[batch_size, num_writes, memory_size,
#               memory_size]` containing the temporal write transition graphs.
#         Returns:
#           A tensor of shape `[batch_size, num_reads, memory_size]` containing the
#           read weights for each read head.
#         """
#         with tf.name_scope(
#                 'read_weights', values=[inputs, memory, prev_read_weights, link]):
#             # c_t^{r, i} - The content weightings for each read head.
#             content_weights = self._read_content_weights_mod(
#                 memory, inputs['read_content_keys'], inputs['read_content_strengths'])
#
#             # Calculates f_t^i and b_t^i.
#             forward_weights = self._linkage.directional_read_weights(
#                 link, prev_read_weights, forward=True)
#             backward_weights = self._linkage.directional_read_weights(
#                 link, prev_read_weights, forward=False)
#
#             backward_mode = inputs['read_mode'][:, :, :self._num_writes]
#             forward_mode = (
#                 inputs['read_mode'][:, :, self._num_writes:2 * self._num_writes])
#             content_mode = inputs['read_mode'][:, :, 2 * self._num_writes]
#
#             read_weights = (
#                     tf.expand_dims(content_mode, 2) * content_weights + tf.reduce_sum(
#                 tf.expand_dims(forward_mode, 3) * forward_weights, 2) +
#                     tf.reduce_sum(tf.expand_dims(backward_mode, 3) * backward_weights, 2))
#
#             return read_weights
#
#     @property
#     def state_size(self):
#         """Returns a tuple of the shape of the state tensors."""
#         return AccessState(
#             memory=tf.TensorShape([self._memory_size, self._word_size]),
#             read_weights=tf.TensorShape([self._num_reads, self._memory_size]),
#             write_weights=tf.TensorShape([self._num_writes, self._memory_size]),
#             linkage=self._linkage.state_size,
#             usage=self._freeness.state_size)
#
#     @property
#     def output_size(self):
#         """Returns the output shape."""
#         return tf.TensorShape([self._num_reads, self._word_size])
