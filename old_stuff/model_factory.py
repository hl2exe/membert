"""

@Author: Federico Ruggeri

@Date: 29/11/18

"""

from nn_models import VanillaLSTM, BiGRU, CBiGRU, MemN2N_tf, EMemN2N_tf, RRN_Network, Bow_EMemN2N_tf, Word_EMemn2n_tf
from utility.log_utils import get_logger

logger = get_logger(__name__)


class ModelFactory(object):

    supported_models = {
        'vanilla_lstm': VanillaLSTM,
        'bi_gru': BiGRU,
        'cbi_gru': CBiGRU,
        'memn2n_tf': MemN2N_tf,
        'window_memn2n_tf': MemN2N_tf,
        'unfair_memn2n_tf': MemN2N_tf,
        'unfair_ememn2n_tf': EMemN2N_tf,
        'bow_unfair_ememn2n_tf': Bow_EMemN2N_tf,
        'recurrent_relation_network': RRN_Network,
        'word_ememn2n_tf': Word_EMemn2n_tf
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ModelFactory.supported_models[key]:
            return ModelFactory.supported_models[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
