"""

@Authors: Federico Ruggeri

@Date: 26/09/2019

Wrappers for nested multiprocessing

"""

import multiprocessing as mp
import os
from functools import reduce
from itertools import product

import numpy as np

import const_define as cd
from old_stuff.converter import ConverterFactory
from utility.cross_validation_utils import build_metrics
from utility.cross_validation_utils import mp_update_cv_info, compute_iteration_validation_error, show_data_shapes
from utility.log_utils import get_logger
from utility.python_utils import merge

logger = get_logger(__name__)


def _cross_validation_routine(validation_percentage, data_handle, converter, repetition_id,
                              fold_idx, test_indexes, n_splits, repetitions, callbacks_data,
                              model_type, network_args, training_config,
                              dataset_list, error_metrics, error_metrics_additional_info=None,
                              compute_test_info=True, save_predictions=False,
                              use_tensorboard=False, tensorboard_base_dir=None,
                              save_model=False, save_path=None,
                              validation_info=None, test_info=None, all_preds=None, tf_version=2):
    """
    For each fold:
        2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
        2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
        2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
        2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
        2E. Model definition: a network model (nn_models_v2.py) is built
        2F. Model training: the network is trained.
        2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets
    """

    from old_stuff.preprocessing import PreprocessingFactory
    from old_stuff.splitter import SplitterFactory
    if tf_version == 2:
        from old_stuff.nn_models_v2 import ModelFactory
        from custom_callbacks_v2 import TensorBoard, EarlyStopping
    else:
        from old_stuff.nn_models_v1 import ModelFactory
        from old_stuff.custom_callbacks_v1 import TensorBoard
        from keras.callbacks import EarlyStopping
        from keras import backend as K

    # Callbacks
    # TODO: automatize callbacks creation
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])
    callbacks = [
        early_stopper,
    ]

    logger.info('Starting Repetition {0}/{1} - Fold {2}/{3}'.format(repetition_id + 1,
                                                                    repetitions,
                                                                    fold_idx + 1,
                                                                    n_splits))

    test_docs = [name.split('.txt')[0] for idx, name in enumerate(dataset_list)
                 if idx in test_indexes]

    train_df, test_df = data_handle.get_split(docs=test_docs)

    # Pre-processing
    if model_type in cd.MODEL_CONFIG:
        preprocessor_type = cd.MODEL_CONFIG[model_type]['preprocessor']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        preprocessor_type = cd.ALGORITHM_CONFIG[archetype]['preprocessor']
    preprocessor_args = {key: value['value'] for key, value in network_args.items()
                         if 'preprocessing' in value['flags']}

    # I need to know which label to look after
    preprocessor_args['label'] = data_handle.label
    preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                **preprocessor_args)

    train_df, additional_data = preprocessor.parse(df=train_df,
                                                   additional_info=data_handle.get_additional_info(),
                                                   return_info=True)
    test_df = preprocessor.parse(df=test_df, additional_info=data_handle.get_additional_info())

    # Data conversion
    x_train, y_train, text_info, additional_data = converter.fit_train_data(train_df=train_df,
                                                                            additional_data=additional_data)

    if text_info is not None:
        for key in text_info:
            logger.info('{0} size: {1}'.format(key, text_info[key]))

    x_test, y_test = converter.convert_data(df=test_df, text_info=text_info, additional_data=additional_data)

    # Train/Validation split
    if model_type in cd.MODEL_CONFIG:
        splitter_type = cd.MODEL_CONFIG[model_type]['splitter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        splitter_type = cd.ALGORITHM_CONFIG[archetype]['splitter']

    splitter_args = {key: value['value'] for key, value in network_args.items()
                     if 'splitter' in value['flags']}
    splitter_args['validation_split'] = validation_percentage
    splitter = SplitterFactory.factory(cl_type=splitter_type,
                                       **splitter_args)

    x_train, \
    y_train, \
    x_val, \
    y_val, \
    text_info, \
    x_test, \
    y_test = splitter.split(x=x_train, y=y_train, x_test=x_test, y_test=y_test,
                            text_info=text_info,
                            return_text_info=True)

    show_data_shapes(x_train, 'Train')
    show_data_shapes(x_val, 'Validation')
    show_data_shapes(x_test, 'Test')

    network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                              if 'model_class' in value['flags']}
    network_retrieved_args['additional_data'] = additional_data
    network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
        cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition_id, fold_idx)
    network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

    # computing positive label weights (for unbalanced dataset)
    network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_begin'):
            callback.on_build_model_begin(logs={'network': network})

    network.build_model(text_info=text_info)

    # Custom callbacks only
    for callback in callbacks:
        if hasattr(callback, 'on_build_model_end'):
            callback.on_build_model_end(logs={'network': network})

    if use_tensorboard:
        fold_log_dir = os.path.join(tensorboard_base_dir,
                                    'repetition_{}'.format(repetition_id),
                                    'fold_{}'.format(fold_idx))
        os.makedirs(fold_log_dir)
        tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                  log_dir=fold_log_dir)
        fold_callbacks = callbacks + [tensorboard]
    else:
        fold_callbacks = callbacks

    # Training
    network.fit(x=x_train, y=y_train,
                callbacks=fold_callbacks,
                validation_data=(x_val, y_val),
                **training_config)

    # Inference
    val_predictions = network.predict(x=x_val,
                                      batch_size=training_config['batch_size'])

    iteration_validation_error = compute_iteration_validation_error(parsed_metrics=error_metrics,
                                                                    true_values=y_val,
                                                                    predicted_values=val_predictions,
                                                                    error_metrics_additional_info=error_metrics_additional_info)

    validation_info = mp_update_cv_info(info=validation_info,
                                        iteration_info=iteration_validation_error,
                                        fold_idx=fold_idx,
                                        repetition_id=repetition_id)

    logger.info('Iteration validation info: {}'.format(iteration_validation_error))

    test_predictions = network.predict(x=x_test,
                                       batch_size=training_config['batch_size'])

    iteration_test_error = compute_iteration_validation_error(parsed_metrics=error_metrics,
                                                              true_values=y_test,
                                                              predicted_values=test_predictions,
                                                              error_metrics_additional_info=error_metrics_additional_info)

    if compute_test_info:
        test_info = mp_update_cv_info(info=test_info,
                                      iteration_info=iteration_test_error,
                                      fold_idx=fold_idx,
                                      repetition_id=repetition_id)
        logger.info('Iteration test info: {}'.format(iteration_test_error))

        if save_predictions:
            all_preds = mp_update_cv_info(info=all_preds,
                                          iteration_info={'predictions': test_predictions.ravel()},
                                          fold_idx=fold_idx,
                                          repetition_id=repetition_id)

    # Save model
    if save_model:
        filepath = os.path.join(save_path,
                                '{0}_repetition_{1}_fold_{2}'.format(
                                    cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                    repetition_id,
                                    fold_idx))
        network.save(filepath=filepath)

    if tf_version == 1:
        # Flush
        K.clear_session()

    if compute_test_info:
        return validation_info, test_info, all_preds
    else:
        return validation_info


def cross_validation_mp(validation_percentage, data_handle, cv, callbacks_data,
                        model_type, network_args, training_config, dataset_list,
                        error_metrics, error_metrics_additional_info=None,
                        compute_test_info=True, save_predictions=False,
                        use_tensorboard=False, repetitions=1, save_model=False,
                        save_path=None, cv_jobs=1, tf_version=2):
    """
    [Repeated] Cross-validation multi-processing routine: each fold is handled by a distinct thread.

        1. [For each repetition]

        2. For each fold (separate thread):
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        logger.error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(save_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)
    else:
        tensorboard_base_dir = None

    # [Step 1]: load embedding model
    if model_type in cd.MODEL_CONFIG:
        converter_type = cd.MODEL_CONFIG[model_type]['converter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        converter_type = cd.ALGORITHM_CONFIG[archetype]['converter']
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}

    # I need to know which label to look after
    converter_args['label'] = data_handle.label
    converter = ConverterFactory.factory(cl_type=converter_type, **converter_args)
    converter.load_embedding_model()

    # Step 2: cross validation
    if cv_jobs == -1:
        cv_jobs = mp.cpu_count()

    folds_info = [(fold_idx, test_indexes) for fold_idx, (_, test_indexes) in enumerate(cv.split(None))]
    repetitions_info = range(repetitions)
    cv_info = product(repetitions_info, folds_info)

    cv_pool = mp.Pool(processes=cv_jobs)

    results = [cv_pool.apply_async(func=_cross_validation_routine,
                                   args=(validation_percentage,
                                         data_handle,
                                         converter,
                                         rep_id,
                                         fold_idx,
                                         test_indexes,
                                         cv.n_splits,
                                         repetitions,
                                         callbacks_data,
                                         model_type,
                                         network_args,
                                         training_config,
                                         dataset_list,
                                         parsed_metrics,
                                         error_metrics_additional_info,
                                         compute_test_info,
                                         save_predictions,
                                         use_tensorboard,
                                         tensorboard_base_dir,
                                         save_model,
                                         save_path,
                                         None,
                                         None,
                                         None,
                                         tf_version))
               for rep_id, (fold_idx, test_indexes) in cv_info]
    output = [p.get() for p in results]

    cv_pool.close()
    cv_pool.join()

    if compute_test_info:
        total_validation_info = reduce(lambda a, b: merge(a, b), [item[0] for item in output])
        total_test_info = reduce(lambda a, b: merge(a, b), [item[1] for item in output])
        total_preds = reduce(lambda a, b: merge(a, b), [item[2] for item in output])

        # Convert to standard format: nested lists
        total_test_info = {metric: {
            fold_idx: sorted([(rep_id, rep_preds) for rep_id, rep_preds in fold_item.items()], key=lambda x: x[0]) for
            fold_idx, fold_item in metric_item.items()} for metric, metric_item in total_test_info.items()}
        total_test_info = {
            metric: sorted(
                [(fold_idx, [rep_item[1] for rep_item in fold_item]) for fold_idx, fold_item in metric_item.items()],
                key=lambda x: x[0])
            for metric, metric_item in total_test_info.items()}
        total_test_info = {metric: [fold_item[1] for fold_item in metric_item] for metric, metric_item in
                           total_test_info.items()}

        total_validation_info = {metric: {
            fold_idx: sorted([(rep_id, rep_preds) for rep_id, rep_preds in fold_item.items()], key=lambda x: x[0]) for
            fold_idx, fold_item in metric_item.items()} for metric, metric_item in total_validation_info.items()}
        total_validation_info = {
            metric: sorted(
                [(fold_idx, [rep_item[1] for rep_item in fold_item]) for fold_idx, fold_item in metric_item.items()],
                key=lambda x: x[0])
            for metric, metric_item in total_validation_info.items()}
        total_validation_info = {metric: [fold_item[1] for fold_item in metric_item] for metric, metric_item in
                                 total_validation_info.items()}

        total_preds = total_preds['predictions']
        total_preds = {
            fold_idx: sorted([(rep_id, rep_preds) for rep_id, rep_preds in fold_preds.items()],
                             key=lambda x: x[0])
            for fold_idx, fold_preds in total_preds.items()}
        total_preds = {fold_idx: [rep_item[1] for rep_item in fold_item] for fold_idx, fold_item in total_preds.items()}

        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

        if save_predictions:
            return {
                'validation_info': total_validation_info,
                'test_info': total_test_info,
                'predictions': total_preds
            }
        else:
            return {
                'validation_info': total_validation_info,
                'test_info': total_test_info,
            }
    else:
        total_validation_info = reduce(lambda a, b: merge(a, b), output)
        total_validation_info = {metric: [list(rep_item[fold_idx].values())
                                          for fold_idx in rep_item]
                                 for metric, rep_item in total_validation_info.items()}
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        return {'validation_info': total_validation_info}
