"""

@Author: Federico Ruggeri

#Date: 16/01/2019

"""

import os

import const_define as cd
import loader
from collections import OrderedDict
from utility.json_utils import load_json
import cleaner


# TODO: documentation
class Task1Loader(object):

    def load(self, data_base_path, labels_base_path, doc_format='txt'):
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        return DataHandle(input_data=x_data,
                          labels=y_data,
                          num_classes=1)


class Task1SingleLoader(object):

    def load(self, data_base_path, labels_base_path, category, doc_format='txt'):
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        category_base_path, _ = os.path.split(data_base_path)

        for doc in x_data:
            doc_key_labels = loader.load_data_labels(os.path.join(category_base_path, 'labels_{}'.format(category)),
                                              doc)
            y_data[doc] = doc_key_labels

        return DataHandle(input_data=x_data,
                          labels=y_data,
                          num_classes=1)


# TODO: documentation
class Task1KBLoader(object):

    def load(self, data_base_path, labels_base_path, kb_path, doc_format='txt'):
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        kb_data = loader.load_file_data(kb_path)

        return KBDataHandle(input_data=x_data,
                            labels=y_data,
                            num_classes=1,
                            kb_data=kb_data)


class Task1SingleKBLoader(object):

    def load(self, data_base_path, labels_base_path, category, doc_format='txt'):
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        category_base_path, _ = os.path.split(data_base_path)

        for doc in x_data:
            doc_key_labels = loader.load_data_labels(os.path.join(category_base_path, 'labels_{}'.format(category)),
                                              doc)
            y_data[doc] = doc_key_labels

        kb_path = os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category))
        kb_data = loader.load_file_data(kb_path)

        return KBDataHandle(input_data=x_data,
                            labels=y_data,
                            num_classes=1,
                            kb_data=kb_data)


class SupervisedTask1SingleLoader(object):

    def load(self, data_base_path, labels_base_path, category, doc_format='txt'):

        if not category.isupper():
            category = category.upper()

        # Load data
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        category_base_path, _ = os.path.split(data_base_path)

        for doc in x_data:
            doc_key_labels = loader.load_data_labels(os.path.join(category_base_path, 'labels_{}'.format(category)),
                                              doc)
            y_data[doc] = doc_key_labels

        # Load KB
        kb_path = os.path.join(cd.KB_DIR, '{}_KB.txt'.format(category))
        kb_data = loader.load_file_data(kb_path)

        # Load supervision targets
        targets_path = os.path.join(cd.LOCAL_DATASETS_DIR, cd.KB_DIR, '{}_targets.json'.format(category))
        supervision_targets = load_json(targets_path)
        supervision_targets = {cleaner.filter_line(key): value for key, value in supervision_targets.items()}

        return SupervisedKBDataHandle(input_data=x_data,
                                      labels=y_data,
                                      num_classes=1,
                                      kb_data=kb_data,
                                      supervision_targets=supervision_targets)


# TODO: documentation
class Task2Loader(object):
    category_vocab = [
        "arbitration",
        "unilateral change",
        "content removal",
        "jurisdiction",
        "choice of law",
        "limitation of liability",
        "unilateral termination",
        "contract by using"
    ]

    label_suffixes = [
        'A',
        'CH',
        'CR',
        'J',
        'LAW',
        'LTD',
        'TER',
        'USE'
    ]

    def load(self, data_base_path, labels_base_path, mode='all', ova_category=None, doc_format='txt'):

        # Step 1: load general data
        x_data, y_data = loader.load_all_data(data_base_path=data_base_path,
                                              labels_base_path=labels_base_path,
                                              doc_format=doc_format)

        for label_suffix, category in zip(Task2Loader.label_suffixes, Task2Loader.category_vocab):
            labels_base_path = os.path.join(cd.LOCAL_DATASETS_DIR, 'Labels_{}'.format(label_suffix))
            for doc in y_data:
                # setting the unfair label to the associated unfair category
                current_labels = loader.load_data_labels(base_path=labels_base_path, doc_name=doc)

                # Multilabeling
                temp = []
                for item, loaded in zip(y_data[doc], current_labels):
                    if loaded != 1:
                        temp.append(item)
                    else:
                        if type(item) == list:
                            item.append(category)
                            temp.append(item)
                        else:
                            temp.append([category])

                y_data[doc] = temp

        # Step 2: Selecting unfair clauses only
        for doc in x_data:
            x_data[doc] = [item for idx, item in enumerate(x_data[doc])
                           if type(y_data[doc][idx]) == list]
            y_data[doc] = [item for item in y_data[doc] if type(item) == list]

        # Step 3: Convert labels

        # All categories considered
        if mode == 'all':
            num_classes = len(Task2Loader.category_vocab)
            for doc in y_data:
                temp = []
                for item in y_data[doc]:
                    cat_value = [0] * num_classes
                    for term in item:
                        cat_value[Task2Loader.category_vocab.index(term)] = 1
                    temp.append(cat_value)

                y_data[doc] = temp

                # y_data[doc] = [category_vocab.index(item) for item in y_data[doc]]
        elif mode == 'ova':
            num_classes = 1
            for doc in y_data:
                y_data[doc] = [1 if ova_category in item else 0 for item in y_data[doc]]
        else:
            raise AttributeError('Invalid mode given! Got: {}'.format(mode))

        return DataHandle(input_data=x_data,
                          labels=y_data,
                          num_classes=num_classes)


# TODO: documentation
class DataHandle(object):

    def __init__(self, input_data, labels, num_classes):
        self.input_data = input_data
        self.labels = labels
        self.num_classes = num_classes

    def get_loo_split(self, test_doc):
        x_train = OrderedDict([(key, value) for key, value in self.input_data.items() if key != test_doc])
        y_train = OrderedDict([(key, value) for key, value in self.labels.items() if key != test_doc])

        x_test = OrderedDict([(test_doc, self.input_data[test_doc])])
        y_test = OrderedDict([(test_doc, self.labels[test_doc])])

        return x_train, y_train, x_test, y_test

    def get_split(self, docs):
        x_train = OrderedDict([(key, value) for key, value in self.input_data.items() if key not in docs])
        y_train = OrderedDict([(key, value) for key, value in self.labels.items() if key not in docs])

        x_test = OrderedDict([(key, value) for key, value in self.input_data.items() if key in docs])
        y_test = OrderedDict([(key, value) for key, value in self.labels.items() if key in docs])

        return x_train, y_train, x_test, y_test

    def get_additional_info(self):
        pass


# TODO: documentation
class KBDataHandle(DataHandle):

    def __init__(self, kb_data, **kwargs):
        super(KBDataHandle, self).__init__(**kwargs)
        self.kb_data = kb_data

    def get_additional_info(self):
        return self.kb_data


class SupervisedKBDataHandle(KBDataHandle):

    def __init__(self, supervision_targets, **kwargs):
        super(SupervisedKBDataHandle, self).__init__(**kwargs)
        self.supervision_targets = supervision_targets

    def get_additional_info(self):
        return {'kb': self.kb_data,
                'supervision': self.supervision_targets}
