"""

@Author: Federico Ruggeri

@Date: 26/09/2019

Multi-processing cross-validation routine.

** BEFORE RUNNING **

1. Check the following configuration files:
    distributed_config.json, callbacks.json, data_loader.json, model_config.json, training_config.json


"""

import os
from datetime import datetime

import numpy as np

import const_define as cd
import reporters
from data_loader import DataLoaderFactory
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.multiprocessing_utils import cross_validation_mp
from utility.python_utils import merge

logger = get_logger(__name__)

if __name__ == '__main__':

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))[cv_test_config['model_type']]

    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Callbacks
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)
    list_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.txt'.format(cv_test_config['prebuilt_folds']))
    dataset_list = cv.load_dataset_list(load_path=list_path)

    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    save_base_path = os.path.join(cd.CV_DIR, cv_test_config['model_type'], current_date)

    if cv_test_config['save_model'] and not os.path.isdir(save_base_path):
        os.makedirs(save_base_path)

    if not cv_test_config['save_model'] and cv_test_config['use_tensorboard']:
        cv_test_config['use_tensorboard'] = False
        logger.warning('Disabling tensorboard since save modle is disabled!')

    scores = cross_validation_mp(validation_percentage=cv_test_config['validation_percentage'],
                                 data_handle=data_handle,
                                 network_args=model_config,
                                 model_type=cv_test_config['model_type'],
                                 error_metrics=cv_test_config['error_metrics'],
                                 error_metrics_additional_info=cv_test_config['error_metrics_additional_info'],
                                 training_config=training_config,
                                 dataset_list=dataset_list,
                                 repetitions=cv_test_config['repetitions'],
                                 cv=cv,
                                 save_model=cv_test_config['save_model'],
                                 save_path=save_base_path,
                                 save_predictions=True,
                                 callbacks_data=callbacks_data,
                                 use_tensorboard=cv_test_config['use_tensorboard'],
                                 cv_jobs=cv_test_config['cv_jobs'],
                                 tf_version=2)

    # Validation
    if cv_test_config['repetitions'] > 1:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average validation scores: {}'.format(
            {key: np.mean(item) for key, item in scores['validation_info'].items() if not key.startswith('avg')}))
    save_json(os.path.join(save_base_path, cd.JSON_VALIDATION_INFO_NAME), scores['validation_info'])

    # Test
    if cv_test_config['repetitions'] > 1:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if key.startswith('avg')}))
    else:
        logger.info('Average test scores: {}'.format(
            {key: np.mean(item) for key, item in scores['test_info'].items() if not key.startswith('avg')}))
    save_json(os.path.join(save_base_path, cd.JSON_TEST_INFO_NAME), scores['test_info'])

    # Predictions
    save_json(os.path.join(save_base_path, cd.JSON_PREDICTIONS_NAME), scores['predictions'])

    # Test config
    reporters.save_model_info(folder=save_base_path, config_data=model_config)
    reporters.save_training_info(folder=save_base_path, training_info=training_config)
    save_json(os.path.join(save_base_path, cd.JSON_CV_TEST_CONFIG_NAME), data=cv_test_config)
    save_json(os.path.join(save_base_path, cd.JSON_CALLBACKS_NAME), data=callbacks_data)
    save_json(os.path.join(save_base_path, cd.JSON_DATA_LOADER_CONFIG_NAME), data=data_loader_config)
