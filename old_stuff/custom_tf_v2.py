"""

@Author: Federico Ruggeri

@Date: 24/07/19

Core part of Tensorflow 2 models. Each network defined in nn_models_v2.py has a corresponding model here.

TODO: fix DLU model.
TODO: support memory update

"""

import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_hub as hub
from tensorflow.python.keras.engine import base_layer_utils
from tensorflow.python.ops import rnn_cell_impl

from utility.python_utils import merge
from utility.tensorflow_utils_v2 import scaled_dot_product_attention, point_wise_feed_forward_network, \
    positional_encoding
import tensorflow_probability as tfp
from transformers.modeling_tf_distilbert import TFDistilBertForSequenceClassification

_zero_state_tensors = rnn_cell_impl._zero_state_tensors  # pylint: disable=protected-access


######################################################
####################### MODELS #######################
######################################################

class M_Basic_Memn2n(tf.keras.Model):
    """
    Basic Memory Network implementation. The model is comprised of three steps:
        1. Memory Lookup: a similarity operation is computed between query and memory cells.
         A content vector is extracted.

        2. Memory Reasoning: extracted content vector is used along with the query to build a new query.

        3. Lookup and Reasoning can be iterated multiple times.

        4. Eventually, an answer module takes care of formulating a prediction.
    """

    def __init__(self, query_size, sentence_size, embedding_info, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 vocab_size, embedding_dimension=32, hops=1,
                 answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, padding_amount=None,
                 accumulate_attention=False, position_encoding=False,
                 output_size=1, **kwargs):

        super(M_Basic_Memn2n, self).__init__(**kwargs)
        self.sentence_size = sentence_size
        self.query_size = query_size
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = position_encoding
        self.output_size = output_size
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.reasoning_info = reasoning_info

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.sentence_embedder = MemorySentenceEmbedder(embedding_info=embedding_info)

        # Can't share parameters if concat mode is enabled with mlp similarity
        if reasoning_info['mode'] == 'concat' and memory_lookup_info['mode'] == 'mlp':
            self.memory_lookup_blocks = [
                MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                     dropout_rate=dropout_rate,
                                     reasoning_info=reasoning_info)
            ]
        else:
            lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                dropout_rate=dropout_rate,
                                                reasoning_info=reasoning_info)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]

        extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                    partial_supervision_info=partial_supervision_info,
                                                    padding_amount=padding_amount)
        self.extraction_blocks = [extraction_block for _ in range(hops)]

        reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)
        self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            # self.answer_blocks.append(tf.keras.layers.BatchNormalization())
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=self.output_size,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        if len(memory.shape) == 2:
            memory = tf.reshape(memory, [query.shape[0], -1, self.sentence_size])

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision_info:
            self.supervision_losses = []

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory}, training=training)

            extraction_input = {key: value for key, value in additional_inputs.items()}
            extraction_input['similarities'] = similarities
            extraction_input['context_mask'] = memory_mask

            probabilities = extraction_block(extraction_input)

            if self.accumulate_attention:
                self.memory_attention.append(extraction_block.memory_attention)

            if self.partial_supervision_info['flag']:
                self.supervision_losses.append(extraction_block.supervision_loss)

            memory_search = tf.reduce_sum(memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search})

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer, training=training)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        return answer


class M_Gated_Memn2n(M_Basic_Memn2n):
    """
    Simple extension of basic memn2n via the addition of a gating mechanism
    """

    def __init__(self, gating_info, **kwargs):
        super(M_Gated_Memn2n, self).__init__(**kwargs)

        if gating_info['mode'] != 'mlp_gating':
            raise RuntimeError(
                'Invalid gating mode! Got: {} -- Supported: [mlp_gating]'.format(self.gating_info['mode']))

        self.gating_blocks = []
        for weight in gating_info['gating_weights']:
            self.gating_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                kwargs['l2_regularization'])))
        self.gating_blocks.append(tf.keras.layers.Dense(units=1,
                                                        activation=tf.nn.sigmoid,
                                                        kernel_regularizer=tf.keras.regularizers.l2(
                                                            kwargs['l2_regularization'])))

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision_info:
            self.supervision_losses = []

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory}, training=training)

            # Gating
            gating = similarities
            for gating_block in self.gating_blocks:
                gating = gating_block(gating)

            extraction_input = merge({'similarities': similarities, 'context_mask': memory_mask},
                                     additional_inputs)
            probabilities = extraction_block(extraction_input)

            if self.accumulate_attention:
                self.memory_attention.append(extraction_block.memory_attention)

            if self.partial_supervision_info['flag']:
                self.supervision_losses.append(extraction_block.supervision_loss)

            # Weight attention by gating
            memory_search = tf.reduce_sum(
                memory_emb * tf.expand_dims(probabilities, axis=-1) * tf.expand_dims(gating, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search})

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer, training=training)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        return answer


class M_Discriminative_Memn2n(M_Basic_Memn2n):

    def __init__(self, discriminator_weights=(), **kwargs):
        super(M_Discriminative_Memn2n, self).__init__(**kwargs)

        self.discriminator_blocks = []
        for weight in discriminator_weights:
            self.discriminator_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                   activation=tf.nn.relu,
                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                       self.l2_regularization)))
            # self.answer_blocks.append(tf.keras.layers.BatchNormalization())
            self.discriminator_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.discriminator_blocks.append(tf.keras.layers.Dense(units=1,
                                                               kernel_regularizer=tf.keras.regularizers.l2(
                                                                   self.l2_regularization)))

        self.dist = tfp.distributions.Beta(concentration1=1, concentration0=3)

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision_info:
            self.supervision_losses = []

        # This is just one hop
        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory}, training=training)

            extraction_input = merge({'similarities': similarities, 'context_mask': memory_mask},
                                     additional_inputs)
            probabilities = extraction_block(extraction_input)

            if self.accumulate_attention:
                self.memory_attention.append(extraction_block.memory_attention)

            if self.partial_supervision_info['flag']:
                self.supervision_losses.append(extraction_block.supervision_loss)

            memory_search = tf.reduce_sum(memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search})

        # Compute lookup answer
        mem_answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                mem_answer = self.answer_blocks[block_idx](mem_answer)
            else:
                if training:
                    mem_answer = self.answer_blocks[block_idx](mem_answer, training=training)

        last_hidden_mem_answer = mem_answer

        # Apply last answer block
        mem_answer = self.answer_blocks[-1](mem_answer)

        # Compute initial answer
        if self.reasoning_info['mode'] == 'concat':
            query_answer = tf.concat((query_emb, query_emb), axis=-1)
        else:
            query_answer = query_emb

        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                query_answer = self.answer_blocks[block_idx](query_answer)
            else:
                if training:
                    query_answer = self.answer_blocks[block_idx](query_answer, training=training)

        last_hidden_query_answer = query_answer

        # Apply last answer block
        query_answer = self.answer_blocks[-1](query_answer)

        # Discriminator
        discriminator_answer = tf.concat((last_hidden_query_answer, last_hidden_mem_answer), axis=1)
        for block_idx in range(len(self.discriminator_blocks) - 1):
            if block_idx % 2 == 0:
                discriminator_answer = self.discriminator_blocks[block_idx](discriminator_answer)
            else:
                if training:
                    discriminator_answer = self.discriminator_blocks[block_idx](discriminator_answer, training=training)

        # Apply last answer block
        discriminator_answer = self.discriminator_blocks[-1](discriminator_answer)

        return query_answer, mem_answer, discriminator_answer


@DeprecationWarning
class M_Entailment_Memn2n(tf.keras.Model):

    def __init__(self, query_size, sentence_size,
                 vocab_size, embedding_dimension=32, hops=1, response_linearity=False,
                 share_memory=True, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, similarity_weights=None, attention_operation='softmax',
                 partial_supervision=False, supervision_margin=0.1, padding_amount=None,
                 accumulate_attention=False, use_positional_encoding=False, aggregation_mode='sum',
                 entailment_supervision=False, entailment_margin=0.5):
        super(M_Entailment_Memn2n, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.entailment_supervision = entailment_supervision
        self.hops = hops

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')
        if share_memory:
            lookup_block = EntailmentLookup(similarity_weights=similarity_weights,
                                            attention_operation=attention_operation,
                                            partial_supervision=partial_supervision,
                                            supervision_margin=supervision_margin,
                                            padding_amount=padding_amount,
                                            aggregation_mode=aggregation_mode,
                                            entailment_margin=entailment_margin,
                                            entailment_supervision=entailment_supervision)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            reasoning_block = BasicMemoryReasoning(response_linearity=response_linearity,
                                                   aggregation_mode=aggregation_mode)
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops - 1)]
        else:
            self.memory_lookup_blocks = [EntailmentLookup(similarity_weights=similarity_weights,
                                                          attention_operation=attention_operation,
                                                          partial_supervision=partial_supervision,
                                                          supervision_margin=supervision_margin,
                                                          padding_amount=padding_amount,
                                                          aggregation_mode=aggregation_mode,
                                                          entailment_margin=entailment_margin,
                                                          entailment_supervision=entailment_supervision)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [BasicMemoryReasoning(response_linearity=response_linearity,
                                                                 aggregation_mode=aggregation_mode)
                                            for _ in range(hops - 1)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, state='training', **kwargs):
        query = inputs['text']
        memory = inputs['context']

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        query_emb = tf.reduce_mean(query_emb, axis=1)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text', 'context', 'targets']} \
            if self.partial_supervision or self.entailment_supervision else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision:
            self.supervision_losses = []

        if self.entailment_supervision:
            self.entailment_supervision_losses = []

        for idx in range(self.hops):
            lookup_block = self.memory_lookup_blocks[idx]
            lookup_input = merge({'query': upd_query, 'memory': upd_memory}, additional_inputs)

            if idx < self.hops - 1:
                upd_memory, memory_search = lookup_block(lookup_input, state=state)

                if self.accumulate_attention:
                    self.memory_attention.append(lookup_block.memory_attention)

                reasoning_block = self.memory_reasoning_blocks[idx]
                upd_query = reasoning_block([upd_query, memory_search], state=state)

                if self.partial_supervision:
                    self.supervision_losses.append(lookup_block.supervision_loss)
            else:
                similarities = lookup_block(lookup_input, state=state)

            if self.entailment_supervision:
                self.entailment_supervision_losses.append(lookup_block.entailment_loss)

        answer = similarities
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        return answer


# TODO: fix
class M_ELMo_Basic_Memn2n(tf.keras.Model):
    """
    Basic Memory Network ELMo-compliant variant. Since input text is already in embeddings format (ELMo), an
    embedding layer is no longer needed.
    """

    def __init__(self, vocab_size, hops=1, response_linearity=False,
                 embedding_mode='sentence', share_memory=True, answer_weights=[],
                 dropout_rate=0.2, l2_regularization=0., similarity_weights=None,
                 attention_operation='softmax', partial_supervision=False, supervision_margin=0.1,
                 padding_amount=None, accumulate_attention=False, use_positional_encoding=False,
                 aggregation_mode='sum'):
        super(M_ELMo_Basic_Memn2n, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.embedding_mode = embedding_mode

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, self.embedding_dimension)

        if share_memory:
            lookup_block = MemoryLookup(similarity_weights=similarity_weights,
                                        attention_operation=attention_operation,
                                        partial_supervision=partial_supervision,
                                        supervision_margin=supervision_margin,
                                        padding_amount=padding_amount,
                                        aggregation_mode=aggregation_mode)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            reasoning_block = BasicMemoryReasoning(response_linearity=response_linearity,
                                                   aggregation_mode=aggregation_mode)
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]
        else:
            self.memory_lookup_blocks = [MemoryLookup(similarity_weights=similarity_weights,
                                                      attention_operation=attention_operation,
                                                      partial_supervision=partial_supervision,
                                                      supervision_margin=supervision_margin,
                                                      padding_amount=padding_amount,
                                                      aggregation_mode=aggregation_mode)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [BasicMemoryReasoning(response_linearity=response_linearity,
                                                                 aggregation_mode=aggregation_mode)
                                            for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, max_query_length, embedding_dim] -> word embeddings
        # [batch_size, embedding_dim] -> sentence embeddings
        query_emb = inputs['text']

        # [batch_size, mem_size, max_sentence_length, embedding_dim] -> word embeddings
        # [batch_size, mem_size, embedding_dim] -> sentence embeddings
        memory_emb = inputs['context']

        if self.embedding_mode == 'word':
            # Sentence embedding
            query_emb = tf.reduce_sum(query_emb, axis=1)
            memory_emb = tf.reduce_sum(memory_emb, axis=2)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :, :tf.shape(memory_emb)[2], :]

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'text_mask',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision:
            self.supervision_losses = []

        for lookup_block, reasoning_block in zip(self.memory_lookup_blocks, self.memory_reasoning_blocks):
            lookup_input = merge({'query': upd_query, 'memory': upd_memory}, additional_inputs)
            upd_memory, memory_search = lookup_block(lookup_input)
            if self.accumulate_attention:
                self.memory_attention.append(lookup_block.memory_attention)

            upd_query = reasoning_block([upd_query, memory_search])

            if self.partial_supervision:
                self.supervision_losses.append(lookup_block.supervision_loss)

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        return answer


# TODO: fix
class M_BERT_Basic_Memn2n(tf.keras.Model):
    """
    Basic Memory Network BERT-compliant variant. Since input text is already in embeddings format (BERT), an
    embedding layer is no longer needed.
    """

    def __init__(self, vocab_size, hops=1, response_linearity=False,
                 embedding_mode='sentence', share_memory=True, answer_weights=[],
                 dropout_rate=0.2, l2_regularization=0., similarity_weights=None,
                 attention_operation='softmax', partial_supervision=False, supervision_margin=0.1,
                 padding_amount=None, accumulate_attention=False, use_positional_encoding=False,
                 aggregation_mode='sum'):

        super(M_BERT_Basic_Memn2n, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.embedding_mode = embedding_mode

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, self.embedding_dimension)

        if share_memory:
            lookup_block = MemoryLookup(similarity_weights=similarity_weights,
                                        attention_operation=attention_operation,
                                        partial_supervision=partial_supervision,
                                        supervision_margin=supervision_margin,
                                        padding_amount=padding_amount,
                                        aggregation_mode=aggregation_mode)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            reasoning_block = BasicMemoryReasoning(response_linearity=response_linearity,
                                                   aggregation_mode=aggregation_mode)
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]
        else:
            self.memory_lookup_blocks = [MemoryLookup(similarity_weights=similarity_weights,
                                                      attention_operation=attention_operation,
                                                      partial_supervision=partial_supervision,
                                                      supervision_margin=supervision_margin,
                                                      padding_amount=padding_amount,
                                                      aggregation_mode=aggregation_mode)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [BasicMemoryReasoning(response_linearity=response_linearity,
                                                                 aggregation_mode=aggregation_mode)
                                            for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        # [batch_size, max_query_length, embedding_dim] -> word embeddings
        # [batch_size, embedding_dim] -> sentence embeddings
        query_emb = inputs['text']

        # [batch_size, mem_size, max_sentence_length, embedding_dim] -> word embeddings
        # [batch_size, mem_size, embedding_dim] -> sentence embeddings
        memory_emb = inputs['context']

        if self.embedding_mode == 'word':
            # Sentence embedding
            query_emb = tf.reduce_sum(query_emb, axis=1)
            memory_emb = tf.reduce_sum(memory_emb, axis=2)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :, :tf.shape(memory_emb)[2], :]

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'text_mask',
                                                                                      'text_segment_ids',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'context_segment_ids',
                                                                                      'targets']} \
            if self.partial_supervision else {}

        if self.accumulate_attention:
            self.memory_attention = []

        if self.partial_supervision:
            self.supervision_losses = []

        for lookup_block, reasoning_block in zip(self.memory_lookup_blocks, self.memory_reasoning_blocks):
            lookup_input = merge({'query': upd_query, 'memory': upd_memory}, additional_inputs)
            upd_memory, memory_search = lookup_block(lookup_input)
            if self.accumulate_attention:
                self.memory_attention.append(lookup_block.memory_attention)

            upd_query = reasoning_block([upd_query, memory_search])

            if self.partial_supervision:
                self.supervision_losses.append(lookup_block.supervision_loss)

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        return answer


@DeprecationWarning
class M_Word_Memn2n(tf.keras.Model):
    """
    Word-level Memory Network. Instead of immediately computing a weighted sentence embedding. Text inputs (query and
    memory cells) are evaluated at word-level during the Memory Lookup and Memory Reasoning phases.
    In this implementation, the content vector is at word-level at the end of the Memory Lookup phase. Subsequently,
    a Seq2Seq architecture is employed for query->content and content->query for query and memory update.
    """

    def __init__(self, query_size, sentence_size, vocab_size, enc_units, dec_units, att_units,
                 embedding_dimension=32, hops=1, batch_size=None,
                 share_memory=True, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, similarity_weights=None, attention_operation='softmax',
                 partial_supervision=False, supervision_margin=0.1, padding_amount=None,
                 accumulate_attention=False, use_positional_encoding=False):
        super(M_Word_Memn2n, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, self.embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix,
                                                          mask_zero=True,
                                                          name='memory_embedding')
        if share_memory:
            lookup_block = WordMemoryLookup(similarity_weights=similarity_weights,
                                            attention_operation=attention_operation,
                                            partial_supervision=partial_supervision,
                                            supervision_margin=supervision_margin,
                                            padding_amount=padding_amount)
            reasoning_block = CoAttentionMemoryReasoning(enc_units=enc_units,
                                                         dec_units=dec_units,
                                                         att_units=att_units,
                                                         batch_size=batch_size)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]
        else:
            self.memory_lookup_blocks = [WordMemoryLookup(similarity_weights=similarity_weights,
                                                          attention_operation=attention_operation,
                                                          partial_supervision=partial_supervision,
                                                          supervision_margin=supervision_margin,
                                                          padding_amount=padding_amount)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [CoAttentionMemoryReasoning(enc_units=enc_units,
                                                                       dec_units=dec_units,
                                                                       att_units=att_units,
                                                                       batch_size=batch_size)
                                            for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        query = inputs['text']
        memory = inputs['context']

        query_emb = self.query_embedding(query)
        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :, :tf.shape(memory_emb)[2], :]

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text', 'context', 'targets']} \
            if self.partial_supervision else {}

        if self.accumulate_attention:
            self.memory_attention = []

        for hop_idx, (lookup_block, reasoning_block) in enumerate(zip(self.memory_lookup_blocks,
                                                                      self.memory_reasoning_blocks)):
            lookup_input = merge({'query': upd_query, 'memory': upd_memory}, additional_inputs)
            upd_memory, memory_search, lookup_probs = lookup_block(lookup_input)

            if self.accumulate_attention:
                self.memory_attention.append(lookup_block.memory_attention)

            if hop_idx == len(self.memory_reasoning_blocks) - 1:
                upd_query = reasoning_block([query_emb, memory_search,
                                             memory_emb, lookup_probs], compute_memory_scores=False)
            else:
                upd_query, upd_memory, memory_search_scores = reasoning_block([query_emb, memory_search,
                                                                               memory_emb, lookup_probs])

        upd_query = tf.reduce_sum(upd_query, 1)
        # upd_memory_search = memory_search * tf.reshape(memory_search_scores, [-1, memory_search_scores.shape[2], 1])
        # upd_memory_search = tf.reduce_sum(upd_memory_search, 1)

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])
        return answer


@DeprecationWarning
class M_Bow_Memn2n(tf.keras.Model):
    """
    Bag-of-Words Basic Memory Network variant. The only difference is that embedding layers are not required.
    """

    def __init__(self, hops=1,
                 response_linearity=False, share_memory=True, answer_weights=[],
                 dropout_rate=.2, l2_regularization=0., similarity_weights=None,
                 attention_operation='softmax', partial_supervision=False,
                 supervision_margin=0.1, padding_amount=None,
                 accumulate_attention=False, aggregation_mode='sum'):
        super(M_Bow_Memn2n, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention

        if share_memory:
            lookup_block = MemoryLookup(similarity_weights=similarity_weights,
                                        attention_operation=attention_operation,
                                        partial_supervision=partial_supervision,
                                        supervision_margin=supervision_margin,
                                        padding_amount=padding_amount,
                                        aggregation_mode=aggregation_mode)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            reasoning_block = BasicMemoryReasoning(response_linearity=response_linearity,
                                                   aggregation_mode=aggregation_mode)
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]
        else:
            self.memory_lookup_blocks = [MemoryLookup(similarity_weights=similarity_weights,
                                                      attention_operation=attention_operation,
                                                      partial_supervision=partial_supervision,
                                                      supervision_margin=supervision_margin,
                                                      padding_amount=padding_amount,
                                                      aggregation_mode=aggregation_mode)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [BasicMemoryReasoning(response_linearity=response_linearity,
                                                                 aggregation_mode=aggregation_mode)
                                            for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        query = inputs['text']
        memory = inputs['context']

        upd_query, upd_memory = query, memory
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text', 'context', 'targets']} \
            if self.partial_supervision else {}

        if self.accumulate_attention:
            self.memory_attention = []

        for lookup_block, reasoning_block in zip(self.memory_lookup_blocks, self.memory_reasoning_blocks):
            lookup_input = merge({'query': upd_query, 'memory': upd_memory}, additional_inputs)
            upd_memory, memory_search = lookup_block(lookup_input)

            if self.accumulate_attention:
                self.memory_attention.append(lookup_block.memory_attention)

            upd_query = reasoning_block([upd_query, memory_search])

        answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])
        return answer


class M_Transformer(tf.keras.Model):
    """
    Transformer implementation according to Tensorflow documentation.
    https://www.tensorflow.org/tutorials/text/transformer
    """

    def __init__(self, num_layers, embedding_dimension, query_size, sentence_size,
                 num_heads, dff, vocab_size, dropout_rate=0.1, embedding_matrix=None,
                 accumulate_attention=False, answer_weights=[],
                 l2_regularization=0., use_positional_encoding=True):
        super(M_Transformer, self).__init__()

        self.embedding_dimension = embedding_dimension
        self.use_positional_encoding = use_positional_encoding

        if use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.dropout = tf.keras.layers.Dropout(dropout_rate)

        self.encoder = Encoder(num_layers, embedding_dimension, num_heads, dff, dropout_rate)

        self.decoder = Decoder(num_layers, embedding_dimension, num_heads, dff, dropout_rate)

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

        self.accumulate_attention = accumulate_attention

    def call(self, x, training=True, state='training'):
        # [batch_size, query_size]
        query = x['text']

        # [batch_size, 1, 1, query_size]
        query_look_ahead_mask = x['text_combined_mask']

        # [batch_size, sentence_size]
        memory = x['context']

        # [batch_size, 1, 1, sentence_size]
        kb_mask = x['context_mask']

        # [batch_size, 1, sentence_size, sentence_size]
        sentence_wise_mask = x['context_combined_mask']

        # [batch_size, query_size, embedding_dimension]
        query_emb = self.query_embedding(query)
        query_emb *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :query_emb.shape[1], :]

        query_emb = self.dropout(query_emb, training=training)

        # [batch_size, sentence_size, embedding_dimension]
        memory_emb = self.memory_embedding(memory)
        memory_emb *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))

        if self.use_positional_encoding:
            memory_emb += self.pos_encoding[:, :memory_emb.shape[1], :]

        memory_emb = self.dropout(memory_emb, training=training)

        # (batch_size, inp_seq_len, embedding_dimension)
        enc_output = self.encoder(memory_emb, training, sentence_wise_mask)

        # dec_output.shape == (batch_size, tar_seq_len, embedding_dimension)
        dec_output = self.decoder(
            query_emb, enc_output, training, query_look_ahead_mask, kb_mask)

        # if self.accumulate_attention:
        #     TODO

        answer = tf.reduce_sum(dec_output, axis=1)
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        return answer


class M_SemanticIteratedTransformer(tf.keras.Model):

    def __init__(self, query_size, sentence_size, mha_layers, mha_heads, mha_dff,
                 vocab_size, segments_amount, embedding_dimension=32, hops=1, reduction_weights=None,
                 share_memory=True, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, partial_supervision=False, supervision_margin=0.1,
                 accumulate_attention=False, use_positional_encoding=False, pooling_att_dimension=256):
        super(M_SemanticIteratedTransformer, self).__init__()
        self.partial_supervision = partial_supervision
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.embedding_dimension = embedding_dimension

        if hops != 1:
            raise NotImplementedError("I'll do it at some point ;)")

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.dropout = tf.keras.layers.Dropout(dropout_rate)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.query_pooler = GeneralizedPooling(segments_amount=segments_amount,
                                               attention_dimension=pooling_att_dimension,
                                               embedding_dimension=embedding_dimension)

        self.memory_pooler = GeneralizedPooling(segments_amount=segments_amount,
                                                attention_dimension=pooling_att_dimension,
                                                embedding_dimension=embedding_dimension)

        if share_memory:
            lookup_block = SemanticTransformerLookup(mha_heads=mha_heads,
                                                     mha_layers=mha_layers,
                                                     mha_dff=mha_dff,
                                                     embedding_dimension=embedding_dimension,
                                                     dropout_rate=dropout_rate,
                                                     partial_supervision=partial_supervision,
                                                     supervision_margin=supervision_margin)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]
            reasoning_block = SemanticTransformerReasoning(reduction_weights=reduction_weights,
                                                           accumulate_attention=accumulate_attention)
            self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]
        else:
            self.memory_lookup_blocks = [SemanticTransformerLookup(mha_heads=mha_heads,
                                                                   mha_layers=mha_layers,
                                                                   mha_dff=mha_dff,
                                                                   embedding_dimension=embedding_dimension,
                                                                   dropout_rate=dropout_rate,
                                                                   partial_supervision=partial_supervision,
                                                                   supervision_margin=supervision_margin)
                                         for _ in range(hops)]
            self.memory_reasoning_blocks = [SemanticTransformerReasoning(reduction_weights=reduction_weights,
                                                                         accumulate_attention=accumulate_attention)
                                            for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, x, training=True, state='training', **kwargs):

        # [batch_size, query_size]
        query = x['text']

        # [batch_size, 1, query_size, query_size]
        query_look_ahead_mask = x['text_combined_mask']

        # [batch_size, memory_size, sentence_size]
        memory = x['context']

        # [batch_size * memory_size, 1, 1, sentence_size]
        kb_mask = x['context_mask']

        # [batch_size, query_size, embedding_dimension]
        query_emb = self.query_embedding(query)
        query_emb *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :query_emb.shape[1], :]

        query_emb = self.dropout(query_emb, training=training)

        self.pooling_regularization = []

        query_emb = self.query_pooler(query_emb)
        self.pooling_regularization.append(self.query_pooler.disagreement_penalization)

        # [batch_size, memory_size, sentence_size, embedding_dimension]
        memory_emb = self.memory_embedding(memory)
        memory_emb *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))

        if self.use_positional_encoding:
            memory_emb += self.pos_encoding[:, :, :memory_emb.shape[2], :]

        memory_emb = self.dropout(memory_emb, training=training)

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.memory_pooler(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[1], memory_emb.shape[2]])

        memory_pooling_disagreement_penalization = tf.reshape(self.memory_pooler.disagreement_penalization,
                                                              [-1, memory.shape[1]])
        memory_pooling_disagreement_penalization = tf.reduce_sum(memory_pooling_disagreement_penalization,
                                                                 axis=1)
        self.pooling_regularization.append(memory_pooling_disagreement_penalization)

        upd_query, upd_memory = query_emb, memory_emb
        for lookup_block, reasoning_block in zip(self.memory_lookup_blocks, self.memory_reasoning_blocks):
            lookup_input = {
                'query': upd_query,
                'memory': upd_memory,
                'text_combined_mask': query_look_ahead_mask,
                'memory_mask': kb_mask
            }
            memory_search, upd_query = lookup_block(lookup_input)

            reasoning_input = {
                'memory_search': memory_search,
                'query': upd_query
            }
            upd_query = reasoning_block(reasoning_input)

            if self.partial_supervision:
                self.supervision_losses.append(lookup_block.supervision_loss)

            if self.accumulate_attention:
                self.query_attention = reasoning_block.query_attention

        # [batch_size * memory_size, embedding_dimension]
        context_vector = tf.reduce_sum(upd_query, axis=1)

        answer = context_vector
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        # [batch_size * memory_size,]
        return answer


class M_SemanticCoAttention(tf.keras.Model):

    def __init__(self, query_size, sentence_size, vocab_size, clause_segments_amount, explanation_segments_amount,
                 embedding_dimension=32, answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None,
                 accumulate_attention=False, use_positional_encoding=False, pooling_att_dimension=256,
                 pooling_use_masking=False):
        super(M_SemanticCoAttention, self).__init__()

        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = use_positional_encoding
        self.embedding_dimension = embedding_dimension

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.dropout = tf.keras.layers.Dropout(dropout_rate)

        self.clause_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=query_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='clause_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.clause_poolers = [GeneralizedPooling(segments_amount=amount,
                                                  attention_dimension=pooling_att_dimension,
                                                  embedding_dimension=embedding_dimension,
                                                  use_masking=pooling_use_masking)
                               for amount in clause_segments_amount]

        self.memory_poolers = [GeneralizedPooling(segments_amount=amount,
                                                  attention_dimension=pooling_att_dimension,
                                                  embedding_dimension=embedding_dimension,
                                                  use_masking=pooling_use_masking)
                               for amount in explanation_segments_amount]

        self.lookup_block = MemoryLookup()

        self.reasoning_block = BasicMemoryReasoning()

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, x, training=True, state='training', **kwargs):

        # [batch_size, clause_size]
        clause = x['text']

        # [batch_size, memory_size, sentence_size]
        memory = x['context']

        # Embedding

        # [batch_size, clause_size, embedding_dim]
        clause_emb = self.clause_embedding(clause)
        clause_emb *= tf.math.sqrt(tf.cast(self.embedding_dimension, tf.float32))

        if self.use_positional_encoding:
            clause_emb += self.pos_encoding[:, :clause_emb.shape[1], :]

        clause_emb = self.dropout(clause_emb, training=training)

        # [batch_size, memory_size, sentence_size, embedding_dim]
        memory_emb = self.memory_embedding(memory)

        # Pooling

        self.pooling_regularization = []

        # [batch_size, embedding_dim]
        for pooler in self.clause_poolers:
            clause_emb = pooler(clause_emb)
            if pooler.segments_amount > 1:
                self.pooling_regularization.append(pooler.disagreement_penalization)

        clause_emb = tf.squeeze(clause_emb, axis=1)

        # [batch_size, memory_size, embedding_dim]
        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        for pooler in self.memory_poolers:

            memory_emb = pooler(memory_emb)

            if pooler.segments_amount > 1:
                memory_pooling_disagreement_penalization = tf.reshape(pooler.disagreement_penalization,
                                                                      [-1, memory.shape[1]])
                memory_pooling_disagreement_penalization = tf.reduce_sum(memory_pooling_disagreement_penalization,
                                                                         axis=1)
                self.pooling_regularization.append(memory_pooling_disagreement_penalization)

        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        # Lookup

        # [batch_size, embedding_dim]
        memory_emb, memory_search = self.lookup_block(inputs={
            'query': clause_emb,
            'memory': memory_emb
        })

        upd_clause = self.reasoning_block([clause_emb, memory_search])

        answer = upd_clause
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        # [batch_size, ]
        return answer


class M_ALBERTTextClassifier(tf.keras.Model):

    def __init__(self, embedding_mode='sentence', albert_model='https://tfhub.dev/google/albert_base/2',
                 answer_weights=(), l2_regularization=0.,
                 dropout_rate=.2, **kwargs):
        super(M_ALBERTTextClassifier, self).__init__(**kwargs)

        self.embedding = ALBERTEmbeddingLayer(embedding_mode=embedding_mode,
                                              albert_model=albert_model,
                                              name='albert_embedding')

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):

        sentence = inputs['text']
        sentence_mask = inputs['text_mask']
        sentence_segment_ids = inputs['text_segment_ids']

        sentence_emb = self.embedding({'input_ids': sentence,
                                       'input_mask': sentence_mask,
                                       'segment_ids': sentence_segment_ids})

        answer = sentence_emb
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        answer = self.answer_blocks[-1](answer)
        answer = tf.reshape(answer, [-1, ])

        return answer


class M_MemoryDistilBERT(TFDistilBertForSequenceClassification):

    def call(self, inputs, **kwargs):
        if not isinstance(inputs, (tuple, list, dict)):
            print(inputs.shape)

        # [bs, dim]
        bert_clause_output = self.distilbert(
            {''.join(key.split('clause_')[1:]): item for key, item in inputs.items() if key.startswith('clause')},
            **kwargs)
        clause_output = bert_clause_output[0]
        clause_output = clause_output[:, 0]

        # [bs * mem_size, dim]
        bert_memory_output = self.distilbert(
            {''.join(key.split('memory_')[1:]): tf.reshape(item, [-1, item.shape[-1]]) for key, item in inputs.items()
             if key.startswith('memory')}, **kwargs)
        memory_output = bert_memory_output[0]
        memory_output = memory_output[:, 0]

        # [bs, mem_size, dim]
        memory_output = tf.reshape(memory_output, [tf.shape(clause_output)[0],
                                                   -1,
                                                   clause_output.shape[1]])

        # [bs, mem_size]
        similarity = tf.reduce_sum(tf.expand_dims(clause_output, 1) * memory_output, axis=2)
        probs = tf.nn.softmax(similarity, axis=1)

        self.probs = probs

        # [bs, dim]
        acc_vector = tf.reduce_sum(tf.expand_dims(probs, axis=-1) * memory_output, axis=1)

        # [bs, 2*dim]
        answer = tf.concat((clause_output, acc_vector), axis=1)

        # [bs, dim]
        # answer = clause_output + acc_vector

        pooled_output = self.pre_classifier(answer)
        pooled_output = self.dropout(pooled_output, training=kwargs.get('training', False))
        logits = self.classifier(pooled_output)

        outputs = (logits,) + bert_clause_output[1:]
        return outputs  # logits, (hidden_states), (attentions)


@DeprecationWarning
class M_ParallelDLU(tf.keras.Model):

    def __init__(self, query_size, sentence_size, max_logic_depth, max_logic_groups, vocab_size, key_vocab,
                 embedding_dimension=32, dropout_rate=.2, l2_regularization=0., embedding_matrix=None,
                 answer_weights=[], unification_weights=[], use_positional_encoding=False, **kwargs):
        super(M_ParallelDLU, self).__init__(**kwargs)
        self.use_positional_encoding = use_positional_encoding

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.encoder = tf.keras.layers.LSTM(embedding_dimension, return_state=True, return_sequences=True)
        self.unification_module = DLUParallelModule(hidden_size=embedding_dimension)
        self.logic_module = DLULogicModule(max_logic_depth=max_logic_depth,
                                           max_logic_groups=max_logic_groups)

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, |V|, query_size]
        query_unification_masks = inputs['text_unification_masks']

        # [batch_size, ]
        query_lengths = inputs['text_lengths']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size, |V|, sentence_size]
        memory_unification_masks = inputs['context_unification_masks']

        # [batch_size, memory_size, ]
        memory_lengths = inputs['context_lengths']

        # [batch_size, memory_size]
        memory_segments = inputs['context_segments']

        # [batch_size, memory_size, max_logic_depth - 1, max_logic_groups, sentence_size]
        logic_group_masks = inputs['logic_group_masks']

        # [batch_size, memory_size, max_logic_depth, max_logic_groups]
        logic_operator_gates = inputs['logic_operator_gates']

        # Embedding

        # [batch_size, query_size, embedding_dimension]
        query_emb = self.query_embedding(query)

        # [batch_size, memory_size, sentence_size, embedding_dimension]
        memory_emb = self.memory_embedding(memory)

        # Positional Encoding

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        # Unstack masks in order to loop over keys

        # list of length |V|: each item has shape [batch_size, query_size]
        query_unification_masks = tf.unstack(query_unification_masks, axis=1)

        # [batch_size * memory_size, |V|, sentence_size]
        memory_unification_masks = tf.reshape(memory_unification_masks, [-1,
                                                                         memory_unification_masks.shape[2],
                                                                         memory_unification_masks.shape[3]])

        # list of length |V|: each item has shape [batch_size * memory_size, sentence_size]
        memory_unification_masks = tf.unstack(memory_unification_masks, axis=1)

        #  Encode query

        enc_outputs, enc_h, enc_c = self.encoder(query_emb)

        # Build (query, memory_cell) pairs

        # [batch_size * memory_size, sentence_size, embedding_dimension]
        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_lengths = tf.reshape(memory_lengths, [-1, ])

        # [batch_size * memory_size, query_size, embedding_dimension]
        enc_outputs_repeated = tf.gather(enc_outputs, memory_segments)
        enc_outputs_repeated = tf.reshape(enc_outputs_repeated, [-1, enc_outputs.shape[1], enc_outputs.shape[2]])

        # [batch_size * memory_size, embedding_dimension]
        enc_h_repeated = tf.gather(enc_h, memory_segments)
        enc_h_repeated = tf.reshape(enc_h_repeated, [-1, enc_h.shape[-1]])

        enc_c_repeated = tf.gather(enc_c, memory_segments)
        enc_c_repeated = tf.reshape(enc_c_repeated, [-1, enc_c.shape[-1]])

        # [[batch_size * memory_size, embedding_dimension], [batch_size * memory_size, embedding_dimension]]
        enc_state_repeated = [enc_h_repeated, enc_c_repeated]

        # [batch_size * memory_size]
        query_lengths_repeated = tf.gather(query_lengths, memory_segments)
        query_lengths_repeated = tf.reshape(query_lengths_repeated, [-1, ])

        # Apply Unification

        # [batch_size * mem_size, |V|, sentence_size, embedding_dim]
        unification_outputs = None
        for query_unification_mask, memory_unification_mask in zip(query_unification_masks,
                                                                   memory_unification_masks):

            # [batch_size * memory_size, query_size]
            query_unification_mask_repeated = tf.gather(query_unification_mask, memory_segments)
            query_unification_mask_repeated = tf.reshape(query_unification_mask_repeated,
                                                         [-1, query_unification_mask.shape[-1]])

            unification_input = {
                'encoder_outputs': enc_outputs_repeated,
                'encoder_state': enc_state_repeated,
                'decoder_unification_mask': memory_unification_mask,
                'encoder_unification_mask': query_unification_mask_repeated,
                'encoder_lengths': query_lengths_repeated,
                'decoder_input': memory_emb,
                'decoder_lengths': memory_lengths
            }

            # [batch_size * mem_size, sentence_size, embedding_dim]
            unification_output = self.unification_module(unification_input)

            unification_output = tf.expand_dims(unification_output, axis=1)

            if unification_outputs is None:
                unification_outputs = unification_output
            else:
                unification_outputs = tf.concat((unification_outputs, unification_output), axis=1)

        # Create candidate memory

        # [batch_size * memory_size, |V|, sentence_size]
        memory_unification_masks = tf.stack(memory_unification_masks, axis=1)

        # [batch_size * memory_size, sentence_size]
        symbolic_memory_mask = tf.reduce_sum(memory_unification_masks, axis=1)

        # [batch_size * memory_size, sentence_size, 1]
        inverted_memory_unification_masks = tf.logical_not(tf.cast(symbolic_memory_mask, tf.bool))
        inverted_memory_unification_masks = tf.cast(inverted_memory_unification_masks, tf.float32)
        inverted_memory_unification_masks = tf.expand_dims(inverted_memory_unification_masks, axis=-1)

        # [batch_size * memory_size, sentence_size, embedding_dim]
        terminal_memory = memory_emb * inverted_memory_unification_masks

        # [batch_size * memory_size, |V|, sentence_size, 1]
        memory_unification_masks = tf.expand_dims(memory_unification_masks, axis=-1)

        # [batch_size * memory_size, sentence_size, embedding_dim]
        unified_memory = terminal_memory + tf.reduce_sum(memory_unification_masks * unification_outputs,
                                                         axis=1)

        # Logic operators

        # [batch_size * memory_size, max_logic_depth - 1, max_logic_groups, sentence_size]
        logic_group_masks = tf.reshape(logic_group_masks,
                                       [-1,
                                        logic_group_masks.shape[2],
                                        logic_group_masks.shape[3],
                                        logic_group_masks.shape[4]])

        # [batch_size * memory_size, max_logic_depth, max_logic_groups]
        logic_operator_gates = tf.reshape(logic_operator_gates, [-1,
                                                                 logic_operator_gates.shape[2],
                                                                 logic_operator_gates.shape[3]])

        logic_inputs = {
            'unification_outputs': unified_memory,
            'real_memory': memory_emb,
            'symbolic_memory_mask': symbolic_memory_mask,
            'logic_group_masks': logic_group_masks,
            'logic_operator_gates': logic_operator_gates
        }

        # [batch_size, memory_size]
        unification_loss = self.logic_module(logic_inputs, training=training)
        unification_loss = tf.reshape(unification_loss, [memory.shape[0], memory.shape[1]])

        # Answer block

        answer = unification_loss
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        # Apply last answer block
        answer = self.answer_blocks[-1](answer)

        answer = tf.reshape(answer, [-1, ])

        return answer


######################################################
####################### LAYERS #######################
######################################################

# Memory Network layers

# Embedding

class MemorySentenceEmbedder(tf.keras.layers.Layer):

    def __init__(self, embedding_info, **kwargs):
        super(MemorySentenceEmbedder, self).__init__(**kwargs)
        self.embedding_info = embedding_info

    def call(self, inputs, training=None, mask=None, **kwargs):
        # [# samples, max_sequence_length, embedding_dim]

        if self.embedding_info['mode'] == 'sum':
            return tf.reduce_sum(inputs, axis=1)
        elif self.embedding_info['mode'] == 'mean':
            return tf.reduce_mean(inputs, axis=1)
        elif self.embedding_info['mode'] == 'generalized_pooling':
            pooled = GeneralizedPooling(segments_amount=1,
                                        attention_dimension=self.embedding_info['attention_dimension'],
                                        embedding_dimension=inputs.shape[-1])(inputs)
            return tf.squeeze(pooled)
        elif self.embedding_info['mode'] == 'recurrent_embedding':
            if self.embedding_info['bidirectional']:
                rnn = tf.keras.layers.Bidirectional(tf.keras.layers.GRU(self.embedding_info['hidden_size']))(inputs)
            else:
                rnn = tf.keras.layers.GRU(self.embedding_info['hidden_size'])(inputs)
        else:
            raise RuntimeError('Invalid sentence embedding mode! Got: {}'.format(self.embedding_info['mode']))

        return rnn


# Lookup

class MemorySentenceLookup(tf.keras.layers.Layer):
    """
    Basic Memory Lookup layer. Query to memory cells similarity is computed either via doc product or via
    a FNN. The content vector is computed by weighting memory cells according to their corresponding computed
    similarities.

    Moreover, the layer is sensitive to strong supervision loss, since the latter is implemented via a max-margin loss
    on computed similarity distribution (generally a softmax).
    """

    def __init__(self,
                 reasoning_info,
                 memory_lookup_info,
                 dropout_rate=0.2,
                 **kwargs):
        super(MemorySentenceLookup, self).__init__(**kwargs)
        self.memory_lookup_info = memory_lookup_info
        self.reasoning_info = reasoning_info
        self.dropout_rate = dropout_rate

    def compute_mask(self, inputs, mask=None):
        return mask

    def call(self, inputs, training=False, mask=None, **kwargs):
        # [batch_size, embedding_dim]
        query = inputs['query']

        # [batch_size, mem_size, embedding_dim]
        memory = inputs['memory']

        if self.memory_lookup_info['mode'] == 'dot_product':
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1] / memory.shape[-1], tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2)
        elif self.memory_lookup_info['mode'] == 'scaled_dot_product':
            # [batch_size, mem_size, embedding_dim]
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1].value / memory.shape[-1].value, tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2) * self.embedding_dimension ** 0.5
        elif self.memory_lookup_info['mode'] == 'mlp':

            if self.reasoning_info['mode'] == 'concat':
                query_dimension = query.shape[-1]
                repeat_amount = query_dimension // memory.shape[-1]
                memory = tf.tile(memory, multiples=[1, 1, repeat_amount])

            # [batch_size, mem_size, embedding_dim]
            repeated_query = tf.expand_dims(query, axis=1)
            repeated_query = tf.tile(repeated_query, [1, memory.shape[1], 1])

            # [batch_size, mem_size, embedding_dim * 2]
            att_input = tf.concat((memory, repeated_query), axis=-1)
            att_input = tf.reshape(att_input, [-1, att_input.shape[-1]])

            dotted = att_input
            for weight in self.memory_lookup_info['weights']:
                dotted = tf.keras.layers.Dense(units=weight, activation=tf.nn.relu)(dotted)
                # dotted = tf.keras.layers.BatchNormalization()(dotted, training=training)
                dotted = tf.keras.layers.Dropout(self.dropout_rate)(dotted, training=training)

            dotted = tf.keras.layers.Dense(units=1)(dotted)
            dotted = tf.reshape(dotted, [-1, memory.shape[1]])
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        # [batch_size, memory_size]
        return dotted


# TODO: fix
@DeprecationWarning
class WordMemoryLookup(MemorySentenceLookup):
    """
    Word Memory Lookup layer variant. Similarities are computed at word level without sentence level reduction.
    """

    def compute_mask(self, inputs, mask=None):
        if mask:
            for idx in range(len(mask)):
                if len(mask[idx].shape) > 2:
                    mask[idx] = tf.math.reduce_any(mask[idx], axis=1)

        return mask

    def call(self, inputs, training=None, mask=None, **kwargs):
        if not self.partial_supervision:
            assert len(inputs) == 2
        else:
            assert len(inputs) == 5

        # [batch_size, max_length, embedding_dimension]
        query_emb = inputs['query']

        # [batch_size, memory_size, max_length, embedding_dimension]
        memory_emb = inputs['memory']

        # Memory Lookup #

        query_lookup = tf.reduce_sum(query_emb, axis=1)

        # Input memory embedding
        # [batch_size, memory_size, embedding_dimension]
        sent_m_A = tf.reduce_sum(memory_emb, 2)

        # Query matching
        # [batch_size, memory_size]
        dotted = self._compute_lookup_similarity(query=query_lookup,
                                                 memory=sent_m_A)

        if self.partial_supervision:
            positive_idxs = inputs['positive_indexes']
            negative_idxs = inputs['negative_indexes']
            mask_idxs = inputs['mask_indexes']
            self._add_supervision_loss(prob_dist=dotted,
                                       positive_idxs=positive_idxs,
                                       negative_idxs=negative_idxs,
                                       mask_idxs=mask_idxs)

        probs = self._compute_similarity_attention(similarities=dotted)
        probs = tf.expand_dims(probs, -1)
        probs = tf.expand_dims(probs, -1)

        # Updating query
        # [batch_size, max_length, embedding_dim]
        o_k = memory_emb * probs
        o_k = tf.reduce_sum(o_k, 1)

        return memory_emb, o_k, probs


# TODO: fix
class BowMemoryLookup(MemorySentenceLookup):
    """
    Bag-of-Words Memory Lookup variant. Any kind of sentence level reduction is not needed.
    """

    def call(self, inputs, training=None, mask=None, **kwargs):
        if not self.partial_supervision:
            assert len(inputs) == 2
        else:
            assert len(inputs) == 5

        query_emb = inputs['query']
        memory_emb = inputs['memory']

        # Memory Lookup #

        # Query matching
        # [batch_size, memory_size]
        dotted = self._compute_lookup_similarity(query=query_emb,
                                                 memory=memory_emb)

        if self.partial_supervision:
            positive_idxs = inputs['positive_indexes']
            negative_idxs = inputs['negative_indexes']
            mask_idxs = inputs['mask_indexes']
            self._add_supervision_loss(prob_dist=dotted,
                                       positive_idxs=positive_idxs,
                                       negative_idxs=negative_idxs,
                                       mask_idxs=mask_idxs)

        probs = self._compute_similarity_attention(similarities=dotted)

        # Updating query
        probs_temp = tf.expand_dims(probs, -1)
        o_k = tf.reduce_sum(memory_emb * probs_temp, axis=1)

        return memory_emb, o_k


# TODO: fix
class EntailmentLookup(MemorySentenceLookup):

    def __init__(self,
                 entailment_supervision=False,
                 entailment_margin=0.5,
                 **kwargs):
        super(EntailmentLookup, self).__init__(**kwargs)
        self.entailment_supervision = entailment_supervision
        self.entailment_margin = entailment_margin
        self.entailment_loss = None

    def _add_entailment_loss(self, similarities, positive_indexes, negative_indexes):

        # [batch_size, 1]
        total_scores = tf.reduce_sum(similarities, axis=1)
        total_scores = tf.nn.sigmoid(total_scores)

        positive_scores = tf.gather(total_scores, positive_indexes)
        negative_scores = tf.gather(total_scores, negative_indexes)

        positive_scores = tf.tile(positive_scores, multiples=[1, tf.shape(negative_scores)[0]])
        negative_scores = tf.transpose(negative_scores, [1, 0])

        hop_entailment_loss = tf.maximum(0., self.entailment_margin - positive_scores + negative_scores)
        hop_entailment_loss = tf.reduce_sum(hop_entailment_loss)
        self.entailment_loss = hop_entailment_loss

    def call(self, inputs, training=None, mask=None, aggregate=False, state='training', **kwargs):
        query_emb = inputs['query']
        memory_emb = inputs['memory']

        if self.reduce_query:
            query_emb = tf.reduce_sum(query_emb, axis=1)

        # Memory Lookup #

        # Input memory embedding
        # [batch_size, memory_size, embedding_dimension]
        sent_m_A = tf.reduce_mean(memory_emb, axis=2)

        # Query matching
        # [batch_size, memory_size]
        dotted = self._compute_lookup_similarity(query=query_emb,
                                                 memory=sent_m_A)

        if self.entailment_supervision and state == 'training':
            positive_idxs = inputs['entailment_positive_indexes']
            negative_idxs = inputs['entailment_negative_indexes']
            self._add_entailment_loss(similarities=dotted,
                                      positive_indexes=positive_idxs,
                                      negative_indexes=negative_idxs)

        if aggregate:
            probs = self._compute_similarity_attention(similarities=dotted)
            self.memory_attention = probs

            if self.partial_supervision:
                positive_idxs = inputs['memory_positive_indexes']
                negative_idxs = inputs['memory_negative_indexes']
                mask_idxs = inputs['memory_mask_indexes']
                self._add_supervision_loss(prob_dist=probs,
                                           positive_idxs=positive_idxs,
                                           negative_idxs=negative_idxs,
                                           mask_idxs=mask_idxs)

            # Output memory embedding
            # [batch_size, memory_size, max_length, embedding_dimension
            sent_m_C = tf.reduce_mean(memory_emb, axis=2)

            # Updating query
            probs_temp = tf.expand_dims(probs, axis=-1)
            o_k = tf.reduce_sum(sent_m_C * probs_temp, axis=1)

            return memory_emb, o_k
        else:
            return dotted


class SemanticTransformerLookup(tf.keras.layers.Layer):

    def __init__(self, mha_layers, mha_heads, mha_dff, embedding_dimension,
                 dropout_rate=0.1, partial_supervision=False, supervision_margin=0.1, **kwargs):
        super(SemanticTransformerLookup, self).__init__(**kwargs)
        self.partial_supervision = partial_supervision
        self.supervision_margin = supervision_margin

        self.encoder = Encoder(num_layers=mha_layers, embedding_dimension=embedding_dimension,
                               num_heads=mha_heads, dff=mha_dff, dropout_rate=dropout_rate)

        self.decoder = Decoder(num_layers=mha_layers, embedding_dimension=embedding_dimension,
                               num_heads=mha_heads, dff=mha_dff,
                               dropout_rate=dropout_rate)

    def call(self, inputs, training=None, mask=None, **kwargs):
        # [batch_size, query_size, embedding_dim]
        query_emb = inputs['query']

        # [batch_size, 1, query_size, query_size]
        query_look_ahead_mask = inputs['text_combined_mask']

        # [batch_size, memory_size, sentence_size, embedding_dim]
        memory_emb = inputs['memory']

        # [batch_size * memory_size, 1, 1, sentence_size]
        memory_mask = inputs['memory_mask']

        # Memory Lookup #

        memory_size = memory_emb.shape[1]

        # [batch_size * memory_size, sentence_size, embedding_dim]
        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])

        # [batch_size * memory_size, query_size, embedding_dim]
        query_emb = tf.expand_dims(query_emb, axis=1)
        query_emb = tf.tile(query_emb, multiples=[1, memory_size, 1, 1])
        query_emb = tf.reshape(query_emb, [-1, query_emb.shape[2], query_emb.shape[3]])

        # [batch_size * memory_size, 1, query_size, query_size]
        query_look_ahead_mask = tf.tile(query_look_ahead_mask, multiples=[1, memory_size, 1, 1])
        query_look_ahead_mask = tf.reshape(query_look_ahead_mask,
                                           [-1, 1, query_look_ahead_mask.shape[2], query_look_ahead_mask.shape[3]])

        # [batch_size * memory_size, sentence_size, embedding_dim]
        enc_output = self.encoder(memory_emb, training, memory_mask)

        # [batch_size * memory_size, query_size, embedding_dim]
        dec_output = self.decoder(query_emb, enc_output, training, query_look_ahead_mask, memory_mask)

        return dec_output, query_emb

    # Reasoning


class SemanticLookup(tf.keras.layers.Layer):

    def __init__(self, embedding_dimension, attention_dimension=256, additive_att_dimension=128, **kwargs):
        super(SemanticLookup, self).__init__(**kwargs)

        self.self_attention = GeneralizedPooling(segments_amount=1,
                                                 embedding_dimension=embedding_dimension,
                                                 attention_dimension=attention_dimension)

        self.memory_add_attention = AdditiveAttention(additive_att_dimension)
        self.clause_add_attention = AdditiveAttention(additive_att_dimension)

    def call(self, inputs, **kwargs):
        # [batch_size, segments_amount, embedding_dim]
        clause = inputs['clause']

        # [batch_size, memory_size, segments_amount, embedding_dim]
        memory = inputs['memory']
        memory_size = memory.shape[1]

        # [batch_size * memory_size, embedding_dim]
        reduced_clause = self.self_attention(clause)
        reduced_clause = tf.tile(reduced_clause, multiples=[1, memory_size, 1])
        reduced_clause = tf.reshape(reduced_clause, [-1, reduced_clause.shape[-1]])

        # [batch_size * memory_size, segments_amount, embedding_dim]
        memory = tf.reshape(memory, [-1, memory.shape[2], memory.shape[3]])

        # [batch_size * memory_size, embedding_dim]
        clause_to_memory_attention = self.memory_add_attention({
            'query': reduced_clause,
            'value': memory
        })

        # [batch_size * memory_size, segments_amount, embedding_dim]
        clause = tf.expand_dims(clause, axis=1)
        clause = tf.tile(clause, multiples=[1, memory_size, 1, 1])
        clause = tf.reshape(clause, [-1, clause.shape[2], clause.shape[3]])

        # [batch_size * memory_size, embedding_dim]
        memory_to_clause_attention = self.clause_add_attention({
            'query': clause_to_memory_attention,
            'value': clause
        })

        return memory_to_clause_attention


# Extraction

class SentenceMemoryExtraction(tf.keras.layers.Layer):

    def __init__(self, extraction_info, partial_supervision_info, padding_amount=None, **kwargs):
        super(SentenceMemoryExtraction, self).__init__(**kwargs)
        self.extraction_info = extraction_info
        self.partial_supervision_info = partial_supervision_info
        self.padding_amount = padding_amount

        self.supervision_loss = None
        self.memory_attention = None

    def _add_supervision_loss(self, prob_dist, positive_idxs, negative_idxs, mask_idxs):

        # Repeat mask for each positive element in each sample memory
        # Mask_idxs shape: [batch_size, padding_amount]
        # Mask res shape: [batch_size * padding_amount, padding_amount]
        mask_res = tf.tile(mask_idxs, multiples=[1, self.padding_amount])
        mask_res = tf.reshape(mask_res, [-1, self.padding_amount])

        # Split each similarity score for a target into a separate sample
        # similarities shape: [batch_size, memory_max_length]
        # positive_idxs shape: [batch_size, padding_amount, 2]
        # gather_nd shape: [batch_size, padding_amount]
        # pos_scores shape: [batch_size * padding_amount, 1]
        pos_scores = tf.gather_nd(prob_dist, positive_idxs)
        pos_scores = tf.reshape(pos_scores, [-1, 1])

        # Repeat similarity scores for non-target memories for each positive score
        # similarities shape: [batch_size, memory_max_length]
        # negative_idxs shape: [batch_size, padding_amount, 2]
        # neg_scores shape: [batch_size * padding_amount, padding_amount]
        neg_scores = tf.gather_nd(prob_dist, negative_idxs)
        neg_scores = tf.tile(neg_scores, multiples=[1, self.padding_amount])
        neg_scores = tf.reshape(neg_scores, [-1, self.padding_amount])

        # Compare each single positive score with all corresponding negative scores
        # [batch_size * padding_amount, padding_amount]
        # Samples without supervision are ignored by applying a zero mask (mask_res)
        hop_supervision_loss = tf.maximum(0., self.partial_supervision_info['margin'] - pos_scores + neg_scores)
        hop_supervision_loss = hop_supervision_loss * mask_res
        hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, self.padding_amount, self.padding_amount])
        hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=2)
        hop_supervision_loss = tf.reduce_min(hop_supervision_loss, axis=1)
        hop_supervision_loss = tf.reduce_sum(hop_supervision_loss)
        self.supervision_loss = hop_supervision_loss

    def call(self, inputs, training=False, mask=None, **kwargs):

        similarities = inputs['similarities']
        context_mask = inputs['context_mask']

        context_mask = tf.cast(context_mask, similarities.dtype)
        similarities += (context_mask * -1e9)

        if self.extraction_info['mode'] == 'softmax':
            probs = tf.nn.softmax(similarities, axis=1)
        elif self.extraction_info['mode'] == 'sparsemax':
            probs = tfa.activations.sparsemax(similarities, axis=1)
        elif self.extraction_info['mode'] == 'sigmoid':
            probs = tf.nn.sigmoid(similarities)
        else:
            raise RuntimeError('Invalid extraction mode! Got: {}'.format(self.extraction_info['mode']))

        self.memory_attention = probs

        if self.partial_supervision_info['flag']:
            self._add_supervision_loss(prob_dist=probs,
                                       positive_idxs=inputs['positive_indexes'],
                                       negative_idxs=inputs['negative_indexes'],
                                       mask_idxs=inputs['mask_indexes'])

        return probs


# Reasoning

class SentenceMemoryReasoning(tf.keras.layers.Layer):
    """
    Basic Memory Reasoning layer. The new query is computed simply by summing the content vector to current query
    """

    def __init__(self, reasoning_info, **kwargs):
        super(SentenceMemoryReasoning, self).__init__(**kwargs)
        self.reasoning_info = reasoning_info

    def call(self, inputs, **kwargs):
        query = inputs['query']
        memory_search = inputs['memory_search']

        if self.reasoning_info['mode'] == 'sum':
            upd_query = query + memory_search
        elif self.reasoning_info['mode'] == 'concat':
            upd_query = tf.concat((query, memory_search), axis=1)
        elif self.reasoning_info['mode'] == 'rnn':
            cell = tf.keras.layers.GRUCell(query.shape[-1])
            upq_query, _ = cell(memory_search, [query])
        elif self.reasoning_info['mode'] == 'mlp':
            upd_query = tf.keras.layers.Dense(query.shape[-1],
                                              activation=tf.nn.relu)(tf.concat((query, memory_search), axis=1))
        else:
            raise RuntimeError(
                'Invalid aggregation mode! Got: {} -- Supported: [sum, concat]'.format(self.reasoning_info['mode']))

        return upd_query


class CoAttentionMemoryReasoning(tf.keras.layers.Layer):
    """
    CoAttention Memory Reasoning layer. A Seq2Seq architecture is employed to compute query->content and
    content->query similarity scores. New query and memory cells are computed by weighting current ones via
    aforementioned similarity scores.
    """

    def __init__(self, enc_units, dec_units, att_units, **kwargs):
        super(CoAttentionMemoryReasoning, self).__init__(**kwargs)
        self.enc_units = enc_units
        self.dec_units = dec_units
        self.att_units = att_units

        self.query_attention = EncDecAttention(enc_units,
                                               dec_units,
                                               att_units)

        self.vector_attention = EncDecAttention(enc_units,
                                                dec_units,
                                                att_units)

    def compute_mask(self, inputs, mask=None):
        return mask

    def call(self, inputs, mask=None, compute_memory_scores=True, **kwargs):
        query = inputs[0]
        memory_vector = inputs[1]
        memory = inputs[2]
        similarity_probs = inputs[3]

        # Mask list: [query_mask, memory_mask_reduced]
        query_scores = self.query_attention([memory_vector, query])

        upd_query = query * query_scores

        if compute_memory_scores:
            memory_vector_scores = self.vector_attention([query, memory_vector])

            # [batch_size, max_length, 1, 1]
            memory_vector_scores = tf.expand_dims(memory_vector_scores, 1)

            upd_memory = memory * memory_vector_scores * similarity_probs

            return upd_query, upd_memory, memory_vector_scores
        else:
            return upd_query


class EncDecAttention(tf.keras.layers.Layer):
    """
    Seq2Seq architecture for query->content and content->query attention.
    """

    def __init__(self, enc_units, dec_units, att_units, **kwargs):
        super(EncDecAttention, self).__init__(**kwargs)
        self.enc_units = enc_units
        self.dec_units = dec_units
        self.att_units = att_units

    def compute_mask(self, inputs, mask):
        return mask

    def call(self, inputs, mask=None, **kwargs):
        # Masks order: [enc_mask, dec_mask]

        # [batch_size, enc_max_length, hidden_size]
        enc_input = inputs[0]

        # [batch_size, dec_max_length, hidden_size]
        dec_input = inputs[1]

        # We can compute lengths based on mask
        enc_lengths = tf.reduce_sum(tf.cast(mask[0], tf.int32), axis=1)
        dec_lengths = tf.reduce_sum(tf.cast(mask[1], tf.int32), axis=1)

        # Encoder
        encoder = tf.keras.layers.LSTM(self.enc_units, return_state=True)
        enc_outputs, enc_h, enc_c = encoder(enc_input)
        enc_state = [enc_h, enc_c]

        # Decoder
        decoder_cell = tf.keras.layers.LSTMCell(self.dec_units)
        projection_layer = tf.keras.layers.Dense(1)

        attention_mechanism = tfa.seq2seq.LuongAttention(self.att_units,
                                                         memory=enc_outputs,
                                                         memory_sequence_length=enc_lengths)
        decoder_cell = tfa.seq2seq.AttentionWrapper(decoder_cell,
                                                    attention_mechanism,
                                                    attention_layer_size=self.att_units)

        dec_init_state = decoder_cell.get_initial_state(dec_input)
        dec_init_state = dec_init_state.clone(cell_state=enc_state)

        sampler = tfa.seq2seq.sampler.TrainingSampler()

        decoder = tfa.seq2seq.BasicDecoder(
            decoder_cell, sampler, output_layer=projection_layer)

        dec_scores, _, _ = decoder(dec_input,
                                   initial_state=dec_init_state,
                                   sequence_length=dec_lengths)

        # [batch_size, dec_real_max_length, 1]
        dec_scores = dec_scores.rnn_output

        # Adding padding scores in order to reach dec_max_length
        if dec_scores.shape[1] < dec_input.shape[1]:
            padding_scores = tf.zeros(shape=(dec_scores.shape[0], dec_input.shape[1] - dec_scores.shape[1], 1))
            dec_scores = tf.concat((dec_scores, padding_scores), axis=1)

        # [batch_size, dec_max_length, 1]
        return dec_scores


class SemanticTransformerReasoning(tf.keras.layers.Layer):

    def __init__(self, reduction_weights=None, l2_regularization=0., accumulate_attention=False, **kwargs):
        super(SemanticTransformerReasoning, self).__init__(**kwargs)

        self.accumulate_attention = accumulate_attention
        self.query_attention = None

        if reduction_weights is None:
            self.reductions = [tf.keras.layers.Dense(units=1,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))]
        else:
            self.reductions = [tf.keras.layers.Dense(units=weight,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
                               for weight in reduction_weights]

    def call(self, inputs, **kwargs):

        # [batch_size * memory_size, query_size, hidden_size]
        memory_search = inputs['memory_search']

        # [batch_size * memory_size, query_size, embedding_dim]
        query = inputs['query']

        # [batch_size * memory_size, query_size, 1]
        query_scores = memory_search
        for reduction in self.reductions:
            query_scores = reduction(query_scores)

        # [batch_size * memory_size, query_size, 1]
        query_scores = tf.squeeze(query_scores)
        query_scores = tf.nn.softmax(query_scores, axis=1)

        if self.accumulate_attention:
            self.query_attention = query_scores

        query_scores = tf.expand_dims(query_scores, -1)

        # [batch_size * memory_size, query_size, embedding_dim]
        upd_query = query_scores * query

        return upd_query


# Transformer Layers
# All these layers were taken from https://www.tensorflow.org/tutorials/text/transformer#scaled_dot_product_attention

class MultiHeadAttention(tf.keras.layers.Layer):

    def __init__(self, embedding_dimension, num_heads):
        super(MultiHeadAttention, self).__init__()
        self.num_heads = num_heads
        self.d_model = embedding_dimension

        assert embedding_dimension % self.num_heads == 0

        self.depth = embedding_dimension // self.num_heads

        # TODO: customize for multiple layers
        self.wq = tf.keras.layers.Dense(embedding_dimension)
        self.wk = tf.keras.layers.Dense(embedding_dimension)
        self.wv = tf.keras.layers.Dense(embedding_dimension)

        self.dense = tf.keras.layers.Dense(embedding_dimension)

    def split_heads(self, x, batch_size):
        """
        Split the last dimension into (num_heads, depth).
        Transpose the result such that the shape is (batch_size, num_heads, seq_len, depth)
        """

        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, v, k, q, mask):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, d_model)
        k = self.wk(k)  # (batch_size, seq_len, d_model)
        v = self.wv(v)  # (batch_size, seq_len, d_model)

        q = self.split_heads(q, batch_size)  # (batch_size, num_heads, seq_len_q, depth)
        k = self.split_heads(k, batch_size)  # (batch_size, num_heads, seq_len_k, depth)
        v = self.split_heads(v, batch_size)  # (batch_size, num_heads, seq_len_v, depth)

        # scaled_attention.shape == (batch_size, num_heads, seq_len_q, depth)
        # attention_weights.shape == (batch_size, num_heads, seq_len_q, seq_len_k)
        scaled_attention, attention_weights = scaled_dot_product_attention(
            q, k, v, mask)

        scaled_attention = tf.transpose(scaled_attention,
                                        perm=[0, 2, 1, 3])  # (batch_size, seq_len_q, num_heads, depth)

        concat_attention = tf.reshape(scaled_attention,
                                      (batch_size, -1, self.d_model))  # (batch_size, seq_len_q, d_model)

        output = self.dense(concat_attention)  # (batch_size, seq_len_q, d_model)

        return output, attention_weights


class Encoder(tf.keras.layers.Layer):

    def __init__(self, num_layers, embedding_dimension,
                 num_heads, dff, dropout_rate=0.1):
        super(Encoder, self).__init__()

        self.num_layers = num_layers

        self.enc_layers = [EncoderLayer(embedding_dimension, num_heads, dff, dropout_rate)
                           for _ in range(num_layers)]

    # The input of the encoder is the concatenation of all memory sentences [batch_size, max_length]
    def call(self, x, training, mask):
        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training, mask)

        # (batch_size, input_seq_len, embedding_dimension)
        return x


class EncoderLayer(tf.keras.layers.Layer):
    def __init__(self, embedding_dimension, num_heads, dff, dropout_rate=0.1):
        super(EncoderLayer, self).__init__()

        self.mha = MultiHeadAttention(embedding_dimension, num_heads)
        self.ffn = point_wise_feed_forward_network(embedding_dimension, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(dropout_rate)
        self.dropout2 = tf.keras.layers.Dropout(dropout_rate)

    def call(self, x, training, mask):
        # self-attention
        attn_output, _ = self.mha(x, x, x, mask)  # (batch_size, input_seq_len, d_model)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(x + attn_output)  # (batch_size, input_seq_len, d_model)

        # (batch_size, input_seq_len, d_model)
        ffn_output = out1
        for layer in self.ffn:
            ffn_output = layer(ffn_output)
        ffn_output = self.dropout2(ffn_output, training=training)
        out2 = self.layernorm2(out1 + ffn_output)  # (batch_size, input_seq_len, d_model)

        return out2


class Decoder(tf.keras.layers.Layer):

    def __init__(self, num_layers, embedding_dimension,
                 num_heads, dff, dropout_rate=0.1):
        super(Decoder, self).__init__()

        self.num_layers = num_layers

        self.dec_layers = [DecoderLayer(embedding_dimension, num_heads, dff, dropout_rate)
                           for _ in range(num_layers)]

    def call(self, x, enc_output, training, look_ahead_mask, padding_mask):
        for i in range(self.num_layers):
            x, block1, block2 = self.dec_layers[i](x, enc_output, training,
                                                   look_ahead_mask, padding_mask)

        # x.shape == (batch_size, target_seq_len, d_model)
        return x


class DecoderLayer(tf.keras.layers.Layer):
    def __init__(self, embedding_dimension, num_heads, dff, dropout_rate=0.1):
        super(DecoderLayer, self).__init__()

        self.mha1 = MultiHeadAttention(embedding_dimension, num_heads)
        self.mha2 = MultiHeadAttention(embedding_dimension, num_heads)

        self.ffn = point_wise_feed_forward_network(embedding_dimension, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(dropout_rate)
        self.dropout2 = tf.keras.layers.Dropout(dropout_rate)
        self.dropout3 = tf.keras.layers.Dropout(dropout_rate)

    def call(self, x, enc_output, training, look_ahead_mask, padding_mask):
        # enc_output.shape == (batch_size, input_seq_len, d_model)

        # Self-attention restricted to past tokens only
        attn1, attn_weights_block1 = self.mha1(x, x, x, look_ahead_mask)  # (batch_size, target_seq_len, d_model)
        attn1 = self.dropout1(attn1, training=training)
        out1 = self.layernorm1(attn1 + x)

        # Attention on x given encoder output
        attn2, attn_weights_block2 = self.mha2(
            enc_output, enc_output, out1, padding_mask)  # (batch_size, target_seq_len, d_model)
        attn2 = self.dropout2(attn2, training=training)
        out2 = self.layernorm2(attn2 + out1)  # (batch_size, target_seq_len, d_model)

        # (batch_size, target_seq_len, d_model)
        ffn_output = out2
        for layer in self.ffn:
            ffn_output = layer(ffn_output)
        ffn_output = self.dropout3(ffn_output, training=training)
        out3 = self.layernorm3(ffn_output + out2)  # (batch_size, target_seq_len, d_model)

        return out3, attn_weights_block1, attn_weights_block2


# ELMo

# This is impossible to use since its too much memory demanding
class ElmoEmbeddingLayer(tf.keras.layers.Layer):
    """
    Simple ELMo model wrapper.
    """

    def __init__(self, trainable=True, output_signature='elmo', **kwargs):
        super(ElmoEmbeddingLayer, self).__init__(**kwargs)
        self.trainable = trainable
        self.output_signature = output_signature
        self.dimension = 1024

    def build(self, input_shape):
        self.elmo = hub.load('https://tfhub.dev/google/elmo/2')

        if self.trainable:
            # Trainable weights are restricted to a limited set
            # as described in the paper: https://tfhub.dev/google/elmo/2
            self._trainable_weights += self.elmo.variables[-2:]

        super(ElmoEmbeddingLayer, self).build(input_shape)

    def call(self, x, mask=None):
        result = self.elmo.signatures['default'](x)

        # [batch_size, max_sequence_length, 1024] if output_signature == 'elmo'
        # [batch_size, 1024] if output_signature == 'default
        return result[self.output_signature]

    def compute_mask(self, inputs, mask=None):
        return tf.not_equal(inputs, 0)


# BERT

# This is impossible to use since its too much memory demanding
class BERTEmbeddingLayer(tf.keras.layers.Layer):
    """
    Simple BERT model wrapper.
    """

    def __init__(self, output_signature='sequence_output', **kwargs):
        super(BERTEmbeddingLayer, self).__init__(**kwargs)
        self.output_signature = output_signature
        self.dimension = 768

    def build(self, input_shape):
        self.bert = hub.load('https://tfhub.dev/google/bert_uncased_L-12_H-768_A-12/1', tags='train')

        # see Parameter Efficient Transfer Learning for NLP for a variant of BERT fine-tuning.
        # However, it requires an ad-hoc model with adapter modules (adapter-BERT)
        # Remove cls variables of BERT
        bert_trainable_variables = [item for item in self.bert.variables if 'cls' not in item.name.lower()]
        self._trainable_weights += bert_trainable_variables
        super(BERTEmbeddingLayer, self).build(input_shape)

    def call(self, inputs, mask=None):
        return self.bert.signatures['tokens'](**inputs)[self.output_signature]

    def compute_mask(self, inputs, mask=None):
        return tf.not_equal(inputs['input_ids'], 0)

    def compute_output_shape(self, input_shape):
        if self.output_signature == 'sequence_output':
            return input_shape + (self.dimension,)
        else:
            return input_shape[0], self.dimension


class ALBERTEmbeddingLayer(tf.keras.layers.Layer):
    """
    Simple ALBERT model wrapper.
    """

    def __init__(self, embedding_mode='sentence',
                 albert_model='https://tfhub.dev/google/albert_base/2', **kwargs):
        super(ALBERTEmbeddingLayer, self).__init__(**kwargs)
        if embedding_mode == 'sentence':
            self.output_signature = 'pooled_output'
        else:
            self.output_signature = 'sequence_output'
        self.albert_model = albert_model
        self.dimension = 768

    def build(self, input_shape):
        self.bert = hub.load(self.albert_model, tags='train')

        # see Parameter Efficient Transfer Learning for NLP for a variant of BERT fine-tuning.
        # However, it requires an ad-hoc model with adapter modules (adapter-BERT)
        # Remove cls variables of BERT
        bert_trainable_variables = [item for item in self.bert.variables if 'cls' not in item.name.lower()]
        self._trainable_weights += bert_trainable_variables
        super(ALBERTEmbeddingLayer, self).build(input_shape)

    def call(self, inputs, mask=None):
        return self.bert.signatures['tokens'](**inputs)[self.output_signature]

    def compute_mask(self, inputs, mask=None):
        return tf.not_equal(inputs['input_ids'], 0)

    def compute_output_shape(self, input_shape):
        if self.output_signature == 'sequence_output':
            return input_shape + (self.dimension,)
        else:
            return input_shape[0], self.dimension


# Differentiable Logic Unification (DLU)

# TODO: move to tensorflow_utils_v2
def sparse_softmax(tensor, mask):
    """
    Sparse softmax that ignores padding values (0s).
    """

    indexes = tf.where(mask)
    sparse_tensor = tf.SparseTensor(indexes, tf.gather_nd(tensor, indexes), tensor.get_shape())
    sparse_probs = tf.sparse.softmax(sparse_tensor)
    dense_probs = tf.sparse.to_dense(sparse_probs)

    return dense_probs


@DeprecationWarning
class DLUParallelModule(tf.keras.layers.Layer):
    """

    Takes care of the decoding step only.
    1 Encoder -> |V| Decoders that will be merged. (quite inefficient)

    """

    def __init__(self, hidden_size, **kwargs):
        super(DLUParallelModule, self).__init__(**kwargs)
        self.hidden_size = hidden_size

    def call(self, inputs, **kwargs):
        # [batch_size * memory_size, enc_max_length, hidden_size]
        encoder_outputs = inputs['encoder_outputs']

        # [[batch_size * memory_size, hidden_size], [batch_size * memory_size, hidden_size]]
        encoder_state = inputs['encoder_state']

        # [batch_size * memory_size, dec_max_length]
        decoder_unification_mask = inputs['decoder_unification_mask']

        # [batch_size * memory_size, enc_max_length]
        encoder_unification_mask = inputs['encoder_unification_mask']

        # [batch_size * memory_size, ]
        encoder_lengths = inputs['encoder_lengths']

        # [batch_size * memory_size, dec_max_length, embedding_dim]
        decoder_input = inputs['decoder_input']

        # [batch_size * memory_size, ]
        decoder_lengths = inputs['decoder_lengths']

        decoder_cell = tf.keras.layers.LSTMCell(self.hidden_size)
        attention_mechanism = DLULuongAttention(units=self.hidden_size,
                                                memory=encoder_outputs,
                                                memory_sequence_length=encoder_lengths,
                                                memory_unification_mask=encoder_unification_mask)
        decoder_cell = DLUAttentionWrapper(cell=decoder_cell,
                                           output_attention=False,
                                           attention_mechanism=attention_mechanism)
        dec_init_state = decoder_cell.get_initial_state(decoder_input)
        dec_init_state = dec_init_state.clone(cell_state=encoder_state)

        sampler = tfa.seq2seq.sampler.TrainingSampler()

        decoder = tfa.seq2seq.BasicDecoder(decoder_cell, sampler)
        decoder_unification_mask = tf.expand_dims(decoder_unification_mask, -1)
        decoder_unification_mask = tf.tile(decoder_unification_mask, [1, 1, decoder_input.shape[-1]])
        unification_output, _, _ = decoder([decoder_input, decoder_unification_mask],
                                           initial_state=dec_init_state,
                                           sequence_length=decoder_lengths)

        # [batch_size * memory_size, dec_real_max_length, hidden_size]
        unification_output = unification_output.rnn_output

        if unification_output.shape[1] < decoder_input.shape[1]:
            padding_scores = tf.zeros(
                shape=(unification_output.shape[0], decoder_input.shape[1] - unification_output.shape[1], 1))
            unification_output = tf.concat((unification_output, padding_scores), axis=1)

        # [batch_size * memory_size, dec_max_length, hidden_size]
        return unification_output


@DeprecationWarning
class DLUAttentionWrapper(tfa.seq2seq.AttentionWrapper):

    def __init__(self, **kwargs):
        super(DLUAttentionWrapper, self).__init__(**kwargs)
        self._cell_input_fn = None

    def call(self, inputs, state, **kwargs):
        rnn_input = inputs[0]
        unification_mask = inputs[1]

        if not isinstance(state, tfa.seq2seq.AttentionWrapperState):
            raise TypeError(
                "Expected state to be instance of AttentionWrapperState. "
                "Received type %s instead." % type(state))

        # We do not need to concatenate previous attention with inputs
        cell_state = state.cell_state
        cell_output, next_cell_state = self._cell(rnn_input, cell_state,
                                                  **kwargs)

        cell_batch_size = (tf.compat.dimension_value(cell_output.shape[0])
                           or tf.shape(cell_output)[0])
        error_message = (
                "When applying AttentionWrapper %s: " % self.name +
                "Non-matching batch sizes between the memory "
                "(encoder output) and the query (decoder output).  Are you using "
                "the BeamSearchDecoder?  You may need to tile your memory input "
                "via the tfa.seq2seq.tile_batch function with argument "
                "multiple=beam_width.")
        with tf.control_dependencies(
                self._batch_size_checks(cell_batch_size, error_message)):  # pylint: disable=bad-continuation
            cell_output = tf.identity(cell_output, name="checked_cell_output")

        if self._is_multi:
            previous_attention_state = state.attention_state
            previous_alignment_history = state.alignment_history
        else:
            previous_attention_state = [state.attention_state]
            previous_alignment_history = [state.alignment_history]

        all_alignments = []
        all_attentions = []
        all_attention_states = []
        maybe_all_histories = []
        for i, attention_mechanism in enumerate(self._attention_mechanisms):
            attention, alignments, next_attention_state = self._attention_fn(
                attention_mechanism, cell_output, previous_attention_state[i],
                self._attention_layers[i] if self._attention_layers else None)
            alignment_history = previous_alignment_history[i].write(
                state.time, alignments) if self._alignment_history else ()

            all_attention_states.append(next_attention_state)
            all_alignments.append(alignments)
            all_attentions.append(attention)
            maybe_all_histories.append(alignment_history)

        attention = tf.concat(all_attentions, 1)
        next_state = tfa.seq2seq.AttentionWrapperState(
            time=state.time + 1,
            cell_state=next_cell_state,
            attention=attention,
            attention_state=self._item_or_tuple(all_attention_states),
            alignments=self._item_or_tuple(all_alignments),
            alignment_history=self._item_or_tuple(maybe_all_histories))

        # Compute unification output
        if self._output_attention:
            return attention, next_state
        else:
            to_output = rnn_input * (1 - unification_mask) + unification_mask * attention
            return to_output, next_state


def _prepare_memory(memory,
                    memory_sequence_length=None,
                    memory_mask=None,
                    check_inner_dims_defined=True):
    """Convert to tensor and possibly mask `memory`.

    Args:
      memory: `Tensor`, shaped `[batch_size, max_time, ...]`.
      memory_sequence_length: `int32` `Tensor`, shaped `[batch_size]`.
      memory_mask: `boolean` tensor with shape [batch_size, max_time]. The
        memory should be skipped when the corresponding mask is False.
      check_inner_dims_defined: Python boolean.  If `True`, the `memory`
        argument's shape is checked to ensure all but the two outermost
        dimensions are fully defined.

    Returns:
      A (possibly masked), checked, new `memory`.

    Raises:
      ValueError: If `check_inner_dims_defined` is `True` and not
        `memory.shape[2:].is_fully_defined()`.
    """
    memory = tf.nest.map_structure(
        lambda m: tf.convert_to_tensor(m, name="memory"), memory)
    if memory_sequence_length is not None and memory_mask is not None:
        raise ValueError(
            "memory_sequence_length and memory_mask can't be provided "
            "at same time.")
    if memory_sequence_length is not None:
        memory_sequence_length = tf.convert_to_tensor(
            memory_sequence_length, name="memory_sequence_length")
    if check_inner_dims_defined:

        def _check_dims(m):
            if not m.get_shape()[2:].is_fully_defined():
                raise ValueError(
                    "Expected memory %s to have fully defined inner dims, "
                    "but saw shape: %s" % (m.name, m.get_shape()))

        tf.nest.map_structure(_check_dims, memory)
    if memory_sequence_length is None and memory_mask is None:
        return memory
    elif memory_sequence_length is not None:
        seq_len_mask = tf.sequence_mask(
            memory_sequence_length,
            maxlen=tf.shape(tf.nest.flatten(memory)[0])[1],
            dtype=tf.nest.flatten(memory)[0].dtype)
    else:
        # For memory_mask is not None
        seq_len_mask = tf.cast(
            memory_mask, dtype=tf.nest.flatten(memory)[0].dtype)

    def _maybe_mask(m, seq_len_mask):
        """Mask the memory based on the memory mask."""
        rank = m.get_shape().ndims
        rank = rank if rank is not None else tf.rank(m)
        extra_ones = tf.ones(rank - 2, dtype=tf.int32)
        seq_len_mask = tf.reshape(
            seq_len_mask, tf.concat((tf.shape(seq_len_mask), extra_ones), 0))
        return m * seq_len_mask

    return tf.nest.map_structure(lambda m: _maybe_mask(m, seq_len_mask),
                                 memory)


def _maybe_mask_score(score,
                      memory_sequence_length=None,
                      memory_mask=None,
                      score_mask_value=None):
    """Mask the attention score based on the masks."""
    if memory_sequence_length is None and memory_mask is None:
        return score
    if memory_sequence_length is not None and memory_mask is not None:
        raise ValueError(
            "memory_sequence_length and memory_mask can't be provided "
            "at same time.")
    if memory_sequence_length is not None:
        message = ("All values in memory_sequence_length must greater than "
                   "zero.")
        with tf.control_dependencies([
            tf.compat.v1.assert_positive(  # pylint: disable=bad-continuation
                memory_sequence_length,
                message=message)
        ]):
            memory_mask = tf.sequence_mask(
                memory_sequence_length, maxlen=tf.shape(score)[1])
    score_mask_values = score_mask_value * tf.ones_like(score)
    memory_mask = tf.cast(memory_mask, tf.bool)
    return tf.where(memory_mask, score, score_mask_values)


def hardmax(logits, name=None):
    """Returns batched one-hot vectors.

    The depth index containing the `1` is that of the maximum logit value.

    Args:
      logits: A batch tensor of logit values.
      name: Name to use when creating ops.
    Returns:
      A batched one-hot tensor.
    """
    with tf.name_scope(name or "Hardmax"):
        logits = tf.convert_to_tensor(logits, name="logits")
        if tf.compat.dimension_value(logits.get_shape()[-1]) is not None:
            depth = tf.compat.dimension_value(logits.get_shape()[-1])
        else:
            depth = tf.shape(logits)[-1]
        return tf.one_hot(tf.argmax(logits, -1), depth, dtype=logits.dtype)


@DeprecationWarning
class DLUAttention(tfa.seq2seq.AttentionMechanism, tf.keras.layers.Layer):

    def __init__(self,
                 memory_unification_mask,
                 memory,
                 probability_fn,
                 query_layer=None,
                 memory_layer=None,
                 memory_sequence_length=None,
                 **kwargs):

        if (query_layer is not None
                and not isinstance(query_layer, tf.keras.layers.Layer)):
            raise TypeError(
                "query_layer is not a Layer: %s" % type(query_layer).__name__)
        if (memory_layer is not None
                and not isinstance(memory_layer, tf.keras.layers.Layer)):
            raise TypeError("memory_layer is not a Layer: %s" %
                            type(memory_layer).__name__)
        self.query_layer = query_layer
        self.memory_layer = memory_layer
        if self.memory_layer is not None and "dtype" not in kwargs:
            kwargs["dtype"] = self.memory_layer.dtype
        super(DLUAttention, self).__init__(**kwargs)
        if not callable(probability_fn):
            raise TypeError("probability_fn must be callable, saw type: %s" %
                            type(probability_fn).__name__)
        self.default_probability_fn = probability_fn
        self.probability_fn = probability_fn

        self.keys = None
        self.values = None
        self.batch_size = None
        self._memory_initialized = False
        self._check_inner_dims_defined = True
        self.supports_masking = True

        if memory is not None:
            # Setup the memory by self.__call__() with memory and
            # memory_seq_length. This will make the attention follow the keras
            # convention which takes all the tensor inputs via __call__().
            if memory_sequence_length is None:
                inputs = [memory, memory_unification_mask]
            else:
                inputs = [memory, memory_unification_mask, memory_sequence_length]

            self.values = super(DLUAttention, self).__call__(
                inputs, setup_memory=True)

    def build(self, input_shape):
        if not self._memory_initialized:
            # This is for setting up the memory, which contains memory and
            # optional memory_sequence_length. Build the memory_layer with
            # memory shape.
            if self.memory_layer is not None and not self.memory_layer.built:
                if isinstance(input_shape, list):
                    self.memory_layer.build(input_shape[0])
                else:
                    self.memory_layer.build(input_shape)
        else:
            # The input_shape should be query.shape and state.shape. Use the
            # query to init the query layer.
            if self.query_layer is not None and not self.query_layer.built:
                self.query_layer.build(input_shape[0])

    def __call__(self, inputs, **kwargs):
        """Preprocess the inputs before calling `base_layer.__call__()`.

        Note that there are situation here, one for setup memory, and one with
        actual query and state.
        1. When the memory has not been configured, we just pass all the param
           to base_layer.__call__(), which will then invoke self.call() with
           proper inputs, which allows this class to setup memory.
        2. When the memory has already been setup, the input should contain
           query and state, and optionally processed memory. If the processed
           memory is not included in the input, we will have to append it to
           the inputs and give it to the base_layer.__call__(). The processed
           memory is the output of first invocation of self.__call__(). If we
           don't add it here, then from keras perspective, the graph is
           disconnected since the output from previous call is never used.

        Args:
          inputs: the inputs tensors.
          **kwargs: dict, other keyeword arguments for the `__call__()`
        """
        if self._memory_initialized:
            inputs = [inputs, kwargs['state']]
            if len(inputs) not in (2, 3):
                raise ValueError(
                    "Expect the inputs to have 2 or 3 tensors, got %d" %
                    len(inputs))
            if len(inputs) == 2:
                # We append the calculated memory here so that the graph will be
                # connected.
                inputs.append(self.values)
        return super(DLUAttention, self).__call__(inputs, **kwargs)

    def setup_memory(self,
                     memory,
                     memory_sequence_length=None,
                     memory_mask=None):
        """Pre-process the memory before actually query the memory.

        This should only be called once at the first invocation of call().

        Args:
          memory: The memory to query; usually the output of an RNN encoder.
            This tensor should be shaped `[batch_size, max_time, ...]`.
          memory_sequence_length (optional): Sequence lengths for the batch
            entries in memory. If provided, the memory tensor rows are masked
            with zeros for values past the respective sequence lengths.
          memory_mask: (Optional) The boolean tensor with shape `[batch_size,
            max_time]`. For any value equal to False, the corresponding value
            in memory should be ignored.
        """
        if memory_sequence_length is not None and memory_mask is not None:
            raise ValueError(
                "memory_sequence_length and memory_mask cannot be "
                "used at same time for attention.")
        with tf.name_scope(self.name or "BaseAttentionMechanismInit"):
            self.values = _prepare_memory(
                memory,
                memory_sequence_length=memory_sequence_length,
                memory_mask=memory_mask,
                check_inner_dims_defined=self._check_inner_dims_defined)
            # Mark the value as check since the memory and memory mask might not
            # passed from __call__(), which does not have proper keras metadata.
            # TODO(omalleyt12): Remove this hack once the mask the has proper
            # keras history.
            base_layer_utils.mark_checked(self.values)
            if self.memory_layer is not None:
                self.keys = self.memory_layer(self.values)
            else:
                self.keys = self.values
            self.batch_size = (tf.compat.dimension_value(self.keys.shape[0])
                               or tf.shape(self.keys)[0])
            self._alignments_size = (tf.compat.dimension_value(
                self.keys.shape[1]) or tf.shape(self.keys)[1])
            if memory_mask is not None or memory_sequence_length is not None:
                unwrapped_probability_fn = self.default_probability_fn

                # Sparsemax since we are masking many values
                def _mask_probability_fn(score, prev):
                    masked_scores = _maybe_mask_score(
                        score,
                        memory_mask=memory_mask,
                        memory_sequence_length=memory_sequence_length,
                        score_mask_value=score.dtype.min)
                    return sparse_softmax(masked_scores, memory_mask)

                self.probability_fn = _mask_probability_fn
        self._memory_initialized = True

    def _calculate_attention(self, query, state):
        raise NotImplementedError(
            "_calculate_attention need to be implemented by subclasses.")

    def compute_mask(self, inputs, mask=None):
        # There real input of the attention is query and state, and the memory
        # layer mask shouldn't be pass down. Returning None for all output mask
        # here.
        return None, None

    def get_config(self):
        config = {}
        # Since the probability_fn is likely to be a wrapped function, the child
        # class should preserve the original function and how its wrapped.

        if self.query_layer is not None:
            config["query_layer"] = {
                "class_name": self.query_layer.__class__.__name__,
                "config": self.query_layer.get_config(),
            }
        if self.memory_layer is not None:
            config["memory_layer"] = {
                "class_name": self.memory_layer.__class__.__name__,
                "config": self.memory_layer.get_config(),
            }
        # memory is a required init parameter and its a tensor. It cannot be
        # serialized to config, so we put a placeholder for it.
        config["memory"] = None
        base_config = super(DLUAttention, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def _process_probability_fn(self, func_name):
        """Helper method to retrieve the probably function by string input."""
        valid_probability_fns = {
            "softmax": tf.nn.softmax,
            "hardmax": hardmax,
        }
        if func_name not in valid_probability_fns.keys():
            raise ValueError("Invalid probability function: %s, options are %s"
                             % (func_name, valid_probability_fns.keys()))
        return valid_probability_fns[func_name]

    @classmethod
    def deserialize_inner_layer_from_config(cls, config, custom_objects):
        """Helper method that reconstruct the query and memory from the config.

        In the get_config() method, the query and memory layer configs are
        serialized into dict for persistence, this method perform the reverse
        action to reconstruct the layer from the config.

        Args:
          config: dict, the configs that will be used to reconstruct the
            object.
          custom_objects: dict mapping class names (or function names) of
            custom (non-Keras) objects to class/functions.
        Returns:
          config: dict, the config with layer instance created, which is ready
            to be used as init parameters.
        """
        # Reconstruct the query and memory layer for parent class.
        # Instead of updating the input, create a copy and use that.
        config = config.copy()
        query_layer_config = config.pop("query_layer", None)
        if query_layer_config:
            query_layer = tf.keras.layers.deserialize(
                query_layer_config, custom_objects=custom_objects)
            config["query_layer"] = query_layer
        memory_layer_config = config.pop("memory_layer", None)
        if memory_layer_config:
            memory_layer = tf.keras.layers.deserialize(
                memory_layer_config, custom_objects=custom_objects)
            config["memory_layer"] = memory_layer
        return config

    @property
    def alignments_size(self):
        return self._alignments_size

    @property
    def state_size(self):
        return self._alignments_size

    def initial_alignments(self, batch_size, dtype):
        """Creates the initial alignment values for the `AttentionWrapper`
        class.

        This is important for AttentionMechanisms that use the previous
        alignment to calculate the alignment at the next time step
        (e.g. monotonic attention).

        The default behavior is to return a tensor of all zeros.

        Args:
          batch_size: `int32` scalar, the batch_size.
          dtype: The `dtype`.

        Returns:
          A `dtype` tensor shaped `[batch_size, alignments_size]`
          (`alignments_size` is the values' `max_time`).
        """
        max_time = self._alignments_size
        return _zero_state_tensors(max_time, batch_size, dtype)

    def initial_state(self, batch_size, dtype):
        """Creates the initial state values for the `AttentionWrapper` class.

        This is important for AttentionMechanisms that use the previous
        alignment to calculate the alignment at the next time step
        (e.g. monotonic attention).

        The default behavior is to return the same output as
        initial_alignments.

        Args:
          batch_size: `int32` scalar, the batch_size.
          dtype: The `dtype`.

        Returns:
          A structure of all-zero tensors with shapes as described by
          `state_size`.
        """
        return self.initial_alignments(batch_size, dtype)

    def call(self, inputs, mask=None, setup_memory=False, **kwargs):
        if setup_memory:
            if isinstance(inputs, list):
                if len(inputs) not in (2, 3):
                    raise ValueError(
                        "Expect inputs to have 2 or 3 tensors, got %d" %
                        len(inputs))
                memory = inputs[0]
                unification_mask = inputs[1]
                memory_sequence_length = inputs[2] if len(
                    inputs) == 3 else None
                if mask is None:
                    memory_mask = unification_mask
                else:
                    memory_mask = mask * unification_mask
            else:
                memory, unification_mask, memory_sequence_length = inputs[0], inputs[1], None
                if mask is None:
                    memory_mask = unification_mask
                else:
                    memory_mask = mask * unification_mask
            self.setup_memory(memory, None, memory_mask)
            # We force the self.built to false here since only memory is,
            # initialized but the real query/state has not been call() yet. The
            # layer should be build and call again.
            self.built = False
            # Return the processed memory in order to create the Keras
            # connectivity data for it.
            return self.values
        else:
            if not self._memory_initialized:
                raise ValueError(
                    "Cannot query the attention before the setup of "
                    "memory")
            if len(inputs) not in (2, 3):
                raise ValueError(
                    "Expect the inputs to have query, state, and optional "
                    "processed memory, got %d items" % len(inputs))
            # Ignore the rest of the inputs and only care about the query and
            # state
            query, state = inputs[0], inputs[1]
            return self._calculate_attention(query, state)


def _luong_score(query, keys, scale):
    """Implements Luong-style (multiplicative) scoring function.

    This attention has two forms.  The first is standard Luong attention,
    as described in:

    Minh-Thang Luong, Hieu Pham, Christopher D. Manning.
    "Effective Approaches to Attention-based Neural Machine Translation."
    EMNLP 2015.  https://arxiv.org/abs/1508.04025

    The second is the scaled form inspired partly by the normalized form of
    Bahdanau attention.

    To enable the second form, call this function with `scale=True`.

    Args:
      query: Tensor, shape `[batch_size, num_units]` to compare to keys.
      keys: Processed memory, shape `[batch_size, max_time, num_units]`.
      scale: the optional tensor to scale the attention score.

    Returns:
      A `[batch_size, max_time]` tensor of unnormalized score values.

    Raises:
      ValueError: If `key` and `query` depths do not match.
    """
    depth = query.get_shape()[-1]
    key_units = keys.get_shape()[-1]
    if depth != key_units:
        raise ValueError(
            "Incompatible or unknown inner dimensions between query and keys. "
            "Query (%s) has units: %s.  Keys (%s) have units: %s.  "
            "Perhaps you need to set num_units to the keys' dimension (%s)?" %
            (query, depth, keys, key_units, key_units))

    # Reshape from [batch_size, depth] to [batch_size, 1, depth]
    # for matmul.
    query = tf.expand_dims(query, 1)

    # Inner product along the query units dimension.
    # matmul shapes: query is [batch_size, 1, depth] and
    #                keys is [batch_size, max_time, depth].
    # the inner product is asked to **transpose keys' inner shape** to get a
    # batched matmul on:
    #   [batch_size, 1, depth] . [batch_size, depth, max_time]
    # resulting in an output shape of:
    #   [batch_size, 1, max_time].
    # we then squeeze out the center singleton dimension.
    score = tf.matmul(query, keys, transpose_b=True)
    score = tf.squeeze(score, [1])

    if scale is not None:
        score = scale * score
    return score


@DeprecationWarning
class DLULuongAttention(DLUAttention):

    def __init__(self,
                 units,
                 memory=None,
                 memory_sequence_length=None,
                 scale=False,
                 probability_fn="softmax",
                 dtype=None,
                 name="DLULuongAttention",
                 **kwargs):
        """Construct the AttentionMechanism mechanism.

        Args:
          units: The depth of the attention mechanism.
          memory: The memory to query; usually the output of an RNN encoder.
            This tensor should be shaped `[batch_size, max_time, ...]`.
          memory_sequence_length: (optional): Sequence lengths for the batch
            entries in memory.  If provided, the memory tensor rows are masked
            with zeros for values past the respective sequence lengths.
          scale: Python boolean. Whether to scale the energy term.
          probability_fn: (optional) string, the name of function to convert
            the attention score to probabilities. The default is `softmax`
            which is `tf.nn.softmax`. Other options is `hardmax`, which is
            hardmax() within this module. Any other value will result
            intovalidation error. Default to use `softmax`.
          dtype: The data type for the memory layer of the attention mechanism.
          name: Name to use when creating ops.
          **kwargs: Dictionary that contains other common arguments for layer
            creation.
        """
        # For LuongAttention, we only transform the memory layer; thus
        # num_units **must** match expected the query depth.
        self.probability_fn_name = probability_fn
        probability_fn = self._process_probability_fn(self.probability_fn_name)
        wrapped_probability_fn = lambda score, _: probability_fn(score)
        if dtype is None:
            dtype = tf.float32
        memory_layer = kwargs.pop("memory_layer", None)
        if not memory_layer:
            memory_layer = tf.keras.layers.Dense(
                units, name="memory_layer", use_bias=False, dtype=dtype)
        self.units = units
        self.scale = scale
        self.scale_weight = None
        super(DLULuongAttention, self).__init__(
            memory=memory,
            memory_sequence_length=memory_sequence_length,
            query_layer=None,
            memory_layer=memory_layer,
            probability_fn=wrapped_probability_fn,
            name=name,
            dtype=dtype,
            **kwargs)

    def build(self, input_shape):
        super(DLULuongAttention, self).build(input_shape)
        if self.scale and self.scale_weight is None:
            self.scale_weight = self.add_weight(
                "attention_g", initializer=tf.ones_initializer, shape=())
        self.built = True

    def _calculate_attention(self, query, state):
        """Score the query based on the keys and values.

        Args:
          query: Tensor of dtype matching `self.values` and shape
            `[batch_size, query_depth]`.
          state: Tensor of dtype matching `self.values` and shape
            `[batch_size, alignments_size]`
            (`alignments_size` is memory's `max_time`).

        Returns:
          alignments: Tensor of dtype matching `self.values` and shape
            `[batch_size, alignments_size]` (`alignments_size` is memory's
            `max_time`).
          next_state: Same as the alignments.
        """
        score = _luong_score(query, self.keys, self.scale_weight)
        alignments = self.probability_fn(score, state)
        next_state = alignments
        return alignments, next_state

    def get_config(self):
        config = {
            "units": self.units,
            "scale": self.scale,
            "probability_fn": self.probability_fn_name,
        }
        base_config = super(DLULuongAttention, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    @classmethod
    def from_config(cls, config, custom_objects=None):
        config = DLUAttention.deserialize_inner_layer_from_config(
            config, custom_objects=custom_objects)
        return cls(**config)


@DeprecationWarning
class DLULogicModule(tf.keras.layers.Layer):

    def __init__(self, max_logic_depth, max_logic_groups,
                 l2_regularization=0., dropout_rate=0.2,
                 unification_weights=[], **kwargs):
        super(DLULogicModule, self).__init__(**kwargs)
        self.max_logic_depth = max_logic_depth
        self.max_logic_groups = max_logic_groups

        self.unification_blocks = []
        for weight in unification_weights:
            self.unification_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                 kernel_regularizer=tf.keras.regularizers.l2(
                                                                     l2_regularization)))
            self.unification_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.unification_blocks.append(tf.keras.layers.Dense(units=1,
                                                             kernel_regularizer=tf.keras.regularizers.l2(
                                                                 l2_regularization)))

    def compute_unification_loss_mse(self, memory, candidate, mask):
        # [batch_size * memory_size, sentence_size, embedding_dim]
        # [batch_size * memory_size, sentence_size, embedding_dim]
        # [batch_size * memory_size, sentence_size]

        sentence_size = memory.shape[1]

        # [batch_size * memory_size * sentence_size, embedding_dim]
        memory = tf.reshape(memory, [-1, memory.shape[-1]])

        # [batch_size * memory_size * sentence_size, embedding_dim]
        candidate = tf.reshape(candidate, [-1, candidate.shape[-1]])

        # [batch_size * memory_size * sentence_size]
        # NOTE: memory should be considered constant here (!) -> stop gradient
        unification_loss = tf.losses.mean_squared_error(tf.stop_gradient(memory), candidate)

        mask = tf.reshape(mask, [-1, ])

        masked_loss = unification_loss * mask
        masked_loss = tf.reshape(masked_loss, [-1, sentence_size])

        return masked_loss

    def compute_unification_loss_mp(self, memory, candidate, mask, training=False):
        # [batch_size * memory_size, sentence_size, embedding_dim]
        # [batch_size * memory_size, sentence_size, embedding_dim]
        # [batch_size * memory_size, sentence_size]

        # [batch_size * memory_size * sentence_size, embedding_dim]
        memory = tf.reshape(memory, [-1, memory.shape[-1]])

        # [batch_size * memory_size * sentence_size, embedding_dim]
        candidate = tf.reshape(candidate, [-1, candidate.shape[-1]])

        # [batch_size * memory_size * sentence_size, embedding_dim * 2]
        couples = tf.concat((memory, candidate), axis=1)

        unification_loss = couples
        for block_idx in range(len(self.unification_blocks) - 1):
            if block_idx % 2 == 0:
                unification_loss = self.unification_blocks[block_idx](unification_loss)
            else:
                if training:
                    unification_loss = self.unification_blocks[block_idx](unification_loss)

        # Apply last block
        # [batch_size * memory_size, sentence_size]
        unification_loss = self.unification_blocks[-1](unification_loss)
        unification_loss = tf.reshape(unification_loss, [-1, mask.shape[1]])

        return unification_loss * mask

    def apply_logic_operator(self, inputs, operator_gate, masks=None):
        # [batch_size * memory_size, sentence_size]
        # [batch_size * memory_size, max_logic_groups]
        # [batch_size * memory_size, max_logic_groups, sentence_size]

        # Stackable behaviour
        if masks is not None:
            # list of length max_logic_groups, each with shape: [batch_size * memory_size, sentence_size]
            unstacked_masks = tf.unstack(masks, axis=1)

            # list of length max_logic_groups, each with shape: [batch_size * memory_size,]
            unstacked_operators = tf.unstack(operator_gate, axis=1)

            # [batch_size * memory_size, max_logic_groups]
            group_results = None
            for mask, operator in zip(unstacked_masks, unstacked_operators):
                operator = tf.cast(operator, tf.float32)
                operator = tf.expand_dims(operator, axis=-1)

                and_masked_probs = sparse_softmax(inputs, mask)
                or_masked_probs = sparse_softmax(inputs * -1, mask)

                masked_probs = and_masked_probs * operator + or_masked_probs * (1 - operator)

                # [batch_size * memory_size, ]
                current_result = tf.reduce_sum(inputs * masked_probs, axis=1)
                current_result = tf.expand_dims(current_result, -1)

                if group_results is None:
                    group_results = current_result
                else:
                    group_results = tf.concat((group_results, current_result), axis=-1)

            # Pad group results in order to let the module be easily stackable
            # [batch_size * memory_size, sentence_size]
            padding_losses = tf.zeros(shape=[group_results.shape[0], masks.shape[-1] - masks.shape[-2]])
            group_results = tf.concat((group_results, padding_losses), axis=1)

            return group_results
        else:
            operator = tf.reduce_sum(tf.cast(operator_gate, tf.float32), axis=1)
            operator = tf.expand_dims(operator, axis=-1)

            and_masked_probs = sparse_softmax(inputs, tf.not_equal(inputs, 0.))
            or_masked_probs = sparse_softmax(inputs, tf.not_equal(inputs, 0.))

            probs = and_masked_probs * operator + or_masked_probs * (1 - operator)
            result = tf.reduce_sum(inputs * probs, axis=1)
            return result

    def call(self, inputs, training=False, **kwargs):

        # [batch_size * memory_size, sentence_size, embedding_dim]
        unification_outputs = inputs['unification_outputs']

        # [batch_size * memory_size, sentence_size, embedding_dim]
        real_memory = inputs['real_memory']

        # [batch_size * memory_size, max_logic_depth - 1, max_logic_groups, sentence_size]
        logic_group_masks = inputs['logic_group_masks']

        # [batch_size * memory_size, max_logic_depth, max_logic_groups]
        logic_operator_gates = inputs['logic_operator_gates']

        # [batch_size * memory_size, sentence_size]
        symbolic_memory_mask = inputs['symbolic_memory_mask']

        # Compute unification loss

        # [batch_size * memory_size, sentence_size]
        unification_loss = self.compute_unification_loss_mse(memory=real_memory,
                                                             candidate=unification_outputs,
                                                             mask=symbolic_memory_mask)

        # Aggregate logic losses

        # list of size max_logic_depth, each with shape: [batch_size * memory_size, max_logic_groups, sentence_size]
        logic_group_masks = tf.unstack(logic_group_masks, axis=1)

        # list of size max_logic_depth, each with shape: [batch_size * memory_size, max_logic_groups]
        logic_operator_gates = tf.unstack(logic_operator_gates, axis=1)

        current_input = unification_loss
        for logic_group_mask, logic_operator_gate in zip(logic_group_masks, logic_operator_gates[:-1]):
            group_losses = self.apply_logic_operator(inputs=current_input,
                                                     masks=logic_group_mask,
                                                     operator_gate=logic_operator_gate)

            current_input = group_losses

        # Apply last logic operator without mask(s) to obtain final result
        # [batch_size * memory_size]
        final_losses = self.apply_logic_operator(inputs=current_input,
                                                 operator_gate=logic_operator_gates[-1])

        return final_losses


# Utility Layers

class HeadAttention(tf.keras.layers.Layer):

    def __init__(self, segments_amount, head_attention, embedding_dimension, attention_dimension,
                 use_masking=False, **kwargs):
        super(HeadAttention, self).__init__(**kwargs)
        self.head_attention = head_attention
        self.embedding_dimension = embedding_dimension
        self.attention_dimension = attention_dimension
        self.segments_amount = segments_amount
        self.use_masking = use_masking

        self.reduction_layers = [tf.keras.layers.Dense(units=self.attention_dimension, activation=tf.nn.relu),
                                 tf.keras.layers.Dense(units=self.embedding_dimension)]

    def call(self, x):

        # [batch_size, seq_length, embedding_dim]

        # [batch_size, seq_length, embedding_dim]
        reduction_input = x
        for reduction_layer in self.reduction_layers:
            reduction_input = reduction_layer(reduction_input)

        # [batch_size, embedding_dim]
        if self.use_masking:
            mask_width = x.shape[1] // self.segments_amount
            indexes = tf.range(0, x.shape[1])
            desired_shape = [x.shape[0], x.shape[1]]
            mask_lower = tf.where(indexes >= mask_width * self.head_attention,
                                  tf.ones(desired_shape), tf.zeros(desired_shape))
            mask_upper = tf.where(indexes < (mask_width * self.head_attention) + mask_width,
                                  tf.ones(desired_shape), tf.zeros(desired_shape))
            mask = mask_lower * mask_upper
            mask = tf.expand_dims(mask, axis=-1)
            mask = tf.tile(mask, multiples=[1, 1, self.embedding_dimension])
            reduction_weights = sparse_softmax(tf.transpose(reduction_input, [0, 2, 1]),
                                               tf.transpose(mask, [0, 2, 1]))
            reduction_weights = tf.transpose(reduction_weights, [0, 2, 1])
        else:
            reduction_weights = tf.nn.softmax(reduction_input, axis=1)
        pooled_embeddings = tf.reduce_sum(x * reduction_weights, axis=1)

        return pooled_embeddings


class GeneralizedPooling(tf.keras.layers.Layer):
    def __init__(self, segments_amount, attention_dimension, embedding_dimension, use_masking=False, **kwargs):
        super(GeneralizedPooling, self).__init__(*kwargs)

        self.segments_amount = segments_amount
        self.attention_dimension = attention_dimension
        self.embedding_dimension = embedding_dimension
        self.use_masking = use_masking

    def _compute_disagreement_penalization(self, segment_embeddings):
        segment_embeddings = tf.math.l2_normalize(segment_embeddings, axis=2)

        # [batch_size, segments_amount, segments_amount]
        cosine_distance = tf.matmul(segment_embeddings, segment_embeddings, transpose_b=True)

        # [batch_size, ]
        cosine_distance = tf.reduce_sum(cosine_distance, axis=2)
        cosine_distance = tf.reduce_sum(cosine_distance, axis=1)

        # [batch_size, ]
        cosine_distance = cosine_distance / (self.segments_amount * self.segments_amount)

        return cosine_distance

    def call(self, x, **kwargs):
        # [batch_size, seq_length, embedding_dim]

        def condition(index, stack, segments_amount, head_attention,
                      embedding_dimension, attention_dimension, use_masking=False):
            return tf.less(index, self.segments_amount)

        def body(index, stack, segments_amount, head_attention,
                 embedding_dimension, attention_dimension, use_masking=False):
            pooled_embeddings = HeadAttention(segments_amount=segments_amount,
                                              head_attention=head_attention,
                                              embedding_dimension=embedding_dimension,
                                              attention_dimension=attention_dimension,
                                              use_masking=use_masking)(x)
            stack = stack.write(index, pooled_embeddings)
            index = tf.add(index, 1)
            head_attention = head_attention + 1
            return index, stack, segments_amount, head_attention, embedding_dimension, attention_dimension, use_masking

        temp_stack = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
        result = tf.while_loop(condition,
                               body,
                               [tf.constant(0),
                                temp_stack,
                                self.segments_amount,
                                tf.constant(0),
                                self.embedding_dimension,
                                self.attention_dimension,
                                self.use_masking])
        segment_embeddings = result[1].stack()

        # [batch_size, segments_amount, embedding_dim]
        segment_embeddings = tf.transpose(segment_embeddings, [1, 0, 2])

        if self.segments_amount > 1:
            self.disagreement_penalization = self._compute_disagreement_penalization(segment_embeddings)

        return segment_embeddings


class AdditiveAttention(tf.keras.layers.Layer):

    def __init__(self, units=128, **kwargs):
        super(AdditiveAttention, self).__init__(**kwargs)

        self.value_dense = tf.keras.layers.Dense(units=units, use_bias=True)
        self.query_dense = tf.keras.layers.Dense(units=units, use_bias=False)
        self.attention_dense = tf.keras.layers.Dense(units=1, use_bias=False)

    def call(self, inputs, **kwargs):
        # [batch_size, embedding_dim]
        query = inputs['query']

        # [batch_size, sequence_length, embedding_dim]
        value = inputs['value']

        # [batch_size, sequence_length]
        additive_attention = tf.nn.tanh(self.value_dense(value) + tf.expand_dims(self.query_dense(query), axis=1))
        additive_attention = self.attention_dense(additive_attention)
        additive_attention = tf.squeeze(additive_attention)
        additive_attention = tf.nn.softmax(additive_attention, axis=1)

        # [batch_size, embedding_dim]
        attention_vector = tf.reduce_sum(value * tf.expand_dims(additive_attention, -1), axis=1)

        return attention_vector


######################################################
####################### BASELINE #####################
######################################################


class M_Baseline_LSTM(tf.keras.Model):
    """
    LSTM baseline for ToS Task 1. This is a simple stacked-LSTMs model.
    """

    def __init__(self, sentence_size, vocab_size, lstm_weights,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Baseline_LSTM, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.lstm_blocks = []
        for weight in self.lstm_weights[:-1]:
            self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                       return_sequences=True,
                                                                                       kernel_regularizer=tf.keras.regularizers.l2(
                                                                                           self.l2_regularization))))
            self.lstm_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.lstm_weights[-1],
                                                                                   return_sequences=False,
                                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                                       self.l2_regularization))))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block_idx in range(len(self.lstm_blocks) - 1):
            if block_idx % 2 == 0:
                lstm_input = self.lstm_blocks[block_idx](lstm_input)
            else:
                if training:
                    lstm_input = self.lstm_blocks[block_idx](lstm_input)

        lstm_input = self.lstm_blocks[-1](lstm_input)

        answer = lstm_input
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        answer = self.answer_blocks[-1](answer)
        answer = tf.reshape(answer, [-1, ])

        return answer


class M_Baseline_CNN(tf.keras.Model):
    """
    CNN baseline for ToS Task 1. This is a simple stacked-CNNs model.
    """

    def __init__(self, sentence_size, vocab_size, cnn_weights,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None):
        super(M_Baseline_CNN, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.cnn_weights = cnn_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.cnn_blocks = []
        for filters, kernel in self.cnn_weights:
            self.cnn_blocks.append(tf.keras.layers.Conv1D(filters,
                                                          kernel,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              self.l2_regularization)))
            self.cnn_blocks.append(tf.keras.layers.MaxPool1D())
            self.cnn_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.cnn_blocks.append(tf.keras.layers.Flatten())

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=1,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        cnn_input = sentence_emb
        sub_block_step = 0
        for block_idx in range(len(self.cnn_blocks) - 1):
            # We have to take into account max_pooling
            if sub_block_step == 2:
                sub_block_step = -1
                if training:
                    cnn_input = self.cnn_blocks[block_idx](cnn_input)
            else:
                cnn_input = self.cnn_blocks[block_idx](cnn_input)
            sub_block_step += 1

        cnn_input = self.cnn_blocks[-1](cnn_input)

        answer = cnn_input
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                answer = self.answer_blocks[block_idx](answer)
            else:
                if training:
                    answer = self.answer_blocks[block_idx](answer)

        answer = self.answer_blocks[-1](answer)
        answer = tf.reshape(answer, [-1, ])

        return answer
