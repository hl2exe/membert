"""

@Author: Federico Ruggeri

@Date: 17/09/2019

"""

import numpy as np
import tensorflow as tf

from utility.log_utils import get_logger
from utility.preprocessing_utils import filter_line, punctuation_filtering,\
    tf_remove_special_words, sentence_to_lower, tf_number_replacing_with_constant

from collections import OrderedDict

logger = get_logger(__name__)


class BasePreprocessor(object):
    """
    Base pre-processing interface
    """

    def __init__(self, text_column='text'):
        self.text_column = text_column

    def parse(self, data, additional_info=None, return_info=False):
        if return_info:
            return data, additional_info
        else:
            return data


class TextPreprocessor(BasePreprocessor):

    def __init__(self, function_names=None, **kwargs):
        super(TextPreprocessor, self).__init__(**kwargs)
        self.function_names = function_names

    def _text_filter(self, batch):

        def vect_filter(item):
            # item = item.astype(np.bytes_)
            encode_filter = lambda x: x.decode('UTF-8')
            item = np.vectorize(encode_filter)(item)
            # item = np.char.decode(item, 'UTF-8')
            my_filter = lambda x: filter_line(x, function_names=self.function_names)
            vfunc = np.vectorize(my_filter)
            return vfunc(item)

        batch[self.text_column] = tf.numpy_function(func=vect_filter,
                                                    inp=[batch[self.text_column]],
                                                    Tout=tf.string)

        return batch

    def parse(self, data, additional_info=None, return_info=False):
        # data = data.map(lambda batch: self._text_filter(batch))

        if additional_info is not None:
            for key, item in additional_info.items():
                if type(item) == list:
                    additional_info[key] = [filter_line(sent) for sent in item]
                elif type(item) in [dict, OrderedDict]:
                    additional_info[key] = {filter_line(key): value for key, value in item.items()}

        if return_info:
            return data, additional_info
        else:
            return data


class PreprocessingFactory(object):
    supported_preprocessors = {
        'base_preprocessor': BasePreprocessor,
        'text_preprocessor': TextPreprocessor
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if PreprocessingFactory.supported_preprocessors[key]:
            return PreprocessingFactory.supported_preprocessors[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
