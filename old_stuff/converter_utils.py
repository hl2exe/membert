"""

@Author: Federico Ruggeri

@Date: 17/05/2019

"""

from itertools import chain
from collections import OrderedDict


def flat_dict(x):
    converted = list(chain(*x.values()))

    if type(converted[0]) == list:
        converted = [item for sublist in converted for item in sublist]

    return converted


def flat_data(x):
    if type(x) in [dict, OrderedDict]:
        return flat_dict(x)

    converted = []
    if type(x) == list:
        for item in x:
            converted.append(flat_dict(item))

    return converted
