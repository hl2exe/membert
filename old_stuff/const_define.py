"""

@Author: Federico Ruggeri

@Date: 29/11/2018

Constant python configuration script.

TODO: clean deprecated models.
TODO: clean other stuff (reporters, etc...)

------------
 Algorithms
------------

Algorithms are structured based on their architecture. Right now only two architectures are supported. Feel free to
add new ones.

    1. Sentence wise: these models just consider a simple text input.
    2. Context wise: these models also consider an additional context input.
       Memory Networks follow this architecture by considering the memory as the context.

Since each model may have its own pipeline the MODEL_CONFIG dictionary stores ad hoc configurations. If a model is
not found in there, the default configuration associated to its architecture is applied, stored in ALGORITHM_CONFIG.

A pipeline is comprised of three steps: preprocessing, conversion and set splitting.

------------
 Constants
------------

Additionally, framework constants are defined here: folder paths and file names. This is useful since many scripts
share the same paths or save information by using a fixed set of names. In this way, code re-factoring is easier.


"""

import os

NAME_LOG = 'daily_log.log'

# Algorithms

SUPPORTED_LEARNERS = ['sentence_wise', 'context_wise']
SUPPORTED_ALGORITHMS = {
    'vanilla_lstm_v1': {
        'save_suffix': 'vanilla_lstm_v1',
        'model_type': 'sentence_wise'
    },
    'bi_gru_v1': {
        'save_suffix': 'bi_gru_v1',
        'model_type': 'sentence_wise'
    },
    'cbi_gru_v1': {
        'save_suffix': 'cbi_gru_v1',
        'model_type': 'sentence_wise'
    },
    'basic_memn2n_v1': {
        'save_suffix': 'basic_memn2n_v1',
        'model_type': 'context_wise'
    },
    'multi_basic_memn2n_v1': {
        'save_suffix': 'multi_basic_memn2n_v1',
        'model_type': 'context_wise'
    },
    'similarity_memn2n_v1': {
        'save_suffix': 'similarity_memn2n_v1',
        'model_type': 'context_wise'
    },
    'gated_memn2n_v1': {
        'save_suffix': 'gated_memn2n_v1',
        'model_type': 'context_wise'
    },
    'entailment_memn2n_v1': {
        'save_suffix': 'entailment_memn2n_v1',
        'model_type': 'context_wise'
    },
    'basic_elmo_memn2n_v1': {
        'save_suffix': 'basic_elmo_memn2n_v1',
        'model_type': 'context_wise'
    },
    'basic_bert_memn2n_v1': {
        'save_suffix': 'basic_bert_memn2n_v1',
        'model_type': 'context_wise'
    },
    'word_ememn2n_v1': {
        "save_suffix": "word_ememn2n_v1",
        "model_type": "context_wise"
    },
    'han_v1': {
        "save_suffix": "han_v1",
        "model_type": "context_wise"
    },
    'transformer_v1': {
        'save_suffix': 'transformer_v1',
        'model_type': 'context_wise'
    },
    'basic_memn2n_v2': {
        'save_suffix': 'basic_memn2n_v2',
        'model_type': 'context_wise'
    },
    'gated_memn2n_v2': {
        'save_suffix': 'gated_memn2n_v2',
        'model_type': 'context_wise'
    },
    'discriminative_memn2n_v2': {
        "save_suffix": 'discriminative_memn2n_v2',
        "model_type": "context_wise"
    },
    'entailment_memn2n_v2': {
        'save_suffix': 'entailment_memn2n_v2',
        'model_type': 'context_wise'
    },
    'word_memn2n_v2': {
        'save_suffix': 'word_memn2n_v2',
        'model_type': 'context_wise'
    },
    'bow_memn2n_v2': {
        'save_suffix': 'bow_memn2n_v2',
        'model_type': 'context_wise'
    },
    'bow_memn2n_v1': {
        'save_suffix': 'bow_memn2n_v1',
        'model_type': 'context_wise'
    },
    'baseline_lstm_v2': {
        'save_suffix': 'baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'baseline_cnn_v2': {
        'save_suffix': 'baseline_cnn_v2',
        'model_type': 'sentence_wise'
    },
    'albert_text_classifier_v2': {
        'save_suffix': 'albert_text_classifier_v2',
        'model_type': 'sentence_wise'
    },
    'albert_text_classifier_v1': {
        'save_suffix': 'albert_text_classifier_v1',
        'model_type': 'sentence_wise'
    },
    'transformer_v2': {
        'save_suffix': 'transformer_v2',
        'model_type': 'context_wise'
    },
    'basic_elmo_memn2n_v2': {
        'save_suffix': 'basic_elmo_memn2n_v2',
        'model_type': 'context_wise'
    },
    'basic_bert_memn2n_v2': {
        'save_suffix': 'basic_bert_memn2n_v2',
        'model_type': 'context_wise'
    },
    'parallel_dlu': {
        "save_suffix": 'parallel_dlu',
        'model_type': 'context_wise'
    },
    'semantic_iterated_transformer_v2': {
        "save_suffix": 'semantic_iterated_transformer',
        'model_type': 'context_wise'
    },
    "semantic_coattention_v2": {
        "save_suffix": 'semantic_coattention',
        'model_type': 'context_wise'
    }
}

MODEL_CONFIG = {
    'basic_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'multi_basic_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'similarity_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'task2_kb_context_converter',
        'splitter': 'general_splitter'
    },
    'gated_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'entailment_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'basic_elmo_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_elmo_converter',
        'splitter': 'general_splitter'
    },
    'basic_bert_memn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_bert_converter',
        'splitter': 'general_splitter'
    },
    'ememn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'word_ememn2n_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter'
    },
    "bow_memn2n_v1": {
        'preprocessor': 'text_preprocessor',
        'converter': 'bow_supervised_context_converter',
        'splitter': 'general_splitter'
    },
    'han_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'kb_context_converter',
        'splitter': 'general_splitter',
    },
    'transformer_v1': {
        'preprocessor': 'text_preprocessor',
        'converter': 'transformer_converter',
        'splitter': 'general_splitter'
    },
    "recurrent_relation_network_v1": {
        'preprocessor': 'text_preprocessor',
        'converter': 'context_converter',
        'splitter': 'general_splitter'
    },
    'basic_memn2n_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter'
    },
    'gated_memn2n_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    "discriminative_memn2n_v2": {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter'
    },
    'entailment_memn2n_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter',
    },
    'word_memn2n_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter'
    },
    'bow_memn2n_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'bow_supervised_context_converter',
        'splitter': 'general_splitter'
    },
    'baseline_lstm_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'base_converter',
        'splitter': 'general_splitter'
    },
    'baseline_cnn_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'base_converter',
        'splitter': 'general_splitter'
    },
    'albert_text_classifier_v2': {
        'preprocessor': 'bert_preprocessor',
        'converter': 'fine_tuning_bert_converter',
        'splitter': 'general_splitter'
    },
    'albert_text_classifier_v1': {
        'preprocessor': 'bert_preprocessor',
        'converter': 'fine_tuning_bert_converter',
        'splitter': 'general_splitter'
    },
    'transformer_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'transformer_converter',
        'splitter': 'general_splitter'
    },
    "basic_elmo_memn2n_v2": {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_elmo_converter',
        'splitter': 'general_splitter'
    },
    "basic_bert_memn2n_v2": {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_bert_converter',
        'splitter': 'general_splitter'
    },
    'parallel_dlu': {
        'preprocessor': 'text_preprocessor',
        'converter': 'dlu_kb_context_converter',
        'splitter': 'general_splitter'
    },
    'semantic_iterated_transformer_v2': {
        'preprocessor': 'text_preprocessor',
        'converter': 'transformer_converter',
        'splitter': 'general_splitter'
    },
    "semantic_coattention_v2": {
        'preprocessor': 'text_preprocessor',
        'converter': 'supervised_context_converter',
        'splitter': 'general_splitter'
    }
}

ALGORITHM_CONFIG = {
    'sentence_wise': {
        'preprocessor': 'base_preprocessor',
        'converter': 'base_converter',
        'splitter': 'base_splitter',
    },
    'context_wise': {
        'preprocessor': 'context_preprocessor',
        'converter': 'base_converter',
        'splitter': 'base_splitter',
    }
}

# DEFAULT DIRECTORIES
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIGS_DIR = os.path.join(PROJECT_DIR, 'configs')
INTERNAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'datasets')
SAVED_MODELS_DIR = os.path.join(PROJECT_DIR, 'saved_models')
SAVED_MODELS_ADDITIONAL_RESULTS_DIR = os.path.join(PROJECT_DIR, 'saved_models_additional_results')
INFERENCE_DIR = os.path.join(PROJECT_DIR, 'inference')
PATH_LIST_DATASETS_XLS = os.path.join(PROJECT_DIR, 'ml_utilities', 'ml_lib', 'utility')
PATH_LOG = os.path.join(PROJECT_DIR, 'log')
REPORT_PATH = os.path.join(PROJECT_DIR, 'reports')
RUNNABLES_DIR = os.path.join(PROJECT_DIR, 'runnables')
LOCAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'local_database')
LOO_DIR = os.path.join(PROJECT_DIR, 'loo_test')
CV_DIR = os.path.join(PROJECT_DIR, 'cv_test')
CALIBRATION_RESULTS_DIR = os.path.join(PROJECT_DIR, 'calibration_results')
EMBEDDING_MODELS_DIR = os.path.join(PROJECT_DIR, 'embedding_models')
TOS_50_DIR = os.path.join(LOCAL_DATASETS_DIR, 'ToS_50')
TOS_100_DIR = os.path.join(LOCAL_DATASETS_DIR, 'ToS_100')
KB_DIR = os.path.join(LOCAL_DATASETS_DIR, 'KB')
PREBUILT_FOLDS_DIR = os.path.join(PROJECT_DIR, 'prebuilt_folds')
SAVED_INFO_DIR = os.path.join(PROJECT_DIR, 'saved_info')
MONGO_DB_DIR = os.path.join(PROJECT_DIR, 'mongo_db')
TESTS_DATA_DIR = os.path.join(PROJECT_DIR, 'tests_data')

DATASET_PATHS = {
    'tos_50': TOS_50_DIR,
    'tos_100': TOS_100_DIR
}

# JSON FILES
JSON_CALLBACKS_NAME = 'callbacks.json'
JSON_MODEL_CONFIG_NAME = 'model_config.json'
JSON_DISTRIBUTED_MODEL_CONFIG_NAME = 'distributed_model_config.json'
JSON_MODEL_HISTORY_NAME = 'model_history.json'
JSON_TRAINING_CONFIG_NAME = 'training_config.json'
JSON_LOO_TEST_CONFIG_NAME = 'loo_test_config.json'
JSON_CV_TEST_CONFIG_NAME = 'cv_test_config.json'
JSON_DATA_LOADER_CONFIG_NAME = 'data_loader.json'
JSON_HYPEROPT_MODEL_GRIDSEARCH_NAME = 'hyperopt_model_gridsearch.json'
JSON_CALIBRATOR_INFO_NAME = 'calibrator_info.json'
JSON_VALIDATION_INFO_NAME = 'validation_info.json'
JSON_TEST_INFO_NAME = 'test_info.json'
JSON_PREDICTIONS_NAME = 'predictions.json'
JSON_DISTRIBUTED_CONFIG_NAME = 'distributed_config.json'
JSON_MODEL_DATA_CONFIGS_NAME = 'data_configs.json'

# NAMES REPORTS
REPORT_TRAIN_CSV = 'report_train.csv'
REPORT_PRED_CSV = 'report_pred.csv'
