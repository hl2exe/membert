"""

@Author: Federico Ruggeri

@Date: 17/05/2019

"""

import math
import numpy as np
from skimage.util import view_as_windows


# TODO: documentation
def windowing(x, y, window_dim, group_output=False):
    if type(x) is not np.ndarray:
        x = np.asarray(x)
    if type(y) is not np.ndarray:
        y = np.asarray(y)

    grouped_x = view_as_windows(x, window_shape=window_dim, step=1)

    if group_output:
        y = view_as_windows(y, window_shape=window_dim, step=1)

    y = y[:grouped_x.shape[0]]

    return grouped_x, y


# TODO: documentation
def group_data_including(x, y, window_dim, group_output=False):
    x = np.asarray(x)
    y = np.asarray(y)

    grouped_x = []
    if group_output:
        grouped_y = []

    if window_dim == 1:
        x = [[sentence] for sentence in x]
        return x, y

    for idx, sentence in enumerate(x):

        context_left = int(math.floor(window_dim / 2))
        context_right = window_dim - context_left - 1

        context = []

        # First sentences
        if idx < context_left:
            context_right += context_left - idx
            context_left = idx

        # Last sentences
        if len(x) - idx - 1 < context_right:
            context_left += context_right - (len(x) - idx - 1)
            context_right = len(x) - idx - 1

        left_sentences = x[idx - context_left:idx]
        context.extend(left_sentences)
        context.append(sentence)
        right_sentences = x[idx + 1: idx + context_right + 1]
        context.extend(right_sentences)

        grouped_x.append(context)

        if group_output:
            left_labels = y[idx - context_left:idx]
            right_labels = y[idx + 1: idx + context_right + 1]
            grouped_y.append(left_labels + y[idx] + right_labels)

    if group_output:
        return grouped_x, grouped_y
    else:
        return grouped_x, y


# TODO: documentation
def group_data_excluding(x, y, window_dim, group_output=False):
    x = np.asarray(x)
    y = np.asarray(y)

    grouped_x = []

    if group_output:
        grouped_y = []

    for idx, sentence in enumerate(x):

        context_left = int(math.ceil(window_dim / 2))
        context_right = window_dim - context_left

        # Initial sentences
        if idx < context_left:
            context_right += context_left - idx
            context_left = idx

        # Last sentences
        if len(x) - idx - 1 < context_right:
            context_left += context_right - (len(x) - idx - 1)
            context_right = len(x) - idx - 1

        if idx != 0 and idx != len(x) - 1:
            left_sentences = x[idx - context_left: idx]
            right_sentences = x[idx + 1: idx + context_right + 1]
            context = []
            context.extend(left_sentences)
            context.extend(right_sentences)
        elif idx == 0:
            context = x[1: context_right + 1].tolist()
        else:
            context = x[idx - context_left: idx].tolist()

        grouped_x.append(context)

        if group_output:
            left_labels = y[idx - context_left:idx]
            right_labels = y[idx + 1: idx + context_right + 1]
            grouped_y.append(left_labels + y[idx] + right_labels)

    if group_output:
        return grouped_x, grouped_y
    else:
        return grouped_x, y
