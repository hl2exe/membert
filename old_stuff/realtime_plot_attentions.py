"""

@Author: Federico Ruggeri

@Date: 10/06/2019

"""

import os
import pickle

import matplotlib.pyplot as plt

import const_define as cd
from utility.plot_utils import plot_gates_distributions, plot_attention_distributions, plot_attention_contour

# Settings
att_masking = 'positive'
gate_masking = 'positive'
show_best_only = False

test_dir = "20190628-225109"
fold = "fold_0"
checkpoint = 15
load_path = os.path.join(cd.PROJECT_DIR, 'logs', test_dir, fold)

show_att = True
show_gates = True

# Load True Values
with open(os.path.join(load_path, 'true_values.pickle'), 'rb') as f:
    true_values = pickle.load(f)

# Load attention
with open(os.path.join(load_path, 'attentions_{}.pickle'.format(checkpoint)), 'rb') as f:
    attention_data = pickle.load(f)

# Load Gates
try:
    with open(os.path.join(load_path, 'gates_{}.pickle'.format(checkpoint)), 'rb') as f:
        gates_data = pickle.load(f)
except FileNotFoundError as fnfe:
    show_gates = False

# Load Indexes
with open(os.path.join(load_path, 'indexes_{}.pickle'.format(checkpoint)), 'rb') as f:
    indexes_data = pickle.load(f)

if show_att:
    for hop_idx, att_hop in enumerate(attention_data):
        # plot_attention_distributions(attention=att_hop,
        #                              indexes=indexes_data,
        #                              name='attention_distribution_{}'.format(hop_idx),
        #                              true_values=true_values,
        #                              masking=att_masking,
        #                              show_best_only=show_best_only)
        plot_attention_contour(attention=att_hop,
                               indexes=indexes_data,
                               name='attention_distribution_{}'.format(hop_idx),
                               true_values=true_values)

if show_gates:
    for hop_idx, gate_hop in enumerate(gates_data):
        plot_gates_distributions(gates=gate_hop,
                                 true_values=true_values,
                                 name='gate_distribution_{}'.format(hop_idx),
                                 masking=gate_masking)

if show_att or show_gates:
    plt.show()
