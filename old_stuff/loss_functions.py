import tensorflow as tf
from keras.losses import binary_crossentropy
from keras import backend as K


def create_weighted_crossentropy(pos_weight, logits):
    def weighted_crossentropy(y_true, y_pred):
        loss = tf.nn.weighted_cross_entropy_with_logits(targets=y_true,
                                                        logits=logits,
                                                        pos_weight=pos_weight)
        return loss

    return weighted_crossentropy


def create_weighted_crossentropy_from_weights(pos_weight):

    def keras_weighted_crossentropy(y_true, y_pred):

        bin = binary_crossentropy(y_true=y_true, y_pred=y_pred)

        weight_vector = y_true * pos_weight + (1. - y_true) * (1 - pos_weight)
        weighted_bin = weight_vector * bin

        return K.mean(weighted_bin)

    return keras_weighted_crossentropy
