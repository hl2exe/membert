"""

@Author: Federico Ruggeri

@Date: 22/07/2019

TODO: generalize cross_validation routine: remove dataset_list

"""

import os
from collections import OrderedDict

import ktrain
import numpy as np
from ktrain import text
from sklearn.utils import compute_class_weight
from tensorflow.python.keras import backend as K

import const_define as cd
from old_stuff.converter import ConverterFactory
from custom_callbacks_v2 import TensorBoard
from old_stuff.nn_models_v2 import ModelFactory
from old_stuff.preprocessing import PreprocessingFactory
from old_stuff.splitter import SplitterFactory
from utility.cross_validation_utils import build_metrics, compute_iteration_validation_error, update_cv_validation_info, \
    show_data_shapes
from utility.json_utils import load_json, save_json
from utility.log_utils import get_logger
from utility.python_utils import merge
from ktrain.text.learner import TransformerTextClassLearner
from utility.ktrain_utils import KBTransformerSequence
from old_stuff.custom_tf_v2 import M_MemoryDistilBERT
import tensorflow as tf

logger = get_logger(__name__)


def cross_validation(validation_percentage, data_handle, cv,
                     model_type, network_args, training_config,
                     error_metrics, error_metrics_additional_info=None,
                     callbacks=None, compute_test_info=True, save_predictions=False,
                     use_tensorboard=False, repetitions=1, save_model=False,
                     save_path=None, pre_loaded=False, build_validation=True, split_key=None):
    """
    [Repeated] Cross-validation routine:

        1. [For each repetition]

        2. For each fold:
            2A. Load fold data: a DataHandle (tf_data_loader.py) object is used to retrieved cv fold data.
            2B. Pre-processing: a preprocessor (experimental_preprocessing.py) is used to parse text input.
            2C. Conversion: a converter (converters.py) is used to convert text to numerical format.
            2D. Train/Val/Test split: a splitter (splitters.py) is used to defined train/val/test sets
            2E. Model definition: a network model (nn_models_v2.py) is built
            2F. Model training: the network is trained.
            2G. Model evaluation on val/test sets: trained model is evaluated on val/test sets

        3. Results post-processing: macro-average values are computed.
    """

    if repetitions < 1:
        message = 'Repetitions should be at least 1! Got: {}'.format(repetitions)
        logger.error(message)
        raise AttributeError(message)

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # Step 0: add tensorboard visualization
    if use_tensorboard:
        test_name = os.path.split(save_path)[-1]
        tensorboard_base_dir = os.path.join(cd.PROJECT_DIR, 'logs', test_name)
        os.makedirs(tensorboard_base_dir)

    # [Step 1]: load embedding model
    if model_type in cd.MODEL_CONFIG:
        converter_type = cd.MODEL_CONFIG[model_type]['converter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        converter_type = cd.ALGORITHM_CONFIG[archetype]['converter']
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}

    # I need to know which label to look after
    converter_args['label'] = data_handle.label
    converter = ConverterFactory.factory(cl_type=converter_type, **converter_args)
    converter.load_embedding_model()

    # Step 2: cross validation
    total_validation_info = OrderedDict()
    total_test_info = OrderedDict()
    total_preds = OrderedDict()

    for repetition in range(repetitions):
        logger.info('Repetition {0}/{1}'.format(repetition + 1, repetitions))

        validation_info = OrderedDict()
        test_info = OrderedDict()
        all_preds = OrderedDict()

        for fold_idx, (train_indexes, val_indexes) in enumerate(cv.split(None)):
            logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

            train_df, val_df, test_df = data_handle.get_split(key=split_key,
                                                      key_values=val_indexes,
                                                      validation_percentage=validation_percentage)

            # Pre-processing
            if model_type in cd.MODEL_CONFIG:
                preprocessor_type = cd.MODEL_CONFIG[model_type]['preprocessor']
            else:
                archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
                preprocessor_type = cd.ALGORITHM_CONFIG[archetype]['preprocessor']
            preprocessor_args = {key: value['value'] for key, value in network_args.items()
                                 if 'preprocessing' in value['flags']}

            preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                        **preprocessor_args)

            train_df, additional_data = preprocessor.parse(df=train_df,
                                                           additional_info=data_handle.get_additional_info(),
                                                           return_info=True,
                                                           data_keys=data_handle.data_keys)
            test_df = preprocessor.parse(df=test_df,
                                         additional_info=data_handle.get_additional_info(),
                                         data_keys=data_handle.data_keys)

            # Data conversion
            x_train, y_train, text_info, additional_data = converter.fit_train_data(train_df=train_df,
                                                                                    additional_data=additional_data)

            if text_info is not None:
                for key in text_info:
                    logger.info('{0} size: {1}'.format(key, text_info[key]))

            x_test, y_test = converter.convert_data(df=test_df, text_info=text_info, additional_data=additional_data)

            # Train/Validation split
            if model_type in cd.MODEL_CONFIG:
                splitter_type = cd.MODEL_CONFIG[model_type]['splitter']
            else:
                archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
                splitter_type = cd.ALGORITHM_CONFIG[archetype]['splitter']

            splitter_args = {key: value['value'] for key, value in network_args.items()
                             if 'splitter' in value['flags']}
            splitter_args['validation_split'] = validation_percentage
            splitter = SplitterFactory.factory(cl_type=splitter_type,
                                               **splitter_args)

            if build_validation:
                logger.info('Building validation split!')

                x_train, \
                y_train, \
                x_val, \
                y_val, \
                text_info, \
                x_test, \
                y_test = splitter.split(x=x_train, y=y_train, x_test=x_test, y_test=y_test,
                                        text_info=text_info,
                                        return_text_info=True,
                                        data_keys=data_handle.data_keys)
            else:
                logger.info('Retrieving fixed test set!')

                x_train, \
                y_train, \
                x_val, \
                y_val, \
                text_info, \
                x_test, \
                y_test = splitter.split_by_indexes(x=x_train,
                                                   y=y_train,
                                                   val_indexes=val_indexes,
                                                   train_indexes=train_indexes,
                                                   x_test=x_test,
                                                   y_test=y_test,
                                                   text_info=text_info,
                                                   return_text_info=True)

            show_data_shapes(x_train, 'Train')
            show_data_shapes(x_val, 'Validation')
            show_data_shapes(x_test, 'Test')

            network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                      if 'model_class' in value['flags']}
            network_retrieved_args['additional_data'] = additional_data
            network_retrieved_args['name'] = '{0}_repetition_{1}_fold_{2}'.format(
                cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], repetition, fold_idx)
            network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

            # computing positive label weights (for unbalanced dataset)
            network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_begin'):
                    callback.on_build_model_begin(logs={'network': network})

            network.build_model(text_info=text_info)

            # Custom callbacks only
            for callback in callbacks:
                if hasattr(callback, 'on_build_model_end'):
                    callback.on_build_model_end(logs={'network': network})

            if use_tensorboard:
                fold_log_dir = os.path.join(tensorboard_base_dir,
                                            'repetition_{}'.format(repetition),
                                            'fold_{}'.format(fold_idx))
                os.makedirs(fold_log_dir)
                tensorboard = TensorBoard(batch_size=training_config['batch_size'],
                                          histogram_freq=True,
                                          log_dir=fold_log_dir)
                fold_callbacks = callbacks + [tensorboard]
            else:
                fold_callbacks = callbacks

            if not pre_loaded:
                # Training
                network.fit(x=x_train, y=y_train,
                            callbacks=fold_callbacks,
                            validation_data=(x_val, y_val),
                            **training_config)
            else:
                # Loading
                logger.info('Loading pre-trained model...')

                current_weight_filename = '{0}_repetition_{1}_fold_{2}'.format(model_type,
                                                                               repetition,
                                                                               fold_idx)
                network.load(os.path.join(save_path, current_weight_filename))

            # Inference
            val_predictions = network.predict(x=x_val,
                                              batch_size=training_config['batch_size'])

            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=y_val,
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info)

            validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                        iteration_validation_info=iteration_validation_error)

            logger.info('Iteration validation info: {}'.format(iteration_validation_error))

            test_predictions = network.predict(x=x_test,
                                               batch_size=training_config['batch_size'])

            iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                      true_values=y_test,
                                                                      predicted_values=test_predictions,
                                                                      error_metrics_additional_info=error_metrics_additional_info)

            if compute_test_info:
                test_info = update_cv_validation_info(test_validation_info=test_info,
                                                      iteration_validation_info=iteration_test_error)
                logger.info('Iteration test info: {}'.format(iteration_test_error))

                if save_predictions:
                    all_preds[fold_idx] = test_predictions.ravel()

            # Save model
            if save_model and not pre_loaded:
                filepath = os.path.join(save_path,
                                        '{0}_repetition_{1}_fold_{2}'.format(
                                            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'],
                                            repetition,
                                            fold_idx))
                network.save(filepath=filepath)

            # Save ground truth
            if save_model:
                filepath = os.path.join(save_path, 'y_test_fold_{}.json'.format(fold_idx))
                if not os.path.isfile(filepath):
                    save_json(filepath=filepath, data=y_test)

            # Flush
            K.clear_session()

        for key, item in validation_info.items():
            total_validation_info.setdefault(key, []).append(item)
        for key, item in test_info.items():
            total_test_info.setdefault(key, []).append(item)
        for key, item in all_preds.items():
            total_preds.setdefault(key, []).append(item)

    if repetitions == 1:
        total_validation_info = {key: np.mean(item, 0) for key, item in total_validation_info.items()}
        total_test_info = {key: np.mean(item, 0) for key, item in total_test_info.items()}
        total_preds = {key: np.mean(item, 0) for key, item in total_preds.items()}
    else:
        avg_validation_info = {}
        for key, item in total_validation_info.items():
            avg_validation_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_validation_info = merge(total_validation_info, avg_validation_info)

        avg_test_info = {}
        for key, item in total_test_info.items():
            avg_test_info['avg_{}'.format(key)] = np.mean(item, 0)
        total_test_info = merge(total_test_info, avg_test_info)

    result = {
        'validation_info': total_validation_info,
    }

    if compute_test_info:
        result['test_info'] = total_test_info
        if save_predictions:
            result['predictions'] = total_preds
    else:
        if save_predictions:
            result['predictions'] = total_preds

    return result


def ktrain_cross_validation(validation_percentage, data_handle, cv,
                            training_config, model_name, error_metrics,
                            error_metrics_additional_info=None, callbacks=None,
                            compute_test_info=True, save_predictions=False,
                            save_model=False, save_path=None,
                            split_key=None):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    validation_info = OrderedDict()
    test_info = OrderedDict()
    all_preds = OrderedDict()

    for fold_idx, (train_indexes, val_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, test_df = data_handle.get_split(key=split_key, key_values=val_indexes)

        preprocessor = text.Transformer(model_name, maxlen=300, classes=[0, 1])

        validation_amount = int(train_df.shape[0] * validation_percentage)
        train_amount = train_df.shape[0] - validation_amount
        train_df, val_df = train_df.iloc[:train_amount], train_df.iloc[train_amount:]

        train_data = preprocessor.preprocess_train(train_df.text.values,
                                                   train_df[data_handle.label].values)

        val_data = preprocessor.preprocess_test(val_df.text.values,
                                                val_df[data_handle.label].values)

        test_data = preprocessor.preprocess_test(test_df.text.values,
                                                 test_df[data_handle.label].values)

        val_texts = val_df.text.values
        test_texts = test_df.text.values

        model = preprocessor.get_classifier()
        learner = ktrain.get_learner(model, train_data=train_data, val_data=val_data,
                                     batch_size=training_config['batch_size'])

        # computing positive label weights (for unbalanced dataset)
        class_weights = compute_class_weight(class_weight='balanced',
                                             classes=[0, 1],
                                             y=train_df[data_handle.label].values)

        es_best_score = None
        es_best_weights = None
        es_patience = 3
        es_current_patience = 0
        for epoch in range(training_config['epochs']):
            logger.info('Epoch: {}'.format(epoch + 1))
            learner.fit(lr=5e-6, n_cycles=1, class_weight=class_weights, verbose=1)
            predictor = ktrain.get_predictor(learner.model, preproc=preprocessor)
            val_predictions = predictor.predict(val_texts)
            iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                            true_values=np.argmax(val_data.y, axis=1),
                                                                            predicted_values=val_predictions,
                                                                            error_metrics_additional_info=error_metrics_additional_info)
            logger.info('Validation scores: {}'.format(iteration_validation_error))
            if es_best_score is None:
                es_best_score = iteration_validation_error['f1_score']
                es_best_weights = learner.model.get_weights()
            else:
                if es_best_score < iteration_validation_error['f1_score']:
                    es_best_score = iteration_validation_error['f1_score']
                    es_best_weights = learner.model.get_weights()
                    es_current_patience = 0
                else:
                    es_current_patience += 1

            if es_current_patience >= es_patience:
                break

        learner.model.set_weights(es_best_weights)

        # Inference

        predictor = ktrain.get_predictor(learner.model, preproc=preprocessor)

        val_predictions = predictor.predict(val_texts)

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np.argmax(val_data.y, axis=1),
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions = predictor.predict(test_texts)

        iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                  true_values=np.argmax(test_data.y, axis=1),
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info)

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            if save_predictions:
                all_preds[fold_idx] = test_predictions

        # Save model
        if save_model:
            filepath = os.path.join(save_path,
                                    '{0}_fold_{1}'.format(
                                        model_name,
                                        fold_idx))
            predictor.save(filepath)

            # Save ground truth
            filepath = os.path.join(save_path, 'y_test_fold_{}.json'.format(fold_idx))
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=test_data.y)

        # Flush
        K.clear_session()

    result = {
        'validation_info': validation_info
    }

    if compute_test_info:
        result['test_info'] = test_info
        result['predictions'] = all_preds

    return result


def ktrain_custom_cross_validation(validation_percentage, data_handle, cv,
                                   training_config, model_name, error_metrics,
                                   error_metrics_additional_info=None, callbacks=None,
                                   compute_test_info=True, save_predictions=False,
                                   save_model=False, save_path=None,
                                   split_key=None, pretrained=False):
    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    validation_info = OrderedDict()
    test_info = OrderedDict()
    all_preds = OrderedDict()

    for fold_idx, (train_indexes, val_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, test_df = data_handle.get_split(key=split_key, key_values=val_indexes)

        preprocessor = text.Transformer(model_name, maxlen=300, classes=[0, 1])

        validation_amount = int(train_df.shape[0] * validation_percentage)
        train_amount = train_df.shape[0] - validation_amount
        train_df, val_df = train_df.iloc[:train_amount], train_df.iloc[train_amount:]

        train_data = preprocessor.preprocess_train(train_df.text.values,
                                                   train_df[data_handle.label].values)

        val_data = preprocessor.preprocess_test(val_df.text.values,
                                                val_df[data_handle.label].values)

        test_data = preprocessor.preprocess_test(test_df.text.values,
                                                 test_df[data_handle.label].values)

        kb_data = preprocessor.preprocess_test(data_handle.get_additional_info()['kb'][data_handle.label])

        # Adding KB
        train_data = KBTransformerSequence.from_transformer_sequences(train_data, kb_data)
        val_data = KBTransformerSequence.from_transformer_sequences(val_data, kb_data)
        test_data = KBTransformerSequence.from_transformer_sequences(test_data, kb_data)

        orig_model = preprocessor.get_classifier()

        model = M_MemoryDistilBERT(config=orig_model.config)
        loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
        model.compile(loss=loss_fn,
                      optimizer=tf.keras.optimizers.Adam(learning_rate=3e-5, epsilon=1e-08),
                      metrics=['accuracy'])

        # Fake input
        clause_input_ids = np.array([7, 6, 4, 0, 0]).reshape(1, 5)
        clause_attention_mask = np.array([1, 1, 1, 0, 0]).reshape(1, 5)
        clause_token_type_ids = np.zeros_like(clause_input_ids)

        memory_input_ids = np.array([[5, 4, 3, 0, 0], [8, 20, 3, 13, 0]]).reshape(1, -1, 5)
        memory_attention_mask = np.array([[1, 1, 1, 0, 0], [1, 1, 1, 1, 0]]).reshape(1, -1, 5)
        memory_token_type_ids = np.zeros_like(memory_input_ids)

        ret = model({
            'clause_input_ids': clause_input_ids,
            'clause_attention_mask': clause_attention_mask,
            'clause_token_type_ids': clause_token_type_ids,
            'memory_input_ids': memory_input_ids,
            'memory_attention_mask': memory_attention_mask,
            'memory_token_type_ids': memory_token_type_ids
        })

        # Set weights
        for original, custom in zip(orig_model._layers, model._layers):
            try:
                custom.set_weights(original.get_weights())
            except Exception as e:
                continue

        learner = TransformerTextClassLearner(model, train_data=train_data, val_data=val_data,
                                              batch_size=training_config['batch_size'])

        # computing positive label weights (for unbalanced dataset)
        class_weights = compute_class_weight(class_weight='balanced',
                                             classes=[0, 1],
                                             y=train_df[data_handle.label].values)

        if not pretrained:
            es_best_score = None
            es_best_weights = None
            es_patience = 3
            es_current_patience = 0
            for epoch in range(training_config['epochs']):
                logger.info('Epoch: {}'.format(epoch + 1))
                learner.fit(lr=5e-6, n_cycles=1, class_weight=class_weights, verbose=1)
                val_predictions = learner.model.predict(val_data.to_tfdataset(shuffle=False, repeat=False))
                val_predictions = tf.keras.activations.softmax(tf.convert_to_tensor(val_predictions)).numpy()
                val_predictions = np.argmax(val_predictions, axis=1)
                iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                                true_values=np.argmax(val_data.y, axis=1),
                                                                                predicted_values=val_predictions,
                                                                                error_metrics_additional_info=error_metrics_additional_info)
                logger.info('Validation scores: {}'.format(iteration_validation_error))
                if es_best_score is None:
                    es_best_score = iteration_validation_error['f1_score']
                    es_best_weights = learner.model.get_weights()
                else:
                    if es_best_score < iteration_validation_error['f1_score']:
                        es_best_score = iteration_validation_error['f1_score']
                        es_best_weights = learner.model.get_weights()
                        es_current_patience = 0
                    else:
                        es_current_patience += 1

                if es_current_patience >= es_patience:
                    break

            learner.model.set_weights(es_best_weights)
        else:
            filepath = os.path.join(save_path,
                                    '{0}_fold_{1}'.format(
                                        model_name,
                                        fold_idx))
            learner.model.load(filepath)

        # Inference

        # TODO: modify predict in order to allow inference callbacks
        val_predictions = learner.model.predict(val_data.to_tfdataset(shuffle=False, repeat=False))
        val_predictions = tf.keras.activations.softmax(tf.convert_to_tensor(val_predictions)).numpy()
        val_predictions = np.argmax(val_predictions, axis=1)

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=np.argmax(val_data.y, axis=1),
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions = learner.model.predict(test_data.to_tfdataset(shuffle=False, repeat=False))
        test_predictions = tf.keras.activations.softmax(tf.convert_to_tensor(test_predictions)).numpy()
        test_predictions = np.argmax(test_predictions, axis=1)

        iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                  true_values=np.argmax(test_data.y, axis=1),
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info)

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            if save_predictions:
                all_preds[fold_idx] = test_predictions

        # Save model
        if save_model:
            filepath = os.path.join(save_path,
                                    '{0}_fold_{1}'.format(
                                        model_name,
                                        fold_idx))
            learner.model.save(filepath, save_format='tf')

            # Save ground truth
            filepath = os.path.join(save_path, 'y_test_fold_{}.json'.format(fold_idx))
            if not os.path.isfile(filepath):
                save_json(filepath=filepath, data=test_data.y)

        # Flush
        K.clear_session()

    result = {
        'validation_info': validation_info
    }

    if compute_test_info:
        result['test_info'] = test_info
        result['predictions'] = all_preds

    return result


def cross_validation_forward_pass(validation_percentage, data_handle, cv,
                                  model_type, network_args, training_config,
                                  error_metrics, error_metrics_additional_info=None,
                                  callbacks=None, compute_test_info=True, save_predictions=False,
                                  save_path=None, repetition_ids=None, retrieval_metric=None,
                                  highlight_targets=False, split_key=None, build_validation=True):
    """
    Simple CV variant that retrieves a pre-trained model to do the forward pass for each val/test fold sets.
    """

    # Step 0: build metrics
    parsed_metrics = build_metrics(error_metrics=error_metrics)

    # [Step 1]: load embedding model
    if model_type in cd.MODEL_CONFIG:
        converter_type = cd.MODEL_CONFIG[model_type]['converter']
    else:
        archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
        converter_type = cd.ALGORITHM_CONFIG[archetype]['converter']
    converter_args = {key: value['value'] for key, value in network_args.items()
                      if 'converter' in value['flags']}

    # I need to know which label to look after
    converter_args['label'] = data_handle.label
    converter = ConverterFactory.factory(cl_type=converter_type, **converter_args)
    converter.load_embedding_model()

    # Load samples targets if any
    if highlight_targets:
        targets_path = os.path.join(cd.KB_DIR, '{}_targets.json'.format(data_handle.label.upper()))
        annotated_samples = load_json(targets_path)
        # annotated_samples = {filter_line(key): item
        #                      for key, item in annotated_samples.items()}

    # Step 2: cross validation
    validation_info = OrderedDict()
    test_info = OrderedDict()
    all_preds = OrderedDict()

    # Retrieve repetition ids if not given
    if repetition_ids is None:
        if retrieval_metric is None or retrieval_metric not in error_metrics:
            raise AttributeError('Invalid retrieval metric! It is required in'
                                 ' order to determine best folds. Got: {}'.format(retrieval_metric))
        loaded_val_results = load_json(os.path.join(save_path, cd.JSON_VALIDATION_INFO_NAME))
        metric_val_results = loaded_val_results[retrieval_metric]
        repetition_ids = np.argmax(metric_val_results, axis=0)

    for fold_idx, (train_indexes, val_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, test_df = data_handle.get_split(key=split_key, key_values=val_indexes)

        if highlight_targets:
            # Getting local indexes
            copied_test = test_df.copy()
            copied_test.index = np.arange(copied_test.shape[0])
            detected = copied_test[copied_test['text'].isin(annotated_samples.keys())]
            detected_info = {
                row_id: row['text'] for row_id, row in detected.iterrows()
            }
            save_json(os.path.join(save_path, 'annotated_samples_ids_fold_{}.json'.format(fold_idx)), detected_info)

        # Pre-processing
        if model_type in cd.MODEL_CONFIG:
            preprocessor_type = cd.MODEL_CONFIG[model_type]['preprocessor']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            preprocessor_type = cd.ALGORITHM_CONFIG[archetype]['preprocessor']
        preprocessor_args = {key: value['value'] for key, value in network_args.items()
                             if 'preprocessing' in value['flags']}

        preprocessor = PreprocessingFactory.factory(cl_type=preprocessor_type,
                                                    **preprocessor_args)

        train_df, additional_data = preprocessor.parse(df=train_df,
                                                       additional_info=data_handle.get_additional_info(),
                                                       return_info=True,
                                                       data_keys=data_handle.data_keys)
        test_df = preprocessor.parse(df=test_df,
                                     additional_info=data_handle.get_additional_info(),
                                     data_keys=data_handle.data_keys)

        # Data conversion
        x_train, y_train, text_info, additional_data = converter.fit_train_data(train_df=train_df,
                                                                                additional_data=additional_data)

        if text_info is not None:
            for key in text_info:
                logger.info('{0} size: {1}'.format(key, text_info[key]))

        x_test, y_test = converter.convert_data(df=test_df, text_info=text_info,
                                                additional_data=additional_data)

        # Train/Validation split
        if model_type in cd.MODEL_CONFIG:
            splitter_type = cd.MODEL_CONFIG[model_type]['splitter']
        else:
            archetype = cd.SUPPORTED_ALGORITHMS[model_type]['model_type']
            splitter_type = cd.ALGORITHM_CONFIG[archetype]['splitter']

        splitter_args = {key: value['value'] for key, value in network_args.items()
                         if 'splitter' in value['flags']}
        splitter_args['validation_split'] = validation_percentage
        splitter = SplitterFactory.factory(cl_type=splitter_type,
                                           **splitter_args)

        if build_validation:
            logger.info('Building validation split!')

            x_train, \
            y_train, \
            x_val, \
            y_val, \
            text_info, \
            x_test, \
            y_test = splitter.split(x=x_train, y=y_train, x_test=x_test, y_test=y_test,
                                    text_info=text_info,
                                    return_text_info=True,
                                    data_keys=data_handle.data_keys)
        else:
            logger.info('Retrieving fixed test set!')

            x_train, \
            y_train, \
            x_val, \
            y_val, \
            text_info, \
            x_test, \
            y_test = splitter.split_by_indexes(x=x_train,
                                               y=y_train,
                                               val_indexes=val_indexes,
                                               train_indexes=train_indexes,
                                               x_test=x_test,
                                               y_test=y_test,
                                               text_info=text_info,
                                               return_text_info=True)

        show_data_shapes(x_train, 'Train')
        show_data_shapes(x_val, 'Validation')
        show_data_shapes(x_test, 'Test')

        network_retrieved_args = {key: value['value'] for key, value in network_args.items()
                                  if 'model_class' in value['flags']}
        network_retrieved_args['additional_data'] = additional_data
        network_retrieved_args['name'] = '{0}_fold_{1}'.format(
            cd.SUPPORTED_ALGORITHMS[model_type]['save_suffix'], fold_idx)
        network = ModelFactory.factory(cl_type=model_type, **network_retrieved_args)

        # computing positive label weights (for unbalanced dataset)
        network.compute_output_weights(y_train=y_train, num_classes=data_handle.num_classes)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_begin'):
                callback.on_build_model_begin(logs={'network': network})

        network.build_model(text_info=text_info)

        # Custom callbacks only
        for callback in callbacks:
            if hasattr(callback, 'on_build_model_end'):
                callback.on_build_model_end(logs={'network': network})

        # TODO: how to fix this?
        # Setup model by feeding an input
        network.predict(x=x_val, batch_size=training_config['batch_size'])

        # load pre-trained weights
        current_weight_filename = '{0}_repetition_{1}_fold_{2}.h5'.format(model_type,
                                                                          repetition_ids[fold_idx],
                                                                          fold_idx)
        network.load(os.path.join(save_path, current_weight_filename))

        # Inference
        val_predictions = network.predict(x=x_val,
                                          batch_size=training_config['batch_size'])

        iteration_validation_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                        true_values=y_val,
                                                                        predicted_values=val_predictions,
                                                                        error_metrics_additional_info=error_metrics_additional_info)

        validation_info = update_cv_validation_info(test_validation_info=validation_info,
                                                    iteration_validation_info=iteration_validation_error)

        logger.info('Iteration validation info: {}'.format(iteration_validation_error))

        test_predictions = network.predict(x=x_test,
                                           batch_size=training_config['batch_size'],
                                           callbacks=callbacks)

        iteration_test_error = compute_iteration_validation_error(parsed_metrics=parsed_metrics,
                                                                  true_values=y_test,
                                                                  predicted_values=test_predictions,
                                                                  error_metrics_additional_info=error_metrics_additional_info)

        if compute_test_info:
            test_info = update_cv_validation_info(test_validation_info=test_info,
                                                  iteration_validation_info=iteration_test_error)
            logger.info('Iteration test info: {}'.format(iteration_test_error))

            if save_predictions:
                all_preds[fold_idx] = test_predictions.ravel()

        # Flush
        K.clear_session()

    result = {
        'validation_info': validation_info,
    }

    if compute_test_info:
        result['test_info'] = test_info
        if save_predictions:
            result['predictions'] = all_preds
    else:
        if save_predictions:
            result['predictions'] = all_preds

    return result


def cross_validation_predictions_comparison(data_handle, cv, model_type,
                                            predictions, retrieval_metric, error_metrics,
                                            save_path=None, repetition_ids=None, split_key=None):
    """
    Cross-validation forward-pass variant that retrieves attention weights for val/test predictions.
    """

    # Retrieve repetition ids if not given
    if repetition_ids is None:
        if retrieval_metric is None or retrieval_metric not in error_metrics:
            raise AttributeError('Invalid retrieval metric! It is required in'
                                 ' order to determine best folds. Got: {}'.format(retrieval_metric))
        loaded_val_results = load_json(os.path.join(save_path, cd.JSON_VALIDATION_INFO_NAME))
        metric_val_results = loaded_val_results[retrieval_metric]
        repetition_ids = np.argmax(metric_val_results, axis=0)

    for fold_idx, (train_indexes, val_indexes) in enumerate(cv.split(None)):
        logger.info('Starting Fold {0}/{1}'.format(fold_idx + 1, cv.n_splits))

        train_df, test_df = data_handle.get_split(key=split_key, key_values=val_indexes)

        # Load fold predictions
        fold_repetition_id = repetition_ids[fold_idx]
        fold_predictions = predictions[str(fold_idx)][fold_repetition_id]

        # Retrieve samples IDs that are unfair and for which the model predicted correctly
        labels = test_df[data_handle.label].values
        correct_unfair = np.logical_and((labels == fold_predictions), (labels == 1))
        correct_unfair_texts = test_df[correct_unfair]['text'].values
        correct_unfair_ids = np.argwhere(correct_unfair).ravel()

        # Load model's attention for fold
        attention_path = os.path.join(save_path,
                                      '{0}_fold_{1}_attention_weights.json'.format(model_type, fold_idx))
        fold_attention = load_json(attention_path)

        # Get attention for samples of interest
        selected_attention = fold_attention[correct_unfair]

        # Get selected memories
        selected_attention = np.argmax(selected_attention, axis=2)

        # Save retrieved samples long with their model's attention (selected memories)
        to_save = {int(unfair_id): {'text': unfair_text, 'attention': unfair_attention}
                   for unfair_id, unfair_text, unfair_attention in zip(correct_unfair_ids,
                                                                       correct_unfair_texts,
                                                                       selected_attention)}
        save_json(os.path.join(save_path, 'correct_unfair_info_fold_{}.json'.format(fold_idx)), to_save)

        # Retrieve samples IDs that are unfair and for which the model predicted wrongly
        wrong_unfair = np.logical_and((labels != fold_predictions), (labels == 1))
        wrong_unfair_texts = test_df[wrong_unfair]['text'].values
        wrong_unfair_ids = np.argwhere(wrong_unfair).ravel()

        # Get selected memories
        wrong_attention = fold_attention[wrong_unfair]
        wrong_attention = np.argmax(wrong_attention, axis=2)

        # Save retrieved samples long with their model's attention (selected memories)
        to_save = {int(unfair_id): {'text': unfair_text, 'attention': unfair_attention}
                   for unfair_id, unfair_text, unfair_attention in zip(wrong_unfair_ids,
                                                                       wrong_unfair_texts,
                                                                       wrong_attention)}
        save_json(os.path.join(save_path, 'wrong_unfair_info_fold_{}.json'.format(fold_idx)), to_save)
