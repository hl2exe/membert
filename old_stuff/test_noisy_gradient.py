"""

@Author: cpury (github.com/cpury)

@Date: 17/12/18

"""

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from custom_keras import add_gradient_noise

if __name__ == '__main__':

    noisy_adam = add_gradient_noise(Adam)

    x = np.array([[0.1], [0.5], [0.8], [0.3]])
    y = np.array([[0.9], [0.5], [0.2], [0.7]])

    model = Sequential()
    model.add(Dense(1, input_shape=(1,)))
    model.compile(optimizer=noisy_adam(), loss='mse')
    model.fit(x, y, epochs=1, batch_size=2, verbose=0)
