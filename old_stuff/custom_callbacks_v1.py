"""

@Author: Federico Ruggeri

@Date: 08/01/19

"""

import os
import warnings

import numpy as np
from keras import backend as K
from keras.callbacks import Callback
from keras.engine.training_utils import standardize_input_data

from nn_models_v1 import Network
from utility.json_utils import save_json
from utility.log_utils import get_logger

logger = get_logger(__name__)


class TensorBoard(Callback):
    """TensorBoard basic visualizations.
    [TensorBoard](https://www.tensorflow.org/guide/summaries_and_tensorboard)
    is a visualization tool provided with TensorFlow.
    This callback writes a log for TensorBoard, which allows
    you to visualize dynamic graphs of your training and test
    metrics, as well as activation histograms for the different
    layers in your model.
    If you have installed TensorFlow with pip, you should be able
    to launch TensorBoard from the command line:
    ```sh
    tensorboard --logdir=/full_path_to_your_logs
    ```
    When using a backend other than TensorFlow, TensorBoard will still work
    (if you have TensorFlow installed), but the only feature available will
    be the display of the losses and metrics plots.
    # Arguments
        log_dir: the path of the directory where to save the log
            files to be parsed by TensorBoard.
        histogram_freq: frequency (in epochs) at which to compute activation
            and weight histograms for the layers of the model. If set to 0,
            histograms won't be computed. Validation data (or split) must be
            specified for histogram visualizations.
        batch_size: size of batch of inputs to feed to the network
            for histograms computation.
        write_graph: whether to visualize the graph in TensorBoard.
            The log file can become quite large when
            write_graph is set to True.
        write_grads: whether to visualize gradient histograms in TensorBoard.
            `histogram_freq` must be greater than 0.
        write_images: whether to write model weights to visualize as
            image in TensorBoard.
        embeddings_freq: frequency (in epochs) at which selected embedding
            layers will be saved. If set to 0, embeddings won't be computed.
            Data to be visualized in TensorBoard's Embedding tab must be passed
            as `embeddings_data`.
        embeddings_layer_names: a list of names of layers to keep eye on. If
            None or empty list all the embedding layer will be watched.
        embeddings_metadata: a dictionary which maps layer name to a file name
            in which metadata for this embedding layer is saved. See the
            [details](https://www.tensorflow.org/guide/embedding#metadata)
            about metadata files format. In case if the same metadata file is
            used for all embedding layers, string can be passed.
        embeddings_data: data to be embedded at layers specified in
            `embeddings_layer_names`. Numpy array (if the model has a single
            input) or list of Numpy arrays (if the model has multiple inputs).
            Learn [more about embeddings](
            https://www.tensorflow.org/guide/embedding).
        update_freq: `'batch'` or `'epoch'` or integer. When using `'batch'`, writes
            the losses and metrics to TensorBoard after each batch. The same
            applies for `'epoch'`. If using an integer, let's say `10000`,
            the callback will write the metrics and losses to TensorBoard every
            10000 samples. Note that writing too frequently to TensorBoard
            can slow down your training.
    """

    def __init__(self, log_dir='./logs',
                 histogram_freq=0,
                 batch_size=32,
                 write_graph=True,
                 write_grads=False,
                 write_images=False,
                 embeddings_freq=0,
                 embeddings_layer_names=None,
                 embeddings_metadata=None,
                 embeddings_data=None,
                 visualization_freq=0,
                 update_freq='epoch'):
        super(TensorBoard, self).__init__()
        global tf, projector
        try:
            import tensorflow as tf
            from tensorflow.contrib.tensorboard.plugins import projector
        except ImportError:
            raise ImportError('You need the TensorFlow module installed to '
                              'use TensorBoard.')

        if K.backend() != 'tensorflow':
            if histogram_freq != 0:
                warnings.warn('You are not using the TensorFlow backend. '
                              'histogram_freq was set to 0')
                histogram_freq = 0
            if write_graph:
                warnings.warn('You are not using the TensorFlow backend. '
                              'write_graph was set to False')
                write_graph = False
            if write_images:
                warnings.warn('You are not using the TensorFlow backend. '
                              'write_images was set to False')
                write_images = False
            if embeddings_freq != 0:
                warnings.warn('You are not using the TensorFlow backend. '
                              'embeddings_freq was set to 0')
                embeddings_freq = 0

        self.log_dir = log_dir
        self.histogram_freq = histogram_freq
        self.merged = None
        self.write_graph = write_graph
        self.write_grads = write_grads
        self.write_images = write_images
        self.embeddings_freq = embeddings_freq
        self.embeddings_layer_names = embeddings_layer_names
        self.embeddings_metadata = embeddings_metadata or {}
        self.batch_size = batch_size
        self.embeddings_data = embeddings_data
        if update_freq == 'batch':
            # It is the same as writing as frequently as possible.
            self.update_freq = 1
        else:
            self.update_freq = update_freq
        self.samples_seen = 0
        self.samples_seen_at_last_write = 0
        self.visualization_freq = visualization_freq

    def _keras_set_model(self):
        for layer in self.model.layers:
            for weight in layer.weights:
                mapped_weight_name = weight.name.replace(':', '_')
                tf.summary.histogram(mapped_weight_name, weight)
                if self.write_grads and weight in layer.trainable_weights:
                    grads = self.model.optimizer.get_gradients(self.model.total_loss,
                                                               weight)

                    def is_indexed_slices(grad):
                        return type(grad).__name__ == 'IndexedSlices'

                    grads = [
                        grad.values if is_indexed_slices(grad) else grad
                        for grad in grads]
                    tf.summary.histogram('{}_grad'.format(mapped_weight_name),
                                         grads)
                if self.write_images:
                    w_img = tf.squeeze(weight)
                    shape = K.int_shape(w_img)
                    if len(shape) == 2:  # dense layer kernel case
                        if shape[0] > shape[1]:
                            w_img = tf.transpose(w_img)
                            shape = K.int_shape(w_img)
                        w_img = tf.reshape(w_img, [1,
                                                   shape[0],
                                                   shape[1],
                                                   1])
                    elif len(shape) == 3:  # convnet case
                        if K.image_data_format() == 'channels_last':
                            # switch to channels_first to display
                            # every kernel as a separate image
                            w_img = tf.transpose(w_img, perm=[2, 0, 1])
                            shape = K.int_shape(w_img)
                        w_img = tf.reshape(w_img, [shape[0],
                                                   shape[1],
                                                   shape[2],
                                                   1])
                    elif len(shape) == 1:  # bias case
                        w_img = tf.reshape(w_img, [1,
                                                   shape[0],
                                                   1,
                                                   1])
                    else:
                        # not possible to handle 3D convnets etc.
                        continue

                    shape = K.int_shape(w_img)
                    assert len(shape) == 4 and shape[-1] in [1, 3, 4]
                    tf.summary.image(mapped_weight_name, w_img)

            if hasattr(layer, 'output'):
                if isinstance(layer.output, list):
                    for i, output in enumerate(layer.output):
                        tf.summary.histogram('{}_out_{}'.format(layer.name, i),
                                             output)
                else:
                    tf.summary.histogram('{}_out'.format(layer.name),
                                         layer.output)

    def tf_set_model(self):
        for weight in self.model.get_named_weights():
            mapped_weight_name = weight.name.replace(':', '_')
            tf.summary.histogram(mapped_weight_name, weight)

            if self.write_images:
                w_img = tf.squeeze(weight)
                shape = K.int_shape(w_img)
                if len(shape) == 2:  # dense layer kernel case
                    if shape[0] > shape[1]:
                        w_img = tf.transpose(w_img)
                        shape = K.int_shape(w_img)
                    w_img = tf.reshape(w_img, [1,
                                               shape[0],
                                               shape[1],
                                               1])
                elif len(shape) == 3:  # convnet case
                    if K.image_data_format() == 'channels_last':
                        # switch to channels_first to display
                        # every kernel as a separate image
                        w_img = tf.transpose(w_img, perm=[2, 0, 1])
                        shape = K.int_shape(w_img)
                    w_img = tf.reshape(w_img, [shape[0],
                                               shape[1],
                                               shape[2],
                                               1])
                elif len(shape) == 1:  # bias case
                    w_img = tf.reshape(w_img, [1,
                                               shape[0],
                                               1,
                                               1])
                else:
                    # not possible to handle 3D convnets etc.
                    continue

                shape = K.int_shape(w_img)
                assert len(shape) == 4 and shape[-1] in [1, 3, 4]
                tf.summary.image(mapped_weight_name, w_img)

        def is_indexed_slices(grad):
            return type(grad[0]).__name__ == 'IndexedSlices'

        if self.write_grads:
            grads = self.model.opt.compute_gradients(self.model.loss_op)
            grads = [
                (grad[0].values, grad[1]) if is_indexed_slices(grad) else grad
                for grad in grads]
            for grad, grad_var in grads:
                tf.summary.histogram('{}_grad'.format(grad_var.name),
                                     grad)

    def set_model(self, model):
        self.model = model
        if K.backend() == 'tensorflow':
            self.sess = K.get_session()

        if self.histogram_freq and self.merged is None:
            if isinstance(self.model, Network):
                self.tf_set_model()
                self.model.build_tensorboard_info()
            else:
                self._keras_set_model()

        self.merged = tf.summary.merge_all()

        if self.write_graph:
            self.writer = tf.summary.FileWriter(self.log_dir,
                                                self.sess.graph)
        else:
            self.writer = tf.summary.FileWriter(self.log_dir)

        if self.embeddings_freq and self.embeddings_data is not None:
            self.embeddings_data = standardize_input_data(self.embeddings_data,
                                                          model.input_names)

            embeddings_layer_names = self.embeddings_layer_names

            if not embeddings_layer_names:
                embeddings_layer_names = [layer.name for layer in self.model.layers
                                          if type(layer).__name__ == 'Embedding']
            self.assign_embeddings = []
            embeddings_vars = {}

            self.batch_id = batch_id = tf.placeholder(tf.int32)
            self.step = step = tf.placeholder(tf.int32)

            for layer in self.model.layers:
                if layer.name in embeddings_layer_names:
                    embedding_input = self.model.get_layer(layer.name).output
                    embedding_size = np.prod(embedding_input.shape[1:])
                    embedding_input = tf.reshape(embedding_input,
                                                 (step, int(embedding_size)))
                    shape = (self.embeddings_data[0].shape[0], int(embedding_size))
                    embedding = tf.Variable(tf.zeros(shape),
                                            name=layer.name + '_embedding')
                    embeddings_vars[layer.name] = embedding
                    batch = tf.assign(embedding[batch_id:batch_id + step],
                                      embedding_input)
                    self.assign_embeddings.append(batch)

            self.saver = tf.train.Saver(list(embeddings_vars.values()))

            if not isinstance(self.embeddings_metadata, str):
                embeddings_metadata = self.embeddings_metadata
            else:
                embeddings_metadata = {layer_name: self.embeddings_metadata
                                       for layer_name in embeddings_vars.keys()}

            config = projector.ProjectorConfig()

            for layer_name, tensor in embeddings_vars.items():
                embedding = config.embeddings.add()
                embedding.tensor_name = tensor.name

                if layer_name in embeddings_metadata:
                    embedding.metadata_path = embeddings_metadata[layer_name]

            projector.visualize_embeddings(self.writer, config)

    def _keras_validate(self, epoch):
        if epoch % self.histogram_freq == 0:

            val_data = self.validation_data
            tensors = (self.model.inputs +
                       self.model.targets +
                       self.model.sample_weights)

            if self.model.uses_learning_phase:
                tensors += [K.learning_phase()]

            assert len(val_data) == len(tensors)
            val_size = val_data[0].shape[0]
            i = 0
            while i < val_size:
                step = min(self.batch_size, val_size - i)
                if self.model.uses_learning_phase:
                    # do not slice the learning phase
                    batch_val = [x[i:i + step] for x in val_data[:-1]]
                    batch_val.append(val_data[-1])
                else:
                    batch_val = [x[i:i + step] for x in val_data]
                assert len(batch_val) == len(tensors)
                feed_dict = dict(zip(tensors, batch_val))
                result = self.sess.run([self.merged], feed_dict=feed_dict)
                summary_str = result[0]
                self.writer.add_summary(summary_str, epoch)
                i += self.batch_size

    def _tf_validate(self, epoch):
        if epoch % self.histogram_freq == 0:
            batches_x, num_batches = self.model.get_batches(data=self.model.validation_data[0])
            batches_y = self.model.get_batches(data=self.model.validation_data[1], num_batches=num_batches)

            for batch_idx in range(num_batches):

                if type(self.model.validation_data[0]) is list:
                    batch_x = [batches_x[item][batch_idx] for item in range(len(self.model.validation_data[0]))]
                elif type(self.model.validation_data[0]) is np.ndarray:
                    batch_x = batches_x[batch_idx]
                else:
                    batch_x = {key: item[batch_idx] for key, item in batches_x.items()}

                if type(self.model.validation_data[1]) is list:
                    batch_y = [batches_y[item][batch_idx] for item in range(len(self.model.validation_data[1]))]
                else:
                    batch_y = batches_y[batch_idx]

                feed_dict = self.model.get_feed_dict(x=batch_x,
                                                     y=batch_y,
                                                     phase='validation')

                result = self.model.session.run([self.merged], feed_dict)
                summary_str = result[0]
                self.writer.add_summary(summary_str, epoch)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        if not self.model.validation_data and self.histogram_freq:
            raise ValueError("If printing histograms, validation_data must be "
                             "provided, and cannot be a generator.")
        if self.embeddings_data is None and self.embeddings_freq:
            raise ValueError("To visualize embeddings, embeddings_data must "
                             "be provided.")
        if self.model.validation_data and self.histogram_freq:
            if isinstance(self.model, Network):
                self._tf_validate(epoch)
            else:
                self._keras_validate(epoch)

        if self.embeddings_freq and self.embeddings_data is not None:
            if epoch % self.embeddings_freq == 0:
                # We need a second forward-pass here because we're passing
                # the `embeddings_data` explicitly. This design allows to pass
                # arbitrary data as `embeddings_data` and results from the fact
                # that we need to know the size of the `tf.Variable`s which
                # hold the embeddings in `set_model`. At this point, however,
                # the `validation_data` is not yet set.

                # More details in this discussion:
                # https://github.com/keras-team/keras/pull/7766#issuecomment-329195622

                embeddings_data = self.embeddings_data
                n_samples = embeddings_data[0].shape[0]

                i = 0
                while i < n_samples:
                    step = min(self.batch_size, n_samples - i)
                    batch = slice(i, i + step)

                    if type(self.model.input) == list:
                        feed_dict = {_input: embeddings_data[idx][batch]
                                     for idx, _input in enumerate(self.model.input)}
                    else:
                        feed_dict = {self.model.input: embeddings_data[0][batch]}

                    feed_dict.update({self.batch_id: i, self.step: step})

                    if self.model.uses_learning_phase:
                        feed_dict[K.learning_phase()] = False

                    self.sess.run(self.assign_embeddings, feed_dict=feed_dict)
                    self.saver.save(self.sess,
                                    os.path.join(self.log_dir,
                                                 'keras_embedding.ckpt'),
                                    epoch)

                    i += self.batch_size

        if self.update_freq == 'epoch':
            index = epoch
        else:
            index = self.samples_seen
        self._write_logs(logs, index)

    def _write_logs(self, logs, index):
        for name, value in logs.items():
            if name in ['batch', 'size', 'train_x', 'train_y', 'batch_size']:
                continue

            summary = tf.Summary()
            summary_value = summary.value.add()
            if isinstance(value, np.ndarray):
                summary_value.simple_value = value.item()
            else:
                summary_value.simple_value = value
            summary_value.tag = name
            self.writer.add_summary(summary, index)

        # if index > 0 and index % self.visualization_freq == 0:
        #     attentions, memory_indices = self.model.get_attentions_weights(x=logs['train_x'],
        #                                                                    batch_size=logs['batch_size'])
        #     with open(os.path.join(self.log_dir, 'attentions_{}.pickle'.format(index)), 'wb') as f:
        #         pickle.dump(attentions['memory_slots'], f)
        #     if self.model.use_gates:
        #         with open(os.path.join(self.log_dir, 'gates_{}.pickle'.format(index)), 'wb') as f:
        #             pickle.dump(attentions['gates'], f)
        #     with open(os.path.join(self.log_dir, 'indexes_{}.pickle'.format(index)), 'wb') as f:
        #         pickle.dump(memory_indices, f)
        #     if not os.path.isfile(os.path.join(self.log_dir, 'true_values.pickle')):
        #         with open(os.path.join(self.log_dir, 'true_values.pickle'), 'wb') as f:
        #             pickle.dump(logs['train_y'], f)

        self.writer.flush()

    def on_train_end(self, logs=None):
        self.writer.close()

    def on_batch_end(self, batch, logs=None):
        if self.update_freq != 'epoch':
            self.samples_seen += logs['size']
            samples_seen_since = self.samples_seen - self.samples_seen_at_last_write
            if samples_seen_since >= self.update_freq:
                self._write_logs(logs, self.samples_seen)
                self.samples_seen_at_last_write = self.samples_seen


class ReduceLROnPlateau(Callback):
    """Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This callback monitors a
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.
    # Example
    ```python
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                  patience=5, min_lr=0.001)
    model.fit(X_train, Y_train, callbacks=[reduce_lr])
    ```
    # Arguments
        monitor: quantity to be monitored.
        factor: factor by which the learning rate will
            be reduced. new_lr = lr * factor
        patience: number of epochs with no improvement
            after which learning rate will be reduced.
        verbose: int. 0: quiet, 1: update messages.
        mode: one of {auto, min, max}. In `min` mode,
            lr will be reduced when the quantity
            monitored has stopped decreasing; in `max`
            mode it will be reduced when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        min_delta: threshold for measuring the new optimum,
            to only focus on significant changes.
        cooldown: number of epochs to wait before resuming
            normal operation after lr has been reduced.
        min_lr: lower bound on the learning rate.
    """

    def __init__(self, monitor='val_loss', factor=0.1, patience=10,
                 verbose=0, mode='auto', min_delta=1e-4, cooldown=0, min_lr=0,
                 **kwargs):
        super(ReduceLROnPlateau, self).__init__()

        self.monitor = monitor
        if factor >= 1.0:
            raise ValueError('ReduceLROnPlateau '
                             'does not support a factor >= 1.0.')
        if 'epsilon' in kwargs:
            min_delta = kwargs.pop('epsilon')
            warnings.warn('`epsilon` argument is deprecated and '
                          'will be removed, use `min_delta` instead.')
        self.factor = factor
        self.min_lr = min_lr
        self.min_delta = min_delta
        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0
        self.best = 0
        self.mode = mode
        self.monitor_op = None
        self._reset()

    def _reset(self):
        """Resets wait counter and cooldown counter.
        """
        if self.mode not in ['auto', 'min', 'max']:
            warnings.warn('Learning Rate Plateau Reducing mode %s is unknown, '
                          'fallback to auto mode.' % (self.mode),
                          RuntimeWarning)
            self.mode = 'auto'
        if (self.mode == 'min' or
                (self.mode == 'auto' and 'acc' not in self.monitor)):
            self.monitor_op = lambda a, b: np.less(a, b - self.min_delta)
            self.best = np.Inf
        else:
            self.monitor_op = lambda a, b: np.greater(a, b + self.min_delta)
            self.best = -np.Inf
        self.cooldown_counter = 0
        self.wait = 0

    def on_train_begin(self, logs=None):
        self._reset()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = self.model.get_lr()
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn(
                'Reduce LR on plateau conditioned on metric `%s` '
                'which is not available. Available metrics are: %s' %
                (self.monitor, ','.join(list(logs.keys()))), RuntimeWarning
            )

        else:
            if self.in_cooldown():
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif not self.in_cooldown():
                self.wait += 1
                if self.wait >= self.patience:
                    old_lr = float(self.model.get_lr())
                    if old_lr > self.min_lr:
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        self.model.set_lr(new_lr=new_lr)
                        if self.verbose > 0:
                            print('\nEpoch %05d: ReduceLROnPlateau reducing '
                                  'learning rate to %s.' % (epoch + 1, new_lr))
                        self.cooldown_counter = self.cooldown
                        self.wait = 0

    def in_cooldown(self):
        return self.cooldown_counter > 0


class AttentionRetriever(Callback):
    """
    Simple callback that allows to extract and save attention tensors during prediction phase.
    Extraction is simply implemented as attribute inspection.
    """

    def __init__(self, save_path, save_suffix=None):
        super(Callback, self).__init__()
        self.start_monitoring = False
        self.stored_memory_attention = None
        self.save_path = save_path
        self.save_suffix = save_suffix

    def on_build_model_begin(self, logs=None):
        self.network = logs['network']
        self.network.accumulate_attention = True

    def on_prediction_begin(self, logs=None):
        self.start_monitoring = True

    def on_batch_prediction_begin(self, batch, logs=None):
        if self.start_monitoring:
            batch_x = logs['batch']
            feed_dict = self.network.get_feed_dict(x=batch_x, y=None, phase='test')
            memory_attention = self.network.session.run(self.network.probs, feed_dict)
            memory_attention = np.array(memory_attention).reshape(-1,
                                                                  len(memory_attention),
                                                                  memory_attention[0].shape[-1])

            if batch == 0:
                # [batch_size, hops, mem_size]
                self.stored_memory_attention = memory_attention
            else:
                # [samples, hops, mem_size]
                self.stored_memory_attention = np.append(self.stored_memory_attention, memory_attention, axis=0)

    def on_prediction_end(self, logs=None):

        if self.start_monitoring:
            # Saving
            filepath = os.path.join(self.save_path, '{0}_{1}_attention_weights.json'.format(self.network.name, self.save_suffix))
            save_json(filepath=filepath, data=self.stored_memory_attention)

            # Resetting
            self.start_monitoring = None
            del self.stored_memory_attention


class TrainingLogger(Callback):

    def __init__(self, filepath, suffix=None, **kwargs):
        super(TrainingLogger, self).__init__(**kwargs)
        self.filepath = filepath
        self.suffix = suffix
        self.info = {}

        if not os.path.isdir(self.filepath):
            os.makedirs(self.filepath)

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            assert type(logs) == dict

            for key, item in logs.items():
                self.info.setdefault(key, []).append(item)

    def on_train_end(self, logs=None):
        if logs is not None and 'name' in logs:
            self.suffix = logs['name']

        if self.suffix is None:
            filename = 'info.npy'
        else:
            filename = '{}_info.npy'.format(self.suffix)
        savepath = os.path.join(self.filepath, filename)

        np.save(savepath, self.info)

        self.info = {}