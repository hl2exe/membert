"""

@Author: Federico Ruggeri

@Date: 22/07/19

TODO: fix DLU converters
TODO: generalize embeddings routine: ELMo/BERT and more traditional embeddings should go together.
TODO: re-write BoW converters. Add pre-load for POS tags

"""

import os

import nltk
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm

import const_define as cd
from utility.bert_utils import FullTokenizer
from utility.embedding_utils import build_embeddings_matrix, pad_data, \
    load_embedding_model
from utility.json_utils import load_json
from utility.log_utils import get_logger
from utility.preprocessing_utils import windowing, group_data_including, group_data_excluding

logger = get_logger(__name__)


# TODO: add support for OOV tokens (more flexible)
class BaseConverter(object):
    """
    Base converter interface. Text and labels are converted into numerical format (if needed).
    """

    def __init__(self, label, build_embedding_matrix=False,
                 embedding_model_type="",
                 embedding_dimension=32,
                 padding=True):
        self.label = label
        self.build_embedding_matrix = build_embedding_matrix
        self.embedding_model = None
        self.embedding_model_type = embedding_model_type
        self.embedding_dimension = embedding_dimension
        self.padding = padding

    def load_embedding_model(self):
        if self.build_embedding_matrix:
            self.embedding_model = load_embedding_model(model_type=self.embedding_model_type,
                                                        embedding_dimension=self.embedding_dimension)

    def fit_train_data(self, train_df, additional_data=None):

        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_data = self.tokenizer.texts_to_sequences(data_to_fit)
        padding_max_length = max([len(row) for row in encoded_data])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       word_to_idx=self.tokenizer.word_index)

        text_info = {'padding_max_length': padding_max_length,
                     'vocab_size': vocab_size,
                     'embedding_matrix': embedding_matrix}

        x_train, y_train, text_info = self.convert_data(
            df=train_df,
            text_info=text_info,
            return_text_info=True,
            additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        """
        I'm expecting the output of a BasePrepocessor -> default x and y
        """

        text = df['text'].values
        text = self.tokenizer.texts_to_sequences(text)

        label = df[self.label].values

        if self.padding:
            text = pad_data(data=text,
                            padding_length=text_info['padding_max_length'],
                            padding='post')

        if return_text_info:
            return {'text': text}, label, text_info
        else:
            return {'text': text}, label


class Task2BaseConverter(BaseConverter):

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        text = self.tokenizer.texts_to_sequences(text)

        targets = df[self.label].values
        targets = [[int(item) for item in target_set[1:-1].split(',')] if type(target_set) != float else None
                   for target_set in targets]
        targets = [[1 if idx in target_set else 0 for idx in range(max(target_set))] if target_set is not None else [0]
                   for target_set in targets]
        targets = pad_data(targets, padding='post', dtype=np.int32)

        if return_text_info:
            return {'text': text}, targets, text_info
        else:
            return {'text': text}, targets


class ContextConverter(BaseConverter):
    """
    Context-aware converter. If context_dim is greater than 1, a context additional input is defined.
    Otherwise, this converter behaves just like the BaseConverter.
    """

    def __init__(self, context_dim, mode, **kwargs):
        super(ContextConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        sentence_max_length = max([len(row) for row in encoded_query])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        text_info = {
            'sentence_max_length': sentence_max_length,
            'query_max_length': sentence_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix,
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        text = self.tokenizer.texts_to_sequences(text)

        category = df['category'].values

        if self.padding:
            text = pad_data(data=text,
                            padding_length=text_info['query_max_length'],
                            padding='post')

        label = df[self.label].values

        if self.context_dim > 1:
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=text,
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)

            context = df['context'].values

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                return {'text': text, 'context': context}, label, text_info
            else:
                return {'text': text, 'context': context}, label

        if return_text_info:
            return {'text': text, 'category': category}, label, text_info
        else:
            return {'text': text, 'category': category}, label


class Task2ContextConverter(Task2BaseConverter):

    def __init__(self, context_dim, mode, **kwargs):
        super(Task2ContextConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        sentence_max_length = max([len(row) for row in encoded_query])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        text_info = {
            'sentence_max_length': sentence_max_length,
            'query_max_length': sentence_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix,
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        text = self.tokenizer.texts_to_sequences(text)

        category = df['category'].values

        if self.padding:
            text = pad_data(data=text,
                            padding_length=text_info['query_max_length'],
                            padding='post')

        targets = df[self.label].values
        targets = [[int(item) for item in target_set[1:-1].split(',')] if type(target_set) != float else None
                   for target_set in targets]
        targets = [[1 if idx in target_set else 0 for idx in range(max(target_set))] if target_set is not None else [0]
                   for target_set in targets]
        targets = pad_data(targets, padding='post', dtype=np.int32, padding_length=additional_data['max_targets'])

        if self.context_dim > 1:
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=text,
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)

            context = df['context'].values

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                return {'text': text, 'context': context}, targets, text_info
            else:
                return {'text': text, 'context': context}, targets

        if return_text_info:
            return {'text': text, 'category': category}, targets, text_info
        else:
            return {'text': text, 'category': category}, targets


@DeprecationWarning
class DLUContextConverter(ContextConverter):

    def __init__(self, key_vocab, **kwargs):
        super(DLUContextConverter, self).__init__(**kwargs)
        self.key_vocab = key_vocab
        self.aggregation_mapping = {
            'JJ': 'J',
            'JJR': 'J',
            'JJS': 'J',
            'RB': 'R',
            'RP': 'R',
            'RBS': 'R',
            'RBR': 'R',
            'WDT': 'W',
            'WP': 'W',
            'WPZ': 'W',
            'WRB': 'W',
            'WP$': 'W',
            'NN': 'N',
            'NNS': 'N',
            'NNSZ': 'N',
            'NP': 'N',
            'NPS': 'N',
            'NNP': 'N',
            'NNPS': 'N',
            'NPSZ': 'N',
            'NPZ': 'N',
            'PP': 'P',
            'PPZ': 'P',
            'PRP': 'P',
            'PRP$': 'P',
            'VB': 'V',
            'VBD': 'V',
            'VBG': 'V',
            'VBN': 'V',
            'VBP': 'V',
            'VBZ': 'V',
            'VH': 'V',
            'VHD': 'V',
            'VHG': 'V',
            'VHN': 'V',
            'VHP': 'V',
            'VHZ': 'V',
            'VV': 'V',
            'VVD': 'V',
            'VVG': 'V',
            'VVN': 'V',
            'VVP': 'V',
            'VVZ': 'V'
        }
        if self.context_dim != 1:
            raise RuntimeError('Context is disabled for DLU!')
        if os.path.isfile('loaded_pos_tags.npy'):
            self.loaded_pos_tags = np.load('loaded_pos_tags.npy', allow_pickle=True).item()
        else:
            self.loaded_pos_tags = {}

    def build_pos_tags(self, data):

        pos_tags = []
        for item in tqdm(data):
            key = ''.join(item)
            if key in self.loaded_pos_tags:
                pos_tags.append(self.loaded_pos_tags[key])
            else:
                item_tokens = item.split(' ')
                tags = nltk.pos_tag(item_tokens)
                # TODO: check if some POS tags are left out
                tags = [self.aggregation_mapping[tup[1]]
                        if tup[1] in self.aggregation_mapping else tup[1]
                        for tup in tags]
                self.loaded_pos_tags[key] = tags
                pos_tags.append(tags)

        np.save('loaded_pos_tags', self.loaded_pos_tags)
        return pos_tags

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values

        text_pos = self.build_pos_tags(data=text)
        text_unification_masks = [[1 if tag == vocab_key else 0 for tag in item]
                                  for item in text_pos
                                  for vocab_key in self.key_vocab]

        text = self.tokenizer.texts_to_sequences(text)
        text_lengths = np.array([len(item) for item in text], dtype=np.int32)

        if self.padding:
            text = pad_data(data=text,
                            padding_length=text_info['query_max_length'],
                            padding='post')
            text_unification_masks = pad_data(data=text_unification_masks,
                                              padding_length=text_info['query_max_length'],
                                              padding='post')
            text_unification_masks = text_unification_masks.astype(np.float32)
            text_unification_masks = np.reshape(text_unification_masks, [-1,
                                                                         len(self.key_vocab),
                                                                         text_info['query_max_length']])

        label = df[self.label].values

        if self.context_dim > 1:
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=text,
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)

            context = df['context'].values

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                return {'text': text, 'context': context}, label, text_info
            else:
                return {'text': text, 'context': context}, label

        if return_text_info:
            return {'text': text,
                    'text_unification_masks': text_unification_masks,
                    'text_lengths': text_lengths
                    }, label, text_info
        else:
            return {'text': text,
                    'text_unification_masks': text_unification_masks,
                    'text_lengths': text_lengths
                    }, label


class KBContextConverter(ContextConverter):
    """
    KB-aware converter. Additional info (KB) stored in the DataHandle instance is considered during the
    tokenizer fitting phase. Moreover, the loaded KB is converted and maintained as additional info.
    It will be later accessed during each batch feeding. This prevents huge memory usage.
    """

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        kb_sentences = [sent for seq in list(additional_data['kb'].values()) for sent in seq]
        data_to_fit = np.append(data_to_fit, kb_sentences)
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        query_max_length = max([len(row) for row in encoded_query])

        encoded_kb_sentences = self.tokenizer.texts_to_sequences(kb_sentences)
        sentence_max_length = max([len(seq) for seq in encoded_kb_sentences])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        # Converting KB
        additional_data['kb'] = {key: self.tokenizer.texts_to_sequences(value)
                                 for key, value in additional_data['kb'].items()}

        text_info = {
            'sentence_max_length': sentence_max_length,
            'query_max_length': query_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix,
            'kb_max_length': max([len(seq) for seq in additional_data['kb'].values()]),
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(KBContextConverter, self).convert_data(text_info=text_info,
                                                              df=df,
                                                              return_text_info=return_text_info,
                                                              additional_data=additional_data)

        # Updating text info
        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + text_info['kb_max_length']
            else:
                text_info['memory_max_length'] = text_info['kb_max_length']
            output = list(output)[:-1] + [text_info]

        return output


class Task2KBContextConverter(Task2ContextConverter):

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        kb_sentences = [sent for seq in list(additional_data['kb'].values()) for sent in seq]
        data_to_fit = np.append(data_to_fit, kb_sentences)
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        query_max_length = max([len(row) for row in encoded_query])

        encoded_kb_sentences = self.tokenizer.texts_to_sequences(kb_sentences)
        sentence_max_length = max([len(seq) for seq in encoded_kb_sentences])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        # Converting KB
        additional_data['kb'] = {key: self.tokenizer.texts_to_sequences(value)
                                 for key, value in additional_data['kb'].items()}

        text_info = {
            'sentence_max_length': sentence_max_length,
            'query_max_length': query_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix,
            'kb_max_length': max([len(seq) for seq in additional_data['kb'].values()]),
            'max_targets': additional_data['max_targets']
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(Task2KBContextConverter, self).convert_data(text_info=text_info,
                                                                   df=df,
                                                                   return_text_info=return_text_info,
                                                                   additional_data=additional_data)

        # Updating text info
        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + text_info['kb_max_length']
            else:
                text_info['memory_max_length'] = text_info['kb_max_length']
            output = list(output)[:-1] + [text_info]

        return output


@DeprecationWarning
class DLUKBContextConverter(DLUContextConverter):

    def _load_logic_info(self, additional_data, category='label'):
        filename = '{}_logic_group_masks.json'.format(category) if category != 'label' else 'logic_group_mask.json'
        filepath = os.path.join(cd.KB_DIR, filename)
        logic_info = load_json(filepath)

        # [memory_size, max_logic_depth - 1, max_logic_groups, sentence_size]
        logic_masks = []

        # [memory_size, max_logic_depth, max_logic_groups]
        logic_operators = []

        logic_info_keys = list(logic_info.keys())
        logic_info_keys = sorted(logic_info_keys, key=lambda x: int(x))

        padding_length = max(additional_data['kb_lengths'])

        def get_max_groups(additional_data, logic_info, logic_info_keys, key_vocab):
            max_groups = None

            for sentence_id in logic_info_keys:
                if logic_info[sentence_id] is None:
                    key_groups_len = len([item for item in additional_data['kb_pos'][int(sentence_id)]
                                          if item in key_vocab])
                    if max_groups is None:
                        max_groups = key_groups_len
                    elif max_groups < key_groups_len:
                        max_groups = key_groups_len
                else:
                    level_0_groups = logic_info[sentence_id]['0']
                    key_groups_len = len([item for item in additional_data['kb_pos'][int(sentence_id)]
                                          if item in key_vocab])
                    level_0_groups_len = [len(item[0]) - 1 for item in level_0_groups]
                    real_len = key_groups_len - sum(level_0_groups_len)
                    if max_groups is None:
                        max_groups = real_len
                    elif max_groups < real_len:
                        max_groups = real_len

            return max_groups

        padding_groups = get_max_groups(additional_data, logic_info, logic_info_keys, self.key_vocab)
        logger.info('Max logic groups: {}'.format(padding_groups))

        def get_max_depth(logic_info):
            max_depth = None

            for item in logic_info.values():
                if item is not None:
                    level_keys = list(item.keys())
                    level_keys = [int(key) for key in level_keys if key != 'last']
                    item_depth = max(level_keys) + 1
                    if max_depth is None:
                        max_depth = item_depth
                    elif max_depth < item_depth:
                        max_depth = item_depth

            return max_depth

        padding_depth = get_max_depth(logic_info)
        logger.info('Max logic depth: {}'.format(padding_depth))

        def create_hot_encoding_logic_masks(sentence_pos, key_vocab, padding_length, padding_groups=None,
                                            seen_indices=None):
            logic_masks = []
            logic_operators = []

            for pos_idx, pos_tag in enumerate(sentence_pos):
                if seen_indices is not None and pos_idx in seen_indices:
                    continue

                if pos_tag in key_vocab:
                    pos_mask = [0] * padding_length
                    pos_mask[pos_idx] = 1
                    logic_masks.append(pos_mask)
                    logic_operators.append(1)

            # Pad to max groups
            if padding_groups is not None:
                if len(logic_masks) < padding_groups:
                    for pad_idx in range(len(logic_masks), padding_groups):
                        pad_encodings = [0] * padding_length
                        logic_masks.append(pad_encodings)
                        logic_operators.append(1)

            return logic_masks, logic_operators

        def create_upper_level_hot_encoding(previous_logic_masks, padding_length, padding_groups):

            logic_masks = []
            logic_operators = []

            # Filter out paddings
            previous_logic_masks = [mask for mask in previous_logic_masks if np.sum(mask) > 0]

            for mask_idx, mask in enumerate(previous_logic_masks):
                hot_encoding = [0] * padding_length
                hot_encoding[mask_idx] = 1
                logic_masks.append(hot_encoding)
                logic_operators.append(1)

            if len(logic_masks) < padding_groups:
                for pad_idx in range(len(logic_masks), padding_groups):
                    pad_encodings = [0] * padding_length
                    logic_masks.append(pad_encodings)
                    logic_operators.append(1)

            return logic_masks, logic_operators

        def create_logic_group_masks(sentence_pos, group_masks, key_vocab, padding_length, padding_groups):
            logic_masks = []
            logic_operators = []
            seen_indices = []

            # mask_info: [indices, operator]
            for mask_info in group_masks:
                group_mask = [1 if idx in mask_info[0] else 0 for idx, _ in enumerate(sentence_pos)]
                group_mask.extend([0] * (padding_length - len(sentence_pos)))
                logic_masks.append(group_mask)
                logic_operators.append(1 if mask_info[1] == 'and' else 0)
                seen_indices.extend(mask_info[0])

            hot_encodings, hot_operators = create_hot_encoding_logic_masks(sentence_pos=sentence_pos,
                                                                           key_vocab=key_vocab,
                                                                           seen_indices=seen_indices,
                                                                           padding_length=padding_length)
            logic_masks.extend(hot_encodings)
            logic_operators.extend(hot_operators)

            # Sort logic masks and operators in order to keep position
            indexes = [np.argmax(mask) for mask in logic_masks]
            indexes = np.argsort(indexes)
            logic_masks = [logic_masks[idx] for idx in indexes]
            logic_operators = [logic_operators[idx] for idx in indexes]

            if len(logic_masks) < padding_groups:
                for pad_idx in range(len(logic_masks), padding_groups):
                    pad_encodings = [0] * padding_length
                    logic_masks.append(pad_encodings)
                    logic_operators.append(1)

            return logic_masks, logic_operators

        def create_upper_level_logic_group_masks(previous_logic_masks, group_masks, padding_length, padding_groups):

            logic_masks = []
            logic_operators = []
            seen_indices = []

            # Sorting logic masks (filter out paddings)
            previous_mask_indexes = [np.argmax(mask) for mask in previous_logic_masks if np.sum(mask) > 0]
            previous_mask_indexes = sorted(previous_mask_indexes)

            for mask_info in group_masks:
                group_mask = [1 if idx in mask_info[0] else 0 for idx, _ in enumerate(previous_mask_indexes)]
                group_mask.extend([0] * (padding_length - len(group_mask)))
                logic_masks.append(group_mask)
                logic_operators.append(1 if mask_info[1] == 'and' else 0)
                seen_indices.extend(mask_info[0])

            for mask_idx, _ in enumerate(previous_mask_indexes):
                if mask_idx not in seen_indices:
                    hot_encoding = [0] * padding_length
                    hot_encoding[mask_idx] = 1
                    logic_masks.append(hot_encoding)
                    logic_operators.append(1)

            # Sort logic masks and operators in order to keep position
            indexes = [np.argmax(mask) for mask in logic_masks]
            indexes = np.argsort(indexes)
            logic_masks = [logic_masks[idx] for idx in indexes]
            logic_operators = [logic_operators[idx] for idx in indexes]

            if len(logic_masks) < padding_groups:
                for pad_idx in range(len(logic_masks), padding_groups):
                    pad_encodings = [0] * padding_length
                    logic_masks.append(pad_encodings)
                    logic_operators.append(1)

            return logic_masks, logic_operators

        for sentence_id in logic_info_keys:
            mask_info = logic_info[sentence_id]
            int_sentence_id = int(sentence_id)
            sentence_pos = additional_data['kb_pos'][int(int_sentence_id)]

            # If no logic groups -> add hot encoding of unifiable terms
            if mask_info is None:
                hot_encodings, hot_operators = create_hot_encoding_logic_masks(sentence_pos=sentence_pos,
                                                                               key_vocab=self.key_vocab,
                                                                               padding_length=padding_length,
                                                                               padding_groups=padding_groups)
                logic_masks.append([hot_encodings])
                logic_operators.append([hot_operators])
            else:
                level_keys = list(mask_info.keys())
                level_keys = [key for key in level_keys if key != 'last']
                level_keys = sorted(level_keys, key=lambda x: int(x))

                # Loop over logic levels
                for level in level_keys:

                    # Start with initial logic groups
                    if level == '0':
                        initial_masks, initial_operators = create_logic_group_masks(sentence_pos=sentence_pos,
                                                                                    key_vocab=self.key_vocab,
                                                                                    group_masks=mask_info[level],
                                                                                    padding_length=padding_length,
                                                                                    padding_groups=padding_groups)
                        logic_masks.append([initial_masks])
                        logic_operators.append([initial_operators])

                    # Build relative logic groups
                    else:
                        previous_logic_masks = logic_masks[int_sentence_id][-1]
                        level_masks, level_operators = create_upper_level_logic_group_masks(
                            previous_logic_masks=previous_logic_masks,
                            group_masks=mask_info[level],
                            padding_length=padding_length,
                            padding_groups=padding_groups)
                        logic_masks[int_sentence_id].append(level_masks)
                        logic_operators[int_sentence_id].append(level_operators)

            # Pad to max logic depth
            if len(logic_masks[int_sentence_id]) < padding_depth:
                depth_to_add = padding_depth - len(logic_masks[int_sentence_id])
                previous_logic_masks = logic_masks[int_sentence_id][-1]
                for depth_idx in range(depth_to_add):
                    pad_encodings, pad_operators = create_upper_level_hot_encoding(
                        previous_logic_masks=previous_logic_masks,
                        padding_length=padding_length,
                        padding_groups=padding_groups)
                    logic_masks[int_sentence_id].append(pad_encodings)
                    logic_operators[int_sentence_id].append(pad_operators)

            # Add last level operators
            if mask_info is None:
                logic_operators[int_sentence_id].append([1] + [0] * (padding_groups - 1))
            else:
                operator_gate = [1] if mask_info['last'] == 'and' else [0]
                operator_gate.extend([0] * (padding_groups - 1))
                logic_operators[int_sentence_id].append(operator_gate)

        return np.array(logic_masks, dtype=np.int32), \
               np.array(logic_operators, dtype=np.int32), \
               padding_groups, \
               padding_depth, \
               padding_length

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        data_to_fit = np.append(data_to_fit, additional_data['kb'])
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        sentence_max_length = max([len(row) for row in encoded_query])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        # Build KB unification masks
        kb_pos = self.build_pos_tags(data=additional_data['kb'])
        kb_unification_masks = [[1 if tag == vocab_key else 0 for tag in item]
                                for item in kb_pos
                                for vocab_key in self.key_vocab]

        # Converting KB
        additional_data['kb'] = self.tokenizer.texts_to_sequences(additional_data['kb'])
        additional_data['kb_pos'] = kb_pos
        additional_data['kb_lengths'] = np.array([len(item) for item in additional_data['kb']], dtype=np.int32)
        additional_data['kb_segments'] = [[idx for _ in range(len(item))]
                                          for idx, item in enumerate(additional_data['kb'])]
        kb_logic_masks, \
        kb_logic_operators, \
        max_logic_groups, \
        max_logic_depth, \
        kb_max_length = self._load_logic_info(additional_data, self.label)
        additional_data['kb_logic_masks'] = kb_logic_masks
        additional_data['kb_logic_operators'] = kb_logic_operators

        kb_unification_masks = pad_data(kb_unification_masks,
                                        padding='post',
                                        dtype=np.float32,
                                        padding_length=kb_max_length)
        kb_unification_masks = np.reshape(kb_unification_masks, [-1, len(self.key_vocab), kb_max_length])
        additional_data['kb_unification_masks'] = kb_unification_masks

        text_info = {
            'sentence_max_length': kb_max_length,
            'query_max_length': sentence_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix,
            'max_logic_groups': max_logic_groups,
            'max_logic_depth': max_logic_depth
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(DLUKBContextConverter, self).convert_data(text_info=text_info,
                                                                 df=df,
                                                                 return_text_info=return_text_info,
                                                                 additional_data=additional_data)

        # Updating text info
        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + len(additional_data['kb'])
            else:
                text_info['memory_max_length'] = len(additional_data['kb'])
            output = list(output)[:-1] + [text_info]

        return output


class TransformerConverter(KBContextConverter):
    """
    KB-aware converter for the Transformer model. In addition to traditional look-ahead and padding masks,
    a content separation mask is needed when considering the KB as the context, since memory cells are independent.
    This mask is later defined just before the batch feed phase (_parse_input() method).
    """

    def __init__(self, concatenate_context=True, **kwargs):
        super(TransformerConverter, self).__init__(**kwargs)
        self.concatenate_context = concatenate_context

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        data_to_fit = np.append(data_to_fit, additional_data['kb'])
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        encoded_query = self.tokenizer.texts_to_sequences(data_to_fit)
        query_max_length = max([len(item) for item in encoded_query])

        embedding_matrix = None

        if self.build_embedding_matrix:
            embedding_matrix = build_embeddings_matrix(vocab_size=vocab_size,
                                                       embedding_model=self.embedding_model,
                                                       embedding_dimension=self.embedding_dimension,
                                                       tokenizer=self.tokenizer)

        # Converting KB
        additional_data['kb'] = self.tokenizer.texts_to_sequences(additional_data['kb'])

        if self.concatenate_context:
            concat_kb = [item for seq in additional_data['kb'] for item in seq]
            sentence_max_length = len(concat_kb)
        else:
            sentence_max_length = max([len(item) for item in additional_data['kb']])

        text_info = {
            'sentence_max_length': sentence_max_length,
            'query_max_length': query_max_length,
            'vocab_size': vocab_size,
            'embedding_matrix': embedding_matrix
        }

        x_train, y_train, text_info = self.convert_data(df=train_df,
                                                        text_info=text_info,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(TransformerConverter, self).convert_data(text_info=text_info,
                                                                df=df,
                                                                return_text_info=return_text_info,
                                                                additional_data=additional_data)

        # Build input mask
        # tokens that have a mask value of 1 are ignored during multi-head attention
        # [samples, 1, 1, query_max_length]
        query_input = output[0]['text']
        query_input = pad_data(data=query_input)

        query_mask = query_input == 0
        query_mask = query_mask.reshape([query_mask.shape[0], 1, 1, query_mask.shape[1]]).astype(np.float32)

        # Build look-ahead mask: shape [samples, sample_length, sample_length]
        # tokens that have a mask value of 1 are ignored during multi-head attention
        look_ahead_mask = 1 - tf.linalg.band_part(tf.ones((query_input.shape[1], query_input.shape[1])), -1, 0)

        # Compute combined mask
        # [samples, 1, query_max_length, query_max_length]
        combined_mask = np.maximum(look_ahead_mask, query_mask)
        output[0]['text_combined_mask'] = combined_mask

        return output


class SupervisionContextConverter(KBContextConverter):
    """
    Context-aware converter that also builds supervision targets.
    """

    def __init__(self, partial_supervision_info, **kwargs):
        super(SupervisionContextConverter, self).__init__(**kwargs)
        self.partial_supervision_info = partial_supervision_info

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        output = super(SupervisionContextConverter, self).convert_data(text_info=text_info,
                                                                       df=df,
                                                                       return_text_info=return_text_info,
                                                                       additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision_info['flag']:
            # output[0][0] -> text
            # output[1] -> label

            exp_targets = df['{}_targets'.format(self.label)].values

            if return_text_info:
                text_info = output[-1]

            targets = []
            for target_set in exp_targets:

                if type(target_set) is not float:
                    target_set = [int(item) for item in target_set[1:-1].split(',')]
                    sent_targets = [1 if idx + (self.context_dim - 1) in target_set else 0
                                    for idx in range(text_info['memory_max_length'])]
                    targets.append(sent_targets)
                else:
                    targets.append([0] * text_info['memory_max_length'])

            output[0]['targets'] = targets

            if return_text_info:
                max_positive_amount = np.max(np.sum(targets, axis=1))
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


class ELMoConverter(object):
    """
    ELMo-compliant base converter. Each sentence is converted to its corresponding ELMo embedding vector.
    ELMo embeddings can either be at word or sentence level.
    """

    def __init__(self, label, embedding_mode='sentence', elmo_model="https://tfhub.dev/google/elmo/3"):
        self.label = label

        if embedding_mode not in ['sentence', 'word']:
            msg = 'Invalid embedding mode for ELMo embeddings! Got: {0}, Available: {1}'.format(embedding_mode,
                                                                                                ['sentence', 'word'])
            logger.exception(msg)
            raise RuntimeError(msg)

        self.embedding_mode = embedding_mode
        self.elmo_model = elmo_model

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        text_info = {
            'vocab_size': vocab_size,
        }

        x_train, y_train, text_info = self.convert_data(
            text_info=text_info,
            df=train_df,
            return_text_info=True,
            additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def load_embedding_model(self):
        pass

    def _elmo_convert(self, text, text_info, key=None, return_text_info=False):

        preloaded_path = os.path.join(cd.EMBEDDING_MODELS_DIR, 'ELMo', 'preloaded_{}.npy'.format(self.embedding_mode))
        if os.path.isfile(preloaded_path):
            preloaded = np.load(preloaded_path, allow_pickle=True).item()
        else:
            preloaded = {}

        text_elmo = []
        added = 0
        with hub.eval_function_for_module(self.elmo_model) as elmo:
            for sentence in tqdm(text):
                if sentence in preloaded:
                    text_elmo.append(preloaded[sentence])
                else:
                    batch_in = np.array([sentence])
                    if self.embedding_mode == 'sentence':
                        batch_out = elmo(batch_in, as_dict=True)['default'][0]
                    else:
                        batch_out = elmo(batch_in, as_dict=True)['elmo'][0]
                    text_elmo.append(batch_out)
                    preloaded[sentence] = batch_out
                    added += 1

        if added > 0:
            np.save(preloaded_path, preloaded)

        lengths = [item.shape[0] for item in text_elmo]

        if return_text_info:
            sentence_max_length = max(lengths)
            text_info[key] = sentence_max_length

        if self.embedding_mode == 'sentence':
            conv_text = np.array(text_elmo)
        else:
            conv_text = np.zeros((len(text), text_info[key], 1024), dtype=np.float32)
            mask = np.arange(text_info[key]) < np.array(lengths)[:, None]
            conv_text[mask] = np.concatenate(text_elmo)

        return conv_text, text_info

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        label = df[self.label].values

        conv_text, text_info = self._elmo_convert(text=text,
                                                  text_info=text_info,
                                                  key='padding_max_length',
                                                  return_text_info=return_text_info)

        if return_text_info:
            return {'text': conv_text}, label, text_info
        else:
            return {'text': conv_text}, label


class ContextELMoConverter(ELMoConverter):
    """
    ELMo-compliant context converter.
    """

    def __init__(self, context_dim, mode, **kwargs):
        super(ContextELMoConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        label = df[self.label].values

        conv_text, text_info = self._elmo_convert(text=text,
                                                  text_info=text_info,
                                                  key='query_max_length',
                                                  return_text_info=return_text_info)

        # TODO: verify
        if self.context_dim > 1:
            text_info['sentence_max_length'] = text_info['query_max_length']
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=conv_text,
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)
            context = df['context'].values

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                return {'text': conv_text, 'context': context}, \
                       label, \
                       text_info
            else:
                return {'text': conv_text, 'context': context}, \
                       label

        if return_text_info:
            return {'text': conv_text}, label, text_info
        else:
            return {'text': conv_text}, label


class KBContextELMoConverter(ContextELMoConverter):
    """
    ELMo-compliant KB converter.
    """

    def fit_train_data(self, train_df, additional_data=None):
        self.tokenizer = tf.keras.preprocessing.text.Tokenizer(oov_token=1)
        data_to_fit = train_df['text'].values
        kb_sentences = [sent for seq in list(additional_data['kb'].values()) for sent in seq]
        data_to_fit = np.append(data_to_fit, kb_sentences)
        self.tokenizer.fit_on_texts(data_to_fit)

        # Add 1 in order to consider padding
        vocab_size = len(self.tokenizer.word_index) + 1

        text_info = {
            'vocab_size': vocab_size
        }

        parsed_kb = {key: self._elmo_convert(text=value, key='kb_max_length', text_info=text_info)[0]
                     for key, value in additional_data['kb'].items()}

        kb_lengths = [value.shape[0] for key, value in parsed_kb.items()]
        text_info['kb_max_length'] = max(kb_lengths)

        additional_data['kb'] = parsed_kb

        x_train, y_train, text_info = self.convert_data(
            text_info=text_info,
            df=train_df,
            return_text_info=True,
            additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(KBContextELMoConverter, self).convert_data(text_info=text_info,
                                                                  df=df,
                                                                  return_text_info=return_text_info,
                                                                  additional_data=additional_data)

        # Updating text info
        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + text_info['kb_max_length']
            else:
                text_info['memory_max_length'] = text_info['kb_max_length']
            output = list(output)[:-1] + [text_info]

        return output


class SupervisionContextELMoConverter(KBContextELMoConverter):
    """
    ELMo-compliant strong supervision converter.
    """

    def __init__(self, partial_supervision_info=None, **kwargs):
        super(SupervisionContextELMoConverter, self).__init__(**kwargs)
        self.partial_supervision_info = partial_supervision_info

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        output = super(SupervisionContextELMoConverter, self).convert_data(text_info=text_info,
                                                                           df=df,
                                                                           return_text_info=return_text_info,
                                                                           additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision_info['flag']:
            # output[0][0] -> text
            # output[1] -> label

            exp_targets = df['{}_targets'.format(self.label)].values

            if return_text_info:
                text_info = output[-1]

            targets = []
            for target_set in exp_targets:

                if type(target_set) is not float:
                    target_set = [int(item) for item in target_set[1:-1].split(',')]
                    sent_targets = [1 if idx + (self.context_dim - 1) in target_set else 0
                                    for idx in range(text_info['memory_max_length'])]
                    targets.append(sent_targets)
                else:
                    targets.append([0] * text_info['memory_max_length'])

            output[0]['targets'] = targets

            if return_text_info:
                max_positive_amount = np.max(np.sum(targets, axis=1))
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


class FineTuningBERTConverter(object):

    def __init__(self, label, bert_model, spm_model_file):
        self.label = label
        self.bert_model = bert_model
        self.spm_model_file = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', spm_model_file)
        self.vocab_file = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'adj_vocab.txt')
        self.tokenizer = FullTokenizer.from_hub_module_v2(hub_module=self.bert_model,
                                                          spm_model_file=self.spm_model_file)

    def load_embedding_model(self):
        pass

    def fit_train_data(self, train_df, additional_data=None):
        vocab_size = len(self.tokenizer.vocab)

        text_info = {
            'vocab_size': vocab_size
        }

        x_train, y_train, text_info = self.convert_data(
            text_info=text_info,
            df=train_df,
            return_text_info=True,
            additional_data=additional_data
        )
        return x_train, y_train, text_info, additional_data

    def _parse_sentence(self, sentence):
        return ['[CLS]'] + self.tokenizer.tokenize(sentence) + ['[SEP]']

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        text = list(map(lambda item: self.tokenizer.convert_tokens_to_ids(self._parse_sentence(item)), text))

        text_mask = [[1 if item != 0 else 0 for item in sentence] for sentence in text]
        text_segment_ids = [[0] * len(sentence) for sentence in text]

        label = df[self.label].values

        if return_text_info:
            return {'text': text, 'text_mask': text_mask, 'text_segment_ids': text_segment_ids}, label, text_info
        else:
            return {'text': text, 'text_mask': text_mask, 'text_segment_ids': text_segment_ids}, label


class BERTConverter(object):
    """
    BERT-compliant base converter. Each sentence is converted to its corresponding ELMo embedding vector.
    BERT embeddings can either be at word or sentence level.
    """

    def __init__(self, label, embedding_mode='sentence',
                 bert_model="https://tfhub.dev/google/bert_uncased_L-12_H-768_A-12/1"):
        self.label = label
        self.vocab_file = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'vocab.txt')

        if embedding_mode not in ['sentence', 'word']:
            msg = 'Invalid embedding mode for BERT embeddings! Got: {0}, Available: {1}'.format(embedding_mode,
                                                                                                ['sentence', 'word'])
            logger.exception(msg)
            raise RuntimeError(msg)

        self.embedding_mode = embedding_mode
        self.bert_model = bert_model
        self.tokenizer = FullTokenizer(vocab_file=self.vocab_file,
                                       do_lower_case=True)

    def load_embedding_model(self):
        pass

    def fit_train_data(self, train_df, additional_data=None):
        vocab_size = len(self.tokenizer.vocab)

        text_info = {
            'vocab_size': vocab_size
        }

        x_train, y_train, text_info = self.convert_data(
            text_info=text_info,
            df=train_df,
            return_text_info=True,
            additional_data=additional_data
        )
        return x_train, y_train, text_info, additional_data

    def _parse_sentence(self, sentence):
        return ['[CLS]'] + self.tokenizer.tokenize(sentence) + ['[SEP]']

    def _bert_convert(self, text, text_info, key=None, return_text_info=False):

        preloaded_path = os.path.join(cd.EMBEDDING_MODELS_DIR, 'BERT', 'preloaded_{}.npy'.format(self.embedding_mode))
        if os.path.isfile(preloaded_path):
            preloaded = np.load(preloaded_path, allow_pickle=True).item()
        else:
            preloaded = {}

        text_bert = []
        added = 0
        with hub.eval_function_for_module(self.bert_model) as bert:
            for sentence in tqdm(text):
                if sentence in preloaded:
                    text_bert.append(preloaded[sentence])
                else:
                    parsed_sentence = self._parse_sentence(sentence)
                    parsed_sentence = self.tokenizer.convert_tokens_to_ids(parsed_sentence)
                    sentence_mask = [1 if item != 0 else 0 for item in parsed_sentence]
                    sentence_segment_ids = [0] * len(parsed_sentence)
                    batch_in = {'input_ids': [parsed_sentence],
                                'input_mask': [sentence_mask],
                                'segment_ids': [sentence_segment_ids]}
                    if self.embedding_mode == 'sentence':
                        batch_out = bert(batch_in, as_dict=True, signature='tokens')['pooled_output'][0]
                    else:
                        batch_out = bert(batch_in, as_dict=True, signature='tokens')['sequence_output'][0]
                    text_bert.append(batch_out)
                    preloaded[sentence] = batch_out
                    added += 1

        if added > 0:
            np.save(preloaded_path, preloaded)

        lengths = [item.shape[0] for item in text_bert]

        if return_text_info:
            sentence_max_length = max(lengths)
            text_info[key] = sentence_max_length

        if self.embedding_mode == 'sentence':
            conv_text = np.array(text_bert)
        else:
            conv_text = np.zeros((len(text), text_info[key], 768), dtype=np.float32)
            mask = np.arange(text_info[key]) < np.array(lengths)[:, None]
            conv_text[mask] = np.concatenate(text_bert)

        return conv_text, text_info

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        conv_text, text_info = self._bert_convert(text=df['text'].values,
                                                  text_info=text_info,
                                                  key='padding_max_length',
                                                  return_text_info=return_text_info)
        label = df[self.label].values

        if return_text_info:
            return {'text': conv_text}, label, text_info
        else:
            return {'text': conv_text}, label


class ContextBERTConverter(BERTConverter):
    """
    BERT-compliant context converter.
    """

    def __init__(self, context_dim, mode, **kwargs):
        super(ContextBERTConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        label = df[self.label].values

        conv_text, text_info = self._bert_convert(text=text,
                                                  text_info=text_info,
                                                  key='query_max_length',
                                                  return_text_info=return_text_info)

        # TODO: verify grouped masking
        if self.context_dim > 1:
            text_info['sentence_max_length'] = text_info['query_max_length']
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=conv_text,
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)

            context = df['context'].values

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                return {'text': conv_text, 'context': context}, label, text_info
            else:
                return {'text': conv_text, 'context': context}, label

        if return_text_info:
            return {'text': conv_text}, label, text_info
        else:
            return {'text': conv_text}, label


class KBContextBERTConverter(ContextBERTConverter):
    """
    BERT-compliant KB converter.
    """

    def fit_train_data(self, train_df, additional_data=None):
        vocab_size = len(self.tokenizer.vocab)

        text_info = {
            'vocab_size': vocab_size
        }

        parsed_kb = {key: self._bert_convert(text=value, key='kb_max_length', text_info=text_info)[0]
                     for key, value in additional_data['kb'].items()}

        kb_lengths = [value.shape[0] for key, value in parsed_kb.items()]
        text_info['kb_max_length'] = max(kb_lengths)

        additional_data['kb'] = parsed_kb

        x_train, y_train, text_info = self.convert_data(
            text_info=text_info,
            df=train_df,
            return_text_info=True,
            additional_data=additional_data
        )
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(KBContextBERTConverter, self).convert_data(text_info=text_info,
                                                                  df=df,
                                                                  return_text_info=return_text_info,
                                                                  additional_data=additional_data)

        # Updating text info
        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + text_info['kb_max_length']
            else:
                text_info['memory_max_length'] = text_info['kb_max_length']
            output = list(output)[:-1] + [text_info]

        return output


class SupervisionContextBERTConverter(KBContextBERTConverter):
    """
    BERT-compliant strong supervision converter.
    """

    def __init__(self, partial_supervision_info=None, **kwargs):
        super(SupervisionContextBERTConverter, self).__init__(**kwargs)
        self.partial_supervision_info = partial_supervision_info

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        output = super(SupervisionContextBERTConverter, self).convert_data(text_info=text_info,
                                                                           df=df,
                                                                           return_text_info=return_text_info,
                                                                           additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision_info['flag']:
            # output[0][0] -> text
            # output[1] -> label

            exp_targets = df['{}_targets'.format(self.label)].values

            if return_text_info:
                text_info = output[-1]

            targets = []
            for target_set in exp_targets:

                if type(target_set) is not float:
                    target_set = [int(item) for item in target_set[1:-1].split(',')]
                    sent_targets = [1 if idx + (self.context_dim - 1) in target_set else 0
                                    for idx in range(text_info['memory_max_length'])]
                    targets.append(sent_targets)
                else:
                    targets.append([0] * text_info['memory_max_length'])

            output[0]['targets'] = targets

            if return_text_info:
                max_positive_amount = np.max(np.sum(targets, axis=1))
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


# TODO: enable POS tags
class BoWConverter(BaseConverter):
    """
    Bag-of-Words base converter. N-grams and POS tags (optionally) are computed and used as features.
    Due to the huge amount of features, dimensionality reduction is required.
    """

    def __init__(self, bow_ngrams=(1, 2), use_pos_tags=True, **kwargs):
        super(BoWConverter, self).__init__(embedding_model_type="",
                                           build_embedding_matrix=False,
                                           **kwargs)
        self.bow_ngrams = bow_ngrams
        self.use_pos_tags = use_pos_tags
        self.sparse_support = False

    def build_pos_tags(self, data, memo=None, return_memo=False):
        if memo is None:
            memo = {}
            pos_tags = []
            for item in data:
                key = ''.join(item)
                if key in memo:
                    pos_tags.append(memo[key])
                else:
                    item_tokens = nltk.word_tokenize(item)
                    tags = nltk.pos_tag(item_tokens)
                    tags = ' '.join(['-'.join(tup) for tup in tags])
                    memo[key] = tags
                    pos_tags.append(tags)
        else:
            pos_tags = [memo[item] for item in data]

        if return_memo:
            return memo
        return pos_tags

    def fit_train_data(self, train_df, additional_data=None):

        data_to_fit = train_df['text'].values

        # BoW fitting
        self.bow_vectorizer = TfidfVectorizer(ngram_range=self.bow_ngrams, max_features=self.embedding_dimension)
        self.bow_vectorizer.fit(data_to_fit)

        # PoS fitting
        # if self.use_pos_tags:
        #     self.pos_vectorizer = TfidfVectorizer(max_features=self.embedding_dimension)
        #     prebuilt_train_tags = self.build_pos_tags(data_to_fit, return_memo=True)
        #     self.pos_vectorizer.fit(prebuilt_train_tags.values())

        x_train, y_train, text_info = self.convert_data(text_info={},
                                                        df=train_df,
                                                        return_text_info=True,
                                                        additional_data=additional_data)
        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        text = df['text'].values
        conv_text = self.bow_vectorizer.transform(text).toarray()

        # if self.use_pos_tags:
        #     text_tags = self.build_pos_tags(data=conv_text, return_memo=False)
        #     conv_text_tags = self.pos_vectorizer.transform(text_tags)

        label = df[self.label].values

        if return_text_info:
            text_info['padding_max_length'] = conv_text.shape[-1]

            return {'text': conv_text}, label, text_info
        else:
            return {'text': conv_text}, label


class BowContextConverter(BoWConverter):
    """
    Bag-of-Words context converter.
    """

    def __init__(self, context_dim, mode, **kwargs):
        super(BowContextConverter, self).__init__(**kwargs)
        self.context_dim = context_dim
        self.mode = mode

        # Determining parsing method
        # Mode selection
        if self.mode == 'windowing':
            self.parsing = windowing
        elif self.mode == 'group_data_including':
            self.parsing = group_data_including
        elif self.mode == 'group_data_excluding':
            self.parsing = group_data_excluding
        else:
            msg = 'Invalid mode given! Got: {}'.format(self.mode)
            logger.exception(msg)
            raise AttributeError(msg)

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        output = super(BowContextConverter, self).convert_data(text_info=text_info,
                                                               df=df,
                                                               return_text_info=return_text_info,
                                                               additional_data=additional_data)

        if self.context_dim > 1:
            df['context'] = ''
            grouped = df.groupby(by='document')
            for _, doc_group in grouped:
                df.iloc[doc_group.index.values]['context'] = self.parsing(x=output[0]['text'],
                                                                          y=doc_group[self.label].values,
                                                                          window_dim=self.context_dim)

            context = df['context'].values
            output[0]['context'] = context

            if return_text_info:
                memory_max_length = max([len(context) for context in context])
                text_info['memory_max_length'] = memory_max_length
                text_info['sentence_max_length'] = output[0]['context'].shape[1]
                text_info['query_max_length'] = output[0]['text'].shape[1]
                return output
            else:
                return output
        else:
            if return_text_info:
                text_info['sentence_max_length'] = output[0]['text'].shape[1]
                text_info['query_max_length'] = output[0]['text'].shape[1]
            return output


class BowKBContextConverter(BowContextConverter):
    """
    Bag-of-Words KB converter.
    """

    def fit_train_data(self, train_df, additional_data=None):
        data_to_fit = train_df['text'].values
        data_to_fit = np.append(data_to_fit, additional_data['kb'])

        # BoW fitting
        self.bow_vectorizer = TfidfVectorizer(ngram_range=self.bow_ngrams, max_features=self.embedding_dimension)
        self.bow_vectorizer.fit(data_to_fit)

        # PoS fitting
        # self.pos_vectorizer = TfidfVectorizer(max_df=0.7)
        # prebuilt_train_tags = self.build_pos_tags(data_to_fit, return_memo=True)
        # data_to_fit_tags = [prebuilt_train_tags[item] for item in data_to_fit]
        # self.pos_vectorizer.fit(prebuilt_train_tags.values())

        additional_data['kb'] = self.bow_vectorizer.transform(additional_data['kb']).toarray()

        x_train, y_train, text_info = self.convert_data(text_info={},
                                                        df=train_df,
                                                        return_text_info=True,
                                                        additional_data=additional_data)

        return x_train, y_train, text_info, additional_data

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):
        output = super(BowKBContextConverter, self).convert_data(text_info=text_info,
                                                                 df=df,
                                                                 return_text_info=return_text_info,
                                                                 additional_data=additional_data)

        if return_text_info:
            text_info = output[-1]
            if self.context_dim > 1:
                text_info['memory_max_length'] = text_info['memory_max_length'] + len(additional_data['kb'])
                text_info['sentence_max_length'] = output[0]['context'].shape[1]
            else:
                text_info['memory_max_length'] = len(additional_data['kb'])
                text_info['sentence_max_length'] = max([len(item) for item in additional_data['kb']])

            text_info['query_max_length'] = output[0]['text'].shape[1]
            output = list(output)[:-1] + [text_info]

        return output


class BowSupervisionContextConverter(BowKBContextConverter):
    """
    Bag-of-Words strong supervision converter.
    """

    def __init__(self, partial_supervision=None, **kwargs):
        super(BowSupervisionContextConverter, self).__init__(**kwargs)
        # Tuple: [Flag, Coefficient]
        if partial_supervision is not None:
            self.partial_supervision = partial_supervision[0]
        else:
            self.partial_supervision = False

    def convert_data(self, text_info, df, return_text_info=False, additional_data=None):

        output = super(BowSupervisionContextConverter, self).convert_data(text_info=text_info,
                                                                          df=df,
                                                                          return_text_info=return_text_info,
                                                                          additional_data=additional_data)

        # Build supervision targets
        if self.partial_supervision:
            # output[0][0] -> text
            # output[1] -> label

            exp_targets = df['{}_targets'.format(self.label)].values

            if return_text_info:
                text_info = output[-1]

            targets = []
            for target_set in exp_targets:

                if type(target_set) is not float:
                    target_set = [int(item) for item in target_set[1:-1].split(',')]
                    sent_targets = [1 if idx + (self.context_dim - 1) in target_set else 0
                                    for idx in range(text_info['memory_max_length'])]
                    targets.append(sent_targets)
                else:
                    targets.append([0] * text_info['memory_max_length'])

            output[0]['targets'] = targets

            if return_text_info:
                max_positive_amount = np.max(np.sum(targets, axis=1))
                # min_positive_amount = np.min(np.sum(targets, axis=1))
                # max_negative_amount = text_info['memory_max_length'] - min_positive_amount
                # padding_amount = max(max_positive_amount, max_negative_amount)
                padding_amount = max_positive_amount
                text_info['padding_amount'] = padding_amount
                output = list(output)[:-1] + [text_info]

        return output


class ConverterFactory(object):
    supported_converters = {
        'bow_converter': BoWConverter,
        'bow_context_converter': BowContextConverter,
        'bow_kb_context_converter': BowKBContextConverter,
        'bow_supervised_context_converter': BowSupervisionContextConverter,
        'context_converter': ContextConverter,
        'kb_context_converter': KBContextConverter,
        'supervised_context_converter': SupervisionContextConverter,
        'base_converter': BaseConverter,
        'transformer_converter': TransformerConverter,
        'elmo_converter': ELMoConverter,
        'context_elmo_converter': ContextELMoConverter,
        'kb_context_elmo_converter': KBContextELMoConverter,
        'supervised_context_elmo_converter': SupervisionContextELMoConverter,
        'fine_tuning_bert_converter': FineTuningBERTConverter,
        'bert_converter': BERTConverter,
        'context_bert_converter': ContextBERTConverter,
        'kb_context_bert_converter': KBContextBERTConverter,
        'supervised_context_bert_converter': SupervisionContextBERTConverter,
        'dlu_context_converter': DLUContextConverter,
        'dlu_kb_context_converter': DLUKBContextConverter,
        'task2_base_converter': Task2BaseConverter,
        'task2_context_converter': Task2ContextConverter,
        'task2_kb_context_converter': Task2KBContextConverter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ConverterFactory.supported_converters[key]:
            return ConverterFactory.supported_converters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
