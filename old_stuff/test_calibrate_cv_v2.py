"""

@Author: Federico Ruggeri

@Date: 03/09/2019

Hyperopt hyper-parameters calibration.

** BEFORE RUNNING **

1. Check the following configuration files:
    calibrator_info.json, hyperopt_model_gridsearch.json, distributed_config.json, callbacks.json, data_loader.json,
    model_config.json, training_config.json

"""

import os

from keras.callbacks import EarlyStopping

import const_define as cd
from calibrators import HyperOptCalibrator
from data_loader import DataLoaderFactory
from utility.cross_validation_utils import PrebuiltCV
from utility.json_utils import load_json
from utility.test_utils_v2 import cross_validation
from utility.python_utils import merge

if __name__ == '__main__':
    # Step 1: Validator config

    cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))[cv_test_config['model_type']]
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    loader_additional_info = {key: value['value'] for key, value in model_config.items()
                              if 'data_loader' in value['flags']}
    data_loader_info = merge(data_loader_info, loader_additional_info)

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    # Step 2: Calibrator config

    calibrator_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALIBRATOR_INFO_NAME))

    # Callbacks
    callbacks_data = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CALLBACKS_NAME))
    early_stopper = EarlyStopping(**callbacks_data['earlystopping'])

    # CV
    cv = PrebuiltCV(n_splits=10, shuffle=True, random_state=None, held_out_key=cv_test_config['cv_held_out_key'])
    folds_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}.json'.format(cv_test_config['prebuilt_folds']))
    cv.load_folds(load_path=folds_path)

    validator_base_args = {
        'validation_percentage': cv_test_config['validation_percentage'],
        'data_handle': data_handle,
        'network_args': model_config,
        'model_type': cv_test_config['model_type'],
        'training_config': training_config,
        'repetitions': cv_test_config['repetitions'],
        'error_metrics': cv_test_config['error_metrics'],
        'error_metrics_additional_info': cv_test_config['error_metrics_additional_info'],
        'cv': cv,
        'compute_test_info': False,
        'callbacks': [early_stopper],
        'split_key': cv_test_config['split_key'],
        'build_validation': cv_test_config['build_validation']
    }

    calibrator = HyperOptCalibrator(model_type=cv_test_config['model_type'],
                                    validator_method=cross_validation,
                                    validator_base_args=validator_base_args,
                                    **calibrator_config)

    space = calibrator.load_space()
    calibrator.run(space=space)
