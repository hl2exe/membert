"""

@Author: Federico Ruggeri

@Date: 10/01/2019

"""

import io
from collections import Counter

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras_preprocessing.sequence import pad_sequences
from matplotlib import cm
from matplotlib import ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import logistic
import os
from utility.json_utils import load_json
import operator


def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Arguments:
        data       : A 2D numpy array of shape (N,M)
        row_labels : A list or array of length N with the labels
                     for the rows
        col_labels : A list or array of length M with the labels
                     for the columns
    Optional arguments:
        ax         : A matplotlib.axes.Axes instance to which the heatmap
                     is plotted. If not provided, use current axes or
                     create a new one.
        cbar_kw    : A dictionary with arguments to
                     :meth:`matplotlib.Figure.colorbar`.
        cbarlabel  : The label for the colorbar
    All other arguments are directly passed on to the imshow call.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Arguments:
        im         : The AxesImage to be labeled.
    Optional arguments:
        data       : Data used to annotate. If None, the image's data is used.
        valfmt     : The format of the annotations inside the heatmap.
                     This should either use the string format method, e.g.
                     "$ {x:.2f}", or be a :class:`matplotlib.ticker.Formatter`.
        textcolors : A list or array of two color specifications. The first is
                     used for values below a threshold, the second for those
                     above.
        threshold  : Value in data units according to which the colors from
                     textcolors are applied. If None (the default) uses the
                     middle of the colormap as separation.

    Further arguments are passed on to the created text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max()) / 2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[im.norm(data[i, j]) > threshold])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def plot_attention_distributions(attention, true_values, indexes=None,
                                 name='attention_distribution',
                                 masking='positive',
                                 show_best_only=True):
    # If indexes is None, attention should be [samples, mem_size]
    if indexes is None and len(attention.shape) != 2:
        raise RuntimeError('Invalid shape for attention vector: {}'.format(attention.shape))

    if indexes is not None:
        attention = [attention[indexes_group].tolist() for indexes_group in indexes]
        max_memory = max([len(item) for item in attention])
        samples = len(attention)
        attention = pad_sequences(attention, padding='post', dtype=np.float32)
    else:
        max_memory = attention.shape[1]
        samples = attention.shape[0]

    xpos = np.arange(max_memory)
    xpos = np.tile(xpos, samples)
    ypos = np.repeat(np.arange(len(indexes)), repeats=max_memory)
    zpos = np.zeros(samples * max_memory)

    # Ignore zero height bars
    dz = attention.ravel()
    dx = np.ones_like(dz) / 4
    dy = np.ones_like(dz) / 4

    fig = plt.figure(figsize=(8, 3), num=name)
    ax = Axes3D(fig)

    rep_true_values = np.repeat(true_values, repeats=max_memory)

    if masking.lower() == 'positive':
        mask = np.argwhere(rep_true_values)
        mask = mask.ravel()
    elif masking.lower() == 'negative':
        mask = np.argwhere(np.logical_not(rep_true_values))
        mask = mask.ravel()
    else:
        mask = np.arange(xpos.shape[0])

    # Ignore zero level bars
    height_mask = np.argwhere(dz)
    height_mask = height_mask.ravel()

    if show_best_only:
        best_mask = np.argmax(attention, axis=1)
        absolute_indexes = np.arange(attention.shape[0])
        best_mask = best_mask + absolute_indexes * max_memory

    final_mask = np.array(list(set(mask).intersection(set(height_mask))))

    if show_best_only:
        final_mask = np.array(list(set(best_mask).intersection(set(final_mask))))

    # Plot
    xplot = xpos[final_mask] - dx[final_mask] / 2
    yplot = ypos[final_mask] - dy[final_mask] / 2
    zplot = zpos[final_mask]
    dxplot = dx[final_mask]
    dyplot = dy[final_mask]
    dzplot = dz[final_mask]
    ax.bar3d(xplot, yplot, zplot, dxplot, dyplot, dzplot, shade=True, edgecolor='black')

    # Show grid
    ax.grid(True)
    ax.w_xaxis.gridlines.set_lw(2.0)
    ax.w_yaxis.gridlines.set_lw(2.0)

    # Set ticks
    ax.w_xaxis.set_ticks(np.arange(max_memory))
    ax.w_xaxis.set_ticklabels(np.arange(max_memory))

    # Labeling
    ax.set_xlabel('Memory slots')
    ax.set_ylabel('Samples')
    ax.set_zlabel('Probability')

    # fig.tight_layout()

    return fig


def plot_attention_contour(attention, true_values=None, indexes=None, masking='positive'):
    # If indexes is None, attention should be [samples, mem_size]
    if indexes is None and len(attention.shape) != 2:
        raise RuntimeError('Invalid shape for attention vector: {}'.format(attention.shape))

    if indexes is not None:
        attention = [attention[indexes_group].tolist() for indexes_group in indexes]
        max_memory = max([len(item) for item in attention])
        samples = len(attention)
        attention = pad_sequences(attention, padding='post', dtype=np.float32)
    else:
        max_memory = attention.shape[1]
        samples = attention.shape[0]

    if masking.lower() == 'positive' and true_values is not None:
        mask = np.argwhere(true_values)
        mask = mask.ravel()
    elif masking.lower() == 'negative' and true_values is not None:
        mask = np.argwhere(np.logical_not(true_values))
        mask = mask.ravel()
    else:
        mask = np.arange(attention.shape[0])

    xpos = np.arange(max_memory)
    ypos = np.arange(attention.shape[0])
    xpos, ypos = np.meshgrid(xpos, ypos)
    zpos = np.reshape(attention, newshape=(samples, max_memory))

    xpos = xpos[mask]
    ypos = ypos[mask]
    zpos = zpos[mask]

    fig, ax = plt.subplots(1, 1)

    CS = ax.contourf(xpos, ypos, zpos, 15, levels=np.arange(0, np.max(zpos), np.max(zpos) * 0.001),
                     cmap=cm.coolwarm)
    divider = make_axes_locatable(ax)
    cax = divider.new_vertical(size="5%", pad=0.5, pack_start=True)
    fig.add_axes(cax)
    cbar = plt.colorbar(CS, cax=cax, orientation='horizontal')

    # Show grid
    ax.grid(True)
    ax.set_xticks(np.arange(max_memory))

    # Labeling
    ax.set_xlabel('Memory slots')
    ax.set_ylabel('Samples')

    return fig


def plot_attention_contour_v2(attention, name=None):
    name = name or ""

    samples = attention.shape[0]
    memory_size = attention.shape[2]

    figures = []

    for hop in range(attention.shape[1]):
        # for hop in range(1):
        xpos = np.arange(memory_size)
        ypos = np.arange(samples)
        xpos, ypos = np.meshgrid(xpos, ypos)
        zpos = np.reshape(attention[:, hop, :], newshape=(samples, memory_size))

        fig, ax = plt.subplots(1, 1)
        ax.set_title('{0} Hop {1}'.format(name, hop + 1))

        CS = ax.contourf(xpos, ypos, zpos, 15, levels=np.arange(0, np.max(attention), np.max(attention) * 0.001),
                         cmap=cm.coolwarm)
        divider = make_axes_locatable(ax)
        cax = divider.new_vertical(size="5%", pad=0.5, pack_start=True)
        fig.add_axes(cax)
        cbar = plt.colorbar(CS, cax=cax, orientation='horizontal')

        # Show grid
        ax.grid(True)
        ax.set_xticks(np.arange(memory_size))

        # Labeling
        ax.set_xlabel('Memory slots')
        ax.set_ylabel('Samples')

        figures.append(fig)

    return figures


def memory_histogram(attention_weights, model_path, filter_unfair=False,
                     true_values=None, memory_labels=None):

    memory_size = None
    counter = None
    for idx, weight_name in enumerate(attention_weights):
        sub_path = os.path.join(model_path, weight_name)
        fold_name = weight_name.split('fold')[1].split('_')[1]

        loaded_weights = load_json(sub_path)

        if memory_size is None:
            memory_size = loaded_weights.shape[2]

        if filter_unfair:
            loaded_weights = loaded_weights[np.argwhere(true_values[int(fold_name)]).ravel()]

        selected_memories = np.argmax(loaded_weights, axis=2)
        selected_memories = [list(set(item)) for item in selected_memories]
        flat_selections = [item for seq in selected_memories for item in seq]

        if idx == 0:
            counter = Counter(flat_selections)
        else:
            counter.update(flat_selections)

    print('Distinct memories uses: ', counter)

    fig, ax = plt.subplots()
    ax.set_title('Memory usage', fontsize=32)

    # memory_indexes = np.arange(memory_size) + 1
    # counter_values = np.zeros_like(memory_indexes, dtype=np.int64)
    # for key, item in counter.items():
    #     counter_values[key] = item
    memory_indexes = np.arange(len(counter)) + 1
    used_memories = np.array(list(counter.keys())) + 1
    if memory_labels is not None:
        used_memories = [memory_labels[item - 1] for item in used_memories]
    counter_values = list(counter.values())

    ax.bar(memory_indexes, counter_values, align='center')

    ax.set_xlabel('Memory slots', fontsize=32)
    ax.set_ylabel('Selections amount', fontsize=32)
    ax.set_xticks(memory_indexes)
    ax.set_xticklabels(used_memories)
    ax.tick_params(axis='both', labelsize=24)

    for idx, value in enumerate(counter_values):
        if value > 0:
            ax.text(idx + 0.88, value + 3, str(value), color='k', fontweight='bold', fontsize=24)

    if memory_labels is not None:
        for tick in ax.get_xticklabels():
            tick.set_rotation(75)

    return fig


def compute_trajectories_distribution(attention):
    attention = np.argmax(attention, axis=2)

    # Convert to string by concatenating
    str_operation = lambda seq: '-'.join(seq.astype(np.str))
    attention_str = np.apply_along_axis(func1d=str_operation, axis=1, arr=attention)

    counter = Counter(attention_str)
    return counter


def visualize_unfair_trajectories(unfair_info, fold_name, key, aggregate=False):
    # print('Fold {0} {1} info: {2}'.format(fold_name, key, unfair_info))
    str_op = lambda seq: '-'.join(seq.astype(np.str))
    if not aggregate:
        info_values = [item['attention'] for _, item in unfair_info.items()]
        info_values = np.array(info_values)
        info_values_str = np.apply_along_axis(func1d=str_op, axis=1, arr=info_values)
    else:
        info_values = [np.unique(item['attention']) for _, item in unfair_info.items()]
        info_values_str = [str_op(item) if len(item) > 1 else str(item[0]) for item in info_values]
    counter = Counter(info_values_str)
    print('{0} trajectories distribution fold {1}: {2}'.format(key, fold_name, counter))


def plot_attention_trajectories(unfair_info, fold_name, key):
    info_values = [item['attention'] for _, item in unfair_info.items()]
    info_values = np.array(info_values)
    info_values = info_values + 1

    fig, ax = plt.subplots()
    ax.set_title('{0}_trajectories_fold_{1}'.format(key, fold_name))

    hops = np.arange(len(info_values[0])) + 1

    for traj in info_values:
        ax.plot(traj, hops, linewidth=1, marker='D', markersize=6, linestyle='dashed', c='r')

    ax.set_xlabel('Memory slots')
    ax.set_ylabel('Memory iterations')
    ax.set_xticks(np.arange(np.max(info_values)) + 1)

    # Annotate points
    for hop in hops:
        hop_counter = Counter(info_values[:, hop - 1])
        for value, count in hop_counter.items():
            ax.annotate(count, (value + 0.2, hop), size=16)


def plot_memory_selections(attention, width=0.35, name=None):
    name = name or ""

    # [#samples, #hops, memory_size]
    assert len(attention.shape) == 3

    memory_size = attention.shape[2]
    hops = attention.shape[1]

    # [#samples, #hops]
    attention = np.argmax(attention, axis=2)

    hop_counts = []
    for hop in range(hops):
        c = Counter(attention[:, hop])
        hop_values = list(c.values())
        hop_keys = list(c.keys())
        adjusted = np.zeros(memory_size, dtype=np.int32)
        for key, value in zip(hop_keys, hop_values):
            adjusted[key] = value
        hop_counts.append(adjusted)

    hop_counts = np.array(hop_counts)
    memory_indexes = np.arange(memory_size) + 1

    fig, ax = plt.subplots(1, 1)
    ax.set_title('{0}'.format(name))

    for hop in range(hops):
        if hop == 0:
            ax.bar(memory_indexes, hop_counts[hop], width=width)
        else:
            ax.bar(memory_indexes, hop_counts[hop], width=width, bottom=np.sum(hop_counts[:hop], axis=0))

    ax.set_xlabel('Memory slots')
    ax.set_ylabel('Selections amount')
    ax.set_xticks(memory_indexes)

    return fig


def show_target_coverage(attention, targets_info, kb, fold_name, predictions, true_values):
    def get_hits(target, predicted):
        target = set(target)
        predicted = set(predicted)
        intersection = predicted.intersection(target)
        hits = len(intersection)
        missed = target.difference(intersection)
        others = predicted.difference(intersection)
        return hits, missed, others

    # Get targets attention weights
    total_hits = 0
    total_correct_missed = 0
    total_correct_got = 0
    for target_id, sample_text in targets_info.items():
        target_id = int(target_id)
        print('*' * 20)
        print('Sample ', target_id, ': ', sample_text)
        target_values = kb[sample_text]
        predicted_label = predictions[target_id]
        true_label = true_values[target_id]
        predicted_values = np.argmax(attention[target_id], axis=1).ravel().tolist()
        hits, missed, others = get_hits(target=target_values, predicted=predicted_values)

        print('Hits: ', hits,
              ' Missed: ', missed,
              ' Others: ', others,
              'Predicted Label: ', predicted_label,
              'True Label: ', true_label)
        print('*' * 20)
        if hits > 0:
            total_hits += 1
        if hits > 0:
            total_correct_got += int(predicted_label == true_label)
        else:
            total_correct_missed += int(predicted_label == true_label)

    print('Fold {0} total hits: {1}/{2},'
          ' total correct with hits: {3}/{1}, '
          ' total correct with no hits: {4}/{5}'.format(fold_name,
                                                      total_hits,
                                                      len(targets_info),
                                                        total_correct_got,
                                                        total_correct_missed,
                                                        len(targets_info) - total_hits))


def plot_gates_distributions(gates, true_values, name='gates_distribution', apply_sigmoid=True, masking='positive'):
    if apply_sigmoid:
        gates = logistic.cdf(gates)

    fig = plt.figure(figsize=(8, 3), num=name)
    ax = plt.subplot(111)

    if masking.lower() == 'positive':
        mask = np.argwhere(true_values)
        mask = mask.ravel()
    elif masking.lower() == 'negative':
        mask = np.argwhere(np.logical_not(true_values))
        mask = mask.ravel()
        gate_mask = np.argwhere(gates.ravel() >= 0.5)
        gate_mask = gate_mask.ravel()
        mask = np.array(list(set(mask).intersection(set(gate_mask))))
    else:
        mask = np.arange(gates.shape[0])

    ax.plot(gates[mask])
    ax.axhline(y=0.5, c='red', linewidth=1.0)

    # Show grid
    ax.grid(True)

    # Labeling
    ax.set_xlabel('Samples')
    ax.set_ylabel('Gate Activation')

    fig.tight_layout()

    return fig


# @tfmpl.figure_tensor
def create_plot(probs, true_values, name, indexes=None):
    fig = plot_attention_distributions(attention=probs, indexes=indexes, true_values=true_values, name=name)
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    return image
    # return fig
