"""

@Author: Federico Ruggeri

@Date: 09/05/2019

Caricamento in base agli indici
Caricamento condizionato da flag -> train/val split

Si parte sempre dalla lista di documenti -> lista files -> lunghezza righe -> indici

Due livelli di indici -> indice documento, indice riga -> ordinati in base al primo indice per limitare il numero di accessi al file

Dal doc_id devo risalire al nome del documento


"""

import os
from old_stuff.loader import load_file_data, load_data_labels
import numpy as np
from itertools import tee


class Streamer(object):

    def __init__(self, doc_mappings, data_base_path, labels_base_path):
        self.doc_mappings = doc_mappings
        self.data_base_path = data_base_path
        self.labels_base_path = labels_base_path

    def _go_over_all_data(self, names, is_data=True):
        for doc_id in names:
            if is_data:
                doc_path = os.path.join(self.data_base_path, self.doc_mappings[doc_id])
                data = load_file_data(doc_path)
            else:
                data = load_data_labels(self.labels_base_path, self.doc_mappings[doc_id])

            data = np.array(data)

            for item in data:
                yield item

    def _go_over_conditioned_data(self, indexes, is_data=True):
        for doc_id, line_ids in indexes.items():
            if is_data:
                doc_path = os.path.join(self.data_base_path, self.doc_mappings[doc_id])
                data = load_file_data(doc_path)
            else:
                data = load_data_labels(self.labels_base_path, self.doc_mappings[doc_id])

            data = np.array(data)

            for item in data[line_ids]:
                yield item

    def iterate_over_data(self, indexes, is_data=True):
        """
        :param indexes: dictionary key -> document index, value -> list of line indexes || list
        :param is_data
        :return:
        """

        if type(indexes) == dict:
            data = self._go_over_conditioned_data(indexes=indexes, is_data=is_data)
        else:
            data = self._go_over_all_data(names=indexes, is_data=is_data)

        for item in data:
            yield item

    @staticmethod
    def split(data, index):
        _, clone = tee(data)

        if type(index) == tuple:
            start_index = index[0]
            end_index = index[1]

            for item in clone:
                yield item[start_index:end_index]
        else:
            for item in clone:
                yield item[index]

    @staticmethod
    def to_stream(x):
        for item in x:
            yield item

