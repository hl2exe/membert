"""

@Author: Federico Ruggeri

@Date: 15/12/18

"""

from utility.log_utils import get_logger
from converter import BoWConverter, SimpleContextConverter, SimpleSupervisionContextConverter, BowSupervisionConverter

logger = get_logger(__name__)


class ConverterFactory(object):

    supported_converters = {
        'bow_converter': BoWConverter,
        'bow_supervised_converter': BowSupervisionConverter,
        'simple_context_converter': SimpleContextConverter,
        'simple_supervised_context_converter': SimpleSupervisionContextConverter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if ConverterFactory.supported_converters[key]:
            return ConverterFactory.supported_converters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
