""""

@Author: Federico Ruggeri

@Date: 06/05/2019

"""

import os
import const_define as cd
from utility.json_utils import load_json
from collections import OrderedDict, Counter
from old_stuff.loader import load_data_labels, iterate_over_files
import numpy as np

# Step 0: settings

model_type = 'unfair_ememn2n_tf'
network_id = '01-05-2019-13-09-55'

# Step 1: load test data labels

cv_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_CV_TEST_CONFIG_NAME))

dataset_path = cd.DATASET_PATHS[cv_test_config['dataset']]

labels_base_path = os.path.join(dataset_path, cv_test_config['labels_sub_folder'])

y_data = OrderedDict()

for (data_sub_path, doc_name) in iterate_over_files(base_path=labels_base_path):

    # Load labels
    labels = load_data_labels(base_path=labels_base_path, doc_name=doc_name)
    y_data[doc_name] = labels


# Step 2: build test folds

folds_data = load_json(os.path.join(cd.PREBUILT_FOLDS_DIR, '{}_splits_10_kfold.json'.format(cv_test_config['dataset'])))
list_path = os.path.join(cd.PREBUILT_FOLDS_DIR, '{}_splits_10_kfold.txt'.format(cv_test_config['dataset']))

with open(list_path, 'r') as f:
    dataset_list = [item.strip() for item in f.readlines()]


fold_labels = OrderedDict()

for idx in range(10):
    fold_name = 'fold_{}'.format(idx)

    test_indexes = folds_data[fold_name]['test']
    test_docs = [name for idx, name in enumerate(dataset_list) if idx in test_indexes]

    test_labels = OrderedDict([(key, value) for key, value in y_data.items() if key in test_docs])
    test_labels = list(test_labels.values())
    test_labels = np.array([label for doc_labels in test_labels for label in doc_labels])

    fold_labels['{}'.format(idx)] = test_labels

# Step 3: load model predictions and memories

network_data_path = os.path.join(cd.CV_DIR, model_type, network_id)
network_predictions = load_json(os.path.join(network_data_path, 'predictions.json'))
network_memories = load_json(os.path.join(network_data_path, 'weights.json'))

for idx in range(10):
    fold_name = '{}'.format(idx)

    correct = np.where(fold_labels[fold_name] == network_predictions[fold_name])[0]
    correct_memories = network_memories[fold_name][correct]

    correct_and_unfair = np.where(fold_labels[fold_name][correct] == 1)[0]
    correct_and_unfair_memories = correct_memories[correct_and_unfair]

    wrong = np.where(fold_labels[fold_name] != network_predictions[fold_name])[0]
    wrong_memories = network_memories[fold_name][wrong]

    wrong_and_unfair = np.where(fold_labels[fold_name][wrong] == 1)[0]
    wrong_and_unfair_memories = network_memories[fold_name][wrong_and_unfair]

    # Count select memories for each (for each hop)
    hops = len(network_memories[fold_name][0])

    predictions_counters = [Counter(network_memories[fold_name][:, hop_id]) for hop_id in range(hops)]

    correct_counters = [Counter(correct_memories[:, hop_id]) for hop_id in range(hops)]
    correct_and_unfair_counters = [Counter(correct_and_unfair_memories[:, hop_id]) for hop_id in range(hops)]
    wrong_counters = [Counter(wrong_memories[:, hop_id]) for hop_id in range(hops)]
    wrong_and_unfair_counters = [Counter(wrong_and_unfair_memories[:, hop_id]) for hop_id in range(hops)]

    print('Predictions')
    print(predictions_counters)
    print('Fold: {}'.format(idx))
    print('Correct Counters')
    print(correct_counters)
    print('\n\n')
    print('Correct and Unfair Counters')
    print(correct_and_unfair_counters)
    print('\n\n')
    print('Wrong Counters')
    print(wrong_counters)
    print('\n\n')
    print('Wrong and Unfair Counters')
    print(wrong_and_unfair_counters)
    print('\n\n\n\n')