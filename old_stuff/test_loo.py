"""

@Author: Federico Ruggeri

@Date: 29/11/2018

"""

import os
from datetime import datetime

import numpy as np

import const_define as cd
import reporters
from old_stuff.data_loader_factory import DataLoaderFactory
from utility.json_utils import save_json, load_json
from utility.log_utils import get_logger
from utility.test_utils import loo_test_mt, loo_test

logger = get_logger(__name__)


if __name__ == '__main__':

    loo_test_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_LOO_TEST_CONFIG_NAME))

    dataset_path = cd.DATASET_PATHS[loo_test_config['dataset']]

    data_base_path = os.path.join(dataset_path, loo_test_config['data_sub_folder'])
    labels_base_path = os.path.join(dataset_path, loo_test_config['labels_sub_folder'])

    model_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_MODEL_CONFIG_NAME))[loo_test_config['model_type']]
    training_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_TRAINING_CONFIG_NAME))

    error_metrics = [
        "f1_score",
        "accuracy_score",
        "recall_score",
        "precision_score"
    ]
    error_metrics_additional_info = {
        "f1_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        },
        "recall_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        },
        "precision_score": {
            "labels": [
                0,
                1
            ],
            "pos_label": 1,
            "average": "binary"
        }
    }

    # Loading data
    data_loader_config = load_json(os.path.join(cd.CONFIGS_DIR, cd.JSON_DATA_LOADER_CONFIG_NAME))
    data_loader_type = data_loader_config['type']
    data_loader_info = data_loader_config['configs'][data_loader_type]
    data_loader_info['data_base_path'] = data_base_path
    data_loader_info['labels_base_path'] = labels_base_path

    data_loader = DataLoaderFactory.factory(data_loader_type)
    data_handle = data_loader.load(**data_loader_info)

    usage = 'singlethread'

    if usage == 'multithread':
        scores = loo_test_mt(validation_percentage=loo_test_config['validation_percentage'],
                             data_base_path=data_base_path,
                             n_jobs=3,
                             labels_base_path=labels_base_path,
                             doc_format=loo_test_config['doc_format'],
                             network_args=model_config,
                             model_type=loo_test_config['model_type'],
                             error_metrics=error_metrics,
                             error_metrics_additional_info=error_metrics_additional_info,
                             **training_config)
    else:
        scores = loo_test(validation_percentage=loo_test_config['validation_percentage'],
                          data_handle=data_handle,
                          network_args=model_config,
                          model_type=loo_test_config['model_type'],
                          error_metrics=error_metrics,
                          error_metrics_additional_info=error_metrics_additional_info,
                          compute_test_info=True,
                          training_config=training_config)

    current_date = datetime.today().strftime('%d-%m-%Y-%H-%M-%S')
    base_path = os.path.join(cd.LOO_DIR, loo_test_config['model_type'], current_date)

    if not os.path.isdir(base_path):
        os.makedirs(base_path)

    logger.info('Average validation f1: {}'.format(np.mean(scores[0]['f1_score'])))
    logger.info('Average test f1: {}'.format(np.mean(scores[1]['f1_score'])))

    save_path = os.path.join(base_path, 'scores.json')
    save_json(filepath=save_path, data=scores)

    reporters.save_model_info(folder=base_path, config_data=model_config)
    reporters.save_training_info(folder=base_path, training_info=training_config)
