## MemBERT: Injecting Unstructured Knowledge into BERT

## Repository maintainer: Federico Ruggeri

Repository is structured according to a WIP internal framework. For better understanding, we will briefly describe the overall project structure and, subsequently, describe how to reproduce results and launch experiments.

# Version Control

Current framework version is heavily based on tensorflow library.

* tensorflow-gpu: 2.3
* python: 3.6

For more information, please check ```requirements_v2.txt``` file.

# Project Structure

The framework proposes the following data pipeline:

* Data Loading
* Data Pre-processing
* Data Conversion
* Test Routine (e.g. cross-validation)
* Model Definition
* Model Training
* Model Evaluation
* Results post-processing

The framework is heavily from Keras library and basically extends some of its functionality and offers a whole modular pipeline for quick benchmarking.


## Framework Workflow

We now give more details about each pipeline step while pointing out the corresponding python script files for quick inspection.

* Data Loading (```data_loader.py```): each dataset is wrapped with a custom data loader object. Loaded data is then passed to a specific data wrapper that is compliant with the specified test routine.
* Data Pre-processing (```data_processor.py```): input data is converted into a set of input examples. In particular, text pre-processing operations like text normalization and tokenization are applied.
* Data Conversion (```data_converter.py```): textual data is then converted into numerical format in order to be compliant with a neural network model. Data is serialized by using Tensorflow APIs.
* Test Routine (```utility/distributed_test_utils.py```): all test routines are defined here. Each routine follows an internal workflow:
    * Load evaluation metrics
    * Load model specific configuration ID
    * Build workflow wrappers: data pre-processing, data tokenization, data conversion
    * Test routine loop:
        * Get data splits (train, val, test)
        * Build dataset if it does not exist
        * Build network model
        * Model training
        * Model evaluation
    * Wrap-up test routine performance scores
* Save model configuration, test routine configuration, performance scores

## Configuration files

All project scripts are heavily based on JSON configuration files (check ```/configs``` folder). Intuitively, each configuration file is dedicated to a particular workflow step, spanning from model parameters (```configs/distributed_model_config.json```) to test routine specific arguments (```configs/cv_test_config.json```).


# FAQ

## How to run an experiment?

There are two ways to run an experiment. First of all, runnable scripts are stored in the ```runnables/``` folder.

### Manual way

You can manually modify each JSON configuration file and then launch test routine scripts like ```runnables/test_cross_validation_v2.py```. However, due to the large amount of parameters and configuration files, this is not the recommended way.

### Script-based way

To avoid any kind of error, you can automatically configure configuration files for your model by running ```runnables/other/quick_test_setup.py```. The script can be also launched from terminal (check argparse help).

You only have to specify 5-6 arguments and you are then ready to launch your routine script (e.g. ```runnables/test_cross_validation_v2.py```). 

*Before that, don't forget to check model configuration (```configs/distributed_model_config.json```).*


## How to run sequential experiments?

It is also possible to run experiments in sequence. Open and modify ```runnables/run_test_sequence.py``` according to you needs.

Unfortunately, the script currently does not support argparse, but the behaviour is quite simple: it basically runs the following sequence of commands:

* ```runnables/other/quick_test_setup.py```
* ```runnables/test_cross_validation_v2.py```

In particular, the script waits for the termination of each executed script.

It is also possible to run ```runnables/run_test_sequence.py``` in parallel. Each instance modifies configuration files without interference via a lock system.

## Where do test specific files get saved? (model, configuration files, etc..)

Everytime a test routine is executed by means of a specific runnable test script, e.g. ```runnables/test_cross_validation.py```, a folder is created concerning current test instance. Depending on the given test, the folder is located in a corresponding root folder.

For instance, cross-validation tests are saved into ```cv_test``` folder. Model architectures are distinguished by a specific root folder as well.
The overall architecture is as follows:

* test_routine_folder
    * model_architecture_folder
        * test_instance_folder

In particular, the ```test_instance_folder``` is by default determined by the test instance execution time under the following date format: DD-MM-YYYY-hh-mm-ss.

However, it is also possible to specificy a specific name (check ```configs/cv_test_config.json```) by which the folder will be renamed at test completion. 

## What information gets saved into each test instance folder?

A lot of information for reproducibility and easy tracing is stored.

* Configuration files (a copy of ```configs/``` directory)
* Model weights (.h5) and, if any, model specific states. For instance, sampling strategies require to save the learnt priority distribution.
* Test evaluation metrics: both for validation and test sets.
* Model predictions on test set

Files are saved progressively during the test routine, i.e. cross-validation, so that it is possible to resume the test from the latest checkpoint or to quickly inspect model content.

## How to acquire model attention scores w.r.t. memory?

Once a test instance has completed, it is possible to quickly collect attention weights. Just execute ```runnables/other/gather_attention_distributions_v2.py```. The script can be executed from command line as well (check argparse help).

The script basically does a cv-test only from the inference perspective (the trained model is loaded for prediction).

At the end of the process, attention weights are stored in the same test instance original folder. In particulr, attention weights are in JSON format and can be quickly inspected.

## How to compute memory related metrics?

In order to compute memory related metrics it is necessary to collect memory attention weights first. Check previous FAQ point (How to acquire model attention scores w.r.t. memory?).

To compute memory related metrics it is only necessary to execute ```runnables/other/visualize_voting_coverage.py``` script. The script automatically computes each memory related metric for each model repetition.

## How to plot memory related metrics?

In order to plot memory related metrics it is necessary to collect memory attention weights first. Check previous FAQ point (How to acquire model attention scores w.r.t. memory?).

To plot memory related metric it is only necessary to execute ```runnables/other/plot_voting_coverage```. 

Unfortunately, the script currently does not support command line execution via argparse. Thus, it is necessary to modify few script variables.

## How to quickly inspect evaluation metrics?

Run ```runnables/other/compute_cv_stats.py```. The script supports command line execution (check argparse help).

## How to inspect sampling priority distribution?

Sampling priority distributions are automatically saved during training and can be inspected (numpy file).

You can also visualize how the priority distribution has changed during training. Execute ```runnables/other/inspect_sampling_weights.py```.

Unfortunately, the script currently does not support command line execution via argparse. Thus, it is necessary to modify few script variables.

## Where are the KB stored?

KB content is stored into different folders depending on the given dataset. In general, datasets are stored in ```local_database``` folder.

Here's the list of KB content for each dataset and its corresponding folder:

* ToS-30: KB is stored in ```local_database/KB``` folder.
* IBM2015: KB is stored in ```local_database/IBM_Debater_(R)_CE-EMNLP-2015.v3/ibm2015_memory_*topic_subsets*```.

## Are there any cv-test pre-built folds for quick comparison?

Yes of course! Please check ```prebuild_folds``` folder. It contains all prebuilt cv test folds.

In particular, there's a JSON file containing fold indexes, whereas, corresponding indexed data is stored in a TXT file with the same name.

# Contact

Federico Ruggeri: federico.ruggeri6@unibo.it

# Credits

Many thanks to all these people for their valuable feedback!

* *Andrea Galassi*
* Federico Baldo
* Giorgio Visani
* Mattia Silvestri
* Michele Lombardi
* Fabrizio Detassis
* Antonio Macaluso

Special thanks to:

* Marco Lippi
* Paolo Torroni