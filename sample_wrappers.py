"""

Simple wrappers for each dataset example

"""

from ast import literal_eval
from collections import OrderedDict
from functools import reduce

import numpy as np
import tensorflow as tf

from utility.log_utils import get_logger
from utility.wrapping_utils import create_int_feature, create_string_feature

logger = get_logger(__name__)


# TODO: reformat get_feature_records and get_mappings

# Examples


class TextExample(object):

    def __init__(self, guid, text, label=None):
        self.guid = guid
        self.text = text
        self.label = label

    def get_data(self):
        return self.text


class PairwiseTextExample(object):

    def __init__(self, example_id, text, explanation=None, label=None):
        self.example_id = example_id
        self.text = text
        self.explanation = explanation
        self.label = label

    def get_data(self):
        if self.explanation is not None:
            return self.text + ' ' + self.explanation
        else:
            return self.text


class TextKBExample(TextExample):

    def __init__(self, kb, **kwargs):
        super(TextKBExample, self).__init__(**kwargs)
        self.kb = kb

    def get_data(self):
        return self.text


class TextKBSupervisionExample(TextKBExample):

    def __init__(self, targets, **kwargs):
        super(TextKBSupervisionExample, self).__init__(**kwargs)
        self.targets = targets


class TextSupervisionExample(TextExample):

    def __init__(self, targets, **kwargs):
        super(TextSupervisionExample, self).__init__(**kwargs)
        self.targets = targets


class TextRationaleExample(object):

    def __init__(self, guid, doc, doc_label=None, sentence_targets=None):
        self.guid = guid
        self.doc = doc
        self.doc_label = doc_label
        self.sentence_targets = sentence_targets

    def get_data(self):
        return self.doc


class TextSquadExample(object):

    def __init__(self, question_tokens, context_tokens, answer_texts=None,
                 answer_starts=None, answer_ends=None):
        self.question_tokens = question_tokens
        self.context_tokens = context_tokens
        self.answer_texts = answer_texts
        self.answer_starts = answer_starts
        self.answer_ends = answer_ends

    def get_data(self):
        return ' '.join(self.context_tokens) + ' ' + ' '.join(self.question_tokens)


class BertTextSquadExample(object):

    def __init__(self, question_ids, question_attention_mask, context_ids, context_attention_mask,
                 answer_texts=None, answer_starts=None, answer_ends=None):
        self.question_ids = question_ids
        self.question_attention_mask = question_attention_mask
        self.context_ids = context_ids
        self.context_attention_mask = context_attention_mask
        self.answer_texts = answer_texts
        self.answer_starts = answer_starts
        self.answer_ends = answer_ends

    def get_data(self):
        return None


# List wrappers

class TextExampleList(object):

    def __init__(self):
        self.content = []
        self.added_state = set()

    def __iter__(self):
        return self.content.__iter__()

    def append(self, item):
        self.content.append(item)

    def __len__(self):
        return len(self.content)

    def __getitem__(self, item):
        return self.content[item]

    def add_state(self, property_name, property_value):
        setattr(self, property_name, property_value)
        self.added_state.add(property_name)

    def get_state(self, property_name):
        return getattr(self, property_name, None)

    def get_added_state(self):
        return {key: self.get_state(key) for key in self.added_state}

    def get_data(self):
        return [item.get_data() for item in self.content]


class TextKBExampleList(TextExampleList):

    def _flatten_kb(self):
        kb = self.get_state('kb')
        return reduce(lambda a, b: a + b, list(kb.values()))

    def get_data(self):
        assert hasattr(self, 'kb')
        texts = super(TextKBExampleList, self).get_data()
        return texts + self._flatten_kb()


# Features


class Features(object):

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        raise NotImplementedError()

    @classmethod
    def _retrieve_default_label_mappings(cls, mappings, conversion_args, converter_args=None, has_labels=True):
        num_labels = conversion_args['num_labels']
        num_classes_per_label = conversion_args['num_classes_per_label']
        label_map = conversion_args['label_map']

        if has_labels:
            # Multi-label
            if num_labels > 1:
                for output_label in label_map:
                    mappings['label_id_{}'.format(output_label)] = tf.io.FixedLenFeature(
                        [num_classes_per_label[output_label]], tf.int64)

            # Multi-class
            else:
                mappings['label_id'] = tf.io.FixedLenFeature([num_classes_per_label], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        raise NotImplementedError()

    @classmethod
    def _retrieve_default_label_feature_records(cls, feature, features, converter_args=None):
        if feature.label_id is not None:
            # Multi-label
            if type(feature.label_id) == OrderedDict:
                for key, value in feature.label_id.items():
                    features['label_id_{0}'.format(key)] = create_int_feature(value)

            # Multi-class
            else:
                features['label_id'] = create_int_feature(feature.label_id)

        return features

    @classmethod
    def get_dataset_selector(cls):
        raise NotImplementedError()

    @classmethod
    def _retrieve_default_label_dataset_selector(cls, x, record):
        labels = [key for key in record if key.startswith('label_id')]
        if len(labels):
            if len(labels) == 1:
                y = record['label_id']
            else:
                y = {key: record[key] for key in labels}
            return x, y
        else:
            return x

    @classmethod
    def _convert_labels(cls, example_label, label_list, has_labels=True, converter_args=None):
        """

        Case: multi-class
            example_label: A, B, C, D
            label_list: [A, B, C, D]
            label_map: {
                A: [0, 0, 0, 1]
                B: [0, 0, 1, 0]
                C: [0, 1, 0, 0]
                D: [1, 0, 0, 0]
            }

        Case: multi-label
            example_label: {
                A: A1,
                B: B2,
                C: C1,
                D: D3
            }
            label_list: {
                A: [A1, A2, A3],
                B: [B1, B2],
                C: [C1, C2, C3],
                D: [D1, D2, D3, D4]
            }
            label_map: {
                A: {
                    A1: [0, 1],
                    A2: [1, 0]
                },
                B: {
                    B1: [0, 0, 1],
                    B2: [0, 1, 0],
                    B3: [1, 0, 0]
                },
                C: { ... },
                D: { ... }
            }

        """

        label_map = {}
        label_id = None

        if label_list is not None or has_labels:
            # Multi-label
            if type(example_label) == OrderedDict:
                for output_label, label_values in label_list.items():
                    for value_idx, label_value in enumerate(label_values):
                        hot_encoding = [0] * len(label_values)
                        hot_encoding[value_idx] = 1
                        label_map.setdefault(output_label, {}).setdefault(label_value, hot_encoding)

                label_id = OrderedDict([(key, label_map[key][value]) for key, value in example_label.items()])

            # Multi-class
            else:
                for value_idx, label_value in enumerate(label_list):
                    hot_encoding = [0] * len(label_list)
                    hot_encoding[value_idx] = 1
                    label_map.setdefault(label_value, hot_encoding)

                label_id = label_map[example_label]

        return label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):
        raise NotImplementedError()

    @classmethod
    def convert_wrapper_state(cls, tokenizer, wrapper_state={}, converter_args=None):
        raise NotImplementedError()

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):
        raise NotImplementedError()


class TextFeatures(Features):

    def __init__(self, text_ids, label_id):
        self.text_ids = text_ids
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        max_seq_length = conversion_args['max_seq_length']

        mappings = dict()
        mappings['text_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)

        mappings = cls._retrieve_default_label_mappings(mappings=mappings,
                                                        conversion_args=conversion_args,
                                                        converter_args=converter_args,
                                                        has_labels=has_labels)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = OrderedDict()
        features['text_ids'] = create_int_feature(feature.text_ids)

        features = cls._retrieve_default_label_feature_records(feature=feature,
                                                               features=features,
                                                               converter_args=converter_args)

        return features

    @classmethod
    def get_dataset_selector(cls):
        def _selector(record):
            x = {
                'text': record['text_ids'],
            }
            return cls._retrieve_default_label_dataset_selector(x, record)

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):
        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        tokens = tokenizer.tokenize(example.text)
        text_ids = tokenizer.convert_tokens_to_ids(tokens)

        return text_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):
        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        text_ids, label_id, label_map = TextFeatures.convert_example(example=example, label_list=label_list,
                                                                     tokenizer=tokenizer, has_labels=has_labels,
                                                                     converter_args=converter_args,
                                                                     conversion_args=conversion_args,
                                                                     wrapper_state=wrapper_state)

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length

        feature = TextFeatures(text_ids=text_ids, label_id=label_id)
        return feature


class PairwiseTextFeatures(TextFeatures):

    def __init__(self, example_id, text_ids, explanation_ids, label_id):
        super(PairwiseTextFeatures, self).__init__(text_ids=text_ids, label_id=label_id)
        self.example_id = example_id
        self.explanation_ids = explanation_ids

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        mappings = super(PairwiseTextFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                 converter_args=converter_args,
                                                                 has_labels=has_labels)
        max_seq_length = conversion_args['max_seq_length']
        mappings['explanation_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)
        mappings['example_id'] = tf.io.FixedLenFeature([], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = super(PairwiseTextFeatures, cls).get_feature_records(feature=feature,
                                                                        converter_args=converter_args)
        features['explanation_ids'] = create_int_feature(feature.explanation_ids)
        features['example_id'] = create_int_feature([feature.example_id])

        return features

    @classmethod
    def get_dataset_selector(cls):
        def _selector(record):
            x = {
                'text': record['text_ids'],
                'example_id': record['example_id'],
                'explanation': record['explanation_ids']
            }
            return cls._retrieve_default_label_dataset_selector(x, record)

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):
        text_ids, label_id, label_map = super(PairwiseTextFeatures, cls).convert_example(example=example,
                                                                                         label_list=label_list,
                                                                                         tokenizer=tokenizer,
                                                                                         has_labels=has_labels,
                                                                                         converter_args=converter_args,
                                                                                         conversion_args=conversion_args,
                                                                                         wrapper_state=wrapper_state)

        if example.explanation:
            explanation_tokens = tokenizer.tokenize(example.explanation)
            explanation_ids = tokenizer.convert_tokens_to_ids(explanation_tokens)
        else:
            explanation_ids = []

        return text_ids, explanation_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):
        if not isinstance(example, PairwiseTextExample):
            raise AttributeError('Expected PairwiseTextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        text_ids, explanation_ids, \
        label_id, label_map = cls.convert_example(example=example, label_list=label_list,
                                                  tokenizer=tokenizer, has_labels=has_labels,
                                                  converter_args=converter_args,
                                                  conversion_args=conversion_args,
                                                  wrapper_state=wrapper_state)

        # Padding
        text_ids += [0] * (max_seq_length - len(text_ids))
        text_ids = text_ids[:max_seq_length]

        explanation_ids += [0] * (max_seq_length - len(explanation_ids))
        explanation_ids = explanation_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length
        assert len(explanation_ids) == max_seq_length

        feature = cls(text_ids=text_ids, label_id=label_id, explanation_ids=explanation_ids,
                      example_id=example.example_id)
        return feature


class TextKBFeatures(TextFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        text_tokens = tokenizer.tokenize(example.text)
        text_ids = tokenizer.convert_tokens_to_ids(text_tokens)

        return text_ids, label_id, label_map

    @classmethod
    def convert_wrapper_state(cls, tokenizer, wrapper_state={}, converter_args=None):

        assert 'kb' in wrapper_state

        kb = OrderedDict()
        for key, value in wrapper_state['kb'].items():
            value_tokens = list(map(lambda t: tokenizer.tokenize(t), value))
            value_ids = tokenizer.convert_tokens_to_ids(value_tokens)
            kb[key] = {
                'kb_ids': value_ids,
                'kb_mask': [0] * len(value_ids)
            }

        return kb

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        text_ids, label_id, label_map = TextKBFeatures.convert_example(example, label_list, tokenizer,
                                                                       has_labels=has_labels,
                                                                       converter_args=converter_args,
                                                                       conversion_args=conversion_args,
                                                                       wrapper_state=wrapper_state)

        # Padding
        if len(text_ids) < max_seq_length:
            text_ids += [0] * (max_seq_length - len(text_ids))

        text_ids = text_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length

        feature = TextKBFeatures(text_ids=text_ids, label_id=label_id)
        return feature


class TextKBSupervisionFeatures(TextKBFeatures):

    def __init__(self, target_mask, **kwargs):
        super(TextKBSupervisionFeatures, self).__init__(**kwargs)
        self.target_mask = target_mask

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        mappings = super(TextKBSupervisionFeatures, cls).get_mappings(conversion_args=conversion_args,
                                                                      converter_args=converter_args,
                                                                      has_labels=has_labels)

        max_kb_length = conversion_args['max_kb_length']
        mappings['target_mask'] = tf.io.FixedLenFeature([max_kb_length], dtype=tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = super(TextKBSupervisionFeatures, cls).get_feature_records(feature, converter_args)
        features['target_mask'] = create_int_feature(feature.target_mask)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'text': record['text_ids'],
                'target_mask': record['target_mask']

            }
            return cls._retrieve_default_label_dataset_selector(x, record)

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        text_tokens = tokenizer.tokenize(example.text)
        text_ids = tokenizer.convert_tokens_to_ids(text_tokens)

        kb_offset = 0
        max_kb_length = wrapper_state['max_kb_length'] if 'max_kb_length' in wrapper_state else conversion_args[
            'max_kb_length']
        kb_length = wrapper_state['kb_length'] if 'kb_length' in wrapper_state else conversion_args['kb_length']

        target_ids = [0] * max_kb_length

        for key, target_set in example.targets.items():
            key_kb_length = kb_length[key]
            if type(target_set) is not float and target_set is not None:
                target_set = literal_eval(target_set)
                target_set = [int(item) for item in target_set]
                target_ids[kb_offset: kb_offset + key_kb_length] = [1 if idx in target_set else 0
                                                                    for idx in range(key_kb_length)]

            kb_offset += key_kb_length

        return text_ids, target_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextSupervisionExample):
            raise AttributeError('Expected TextSupervisionExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']
        max_kb_length = conversion_args['max_kb_length']

        text_ids, target_ids, label_id, label_map = TextKBSupervisionFeatures.convert_example(example, label_list,
                                                                                              tokenizer,
                                                                                              has_labels=has_labels,
                                                                                              converter_args=converter_args,
                                                                                              conversion_args=conversion_args,
                                                                                              wrapper_state=wrapper_state)

        # Padding
        if len(text_ids) < max_seq_length:
            text_ids += [0] * (max_seq_length - len(text_ids))

        text_ids = text_ids[:max_seq_length]

        assert len(text_ids) == max_seq_length

        target_ids += [0] * (max_kb_length - len(target_ids))
        target_ids = np.array(target_ids)

        assert len(target_ids) == max_kb_length

        feature = TextKBSupervisionFeatures(text_ids=text_ids, label_id=label_id, target_mask=target_ids)
        return feature


class BertTextFeatures(Features):

    def __init__(self, input_ids, attention_mask, label_id):
        self.input_ids = input_ids
        self.attention_mask = attention_mask
        self.label_id = label_id

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        max_seq_length = conversion_args['max_seq_length']

        mappings = {
            'input_ids': tf.io.FixedLenFeature([max_seq_length], tf.int64),
            'attention_mask': tf.io.FixedLenFeature([max_seq_length], tf.int64),
        }

        mappings = cls._retrieve_default_label_mappings(mappings=mappings,
                                                        conversion_args=conversion_args,
                                                        converter_args=converter_args,
                                                        has_labels=has_labels)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = OrderedDict()
        features['input_ids'] = create_int_feature(feature.input_ids)
        features['attention_mask'] = create_int_feature(feature.attention_mask)

        features = cls._retrieve_default_label_feature_records(feature=feature,
                                                               features=features,
                                                               converter_args=converter_args)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'input_ids': record['input_ids'],
                'attention_mask': record['attention_mask'],
            }
            return cls._retrieve_default_label_dataset_selector(x, record)

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        inputs = tokenizer.tokenizer.encode_plus(
            example.text,
            None,
            add_special_tokens=True,
            max_length=min(converter_args['max_tokens_limit'], 512),
        )
        input_ids, \
        attention_mask = inputs["input_ids"], inputs['attention_mask']

        assert np.equal(attention_mask, [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)).all()

        return input_ids, attention_mask, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        input_ids, attention_mask, label_id, label_map = cls.convert_example(example=example,
                                                                             label_list=label_list,
                                                                             tokenizer=tokenizer,
                                                                             has_labels=has_labels,
                                                                             converter_args=converter_args,
                                                                             conversion_args=conversion_args,
                                                                             wrapper_state=wrapper_state)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)
        if converter_args['pad_on_left']:
            input_ids = ([converter_args['pad_token']] * padding_length) + input_ids
            attention_mask = ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length) + attention_mask
        else:
            input_ids = input_ids + ([converter_args['pad_token']] * padding_length)
            attention_mask = attention_mask + ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length)

        input_ids = input_ids[:max_seq_length]
        attention_mask = attention_mask[:max_seq_length]

        assert len(input_ids) == max_seq_length, "Error with input length {} vs {}".format(len(input_ids),
                                                                                           max_seq_length)
        assert len(attention_mask) == max_seq_length, "Error with input length {} vs {}".format(len(attention_mask),
                                                                                                max_seq_length)

        feature = cls(input_ids=input_ids, label_id=label_id,
                      attention_mask=attention_mask)
        return feature


class BertTextKBFeatures(BertTextFeatures):

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        inputs = tokenizer.tokenizer.encode_plus(
            example.text,
            None,
            add_special_tokens=True,
            max_length=min(converter_args['max_tokens_limit'], 512)
        )
        input_ids, \
        attention_mask = inputs["input_ids"], inputs['attention_mask']

        return input_ids, attention_mask, label_id, label_map

    @classmethod
    def convert_wrapper_state(cls, tokenizer, wrapper_state={}, converter_args=None):

        assert 'kb' in wrapper_state

        kb = OrderedDict()
        for key, value in wrapper_state['kb'].items():
            kb_input_ids, kb_attention_mask = [], []
            for text in value:
                curr_inputs = tokenizer.tokenizer.encode_plus(
                    text,
                    None,
                    add_special_tokens=True,
                    max_length=min(converter_args['max_tokens_limit'], 512)
                )
                kb_input_ids.append(curr_inputs['input_ids'])
                kb_attention_mask.append(curr_inputs['attention_mask'])

            kb[key] = {
                'kb_input_ids': kb_input_ids,
                'kb_attention_mask': kb_attention_mask,
                'kb_mask': [0] * len(kb_input_ids)
            }

        return kb

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']

        input_ids, attention_mask, \
        label_id, label_map = cls.convert_example(example=example, label_list=label_list, tokenizer=tokenizer,
                                                  has_labels=has_labels, converter_args=converter_args,
                                                  conversion_args=conversion_args,
                                                  wrapper_state=wrapper_state)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)
        # kb_attention_mask = [[[1 if converter_args['mask_padding_with_zero'] else 0] * len(item)] for item in
        #                      kb_input_ids]

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)

        if converter_args['pad_on_left']:
            input_ids = ([converter_args['pad_token']] * padding_length) + input_ids
            attention_mask = ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length) + attention_mask
        else:
            input_ids = input_ids + ([converter_args['pad_token']] * padding_length)
            attention_mask = attention_mask + ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length)

        # KB padding is not necessary since it is constant for each example

        assert len(input_ids) == max_seq_length, "Error with input length {} vs {}".format(len(input_ids),
                                                                                           max_seq_length)
        assert len(attention_mask) == max_seq_length, "Error with input length {} vs {}".format(len(attention_mask),
                                                                                                max_seq_length)

        feature = cls(input_ids=input_ids, label_id=label_id,
                      attention_mask=attention_mask)
        return feature


class BertTextKBSupervisionFeatures(BertTextKBFeatures):

    def __init__(self, target_mask, **kwargs):
        super(BertTextKBSupervisionFeatures, self).__init__(**kwargs)
        self.target_mask = target_mask

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        mappings = super(BertTextKBSupervisionFeatures, cls).get_mappings(conversion_args, converter_args, has_labels)

        max_kb_length = conversion_args['max_kb_length']
        mappings['target_mask'] = tf.io.FixedLenFeature([max_kb_length], dtype=tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = super(BertTextKBSupervisionFeatures, cls).get_feature_records(feature, converter_args)
        features['target_mask'] = create_int_feature(feature.target_mask)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'input_ids': record['input_ids'],
                'attention_mask': record['attention_mask'],
                'target_mask': record['target_mask']
            }
            return cls._retrieve_default_label_dataset_selector(x, record)

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        inputs = tokenizer.tokenizer.encode_plus(
            example.text,
            None,
            add_special_tokens=True,
            max_length=min(converter_args['max_tokens_limit'], 512)
        )
        input_ids, \
        attention_mask = inputs["input_ids"], inputs['attention_mask']

        kb_offset = 0
        max_kb_length = wrapper_state['max_kb_length'] if 'max_kb_length' in wrapper_state else conversion_args[
            'max_kb_length']
        kb_length = wrapper_state['kb_length'] if 'kb_length' in wrapper_state else conversion_args['kb_length']

        target_ids = [0] * max_kb_length

        for key, target_set in example.targets.items():
            key_kb_length = kb_length[key]
            if type(target_set) is not float and target_set is not None:
                target_set = literal_eval(target_set)
                target_set = [int(item) for item in target_set]
                target_ids[kb_offset: kb_offset + key_kb_length] = [1 if idx in target_set else 0
                                                                    for idx in range(key_kb_length)]

            kb_offset += key_kb_length

        return input_ids, attention_mask, \
               target_ids, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextExample):
            raise AttributeError('Expected TextExample instance, got: {}'.format(type(example)))

        max_seq_length = conversion_args['max_seq_length']
        max_kb_length = conversion_args['max_kb_length']

        input_ids, attention_mask, \
        target_ids, label_id, label_map = cls.convert_example(example=example, label_list=label_list,
                                                              tokenizer=tokenizer, has_labels=has_labels,
                                                              converter_args=converter_args,
                                                              conversion_args=conversion_args,
                                                              wrapper_state=wrapper_state)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)
        # kb_attention_mask = [[[1 if converter_args['mask_padding_with_zero'] else 0] * len(item)] for item in
        #                      kb_input_ids]

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)

        if converter_args['pad_on_left']:
            input_ids = ([converter_args['pad_token']] * padding_length) + input_ids
            attention_mask = ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length) + attention_mask
        else:
            input_ids = input_ids + ([converter_args['pad_token']] * padding_length)
            attention_mask = attention_mask + ([0 if converter_args['mask_padding_with_zero'] else 1] * padding_length)

        target_ids += [0] * (max_kb_length - len(target_ids))
        target_ids = np.array(target_ids)

        # KB padding is not necessary since it is constant for each example
        input_ids = input_ids[:max_seq_length]
        attention_mask = attention_mask[:max_seq_length]

        assert len(input_ids) == max_seq_length, "Error with input length {} vs {}".format(len(input_ids),
                                                                                           max_seq_length)
        assert len(attention_mask) == max_seq_length, "Error with input length {} vs {}".format(len(attention_mask),
                                                                                                max_seq_length)

        feature = cls(input_ids=input_ids, label_id=label_id,
                      attention_mask=attention_mask, target_mask=target_ids)
        return feature


class TextRationaleFeatures(Features):

    def __init__(self, doc_ids, label_id, sentence_targets):
        self.doc_ids = doc_ids
        self.label_id = label_id
        self.sentence_targets = sentence_targets

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        max_sentence_length = conversion_args['max_seq_length']
        max_doc_length = conversion_args['max_doc_length']
        num_labels = conversion_args['num_labels']

        mappings = dict()
        mappings['doc_ids'] = tf.io.FixedLenFeature([max_sentence_length * max_doc_length], tf.int64)

        if has_labels:
            mappings['label_id'] = tf.io.FixedLenFeature([num_labels], tf.int64)
            mappings['sentence_targets'] = tf.io.FixedLenFeature([num_labels * max_doc_length], tf.int64)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = OrderedDict()
        features['doc_ids'] = create_int_feature(feature.doc_ids)

        if feature.label_id is not None:
            features['label_id'] = create_int_feature(feature.label_id)
            features['sentence_targets'] = create_int_feature(feature.sentence_targets)

        return features

    @classmethod
    def get_dataset_selector(cls):

        def _selector(record):
            x = {
                'doc_ids': record['doc_ids'],
                'sentence_targets': record['sentence_targets']
            }

            if 'label_id' in record:
                y = record['label_id']
                return x, y
            else:
                return x

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):

        label_id, label_map = cls._convert_labels(example_label=example.label,
                                                  label_list=label_list,
                                                  has_labels=has_labels,
                                                  converter_args=converter_args)

        doc_ids = list(map(lambda item: tokenizer.convert_tokens_to_ids(tokenizer.tokenize(item)), example.doc))
        sentence_targets = list(map(lambda item: label_map[item] if item in label_map else None,
                                    example.sentence_targets))

        return doc_ids, sentence_targets, label_id, label_map

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, TextRationaleExample):
            raise AttributeError("Expected TextRationaleExample instance, got: {}".format(type(example)))

        max_sentence_length = conversion_args['max_seq_length']
        max_doc_length = conversion_args['max_doc_length']
        num_labels = conversion_args['num_labels']

        doc_ids, sentence_targets, label_id, label_map = cls.convert_example(example=example, label_list=label_list,
                                                                             tokenizer=tokenizer, has_labels=has_labels,
                                                                             converter_args=converter_args)

        # Padding
        doc_ids = [item + [0] * (max_sentence_length - len(item)) for item in doc_ids]
        doc_ids = [item[:max_sentence_length] for item in doc_ids]

        if len(doc_ids) < max_doc_length:
            difference = max_doc_length - len(doc_ids)
            doc_ids += [[0] * max_sentence_length for _ in range(difference)]
        else:
            doc_ids = doc_ids[:max_doc_length]

        assert len(doc_ids) == max_doc_length
        for item in doc_ids:
            assert len(item) == max_sentence_length

        # Flatten KB for TFRecord saving
        doc_ids = [item for seq in doc_ids for item in seq]
        sentence_targets = [item for seq in sentence_targets for item in seq]

        assert len(sentence_targets) == max_doc_length * num_labels

        feature = TextRationaleFeatures(doc_ids=doc_ids, label_id=label_id, sentence_targets=sentence_targets)
        return feature


class SquadTextFeatures(Features):

    def __init__(self, context_ids, question_ids, answer_starts, answer_ends, answer_texts):
        self.context_ids = context_ids
        self.question_ids = question_ids
        self.answer_starts = answer_starts
        self.answer_ends = answer_ends
        self.answer_texts = answer_texts

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        max_context_length = conversion_args['max_context_length']
        max_seq_length = conversion_args['max_seq_length']

        mappings = dict()
        mappings['context_ids'] = tf.io.FixedLenFeature([max_context_length], tf.int64)
        mappings['question_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)
        mappings['answer_starts'] = tf.io.FixedLenFeature([], tf.string)
        mappings['answer_ends'] = tf.io.FixedLenFeature([], tf.string)
        mappings['answer_texts'] = tf.io.FixedLenFeature([], tf.string)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = OrderedDict()
        features['context_ids'] = create_int_feature(feature.context_ids)
        features['question_ids'] = create_int_feature(feature.question_ids)
        features['answer_starts'] = create_string_feature(feature.answer_starts.encode('utf-8'))
        features['answer_ends'] = create_string_feature(feature.answer_ends.encode('utf-8'))
        features['answer_texts'] = create_string_feature(feature.answer_texts.encode('utf-8'))

        return features

    @classmethod
    def get_dataset_selector(cls):
        def _selector(record):
            x = {
                'context_ids': record['context_ids'],
                'question_ids': record['question_ids'],
            }
            y = {
                'answer_starts': record['answer_starts'],
                'answer_ends': record['answer_ends'],
                'answer_texts': record['answer_texts']
            }
            return x, y

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):
        # Context
        # We add NULL token to account for unanswerable questions
        context_token_ids = tokenizer.convert_tokens_to_ids([tokenizer.null_token] + example.context_tokens)

        # Question
        question_token_ids = tokenizer.convert_tokens_to_ids(example.question_tokens)

        return context_token_ids, question_token_ids, example.answer_starts, example.answer_ends, example.answer_texts

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):
        if not isinstance(example, TextSquadExample):
            raise AttributeError('Expected TextSquadExample instance, got: {}'.format(type(example)))

        max_context_length = conversion_args['max_context_length']
        max_seq_length = conversion_args['max_seq_length']

        context_token_ids, \
        question_token_ids, \
        answer_starts, answer_ends, answer_texts = SquadTextFeatures.convert_example(example=example,
                                                                                     label_list=label_list,
                                                                                     tokenizer=tokenizer,
                                                                                     has_labels=has_labels,
                                                                                     converter_args=converter_args,
                                                                                     conversion_args=conversion_args,
                                                                                     wrapper_state=wrapper_state)

        # Padding
        question_token_ids += [0] * (max_seq_length - len(question_token_ids))
        question_token_ids = question_token_ids[:max_seq_length]

        padding_context = max_context_length - len(context_token_ids)

        answer_starts_int = [int(item) for item in answer_starts.split('<DIV>')]
        answer_ends_int = [int(item) for item in answer_ends.split('<DIV>')]

        # Just the first should be fine
        for answer_start, answer_end in zip(answer_starts_int[:1], answer_ends_int[:1]):
            assert answer_start < len(context_token_ids)
            assert answer_end < len(context_token_ids)

        context_token_ids += [0] * padding_context
        context_token_ids = context_token_ids[:max_context_length]

        if padding_context <= 0:
            answer_starts_int[0] = 0 if answer_starts_int[0] >= max_context_length - 1 else answer_starts_int[0]
            answer_ends_int[0] = 0 if answer_ends_int[0] >= max_context_length - 1 else answer_ends_int[0]

            if answer_ends_int[0] < answer_starts_int[0]:
                answer_starts_int[0] = answer_ends_int[0]

            # make impossible answer if start and end are equal to 0
            if answer_starts_int[0] == 0 and answer_ends_int[0] == 0:
                if len(answer_starts_int) > 1:
                    answer_texts = tokenizer.null_token + '<DIV>' + '<DIV>'.join(answer_texts.split('<DIV>')[1:])
                    answer_texts = answer_texts[:-len('<DIV>')]
                else:
                    answer_texts = tokenizer.null_token

        assert len(question_token_ids) == max_seq_length
        assert len(context_token_ids) == max_context_length
        assert answer_starts_int[0] < max_context_length - 1
        assert answer_ends_int[0] < max_context_length - 1
        assert answer_ends_int[0] >= answer_starts_int[0]
        assert len(answer_starts)
        assert len(answer_ends)
        assert len(answer_texts)

        assert 0 not in context_token_ids[answer_starts_int[0]:answer_ends_int[0] + 1]

        answer_starts = '<DIV>'.join([str(item) for item in answer_starts_int]) if len(answer_starts_int) > 1 else str(
            answer_starts_int[0])
        answer_ends = '<DIV>'.join([str(item) for item in answer_ends_int]) if len(answer_ends_int) > 1 else str(
            answer_ends_int[0])

        assert not answer_starts.startswith('<DIV>')
        assert not answer_ends.startswith('<DIV>')

        feature = cls(context_ids=context_token_ids,
                      question_ids=question_token_ids,
                      answer_starts=answer_starts,
                      answer_ends=answer_ends,
                      answer_texts=answer_texts)
        return feature


class BertSquadTextFeatures(Features):

    def __init__(self, context_ids, context_attention_mask, question_ids, question_attention_mask,
                 answer_starts, answer_ends, answer_texts):
        self.context_ids = context_ids
        self.context_attention_mask = context_attention_mask
        self.question_ids = question_ids
        self.question_attention_mask = question_attention_mask
        self.answer_starts = answer_starts
        self.answer_ends = answer_ends
        self.answer_texts = answer_texts

    @classmethod
    def get_mappings(cls, conversion_args, converter_args=None, has_labels=True):
        max_context_length = conversion_args['max_context_length']
        max_seq_length = conversion_args['max_seq_length']

        mappings = dict()
        mappings['context_ids'] = tf.io.FixedLenFeature([max_context_length], tf.int64)
        mappings['context_attention_mask'] = tf.io.FixedLenFeature([max_context_length], tf.int64)
        mappings['question_ids'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)
        mappings['question_attention_mask'] = tf.io.FixedLenFeature([max_seq_length], tf.int64)
        mappings['answer_starts'] = tf.io.FixedLenFeature([], tf.string)
        mappings['answer_ends'] = tf.io.FixedLenFeature([], tf.string)
        mappings['answer_texts'] = tf.io.FixedLenFeature([], tf.string)

        return mappings

    @classmethod
    def get_feature_records(cls, feature, converter_args=None):
        features = OrderedDict()
        features['context_ids'] = create_int_feature(feature.context_ids)
        features['context_attention_mask'] = create_int_feature(feature.context_attention_mask)
        features['question_ids'] = create_int_feature(feature.question_ids)
        features['question_attention_mask'] = create_int_feature(feature.question_attention_mask)
        features['answer_starts'] = create_string_feature(feature.answer_starts.encode('utf-8'))
        features['answer_ends'] = create_string_feature(feature.answer_ends.encode('utf-8'))
        features['answer_texts'] = create_string_feature(feature.answer_texts.encode('utf-8'))

        return features

    @classmethod
    def get_dataset_selector(cls):
        def _selector(record):
            x = {
                'context_ids': record['context_ids'],
                'context_attention_mask': record['context_attention_mask'],
                'question_ids': record['question_ids'],
                'question_attention_mask': record['question_attention_mask']
            }
            y = {
                'answer_starts': record['answer_starts'],
                'answer_ends': record['answer_ends'],
                'answer_texts': record['answer_texts']
            }
            return x, y

        return _selector

    @classmethod
    def convert_example(cls, example, label_list, tokenizer, has_labels=True, converter_args=None, conversion_args=None,
                        wrapper_state={}):
        return example.context_ids, example.context_attention_mask, \
               example.question_ids, example.question_attention_mask, \
               example.answer_starts, example.answer_ends, example.answer_texts

    @classmethod
    def from_example(cls, example, label_list, tokenizer, conversion_args, has_labels=True, converter_args=None,
                     wrapper_state={}):

        if not isinstance(example, BertTextSquadExample):
            raise AttributeError('Expected BertTextSquadExample instance, got: {}'.format(type(example)))

        max_context_length = conversion_args['max_context_length']
        max_seq_length = conversion_args['max_seq_length']

        context_ids, context_attention_mask, \
        question_ids, question_attention_mask, \
        answer_starts, answer_ends, answer_texts = cls.convert_example(example=example,
                                                                       label_list=label_list,
                                                                       tokenizer=tokenizer,
                                                                       has_labels=has_labels,
                                                                       converter_args=converter_args,
                                                                       conversion_args=conversion_args,
                                                                       wrapper_state=wrapper_state)

        # Padding

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        # attention_mask = [1 if converter_args['mask_padding_with_zero'] else 0] * len(input_ids)

        # Context
        # Zero-pad up to the sequence length.
        context_padding_length = max_context_length - len(context_ids)

        answer_starts_int = [int(item) for item in answer_starts.split('<DIV>')]
        answer_ends_int = [int(item) for item in answer_ends.split('<DIV>')]

        if converter_args['pad_on_left']:
            context_ids = ([tokenizer.pad_token_id] * context_padding_length) + context_ids
            context_attention_mask = ([0 if converter_args[
                'mask_padding_with_zero'] else 1] * context_padding_length) + context_attention_mask
        else:
            context_ids = context_ids + ([tokenizer.pad_token_id] * context_padding_length)
            context_attention_mask = context_attention_mask + (
                    [0 if converter_args['mask_padding_with_zero'] else 1] * context_padding_length)

        if context_padding_length <= 0:
            answer_starts_int[0] = 0 if answer_starts_int[0] >= max_context_length - 1 else answer_starts_int[0]
            answer_ends_int[0] = 0 if answer_ends_int[0] >= max_context_length - 1 else answer_ends_int[0]

            if answer_ends_int[0] < answer_starts_int[0]:
                answer_starts_int[0] = answer_ends_int[0]

            # make impossible answer if start and end are equal to 0
            if answer_starts_int[0] == 0 and answer_ends_int[0] == 0:
                if len(answer_starts_int) > 1:
                    answer_texts = tokenizer.cls_token + '<DIV>' + '<DIV>'.join(answer_texts.split('<DIV>')[1:])
                    answer_texts = answer_texts[:-len('<DIV>')]
                else:
                    answer_texts = tokenizer.cls_token

        # Just the first should be fine
        for answer_start, answer_end in zip(answer_starts_int[:1], answer_ends_int[:1]):
            assert answer_start < len(context_ids)
            assert answer_end < len(context_ids)

        # Question
        # Zero-pad up to the sequence length.
        question_padding_length = max_seq_length - len(question_ids)
        if converter_args['pad_on_left']:
            question_ids = ([tokenizer.pad_token_id] * question_padding_length) + question_ids
            question_attention_mask = ([0 if converter_args[
                'mask_padding_with_zero'] else 1] * question_padding_length) + question_attention_mask
        else:
            question_ids = question_ids + ([tokenizer.pad_token_id] * question_padding_length)
            question_attention_mask = question_attention_mask + (
                    [0 if converter_args['mask_padding_with_zero'] else 1] * question_padding_length)

        # Truncation (make sure sequences end with [SEP] token)
        if len(context_ids) > max_context_length:
            context_ids = context_ids[:max_context_length - 1] + [tokenizer.sep_token_id]
        else:
            context_ids = context_ids[:max_context_length]

        context_attention_mask = context_attention_mask[:max_context_length]

        if len(question_ids) > max_seq_length:
            question_ids = question_ids[:max_seq_length - 1] + [tokenizer.sep_token_id]
        else:
            question_ids = question_ids[:max_seq_length]

        question_attention_mask = question_attention_mask[:max_seq_length]

        assert len(context_ids) == max_context_length
        assert len(context_attention_mask) == max_context_length
        assert len(question_ids) == max_seq_length
        assert len(question_attention_mask) == max_seq_length
        assert answer_starts_int[0] < max_context_length - 1
        assert answer_ends_int[0] < max_context_length - 1
        assert answer_ends_int[0] >= answer_starts_int[0]

        assert tokenizer.pad_token_id not in context_ids[answer_starts_int[0]:answer_ends_int[0] + 1]

        answer_starts = '<DIV>'.join([str(item) for item in answer_starts_int]) if len(answer_starts_int) > 1 else str(
            answer_starts_int[0])
        answer_ends = '<DIV>'.join([str(item) for item in answer_ends_int]) if len(answer_ends_int) > 1 else str(
            answer_ends_int[0])

        feature = cls(context_ids=context_ids,
                      context_attention_mask=context_attention_mask,
                      question_ids=question_ids,
                      question_attention_mask=question_attention_mask,
                      answer_starts=answer_starts,
                      answer_ends=answer_ends,
                      answer_texts=answer_texts)
        return feature


class FeatureFactory(object):
    supported_features = {
        'text_features': TextFeatures,
        'pairwise_text_features': PairwiseTextFeatures,
        'text_kb_features': TextKBFeatures,
        'text_kb_supervision_features': TextKBSupervisionFeatures,

        'bert_text_features': BertTextFeatures,
        'bert_text_kb_features': BertTextKBFeatures,
        'bert_text_kb_supervision_features': BertTextKBSupervisionFeatures,

        'rationale_cnn_text_features': TextRationaleFeatures,

        'squad_text_features': SquadTextFeatures,
        'bert_squad_text_features': BertSquadTextFeatures
    }
