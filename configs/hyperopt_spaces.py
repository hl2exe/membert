"""

Hyperopt spaces for calibration

"""

from hyperopt import hp

spaces = {
    # "basic_memn2n_v1": {
        # "optimizer_args": hp.choice("optimizer_args", [{
        #     "learning_rate": 1e-05
        # }, {
        #     "learning_rate": 1e-04
        # }, {
        #     "learning_rate": 1e-03
        # }, {"learning_rate": 2e-04}, {
        #     "learning_rate": 2e-05
        # }]),
        # "hops": hp.choice("hops", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        # "add_gradient_noise": hp.choice('add_gradient_noise', [False, True]),
        # "clip_gradient": hp.choice('clip_gradient', [False, True]),
        # "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 300]),
        # "l2_regularization": hp.choice("l2_regularization", [None, 1e-05, 1e-04, 2e-04, 2e-05]),
        # "answer_weights": hp.choice("answer_weights", [
        #     [(hp.randint('answer_layer1_1', 480) + 32)],
        #     [(hp.randint("answer_layer1_2", 480) + 32),
        #      (hp.randint("answer_layer2_2", 480) + 32)],
        #     [(hp.randint("answer_layer1_3", 480) + 32),
        #      (hp.randint("answer_layer2_3", 480) + 32),
        #      (hp.randint("answer_layer3_3", 480) + 32)]]),
        # "dropout_rate": hp.uniform("dropout_rate", 0.1, 0.5),
        # "embedding_info": hp.choice("embedding_info", [
        #     {'mode': 'sum'},
        #     {'mode': 'mean'},
        #     {'mode': 'generalized_pooling',
        #      'attention_dimension': hp.randint('attention_dimension', 480) + 32},
        #     {'mode': 'recurrent',
        #      'hidden_size': hp.randint('hidden_size', 480) + 32,
        #      'bidirectional': hp.choice('bidirectional', [False, True])}
        # ]),
        # "memory_lookup_info": hp.choice('memory_lookup_info', [
        #     {'mode': 'dot_product'},
        #     {'mode': 'scaled_dot_product'},
        #     {'mode': 'mlp',
        #      'weights': hp.choice('weights', [
        #          [(hp.randint('mlp_layer1_1', 480) + 32)],
        #          [(hp.randint("mlp_layer1_2", 480) + 32),
        #           (hp.randint("mlp_layer2_2", 480) + 32)],
        #          [(hp.randint("mlp_layer1_3", 480) + 32),
        #           (hp.randint("mlp_layer2_3", 480) + 32),
        #           (hp.randint("mlp_layer3_3", 480) + 32)]])}
        # ]),
        # "extraction_info": hp.choice('extraction_info', [
        #     {'mode': 'sparsemax'},
        #     {'mode': 'softmax'},
        #     {'mode': 'sigmoid'}
        # ]),
        # "reasoning_info": hp.choice('reasoning_info', [
        #     {'mode': 'sum'},
        #     {'mode': 'concat'},
        #     {'mode': 'rnn'},
        #     {'mode': 'mlp'}
        # ]),
    #     "partial_supervision_info": hp.choice('partial_supervision_info', [
    #         {'flag': True,
    #          'coefficient': hp.choice('coefficient', [1e-3, 1e-2, 1e-1, 2e-1, 5e-1, 1., 2., 3., 10]),
    #          'margin': hp.choice('margin', [0.3, 0.4, 0.5, 0.6, 0.7, 0.8])}
    #     ])
    # },
    # "basic_memn2n_v2": {
    #     # "optimizer_args": hp.choice("optimizer_args", [{
    #     #     "learning_rate": 1e-05
    #     # }, {
    #     #     "learning_rate": 1e-04
    #     # }, {
    #     #     "learning_rate": 1e-03
    #     # }, {"learning_rate": 2e-04}, {
    #     #     "learning_rate": 2e-05
    #     # }]),
    #     # "hops": hp.choice("hops", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    #     # "add_gradient_noise": hp.choice('add_gradient_noise', [False, True]),
    #     # "clip_gradient": hp.choice('clip_gradient', [False, True]),
    #     # "embedding_dimension": hp.choice("embedding_dimension", [32, 64, 128, 300]),
    #     # "l2_regularization": hp.choice("l2_regularization", [None, 1e-05, 1e-04, 2e-04, 2e-05]),
    #     # "answer_weights": hp.choice("answer_weights", [
    #     #     [(hp.randint('answer_layer1_1', 480) + 32)],
    #     #     [(hp.randint("answer_layer1_2", 480) + 32),
    #     #      (hp.randint("answer_layer2_2", 480) + 32)],
    #     #     [(hp.randint("answer_layer1_3", 480) + 32),
    #     #      (hp.randint("answer_layer2_3", 480) + 32),
    #     #      (hp.randint("answer_layer3_3", 480) + 32)]]),
    #     # "dropout_rate": hp.uniform("dropout_rate", 0.1, 0.5),
    #     # "embedding_info": hp.choice("embedding_info", [
    #     #     {'mode': 'sum'},
    #     #     {'mode': 'mean'},
    #     #     {'mode': 'generalized_pooling',
    #     #      'attention_dimension': hp.randint('attention_dimension', 480) + 32},
    #     #     {'mode': 'recurrent',
    #     #      'hidden_size': hp.randint('hidden_size', 480) + 32,
    #     #      'bidirectional': hp.choice('bidirectional', [False, True])}
    #     # ]),
    #     # "memory_lookup_info": hp.choice('memory_lookup_info', [
    #     #     {'mode': 'dot_product'},
    #     #     {'mode': 'scaled_dot_product'},
    #     #     {'mode': 'mlp',
    #     #      'weights': hp.choice('weights', [
    #     #          [(hp.randint('mlp_layer1_1', 480) + 32)],
    #     #          [(hp.randint("mlp_layer1_2", 480) + 32),
    #     #           (hp.randint("mlp_layer2_2", 480) + 32)],
    #     #          [(hp.randint("mlp_layer1_3", 480) + 32),
    #     #           (hp.randint("mlp_layer2_3", 480) + 32),
    #     #           (hp.randint("mlp_layer3_3", 480) + 32)]])}
    #     # ]),
    #     # "extraction_info": hp.choice('extraction_info', [
    #     #     {'mode': 'sparsemax'},
    #     #     {'mode': 'softmax'},
    #     #     {'mode': 'sigmoid'}
    #     # ]),
    #     # "reasoning_info": hp.choice('reasoning_info', [
    #     #     {'mode': 'sum'},
    #     #     {'mode': 'concat'},
    #     #     {'mode': 'rnn'},
    #     #     {'mode': 'mlp'}
    #     # ]),
    #     "partial_supervision_info": hp.choice('partial_supervision_info', [
    #         {'flag': True,
    #          'coefficient': hp.choice('coefficient', [1e-4, 1e-3, 1e-2, 1e-1, 2e-1, 5e-1, 1., 2., 3, 10]),
    #          'margin': hp.choice('margin', [0.3, 0.4, 0.5, 0.6, 0.7, 0.8])}
    #     ])
    # },
    # "gated_memn2n_v1": {
    #     "optimizer_args": hp.choice("optimizer_args", [{
    #         "learning_rate": 1e-05
    #     }, {
    #         "learning_rate": 1e-04
    #     }, {
    #         "learning_rate": 1e-03
    #     }, {"learning_rate": 2e-04}, {
    #         "learning_rate": 2e-05
    #     }]),
    #     "hops": hp.choice("hops", [1]),
    #     "embedding_dimension": hp.choice("embedding_dimension", [128, 300, 512]),
    #     "l2_regularization": hp.choice("l2_regularization", [None, 1e-05, 1e-04, 2e-04, 2e-05, 1e-03]),
    #     "answer_weights": hp.choice("answer_weights", [
    #         [(hp.randint('answer_layer1_1', 992) + 32)],
    #         [(hp.randint("answer_layer1_2", 992) + 32),
    #          (hp.randint("answer_layer2_2", 992) + 32)],
    #         [(hp.randint("answer_layer1_3", 992) + 32),
    #          (hp.randint("answer_layer2_3", 992) + 32),
    #          (hp.randint("answer_layer3_3", 992) + 32)]]),
    #     "dropout_rate": hp.uniform("dropout_rate", 0.1, 0.5),
    #     "embedding_info": hp.choice("embedding_info", [
    #         {'mode': 'sum'},
    #         # {'mode': 'mean'},
    #         # {'mode': 'generalized_pooling',
    #         #  'attention_dimension': hp.randint('attention_dimension', 992) + 32},
    #         # {'mode': 'recurrent',
    #         #  'hidden_size': hp.randint('hidden_size', 992) + 32,
    #         #  'bidirectional': hp.choice('bidirectional', [False, True])}
    #     ]),
    #     # "memory_lookup_info": hp.choice('memory_lookup_info', [
    #     #     {'mode': 'dot_product'},
    #     #     {'mode': 'scaled_dot_product'},
    #     #     {'mode': 'mlp',
    #     #      'weights': hp.choice('weights', [
    #     #          [(hp.randint('mlp_layer1_1', 992) + 32)],
    #     #          [(hp.randint("mlp_layer1_2", 992) + 32),
    #     #           (hp.randint("mlp_layer2_2", 992) + 32)],
    #     #          [(hp.randint("mlp_layer1_3", 992) + 32),
    #     #           (hp.randint("mlp_layer2_3", 992) + 32),
    #     #           (hp.randint("mlp_layer3_3", 992) + 32)]])}
    #     # ]),
    #     # "extraction_info": hp.choice('extraction_info', [
    #     #     {'mode': 'softmax'},
    #     #     {'mode': 'sigmoid'}
    #     # ]),
    #     # "reasoning_info": hp.choice('reasoning_info', [
    #     #     {'mode': 'sum'},
    #     #     {'mode': 'concat'},
    #     #     {'mode': 'rnn'},
    #     #     {'mode': 'mlp'}
    #     # ]),
    #     "partial_supervision_info": hp.choice('partial_supervision_info', [
    #         {'flag': True,
    #          'coefficient': hp.choice('coefficient', [1e-4, 1e-3, 1e-2, 1e-1, 2e-1, 5e-1, 1., 2., 10]),
    #          'margin': hp.uniform('margin', 0.1, 0.8)}
    #     ]),
    #     "gating_info": hp.choice('gating_info', [
    #         {'mode': 'mlp_gating',
    #          'gating_weights': hp.choice('gating_weights', [
    #              [(hp.randint('gating_mlp_layer1_1', 992) + 32)],
    #              [(hp.randint("gating_mlp_layer1_2", 992) + 32),
    #               (hp.randint("gating_mlp_layer2_2", 992) + 32)],
    #              [(hp.randint("gating_mlp_layer1_3", 992) + 32),
    #               (hp.randint("gating_mlp_layer2_3", 992) + 32),
    #               (hp.randint("gating_mlp_layer3_3", 992) + 32)]])}
    #     ])
    # },
    # "discriminative_memn2n_v2": {
    #     "optimizer_args": hp.choice("optimizer_args", [{
    #         "learning_rate": 1e-05
    #     }, {
    #         "learning_rate": 1e-04
    #     }, {
    #         "learning_rate": 1e-03
    #     }, {"learning_rate": 2e-04}, {
    #         "learning_rate": 2e-05
    #     }]),
    #     "embedding_dimension": hp.choice("embedding_dimension", [128, 300, 512]),
    #     "l2_regularization": hp.choice("l2_regularization", [None, 1e-05, 1e-04, 2e-04, 2e-05, 1e-03]),
    #     "answer_weights": hp.choice("answer_weights", [
    #         [hp.choice('answer_layer1_1', [128, 256, 512, 1024])],
    #         [hp.choice("answer_layer1_2", [128, 256, 512, 1024]),
    #          hp.choice("answer_layer2_2", [128, 256, 512, 1024])],
    #         [hp.choice("answer_layer1_3", [128, 256, 512, 1024]),
    #          hp.choice("answer_layer2_3", [128, 256, 512, 1024]),
    #          hp.choice("answer_layer3_3", [128, 256, 512, 1024])]]),
    #     "dropout_rate": hp.choice("dropout_rate", [0.1, 0.2, 0.3, 0.4, 0.5]),
    #     "embedding_info": hp.choice("embedding_info", [
    #         {'mode': 'sum'},
    #         {'mode': 'mean'},
    #     ]),
    #     "memory_lookup_info": hp.choice('memory_lookup_info', [
    #         {'mode': 'dot_product'},
    #         {'mode': "mlp",
    #          'weights': hp.choice('weights', [
    #              [hp.choice('mlp_layer1_1', [128, 256, 512, 1024])],
    #              [hp.choice("mlp_layer1_2", [128, 256, 512, 1024]),
    #               hp.choice("mlp_layer2_2", [128, 256, 512, 1024])],
    #              [hp.choice("mlp_layer1_3", [128, 256, 512, 1024]),
    #               hp.choice("mlp_layer2_3", [128, 256, 512, 1024]),
    #               hp.choice("mlp_layer3_3", [128, 256, 512, 1024])]])}
    #     ]),
    #     "extraction_info": hp.choice('extraction_info', [
    #         {'mode': 'softmax'}
    #     ]),
    #     "reasoning_info": hp.choice('reasoning_info', [
    #         {'mode': 'sum'},
    #         {'mode': 'concat'}
    #     ]),
    #     "partial_supervision_info": hp.choice('partial_supervision_info', [
    #         {'flag': True,
    #          'coefficient': hp.choice('coefficient', [1e-4, 1e-3, 1e-2, 1e-1, 2e-1, 5e-1, 1., 2., 10]),
    #          'margin': hp.choice('margin', [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8])}
    #     ]),
    #     "discriminator_weights": hp.choice("discriminator_weights", [
    #         [hp.choice('discriminator_layer1_1', [128, 256, 512, 1024])],
    #         [hp.choice("discriminator_layer1_2", [128, 256, 512, 1024]),
    #          hp.choice("discriminator_layer2_2", [128, 256, 512, 1024])],
    #         [hp.choice("discriminator_layer1_3", [128, 256, 512, 1024]),
    #          hp.choice("discriminator_layer2_3", [128, 256, 512, 1024]),
    #          hp.choice("discriminator_layer3_3", [128, 256, 512, 1024])]]),
    #     "confidence_coefficient": hp.choice('confidence_coefficient', [1e-2, 0.1, 0.5, 1, 1.5])
    # },
    "experimental_gated_memn2n_v2": {
        "partial_supervision_info": hp.choice('partial_supervision_info', [
            {'flag': True,
             'mask_by_attention': False,
             'coefficient': hp.choice('coefficient', [1e-1, 2e-1, 5e-1]),
             'margin': hp.choice('margin', [0.2, 0.3, 0.4, 0.49, 0.51, 0.6, 0.7, 0.8])}
        ])
    }
}
