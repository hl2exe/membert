"""

"""

import pandas as pd
import numpy as np
from tqdm import tqdm

original_excel = pd.ExcelFile('LTD_original.xlsx')

original_samples = original_excel.parse('ltd')

original_fl = original_samples['Explanation id_FL'].values.reshape(-1, 1)
original_kd = original_samples['Explanation id_KD'].values.reshape(-1, 1)
original_concat = np.concatenate((original_fl, original_kd), axis=1)
original_concat = np.array([','.join([item for item in seq if type(item) is not float]) for seq in original_concat])

original_data = [(doc, clause, targets) for doc, clause, targets in zip(original_samples.Document.values, original_samples.Clause.values, original_concat)]

current_samples = pd.read_csv('LTD_samples.csv')

current_data = [(idx, doc, clause, targets) for idx, doc, clause, targets in zip(current_samples.index.values, current_samples.Document.values, current_samples.Clause.values, current_samples.Targets.values)]

print(len(current_data))
print(current_samples.shape)

not_found = []

# Very Inefficient but quick code
for curr_idx, curr_doc, curr_clause, curr_targets in tqdm(current_data):
	found = False
	for orig_doc, orig_clause, orig_targets in original_data:
		if (curr_doc, curr_clause) == (orig_doc, orig_clause):
			found = True
			diff = set(orig_targets) - set(curr_targets)
			if diff:
				print('*' * 50)
				print('Mismatch at index: ', curr_idx)
				print('Original: ', orig_targets)
				print('Current: ', curr_targets)
				action = input('Substitute? Y/N ')
				if action.lower() == 'y':
					print('Substitution done!')
					current_data[curr_idx] = (curr_idx, curr_doc, curr_clause, orig_targets)
				print('*' * 50)
			break

	if not found:
		print("Can't find clause: ", curr_clause)
		print('This should not happen! Or maybe yes?')
		not_found.append((curr_doc, curr_clause, curr_targets))

print('Remaining clauses (', len(not_found), '): ', not_found)

current_samples.Targets = [targets for _,_,_,targets in current_data]
current_samples.to_csv('./LTD_samples.csv')

print('Done!')
