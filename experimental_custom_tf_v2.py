"""

@Author: Federico Ruggeri

@Date: 24/07/19

Core part of Tensorflow 2 models. Each network defined in nn_models_v2.py has a corresponding model here.

TODO: fix DLU model.

"""

from collections import OrderedDict
from functools import reduce

import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_probability as tfp
from tensorflow.python.ops import rnn_cell_impl
from transformers import TFDistilBertModel, TFRobertaModel, TFBertModel

from utility.tensorflow_utils_v2 import positional_encoding, get_initializer, masked_softmax

_zero_state_tensors = rnn_cell_impl._zero_state_tensors  # pylint: disable=protected-access


######################################################
####################### MODELS #######################
######################################################

class M_Basic_Memn2n(tf.keras.Model):
    """
    Basic Memory Network implementation. The model is comprised of three steps:
        1. Memory Lookup: a similarity operation is computed between query and memory cells.
         A content vector is extracted.

        2. Memory Reasoning: extracted content vector is used along with the query to build a new query.

        3. Lookup and Reasoning can be iterated multiple times.

        4. Eventually, an answer module takes care of formulating a prediction.
    """

    def __init__(self, query_size, sentence_size, embedding_info, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 vocab_size, embedding_dimension=32, hops=1,
                 answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None,
                 accumulate_attention=False, position_encoding=False,
                 num_labels=1, num_classes_per_label=None, kb=None,
                 sampler=None, **kwargs):

        super(M_Basic_Memn2n, self).__init__(**kwargs)
        self.sentence_size = sentence_size
        self.query_size = query_size
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = position_encoding
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.reasoning_info = reasoning_info

        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.sentence_embedder = MemorySentenceEmbedder(embedding_info=embedding_info,
                                                        embedding_dimension=embedding_dimension)

        # Can't share parameters if concat mode is enabled with mlp similarity
        if reasoning_info['mode'] == 'concat' and memory_lookup_info['mode'] == 'mlp':
            self.memory_lookup_blocks = [
                MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                     dropout_rate=dropout_rate,
                                     l2_regularization=l2_regularization,
                                     reasoning_info=reasoning_info,
                                     samplewise_sampling=False if sampler is None
                                     else sampler.samplewise_sampling)
            ]
        else:
            lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                dropout_rate=dropout_rate,
                                                l2_regularization=l2_regularization,
                                                reasoning_info=reasoning_info,
                                                samplewise_sampling=False if sampler is None
                                                else sampler.samplewise_sampling)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]

        extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                    partial_supervision_info=partial_supervision_info)
        self.extraction_blocks = [extraction_block for _ in range(hops)]

        reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)
        self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization))
                                for key, value in self.num_classes_per_label.items()}

        # KB
        if kb is not None:
            # Pad KB
            padded_kb = OrderedDict()
            for category, cat_data in kb.items():
                for key, value in cat_data.items():
                    if key != 'kb_mask':
                        value = [item + [0] * (self.sentence_size - len(item)) for item in value]
                        value = [item[:self.sentence_size] for item in value]

                    # Reverse kb keys to facilitate concatenation
                    padded_kb.setdefault(key, OrderedDict()).setdefault(category, value)

            # Concatenate KB
            concat_kb = OrderedDict()
            for key, cat_data in padded_kb.items():
                concat_kb[key] = reduce(lambda a, b: a + b, list(cat_data.values()))

            # Size info
            self.kb_real_size = len(concat_kb[list(concat_kb.keys())[0]])

            self.kb = {
                key: tf.reshape(tf.convert_to_tensor(value), [-1, self.sentence_size])
                if key != 'kb_mask' else tf.convert_to_tensor(value) for key, value in concat_kb.items()
            }
        else:
            self.kb = None
            self.kb_real_size = None

        self.sampler = sampler
        if sampler is not None:
            assert kb is not None
            self.sampler.set_kb(self.kb, self.kb_real_size)

    def _compute_logits(self, x, training=False):
        for block in self.answer_blocks:
            x = block(x, training=training)

        # Multi-label
        if self.num_labels == 1:
            x = self.final_block(x, training=training)
        else:
            x = {'label_id_{}'.format(key): block(x, training=training) for key, block in
                 self.final_block.items()}

        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):

        # Sample KB
        if self.sampler is not None:
            inputs, sampled_kb = self.sampler.sampling(inputs, additional_info=additional_info)
        else:
            sampled_kb = self.kb

        # [batch_size, query_size]
        query = inputs['text']

        # KB data
        # [batch_size * memory_size, sentence_size]
        memory = sampled_kb['kb_ids']

        # [batch_size, memory_size]
        memory_mask = sampled_kb['kb_mask']

        if len(memory.shape) == 2:
            memory = tf.expand_dims(memory, 0)
            memory_mask = tf.reshape(sampled_kb['kb_mask'], [1, -1])

        query_emb = self.query_embedding(query)
        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

        memory_emb = self.sentence_embedder(memory_emb)
        query_emb = self.sentence_embedder(query_emb, axis=1)

        upd_query, upd_memory = query_emb, memory_emb

        memory_attention = []

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory},
                                        training=training)

            probabilities, hop_supervision_loss = extraction_block({
                'similarities': similarities,
                'context_mask': memory_mask,
                'target_mask': inputs['target_mask']
            },
                state=state,
                training=training)

            memory_attention.append(probabilities)

            if self.partial_supervision_info['flag']:
                supervision_losses.append(hop_supervision_loss)

            # Get accumulator vector from MANN
            memory_search = tf.reduce_sum(memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search},
                                        state=state,
                                        training=training)

        answer = self._compute_logits(upd_query, training=training)

        # Compute answer without memory
        freezed_query = tf.stop_gradient(query_emb)
        if self.reasoning_info['mode'] == 'concat':
            freezed_query = tf.concat((freezed_query, tf.zeros_like(freezed_query)), axis=-1)

        query_only_answer = self._compute_logits(freezed_query, training=False)

        model_additional_info = {'memory_attention': memory_attention,
                                 'supervision_losses': supervision_losses,
                                 'query_emb': query_emb,
                                 'query_only_answer': query_only_answer}

        if self.sampler is not None:
            sampled_additional_info = self.sampler.get_additional_info()
            for key, value in sampled_additional_info.items():
                model_additional_info[key] = value

        return {
                   'MANN': answer,
                   'Q_Only': query_only_answer,
               }, model_additional_info


class M_Mapper_Memn2n(tf.keras.Model):
    """
    Basic Memory Network implementation. The model is comprised of three steps:
        1. Memory Lookup: a similarity operation is computed between query and memory cells.
         A content vector is extracted.

        2. Memory Reasoning: extracted content vector is used along with the query to build a new query.

        3. Lookup and Reasoning can be iterated multiple times.

        4. Eventually, an answer module takes care of formulating a prediction.
    """

    def __init__(self, query_size, sentence_size, embedding_info, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 vocab_size, embedding_dimension=32, hops=1,
                 answer_weights=[], mapper_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None,
                 accumulate_attention=False, position_encoding=False,
                 num_labels=1, num_classes_per_label=None, kb=None,
                 sampler=None, **kwargs):

        super(M_Mapper_Memn2n, self).__init__(**kwargs)
        self.sentence_size = sentence_size
        self.query_size = query_size
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = position_encoding
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.reasoning_info = reasoning_info

        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.sentence_embedder = MemorySentenceEmbedder(embedding_info=embedding_info,
                                                        embedding_dimension=embedding_dimension)

        # Can't share parameters if concat mode is enabled with mlp similarity
        if reasoning_info['mode'] == 'concat' and memory_lookup_info['mode'] == 'mlp':
            self.memory_lookup_blocks = [
                MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                     dropout_rate=dropout_rate,
                                     l2_regularization=l2_regularization,
                                     reasoning_info=reasoning_info,
                                     samplewise_sampling=False if sampler is None
                                     else sampler.samplewise_sampling)
            ]
        else:
            lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                dropout_rate=dropout_rate,
                                                l2_regularization=l2_regularization,
                                                reasoning_info=reasoning_info,
                                                samplewise_sampling=False if sampler is None
                                                else sampler.samplewise_sampling)
            self.memory_lookup_blocks = [lookup_block for _ in range(hops)]

        extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                    partial_supervision_info=partial_supervision_info)
        self.extraction_blocks = [extraction_block for _ in range(hops)]

        reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)
        self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization))
                                for key, value in self.num_classes_per_label.items()}

        # KB
        if kb is not None:
            # Pad KB
            padded_kb = OrderedDict()
            for category, cat_data in kb.items():
                for key, value in cat_data.items():
                    if key != 'kb_mask':
                        value = [item + [0] * (self.sentence_size - len(item)) for item in value]
                        value = [item[:self.sentence_size] for item in value]

                    # Reverse kb keys to facilitate concatenation
                    padded_kb.setdefault(key, OrderedDict()).setdefault(category, value)

            # Concatenate KB
            concat_kb = OrderedDict()
            for key, cat_data in padded_kb.items():
                concat_kb[key] = reduce(lambda a, b: a + b, list(cat_data.values()))

            # Size info
            self.kb_real_size = len(concat_kb[list(concat_kb.keys())[0]])

            self.kb = {
                key: tf.reshape(tf.convert_to_tensor(value), [-1, self.sentence_size])
                if key != 'kb_mask' else tf.convert_to_tensor(value) for key, value in concat_kb.items()
            }
        else:
            self.kb = None
            self.kb_real_size = None

        self.sampler = sampler
        if sampler is not None:
            assert kb is not None
            self.sampler.set_kb(self.kb, self.kb_real_size)

        self.mapper_weights = mapper_weights
        self.mapper_blocks = []
        for weight in self.mapper_weights:
            self.mapper_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.selu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
        self.mapper_blocks.append(tf.keras.layers.Dense(units=embedding_dimension,
                                                        kernel_regularizer=tf.keras.regularizers.l2(
                                                            l2_regularization)))

    def _compute_logits(self, x, training=False):
        for block in self.answer_blocks:
            x = block(x, training=training)

        # Multi-label
        if self.num_labels == 1:
            x = self.final_block(x, training=training)
        else:
            x = {'label_id_{}'.format(key): block(x, training=training) for key, block in
                 self.final_block.items()}

        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):

        # Sample KB
        if self.sampler is not None:
            inputs, sampled_kb = self.sampler.sampling(inputs, additional_info=additional_info)
        else:
            sampled_kb = self.kb

        # [batch_size, query_size]
        query = inputs['text']

        # KB data
        # [batch_size * memory_size, sentence_size]
        memory = sampled_kb['kb_ids']

        # [batch_size, memory_size]
        memory_mask = sampled_kb['kb_mask']

        if len(memory.shape) == 2:
            memory = tf.expand_dims(memory, 0)
            memory_mask = tf.reshape(sampled_kb['kb_mask'], [1, -1])

        query_emb = self.query_embedding(query)
        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

        memory_emb = self.sentence_embedder(memory_emb)
        query_emb = self.sentence_embedder(query_emb, axis=1)

        upd_query, upd_memory = query_emb, memory_emb

        memory_attention = []

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory},
                                        training=training)

            probabilities, hop_supervision_loss = extraction_block({
                'similarities': similarities,
                'context_mask': memory_mask,
                'target_mask': inputs['target_mask']
            },
                state=state,
                training=training)

            memory_attention.append(probabilities)

            if self.partial_supervision_info['flag']:
                supervision_losses.append(hop_supervision_loss)

            # Get accumulator vector from MANN
            memory_search = tf.reduce_sum(memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            # TODO: replace with layer
            # Get accumulator vector from mapper
            mapper_memory_search = query_emb
            for mapper_block in self.mapper_blocks:
                mapper_memory_search = mapper_block(mapper_memory_search, training=training)

            mapper_similarities = lookup_block({'query': mapper_memory_search, 'memory': upd_memory}, training=training)
            mapper_probabilities, _ = extraction_block({
                'similarities': mapper_similarities,
                'context_mask': memory_mask,
                'target_mask': inputs['target_mask']
            },
                state=state,
                training=training)

            # Compute accumulator vector
            final_memory_search = (1 - additional_info['mapper_coefficient']) * memory_search \
                                  + additional_info['mapper_coefficient'] * mapper_memory_search

            upd_query = reasoning_block({'query': upd_query, 'memory_search': final_memory_search},
                                        state=state,
                                        training=training)

        answer = self._compute_logits(upd_query, training=training)

        # Compute answer without memory
        freezed_query = tf.stop_gradient(query_emb)
        if self.reasoning_info['mode'] == 'concat':
            freezed_query = tf.concat((freezed_query, tf.zeros_like(freezed_query)), axis=-1)

        query_only_answer = self._compute_logits(freezed_query, training=False)

        model_additional_info = {'memory_attention': memory_attention,
                                 'supervision_losses': supervision_losses,
                                 'query_emb': query_emb,
                                 'query_only_answer': query_only_answer,
                                 'upd_memory': upd_memory,
                                 'mapper_memory_search': mapper_memory_search,
                                 'target_mask': inputs['target_mask'],
                                 'probabilities': probabilities,
                                 'mapper_probabilities': mapper_probabilities}

        if self.sampler is not None:
            sampled_additional_info = self.sampler.get_additional_info()
            for key, value in sampled_additional_info.items():
                model_additional_info[key] = value

        return {
                   'MANN': answer,
                   'Q_Only': query_only_answer,
               }, model_additional_info


@DeprecationWarning
class M_Gated_Memn2n(M_Basic_Memn2n):
    """
    Simple extension of basic memn2n via the addition of a gating mechanism
    """

    def __init__(self, gating_info, **kwargs):
        super(M_Gated_Memn2n, self).__init__(**kwargs)

        if gating_info['mode'] != 'mlp_gating':
            raise RuntimeError(
                'Invalid gating mode! Got: {} -- Supported: [mlp_gating]'.format(self.gating_info['mode']))

        self.gating_blocks = []
        for weight in gating_info['gating_weights']:
            self.gating_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                kwargs['l2_regularization'])))
        self.gating_blocks.append(tf.keras.layers.Dense(units=1,
                                                        activation=tf.nn.sigmoid))

    def call(self, inputs, state='training', training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        if len(memory.shape) == 2:
            memory = tf.reshape(memory, [query.shape[0], -1, self.sentence_size])

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            memory_attention = []
        else:
            memory_attention = None

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        memory_gating = []

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory}, training=training, state=state)

            # Gating
            gating = similarities
            for gating_block in self.gating_blocks:
                gating = gating_block(gating)

            memory_gating.append(gating)

            extraction_input = {key: value for key, value in additional_inputs.items()}
            extraction_input['similarities'] = similarities
            extraction_input['context_mask'] = memory_mask
            probabilities, hop_supervision_loss = extraction_block(extraction_input, training=training, state=state)

            if self.accumulate_attention:
                memory_attention.append(probabilities)

            if self.partial_supervision_info['flag']:
                supervision_losses.append(hop_supervision_loss)

            # Weight attention by gating
            memory_search = tf.reduce_sum(
                memory_emb * tf.expand_dims(probabilities, axis=-1) * tf.expand_dims(gating, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search},
                                        training=training,
                                        state=state)

        answer = upd_query
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, {'memory_attention': memory_attention,
                        'gating': memory_gating,
                        'supervision_losses': supervision_losses}


@DeprecationWarning
class M_Res_Memn2n(M_Basic_Memn2n):
    """
    ResNet Memory network as described in the paper "Gated Memory Network"
    """

    def __init__(self, gating_info, **kwargs):
        super(M_Res_Memn2n, self).__init__(**kwargs)

        if gating_info['mode'] != 'mlp_gating':
            raise RuntimeError(
                'Invalid gating mode! Got: {} -- Supported: [mlp_gating]'.format(self.gating_info['mode']))

        self.gating_blocks = []
        for weight in gating_info['gating_weights']:
            self.gating_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                kwargs['l2_regularization'])))
        self.gating_blocks.append(tf.keras.layers.Dense(units=1,
                                                        activation=tf.nn.sigmoid))

    def call(self, inputs, state='training', training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        if len(memory.shape) == 2:
            memory = tf.reshape(memory, [query.shape[0], -1, self.sentence_size])

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            memory_attention = []
        else:
            memory_attention = None

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        memory_gating = []

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory},
                                        state=state,
                                        training=training)

            # Gating
            gating = upd_query
            for gating_block in self.gating_blocks:
                gating = gating_block(gating)

            memory_gating.append(gating)

            extraction_input = {key: value for key, value in additional_inputs.items()}
            extraction_input['similarities'] = similarities
            extraction_input['context_mask'] = memory_mask
            probabilities, hop_supervision_loss = extraction_block(extraction_input,
                                                                   state=state,
                                                                   training=training)

            if self.accumulate_attention:
                memory_attention.append(probabilities)

            if self.partial_supervision_info['flag']:
                supervision_losses.append(hop_supervision_loss)

            # Weight attention by gating
            memory_search = tf.reduce_sum(
                memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            new_query = reasoning_block({'query': upd_query, 'memory_search': memory_search},
                                        training=training,
                                        state=state)

            if self.reasoning_info['mode'] == 'concat':
                upd_query = tf.concat((upd_query, tf.zeros_like(upd_query)), axis=-1)

            upd_query = new_query * gating + (1 - gating) * upd_query

        answer = upd_query
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer, {'memory_attention': memory_attention,
                        'gating': memory_gating,
                        'supervision_losses': supervision_losses}


@DeprecationWarning
class M_Discriminative_Memn2n(M_Basic_Memn2n):

    def __init__(self, discriminator_weights=(), **kwargs):
        super(M_Discriminative_Memn2n, self).__init__(**kwargs)

        self.discriminator_blocks = []
        for weight in discriminator_weights:
            self.discriminator_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                       self.l2_regularization)))
            # self.answer_blocks.append(tf.keras.layers.BatchNormalization())
            self.discriminator_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.discriminator_blocks.append(tf.keras.layers.Dense(units=1,
                                                               kernel_regularizer=tf.keras.regularizers.l2(
                                                                   self.l2_regularization)))

        self.dist = tfp.distributions.Beta(concentration1=1, concentration0=3)

    def call(self, inputs, state='training', training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size, memory_size, sentence_size]
        memory = inputs['context']

        # [batch_size, memory_size]
        memory_mask = inputs['context_mask']

        if len(memory.shape) == 2:
            memory = tf.reshape(memory, [query.shape[0], -1, self.sentence_size])

        query_emb = self.query_embedding(query)

        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        memory_emb = tf.reshape(memory_emb, [-1, memory_emb.shape[2], memory_emb.shape[3]])
        memory_emb = self.sentence_embedder(memory_emb)
        memory_emb = tf.reshape(memory_emb, [-1, memory.shape[1], memory_emb.shape[-1]])

        query_emb = self.sentence_embedder(query_emb)

        upd_query, upd_memory = query_emb, memory_emb
        additional_inputs = {key: item for key, item in inputs.items() if key not in ['text',
                                                                                      'context',
                                                                                      'context_mask',
                                                                                      'targets']} \
            if self.partial_supervision_info['flag'] else {}

        if self.accumulate_attention:
            memory_attention = []
        else:
            memory_attention = None

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        # This is just one hop
        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory}, training=training, state=state)

            extraction_input = {key: value for key, value in additional_inputs.items()}
            extraction_input['similarities'] = similarities
            extraction_input['context_mask'] = memory_mask
            probabilities, hop_supervision_loss = extraction_block(extraction_input,
                                                                   training=training,
                                                                   state=state)

            if self.accumulate_attention:
                memory_attention.append(probabilities)

            if self.partial_supervision_info['flag']:
                supervision_losses.append(hop_supervision_loss)

            memory_search = tf.reduce_sum(memory_emb * tf.expand_dims(probabilities, axis=-1), axis=1)

            upd_query = reasoning_block({'query': upd_query, 'memory_search': memory_search},
                                        state=state,
                                        training=training)

        # Compute lookup answer
        mem_answer = upd_query
        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                mem_answer = self.answer_blocks[block_idx](mem_answer)
            else:
                if training:
                    mem_answer = self.answer_blocks[block_idx](mem_answer, training=training)

        last_hidden_mem_answer = mem_answer

        # Apply last answer block
        mem_answer = self.answer_blocks[-1](mem_answer)

        # Compute initial answer
        if self.reasoning_info['mode'] == 'concat':
            query_answer = tf.concat((query_emb, query_emb), axis=-1)
        else:
            query_answer = query_emb

        for block_idx in range(len(self.answer_blocks) - 1):
            if block_idx % 2 == 0:
                query_answer = self.answer_blocks[block_idx](query_answer)
            else:
                if training:
                    query_answer = self.answer_blocks[block_idx](query_answer, training=training)

        last_hidden_query_answer = query_answer

        # Apply last answer block
        query_answer = self.answer_blocks[-1](query_answer)

        # Discriminator
        discriminator_answer = tf.concat((last_hidden_query_answer, last_hidden_mem_answer), axis=1)
        for block in self.discriminator_blocks:
            discriminator_answer = block(discriminator_answer, training=training)

        return (query_answer, mem_answer, discriminator_answer), \
               {'memory_attention': memory_attention,
                'gating': discriminator_answer,
                'supervision_losses': supervision_losses}


@DeprecationWarning
# TODO: requires per iteration masks for efficient attention
class M_Windowed_HAN(tf.keras.Model):

    def __init__(self, query_size, sentence_size, encoding_size, attention_projection,
                 windowing_info, vocab_size, embedding_dimension=32,
                 answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, accumulate_attention=False, position_encoding=False,
                 output_size=1, **kwargs):

        super(M_Windowed_HAN, self).__init__(**kwargs)
        self.sentence_size = sentence_size
        self.query_size = query_size
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = position_encoding
        self.output_size = output_size
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=query_size,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=sentence_size,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        self.input_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=encoding_size,
                                                                                return_sequences=True))

        self.attention_block = TaskSpecificAttention(projection_size=attention_projection)

        if windowing_info is None:
            windowing_info = []

        windowing_info.insert(0, {'window_size': 1, 'window_step': 1, 'pad_data': False})
        windowing_info.append({'window_size': sentence_size, 'window_step': 1, 'pad_data': True})

        self.windowing_blocks = [Windowing(**hop_info) for hop_info in windowing_info]

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            # self.answer_blocks.append(tf.keras.layers.BatchNormalization())
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_blocks.append(tf.keras.layers.Dense(units=self.output_size,
                                                        kernel_regularizer=tf.keras.regularizers.l2(l2_regularization)))

    def call(self, inputs, state='training', training=False, **kwargs):

        # [batch_size, query_size]
        query = inputs['text']

        # [batch_size * memory_size, sentence_size]
        memory = inputs['context']

        # Embedding

        query_emb = self.query_embedding(query)
        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, [-1, memory.shape[2], memory_emb.shape[-1]])
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]
            memory_emb = tf.reshape(memory_emb, memory.shape + (memory_emb.shape[-1],))

        #   Level 1: word-level
        #   memory      ->  [batch_size * memory_size, sentence_size, dim]          Encoding
        #   query       ->  [batch_size, query_size, dim]                           Encoding

        #   attention   ->  [batch_size * memory_size, sentence_size, query_size]   Attention
        #   memory_att  ->  [batch_size * memory_size, sentence_size]               Attention
        #   query_att   ->  [batch_size * memory_size, query_size]                  Attention

        #   w_mem_att   ->  [batch_size * memory_size, frames, window_size]         Windowing
        #   w_query_att ->  [batch_size, frames, window_size]                       Windowing
        #   w_memory    ->  [batch_size * memory_size, frames, window_size, dim]    Windowing
        #   w_query     ->  [batch_size, frames, window_size, dim]                  Windowing

        #   memory      ->  [batch_size * memory_size, frames, dim]                 Reduction
        #   query       ->  [batch_size, frames, dim]                               Reduction

        #   Level N: sentence-level
        #   memory      ->  [batch_size * memory_size, 1, dim]                      Reduction
        #   query       ->  [batch_size, 1, dim]                                    Reduction
        #
        for windowing_block in self.windowing_blocks:
            # Encoding
            query_emb = self.input_encoder(query_emb)
            memory_emb = self.input_encoder(memory_emb)

            # Attention
            attention = self.attention_block({
                'value': tf.reshape(memory_emb, [query_emb.shape[0], -1, memory_emb.shape[1], memory_emb.shape[2]]),
                'query': tf.expand_dims(query_emb, axis=1)
            }, state=state, training=training)
            memory_attention = tf.nn.softmax(tf.reduce_sum(attention, axis=2), axis=1)
            query_attention = tf.nn.softmax(tf.reduce_sum(attention, axis=1), axis=1)

            # Windowing
            windowed_memory_attention = windowing_block(memory_attention, training=training, state=state)
            windowed_memory_attention = tf.expand_dims(windowed_memory_attention, axis=-1)
            windowed_query_attention = windowing_block(query_attention, training=training, state=state)
            windowed_query_attention = tf.expand_dims(windowed_query_attention, axis=-1)
            windowed_query_emb = windowing_block(windowed_query_emb, training=training, state=state)
            windowed_memory_emb = windowing_block(memory_emb, training=training, state=state)

            # Reduction
            memory_emb = tf.reduce_sum(windowed_memory_emb * windowed_memory_attention, axis=2)
            query_emb = tf.reduce_sum(windowed_query_emb * windowed_query_attention, axis=2)

        # Final encoding

        # [batch_size, memory_size, dim]
        memory_emb = tf.reshape([query_emb.shape[0], -1, memory_emb.shape[-1]])

        # [batch_size, memory_size, 1]
        attention = self.attention_block({'value': memory_emb, 'query': query_emb},
                                         state=state, training=training)

        # [batch_size, memory_size]
        memory_attention = tf.reduce_sum(attention, axis=2)

        # [batch_size, dim]
        memory_emb = tf.reduce_sum(memory_emb * memory_attention, axis=1)

        # [batch_size, dim * 2]
        answer = tf.concat([query_emb, memory_emb], axis=1)
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        return answer


class M_DistilBERT(tf.keras.Model):
    r"""
    Outputs: `Tuple` comprising various elements depending on the configuration (config) and inputs:
        **logits**: ``tf.Tensor`` of shape ``(batch_size, config.num_labels)``
            Classification (or regression if config.num_labels==1) scores (before SoftMax).
        **hidden_states**: (`optional`, returned when ``config.output_hidden_states=True``)
            list of ``tf.Tensor`` (one for the output of each layer + the output of the embeddings)
            of shape ``(batch_size, sequence_length, hidden_size)``:
            Hidden-states of the model at the output of each layer plus the initial embedding outputs.
        **attentions**: (`optional`, returned when ``config.output_attentions=True``)
            list of ``tf.Tensor`` (one for each layer) of shape ``(batch_size, num_heads, sequence_length, sequence_length)``:
            Attentions weights after the attention softmax, used to compute the weighted average in the self-attention heads.

    Examples::

        import tensorflow as tf
        from transformers import BertTokenizer, TFDistilBertForSequenceClassification

        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased')
        input_ids = tf.constant(tokenizer.encode("Hello, my dog is cute"))[None, :]  # Batch size 1
        outputs = model(input_ids)
        logits = outputs[0]
    """

    def __init__(self, bert_config, preloaded_model_name, num_labels=1, num_classes_per_label=None,
                 sentence_embedding_mode='first_token', is_bert_trainable=True,
                 **kwargs):
        super(M_DistilBERT, self).__init__(**kwargs)
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label
        self.sentence_embedding_mode = sentence_embedding_mode

        assert sentence_embedding_mode in ['first_token', 'average']

        self.bert_config = bert_config

        self.distilbert = TFDistilBertModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                            config=bert_config, name='distilbert')

        if not is_bert_trainable:
            for layer in self.distilbert.layers:
                layer.trainable = False

        self.pre_classifier = tf.keras.layers.Dense(self.bert_config.dim,
                                                    kernel_initializer=get_initializer(0.02),
                                                    activation='relu',
                                                    name="pre_classifier")

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_initializer=get_initializer(0.02),
                                                     name="classifier")
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_initializer=get_initializer(0.02),
                                                           name="{}_classifier".format(key))
                                for key, value in self.num_classes_per_label.items()}

        self.dropout = tf.keras.layers.Dropout(self.bert_config.seq_classif_dropout)

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        distilbert_output = self.distilbert(inputs, training=training, **kwargs)

        hidden_state = distilbert_output[0]  # (bs, seq_len, dim)

        if self.sentence_embedding_mode == 'first_token':
            pooled_output = hidden_state[:, 0]  # (bs, dim)
        else:
            pooled_output = tf.reduce_mean(hidden_state, axis=1)

        pooled_output = self.pre_classifier(pooled_output, training=training)  # (bs, dim)
        pooled_output = self.dropout(pooled_output, training=training)  # (bs, dim)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(pooled_output, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(pooled_output, training=training) for key, block in
                      self.final_block.items()}

        return {
                   'Q_Only': logits
               }, {
                   'query_emb': pooled_output
               }


class M_MemoryDistilBERT(M_DistilBERT):

    def __init__(self, query_size, sentence_size, memory_lookup_info, reasoning_info, extraction_info,
                 partial_supervision_info, accumulate_attention=False, kb=None,
                 sampler=None, **kwargs):
        super(M_MemoryDistilBERT, self).__init__(**kwargs)
        self.query_size = query_size
        self.sentence_size = sentence_size
        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.accumulate_attention = accumulate_attention
        self.partial_supervision_info = partial_supervision_info

        self.memory_lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                        dropout_rate=self.bert_config.seq_classif_dropout,
                                                        reasoning_info=reasoning_info,
                                                        samplewise_sampling=False if sampler is None
                                                        else sampler.samplewise_sampling)

        self.extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                         partial_supervision_info=partial_supervision_info)

        self.reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)

        if kb is not None:

            # Pad KB
            padded_kb = OrderedDict()
            for category, cat_data in kb.items():
                for key, value in cat_data.items():
                    if key != 'kb_mask':
                        value = [item + [0] * (self.sentence_size - len(item)) for item in value]
                        value = [item[:self.sentence_size] for item in value]

                    # Reverse kb keys to facilitate concatenation
                    padded_kb.setdefault(key, OrderedDict()).setdefault(category, value)

            # Concatenate KB
            concat_kb = OrderedDict()
            for key, cat_data in padded_kb.items():
                concat_kb[key] = reduce(lambda a, b: a + b, list(cat_data.values()))

            # Size info
            self.kb_real_size = len(concat_kb[list(concat_kb.keys())[0]])

            self.kb = {
                key: tf.reshape(tf.convert_to_tensor(value), [-1, self.sentence_size])
                if key != 'kb_mask' else tf.convert_to_tensor(value) for key, value in concat_kb.items()
            }
        else:
            self.kb = None
            self.kb_real_size = None

        self.sampler = sampler

        if sampler is not None:
            assert kb is not None
            self.sampler.set_kb(self.kb, self.kb_real_size)

    def _compute_logits(self, x, training=False):
        x = self.pre_classifier(x, training=training)
        x = self.dropout(x, training=training)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(x, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(x, training=training) for key, block in
                      self.final_block.items()}

        return logits

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        if not isinstance(inputs, (tuple, list, dict)):
            print(inputs.shape)

        # Sample KB (but only if training)
        if self.sampler is not None:
            inputs, sampled_kb = self.sampler.sampling(inputs, additional_info=additional_info)
        else:
            sampled_kb = self.kb

        # Clause data
        # [bs, clause_length]
        input_ids = inputs['input_ids']
        attention_mask = inputs['attention_mask']

        # KB data
        # [bs, memory_size]
        kb_input_ids = sampled_kb['kb_input_ids']
        kb_attention_mask = sampled_kb['kb_attention_mask']
        memory_mask = sampled_kb['kb_mask']

        if len(kb_input_ids.shape) == 2:
            kb_input_ids = tf.expand_dims(kb_input_ids, 0)
            kb_attention_mask = tf.expand_dims(kb_attention_mask, 0)
            memory_mask = tf.reshape(memory_mask, [1, -1])

        memory_size = kb_input_ids.shape[1]
        kb_input_ids = tf.reshape(kb_input_ids, [-1, self.sentence_size])
        kb_attention_mask = tf.reshape(kb_attention_mask, [-1, self.sentence_size])

        # [bs, dim]
        bert_clause_output = self.distilbert({
            'input_ids': input_ids,
            'attention_mask': attention_mask,
        },
            training=training,
            **kwargs)
        clause_output = bert_clause_output[0]

        if self.sentence_embedding_mode == 'first_token':
            clause_output = clause_output[:, 0]
        else:
            clause_output = tf.reduce_mean(clause_output, axis=1)

        # [bs * mem_size, dim] or [mem_size, dim]
        bert_memory_output = self.distilbert(
            {'input_ids': kb_input_ids,
             'attention_mask': kb_attention_mask},
            training=training,
            **kwargs)
        memory_output = bert_memory_output[0]

        if self.sentence_embedding_mode == 'first_token':
            memory_output = memory_output[:, 0]
        else:
            memory_output = tf.reduce_mean(memory_output, axis=1)

        # [bs, mem_size, dim]
        memory_output = tf.reshape(memory_output, [-1,
                                                   memory_size,
                                                   clause_output.shape[1]])

        memory_attention = []

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        if self.memory_lookup_info['mode'] == 'bert':
            lookup_input = {
                'query': {
                    'input_ids': input_ids,
                    'attention_mask': attention_mask,
                },
                'memory': {
                    'input_ids': kb_input_ids,
                    'attention_mask': kb_attention_mask,
                },
                'bert_layer': self.distilbert
            }
        else:
            lookup_input = {
                'query': clause_output,
                'memory': memory_output
            }

        # [bs, mem_size]
        similarities = self.memory_lookup_block(lookup_input,
                                                training=training, state=state)

        probabilities, hop_supervision_loss = self.extraction_block({
            'similarities': similarities,
            'context_mask': memory_mask,
            'target_mask': inputs['target_mask']
        },
            state=state,
            training=training)

        memory_attention.append(probabilities)

        if self.partial_supervision_info['flag']:
            supervision_losses.append(hop_supervision_loss)

        # [bs, dim]
        acc_vector = tf.reduce_sum(tf.expand_dims(probabilities, axis=-1) * memory_output, axis=1)

        # [bs, 2*dim]
        answer = self.reasoning_block({'query': clause_output, 'memory_search': acc_vector},
                                      state=state,
                                      training=training)

        answer = self._compute_logits(answer, training=training)

        # Compute answer without memory
        freezed_query = tf.stop_gradient(clause_output)
        if self.reasoning_info['mode'] == 'concat':
            freezed_query = tf.concat((freezed_query, tf.zeros_like(freezed_query)), axis=-1)

        query_only_answer = self._compute_logits(freezed_query, training=False)

        model_additional_info = {'memory_attention': memory_attention,
                                 'supervision_losses': supervision_losses,
                                 'query_emb': clause_output,
                                 'query_only_answer': query_only_answer}

        if self.sampler is not None:
            sampled_additional_info = self.sampler.get_additional_info()
            for key, value in sampled_additional_info.items():
                model_additional_info[key] = value

        return {
                   'MANN': answer,
                   'Q_Only': query_only_answer,
               }, model_additional_info


class M_DistilRoBERTa(tf.keras.Model):
    r"""
    Outputs: `Tuple` comprising various elements depending on the configuration (config) and inputs:
        **logits**: ``tf.Tensor`` of shape ``(batch_size, config.num_labels)``
            Classification (or regression if config.num_labels==1) scores (before SoftMax).
        **hidden_states**: (`optional`, returned when ``config.output_hidden_states=True``)
            list of ``tf.Tensor`` (one for the output of each layer + the output of the embeddings)
            of shape ``(batch_size, sequence_length, hidden_size)``:
            Hidden-states of the model at the output of each layer plus the initial embedding outputs.
        **attentions**: (`optional`, returned when ``config.output_attentions=True``)
            list of ``tf.Tensor`` (one for each layer) of shape ``(batch_size, num_heads, sequence_length, sequence_length)``:
            Attentions weights after the attention softmax, used to compute the weighted average in the self-attention heads.

    Examples::

        import tensorflow as tf
        from transformers import BertTokenizer, TFDistilBertForSequenceClassification

        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased')
        input_ids = tf.constant(tokenizer.encode("Hello, my dog is cute"))[None, :]  # Batch size 1
        outputs = model(input_ids)
        logits = outputs[0]
    """

    def __init__(self, bert_config, preloaded_model_name, is_bert_trainable=True,
                 num_labels=1, num_classes_per_label=None, **kwargs):
        super(M_DistilRoBERTa, self).__init__(**kwargs)
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        self.bert_config = bert_config

        self.distilroberta = TFRobertaModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                            config=bert_config, name='distilroberta')

        if not is_bert_trainable:
            for layer in self.distilroberta.layers:
                layer.trainable = False

        self.pre_classifier = tf.keras.layers.Dense(self.bert_config.hidden_size,
                                                    kernel_initializer=get_initializer(0.02),
                                                    activation='relu',
                                                    name="pre_classifier")

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_initializer=get_initializer(0.02),
                                                     name="classifier")
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_initializer=get_initializer(0.02),
                                                           name="{}_classifier".format(key))
                                for key, value in self.num_classes_per_label.items()}

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        distilbert_output = self.distilroberta(inputs, training=training, **kwargs)
        pooled_output = distilbert_output[1]  # (bs, seq_len, dim)
        pooled_output = self.pre_classifier(pooled_output, training=training)  # (bs, dim)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(pooled_output, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(pooled_output, training=training) for key, block in
                      self.final_block.items()}

        return {
                   'Q_Only': logits
               }, {
                   'query_emb': pooled_output
               }


class M_MemoryDistilRoBERTa(M_DistilRoBERTa):

    def __init__(self, query_size, sentence_size, memory_lookup_info, reasoning_info, extraction_info,
                 partial_supervision_info, accumulate_attention=False, kb=None,
                 sampler=None, **kwargs):
        super(M_MemoryDistilRoBERTa, self).__init__(**kwargs)
        self.query_size = query_size
        self.sentence_size = sentence_size
        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.accumulate_attention = accumulate_attention
        self.partial_supervision_info = partial_supervision_info

        self.memory_lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                        dropout_rate=0.0,
                                                        reasoning_info=reasoning_info,
                                                        samplewise_sampling=False if sampler is None
                                                        else sampler.samplewise_sampling)

        self.extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                         partial_supervision_info=partial_supervision_info)

        self.reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)

        if kb is not None:

            # Pad KB
            padded_kb = OrderedDict()
            for category, cat_data in kb.items():
                for key, value in cat_data.items():
                    if key != 'kb_mask':
                        value = [item + [0] * (self.sentence_size - len(item)) for item in value]
                        value = [item[:self.sentence_size] for item in value]

                    # Reverse kb keys to facilitate concatenation
                    padded_kb.setdefault(key, OrderedDict()).setdefault(category, value)

            # Concatenate KB
            concat_kb = OrderedDict()
            for key, cat_data in padded_kb.items():
                concat_kb[key] = reduce(lambda a, b: a + b, list(cat_data.values()))

            # Size info
            self.kb_real_size = len(concat_kb[list(concat_kb.keys())[0]])

            self.kb = {
                key: tf.reshape(tf.convert_to_tensor(value), [-1, self.sentence_size])
                if key != 'kb_mask' else tf.convert_to_tensor(value) for key, value in concat_kb.items()
            }
        else:
            self.kb = None
            self.kb_real_size = None

        self.sampler = sampler

        if sampler is not None:
            assert kb is not None
            self.sampler.set_kb(self.kb, self.kb_real_size)

    def _compute_logits(self, x, training=False):
        x = self.pre_classifier(x, training=training)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(x, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(x, training=training) for key, block in
                      self.final_block.items()}

        return logits

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        if not isinstance(inputs, (tuple, list, dict)):
            print(inputs.shape)

        # Sample KB (but only if training)
        if self.sampler is not None:
            inputs, sampled_kb = self.sampler.sampling(inputs, additional_info=additional_info)
        else:
            sampled_kb = self.kb

        # Clause data
        # [bs, clause_length]
        input_ids = inputs['input_ids']
        attention_mask = inputs['attention_mask']

        # KB data
        kb_input_ids = sampled_kb['kb_input_ids']
        kb_attention_mask = sampled_kb['kb_attention_mask']
        memory_mask = sampled_kb['kb_mask']

        if len(kb_input_ids.shape) == 2:
            kb_input_ids = tf.expand_dims(kb_input_ids, 0)
            kb_attention_mask = tf.expand_dims(kb_attention_mask, 0)
            memory_mask = tf.reshape(memory_mask, [1, -1])

        memory_size = kb_input_ids.shape[1]
        kb_input_ids = tf.reshape(kb_input_ids, [-1, self.sentence_size])
        kb_attention_mask = tf.reshape(kb_attention_mask, [-1, self.sentence_size])

        # [bs, dim]
        bert_clause_output = self.distilroberta({
            'input_ids': input_ids,
            'attention_mask': attention_mask,
        },
            training=training,
            **kwargs)
        clause_output = bert_clause_output[1]

        # [bs * mem_size, dim] or [mem_size, dim]
        bert_memory_output = self.distilroberta(
            {'input_ids': kb_input_ids,
             'attention_mask': kb_attention_mask},
            training=training,
            **kwargs)
        memory_output = bert_memory_output[1]

        # [bs, mem_size, dim]
        memory_output = tf.reshape(memory_output, [-1,
                                                   memory_size,
                                                   clause_output.shape[1]])

        memory_attention = []

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        if self.memory_lookup_info['mode'] == 'bert':
            lookup_input = {
                'query': {
                    'input_ids': input_ids,
                    'attention_mask': attention_mask,
                },
                'memory': {
                    'input_ids': kb_input_ids,
                    'attention_mask': kb_attention_mask,
                },
                'bert_layer': self.distilroberta
            }
        else:
            lookup_input = {
                'query': clause_output,
                'memory': memory_output
            }

        # [bs, mem_size]
        similarities = self.memory_lookup_block(lookup_input,
                                                training=training, state=state)

        probabilities, hop_supervision_loss = self.extraction_block({
            'similarities': similarities,
            'context_mask': memory_mask,
            'target_mask': inputs['target_mask']
        },
            state=state,
            training=training)

        memory_attention.append(probabilities)

        if self.partial_supervision_info['flag']:
            supervision_losses.append(hop_supervision_loss)

        # [bs, dim]
        acc_vector = tf.reduce_sum(tf.expand_dims(probabilities, axis=-1) * memory_output, axis=1)

        # [bs, 2 * dim]
        answer = self.reasoning_block({'query': clause_output, 'memory_search': acc_vector},
                                      state=state,
                                      training=training)

        answer = self._compute_logits(answer, training=training)

        # Compute answer without memory
        freezed_query = tf.stop_gradient(clause_output)
        if self.reasoning_info['mode'] == 'concat':
            freezed_query = tf.concat((freezed_query, tf.zeros_like(freezed_query)), axis=-1)

        query_only_answer = self._compute_logits(freezed_query, training=False)

        model_additional_info = {'memory_attention': memory_attention,
                                 'supervision_losses': supervision_losses,
                                 'query_emb': clause_output,
                                 'query_only_answer': query_only_answer}

        if self.sampler is not None:
            sampled_additional_info = self.sampler.get_additional_info()
            for key, value in sampled_additional_info.items():
                model_additional_info[key] = value

        return {
                   'MANN': answer,
                   'Q_Only': query_only_answer,
               }, model_additional_info


class M_BERT(tf.keras.Model):
    r"""
    Outputs: `Tuple` comprising various elements depending on the configuration (config) and inputs:
        **logits**: ``tf.Tensor`` of shape ``(batch_size, config.num_labels)``
            Classification (or regression if config.num_labels==1) scores (before SoftMax).
        **hidden_states**: (`optional`, returned when ``config.output_hidden_states=True``)
            list of ``tf.Tensor`` (one for the output of each layer + the output of the embeddings)
            of shape ``(batch_size, sequence_length, hidden_size)``:
            Hidden-states of the model at the output of each layer plus the initial embedding outputs.
        **attentions**: (`optional`, returned when ``config.output_attentions=True``)
            list of ``tf.Tensor`` (one for each layer) of shape ``(batch_size, num_heads, sequence_length, sequence_length)``:
            Attentions weights after the attention softmax, used to compute the weighted average in the self-attention heads.

    Examples::

        import tensorflow as tf
        from transformers import BertTokenizer, TFDistilBertForSequenceClassification

        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased')
        input_ids = tf.constant(tokenizer.encode("Hello, my dog is cute"))[None, :]  # Batch size 1
        outputs = model(input_ids)
        logits = outputs[0]
    """

    def __init__(self, bert_config, preloaded_model_name, is_bert_trainable=True,
                 num_labels=1, num_classes_per_label=None, **kwargs):
        super(M_BERT, self).__init__(**kwargs)
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        self.bert_config = bert_config

        self.bert = TFBertModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                config=bert_config, name='bert')

        if not is_bert_trainable:
            for layer in self.bert.layers:
                layer.trainable = False

        self.pre_classifier = tf.keras.layers.Dense(self.bert_config.hidden_size,
                                                    kernel_initializer=get_initializer(0.02),
                                                    activation='relu',
                                                    name="pre_classifier")

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_initializer=get_initializer(0.02),
                                                     name="classifier")
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_initializer=get_initializer(0.02),
                                                           name="{}_classifier".format(key))
                                for key, value in self.num_classes_per_label.items()}

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        distilbert_output = self.bert(inputs, training=training, **kwargs)
        pooled_output = distilbert_output[1]  # (bs, seq_len, dim)
        pooled_output = self.pre_classifier(pooled_output, training=training)  # (bs, dim)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(pooled_output, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(pooled_output, training=training) for key, block in
                      self.final_block.items()}

        return {
                   'Q_Only': logits
               }, {
                   'query_emb': pooled_output
               }


class M_MemoryBERT(M_BERT):

    def __init__(self, query_size, sentence_size, memory_lookup_info, reasoning_info, extraction_info,
                 partial_supervision_info, accumulate_attention=False, kb=None,
                 sampler=None, **kwargs):
        super(M_MemoryBERT, self).__init__(**kwargs)
        self.query_size = query_size
        self.sentence_size = sentence_size
        self.memory_lookup_info = memory_lookup_info
        self.extraction_info = extraction_info
        self.reasoning_info = reasoning_info
        self.accumulate_attention = accumulate_attention
        self.partial_supervision_info = partial_supervision_info

        self.memory_lookup_block = MemorySentenceLookup(memory_lookup_info=memory_lookup_info,
                                                        dropout_rate=0.0,
                                                        reasoning_info=reasoning_info,
                                                        samplewise_sampling=False if sampler is None
                                                        else sampler.samplewise_sampling)

        self.extraction_block = SentenceMemoryExtraction(extraction_info=extraction_info,
                                                         partial_supervision_info=partial_supervision_info)

        self.reasoning_block = SentenceMemoryReasoning(reasoning_info=reasoning_info)

        if kb is not None:

            # Pad KB
            padded_kb = OrderedDict()
            for category, cat_data in kb.items():
                for key, value in cat_data.items():
                    if key != 'kb_mask':
                        value = [item + [0] * (self.sentence_size - len(item)) for item in value]
                        value = [item[:self.sentence_size] for item in value]

                    # Reverse kb keys to facilitate concatenation
                    padded_kb.setdefault(key, OrderedDict()).setdefault(category, value)

            # Concatenate KB
            concat_kb = OrderedDict()
            for key, cat_data in padded_kb.items():
                concat_kb[key] = reduce(lambda a, b: a + b, list(cat_data.values()))

            # Size info
            self.kb_real_size = len(concat_kb[list(concat_kb.keys())[0]])

            self.kb = {
                key: tf.reshape(tf.convert_to_tensor(value), [-1, self.sentence_size])
                if key != 'kb_mask' else tf.convert_to_tensor(value) for key, value in concat_kb.items()
            }
        else:
            self.kb = None
            self.kb_real_size = None

        self.sampler = sampler

        if sampler is not None:
            assert kb is not None
            self.sampler.set_kb(self.kb, self.kb_real_size)

    def _compute_logits(self, x, training=False):
        x = self.pre_classifier(x, training=training)

        # Multi-label
        if self.num_labels == 1:
            logits = self.final_block(x, training=training)
        else:
            logits = {'label_id_{}'.format(key): block(x, training=training) for key, block in
                      self.final_block.items()}

        return logits

    def call(self, inputs, training=False, state='training', additional_info=None, **kwargs):
        if not isinstance(inputs, (tuple, list, dict)):
            print(inputs.shape)

        # Sample KB (but only if training)
        if self.sampler is not None:
            inputs, sampled_kb = self.sampler.sampling(inputs, additional_info=additional_info)
        else:
            sampled_kb = self.kb

        # Clause data
        # [bs, clause_length]
        input_ids = inputs['input_ids']
        attention_mask = inputs['attention_mask']

        # KB data
        kb_input_ids = sampled_kb['kb_input_ids']
        kb_attention_mask = sampled_kb['kb_attention_mask']
        memory_mask = sampled_kb['kb_mask']

        if len(kb_input_ids.shape) == 2:
            kb_input_ids = tf.expand_dims(kb_input_ids, 0)
            kb_attention_mask = tf.expand_dims(kb_attention_mask, 0)
            memory_mask = tf.reshape(memory_mask, [1, -1])

        memory_size = kb_input_ids.shape[1]
        kb_input_ids = tf.reshape(kb_input_ids, [-1, self.sentence_size])
        kb_attention_mask = tf.reshape(kb_attention_mask, [-1, self.sentence_size])

        # [bs, dim]
        bert_clause_output = self.bert({
            'input_ids': input_ids,
            'attention_mask': attention_mask,
        },
            training=training,
            **kwargs)
        clause_output = bert_clause_output[1]

        # [bs * mem_size, dim] or [mem_size, dim]
        bert_memory_output = self.bert(
            {'input_ids': kb_input_ids,
             'attention_mask': kb_attention_mask},
            training=training,
            **kwargs)
        memory_output = bert_memory_output[1]

        # [bs, mem_size, dim]
        memory_output = tf.reshape(memory_output, [-1,
                                                   memory_size,
                                                   clause_output.shape[1]])

        memory_attention = []

        if self.partial_supervision_info:
            supervision_losses = []
        else:
            supervision_losses = None

        if self.memory_lookup_info['mode'] == 'bert':
            lookup_input = {
                'query': {
                    'input_ids': input_ids,
                    'attention_mask': attention_mask,
                },
                'memory': {
                    'input_ids': kb_input_ids,
                    'attention_mask': kb_attention_mask,
                },
                'bert_layer': self.bert
            }
        else:
            lookup_input = {
                'query': clause_output,
                'memory': memory_output
            }

        # [bs, mem_size]
        similarities = self.memory_lookup_block(lookup_input,
                                                training=training, state=state)

        probabilities, hop_supervision_loss = self.extraction_block({
            'similarities': similarities,
            'context_mask': memory_mask,
            'target_mask': inputs['target_mask']
        },
            state=state,
            training=training)

        memory_attention.append(probabilities)

        if self.partial_supervision_info['flag']:
            supervision_losses.append(hop_supervision_loss)

        # [bs, dim]
        acc_vector = tf.reduce_sum(tf.expand_dims(probabilities, axis=-1) * memory_output, axis=1)

        # [bs, 2*dim]
        answer = self.reasoning_block({'query': clause_output, 'memory_search': acc_vector},
                                      state=state,
                                      training=training)

        answer = self._compute_logits(answer, training=training)

        # Compute answer without memory
        freezed_query = tf.stop_gradient(clause_output)
        if self.reasoning_info['mode'] == 'concat':
            freezed_query = tf.concat((freezed_query, tf.zeros_like(freezed_query)), axis=-1)

        query_only_answer = self._compute_logits(freezed_query, training=False)

        model_additional_info = {'memory_attention': memory_attention,
                                 'supervision_losses': supervision_losses,
                                 'query_emb': clause_output,
                                 'query_only_answer': query_only_answer}

        if self.sampler is not None:
            sampled_additional_info = self.sampler.get_additional_info()
            for key, value in sampled_additional_info.items():
                model_additional_info[key] = value

        return {
                   'MANN': answer,
                   'Q_Only': query_only_answer,
               }, model_additional_info


class M_RationaleCNN(tf.keras.Model):

    def __init__(self, doc_size, sentence_size, vocab_size, ngram_filters=[3, 4, 5], n_filters=32, sent_dropout=0.5,
                 doc_dropout=0.5,
                 embedding_matrix=None, embedding_dimension=32, l2_regularization=0., **kwargs):
        super(M_RationaleCNN, self).__init__(**kwargs)

        self.embedding_dimension = embedding_dimension
        self.sentence_size = sentence_size
        self.doc_size = doc_size
        self.ngram_filters = ngram_filters
        self.n_filters = n_filters
        self.sent_dropout = sent_dropout
        self.doc_dropout = doc_dropout

        # Layers definition
        self.embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                   output_dim=embedding_dimension,
                                                   input_length=sentence_size,
                                                   weights=embedding_matrix if embedding_matrix is None else [
                                                       embedding_matrix],
                                                   mask_zero=True,
                                                   name='embedding')

        self.conv_layers = [tf.keras.layers.Conv2D(self.n_filters, (1, n_gram_filter * self.embedding_dimension),
                                                   strides=(1, self.embedding_dimension), activation='relu',
                                                   data_format='channels_first')
                            for n_gram_filter in self.ngram_filters]
        self.pooling_layers = [tf.keras.layers.MaxPooling2D(pool_size=(1, self.sentence_size - n_gram_filter + 1),
                                                            data_format='channels_first') for
                               n_gram_filter in self.ngram_filters]

        self.sentence_clf = tf.keras.layers.Dense(2, activation='softmax',
                                                  kernel_regularizer=tf.keras.regularizers.l2(
                                                      l2_regularization))

        self.doc_dropout_layer = tf.keras.layers.Dropout(rate=self.doc_dropout)

        self.doc_clf = tf.keras.layers.Dense(2, activation='softmax')

    def call(self, inputs, state='training', training=False, **kwargs):
        # doc -> [batch_size, max_doc_len * max_sent_len]

        doc = inputs['doc_ids']

        # [batch_size, max_doc_len * max_sent_len, emb_dim]
        doc_emb = self.embedding(doc)

        # [batch_size, channels, max_doc_len, max_sent_len * emb_dim]
        doc_emb = tf.reshape(doc_emb, [doc.shape[0], 1, self.doc_size, self.sentence_size * self.embedding_dimension])

        convolutions = []

        # conv1 -> [batch_size, n_filters, max_doc_len, dim1]
        # conv2 -> [batch_size, n_filters, max_doc_len, dim2]
        # conv3 -> [batch_size, n_filters, max_doc_len, dim3]
        for conv_layer, pooling_layer in zip(self.conv_layers, self.pooling_layers):
            # [batch_size, n_filters, max_doc_len, dim_conv]
            conv_res = conv_layer(doc_emb)

            # [batch_size, n_filters, max_doc_len, 1]
            conv_res = pooling_layer(conv_res)

            # [batch_size, max_doc_len, n_filters, 1]
            conv_res = tf.transpose(conv_res, [0, 2, 1, 3])

            # [batch_size, max_doc_len, n_filters]
            conv_res = tf.reshape(conv_res, [doc.shape[0], self.doc_size, self.n_filters])
            convolutions.append(conv_res)

        # [batch_size, max_doc_len, n_filters * conv_layers]
        sent_vectors = tf.concat(convolutions, axis=-1)

        sent_preds = self.sentence_clf(sent_vectors)
        sent_weights = tf.reduce_max(sent_preds, axis=-1)

        # Doc level

        doc_vector = tf.reduce_sum(sent_vectors * sent_weights[:, :, None], axis=1)
        doc_vector = self.doc_dropout_layer(doc_vector)

        doc_pred = self.doc_clf(doc_vector)

        return doc_pred, {'sent_weights': sent_weights, 'sent_logits': sent_preds}


class M_SquadMANN(tf.keras.Model):

    def __init__(self, max_seq_length, max_context_length,
                 memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 vocab_size, embedding_dimension=32, hops=1,
                 answer_weights=[], dropout_rate=0.2, l2_regularization=0.,
                 embedding_matrix=None, accumulate_attention=False,
                 position_encoding=False, **kwargs):

        super(M_SquadMANN, self).__init__(**kwargs)
        self.max_seq_length = max_seq_length
        self.max_context_length = max_context_length
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.use_positional_encoding = position_encoding
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.reasoning_info = reasoning_info

        if self.use_positional_encoding:
            self.pos_encoding = positional_encoding(vocab_size, embedding_dimension)

        self.query_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=max_seq_length,
                                                         weights=embedding_matrix if embedding_matrix is None else [
                                                             embedding_matrix],
                                                         mask_zero=True,
                                                         name='query_embedding')
        self.memory_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                          output_dim=embedding_dimension,
                                                          input_length=max_context_length,
                                                          weights=embedding_matrix if embedding_matrix is None else [
                                                              embedding_matrix],
                                                          mask_zero=True,
                                                          name='memory_embedding')

        lookup_block = MemoryTokenLookup(memory_lookup_info=memory_lookup_info,
                                         dropout_rate=dropout_rate,
                                         l2_regularization=l2_regularization,
                                         reasoning_info=reasoning_info)
        self.memory_lookup_blocks = [lookup_block for _ in range(hops)]

        if partial_supervision_info['flag']:
            partial_supervision_info['flag'] = False
        extraction_block = TokenMemoryExtraction(extraction_info=extraction_info,
                                                 partial_supervision_info=partial_supervision_info)
        self.extraction_blocks = [extraction_block for _ in range(hops)]

        reasoning_block = TokenMemoryReasoning(reasoning_info=reasoning_info)
        self.memory_reasoning_blocks = [reasoning_block for _ in range(hops)]

        # Answer start
        self.answer_start_blocks = []
        for idx, weight in enumerate(answer_weights):
            self.answer_start_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                  name='answer_start_block_{}'.format(idx + 1),
                                                                  activation=None,
                                                                  kernel_regularizer=tf.keras.regularizers.l2(
                                                                      l2_regularization)))
            self.answer_start_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_start_final_block = tf.keras.layers.Dense(units=1,
                                                              kernel_regularizer=tf.keras.regularizers.l2(
                                                                  l2_regularization),
                                                              name='answer_start_final_block')

        # Answer end
        self.answer_end_blocks = []
        for idx, weight in enumerate(answer_weights):
            self.answer_end_blocks.append(tf.keras.layers.Dense(units=weight,
                                                                name='answer_end_block_{}'.format(idx + 1),
                                                                activation=None,
                                                                kernel_regularizer=tf.keras.regularizers.l2(
                                                                    l2_regularization)))
            self.answer_end_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        self.answer_end_final_block = tf.keras.layers.Dense(units=1,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization),
                                                            name='answer_end_final_block')

    def _compute_answer_start(self, x, training=False):
        for block in self.answer_start_blocks:
            x = block(x, training=training)

        x = self.answer_start_final_block(x, training=training)
        return x

    def _compute_answer_end(self, x, training=False):
        for block in self.answer_end_blocks:
            x = block(x, training=training)

        x = self.answer_end_final_block(x, training=training)
        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):
        # [batch_size, max_seq_length]
        query = inputs['question_ids']
        query_mask = tf.where(query != 0, tf.ones_like(query), tf.zeros_like(query))

        # [batch_size, max_context_length]
        memory = tf.reshape(inputs['context_ids'], [-1, self.max_context_length])
        memory_mask = tf.where(memory != 0, tf.ones_like(memory), tf.zeros_like(memory))

        query_emb = self.query_embedding(query)
        memory_emb = self.memory_embedding(memory)

        if self.use_positional_encoding:
            query_emb += self.pos_encoding[:, :tf.shape(query_emb)[1], :]
            memory_emb += self.pos_encoding[:, :tf.shape(memory_emb)[1], :]

        upd_query, upd_memory = query_emb, memory_emb

        for lookup_block, extraction_block, reasoning_block in zip(self.memory_lookup_blocks,
                                                                   self.extraction_blocks,
                                                                   self.memory_reasoning_blocks):
            # [batch_size, max_context_length, max_seq_length]
            similarities = lookup_block({'query': upd_query, 'memory': upd_memory},
                                        training=training)

            memory_probs, query_probs = extraction_block({'similarities': similarities,
                                                          'memory_mask': memory_mask,
                                                          'query_mask': query_mask},
                                                         state=state,
                                                         training=training)

            upd_query, upd_memory = reasoning_block({'query': upd_query, 'memory': upd_memory,
                                                     'query_probs': query_probs, 'memory_probs': memory_probs},
                                                    state=state,
                                                    training=training)

        # Compute answer start
        # [batch_size, memory_tokens]
        answer_start_logits = self._compute_answer_start(x=upd_memory, training=training)
        answer_start_logits = tf.squeeze(answer_start_logits)
        answer_start_probs = masked_softmax(logits=answer_start_logits, mask=memory_mask, axis=-1, log_softmax=False)

        # Computer answer end
        # [batch_size, memory_tokens]
        answer_end_logits = self._compute_answer_end(x=upd_memory, training=training)
        answer_end_logits = tf.squeeze(answer_end_logits)
        answer_end_probs = masked_softmax(logits=answer_end_logits, mask=memory_mask, axis=-1, log_softmax=False)

        return {
                   'answer_start': answer_start_logits,
                   'answer_start_probs': answer_start_probs,
                   'answer_end': answer_end_logits,
                   'answer_end_probs': answer_end_probs,
                   'memory_mask': memory_mask,
                   'memory': memory
               }, {
               }


class M_BiDAF(tf.keras.Model):

    def __init__(self, max_seq_length, max_context_length, vocab_size, token_encoder_units,
                 output_encoder_units, embedding_dimension=32, embedding_matrix=None,
                 dropout_rate=0.1, **kwargs):
        super(M_BiDAF, self).__init__(**kwargs)
        self.max_seq_length = max_seq_length
        self.max_context_length = max_context_length
        self.token_encoder_units = token_encoder_units
        self.output_encoder_units = output_encoder_units
        self.dropout_rate = dropout_rate

        self.embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                   output_dim=embedding_dimension,
                                                   input_length=max_context_length,
                                                   weights=embedding_matrix if embedding_matrix is None else [
                                                       embedding_matrix],
                                                   mask_zero=True,
                                                   name='embedding')

        self.token_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.GRU(units=token_encoder_units,
                                                                               return_sequences=True))

        self.bidaf_attention = BiDAFAttention(hidden_size=2 * token_encoder_units,
                                              dropout_rate=dropout_rate)

        self.output_encoder = tf.keras.layers.Bidirectional(tf.keras.layers.GRU(units=output_encoder_units,
                                                                                return_sequences=True))

        self.bidaf_output = BiDAFOutput(hidden_size=2 * token_encoder_units,
                                        dropout_rate=dropout_rate)

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):
        # [batch_size, max_seq_length]
        query = inputs['question_ids']
        query_mask = tf.where(query != 0, tf.ones_like(query), tf.zeros_like(query))

        # [batch_size, max_context_length]
        context = tf.reshape(inputs['context_ids'], [-1, self.max_context_length])
        context_mask = tf.where(context != 0, tf.ones_like(context), tf.zeros_like(context))

        # [batch_size, max_seq_length, 2 * hidden_size]
        query_emb = self.embedding(query)
        query_emb = self.token_encoder(query_emb)

        # [batch_size, max_context_length, 2 * hidden_size]
        context_emb = self.embedding(context)
        context_emb = self.token_encoder(context_emb)

        # [batch_size, max_context_length, 8 * hidden_size]
        bidaf_attention = self.bidaf_attention({
            'query': query_emb,
            'context': context_emb,
            'query_mask': query_mask,
            'context_mask': context_mask
        }, training=training)

        # [batch_size, max_context_length, 2 * hidden_size]
        encoded_output = self.output_encoder(bidaf_attention)

        # 2 * [batch_size, max_context_length]
        answer_start_probs, answer_end_probs = self.bidaf_output({
            'bidaf_attention': bidaf_attention,
            'encoded_output': encoded_output,
            'context_mask': context_mask
        }, training=training)

        return {
                   'answer_start_probs': answer_start_probs,
                   'answer_end_probs': answer_end_probs,
                   'context_mask': context_mask,
                   'context': context
               }, {
               }


class M_SquadDistilBert(tf.keras.Model):

    def __init__(self, bert_config, preloaded_model_name, max_seq_length, max_context_length, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(M_SquadDistilBert, self).__init__(**kwargs)
        self.max_seq_length = max_seq_length
        self.max_context_length = max_context_length
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.reasoning_info = reasoning_info
        self.bert_config = bert_config

        self.distilbert = TFDistilBertModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                            config=bert_config, name='distilbert')

        if not is_bert_trainable:
            for layer in self.distilbert.layers:
                layer.trainable = False

        self.lookup_block = MemoryTokenLookup(memory_lookup_info=memory_lookup_info,
                                              dropout_rate=0.0,
                                              l2_regularization=0.0,
                                              reasoning_info=reasoning_info)

        if partial_supervision_info['flag']:
            partial_supervision_info['flag'] = False
        self.extraction_block = TokenMemoryExtraction(extraction_info=extraction_info,
                                                      partial_supervision_info=partial_supervision_info)

        self.reasoning_block = TokenMemoryReasoning(reasoning_info=reasoning_info)

        # Answer start
        self.answer_start_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                        kernel_initializer=get_initializer(0.02),
                                                        activation='selu',
                                                        name="answer_start_block")
        self.answer_start_final_block = tf.keras.layers.Dense(units=1,
                                                              name='answer_start_final_block')

        # Answer end
        self.answer_end_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                      kernel_initializer=get_initializer(0.02),
                                                      activation='selu',
                                                      name="answer_end_block")
        self.answer_end_final_block = tf.keras.layers.Dense(units=1,
                                                            name='answer_end_final_block')

    def _compute_answer_start(self, x, training=False):
        x = self.answer_start_block(x, training=training)
        x = self.answer_start_final_block(x, training=training)
        return x

    def _compute_answer_end(self, x, training=False):
        x = self.answer_end_block(x, training=training)
        x = self.answer_end_final_block(x, training=training)
        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):
        # [batch_size, max_seq_length]
        query = inputs['question_ids']
        query_attention_mask = inputs['question_attention_mask']
        query_mask = tf.where(query != 0, tf.ones_like(query), tf.zeros_like(query))

        # [batch_size, max_context_length]
        memory = inputs['context_ids']
        memory_attention_mask = inputs['context_attention_mask']
        memory_mask = tf.where(memory != 0, tf.ones_like(memory), tf.zeros_like(memory))

        # Embedding
        query_output = self.distilbert({
            'input_ids': query,
            'attention_mask': query_attention_mask
        }, training=training, **kwargs)
        query_output = query_output[0]

        memory_output = self.distilbert({
            'input_ids': memory,
            'attention_mask': memory_attention_mask
        }, training=training, **kwargs)
        memory_output = memory_output[0]

        # [batch_size, max_context_length, max_seq_length]
        similarities = self.lookup_block({'query': query_output, 'memory': memory_output}, training=training)

        memory_probs, query_probs = self.extraction_block({'similarities': similarities,
                                                           'memory_mask': memory_mask,
                                                           'query_mask': query_mask},
                                                          state=state,
                                                          training=training)

        upd_query, upd_memory = self.reasoning_block({'query': query_output, 'memory': memory_output,
                                                      'query_probs': query_probs, 'memory_probs': memory_probs},
                                                     state=state,
                                                     training=training)

        # Compute answer start
        # [batch_size, memory_tokens]
        answer_start_logits = self._compute_answer_start(x=upd_memory, training=training)
        answer_start_logits = tf.squeeze(answer_start_logits)
        answer_start_probs = masked_softmax(logits=answer_start_logits, mask=memory_mask, axis=-1, log_softmax=False)

        # Computer answer end
        # [batch_size, memory_tokens]
        answer_end_logits = self._compute_answer_end(x=upd_memory, training=training)
        answer_end_logits = tf.squeeze(answer_end_logits)
        answer_end_probs = masked_softmax(logits=answer_end_logits, mask=memory_mask, axis=-1, log_softmax=False)

        return {
                   'answer_start': answer_start_logits,
                   'answer_start_probs': answer_start_probs,
                   'answer_end': answer_end_logits,
                   'answer_end_probs': answer_end_probs,
                   'memory_mask': memory_mask,
                   'memory': memory
               }, {
               }


class M_SquadDistilRoBerta(tf.keras.Model):

    def __init__(self, bert_config, preloaded_model_name, max_seq_length, max_context_length, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(M_SquadDistilRoBerta, self).__init__(**kwargs)
        self.max_seq_length = max_seq_length
        self.max_context_length = max_context_length
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.reasoning_info = reasoning_info
        self.bert_config = bert_config

        self.roberta = TFRobertaModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                      config=bert_config, name='roberta')

        if not is_bert_trainable:
            for layer in self.roberta.layers:
                layer.trainable = False

        self.lookup_block = MemoryTokenLookup(memory_lookup_info=memory_lookup_info,
                                              dropout_rate=0.0,
                                              l2_regularization=0.0,
                                              reasoning_info=reasoning_info)

        if partial_supervision_info['flag']:
            partial_supervision_info['flag'] = False
        self.extraction_block = TokenMemoryExtraction(extraction_info=extraction_info,
                                                      partial_supervision_info=partial_supervision_info)

        self.reasoning_block = TokenMemoryReasoning(reasoning_info=reasoning_info)

        # Answer start
        self.answer_start_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                        kernel_initializer=get_initializer(0.02),
                                                        activation='selu',
                                                        name="answer_start_block")
        self.answer_start_final_block = tf.keras.layers.Dense(units=1,
                                                              name='answer_start_final_block')

        # Answer end
        self.answer_end_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                      kernel_initializer=get_initializer(0.02),
                                                      activation='selu',
                                                      name="answer_end_block")
        self.answer_end_final_block = tf.keras.layers.Dense(units=1,
                                                            name='answer_end_final_block')

    def _compute_answer_start(self, x, training=False):
        x = self.answer_start_block(x, training=training)
        x = self.answer_start_final_block(x, training=training)
        return x

    def _compute_answer_end(self, x, training=False):
        x = self.answer_end_block(x, training=training)
        x = self.answer_end_final_block(x, training=training)
        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):
        # [batch_size, max_seq_length]
        query = inputs['question_ids']
        query_attention_mask = inputs['question_attention_mask']
        query_mask = tf.where(query != 0, tf.ones_like(query), tf.zeros_like(query))

        # [batch_size, max_context_length]
        memory = inputs['context_ids']
        memory_attention_mask = inputs['context_attention_mask']
        memory_mask = tf.where(memory != 0, tf.ones_like(memory), tf.zeros_like(memory))

        # Embedding
        query_output = self.roberta({
            'input_ids': query,
            'attention_mask': query_attention_mask
        }, training=training, **kwargs)
        query_output = query_output[0]

        memory_output = self.roberta({
            'input_ids': memory,
            'attention_mask': memory_attention_mask
        }, training=training, **kwargs)
        memory_output = memory_output[0]

        # [batch_size, max_context_length, max_seq_length]
        similarities = self.lookup_block({'query': query_output, 'memory': memory_output}, training=training)

        memory_probs, query_probs = self.extraction_block({'similarities': similarities,
                                                           'memory_mask': memory_mask,
                                                           'query_mask': query_mask},
                                                          state=state,
                                                          training=training)

        upd_query, upd_memory = self.reasoning_block({'query': query_output, 'memory': memory_output,
                                                      'query_probs': query_probs, 'memory_probs': memory_probs},
                                                     state=state,
                                                     training=training)

        # Compute answer start
        # [batch_size, memory_tokens]
        answer_start_logits = self._compute_answer_start(x=upd_memory, training=training)
        answer_start_logits = tf.squeeze(answer_start_logits)
        answer_start_probs = masked_softmax(logits=answer_start_logits, mask=memory_mask, axis=-1, log_softmax=False)

        # Computer answer end
        # [batch_size, memory_tokens]
        answer_end_logits = self._compute_answer_end(x=upd_memory, training=training)
        answer_end_logits = tf.squeeze(answer_end_logits)
        answer_end_probs = masked_softmax(logits=answer_end_logits, mask=memory_mask, axis=-1, log_softmax=False)

        return {
                   'answer_start': answer_start_logits,
                   'answer_start_probs': answer_start_probs,
                   'answer_end': answer_end_logits,
                   'answer_end_probs': answer_end_probs,
                   'memory_mask': memory_mask,
                   'memory': memory
               }, {
               }


class M_SquadBERT(tf.keras.Model):

    def __init__(self, bert_config, preloaded_model_name, max_seq_length, max_context_length, memory_lookup_info,
                 extraction_info, reasoning_info, partial_supervision_info,
                 accumulate_attention=False, is_bert_trainable=True, **kwargs):
        super(M_SquadBERT, self).__init__(**kwargs)
        self.max_seq_length = max_seq_length
        self.max_context_length = max_context_length
        self.partial_supervision_info = partial_supervision_info
        self.accumulate_attention = accumulate_attention
        self.reasoning_info = reasoning_info
        self.bert_config = bert_config

        self.bert = TFBertModel.from_pretrained(pretrained_model_name_or_path=preloaded_model_name,
                                                config=bert_config, name='bert')

        if not is_bert_trainable:
            for layer in self.bert.layers:
                layer.trainable = False

        self.lookup_block = MemoryTokenLookup(memory_lookup_info=memory_lookup_info,
                                              dropout_rate=0.0,
                                              l2_regularization=0.0,
                                              reasoning_info=reasoning_info)

        if partial_supervision_info['flag']:
            partial_supervision_info['flag'] = False
        self.extraction_block = TokenMemoryExtraction(extraction_info=extraction_info,
                                                      partial_supervision_info=partial_supervision_info)

        self.reasoning_block = TokenMemoryReasoning(reasoning_info=reasoning_info)

        # Answer start
        self.answer_start_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                        kernel_initializer=get_initializer(0.02),
                                                        activation='selu',
                                                        name="answer_start_block")
        self.answer_start_final_block = tf.keras.layers.Dense(units=1,
                                                              name='answer_start_final_block')

        # Answer end
        self.answer_end_block = tf.keras.layers.Dense(self.bert_config.dim,
                                                      kernel_initializer=get_initializer(0.02),
                                                      activation='selu',
                                                      name="answer_end_block")
        self.answer_end_final_block = tf.keras.layers.Dense(units=1,
                                                            name='answer_end_final_block')

    def _compute_answer_start(self, x, training=False):
        x = self.answer_start_block(x, training=training)
        x = self.answer_start_final_block(x, training=training)
        return x

    def _compute_answer_end(self, x, training=False):
        x = self.answer_end_block(x, training=training)
        x = self.answer_end_final_block(x, training=training)
        return x

    def call(self, inputs, state='training', training=False, additional_info=None, **kwargs):
        # [batch_size, max_seq_length]
        query = inputs['question_ids']
        query_attention_mask = inputs['question_attention_mask']
        query_mask = tf.where(query != 0, tf.ones_like(query), tf.zeros_like(query))

        # [batch_size, max_context_length]
        memory = inputs['context_ids']
        memory_attention_mask = inputs['context_attention_mask']
        memory_mask = tf.where(memory != 0, tf.ones_like(memory), tf.zeros_like(memory))

        # Embedding
        query_output = self.bert({
            'input_ids': query,
            'attention_mask': query_attention_mask
        }, training=training, **kwargs)
        query_output = query_output[0]

        memory_output = self.bert({
            'input_ids': memory,
            'attention_mask': memory_attention_mask
        }, training=training, **kwargs)
        memory_output = memory_output[0]

        # [batch_size, max_context_length, max_seq_length]
        similarities = self.lookup_block({'query': query_output, 'memory': memory_output}, training=training)

        memory_probs, query_probs = self.extraction_block({'similarities': similarities,
                                                           'memory_mask': memory_mask,
                                                           'query_mask': query_mask},
                                                          state=state,
                                                          training=training)

        upd_query, upd_memory = self.reasoning_block({'query': query_output, 'memory': memory_output,
                                                      'query_probs': query_probs, 'memory_probs': memory_probs},
                                                     state=state,
                                                     training=training)

        # Compute answer start
        # [batch_size, memory_tokens]
        answer_start_logits = self._compute_answer_start(x=upd_memory, training=training)
        answer_start_logits = tf.squeeze(answer_start_logits)
        answer_start_probs = masked_softmax(logits=answer_start_logits, mask=memory_mask, axis=-1, log_softmax=False)

        # Computer answer end
        # [batch_size, memory_tokens]
        answer_end_logits = self._compute_answer_end(x=upd_memory, training=training)
        answer_end_logits = tf.squeeze(answer_end_logits)
        answer_end_probs = masked_softmax(logits=answer_end_logits, mask=memory_mask, axis=-1, log_softmax=False)

        return {
                   'answer_start': answer_start_logits,
                   'answer_start_probs': answer_start_probs,
                   'answer_end': answer_end_logits,
                   'answer_end_probs': answer_end_probs,
                   'memory_mask': memory_mask,
                   'memory': memory
               }, {
               }


######################################################
####################### LAYERS #######################
######################################################

# Memory Network layers

# Embedding

class MemorySentenceEmbedder(tf.keras.layers.Layer):

    def __init__(self, embedding_info, embedding_dimension, **kwargs):
        super(MemorySentenceEmbedder, self).__init__(**kwargs)
        self.embedding_info = embedding_info

        if self.embedding_info['mode'] == 'generalized_pooling':
            self.pooling = GeneralizedPooling(segments_amount=1,
                                              attention_dimension=self.embedding_info['attention_dimension'],
                                              embedding_dimension=embedding_dimension)

        if self.embedding_info['mode'] == 'recurrent_embedding':
            if self.embedding_info['bidirectional']:
                self.rnn = tf.keras.layers.Bidirectional(tf.keras.layers.GRU(self.embedding_info['hidden_size'],
                                                                             # activation=tf.nn.sigmoid
                                                                             ))
            else:
                self.rnn = tf.keras.layers.GRU(self.embedding_info['hidden_size'],
                                               # activation=tf.nn.sigmoid
                                               )

    def call(self, inputs, training=None, state='training', mask=None, axis=2, **kwargs):
        # [# samples, max_sequence_length, embedding_dim]

        if self.embedding_info['mode'] == 'sum':
            return tf.reduce_sum(inputs, axis=axis)
        elif self.embedding_info['mode'] == 'mean':
            return tf.reduce_mean(inputs, axis=axis)
        elif self.embedding_info['mode'] == 'generalized_pooling':
            pooled = self.pooling(inputs, training=training, state=state)
            return tf.squeeze(pooled)
        elif self.embedding_info['mode'] == 'recurrent_embedding':
            if self.embedding_info['bidirectional']:
                rnn = self.rnn(inputs)
            else:
                rnn = self.rnn(inputs)
            return rnn
        else:
            raise RuntimeError('Invalid sentence embedding mode! Got: {}'.format(self.embedding_info['mode']))


# Lookup

class MemorySentenceLookup(tf.keras.layers.Layer):
    """
    Basic Memory Lookup layer. Query to memory cells similarity is computed either via doc product or via
    a FNN. The content vector is computed by weighting memory cells according to their corresponding computed
    similarities.

    Moreover, the layer is sensitive to strong supervision loss, since the latter is implemented via a max-margin loss
    on computed similarity distribution (generally a softmax).
    """

    def __init__(self,
                 reasoning_info,
                 memory_lookup_info,
                 samplewise_sampling=False,
                 dropout_rate=0.2,
                 l2_regularization=0.,
                 **kwargs):
        super(MemorySentenceLookup, self).__init__(**kwargs)
        self.memory_lookup_info = memory_lookup_info
        self.reasoning_info = reasoning_info
        self.dropout_rate = dropout_rate
        self.samplewise_sampling = samplewise_sampling

        if self.memory_lookup_info['mode'] == 'mlp':
            self.mlp_weights = []
            for weight in self.memory_lookup_info['weights']:
                self.mlp_weights.append(tf.keras.layers.Dense(units=weight,
                                                              activation=tf.tanh,
                                                              kernel_regularizer=tf.keras.regularizers.l2(
                                                                  l2_regularization)))
                self.mlp_weights.append(tf.keras.layers.Dropout(rate=dropout_rate))

            self.mlp_weights.append(tf.keras.layers.Dense(units=1,
                                                          activation=None,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))

    def compute_mask(self, inputs, mask=None):
        return mask

    def call(self, inputs, training=False, state='training', mask=None, **kwargs):
        # [batch_size, embedding_dim]
        query = inputs['query']

        # [batch_size, mem_size, embedding_dim]
        memory = inputs['memory']

        if self.memory_lookup_info['mode'] == 'dot_product':
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1] / memory.shape[-1], tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2)
        elif self.memory_lookup_info['mode'] == 'scaled_dot_product':
            # [batch_size, mem_size, embedding_dim]
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1].value / memory.shape[-1].value, tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2) * self.embedding_dimension ** 0.5
        elif self.memory_lookup_info['mode'] == 'mlp':
            # Memory efficient scenario
            if not self.samplewise_sampling:
                dotted = []
                for mem_id in range(memory.shape[1]):
                    # [batch_size, embedding_dim * 2]
                    mem_slice = tf.tile(memory[:, mem_id, :], [query.shape[0], 1])
                    mem_att_input = tf.concat((mem_slice, query), axis=-1)

                    mem_dotted = mem_att_input
                    for block in self.mlp_weights:
                        mem_dotted = block(mem_dotted, training=training)

                    dotted.append(mem_dotted)

                # [batch_size, mem_size]
                dotted = tf.stack(dotted, axis=-1)
            else:
                if self.reasoning_info['mode'] == 'concat':
                    query_dimension = query.shape[-1]
                    repeat_amount = query_dimension // memory.shape[-1]
                    memory = tf.tile(memory, multiples=[1, 1, repeat_amount])

                # [batch_size, mem_size, embedding_dim]
                repeated_query = tf.expand_dims(query, axis=1)
                repeated_query = tf.tile(repeated_query, [1, memory.shape[1], 1])

                # [batch_size, mem_size, embedding_dim * 2]
                att_input = tf.concat((memory, repeated_query), axis=-1)
                att_input = tf.reshape(att_input, [-1, att_input.shape[-1]])

                dotted = att_input
                for block in self.mlp_weights:
                    dotted = block(dotted, training=training)

            dotted = tf.reshape(dotted, [-1, memory.shape[1]])
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        # [batch_size, memory_size]
        return dotted


class MemoryTokenLookup(tf.keras.layers.Layer):
    """
    Basic Memory Lookup layer. Query to memory cells similarity is computed either via doc product or via
    a FNN. The content vector is computed by weighting memory cells according to their corresponding computed
    similarities.

    Moreover, the layer is sensitive to strong supervision loss, since the latter is implemented via a max-margin loss
    on computed similarity distribution (generally a softmax).
    """

    def __init__(self,
                 reasoning_info,
                 memory_lookup_info,
                 dropout_rate=0.2,
                 l2_regularization=0.,
                 **kwargs):
        super(MemoryTokenLookup, self).__init__(**kwargs)
        self.reasoning_info = reasoning_info
        self.memory_lookup_info = memory_lookup_info
        self.dropout_rate = dropout_rate

        if self.memory_lookup_info['mode'] == 'mlp':
            self.mlp_weights = []
            for weight in self.memory_lookup_info['weights']:
                self.mlp_weights.append(tf.keras.layers.Dense(units=weight,
                                                              activation=tf.nn.selu,
                                                              kernel_regularizer=tf.keras.regularizers.l2(
                                                                  l2_regularization)))
                self.mlp_weights.append(tf.keras.layers.Dropout(rate=dropout_rate))

            self.mlp_weights.append(tf.keras.layers.Dense(units=1,
                                                          activation=None,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              l2_regularization)))

    def call(self, inputs, training=False, state='training', mask=None, **kwargs):
        # [batch_size, query_tokens, embedding_dim]
        query = inputs['query']

        # [batch_size, memory_tokens, embedding_dim]
        memory = inputs['memory']

        if self.memory_lookup_info['mode'] == 'dot_product':
            # [batch_size, memory_tokens, query_tokens]
            dotted = tf.matmul(memory, query, transpose_b=True)
        elif self.memory_lookup_info['mode'] == 'cosine_similarity':
            normalized_query = tf.nn.l2_normalize(query, axis=-1)
            normalized_memory = tf.nn.l2_normalize(memory, axis=-1)
            dotted = tf.matmul(normalized_memory, normalized_query, transpose_b=True)
        elif self.memory_lookup_info['mode'] == 'mlp':
            # [batch_size, memory_tokens, query_tokens, embedding_dim]
            tiled_query = tf.tile(query[:, None, :, :], [1, memory.shape[1], 1, 1])
            tiled_memory = tf.tile(memory[:, :, None, :], [1, 1, query.shape[1], 1])

            # [batch_size, memory_tokens, query_tokens, 2 * embedding_dim]
            dotted = tf.concat((tiled_memory, tiled_query), axis=-1)
            for block in self.mlp_weights:
                dotted = block(dotted, training=training)

            # [batch_size, memory_tokens, query_tokens]
            dotted = tf.squeeze(dotted)
        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        # [batch_size, memory_size]
        return dotted


class BERTMemorySentenceLookup(MemorySentenceLookup):

    def __init__(self, **kwargs):
        super(BERTMemorySentenceLookup, self).__init__(**kwargs)
        self.CLS_token_id = 101
        self.SEP_token_id = 102

        if self.memory_lookup_info['mode'] == 'bert':
            self.score_mlp = tf.keras.layers.Dense(units=1,
                                                   activation=None)

    def call(self, inputs, training=False, state='training', mask=None, **kwargs):
        # [batch_size, embedding_dim]
        query = inputs['query']

        # [batch_size, mem_size, embedding_dim]
        memory = inputs['memory']

        if self.memory_lookup_info['mode'] == 'dot_product':
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1] / memory.shape[-1], tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2)
        elif self.memory_lookup_info['mode'] == 'scaled_dot_product':
            # [batch_size, mem_size, embedding_dim]
            q_temp = tf.expand_dims(query, axis=1)
            memory = tf.tile(input=memory,
                             multiples=[1, 1, tf.cast(query.shape[-1].value / memory.shape[-1].value, tf.int32)])
            dotted = tf.reduce_sum(memory * q_temp, axis=2) * self.embedding_dimension ** 0.5
        elif self.memory_lookup_info['mode'] == 'mlp':

            # Memory efficient scenario
            if memory.shape[0] == 1:
                dotted = []
                for mem_id in range(memory.shape[1]):
                    # [batch_size, embedding_dim * 2]
                    mem_slice = tf.tile(memory[:, mem_id, :], [query.shape[0], 1])
                    mem_att_input = tf.concat((mem_slice, query), axis=-1)

                    mem_dotted = mem_att_input
                    for block in self.mlp_weights:
                        mem_dotted = block(mem_dotted, training=training)

                    dotted.append(mem_dotted)

                # [batch_size, mem_size]
                dotted = tf.stack(dotted, axis=-1)
            else:
                if self.reasoning_info['mode'] == 'concat':
                    query_dimension = query.shape[-1]
                    repeat_amount = query_dimension // memory.shape[-1]
                    memory = tf.tile(memory, multiples=[1, 1, repeat_amount])

                # [batch_size, mem_size, embedding_dim]
                repeated_query = tf.expand_dims(query, axis=1)
                repeated_query = tf.tile(repeated_query, [1, memory.shape[1], 1])

                # [batch_size, mem_size, embedding_dim * 2]
                att_input = tf.concat((memory, repeated_query), axis=-1)
                att_input = tf.reshape(att_input, [-1, att_input.shape[-1]])

                dotted = att_input
                for block in self.mlp_weights:
                    dotted = block(dotted, training=training)

            dotted = tf.reshape(dotted, [-1, memory.shape[1]])
        elif self.memory_lookup_info['mode'] == 'bert':

            # In this scenario, inputs are still in token form (i.e. sequence of tokens)
            # query shape -> [batch_size, tokens] (for each input)
            # memory shape -> [1, mem_size, tokens] (for each input)

            query_input_ids = query['input_ids']
            query_attention_mask = query['attention_mask']

            memory_input_ids = memory['input_ids']
            memory_attention_mask = memory['attention_mask']

            bert_layer = inputs['bert_layer']

            # Create SEP tensor
            sep_input_ids = tf.convert_to_tensor([self.SEP_token_id], dtype=query_input_ids.dtype)[None, :]
            sep_input_ids = tf.tile(sep_input_ids, [query_input_ids.shape[0], 1])
            sep_attention_mask = tf.convert_to_tensor([1], dtype=query_attention_mask.dtype)[None, :]
            sep_attention_mask = tf.tile(sep_attention_mask, [query_input_ids.shape[0], 1])

            # Memory efficient scenario
            dotted = []
            for mem_id in range(memory_input_ids.shape[0]):
                # [batch_size, tokens]
                mem_input_ids_slice = memory_input_ids[mem_id, :]
                mem_attention_mask_slice = memory_attention_mask[mem_id, :]

                mem_input_ids_slice = tf.tile(mem_input_ids_slice[None, :], [query_input_ids.shape[0], 1])
                mem_attention_mask_slice = tf.tile(mem_attention_mask_slice[None, :],
                                                   [query_input_ids.shape[0], 1])

                # concatenate inputs for bert model (ignore second input [CLS] token)
                # [batch_size, tokens * 2 + 1]
                concat_input_ids = tf.concat((query_input_ids, sep_input_ids, mem_input_ids_slice[:, 1:]), axis=1)
                concat_attention_mask = tf.concat(
                    (query_attention_mask, sep_attention_mask, mem_attention_mask_slice[:, 1:]), axis=1)

                # [batch_size, dim]
                bert_output = bert_layer({
                    'input_ids': concat_input_ids,
                    'attention_mask': concat_attention_mask,
                })
                bert_output = bert_output[0]
                bert_output = bert_output[:, 0]

                # [batch_size, 1]
                mem_dotted = self.score_mlp(bert_output)
                dotted.append(mem_dotted)

            # [batch_size, mem_size]
            dotted = tf.stack(dotted, axis=-1)
            dotted = tf.reshape(dotted, [-1, memory_input_ids.shape[0]])

        else:
            raise RuntimeError('Invalid similarity operation! Got: {}'.format(self.memory_lookup_info['mode']))

        # [batch_size, memory_size]
        return dotted


class TaskSpecificAttention(tf.keras.layers.Layer):

    def __init__(self, projection_size, **kwargs):
        self.projection_block = tf.keras.layers.Dense(units=projection_size, activation=tf.leaky_relu)

    def call(self, inputs, training=False, state='training', **kwargs):
        value = inputs['value']
        query = inputs['query']

        value_projection = self.projection_block(value)
        query_projection = self.projection_block(query)

        attention = tf.matmul(value_projection, query_projection, transpose_b=True)

        return attention


class Windowing(tf.keras.layers.Layer):

    def __init__(self, window_size, window_step=1, pad_data=True, **kwargs):
        super(Windowing, self).__init__(**kwargs)

        self.window_size = window_size
        self.window_step = window_step
        self.pad_data = pad_data

    def call(self, inputs, training=False, state='training', **kwargs):
        # 3D tensor, frame second last dimension

        segments = tf.signal.frame(inputs,
                                   frame_length=self.window_size,
                                   frame_step=self.window_step,
                                   pad_end=self.pad_data,
                                   axis=1)

        return segments


# Extraction

class SentenceMemoryExtraction(tf.keras.layers.Layer):

    def __init__(self, extraction_info, partial_supervision_info, **kwargs):
        super(SentenceMemoryExtraction, self).__init__(**kwargs)
        self.extraction_info = extraction_info
        self.partial_supervision_info = partial_supervision_info

        self.supervision_loss = None

    def _add_supervision_loss(self, prob_dist, target_mask):
        target_mask = tf.cast(target_mask, prob_dist.dtype)

        # Divide by the number of positive examples
        positive_examples_mask = tf.reduce_sum(target_mask, axis=-1)
        positive_examples_mask = tf.minimum(positive_examples_mask, 1.0)
        positive_examples_mask = tf.reduce_sum(positive_examples_mask)
        positive_examples_mask = tf.maximum(positive_examples_mask, 1.0)

        positive_scores = prob_dist * target_mask
        negative_scores = prob_dist * (1. - target_mask)

        supervision_loss = negative_scores[:, None, :] + self.partial_supervision_info['margin'] - positive_scores[:, :,
                                                                                                   None]
        supervision_loss = tf.maximum(0., supervision_loss)

        normalization_factor = (1 - target_mask[:, None, :]) * target_mask[:, :, None]
        supervision_loss = supervision_loss * normalization_factor

        supervision_loss = tf.reduce_sum(supervision_loss, axis=[1, 2])

        normalization_factor = tf.reduce_sum(normalization_factor, axis=[1, 2])
        normalization_factor = tf.maximum(1.0, normalization_factor)

        supervision_loss = supervision_loss / normalization_factor
        return tf.reduce_sum(supervision_loss) / positive_examples_mask

    def _add_old_supervision_loss(self, prob_dist, positive_idxs, negative_idxs, mask_idxs):

        padding_amount = self.padding_amount

        # Repeat mask for each positive element in each sample memory
        # Mask_idxs shape: [batch_size, padding_amount]
        # Mask res shape: [batch_size * padding_amount, padding_amount]
        mask_res = tf.tile(mask_idxs, multiples=[1, padding_amount])
        mask_res = tf.reshape(mask_res, [-1, padding_amount, padding_amount])
        mask_res = tf.transpose(mask_res, [0, 2, 1])
        mask_res = tf.reshape(mask_res, [-1, padding_amount])

        # Split each similarity score for a target into a separate sample
        # similarities shape: [batch_size, memory_max_length]
        # positive_idxs shape: [batch_size, padding_amount]
        # gather_nd shape: [batch_size, padding_amount]
        # pos_scores shape: [batch_size * padding_amount, 1]
        pos_scores = tf.gather(prob_dist, positive_idxs, batch_dims=1)
        pos_scores = tf.reshape(pos_scores, [-1, 1])

        # Repeat similarity scores for non-target memories for each positive score
        # similarities shape: [batch_size, memory_max_length]
        # negative_idxs shape: [batch_size, padding_amount]
        # neg_scores shape: [batch_size * padding_amount, padding_amount]
        neg_scores = tf.gather(prob_dist, negative_idxs, batch_dims=1)
        neg_scores = tf.tile(neg_scores, multiples=[1, padding_amount])
        neg_scores = tf.reshape(neg_scores, [-1, padding_amount])

        # Compare each single positive score with all corresponding negative scores
        # [batch_size * padding_amount, padding_amount]
        # [batch_size, padding_amount]
        # [batch_size, 1]
        # Samples without supervision are ignored by applying a zero mask (mask_res)
        hop_supervision_loss = tf.maximum(0., self.partial_supervision_info['margin'] - pos_scores + neg_scores)
        hop_supervision_loss = hop_supervision_loss * tf.cast(mask_res, dtype=hop_supervision_loss.dtype)
        hop_supervision_loss = tf.reshape(hop_supervision_loss, [-1, padding_amount, padding_amount])

        hop_supervision_loss = tf.reduce_sum(hop_supervision_loss, axis=[1, 2])
        # hop_supervision_loss = tf.reduce_max(hop_supervision_loss, axis=1)
        normalization_factor = tf.cast(tf.reshape(mask_res, [-1, padding_amount, padding_amount]),
                                       hop_supervision_loss.dtype)
        normalization_factor = tf.reduce_sum(normalization_factor, axis=[1, 2])
        normalization_factor = tf.maximum(normalization_factor, tf.ones_like(normalization_factor))
        hop_supervision_loss = tf.reduce_sum(hop_supervision_loss / normalization_factor)

        # Normalize by number of unfair examples
        valid_examples = tf.reduce_sum(mask_idxs, axis=1)
        valid_examples = tf.cast(valid_examples, tf.float32)
        valid_examples = tf.minimum(valid_examples, 1.0)
        valid_examples = tf.reduce_sum(valid_examples)
        valid_examples = tf.maximum(valid_examples, 1.0)
        hop_supervision_loss = hop_supervision_loss / valid_examples

        return hop_supervision_loss

    def call(self, inputs, state='training', training=False, mask=None, **kwargs):

        similarities = inputs['similarities']
        context_mask = inputs['context_mask']

        context_mask = tf.cast(context_mask, similarities.dtype)
        similarities += (context_mask * -1e9)

        if self.extraction_info['mode'] == 'softmax':
            probs = tf.nn.softmax(similarities, axis=1)
        elif self.extraction_info['mode'] == 'sparsemax':
            probs = tfa.activations.sparsemax(similarities, axis=1)
        elif self.extraction_info['mode'] == 'sigmoid':
            probs = tf.nn.sigmoid(similarities)
        else:
            raise RuntimeError('Invalid extraction mode! Got: {}'.format(self.extraction_info['mode']))

        supervision_loss = None
        if self.partial_supervision_info['flag'] and state != 'prediction':
            supervision_loss = self._add_supervision_loss(prob_dist=probs,
                                                          target_mask=inputs['target_mask'])

        return probs, supervision_loss


class TokenMemoryExtraction(tf.keras.layers.Layer):

    def __init__(self, extraction_info, partial_supervision_info, **kwargs):
        super(TokenMemoryExtraction, self).__init__(**kwargs)
        self.extraction_info = extraction_info
        self.partial_supervision_info = partial_supervision_info

        self.supervision_loss = None

    def call(self, inputs, state='training', training=False, mask=None, **kwargs):
        # [batch_size, memory_tokens, query_tokens]
        similarities = inputs['similarities']

        # [batch_size, memory_tokens]
        memory_mask = inputs['memory_mask']
        memory_mask = tf.cast(memory_mask, similarities.dtype)

        # [batch_size, query_tokens]
        query_mask = inputs['query_mask']
        query_mask = tf.cast(query_mask, similarities.dtype)

        if self.extraction_info['mode'] == 'softmax':
            memory_probs = masked_softmax(logits=similarities, mask=memory_mask[:, :, None], axis=-1)
            query_probs = masked_softmax(logits=similarities, mask=query_mask[:, None, :], axis=1)
        elif self.extraction_info['mode'] == 'sparsemax':
            memory_probs = tfa.activations.sparsemax(similarities, axis=2)
            query_probs = tfa.activations.sparsemax(similarities, axis=1)
        else:
            raise RuntimeError('Invalid extraction mode! Got: {}'.format(self.extraction_info['mode']))

        return memory_probs, query_probs


# Reasoning

class SentenceMemoryReasoning(tf.keras.layers.Layer):
    """
    Basic Memory Reasoning layer. The new query is computed simply by summing the content vector to current query
    """

    def __init__(self, reasoning_info, **kwargs):
        super(SentenceMemoryReasoning, self).__init__(**kwargs)
        self.reasoning_info = reasoning_info

    def call(self, inputs, training=False, state='training', **kwargs):
        query = inputs['query']
        memory_search = inputs['memory_search']

        if self.reasoning_info['mode'] == 'sum':
            upd_query = query + memory_search
        elif self.reasoning_info['mode'] == 'concat':
            upd_query = tf.concat((query, memory_search), axis=1)
        elif self.reasoning_info['mode'] == 'rnn':
            cell = tf.keras.layers.GRUCell(query.shape[-1])
            upq_query, _ = cell(memory_search, [query])
        elif self.reasoning_info['mode'] == 'mlp':
            upd_query = tf.keras.layers.Dense(query.shape[-1],
                                              activation=tf.nn.relu)(tf.concat((query, memory_search), axis=1))
        else:
            raise RuntimeError(
                'Invalid aggregation mode! Got: {} -- Supported: [sum, concat]'.format(self.reasoning_info['mode']))

        return upd_query


class TokenMemoryReasoning(tf.keras.layers.Layer):
    """
    Basic Memory Reasoning layer. The new query is computed simply by summing the content vector to current query
    """

    def __init__(self, reasoning_info, **kwargs):
        super(TokenMemoryReasoning, self).__init__(**kwargs)
        self.reasoning_info = reasoning_info

    def call(self, inputs, training=False, state='training', **kwargs):
        # [batch_size, query_tokens, embedding_dim]
        query = inputs['query']

        # [batch_size, memory_tokens, embedding_dim]
        memory = inputs['memory']

        # [batch_size, memory_tokens, query_tokens]
        # distribution over memory tokens
        query_probs = inputs['query_probs']

        # [batch_size, memory_tokens, query_tokens]
        # distribution over query tokens
        memory_probs = inputs['memory_probs']

        # [batch_size, memory_tokens, embedding_dim]
        query_search = tf.matmul(memory_probs, query)

        # [batch_size, query_tokens, embedding_dim]
        memory_search = tf.matmul(query_probs, memory, transpose_a=True)

        if self.reasoning_info['mode'] == 'sum':
            upd_query = query + memory_search
            upd_memory = memory + query_search
        elif self.reasoning_info['mode'] == 'rnn':
            query_cell = tf.keras.layers.GRUCell(query.shape[-1])
            upd_query, _ = query_cell(memory_search, [query])

            memory_cell = tf.keras.layers.GRUCell(memory.shape[-1])
            upd_memory, _ = memory_cell(query_search, [memory])
        elif self.reasoning_info['mode'] == 'mlp':
            upd_query = tf.keras.layers.Dense(query.shape[-1],
                                              activation=tf.nn.selu)(tf.concat((query, memory_search), axis=-1))

            upd_memory = tf.keras.layers.Dense(memory.shape[-1],
                                               activation=tf.nn.selu)(tf.concat((memory, query_search)), axis=-1)
        else:
            raise RuntimeError(
                'Invalid aggregation mode! Got: {} -- Supported: [sum, rnn, mlp]'.format(self.reasoning_info['mode']))

        return upd_query, upd_memory


# TODO: move to tensorflow_utils_v2
def sparse_softmax(tensor, mask):
    """
    Sparse softmax that ignores padding values (0s).
    """

    indexes = tf.where(mask)
    sparse_tensor = tf.SparseTensor(indexes, tf.gather_nd(tensor, indexes), tensor.get_shape())
    sparse_probs = tf.sparse.softmax(sparse_tensor)
    dense_probs = tf.sparse.to_dense(sparse_probs)

    return dense_probs


# Utility Layers

class HeadAttention(tf.keras.layers.Layer):

    def __init__(self, segments_amount, head_attention, embedding_dimension, attention_dimension,
                 use_masking=False, **kwargs):
        super(HeadAttention, self).__init__(**kwargs)
        self.head_attention = head_attention
        self.embedding_dimension = embedding_dimension
        self.attention_dimension = attention_dimension
        self.segments_amount = segments_amount
        self.use_masking = use_masking

        self.reduction_layers = [tf.keras.layers.Dense(units=self.attention_dimension, activation=tf.nn.leaky_relu),
                                 tf.keras.layers.Dense(units=self.embedding_dimension)]

    def call(self, x):

        # [batch_size, seq_length, embedding_dim]

        # [batch_size, seq_length, embedding_dim]
        reduction_input = x
        for reduction_layer in self.reduction_layers:
            reduction_input = reduction_layer(reduction_input)

        # [batch_size, embedding_dim]
        if self.use_masking:
            mask_width = x.shape[1] // self.segments_amount
            indexes = tf.range(0, x.shape[1])
            desired_shape = [x.shape[0], x.shape[1]]
            mask_lower = tf.where(indexes >= mask_width * self.head_attention,
                                  tf.ones(desired_shape), tf.zeros(desired_shape))
            mask_upper = tf.where(indexes < (mask_width * self.head_attention) + mask_width,
                                  tf.ones(desired_shape), tf.zeros(desired_shape))
            mask = mask_lower * mask_upper
            mask = tf.expand_dims(mask, axis=-1)
            mask = tf.tile(mask, multiples=[1, 1, self.embedding_dimension])
            reduction_weights = sparse_softmax(tf.transpose(reduction_input, [0, 2, 1]),
                                               tf.transpose(mask, [0, 2, 1]))
            reduction_weights = tf.transpose(reduction_weights, [0, 2, 1])
        else:
            reduction_weights = tf.nn.softmax(reduction_input, axis=1)
        pooled_embeddings = tf.reduce_sum(x * reduction_weights, axis=1)

        return pooled_embeddings


class GeneralizedPooling(tf.keras.layers.Layer):
    def __init__(self, segments_amount, attention_dimension, embedding_dimension, use_masking=False, **kwargs):
        super(GeneralizedPooling, self).__init__(*kwargs)

        self.segments_amount = segments_amount
        self.attention_dimension = attention_dimension
        self.embedding_dimension = embedding_dimension
        self.use_masking = use_masking

    def _compute_disagreement_penalization(self, segment_embeddings):
        segment_embeddings = tf.math.l2_normalize(segment_embeddings, axis=2)

        # [batch_size, segments_amount, segments_amount]
        cosine_distance = tf.matmul(segment_embeddings, segment_embeddings, transpose_b=True)

        # [batch_size, ]
        cosine_distance = tf.reduce_sum(cosine_distance, axis=2)
        cosine_distance = tf.reduce_sum(cosine_distance, axis=1)

        # [batch_size, ]
        cosine_distance = cosine_distance / (self.segments_amount * self.segments_amount)

        return cosine_distance

    def call(self, x, **kwargs):
        # [batch_size, seq_length, embedding_dim]

        def condition(index, stack, segments_amount, head_attention,
                      embedding_dimension, attention_dimension, use_masking=False):
            return tf.less(index, self.segments_amount)

        def body(index, stack, segments_amount, head_attention,
                 embedding_dimension, attention_dimension, use_masking=False):
            pooled_embeddings = HeadAttention(segments_amount=segments_amount,
                                              head_attention=head_attention,
                                              embedding_dimension=embedding_dimension,
                                              attention_dimension=attention_dimension,
                                              use_masking=use_masking)(x)
            stack = stack.write(index, pooled_embeddings)
            index = tf.add(index, 1)
            head_attention = head_attention + 1
            return index, stack, segments_amount, head_attention, embedding_dimension, attention_dimension, use_masking

        temp_stack = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
        result = tf.while_loop(condition,
                               body,
                               [tf.constant(0),
                                temp_stack,
                                self.segments_amount,
                                tf.constant(0),
                                self.embedding_dimension,
                                self.attention_dimension,
                                self.use_masking])
        segment_embeddings = result[1].stack()

        # [batch_size, segments_amount, embedding_dim]
        segment_embeddings = tf.transpose(segment_embeddings, [1, 0, 2])

        if self.segments_amount > 1:
            self.disagreement_penalization = self._compute_disagreement_penalization(segment_embeddings)

        return segment_embeddings


class AdditiveAttention(tf.keras.layers.Layer):

    def __init__(self, units=128, **kwargs):
        super(AdditiveAttention, self).__init__(**kwargs)

        self.value_dense = tf.keras.layers.Dense(units=units, use_bias=True)
        self.query_dense = tf.keras.layers.Dense(units=units, use_bias=False)
        self.attention_dense = tf.keras.layers.Dense(units=1, use_bias=False)

    def call(self, inputs, **kwargs):
        # [batch_size, embedding_dim]
        query = inputs['query']

        # [batch_size, sequence_length, embedding_dim]
        value = inputs['value']

        # [batch_size, sequence_length]
        additive_attention = tf.nn.tanh(self.value_dense(value) + tf.expand_dims(self.query_dense(query), axis=1))
        additive_attention = self.attention_dense(additive_attention)
        additive_attention = tf.squeeze(additive_attention)
        additive_attention = tf.nn.softmax(additive_attention, axis=1)

        # [batch_size, embedding_dim]
        attention_vector = tf.reduce_sum(value * tf.expand_dims(additive_attention, -1), axis=1)

        return attention_vector


# BiDAF Layers


class BiDAFAttention(tf.keras.layers.Layer):

    def __init__(self, hidden_size, dropout_rate=0.1, **kwargs):
        super(BiDAFAttention, self).__init__(**kwargs)
        self.hidden_size = hidden_size
        self.dropout_rate = dropout_rate

        self.c_weight = tf.keras.layers.Dense(units=1, use_bias=False)
        self.q_weight = tf.keras.layers.Dense(units=1, use_bias=False)
        self.cq_weight = tf.Variable(tf.random_uniform_initializer()(shape=(1, 1, hidden_size)),
                                     dtype=tf.float32, name='cq_weight')
        self.bias = tf.Variable(initial_value=tf.constant_initializer([0.])(shape=(1,)),
                                dtype=tf.float32, name='bidaf_att_bias')

        self.dropout = tf.keras.layers.Dropout(rate=dropout_rate)

    def _get_similarity_matrix(self, query, context, training=False):
        query = self.dropout(query, training=training)
        context = self.dropout(context, training=training)

        # [batch_size, max_context_length]
        s0 = self.c_weight(context)

        # [batch_size, max_seq_length]
        s1 = self.q_weight(query)
        s1 = tf.transpose(s1, [0, 2, 1])

        # [batch_size, max_context_length, max_seq_length
        s2 = tf.matmul(context * self.cq_weight, query, transpose_b=True)

        return s0 + s1 + s2 + self.bias

    def call(self, inputs, training=False, state='training', **kwargs):
        # [batch_size, max_context_length, 2 * hidden_size]
        query = inputs['query']

        # [batch_size, max_seq_length]
        query_mask = inputs['query_mask']

        # [batch_size, max_context_length, 2 * hidden_size]
        context = inputs['context']

        # [batch_size, max_seq_length]
        context_mask = inputs['context_mask']

        # [batch_size, max_context_length, max_seq_length]
        similarity = self._get_similarity_matrix(query, context, training=training)

        # [batch_size, max_context_length, max_seq_length]
        s1 = masked_softmax(similarity, query_mask[:, None, :], axis=2, log_softmax=False)

        # [batch_size, max_context_length, max_seq_length]
        s2 = masked_softmax(similarity, context_mask[:, :, None], axis=1, log_softmax=False)

        # [batch_size, max_context_length, 2 * hidden_size]
        a = tf.matmul(s1, query)

        # [batch_size, max_context_length, 2 * hidden_size]
        b = tf.matmul(tf.matmul(s1, s2, transpose_b=True), context)

        # [batch_size, max_context_length, 8 * hidden_size]
        x = tf.concat((context, a, context * a, context * b), axis=-1)
        return x


class BiDAFOutput(tf.keras.layers.Layer):

    def __init__(self, hidden_size, dropout_rate=0.1, **kwargs):
        super(BiDAFOutput, self).__init__(**kwargs)
        self.att_linear_1 = tf.keras.layers.Dense(units=1)
        self.mod_linear_1 = tf.keras.layers.Dense(units=1)

        self.rnn = tf.keras.layers.Bidirectional(tf.keras.layers.GRU(units=hidden_size,
                                                                     return_sequences=True))
        self.dropout = tf.keras.layers.Dropout(rate=dropout_rate)

        self.att_linear_2 = tf.keras.layers.Dense(units=1, activation=None)
        self.mod_linear_2 = tf.keras.layers.Dense(units=1, activation=None)

    def call(self, inputs, training=False, state='training', **kwargs):
        # [batch_size, max_context_length, 8 * hidden_size]
        bidaf_attention = inputs['bidaf_attention']

        # [batch_size, max_context_length, 2 * hidden_size]
        encoded_output = inputs['encoded_output']

        # [batch_size, max_context_length]
        context_mask = inputs['context_mask']

        logits_1 = self.att_linear_1(bidaf_attention) + self.mod_linear_1(encoded_output)
        logits_1 = tf.squeeze(logits_1)

        mod_2 = self.rnn(encoded_output)
        mod_2 = self.dropout(mod_2, training=training)

        logits_2 = self.att_linear_2(bidaf_attention) + self.mod_linear_2(mod_2)
        logits_2 = tf.squeeze(logits_2)

        answer_start_probs = masked_softmax(logits_1, context_mask, axis=-1, log_softmax=False)
        answer_end_probs = masked_softmax(logits_2, context_mask, axis=-1, log_softmax=False)

        return answer_start_probs, answer_end_probs


######################################################
####################### BASELINE #####################
######################################################


class M_Baseline_LSTM(tf.keras.Model):
    """
    LSTM baseline for ToS Task 1. This is a simple stacked-LSTMs model.
    """

    def __init__(self, sentence_size, vocab_size, lstm_weights,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None, num_labels=1, num_classes_per_label=None):
        super(M_Baseline_LSTM, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.lstm_weights = lstm_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.lstm_blocks = []
        for weight in self.lstm_weights[:-1]:
            self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(weight,
                                                                                       return_sequences=True,
                                                                                       kernel_regularizer=tf.keras.regularizers.l2(
                                                                                           self.l2_regularization))))
            self.lstm_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.lstm_blocks.append(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(self.lstm_weights[-1],
                                                                                   return_sequences=False,
                                                                                   kernel_regularizer=tf.keras.regularizers.l2(
                                                                                       self.l2_regularization))))

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization))
                                for key, value in self.num_classes_per_label.items()}

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        lstm_input = sentence_emb
        for block in self.lstm_blocks:
            lstm_input = block(lstm_input, training=training)

        answer = lstm_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        # Multi-label
        if self.num_labels == 1:
            answer = self.final_block(answer, training=training)
        else:
            answer = {'label_id_{}'.format(key): block(answer, training=training) for key, block in
                      self.final_block.items()}

        return {
                   'Q_Only': answer
               }, None


class M_Baseline_CNN(tf.keras.Model):
    """
    CNN baseline for ToS Task 1. This is a simple stacked-CNNs model.
    """

    def __init__(self, sentence_size, vocab_size, cnn_weights,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None, num_labels=1, num_classes_per_label=None):
        super(M_Baseline_CNN, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.cnn_weights = cnn_weights
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.cnn_blocks = []
        for filters, kernel in self.cnn_weights:
            self.cnn_blocks.append(tf.keras.layers.Conv1D(filters,
                                                          kernel,
                                                          kernel_regularizer=tf.keras.regularizers.l2(
                                                              self.l2_regularization)))
            self.cnn_blocks.append(tf.keras.layers.MaxPool1D())
            self.cnn_blocks.append(tf.keras.layers.Dropout(rate=self.dropout_rate))

        self.cnn_blocks.append(tf.keras.layers.Flatten())

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization))
                                for key, value in self.num_classes_per_label.items()}

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        cnn_input = sentence_emb
        for block in self.cnn_blocks:
            cnn_input = block(cnn_input, training=training)

        answer = cnn_input
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        # Multi-label
        if self.num_labels == 1:
            answer = self.final_block(answer, training=training)
        else:
            answer = {'label_id_{}'.format(key): block(answer, training=training) for key, block in
                      self.final_block.items()}

        return {
                   'Q_Only': answer
               }, None


class M_Baseline_Sum(tf.keras.Model):

    def __init__(self, sentence_size, vocab_size,
                 answer_weights, embedding_dimension,
                 l2_regularization=0., dropout_rate=0.2,
                 embedding_matrix=None, num_labels=1, num_classes_per_label=None):
        super(M_Baseline_Sum, self).__init__()
        self.sentence_size = sentence_size
        self.vocab_size = vocab_size
        self.answer_weights = answer_weights
        self.l2_regularization = l2_regularization
        self.dropout_rate = dropout_rate
        self.embedding_dimension = embedding_dimension

        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

        self.input_embedding = tf.keras.layers.Embedding(input_dim=vocab_size,
                                                         output_dim=embedding_dimension,
                                                         input_length=sentence_size,
                                                         weights=embedding_matrix,
                                                         mask_zero=True,
                                                         name='query_embedding')

        self.answer_blocks = []
        for weight in answer_weights:
            self.answer_blocks.append(tf.keras.layers.Dense(units=weight,
                                                            activation=tf.nn.leaky_relu,
                                                            kernel_regularizer=tf.keras.regularizers.l2(
                                                                l2_regularization)))
            self.answer_blocks.append(tf.keras.layers.Dropout(rate=dropout_rate))

        if self.num_labels == 1:
            self.final_block = tf.keras.layers.Dense(units=self.num_classes_per_label,
                                                     kernel_regularizer=tf.keras.regularizers.l2(l2_regularization))
        else:
            self.final_block = {key: tf.keras.layers.Dense(units=value,
                                                           kernel_regularizer=tf.keras.regularizers.l2(
                                                               l2_regularization))
                                for key, value in self.num_classes_per_label.items()}

    def call(self, inputs, training=False, **kwargs):
        sentence = inputs['text']

        sentence_emb = self.input_embedding(sentence)

        sentence_encoding = tf.reduce_sum(sentence_emb, axis=1)

        answer = sentence_encoding
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        # Multi-label
        if self.num_labels == 1:
            answer = self.final_block(answer, training=training)
        else:
            answer = {'label_id_{}'.format(key): block(answer, training=training) for key, block in
                      self.final_block.items()}

        return {
            'Q_Only': answer
               }, None


class M_Pairwise_Baseline_Sum(M_Baseline_Sum):

    def call(self, inputs, training=False, **kwargs):
        # Text encoding
        sentence = inputs['text']
        sentence_emb = self.input_embedding(sentence)
        sentence_encoding = tf.reduce_sum(sentence_emb, axis=1)

        # Explanation encoding
        explanation = inputs['explanation']
        explanation_emb = self.input_embedding(explanation)
        explanation_encoding = tf.reduce_sum(explanation_emb, axis=1)

        answer = tf.concat((sentence_encoding, explanation_encoding), axis=-1)
        for block in self.answer_blocks:
            answer = block(answer, training=training)

        # Multi-label
        if self.num_labels == 1:
            answer = self.final_block(answer, training=training)
        else:
            answer = {'label_id_{}'.format(key): block(answer, training=training) for key, block in
                      self.final_block.items()}

        return {
            'Q_Only': answer,
            'example_id': inputs['example_id']
               }, None
