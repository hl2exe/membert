import os

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from sample_wrappers import Features, TextExampleList, TextKBExampleList, FeatureFactory
from collections import OrderedDict
from utility.log_utils import get_logger

logger = get_logger(__name__)


class BaseConverter(object):

    def __init__(self, feature_class, max_tokens_limit=None):
        self.feature_class = FeatureFactory.supported_features[feature_class]
        self.max_tokens_limit = max_tokens_limit if max_tokens_limit else 100000

    def get_instance_args(self):
        return {
            'max_tokens_limit': self.max_tokens_limit
        }

    def convert_data(self, examples, tokenizer, label_list, output_file, checkpoint=None,
                     is_training=False, has_labels=True):

        assert issubclass(self.feature_class, Features)

        if is_training:
            logger.info('Retrieving training set info...this may take a while...')
            self.training_preparation(examples=examples,
                                      label_list=label_list,
                                      tokenizer=tokenizer)

        example_conversion_method = self.feature_class.from_example
        example_feature_method = self.feature_class.get_feature_records

        writer = tf.io.TFRecordWriter(output_file)

        for ex_index, example in enumerate(tqdm(examples, leave=True, position=0)):
            if checkpoint is not None and ex_index % checkpoint == 0:
                logger.info('Writing example {0} of {1}'.format(ex_index, len(examples)))

            feature = example_conversion_method(example,
                                                label_list,
                                                has_labels=has_labels,
                                                tokenizer=tokenizer,
                                                conversion_args=self.get_conversion_args(),
                                                converter_args=self.get_instance_args(),
                                                wrapper_state=examples.get_added_state())

            features = example_feature_method(feature)

            tf_example = tf.train.Example(features=tf.train.Features(feature=features))
            writer.write(tf_example.SerializeToString())

        writer.close()

    def get_conversion_args(self):
        return {
            'max_seq_length': self.max_seq_length,
            'label_map': self.label_map,
            'num_labels': self.num_labels,
            'num_classes_per_label': self.num_classes_per_label,
            'feature_class': self.feature_class
        }

    def _retrieve_default_label_info(self, label_id):
        num_labels = None
        num_classes_per_label = None

        if label_id is not None:

            if type(label_id) == OrderedDict:
                num_labels = len(label_id)
                num_classes_per_label = {key: len(value) for key, value in label_id.items()}
            else:
                num_labels = 1
                num_classes_per_label = len(label_id)

        return num_labels, num_classes_per_label

    # TODO: we can do better here by defining attribute-defining functions: one for each conversion_args key
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args(),
                                                 wrapper_state=examples.get_added_state())
            text_ids, label_id, label_map = features
            features_ids_len = len(text_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.label_map = label_map
        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label

    def save_conversion_args(self, filepath, prefix=None):
        if prefix:
            filename = 'converter_info_{}.npy'.format(prefix)
        else:
            filename = 'converter_info.npy'
        filepath = os.path.join(filepath, filename)
        np.save(filepath, self.get_conversion_args())

    @staticmethod
    def load_conversion_args(filepath, prefix=None):
        if prefix:
            filename = 'converter_info_{}.npy'.format(prefix)
        else:
            filename = 'converter_info.npy'
        filepath = os.path.join(filepath, filename)
        return np.load(filepath, allow_pickle=True).item()


class PairwiseConverter(BaseConverter):

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args(),
                                                 wrapper_state=examples.get_added_state())
            text_ids, explanation_ids, label_id, label_map = features
            features_ids_len = max(len(text_ids), len(explanation_ids))

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.label_map = label_map
        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label


class KBConverter(BaseConverter):

    def get_conversion_args(self):
        conversion_args = super(KBConverter, self).get_conversion_args()
        conversion_args['kb_length'] = self.kb_length
        conversion_args['max_kb_length'] = self.max_kb_length
        conversion_args['max_kb_seq_length'] = self.max_kb_seq_length
        conversion_args['kb'] = self.kb

        return conversion_args

    def _convert_kb(self, tokenizer, examples):
        kb = self.feature_class.convert_wrapper_state(tokenizer=tokenizer,
                                                      wrapper_state=examples.get_added_state(),
                                                      converter_args=self.get_instance_args())
        self.kb = kb

        self.kb_length = OrderedDict([(key, len(value['kb_ids'])) for key, value in self.kb.items()])
        self.max_kb_length = sum([value for value in self.kb_length.values()])
        self.max_kb_seq_length = {key: max([len(item) for item in value['kb_ids']]) for key, value in self.kb.items()}
        self.max_kb_seq_length = max(list(self.max_kb_seq_length.values()))
        self.max_kb_seq_length = min(self.max_kb_seq_length, self.max_tokens_limit)

        examples.add_state('kb_length', self.kb_length)
        examples.add_state('max_kb_length', self.max_kb_length)

    # Computing max text length and kb info
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        # ListWrapper state

        self._convert_kb(tokenizer=tokenizer, examples=examples)

        # Examples

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None
        for example in tqdm(examples):

            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args(),
                                                          wrapper_state=examples.get_added_state())
            text_ids, label_id, label_map = features

            features_ids_len = len(text_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = min(max(max_seq_length, max_seq_length), self.max_tokens_limit)
        self.num_labels = num_labels
        self.label_map = label_map
        self.num_classes_per_label = num_classes_per_label


class KBSupervisionConverter(KBConverter):

    def get_conversion_args(self):
        conversion_args = super(KBSupervisionConverter, self).get_conversion_args()
        conversion_args['kb_frequencies'] = self.kb_frequencies
        return conversion_args

    # Computing max text length, kb_info and supervision targets
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextKBExampleList)

        # ListWrapper state

        self._convert_kb(tokenizer=tokenizer, examples=examples)

        # Examples

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None
        kb_frequencies = None

        for example in tqdm(examples):
            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args(),
                                                          wrapper_state=examples.get_added_state())
            text_ids, target_ids, label_id, label_map = features

            if kb_frequencies is None:
                kb_frequencies = np.array(target_ids)
            else:
                kb_frequencies += np.array(target_ids)

            features_ids_len = len(text_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = min(max(max_seq_length, max_seq_length), self.max_tokens_limit)
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label
        self.label_map = label_map
        self.kb_frequencies = kb_frequencies / np.sum(kb_frequencies)


class BertConverter(BaseConverter):

    def __init__(self, mask_padding_with_zero=True, pad_on_left=False,
                 pad_token=0, pad_token_segment_id=0, **kwargs):
        super(BertConverter, self).__init__(**kwargs)
        self.mask_padding_with_zero = mask_padding_with_zero
        self.pad_on_left = pad_on_left
        self.pad_token = pad_token
        self.pad_token_segment_id = pad_token_segment_id

    def get_instance_args(self):
        instance_args = super(BertConverter, self).get_instance_args()
        instance_args['mask_padding_with_zero'] = self.mask_padding_with_zero
        instance_args['pad_on_left'] = self.pad_on_left
        instance_args['pad_token'] = self.pad_token
        instance_args['pad_token_segment_id'] = self.pad_token_segment_id

        return instance_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None

        for example in tqdm(examples):

            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args(),
                                                          wrapper_state=examples.get_added_state())
            input_ids, attention_mask, label_id, label_map = features
            features_ids_len = len(input_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.label_map = label_map
        self.num_classes_per_label = num_classes_per_label


class KBBertConverter(BertConverter):

    def get_conversion_args(self):
        conversion_args = super(KBBertConverter, self).get_conversion_args()
        conversion_args['kb_length'] = self.kb_length
        conversion_args['max_kb_length'] = self.max_kb_length
        conversion_args['max_kb_seq_length'] = self.max_kb_seq_length
        conversion_args['kb'] = self.kb

        return conversion_args

    def _convert_kb(self, tokenizer, examples):
        kb = self.feature_class.convert_wrapper_state(
            tokenizer=tokenizer,
            wrapper_state=examples.get_added_state(),
            converter_args=self.get_instance_args())
        self.kb = kb

        self.kb_length = OrderedDict([(key, len(value['kb_input_ids'])) for key, value in self.kb.items()])
        self.max_kb_length = sum([value for value in self.kb_length.values()])
        self.max_kb_seq_length = {key: max([len(item) for item in value['kb_input_ids']]) for key, value in
                                  self.kb.items()}
        self.max_kb_seq_length = max(list(self.max_kb_seq_length.values()))
        self.max_kb_seq_length = min(self.max_kb_seq_length, self.max_tokens_limit)

        examples.add_state('kb_length', self.kb_length)
        examples.add_state('max_kb_length', self.max_kb_length)

    # Computing max text length and kb info
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextKBExampleList)

        # ListWrapper state

        self._convert_kb(tokenizer=tokenizer, examples=examples)

        # Examples

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None

        for example in tqdm(examples):

            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args(),
                                                          wrapper_state=examples.get_added_state())
            input_ids, attention_mask, \
            label_id, label_map = features

            features_ids_len = len(input_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label
        self.label_map = label_map


class KBSupervisionBertConverter(KBBertConverter):

    # Computing max text length and kb info
    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextKBExampleList)

        # ListWrapper state

        self._convert_kb(tokenizer=tokenizer, examples=examples)

        # Examples

        max_seq_length = None
        num_labels = None
        num_classes_per_label = None

        for example in tqdm(examples):
            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args(),
                                                          wrapper_state=examples.get_added_state())
            input_ids, attention_mask, \
            target_ids, label_id, label_map = features

            features_ids_len = len(input_ids)

            num_labels, num_classes_per_label = self._retrieve_default_label_info(label_id)

            if max_seq_length is None:
                max_seq_length = features_ids_len
            elif max_seq_length < features_ids_len <= self.max_tokens_limit:
                max_seq_length = features_ids_len

        self.max_seq_length = max_seq_length
        self.num_labels = num_labels
        self.num_classes_per_label = num_classes_per_label
        self.label_map = label_map


class RationaleConverter(BaseConverter):

    def get_conversion_args(self):
        conversion_args = super(RationaleConverter, self).get_conversion_args()
        conversion_args['max_doc_length'] = self.max_doc_length

        return conversion_args

    def get_instance_args(self):
        instance_args = super(RationaleConverter, self).get_instance_args()
        return instance_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        max_sentence_length = None
        max_doc_length = None
        num_labels = None
        for example in tqdm(examples):

            is_valid_example = True

            features = self.feature_class.convert_example(example, label_list, tokenizer=tokenizer,
                                                          converter_args=self.get_instance_args())
            doc_ids, target_ids, label_id, label_map = features

            doc_len = len(doc_ids)
            sentence_length = max([len(item) for item in doc_ids])

            if label_id is not None:
                num_labels = len(label_id)

            if sentence_length > self.max_tokens_limit:
                continue

            if max_sentence_length is None:
                max_sentence_length = sentence_length
            elif max_sentence_length < sentence_length:
                max_sentence_length = sentence_length

            if max_doc_length is None:
                max_doc_length = doc_len
            elif max_doc_length < doc_len:
                max_doc_length = doc_len

        self.max_seq_length = max_sentence_length
        self.max_doc_length = max_doc_length
        self.num_labels = num_labels
        self.label_map = label_map


class SquadTextConverter(BaseConverter):

    def __init__(self, max_context_length, **kwargs):
        super(SquadTextConverter, self).__init__(**kwargs)
        self.max_context_length = max_context_length

    def get_instance_args(self):
        instance_args = super(SquadTextConverter, self).get_instance_args()
        instance_args['max_context_length'] = self.max_context_length
        return instance_args

    def get_conversion_args(self):
        return {
            'max_seq_length': self.max_seq_length,
            'max_context_length': self.max_context_length,
            'feature_class': self.feature_class
        }

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        max_context_length = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args(),
                                                 wrapper_state=examples.get_added_state())
            context_token_ids, question_token_ids, answer_starts, answer_ends, answer_texts = features

            context_length = len(context_token_ids)

            features_ids_length = max(context_length, len(question_token_ids))

            if max_seq_length is None:
                max_seq_length = features_ids_length
            elif max_seq_length < features_ids_length <= self.max_tokens_limit:
                max_seq_length = features_ids_length

            if max_context_length is None:
                max_context_length = context_length
            elif max_context_length < context_length <= self.max_context_length:
                max_context_length = context_length

        self.max_seq_length = min(max_seq_length, self.max_tokens_limit)
        self.max_context_length = min(max_context_length, self.max_context_length)


class BertSquadTextConverter(SquadTextConverter):

    def __init__(self, mask_padding_with_zero=True, pad_on_left=False,
                 pad_token=0, pad_token_segment_id=0, **kwargs):
        super(BertSquadTextConverter, self).__init__(**kwargs)
        self.mask_padding_with_zero = mask_padding_with_zero
        self.pad_on_left = pad_on_left
        self.pad_token = pad_token
        self.pad_token_segment_id = pad_token_segment_id

    def get_instance_args(self):
        instance_args = super(BertSquadTextConverter, self).get_instance_args()
        instance_args['mask_padding_with_zero'] = self.mask_padding_with_zero
        instance_args['pad_on_left'] = self.pad_on_left
        instance_args['pad_token'] = self.pad_token
        instance_args['pad_token_segment_id'] = self.pad_token_segment_id

        return instance_args

    def training_preparation(self, examples, label_list, tokenizer):

        assert isinstance(examples, TextExampleList)

        example_conversion_method = self.feature_class.convert_example

        max_seq_length = None
        max_context_length = None
        for example in tqdm(examples):
            features = example_conversion_method(example, label_list, tokenizer,
                                                 converter_args=self.get_instance_args(),
                                                 wrapper_state=examples.get_added_state())
            context_ids, context_attention_mask,\
            question_ids, question_attention_mask, answer_starts, answer_ends, answer_texts = features

            context_length = len(context_ids)
            features_ids_length = len(question_ids)

            if max_seq_length is None:
                max_seq_length = features_ids_length
            elif max_seq_length < features_ids_length <= self.max_tokens_limit:
                max_seq_length = features_ids_length

            if max_context_length is None:
                max_context_length = context_length
            elif max_context_length < context_length <= self.max_context_length:
                max_context_length = context_length

        self.max_seq_length = min(max_seq_length, self.max_tokens_limit)
        self.max_context_length = min(max_context_length, self.max_context_length)


class DataConverterFactory(object):
    supported_data_converters = {
        'base_converter': BaseConverter,
        'pairwise_text_converter': PairwiseConverter,
        'kb_converter': KBConverter,
        'kb_supervision_converter': KBSupervisionConverter,

        'bert_converter': BertConverter,
        'kb_bert_converter': KBBertConverter,
        'kb_supervision_bert_converter': KBSupervisionBertConverter,

        'rationale_cnn_converter': RationaleConverter,

        'squad_text_converter': SquadTextConverter,
        'bert_squad_text_converter': BertSquadTextConverter
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if DataConverterFactory.supported_data_converters[key]:
            return DataConverterFactory.supported_data_converters[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
