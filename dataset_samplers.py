import tensorflow as tf
import numpy as np


class Sampler(object):

    def __init__(self, max_memory_size, update_rate=1, samplewise_sampling=False):
        self.max_memory_size = max_memory_size
        self.kb = None
        self.kb_priority = None
        self.kb_size = None
        self.update_rate = update_rate
        self.current_rate = 1
        self.samplewise_sampling = samplewise_sampling

        self.acc_scores = None
        self.acc_counts = None

        assert self.max_memory_size >= 1

    def set_kb(self, kb, kb_real_size):
        self.kb = kb
        self.kb_size = kb_real_size

        # set weights (initially uniform)
        self.kb_priority = np.ones((kb_real_size,), dtype=np.float) * (1 / kb_real_size)

        self.acc_scores = np.zeros((kb_real_size,), dtype=np.float32)
        self.acc_counts = np.zeros((kb_real_size,), dtype=np.float32)

    def set_priority(self, model_additional_info):
        pass

    def update_priority_weights(self, batch_wise_CE, query_only_CE, memory_attention,
                                additional_info,
                                indices, labels, update_mode='loss_gain',
                                filter_positive_samples=True):

        kb_priority = tf.cast(additional_info['kb_priority'], tf.float32)

        if not self.samplewise_sampling:
            indices = tf.tile(indices, [labels.shape[0], 1])

        # Update rate counter
        acc_scores = additional_info['acc_scores']
        acc_counts = additional_info['acc_counts']

        # Update weights based on memory attention scores
        if update_mode == 'attention_only':
            attention_scores = tf.stop_gradient(memory_attention)
            # [update_size]
            attention_scores = tf.squeeze(attention_scores)

            if filter_positive_samples:
                if type(labels) == dict:
                    raise NotImplementedError(
                        "Positive examples filtering in multi-label scenario is currently not supported!")

                reduced_targets = tf.argmax(labels, axis=-1)
                positive_mask = tf.where(reduced_targets == 1, tf.ones_like(reduced_targets),
                                         tf.zeros_like(reduced_targets))
                positive_mask = tf.cast(positive_mask, attention_scores.dtype)
                positive_attention_scores = attention_scores * positive_mask[:, None]

                # Store scores for cumulative update
                acc_scores = tf.tensor_scatter_nd_add(tensor=acc_scores,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(positive_attention_scores,
                                                                         [-1, ]))
                acc_counts = tf.tensor_scatter_nd_add(tensor=acc_counts,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(
                                                          (tf.ones_like(positive_attention_scores) * positive_mask[:, None]),
                                                          [-1, ]))
            else:
                # Store scores for cumulative update
                acc_scores = tf.tensor_scatter_nd_add(tensor=acc_scores,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(attention_scores, [-1, ]))
                acc_counts = tf.tensor_scatter_nd_add(tensor=acc_counts,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(tf.ones_like(attention_scores), [-1, ]))

        # Update weights based on: memory attention and loss gain
        if update_mode == 'loss_gain':
            # the higher the difference, the better is the memory
            # negative gain -> very poor added information
            gain = query_only_CE - tf.stop_gradient(batch_wise_CE)
            gain = tf.clip_by_value(gain, -3.0, 3.0)

            # [update_size]
            attention_scores = memory_attention
            attention_scores = tf.squeeze(attention_scores)

            if filter_positive_samples:
                if type(labels) == dict:
                    raise NotImplementedError(
                        "Positive examples filtering in multi-label scenario is currently not supported!")

                reduced_targets = tf.argmax(labels, axis=-1)
                positive_mask = tf.where(reduced_targets == 1, tf.ones_like(reduced_targets),
                                         tf.zeros_like(reduced_targets))
                positive_mask = tf.cast(positive_mask, gain.dtype)
                gain = attention_scores * positive_mask[:, None] * tf.exp(gain)[:, None]

                acc_scores = tf.tensor_scatter_nd_add(tensor=acc_scores,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(gain,
                                                                         [-1, ]))
                acc_counts = tf.tensor_scatter_nd_add(tensor=acc_counts,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(
                                                          (tf.ones_like(gain) * positive_mask[:, None]),
                                                          [-1, ]))
            else:
                gain = attention_scores * tf.exp(gain)[:, None]
                acc_scores = tf.tensor_scatter_nd_add(tensor=acc_scores,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(gain, [-1, ]))
                acc_counts = tf.tensor_scatter_nd_add(tensor=acc_counts,
                                                      indices=tf.reshape(indices, [-1, 1]),
                                                      updates=tf.reshape(tf.ones_like(gain), [-1, ]))

        return {'kb_priority': kb_priority,
                'acc_scores': acc_scores,
                'acc_counts': acc_counts}

    def sampling(self, item, additional_info=None):
        pass

    def update_internal_state(self, batch_wise_CE, query_only_CE, sampled_indices, memory_attention,
                              additional_info, targets):
        pass

    def random_choice_uniform(self, memory_size, size):
        indices = tf.range(0, memory_size, dtype=tf.int32)
        sample_index = tf.random.shuffle(indices)[:size]
        return sample_index

    def _random_repeated_choice_uniform_body(self, iteration, indices, memory_size, size):
        sampled_indices = self.random_choice_uniform(memory_size=memory_size,
                                                     size=size)

        sampled_indices = tf.reshape(sampled_indices, [1, -1])
        indices = indices.write(iteration, sampled_indices)
        iteration = tf.add(iteration, 1)

        return iteration, indices

    def random_repeated_choice_uniform(self, memory_size, sampling_size, iterations):
        iteration = tf.constant(0, dtype=tf.int32)
        sampled_indices = tf.TensorArray(tf.int32, size=iterations, element_shape=(1, sampling_size))
        exit_condition = lambda i, indices: tf.less(i, iterations)
        body_function = lambda i, indices: self._random_repeated_choice_uniform_body(
            memory_size=memory_size,
            size=sampling_size,
            iteration=i,
            indices=indices)
        iteration, sampled_indices = tf.while_loop(exit_condition, body_function, [iteration, sampled_indices])

        return sampled_indices.concat()

    # Gumbel-max trick
    def priority_choice(self, memory_size, size, priority):
        z = -tf.math.log(-tf.math.log(tf.random.uniform([memory_size, ], 0, 1)))
        priority = tf.cast(priority, z.dtype)
        _, indices = tf.nn.top_k(tf.math.log(priority) + z, size)

        return indices

    def _random_repeated_priority_choice_body(self, iteration, indices, memory_size, size, priority):
        sampled_indices = self.priority_choice(memory_size=memory_size,
                                               size=size,
                                               priority=priority)

        sampled_indices = tf.reshape(sampled_indices, [1, -1])
        indices = indices.write(iteration, sampled_indices)
        iteration = tf.add(iteration, 1)

        return iteration, indices

    def random_repeated_priority_choice(self, memory_size, sampling_size, iterations, priority):
        iteration = tf.constant(0, dtype=tf.int32)
        sampled_indices = tf.TensorArray(tf.int32, size=iterations, element_shape=(1, sampling_size))
        exit_condition = lambda i, indices: tf.less(i, iterations)
        body_function = lambda i, indices: self._random_repeated_priority_choice_body(
            memory_size=memory_size,
            size=sampling_size,
            iteration=i,
            indices=indices,
            priority=priority)
        iteration, sampled_indices = tf.while_loop(exit_condition, body_function, [iteration, sampled_indices])
        return sampled_indices.concat()

    def get_additional_info(self):
        return {
            'kb_size': self.kb_size
        }


class Memn2nSampler(Sampler):

    def __init__(self, sampling_mode='uniform', priority_update_mode='loss_gain',
                 alpha=0.7, epsilon=0.01, filter_positive_samples=True, **kwargs):
        super(Memn2nSampler, self).__init__(**kwargs)
        self.sampled_indices = None
        self.sampling_mode = sampling_mode
        self.priority_update_mode = priority_update_mode
        self.alpha = alpha
        self.epsilon = epsilon
        self.filter_positive_samples = filter_positive_samples

        assert priority_update_mode in ['attention_only', 'loss_gain']

        if sampling_mode.lower().strip() == 'uniform':
            self.sampling_method = self.uniform_sampling
        elif sampling_mode.lower().strip() == 'priority':
            self.sampling_method = self.priority_sampling
        elif sampling_mode.lower().strip() == 'ground_truth':
            self.sampling_method = self.ground_truth_sampling
        else:
            raise AttributeError('Invalid sampling_mode given! Got: {}'.format(sampling_mode))

    def set_kb(self, kb, kb_real_size):
        self.kb = kb
        self.kb_size = kb_real_size

        # set weights (initially uniform)
        if self.sampling_mode != 'ground_truth':
            self.kb_priority = np.ones((kb_real_size,), dtype=np.float) * (1 / kb_real_size)

        self.acc_scores = np.zeros((kb_real_size,), dtype=np.float32)
        self.acc_counts = np.zeros((kb_real_size,), dtype=np.float32)

    def priority_sampling(self, item, additional_info=None):
        batch_size = item['text'].shape[0]

        kb_priority = additional_info['kb_priority']

        # Normalize priority
        kb_priority = kb_priority / tf.reduce_sum(kb_priority)

        kb_ids = self.kb['kb_ids']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_priority_choice(memory_size=full_memory_size,
                                                               sampling_size=self.max_memory_size,
                                                               priority=kb_priority,
                                                               iterations=batch_size if self.samplewise_sampling else 1)

        # Step 2: Sample memory and supervision targets
        # Sample memory
        sampled_kb_ids = tf.gather(kb_ids, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Store indices
        self.sampled_indices = sampled_indices

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'text': item['text'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_ids': sampled_kb_ids,
                   'kb_mask': sampled_kb_mask
               }

    def uniform_sampling(self, item, additional_info=None):
        batch_size = item['text'].shape[0]

        kb_ids = self.kb['kb_ids']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_choice_uniform(memory_size=full_memory_size,
                                                              sampling_size=self.max_memory_size,
                                                              iterations=batch_size if self.samplewise_sampling else 1)

        # Step 2: Sample memory and supervision targets
        # Sample memory
        sampled_kb_ids = tf.gather(kb_ids, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Store indices
        self.sampled_indices = sampled_indices

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'text': item['text'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_ids': sampled_kb_ids,
                   'kb_mask': sampled_kb_mask
               }

    def ground_truth_sampling(self, item, additional_info=None):
        batch_size = item['text'].shape[0]

        kb_priority = additional_info['kb_priority']

        kb_ids = self.kb['kb_ids']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_priority_choice(memory_size=full_memory_size,
                                                               sampling_size=self.max_memory_size,
                                                               priority=kb_priority,
                                                               iterations=batch_size if self.samplewise_sampling else 1)

        # Step 2: Sample memory and supervision targets
        # Sample memory
        sampled_kb_ids = tf.gather(kb_ids, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Store indices
        self.sampled_indices = sampled_indices

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'text': item['text'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_ids': sampled_kb_ids,
                   'kb_mask': sampled_kb_mask
               }

    def sampling(self, item, additional_info=None):
        return self.sampling_method(item, additional_info=additional_info)

    def set_priority(self, model_additional_info):
        if self.sampling_mode.lower().strip() == 'priority':
            assert 'sampler_state' in model_additional_info
            kb_priority = model_additional_info['sampler_state']['kb_priority'].numpy()
            acc_scores = model_additional_info['sampler_state']['acc_scores'].numpy()
            acc_counts = model_additional_info['sampler_state']['acc_counts'].numpy()

            if self.current_rate == self.update_rate:
                priority_mask = np.minimum(acc_counts, 1.0)
                priority = acc_scores / np.maximum(acc_counts, 1.0)
                priority = (priority + self.epsilon) ** self.alpha

                # Un-normalized priority
                kb_priority = priority * priority_mask + (1 - priority_mask) * kb_priority

                self.current_rate = 0

                acc_scores *= 0
                acc_counts *= 0

            # Update sampler state
            self.kb_priority = kb_priority
            self.current_rate += 1
            self.acc_scores = acc_scores
            self.acc_counts = acc_counts

    def update_internal_state(self, batch_wise_CE, query_only_CE, sampled_indices, memory_attention,
                              additional_info, labels):
        if self.sampling_mode.lower().strip() == 'priority':
            update_info = self.update_priority_weights(batch_wise_CE=batch_wise_CE,
                                                       query_only_CE=query_only_CE,
                                                       memory_attention=memory_attention,
                                                       additional_info=additional_info,
                                                       labels=labels,
                                                       indices=sampled_indices,
                                                       update_mode=self.priority_update_mode,
                                                       filter_positive_samples=self.filter_positive_samples)
            return update_info
        else:
            return None

    def get_additional_info(self):
        additional_info = super(Memn2nSampler, self).get_additional_info()
        additional_info['sampled_indices'] = self.sampled_indices
        return additional_info


class MemBertSampler(Sampler):

    def __init__(self, sampling_mode='uniform', priority_update_mode='loss_gain',
                 alpha=0.7, epsilon=0.01, filter_positive_samples=True, **kwargs):
        super(MemBertSampler, self).__init__(**kwargs)
        self.sampled_indices = None
        self.sampling_mode = sampling_mode
        self.priority_update_mode = priority_update_mode
        self.alpha = alpha
        self.epsilon = epsilon
        self.filter_positive_samples = filter_positive_samples

        assert priority_update_mode in ['attention_only', 'loss_gain']

        if sampling_mode.lower().strip() == 'uniform':
            self.sampling_method = self.uniform_sampling
        elif sampling_mode.lower().strip() == 'priority':
            self.sampling_method = self.priority_sampling
        elif sampling_mode.lower().strip() == 'ground_truth':
            self.sampling_method = self.ground_truth_sampling
        else:
            raise AttributeError('Invalid sampling_mode given! Got: {}'.format(sampling_mode))

    def priority_sampling(self, item, additional_info=None):
        """
        Kb is a dictionary
        kb_input_ids,
        kb_attention_mask,
        kb_token_type_ids
        kb_mask
        """

        batch_size = item['input_ids'].shape[0]

        kb_priority = additional_info['kb_priority']

        # Normalize priority
        kb_priority = kb_priority / tf.reduce_sum(kb_priority)

        kb_input_ids = self.kb['kb_input_ids']
        kb_attention_mask = self.kb['kb_attention_mask']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_input_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_priority_choice(memory_size=full_memory_size,
                                                               sampling_size=self.max_memory_size,
                                                               priority=kb_priority,
                                                               iterations=batch_size if self.samplewise_sampling else 1)

        # Store indices
        self.sampled_indices = sampled_indices

        sampled_kb_input_ids = tf.gather(kb_input_ids, sampled_indices, axis=0)
        sampled_kb_attention_mask = tf.gather(kb_attention_mask, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'input_ids': item['input_ids'],
                   'attention_mask': item['attention_mask'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_input_ids': sampled_kb_input_ids,
                   'kb_attention_mask': sampled_kb_attention_mask,
                   'kb_mask': sampled_kb_mask
               }

    def uniform_sampling(self, item, additional_info=None):
        """
        Kb is a dictionary
        kb_input_ids,
        kb_attention_mask,
        kb_token_type_ids
        kb_mask
        """

        batch_size = item['input_ids'].shape[0]

        kb_input_ids = self.kb['kb_input_ids']
        kb_attention_mask = self.kb['kb_attention_mask']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_input_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_choice_uniform(memory_size=full_memory_size,
                                                              sampling_size=self.max_memory_size,
                                                              iterations=batch_size if self.samplewise_sampling else 1)

        sampled_kb_input_ids = tf.gather(kb_input_ids, sampled_indices, axis=0)
        sampled_kb_attention_mask = tf.gather(kb_attention_mask, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Store indices
        self.sampled_indices = sampled_indices

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'input_ids': item['input_ids'],
                   'attention_mask': item['attention_mask'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_input_ids': sampled_kb_input_ids,
                   'kb_attention_mask': sampled_kb_attention_mask,
                   'kb_mask': sampled_kb_mask
               }

    def ground_truth_sampling(self, item, additional_info=None):
        """
        Kb is a dictionary
        kb_input_ids,
        kb_attention_mask,
        kb_token_type_ids
        kb_mask
        """

        batch_size = item['input_ids'].shape[0]

        kb_priority = additional_info['kb_priority']

        kb_input_ids = self.kb['kb_input_ids']
        kb_attention_mask = self.kb['kb_attention_mask']
        kb_mask = self.kb['kb_mask']

        full_memory_size = kb_input_ids.shape[0]

        # Step 1: Get random indexes
        sampled_indices = self.random_repeated_priority_choice(memory_size=full_memory_size,
                                                               sampling_size=self.max_memory_size,
                                                               priority=kb_priority,
                                                               iterations=batch_size if self.samplewise_sampling else 1)

        # Store indices
        self.sampled_indices = sampled_indices

        sampled_kb_input_ids = tf.gather(kb_input_ids, sampled_indices, axis=0)
        sampled_kb_attention_mask = tf.gather(kb_attention_mask, sampled_indices, axis=0)
        sampled_kb_mask = tf.gather(kb_mask, sampled_indices, axis=0)

        # Sample target mask
        if self.samplewise_sampling:
            sampled_target_mask = tf.gather(item['target_mask'], sampled_indices, batch_dims=1)
        else:
            sampled_target_mask = tf.gather(item['target_mask'], tf.tile(sampled_indices, [batch_size, 1]), batch_dims=1)

        return {
                   'input_ids': item['input_ids'],
                   'attention_mask': item['attention_mask'],
                   'target_mask': sampled_target_mask
               }, {
                   'kb_input_ids': sampled_kb_input_ids,
                   'kb_attention_mask': sampled_kb_attention_mask,
                   'kb_mask': sampled_kb_mask
               }

    def sampling(self, item, additional_info=None):
        return self.sampling_method(item, additional_info=additional_info)

    def set_priority(self, model_additional_info):
        if self.sampling_mode.lower().strip() == 'priority':
            assert 'sampler_state' in model_additional_info
            kb_priority = model_additional_info['sampler_state']['kb_priority'].numpy()
            acc_scores = model_additional_info['sampler_state']['acc_scores'].numpy()
            acc_counts = model_additional_info['sampler_state']['acc_counts'].numpy()

            if self.current_rate == self.update_rate:
                priority_mask = np.minimum(acc_counts, 1.0)
                priority = acc_scores / np.maximum(acc_counts, 1.0)
                priority = (priority + self.epsilon) ** self.alpha

                kb_priority = priority * priority_mask + (1 - priority_mask) * kb_priority

                self.current_rate = 0

                acc_scores *= 0
                acc_counts *= 0

            # Update sampler state
            self.kb_priority = kb_priority
            self.current_rate += 1
            self.acc_scores = acc_scores
            self.acc_counts = acc_counts

    def update_internal_state(self, batch_wise_CE, query_only_CE, sampled_indices, memory_attention,
                              additional_info, labels):
        if self.sampling_mode.lower().strip() == 'priority':
            update_info = self.update_priority_weights(batch_wise_CE=batch_wise_CE,
                                                       query_only_CE=query_only_CE,
                                                       memory_attention=memory_attention,
                                                       additional_info=additional_info,
                                                       labels=labels,
                                                       indices=sampled_indices,
                                                       update_mode=self.priority_update_mode,
                                                       filter_positive_samples=self.filter_positive_samples)
            return update_info
        else:
            return None

    def get_additional_info(self):
        additional_info = super(MemBertSampler, self).get_additional_info()
        additional_info['sampled_indices'] = self.sampled_indices
        return additional_info


class SamplerFactory(object):
    supported_data_samplers = {
        'memn2n_sampler': Memn2nSampler,
        'mem_bert_sampler': MemBertSampler
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        key = cl_type.lower()
        if SamplerFactory.supported_data_samplers[key]:
            return SamplerFactory.supported_data_samplers[key](**kwargs)
        else:
            raise ValueError('Bad type creation: {}'.format(cl_type))
