"""

@Author: Federico Ruggeri

@Date: 29/11/2018

Constant python configuration script.

TODO: clean deprecated models.
TODO: clean other stuff (reporters, etc...)

------------
 Algorithms
------------

Algorithms are structured based on their architecture. Right now only two architectures are supported. Feel free to
add new ones.

    1. Sentence wise: these models just consider a simple text input.
    2. Context wise: these models also consider an additional context input.
       Memory Networks follow this architecture by considering the memory as the context.

Since each model may have its own pipeline the MODEL_CONFIG dictionary stores ad hoc configurations. If a model is
not found in there, the default configuration associated to its architecture is applied, stored in ALGORITHM_CONFIG.

A pipeline is comprised of three steps: preprocessing, conversion and set splitting.

------------
 Constants
------------

Additionally, framework constants are defined here: folder paths and file names. This is useful since many scripts
share the same paths or save information by using a fixed set of names. In this way, code re-factoring is easier.


"""

import os

NAME_LOG = 'daily_log.log'

# Algorithms

SUPPORTED_LEARNERS = ['sentence_wise', 'context_wise']
SUPPORTED_ALGORITHMS = {
    'experimental_basic_memn2n_v2': {
        'save_suffix': 'experimental_basic_memn2n_v2',
        'model_type': 'context_wise'
    },
    'experimental_baseline_lstm_v2': {
        'save_suffix': 'experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'experimental_baseline_cnn_v2': {
        'save_suffix': 'experimental_baseline_cnn_v2',
        'model_type': 'sentence_wise'
    },
    'experimental_gated_memn2n_v2': {
        'save_suffix': 'experimental_gated_memn2n_v2',
        'model_type': 'context_wise'
    },
    'experimental_discriminative_memn2n_v2': {
        'save_suffix': 'experimental_discriminative_memn2n_v2',
        'model_type': 'context_wise'
    },
    'experimental_windowed_HAN_v2': {
        'save_suffix': 'experimental_windowed_HAN_v2',
        'model_type': 'context_wise'
    },
    'experimental_res_memn2n_v2': {
        'save_suffix': 'experimental_res_memn2n_v2',
        'model_type': 'context_wise'
    },
    'experimental_baseline_sum_v2': {
        'save_suffix': 'experimental_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },
    'experimental_pairwise_baseline_sum_v2': {
        'save_suffix': 'experimental_pairwise_baseline_sum_v2',
        'model_type': 'sentence_wise'
    },
    'experimental_mapper_memn2n_v2': {
        'save_suffix': 'experimental_mapper_memn2n_v2',
        'model_type': 'context_wise'
    },

    'experimental_multi_basic_memn2n_v2': {
        'save_suffix': 'experimental_multi_basic_memn2n_v2',
        'model_type': 'context_wise'
    },

    'distilbert-base-uncased': {
        'save_suffix': 'distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'memory-distilbert-base-uncased': {
        'save_suffix': 'memory-distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'distilroberta-base': {
        'save_suffix': 'distilroberta-base',
        'model_type': 'sentence_wise'
    },
    'memory-distilroberta-base': {
        'save_suffix': 'memory-distilroberta-base',
        'model_type': 'sentence_wise'
    },

    'bert-base-uncased': {
        'save_suffix': 'bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'memory-bert-base-uncased': {
        'save_suffix': 'memory-bert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'multi-distilbert-base-uncased': {
        'save_suffix': 'multi-distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'multi-memory-distilbert-base-uncased': {
        'save_suffix': 'multi-memory-distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'experimental_multi_baseline_lstm_v2': {
        'save_suffix': 'experimental_multi_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'experimental_multi_baseline_cnn_v2': {
        'save_suffix': 'experimental_multi_baseline_cnn_v2',
        'model_type': 'sentence_wise'
    },

    'rationale_cnn': {
        'save_suffix': 'rationale_cnn',
        'model_type': 'sentence_wise'
    },

    'ibm2015_experimental_basic_memn2n_v2': {
        'save_suffix': 'ibm2015_experimental_basic_memn2n_v2',
        'model_type': 'context_wise'
    },
    'ibm2015_experimental_baseline_lstm_v2': {
        'save_suffix': 'ibm2015_experimental_baseline_lstm_v2',
        'model_type': 'sentence_wise'
    },
    'ibm2015_experimental_baseline_cnn_v2': {
        'save_suffix': 'ibm2015_experimental_baseline_cnn_v2',
        'model_type': 'sentence_wise'
    },

    'ibm2015_distilbert-base-uncased': {
        'save_suffix': 'ibm2015_distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'ibm2015_memory-distilbert-base-uncased': {
        'save_suffix': 'ibm2015_memory-distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'ibm2015_distilroberta-base': {
        'save_suffix': 'ibm2015_distilroberta-base',
        'model_type': 'sentence_wise'
    },
    'ibm2015_memory-distilroberta-base': {
        'save_suffix': 'ibm2015_memory-distilroberta-base',
        'model_type': 'sentence_wise'
    },

    'ibm2015_bert-base-uncased': {
        'save_suffix': 'ibm2015_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'ibm2015_memory-bert-base-uncased': {
        'save_suffix': 'ibm2015_memory-bert-base-uncased',
        'model_type': 'sentence_wise'
    },

    'squad_mann': {
        'save_suffix': 'squad_mann',
        'model_type': 'sentence_wise'
    },
    'squad_distilbert-base-uncased': {
        'save_suffix': 'squad_distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'squad_distilroberta-base': {
        'save_suffix': 'squad_distilroberta-base',
        'model_type': 'sentence_wise'
    },
    'squad_bert-base-uncased': {
        'save_suffix': 'squad_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'bidaf': {
        'save_suffix': 'bidaf',
        'model_type': 'sentence_wise'
    },

    'squad2_mann': {
        'save_suffix': 'squad2_mann',
        'model_type': 'sentence_wise'
    },
    'squad2_distilbert-base-uncased': {
        'save_suffix': 'squad2_distilbert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'squad2_distilroberta-base': {
        'save_suffix': 'squad2_distilroberta-base',
        'model_type': 'sentence_wise'
    },
    'squad2_bert-base-uncased': {
        'save_suffix': 'squad2_bert-base-uncased',
        'model_type': 'sentence_wise'
    },
    'bidaf2': {
        'save_suffix': 'bidaf2',
        'model_type': 'sentence_wise'
    },
}

MODEL_CONFIG = {
    'experimental_basic_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter',
        'sampler': 'memn2n_sampler'
    },
    'experimental_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'experimental_baseline_cnn_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'experimental_gated_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter'
    },
    'experimental_discriminative_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter'
    },
    'experimental_windowed_HAN_v2': {
        'processor': 'text_kb_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_converter'
    },
    'experimental_res_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter'
    },
    'experimental_baseline_sum_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'experimental_pairwise_baseline_sum_v2': {
        'processor': 'pairwise_text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'pairwise_text_converter'
    },
    'experimental_mapper_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter',
        'sampler': 'memn2n_sampler'
    },

    'experimental_multi_basic_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter',
        'sampler': 'memn2n_sampler'
    },

    'experimental_multi_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'experimental_multi_baseline_cnn_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'distilbert-base-uncased': {
        'processor': 'bert_text_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'bert_converter'
    },
    'memory-distilbert-base-uncased': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'distilroberta-base': {
        'processor': 'bert_text_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'bert_converter'
    },
    'memory-distilroberta-base': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'bert-base-uncased': {
        'processor': 'bert_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'memory-bert-base-uncased': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'multi-distilbert-base-uncased': {
        'processor': 'bert_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'multi-memory-distilbert-base-uncased': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    "rationale_cnn": {
        "processor": 'rationale_cnn_processor',
        "tokenizer": 'rationale_keras_tokenizer',
        "converter": 'rationale_cnn_converter'
    },

    'ibm2015_experimental_basic_memn2n_v2': {
        'processor': 'text_kb_supervision_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'kb_supervision_converter',
        'sampler': 'memn2n_sampler'
    },
    'ibm2015_experimental_baseline_lstm_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'ibm2015_experimental_baseline_cnn_v2': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },

    'ibm2015_distilbert-base-uncased': {
        'processor': 'bert_text_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'bert_converter'
    },
    'ibm2015_memory-distilbert-base-uncased': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'ibm2015_distilroberta-base': {
        'processor': 'bert_text_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'bert_converter'
    },
    'ibm2015_memory-distilroberta-base': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'ibm2015_bert-base-uncased': {
        'processor': 'bert_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_converter'
    },
    'ibm2015_memory-bert-base-uncased': {
        'processor': 'bert_text_kb_supervision_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'kb_supervision_bert_converter',
        'sampler': 'mem_bert_sampler'
    },

    'squad_mann': {
        'processor': 'squad_text_processor',
        'tokenizer': 'spacy_tokenizer',
        'converter': 'squad_text_converter'
    },
    'squad_distilbert-base-uncased': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'squad_distilroberta-base': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'squad_bert-base-uncased': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'bidaf': {
        'processor': 'squad_text_processor',
        'tokenizer': 'spacy_tokenizer',
        'converter': 'squad_text_converter'
    },

    'squad2_mann': {
        'processor': 'squad_text_processor',
        'tokenizer': 'spacy_tokenizer',
        'converter': 'squad_text_converter'
    },
    'squad2_distilbert-base-uncased': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'distilbert_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'squad2_distilroberta-base': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'roberta_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'squad2_bert-base-uncased': {
        'processor': 'bert_squad_text_processor',
        'tokenizer': 'bert_tokenizer',
        'converter': 'bert_squad_text_converter'
    },
    'bidaf2': {
        'processor': 'squad_text_processor',
        'tokenizer': 'spacy_tokenizer',
        'converter': 'squad_text_converter'
    },
}

ALGORITHM_CONFIG = {
    'sentence_wise': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    },
    'context_wise': {
        'processor': 'text_processor',
        'tokenizer': 'keras_tokenizer',
        'converter': 'base_converter'
    }
}

# DEFAULT DIRECTORIES
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIGS_DIR = os.path.join(PROJECT_DIR, 'configs')
INTERNAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'datasets')
SAVED_MODELS_DIR = os.path.join(PROJECT_DIR, 'saved_models')
SAVED_MODELS_ADDITIONAL_RESULTS_DIR = os.path.join(PROJECT_DIR, 'saved_models_additional_results')
INFERENCE_DIR = os.path.join(PROJECT_DIR, 'inference')
PATH_LIST_DATASETS_XLS = os.path.join(PROJECT_DIR, 'ml_utilities', 'ml_lib', 'utility')
PATH_LOG = os.path.join(PROJECT_DIR, 'log')
REPORT_PATH = os.path.join(PROJECT_DIR, 'reports')
RUNNABLES_DIR = os.path.join(PROJECT_DIR, 'runnables')
LOCAL_DATASETS_DIR = os.path.join(PROJECT_DIR, 'local_database')
LOO_DIR = os.path.join(PROJECT_DIR, 'loo_test')
CV_DIR = os.path.join(PROJECT_DIR, 'cv_test')
TRAIN_AND_TEST_DIR = os.path.join(PROJECT_DIR, 'train_and_test')
CALIBRATION_RESULTS_DIR = os.path.join(PROJECT_DIR, 'calibration_results')
EMBEDDING_MODELS_DIR = os.path.join(PROJECT_DIR, 'embedding_models')
KB_DIR = os.path.join(LOCAL_DATASETS_DIR, 'KB')
PREBUILT_FOLDS_DIR = os.path.join(PROJECT_DIR, 'prebuilt_folds')
SAVED_INFO_DIR = os.path.join(PROJECT_DIR, 'saved_info')
MONGO_DB_DIR = os.path.join(PROJECT_DIR, 'mongo_db')
TESTS_DATA_DIR = os.path.join(PROJECT_DIR, 'tests_data')

TOS_50_DIR = os.path.join(LOCAL_DATASETS_DIR, 'ToS_50')
TOS_100_DIR = os.path.join(LOCAL_DATASETS_DIR, 'ToS_100')
WIKIMOVIES_DIR = os.path.join(LOCAL_DATASETS_DIR, 'movieqa')
IBM2015_DIR = os.path.join(LOCAL_DATASETS_DIR, 'IBM_Debater_(R)_CE-EMNLP-2015.v3')

DATASET_PATHS = {
    'tos_50': TOS_50_DIR,
    'tos_100': TOS_100_DIR
}

# JSON FILES
JSON_CALLBACKS_NAME = 'callbacks.json'
JSON_MODEL_CONFIG_NAME = 'model_config.json'
JSON_DISTRIBUTED_MODEL_CONFIG_NAME = 'distributed_model_config.json'
JSON_MODEL_HISTORY_NAME = 'model_history.json'
JSON_TRAINING_CONFIG_NAME = 'training_config.json'
JSON_LOO_TEST_CONFIG_NAME = 'loo_test_config.json'
JSON_CV_TEST_CONFIG_NAME = 'cv_test_config.json'
JSON_TRAIN_AND_TEST_CONFIG_NAME = 'train_and_test_config.json'
JSON_DATA_LOADER_CONFIG_NAME = 'data_loader.json'
JSON_HYPEROPT_MODEL_GRIDSEARCH_NAME = 'hyperopt_model_gridsearch.json'
JSON_CALIBRATOR_INFO_NAME = 'calibrator_info.json'
JSON_VALIDATION_INFO_NAME = 'validation_info.json'
JSON_TEST_INFO_NAME = 'test_info.json'
JSON_PREDICTIONS_NAME = 'predictions.json'
JSON_DISTRIBUTED_CONFIG_NAME = 'distributed_config.json'
JSON_MODEL_DATA_CONFIGS_NAME = 'data_configs.json'
JSON_QUICK_SETUP_CONFIG_NAME = "quick_setup_config.json"

# NAMES REPORTS
REPORT_TRAIN_CSV = 'report_train.csv'
REPORT_PRED_CSV = 'report_pred.csv'
