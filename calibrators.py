"""

@Author: Federico Ruggeri

@Date: 30/11/2018

TODO: re-define calibration parameters as a python configuration file. JSON argument parsing should be deprecated.
TODO: add use_mongo flag argument for enabling/disabling specific Trials object.

"""

import os
import pickle

import numpy as np
from hyperopt import fmin, space_eval, tpe, Trials
from hyperopt.mongoexp import MongoTrials

import const_define as cd
from configs import hyperopt_spaces
from utility.cross_validation_utils import average_validation_info, get_validate_condition_value
from utility.json_utils import save_json
from utility.log_utils import get_logger

logger = get_logger(__name__)


class Base(object):
    """
    Base calibrator interface.
    """

    def load_space(self):
        """
        Loads parameter combinations (i.e. search space) from which the calibrator should detect
        the best one, according to the specified evaluation metrics.
        """

        pass

    def run(self, space):
        """
        Calibrator main interface. The run() methods tells the calibrator class to start the parameter search routine.

        :param space: parameters space on which the calibrator optimization search is based.
        """

        pass


class HyperOptCalibrator(Base):
    """
    Hyperopt compliant calibrator which leverages the hyperopt library for the parameter optimization task.
    """

    def __init__(self, model_type, validator_method, validator_base_args,
                 validate_on, validate_condition, db_name, hyperopt_additional_info=None,
                 max_evals=10, random_seed=42, use_mongo=True):

        super(HyperOptCalibrator, self).__init__()

        self.model_type = model_type
        self.validator_method = validator_method
        self.validator_base_args = validator_base_args
        self.validate_on = validate_on
        self.validate_condition = validate_condition
        self.max_evals = max_evals
        self.random_seed = random_seed
        self.hyperopt_additional_info = hyperopt_additional_info
        self.db_name = db_name
        self.use_mongo = use_mongo

        if validate_on not in validator_base_args['error_metrics']:
            message = 'Validate on metric should belong to error metrics!'
            logger.exception(message)
            raise RuntimeError(message)

        self.save_folder = os.path.join(cd.CALIBRATION_RESULTS_DIR, validator_method.__name__, self.model_type)

        # Save folder
        if not os.path.isdir(self.save_folder):
            os.makedirs(self.save_folder)

    def _parse_json_item(self, item, hyperopt_operator_str):
        """
        Parses hyperopt_model_gridsearch.json values in order to build the hyperopt compliant parameter search space

        :param item: list of values for a given parameter
        :param hyperopt_operator_str: string defining the hyperopt parameter selection method (see hyperopt.hp)
        :return: parsed item
        """

        if hyperopt_operator_str == 'choice':
            return [item]
        if hyperopt_operator_str == 'uniform':
            return [0.0, item]

        return item

    def load_space(self):

        space = hyperopt_spaces.spaces[self.model_type]
        return space

    def _minimize_objective(self, param_comb):
        """
        Objective function for the hyperopt calibrator.
        Specifically, for each selected parameter combination a K-fold routine is held, so as to
        obtain robust evaluation metrics test results.

        :param param_comb: a single parameters combination (python dictionary)
        :return: reference evaluation metric result (float)
        """

        for key in param_comb:
            self.validator_base_args['network_args'][key]['value'] = param_comb[key]

        logger.info('Considering hyper-parameters: {}'.format(param_comb))
        logger.info('Network args: {}'.format(self.validator_base_args['network_args']))

        validation_info = self.validator_method(**self.validator_base_args)

        # Average validation errors
        avg_validation_info = average_validation_info(test_validation_info=validation_info['validation_info'])

        logger.info('Combination validation error: {}'.format(avg_validation_info))

        validation_on_value = get_validate_condition_value(validation_error=avg_validation_info,
                                                           validate_on=self.validate_on)

        if self.validate_condition == 'maximization':
            return 1 - validation_on_value

        return validation_on_value

    def run(self, space):
        """
        Starts the grid-search task, which is formulated a minimization problem (evaluation metric minimization).
        All the grid-search intermediate results are stored in a .csv file, along with the Trials object.
        Eventually, the best configuration is saved in JSON format, according to the same syntax used for describing
        the parameter space.
        """

        logger.info('Starting hyper-parameters calibration search! Max evaluations: {}'.format(self.max_evals))

        # Reproducibility
        np.random.seed(self.random_seed)

        if self.use_mongo:
            trials = MongoTrials('mongo://localhost:1234/{}/jobs'.format(self.db_name), exp_key='exp1')
        else:
            # Check for existing trials
            if not os.path.isdir(cd.MONGO_DB_DIR):
                os.makedirs(cd.MONGO_DB_DIR)

            trials_path = os.path.join(cd.MONGO_DB_DIR, '{}.pkl'.format(self.db_name))
            if os.path.exists(trials_path):
                logger.info('Using existing Trials DB!')
                with open(trials_path, 'rb') as f:
                    trials = pickle.load(f)
            else:
                logger.info("Can't find specified Trials DB ({})...creating new one!".format(self.db_name))
                trials = Trials(exp_key='exp1')

        best = fmin(self._minimize_objective,
                    space,
                    algo=tpe.suggest,
                    max_evals=self.max_evals,
                    trials=trials,
                    **self.hyperopt_additional_info)

        best_params = space_eval(space, best)

        logger.info('Hyper-parameters calibration ended..')
        logger.info('Best combination: {}'.format(best_params))

        if not self.use_mongo:
            with open(os.path.join(cd.MONGO_DB_DIR, '{}.pickle'.format(self.db_name)), 'w') as f:
                pickle.dump(f, trials)

        # Clear folder
        for filename in self.save_folder:
            if filename.startswith(self.model_type):
                sub_path = os.path.join(self.save_folder, filename)
                if os.path.isfile(sub_path):
                    os.unlink(sub_path)

        # Best model
        model_info = self.validator_base_args['network_args']

        best_model_info = {}
        for key in model_info:
            if key in best_params:
                best_model_info.setdefault(key, {})['value'] = best_params[key]
            else:
                best_model_info.setdefault(key, {})['value'] = model_info[key]['value']
            best_model_info[key]['flags'] = model_info[key]['flags']

        save_json(os.path.join(self.save_folder,
                               '{}_best_model_info.json'.format(self.db_name)),
                  best_model_info)
